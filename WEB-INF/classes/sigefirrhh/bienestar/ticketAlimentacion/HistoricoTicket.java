package sigefirrhh.bienestar.ticketAlimentacion;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.ProveedorTicket;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Dependencia;
import sigefirrhh.personal.trabajador.Trabajador;

public class HistoricoTicket
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idHistoricoTicket;
  private int anio;
  private int mes;
  private int mesProceso;
  private int anioProceso;
  private Date fechaProceso;
  private int cantidadTicket;
  private double denominacionTicket;
  private double montoCobrar;
  private int totalDescuentosTicket;
  private int netoCantidadTicket;
  private double netoMontoCobrar;
  private String especial;
  private String codTicket;
  private Trabajador trabajador;
  private TipoPersonal tipoPersonal;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  private ProveedorTicket proveedorTicket;
  private Dependencia dependencia;
  private String codDependencia;
  private String nombreDependencia;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "anio", "anioProceso", "cantidadTicket", "codDependencia", "codTicket", "conceptoTipoPersonal", "denominacionTicket", "dependencia", "especial", "fechaProceso", "idHistoricoTicket", "mes", "mesProceso", "montoCobrar", "netoCantidadTicket", "netoMontoCobrar", "nombreDependencia", "proveedorTicket", "tipoPersonal", "totalDescuentosTicket", "trabajador" }; private static final Class[] jdoFieldTypes = { Integer.TYPE, Integer.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), Double.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Dependencia"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Integer.TYPE, Integer.TYPE, Double.TYPE, Integer.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.bienestar.ProveedorTicket"), sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), Integer.TYPE, sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 26, 21, 26, 21, 21, 24, 21, 21, 21, 21, 21, 21, 26, 26, 21, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.ticketAlimentacion.HistoricoTicket"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new HistoricoTicket());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public HistoricoTicket()
  {
    jdoSetcantidadTicket(this, 0);

    jdoSetdenominacionTicket(this, 0.0D);

    jdoSetmontoCobrar(this, 0.0D);

    jdoSettotalDescuentosTicket(this, 0);

    jdoSetnetoCantidadTicket(this, 0);

    jdoSetnetoMontoCobrar(this, 0.0D);

    jdoSetespecial(this, "N");
  }

  public String toString()
  {
    DecimalFormat b = new DecimalFormat();
    b.applyPattern("##,###,###.00");
    String a = b.format(jdoGetnetoMontoCobrar(this));

    return jdoGetanio(this) + "-" + jdoGetmes(this) + "  -  " + jdoGetnetoCantidadTicket(this) + " - " + a;
  }

  public int getAnio() {
    return jdoGetanio(this);
  }

  public void setAnio(int anio)
  {
    jdoSetanio(this, anio);
  }

  public int getAnioProceso()
  {
    return jdoGetanioProceso(this);
  }

  public void setAnioProceso(int anioProceso)
  {
    jdoSetanioProceso(this, anioProceso);
  }

  public String getCodDependencia() {
    return jdoGetcodDependencia(this);
  }

  public void setCodDependencia(String codDependencia)
  {
    jdoSetcodDependencia(this, codDependencia);
  }

  public String getCodTicket()
  {
    return jdoGetcodTicket(this);
  }

  public void setCodTicket(String codTicket)
  {
    jdoSetcodTicket(this, codTicket);
  }

  public ConceptoTipoPersonal getConceptoTipoPersonal()
  {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal)
  {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }

  public Dependencia getDependencia() {
    return jdoGetdependencia(this);
  }

  public void setDependencia(Dependencia dependencia)
  {
    jdoSetdependencia(this, dependencia);
  }

  public String getEspecial()
  {
    return jdoGetespecial(this);
  }

  public void setEspecial(String especial)
  {
    jdoSetespecial(this, especial);
  }

  public Date getFechaProceso()
  {
    return jdoGetfechaProceso(this);
  }

  public void setFechaProceso(Date fechaProceso)
  {
    jdoSetfechaProceso(this, fechaProceso);
  }

  public long getIdHistoricoTicket()
  {
    return jdoGetidHistoricoTicket(this);
  }

  public void setIdHistoricoTicket(long idHistoricoTicket)
  {
    jdoSetidHistoricoTicket(this, idHistoricoTicket);
  }

  public int getMes()
  {
    return jdoGetmes(this);
  }

  public void setMes(int mes)
  {
    jdoSetmes(this, mes);
  }

  public int getMesProceso()
  {
    return jdoGetmesProceso(this);
  }

  public void setMesProceso(int mesProceso)
  {
    jdoSetmesProceso(this, mesProceso);
  }

  public double getMontoCobrar()
  {
    return jdoGetmontoCobrar(this);
  }

  public void setMontoCobrar(double montoCobrar)
  {
    jdoSetmontoCobrar(this, montoCobrar);
  }

  public int getCantidadTicket() {
    return jdoGetcantidadTicket(this);
  }

  public void setCantidadTicket(int cantidadTicket) {
    jdoSetcantidadTicket(this, cantidadTicket);
  }

  public int getNetoCantidadTicket() {
    return jdoGetnetoCantidadTicket(this);
  }

  public void setNetoCantidadTicket(int netoCantidadTicket) {
    jdoSetnetoCantidadTicket(this, netoCantidadTicket);
  }

  public String getNombreDependencia() {
    return jdoGetnombreDependencia(this);
  }

  public void setNombreDependencia(String nombreDependencia)
  {
    jdoSetnombreDependencia(this, nombreDependencia);
  }

  public ProveedorTicket getProveedorTicket()
  {
    return jdoGetproveedorTicket(this);
  }

  public void setProveedorTicket(ProveedorTicket proveedorTicket)
  {
    jdoSetproveedorTicket(this, proveedorTicket);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public void setTipoPersonal(TipoPersonal tipoPersonal)
  {
    jdoSettipoPersonal(this, tipoPersonal);
  }

  public int getTotalDescuentosTicket()
  {
    return jdoGettotalDescuentosTicket(this);
  }

  public void setTotalDescuentosTicket(int totalDescuentosTicket)
  {
    jdoSettotalDescuentosTicket(this, totalDescuentosTicket);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public double getDenominacionTicket() {
    return jdoGetdenominacionTicket(this);
  }

  public void setDenominacionTicket(double denominacionTicket) {
    jdoSetdenominacionTicket(this, denominacionTicket);
  }

  public double getNetoMontoCobrar() {
    return jdoGetnetoMontoCobrar(this);
  }

  public void setNetoMontoCobrar(double netoMontoCobrar) {
    jdoSetnetoMontoCobrar(this, netoMontoCobrar);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 21;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    HistoricoTicket localHistoricoTicket = new HistoricoTicket();
    localHistoricoTicket.jdoFlags = 1;
    localHistoricoTicket.jdoStateManager = paramStateManager;
    return localHistoricoTicket;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    HistoricoTicket localHistoricoTicket = new HistoricoTicket();
    localHistoricoTicket.jdoCopyKeyFieldsFromObjectId(paramObject);
    localHistoricoTicket.jdoFlags = 1;
    localHistoricoTicket.jdoStateManager = paramStateManager;
    return localHistoricoTicket;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anio);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.anioProceso);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cantidadTicket);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codDependencia);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codTicket);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.denominacionTicket);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.dependencia);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.especial);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaProceso);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idHistoricoTicket);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mes);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.mesProceso);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoCobrar);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.netoCantidadTicket);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.netoMontoCobrar);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreDependencia);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.proveedorTicket);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.totalDescuentosTicket);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anio = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.anioProceso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cantidadTicket = localStateManager.replacingIntField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codTicket = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.denominacionTicket = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.dependencia = ((Dependencia)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.especial = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaProceso = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idHistoricoTicket = localStateManager.replacingLongField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mes = localStateManager.replacingIntField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.mesProceso = localStateManager.replacingIntField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoCobrar = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.netoCantidadTicket = localStateManager.replacingIntField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.netoMontoCobrar = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreDependencia = localStateManager.replacingStringField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.proveedorTicket = ((ProveedorTicket)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 19:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.totalDescuentosTicket = localStateManager.replacingIntField(this, paramInt);
      return;
    case 20:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(HistoricoTicket paramHistoricoTicket, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.anio = paramHistoricoTicket.anio;
      return;
    case 1:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.anioProceso = paramHistoricoTicket.anioProceso;
      return;
    case 2:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.cantidadTicket = paramHistoricoTicket.cantidadTicket;
      return;
    case 3:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.codDependencia = paramHistoricoTicket.codDependencia;
      return;
    case 4:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.codTicket = paramHistoricoTicket.codTicket;
      return;
    case 5:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramHistoricoTicket.conceptoTipoPersonal;
      return;
    case 6:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.denominacionTicket = paramHistoricoTicket.denominacionTicket;
      return;
    case 7:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.dependencia = paramHistoricoTicket.dependencia;
      return;
    case 8:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.especial = paramHistoricoTicket.especial;
      return;
    case 9:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.fechaProceso = paramHistoricoTicket.fechaProceso;
      return;
    case 10:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.idHistoricoTicket = paramHistoricoTicket.idHistoricoTicket;
      return;
    case 11:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.mes = paramHistoricoTicket.mes;
      return;
    case 12:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.mesProceso = paramHistoricoTicket.mesProceso;
      return;
    case 13:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.montoCobrar = paramHistoricoTicket.montoCobrar;
      return;
    case 14:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.netoCantidadTicket = paramHistoricoTicket.netoCantidadTicket;
      return;
    case 15:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.netoMontoCobrar = paramHistoricoTicket.netoMontoCobrar;
      return;
    case 16:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.nombreDependencia = paramHistoricoTicket.nombreDependencia;
      return;
    case 17:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.proveedorTicket = paramHistoricoTicket.proveedorTicket;
      return;
    case 18:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramHistoricoTicket.tipoPersonal;
      return;
    case 19:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.totalDescuentosTicket = paramHistoricoTicket.totalDescuentosTicket;
      return;
    case 20:
      if (paramHistoricoTicket == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramHistoricoTicket.trabajador;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof HistoricoTicket))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    HistoricoTicket localHistoricoTicket = (HistoricoTicket)paramObject;
    if (localHistoricoTicket.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localHistoricoTicket, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new HistoricoTicketPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new HistoricoTicketPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoTicketPK))
      throw new IllegalArgumentException("arg1");
    HistoricoTicketPK localHistoricoTicketPK = (HistoricoTicketPK)paramObject;
    localHistoricoTicketPK.idHistoricoTicket = this.idHistoricoTicket;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof HistoricoTicketPK))
      throw new IllegalArgumentException("arg1");
    HistoricoTicketPK localHistoricoTicketPK = (HistoricoTicketPK)paramObject;
    this.idHistoricoTicket = localHistoricoTicketPK.idHistoricoTicket;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoTicketPK))
      throw new IllegalArgumentException("arg2");
    HistoricoTicketPK localHistoricoTicketPK = (HistoricoTicketPK)paramObject;
    localHistoricoTicketPK.idHistoricoTicket = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 10);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof HistoricoTicketPK))
      throw new IllegalArgumentException("arg2");
    HistoricoTicketPK localHistoricoTicketPK = (HistoricoTicketPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 10, localHistoricoTicketPK.idHistoricoTicket);
  }

  private static final int jdoGetanio(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.anio;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.anio;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 0))
      return paramHistoricoTicket.anio;
    return localStateManager.getIntField(paramHistoricoTicket, jdoInheritedFieldCount + 0, paramHistoricoTicket.anio);
  }

  private static final void jdoSetanio(HistoricoTicket paramHistoricoTicket, int paramInt)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.anio = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.anio = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoTicket, jdoInheritedFieldCount + 0, paramHistoricoTicket.anio, paramInt);
  }

  private static final int jdoGetanioProceso(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.anioProceso;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.anioProceso;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 1))
      return paramHistoricoTicket.anioProceso;
    return localStateManager.getIntField(paramHistoricoTicket, jdoInheritedFieldCount + 1, paramHistoricoTicket.anioProceso);
  }

  private static final void jdoSetanioProceso(HistoricoTicket paramHistoricoTicket, int paramInt)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.anioProceso = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.anioProceso = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoTicket, jdoInheritedFieldCount + 1, paramHistoricoTicket.anioProceso, paramInt);
  }

  private static final int jdoGetcantidadTicket(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.cantidadTicket;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.cantidadTicket;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 2))
      return paramHistoricoTicket.cantidadTicket;
    return localStateManager.getIntField(paramHistoricoTicket, jdoInheritedFieldCount + 2, paramHistoricoTicket.cantidadTicket);
  }

  private static final void jdoSetcantidadTicket(HistoricoTicket paramHistoricoTicket, int paramInt)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.cantidadTicket = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.cantidadTicket = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoTicket, jdoInheritedFieldCount + 2, paramHistoricoTicket.cantidadTicket, paramInt);
  }

  private static final String jdoGetcodDependencia(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.codDependencia;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.codDependencia;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 3))
      return paramHistoricoTicket.codDependencia;
    return localStateManager.getStringField(paramHistoricoTicket, jdoInheritedFieldCount + 3, paramHistoricoTicket.codDependencia);
  }

  private static final void jdoSetcodDependencia(HistoricoTicket paramHistoricoTicket, String paramString)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.codDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.codDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoTicket, jdoInheritedFieldCount + 3, paramHistoricoTicket.codDependencia, paramString);
  }

  private static final String jdoGetcodTicket(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.codTicket;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.codTicket;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 4))
      return paramHistoricoTicket.codTicket;
    return localStateManager.getStringField(paramHistoricoTicket, jdoInheritedFieldCount + 4, paramHistoricoTicket.codTicket);
  }

  private static final void jdoSetcodTicket(HistoricoTicket paramHistoricoTicket, String paramString)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.codTicket = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.codTicket = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoTicket, jdoInheritedFieldCount + 4, paramHistoricoTicket.codTicket, paramString);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(HistoricoTicket paramHistoricoTicket)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 5))
      return paramHistoricoTicket.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 5, paramHistoricoTicket.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(HistoricoTicket paramHistoricoTicket, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 5, paramHistoricoTicket.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final double jdoGetdenominacionTicket(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.denominacionTicket;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.denominacionTicket;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 6))
      return paramHistoricoTicket.denominacionTicket;
    return localStateManager.getDoubleField(paramHistoricoTicket, jdoInheritedFieldCount + 6, paramHistoricoTicket.denominacionTicket);
  }

  private static final void jdoSetdenominacionTicket(HistoricoTicket paramHistoricoTicket, double paramDouble)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.denominacionTicket = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.denominacionTicket = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoTicket, jdoInheritedFieldCount + 6, paramHistoricoTicket.denominacionTicket, paramDouble);
  }

  private static final Dependencia jdoGetdependencia(HistoricoTicket paramHistoricoTicket)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.dependencia;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 7))
      return paramHistoricoTicket.dependencia;
    return (Dependencia)localStateManager.getObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 7, paramHistoricoTicket.dependencia);
  }

  private static final void jdoSetdependencia(HistoricoTicket paramHistoricoTicket, Dependencia paramDependencia)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.dependencia = paramDependencia;
      return;
    }
    localStateManager.setObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 7, paramHistoricoTicket.dependencia, paramDependencia);
  }

  private static final String jdoGetespecial(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.especial;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.especial;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 8))
      return paramHistoricoTicket.especial;
    return localStateManager.getStringField(paramHistoricoTicket, jdoInheritedFieldCount + 8, paramHistoricoTicket.especial);
  }

  private static final void jdoSetespecial(HistoricoTicket paramHistoricoTicket, String paramString)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.especial = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.especial = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoTicket, jdoInheritedFieldCount + 8, paramHistoricoTicket.especial, paramString);
  }

  private static final Date jdoGetfechaProceso(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.fechaProceso;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.fechaProceso;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 9))
      return paramHistoricoTicket.fechaProceso;
    return (Date)localStateManager.getObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 9, paramHistoricoTicket.fechaProceso);
  }

  private static final void jdoSetfechaProceso(HistoricoTicket paramHistoricoTicket, Date paramDate)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.fechaProceso = paramDate;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.fechaProceso = paramDate;
      return;
    }
    localStateManager.setObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 9, paramHistoricoTicket.fechaProceso, paramDate);
  }

  private static final long jdoGetidHistoricoTicket(HistoricoTicket paramHistoricoTicket)
  {
    return paramHistoricoTicket.idHistoricoTicket;
  }

  private static final void jdoSetidHistoricoTicket(HistoricoTicket paramHistoricoTicket, long paramLong)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.idHistoricoTicket = paramLong;
      return;
    }
    localStateManager.setLongField(paramHistoricoTicket, jdoInheritedFieldCount + 10, paramHistoricoTicket.idHistoricoTicket, paramLong);
  }

  private static final int jdoGetmes(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.mes;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.mes;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 11))
      return paramHistoricoTicket.mes;
    return localStateManager.getIntField(paramHistoricoTicket, jdoInheritedFieldCount + 11, paramHistoricoTicket.mes);
  }

  private static final void jdoSetmes(HistoricoTicket paramHistoricoTicket, int paramInt)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.mes = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.mes = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoTicket, jdoInheritedFieldCount + 11, paramHistoricoTicket.mes, paramInt);
  }

  private static final int jdoGetmesProceso(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.mesProceso;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.mesProceso;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 12))
      return paramHistoricoTicket.mesProceso;
    return localStateManager.getIntField(paramHistoricoTicket, jdoInheritedFieldCount + 12, paramHistoricoTicket.mesProceso);
  }

  private static final void jdoSetmesProceso(HistoricoTicket paramHistoricoTicket, int paramInt)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.mesProceso = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.mesProceso = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoTicket, jdoInheritedFieldCount + 12, paramHistoricoTicket.mesProceso, paramInt);
  }

  private static final double jdoGetmontoCobrar(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.montoCobrar;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.montoCobrar;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 13))
      return paramHistoricoTicket.montoCobrar;
    return localStateManager.getDoubleField(paramHistoricoTicket, jdoInheritedFieldCount + 13, paramHistoricoTicket.montoCobrar);
  }

  private static final void jdoSetmontoCobrar(HistoricoTicket paramHistoricoTicket, double paramDouble)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.montoCobrar = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.montoCobrar = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoTicket, jdoInheritedFieldCount + 13, paramHistoricoTicket.montoCobrar, paramDouble);
  }

  private static final int jdoGetnetoCantidadTicket(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.netoCantidadTicket;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.netoCantidadTicket;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 14))
      return paramHistoricoTicket.netoCantidadTicket;
    return localStateManager.getIntField(paramHistoricoTicket, jdoInheritedFieldCount + 14, paramHistoricoTicket.netoCantidadTicket);
  }

  private static final void jdoSetnetoCantidadTicket(HistoricoTicket paramHistoricoTicket, int paramInt)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.netoCantidadTicket = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.netoCantidadTicket = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoTicket, jdoInheritedFieldCount + 14, paramHistoricoTicket.netoCantidadTicket, paramInt);
  }

  private static final double jdoGetnetoMontoCobrar(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.netoMontoCobrar;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.netoMontoCobrar;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 15))
      return paramHistoricoTicket.netoMontoCobrar;
    return localStateManager.getDoubleField(paramHistoricoTicket, jdoInheritedFieldCount + 15, paramHistoricoTicket.netoMontoCobrar);
  }

  private static final void jdoSetnetoMontoCobrar(HistoricoTicket paramHistoricoTicket, double paramDouble)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.netoMontoCobrar = paramDouble;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.netoMontoCobrar = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramHistoricoTicket, jdoInheritedFieldCount + 15, paramHistoricoTicket.netoMontoCobrar, paramDouble);
  }

  private static final String jdoGetnombreDependencia(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.nombreDependencia;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.nombreDependencia;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 16))
      return paramHistoricoTicket.nombreDependencia;
    return localStateManager.getStringField(paramHistoricoTicket, jdoInheritedFieldCount + 16, paramHistoricoTicket.nombreDependencia);
  }

  private static final void jdoSetnombreDependencia(HistoricoTicket paramHistoricoTicket, String paramString)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.nombreDependencia = paramString;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.nombreDependencia = paramString;
      return;
    }
    localStateManager.setStringField(paramHistoricoTicket, jdoInheritedFieldCount + 16, paramHistoricoTicket.nombreDependencia, paramString);
  }

  private static final ProveedorTicket jdoGetproveedorTicket(HistoricoTicket paramHistoricoTicket)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.proveedorTicket;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 17))
      return paramHistoricoTicket.proveedorTicket;
    return (ProveedorTicket)localStateManager.getObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 17, paramHistoricoTicket.proveedorTicket);
  }

  private static final void jdoSetproveedorTicket(HistoricoTicket paramHistoricoTicket, ProveedorTicket paramProveedorTicket)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.proveedorTicket = paramProveedorTicket;
      return;
    }
    localStateManager.setObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 17, paramHistoricoTicket.proveedorTicket, paramProveedorTicket);
  }

  private static final TipoPersonal jdoGettipoPersonal(HistoricoTicket paramHistoricoTicket)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.tipoPersonal;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 18))
      return paramHistoricoTicket.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 18, paramHistoricoTicket.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(HistoricoTicket paramHistoricoTicket, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 18, paramHistoricoTicket.tipoPersonal, paramTipoPersonal);
  }

  private static final int jdoGettotalDescuentosTicket(HistoricoTicket paramHistoricoTicket)
  {
    if (paramHistoricoTicket.jdoFlags <= 0)
      return paramHistoricoTicket.totalDescuentosTicket;
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.totalDescuentosTicket;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 19))
      return paramHistoricoTicket.totalDescuentosTicket;
    return localStateManager.getIntField(paramHistoricoTicket, jdoInheritedFieldCount + 19, paramHistoricoTicket.totalDescuentosTicket);
  }

  private static final void jdoSettotalDescuentosTicket(HistoricoTicket paramHistoricoTicket, int paramInt)
  {
    if (paramHistoricoTicket.jdoFlags == 0)
    {
      paramHistoricoTicket.totalDescuentosTicket = paramInt;
      return;
    }
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.totalDescuentosTicket = paramInt;
      return;
    }
    localStateManager.setIntField(paramHistoricoTicket, jdoInheritedFieldCount + 19, paramHistoricoTicket.totalDescuentosTicket, paramInt);
  }

  private static final Trabajador jdoGettrabajador(HistoricoTicket paramHistoricoTicket)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
      return paramHistoricoTicket.trabajador;
    if (localStateManager.isLoaded(paramHistoricoTicket, jdoInheritedFieldCount + 20))
      return paramHistoricoTicket.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 20, paramHistoricoTicket.trabajador);
  }

  private static final void jdoSettrabajador(HistoricoTicket paramHistoricoTicket, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramHistoricoTicket.jdoStateManager;
    if (localStateManager == null)
    {
      paramHistoricoTicket.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramHistoricoTicket, jdoInheritedFieldCount + 20, paramHistoricoTicket.trabajador, paramTrabajador);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}