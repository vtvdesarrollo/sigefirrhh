package sigefirrhh.bienestar.ticketAlimentacion;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class SeguridadTicketForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(SeguridadTicketForm.class.getName());
  private SeguridadTicket seguridadTicket;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private TicketAlimentacionFacade ticketAlimentacionFacade = new TicketAlimentacionFacade();
  private boolean showSeguridadTicketByGrupoTicket;
  private String findSelectGrupoTicket;
  private Collection findColGrupoTicket;
  private Collection colGrupoTicket;
  private String selectGrupoTicket;
  private Object stateResultSeguridadTicketByGrupoTicket = null;

  public String getFindSelectGrupoTicket()
  {
    return this.findSelectGrupoTicket;
  }
  public void setFindSelectGrupoTicket(String valGrupoTicket) {
    this.findSelectGrupoTicket = valGrupoTicket;
  }

  public Collection getFindColGrupoTicket() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColGrupoTicket.iterator();
    GrupoTicket grupoTicket = null;
    while (iterator.hasNext()) {
      grupoTicket = (GrupoTicket)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoTicket.getIdGrupoTicket()), 
        grupoTicket.toString()));
    }
    return col;
  }

  public String getSelectGrupoTicket()
  {
    return this.selectGrupoTicket;
  }
  public void setSelectGrupoTicket(String valGrupoTicket) {
    Iterator iterator = this.colGrupoTicket.iterator();
    GrupoTicket grupoTicket = null;
    this.seguridadTicket.setGrupoTicket(null);
    while (iterator.hasNext()) {
      grupoTicket = (GrupoTicket)iterator.next();
      if (String.valueOf(grupoTicket.getIdGrupoTicket()).equals(
        valGrupoTicket)) {
        this.seguridadTicket.setGrupoTicket(
          grupoTicket);
        break;
      }
    }
    this.selectGrupoTicket = valGrupoTicket;
  }
  public Collection getResult() {
    return this.result;
  }

  public SeguridadTicket getSeguridadTicket() {
    if (this.seguridadTicket == null) {
      this.seguridadTicket = new SeguridadTicket();
    }
    return this.seguridadTicket;
  }

  public SeguridadTicketForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getListEspecial()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = SeguridadTicket.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getColGrupoTicket()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colGrupoTicket.iterator();
    GrupoTicket grupoTicket = null;
    while (iterator.hasNext()) {
      grupoTicket = (GrupoTicket)iterator.next();
      col.add(new SelectItem(
        String.valueOf(grupoTicket.getIdGrupoTicket()), 
        grupoTicket.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.findColGrupoTicket = 
        this.ticketAlimentacionFacade.findGrupoTicketByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colGrupoTicket = 
        this.ticketAlimentacionFacade.findGrupoTicketByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findSeguridadTicketByGrupoTicket()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.ticketAlimentacionFacade.findSeguridadTicketByGrupoTicket(Long.valueOf(this.findSelectGrupoTicket).longValue());
      this.showSeguridadTicketByGrupoTicket = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showSeguridadTicketByGrupoTicket)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectGrupoTicket = null;

    return null;
  }

  public boolean isShowSeguridadTicketByGrupoTicket() {
    return this.showSeguridadTicketByGrupoTicket;
  }

  public String selectSeguridadTicket()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectGrupoTicket = null;

    long idSeguridadTicket = 
      Long.parseLong((String)requestParameterMap.get("idSeguridadTicket"));
    try
    {
      this.seguridadTicket = 
        this.ticketAlimentacionFacade.findSeguridadTicketById(
        idSeguridadTicket);
      if (this.seguridadTicket.getGrupoTicket() != null) {
        this.selectGrupoTicket = 
          String.valueOf(this.seguridadTicket.getGrupoTicket().getIdGrupoTicket());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.seguridadTicket = null;
    this.showSeguridadTicketByGrupoTicket = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if ((this.seguridadTicket.getFechaProceso() != null) && 
      (this.seguridadTicket.getFechaProceso().compareTo(new Date()) > 0)) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "La fecha Fecha Proceso no puede ser mayor a la fecha de hoy", ""));
      error = true;
    }

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.ticketAlimentacionFacade.addSeguridadTicket(
          this.seguridadTicket);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.ticketAlimentacionFacade.updateSeguridadTicket(
          this.seguridadTicket);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.ticketAlimentacionFacade.deleteSeguridadTicket(
        this.seguridadTicket);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.seguridadTicket = new SeguridadTicket();

    this.selectGrupoTicket = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.seguridadTicket.setIdSeguridadTicket(identityGenerator.getNextSequenceNumber("sigefirrhh.bienestar.ticketAlimentacion.SeguridadTicket"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.seguridadTicket = new SeguridadTicket();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}