package sigefirrhh.bienestar.beca;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.Personal;

public class BecaFamiliar
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_NIVEL_EDUCATIVO;
  protected static final Map LISTA_SINO;
  protected static final Map LISTA_ESTATUS;
  protected static final Map LISTA_TRAMITE;
  private long idBecaFamiliar;
  private Date fecha;
  private Personal personal;
  private Familiar familiar;
  private String tramiteBeca;
  private String nivelEducativo;
  private double montoBeca;
  private int grado;
  private int promedioNotas;
  private String estatusBeca;
  private String instituto;
  private String institutoOtraBeca;
  private String carreraEspecialidad;
  private String otraBeca;
  private double montoOtraBeca;
  private Date fechaUltimoPago;
  private String trajoRecaudos;
  private String pagada;
  private ConceptoTipoPersonal conceptoTipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "carreraEspecialidad", "conceptoTipoPersonal", "estatusBeca", "familiar", "fecha", "fechaUltimoPago", "grado", "idBecaFamiliar", "instituto", "institutoOtraBeca", "montoBeca", "montoOtraBeca", "nivelEducativo", "otraBeca", "pagada", "personal", "promedioNotas", "trajoRecaudos", "tramiteBeca" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.ConceptoTipoPersonal"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Familiar"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Double.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.expediente.Personal"), Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 26, 21, 26, 21, 21, 21, 24, 21, 21, 21, 21, 21, 21, 21, 26, 21, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.beca.BecaFamiliar"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new BecaFamiliar());

    LISTA_NIVEL_EDUCATIVO = 
      new LinkedHashMap();
    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_ESTATUS = 
      new LinkedHashMap();
    LISTA_TRAMITE = 
      new LinkedHashMap();
    LISTA_ESTATUS.put("A", "APROBADA");
    LISTA_ESTATUS.put("L", "RECHAZADA POR LIMITE");
    LISTA_ESTATUS.put("N", "RECHAZADA POR NOTAS");
    LISTA_ESTATUS.put("C", "RECHAZADA POR CRITERIO");
    LISTA_ESTATUS.put("S", "SOLICITUD");
    LISTA_ESTATUS.put("R", "RECHAZADO EN PROCESO");

    LISTA_TRAMITE.put("S", "SOLICITUD");
    LISTA_TRAMITE.put("R", "RENOVACION");

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");

    LISTA_NIVEL_EDUCATIVO.put("P", "PRESCOLAR");
    LISTA_NIVEL_EDUCATIVO.put("B", "BASICA");
    LISTA_NIVEL_EDUCATIVO.put("I", "PRIMARIA");
    LISTA_NIVEL_EDUCATIVO.put("D", "DIVERSIFICADO");
    LISTA_NIVEL_EDUCATIVO.put("H", "BACHILLERATO");
    LISTA_NIVEL_EDUCATIVO.put("T", "TÉCNICO MEDIO");
    LISTA_NIVEL_EDUCATIVO.put("S", "TÉCNICO SUPERIOR");
    LISTA_NIVEL_EDUCATIVO.put("U", "UNIVERSITARIO");
    LISTA_NIVEL_EDUCATIVO.put("E", "ESPECIALIZACIÓN");
    LISTA_NIVEL_EDUCATIVO.put("M", "MAESTRIA");
    LISTA_NIVEL_EDUCATIVO.put("C", "DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("R", "POST DOCTORADO");
    LISTA_NIVEL_EDUCATIVO.put("G", "POSTGRADO");
    LISTA_NIVEL_EDUCATIVO.put("L", "DIPLOMADO");
    LISTA_NIVEL_EDUCATIVO.put("O", "OTRO");
  }

  public BecaFamiliar()
  {
    jdoSettramiteBeca(this, "S");

    jdoSetnivelEducativo(this, "O");

    jdoSetmontoBeca(this, 0.0D);

    jdoSetestatusBeca(this, "S");

    jdoSetotraBeca(this, "N");

    jdoSetmontoOtraBeca(this, 0.0D);

    jdoSettrajoRecaudos(this, "N");

    jdoSetpagada(this, "N");
  }

  public String toString()
  {
    return jdoGetfamiliar(this).getPrimerApellido() + " " + 
      jdoGetfamiliar(this).getPrimerNombre() + " - " + 
      LISTA_NIVEL_EDUCATIVO.get(jdoGetnivelEducativo(this));
  }

  public String getCarreraEspecialidad() {
    return jdoGetcarreraEspecialidad(this);
  }
  public void setCarreraEspecialidad(String carreraEspecialidad) {
    jdoSetcarreraEspecialidad(this, carreraEspecialidad);
  }
  public Date getFechaUltimoPago() {
    return jdoGetfechaUltimoPago(this);
  }
  public void setFechaUltimoPago(Date fechaUltimoPago) {
    jdoSetfechaUltimoPago(this, fechaUltimoPago);
  }
  public String getInstituto() {
    return jdoGetinstituto(this);
  }
  public void setInstituto(String instituto) {
    jdoSetinstituto(this, instituto);
  }
  public String getInstitutoOtraBeca() {
    return jdoGetinstitutoOtraBeca(this);
  }
  public void setInstitutoOtraBeca(String institutoOtraBeca) {
    jdoSetinstitutoOtraBeca(this, institutoOtraBeca);
  }
  public double getMontoOtraBeca() {
    return jdoGetmontoOtraBeca(this);
  }
  public void setMontoOtraBeca(double montoOtraBeca) {
    jdoSetmontoOtraBeca(this, montoOtraBeca);
  }
  public String getOtraBeca() {
    return jdoGetotraBeca(this);
  }
  public void setOtraBeca(String otraBeca) {
    jdoSetotraBeca(this, otraBeca);
  }
  public String getTrajoRecaudos() {
    return jdoGettrajoRecaudos(this);
  }
  public void setTrajoRecaudos(String trajoRecaudos) {
    jdoSettrajoRecaudos(this, trajoRecaudos);
  }
  public String getTramiteBeca() {
    return jdoGettramiteBeca(this);
  }
  public void setTramiteBeca(String tramiteBecA) {
    jdoSettramiteBeca(this, tramiteBecA);
  }

  public String getEstatusBeca()
  {
    return jdoGetestatusBeca(this);
  }

  public Familiar getFamiliar()
  {
    return jdoGetfamiliar(this);
  }

  public int getGrado()
  {
    return jdoGetgrado(this);
  }

  public long getIdBecaFamiliar()
  {
    return jdoGetidBecaFamiliar(this);
  }

  public double getMontoBeca()
  {
    return jdoGetmontoBeca(this);
  }

  public String getNivelEducativo()
  {
    return jdoGetnivelEducativo(this);
  }

  public Personal getPersonal()
  {
    return jdoGetpersonal(this);
  }

  public int getPromedioNotas()
  {
    return jdoGetpromedioNotas(this);
  }

  public void setEstatusBeca(String string)
  {
    jdoSetestatusBeca(this, string);
  }

  public void setFamiliar(Familiar familiar)
  {
    jdoSetfamiliar(this, familiar);
  }

  public void setGrado(int grado)
  {
    jdoSetgrado(this, grado);
  }

  public void setIdBecaFamiliar(long l)
  {
    jdoSetidBecaFamiliar(this, l);
  }

  public void setMontoBeca(double d)
  {
    jdoSetmontoBeca(this, d);
  }

  public void setNivelEducativo(String string)
  {
    jdoSetnivelEducativo(this, string);
  }

  public void setPersonal(Personal personal)
  {
    jdoSetpersonal(this, personal);
  }

  public void setPromedioNotas(int promedioNotas)
  {
    jdoSetpromedioNotas(this, promedioNotas);
  }

  public Date getFecha() {
    return jdoGetfecha(this);
  }
  public void setFecha(Date fecha) {
    jdoSetfecha(this, fecha);
  }
  public ConceptoTipoPersonal getConceptoTipoPersonal() {
    return jdoGetconceptoTipoPersonal(this);
  }

  public void setConceptoTipoPersonal(ConceptoTipoPersonal conceptoTipoPersonal) {
    jdoSetconceptoTipoPersonal(this, conceptoTipoPersonal);
  }
  public String getPagada() {
    return jdoGetpagada(this);
  }
  public void setPagada(String pagada) {
    jdoSetpagada(this, pagada);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 19;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    BecaFamiliar localBecaFamiliar = new BecaFamiliar();
    localBecaFamiliar.jdoFlags = 1;
    localBecaFamiliar.jdoStateManager = paramStateManager;
    return localBecaFamiliar;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    BecaFamiliar localBecaFamiliar = new BecaFamiliar();
    localBecaFamiliar.jdoCopyKeyFieldsFromObjectId(paramObject);
    localBecaFamiliar.jdoFlags = 1;
    localBecaFamiliar.jdoStateManager = paramStateManager;
    return localBecaFamiliar;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.carreraEspecialidad);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.conceptoTipoPersonal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatusBeca);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.familiar);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fecha);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaUltimoPago);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.grado);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idBecaFamiliar);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.instituto);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.institutoOtraBeca);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoBeca);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoOtraBeca);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nivelEducativo);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.otraBeca);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.pagada);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.personal);
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.promedioNotas);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.trajoRecaudos);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tramiteBeca);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.carreraEspecialidad = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.conceptoTipoPersonal = ((ConceptoTipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatusBeca = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.familiar = ((Familiar)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fecha = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaUltimoPago = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.grado = localStateManager.replacingIntField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idBecaFamiliar = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.instituto = localStateManager.replacingStringField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.institutoOtraBeca = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoBeca = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoOtraBeca = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelEducativo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.otraBeca = localStateManager.replacingStringField(this, paramInt);
      return;
    case 14:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.pagada = localStateManager.replacingStringField(this, paramInt);
      return;
    case 15:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.personal = ((Personal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 16:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.promedioNotas = localStateManager.replacingIntField(this, paramInt);
      return;
    case 17:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trajoRecaudos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 18:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tramiteBeca = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(BecaFamiliar paramBecaFamiliar, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.carreraEspecialidad = paramBecaFamiliar.carreraEspecialidad;
      return;
    case 1:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.conceptoTipoPersonal = paramBecaFamiliar.conceptoTipoPersonal;
      return;
    case 2:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.estatusBeca = paramBecaFamiliar.estatusBeca;
      return;
    case 3:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.familiar = paramBecaFamiliar.familiar;
      return;
    case 4:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.fecha = paramBecaFamiliar.fecha;
      return;
    case 5:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.fechaUltimoPago = paramBecaFamiliar.fechaUltimoPago;
      return;
    case 6:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.grado = paramBecaFamiliar.grado;
      return;
    case 7:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.idBecaFamiliar = paramBecaFamiliar.idBecaFamiliar;
      return;
    case 8:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.instituto = paramBecaFamiliar.instituto;
      return;
    case 9:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.institutoOtraBeca = paramBecaFamiliar.institutoOtraBeca;
      return;
    case 10:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.montoBeca = paramBecaFamiliar.montoBeca;
      return;
    case 11:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.montoOtraBeca = paramBecaFamiliar.montoOtraBeca;
      return;
    case 12:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.nivelEducativo = paramBecaFamiliar.nivelEducativo;
      return;
    case 13:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.otraBeca = paramBecaFamiliar.otraBeca;
      return;
    case 14:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.pagada = paramBecaFamiliar.pagada;
      return;
    case 15:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.personal = paramBecaFamiliar.personal;
      return;
    case 16:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.promedioNotas = paramBecaFamiliar.promedioNotas;
      return;
    case 17:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.trajoRecaudos = paramBecaFamiliar.trajoRecaudos;
      return;
    case 18:
      if (paramBecaFamiliar == null)
        throw new IllegalArgumentException("arg1");
      this.tramiteBeca = paramBecaFamiliar.tramiteBeca;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof BecaFamiliar))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    BecaFamiliar localBecaFamiliar = (BecaFamiliar)paramObject;
    if (localBecaFamiliar.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localBecaFamiliar, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new BecaFamiliarPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new BecaFamiliarPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BecaFamiliarPK))
      throw new IllegalArgumentException("arg1");
    BecaFamiliarPK localBecaFamiliarPK = (BecaFamiliarPK)paramObject;
    localBecaFamiliarPK.idBecaFamiliar = this.idBecaFamiliar;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BecaFamiliarPK))
      throw new IllegalArgumentException("arg1");
    BecaFamiliarPK localBecaFamiliarPK = (BecaFamiliarPK)paramObject;
    this.idBecaFamiliar = localBecaFamiliarPK.idBecaFamiliar;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BecaFamiliarPK))
      throw new IllegalArgumentException("arg2");
    BecaFamiliarPK localBecaFamiliarPK = (BecaFamiliarPK)paramObject;
    localBecaFamiliarPK.idBecaFamiliar = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BecaFamiliarPK))
      throw new IllegalArgumentException("arg2");
    BecaFamiliarPK localBecaFamiliarPK = (BecaFamiliarPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localBecaFamiliarPK.idBecaFamiliar);
  }

  private static final String jdoGetcarreraEspecialidad(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.carreraEspecialidad;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.carreraEspecialidad;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 0))
      return paramBecaFamiliar.carreraEspecialidad;
    return localStateManager.getStringField(paramBecaFamiliar, jdoInheritedFieldCount + 0, paramBecaFamiliar.carreraEspecialidad);
  }

  private static final void jdoSetcarreraEspecialidad(BecaFamiliar paramBecaFamiliar, String paramString)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.carreraEspecialidad = paramString;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.carreraEspecialidad = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaFamiliar, jdoInheritedFieldCount + 0, paramBecaFamiliar.carreraEspecialidad, paramString);
  }

  private static final ConceptoTipoPersonal jdoGetconceptoTipoPersonal(BecaFamiliar paramBecaFamiliar)
  {
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.conceptoTipoPersonal;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 1))
      return paramBecaFamiliar.conceptoTipoPersonal;
    return (ConceptoTipoPersonal)localStateManager.getObjectField(paramBecaFamiliar, jdoInheritedFieldCount + 1, paramBecaFamiliar.conceptoTipoPersonal);
  }

  private static final void jdoSetconceptoTipoPersonal(BecaFamiliar paramBecaFamiliar, ConceptoTipoPersonal paramConceptoTipoPersonal)
  {
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.conceptoTipoPersonal = paramConceptoTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramBecaFamiliar, jdoInheritedFieldCount + 1, paramBecaFamiliar.conceptoTipoPersonal, paramConceptoTipoPersonal);
  }

  private static final String jdoGetestatusBeca(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.estatusBeca;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.estatusBeca;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 2))
      return paramBecaFamiliar.estatusBeca;
    return localStateManager.getStringField(paramBecaFamiliar, jdoInheritedFieldCount + 2, paramBecaFamiliar.estatusBeca);
  }

  private static final void jdoSetestatusBeca(BecaFamiliar paramBecaFamiliar, String paramString)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.estatusBeca = paramString;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.estatusBeca = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaFamiliar, jdoInheritedFieldCount + 2, paramBecaFamiliar.estatusBeca, paramString);
  }

  private static final Familiar jdoGetfamiliar(BecaFamiliar paramBecaFamiliar)
  {
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.familiar;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 3))
      return paramBecaFamiliar.familiar;
    return (Familiar)localStateManager.getObjectField(paramBecaFamiliar, jdoInheritedFieldCount + 3, paramBecaFamiliar.familiar);
  }

  private static final void jdoSetfamiliar(BecaFamiliar paramBecaFamiliar, Familiar paramFamiliar)
  {
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.familiar = paramFamiliar;
      return;
    }
    localStateManager.setObjectField(paramBecaFamiliar, jdoInheritedFieldCount + 3, paramBecaFamiliar.familiar, paramFamiliar);
  }

  private static final Date jdoGetfecha(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.fecha;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.fecha;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 4))
      return paramBecaFamiliar.fecha;
    return (Date)localStateManager.getObjectField(paramBecaFamiliar, jdoInheritedFieldCount + 4, paramBecaFamiliar.fecha);
  }

  private static final void jdoSetfecha(BecaFamiliar paramBecaFamiliar, Date paramDate)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.fecha = paramDate;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.fecha = paramDate;
      return;
    }
    localStateManager.setObjectField(paramBecaFamiliar, jdoInheritedFieldCount + 4, paramBecaFamiliar.fecha, paramDate);
  }

  private static final Date jdoGetfechaUltimoPago(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.fechaUltimoPago;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.fechaUltimoPago;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 5))
      return paramBecaFamiliar.fechaUltimoPago;
    return (Date)localStateManager.getObjectField(paramBecaFamiliar, jdoInheritedFieldCount + 5, paramBecaFamiliar.fechaUltimoPago);
  }

  private static final void jdoSetfechaUltimoPago(BecaFamiliar paramBecaFamiliar, Date paramDate)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.fechaUltimoPago = paramDate;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.fechaUltimoPago = paramDate;
      return;
    }
    localStateManager.setObjectField(paramBecaFamiliar, jdoInheritedFieldCount + 5, paramBecaFamiliar.fechaUltimoPago, paramDate);
  }

  private static final int jdoGetgrado(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.grado;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.grado;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 6))
      return paramBecaFamiliar.grado;
    return localStateManager.getIntField(paramBecaFamiliar, jdoInheritedFieldCount + 6, paramBecaFamiliar.grado);
  }

  private static final void jdoSetgrado(BecaFamiliar paramBecaFamiliar, int paramInt)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.grado = paramInt;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.grado = paramInt;
      return;
    }
    localStateManager.setIntField(paramBecaFamiliar, jdoInheritedFieldCount + 6, paramBecaFamiliar.grado, paramInt);
  }

  private static final long jdoGetidBecaFamiliar(BecaFamiliar paramBecaFamiliar)
  {
    return paramBecaFamiliar.idBecaFamiliar;
  }

  private static final void jdoSetidBecaFamiliar(BecaFamiliar paramBecaFamiliar, long paramLong)
  {
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.idBecaFamiliar = paramLong;
      return;
    }
    localStateManager.setLongField(paramBecaFamiliar, jdoInheritedFieldCount + 7, paramBecaFamiliar.idBecaFamiliar, paramLong);
  }

  private static final String jdoGetinstituto(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.instituto;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.instituto;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 8))
      return paramBecaFamiliar.instituto;
    return localStateManager.getStringField(paramBecaFamiliar, jdoInheritedFieldCount + 8, paramBecaFamiliar.instituto);
  }

  private static final void jdoSetinstituto(BecaFamiliar paramBecaFamiliar, String paramString)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.instituto = paramString;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.instituto = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaFamiliar, jdoInheritedFieldCount + 8, paramBecaFamiliar.instituto, paramString);
  }

  private static final String jdoGetinstitutoOtraBeca(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.institutoOtraBeca;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.institutoOtraBeca;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 9))
      return paramBecaFamiliar.institutoOtraBeca;
    return localStateManager.getStringField(paramBecaFamiliar, jdoInheritedFieldCount + 9, paramBecaFamiliar.institutoOtraBeca);
  }

  private static final void jdoSetinstitutoOtraBeca(BecaFamiliar paramBecaFamiliar, String paramString)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.institutoOtraBeca = paramString;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.institutoOtraBeca = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaFamiliar, jdoInheritedFieldCount + 9, paramBecaFamiliar.institutoOtraBeca, paramString);
  }

  private static final double jdoGetmontoBeca(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.montoBeca;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.montoBeca;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 10))
      return paramBecaFamiliar.montoBeca;
    return localStateManager.getDoubleField(paramBecaFamiliar, jdoInheritedFieldCount + 10, paramBecaFamiliar.montoBeca);
  }

  private static final void jdoSetmontoBeca(BecaFamiliar paramBecaFamiliar, double paramDouble)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.montoBeca = paramDouble;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.montoBeca = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramBecaFamiliar, jdoInheritedFieldCount + 10, paramBecaFamiliar.montoBeca, paramDouble);
  }

  private static final double jdoGetmontoOtraBeca(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.montoOtraBeca;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.montoOtraBeca;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 11))
      return paramBecaFamiliar.montoOtraBeca;
    return localStateManager.getDoubleField(paramBecaFamiliar, jdoInheritedFieldCount + 11, paramBecaFamiliar.montoOtraBeca);
  }

  private static final void jdoSetmontoOtraBeca(BecaFamiliar paramBecaFamiliar, double paramDouble)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.montoOtraBeca = paramDouble;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.montoOtraBeca = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramBecaFamiliar, jdoInheritedFieldCount + 11, paramBecaFamiliar.montoOtraBeca, paramDouble);
  }

  private static final String jdoGetnivelEducativo(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.nivelEducativo;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.nivelEducativo;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 12))
      return paramBecaFamiliar.nivelEducativo;
    return localStateManager.getStringField(paramBecaFamiliar, jdoInheritedFieldCount + 12, paramBecaFamiliar.nivelEducativo);
  }

  private static final void jdoSetnivelEducativo(BecaFamiliar paramBecaFamiliar, String paramString)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.nivelEducativo = paramString;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.nivelEducativo = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaFamiliar, jdoInheritedFieldCount + 12, paramBecaFamiliar.nivelEducativo, paramString);
  }

  private static final String jdoGetotraBeca(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.otraBeca;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.otraBeca;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 13))
      return paramBecaFamiliar.otraBeca;
    return localStateManager.getStringField(paramBecaFamiliar, jdoInheritedFieldCount + 13, paramBecaFamiliar.otraBeca);
  }

  private static final void jdoSetotraBeca(BecaFamiliar paramBecaFamiliar, String paramString)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.otraBeca = paramString;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.otraBeca = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaFamiliar, jdoInheritedFieldCount + 13, paramBecaFamiliar.otraBeca, paramString);
  }

  private static final String jdoGetpagada(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.pagada;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.pagada;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 14))
      return paramBecaFamiliar.pagada;
    return localStateManager.getStringField(paramBecaFamiliar, jdoInheritedFieldCount + 14, paramBecaFamiliar.pagada);
  }

  private static final void jdoSetpagada(BecaFamiliar paramBecaFamiliar, String paramString)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.pagada = paramString;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.pagada = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaFamiliar, jdoInheritedFieldCount + 14, paramBecaFamiliar.pagada, paramString);
  }

  private static final Personal jdoGetpersonal(BecaFamiliar paramBecaFamiliar)
  {
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.personal;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 15))
      return paramBecaFamiliar.personal;
    return (Personal)localStateManager.getObjectField(paramBecaFamiliar, jdoInheritedFieldCount + 15, paramBecaFamiliar.personal);
  }

  private static final void jdoSetpersonal(BecaFamiliar paramBecaFamiliar, Personal paramPersonal)
  {
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.personal = paramPersonal;
      return;
    }
    localStateManager.setObjectField(paramBecaFamiliar, jdoInheritedFieldCount + 15, paramBecaFamiliar.personal, paramPersonal);
  }

  private static final int jdoGetpromedioNotas(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.promedioNotas;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.promedioNotas;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 16))
      return paramBecaFamiliar.promedioNotas;
    return localStateManager.getIntField(paramBecaFamiliar, jdoInheritedFieldCount + 16, paramBecaFamiliar.promedioNotas);
  }

  private static final void jdoSetpromedioNotas(BecaFamiliar paramBecaFamiliar, int paramInt)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.promedioNotas = paramInt;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.promedioNotas = paramInt;
      return;
    }
    localStateManager.setIntField(paramBecaFamiliar, jdoInheritedFieldCount + 16, paramBecaFamiliar.promedioNotas, paramInt);
  }

  private static final String jdoGettrajoRecaudos(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.trajoRecaudos;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.trajoRecaudos;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 17))
      return paramBecaFamiliar.trajoRecaudos;
    return localStateManager.getStringField(paramBecaFamiliar, jdoInheritedFieldCount + 17, paramBecaFamiliar.trajoRecaudos);
  }

  private static final void jdoSettrajoRecaudos(BecaFamiliar paramBecaFamiliar, String paramString)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.trajoRecaudos = paramString;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.trajoRecaudos = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaFamiliar, jdoInheritedFieldCount + 17, paramBecaFamiliar.trajoRecaudos, paramString);
  }

  private static final String jdoGettramiteBeca(BecaFamiliar paramBecaFamiliar)
  {
    if (paramBecaFamiliar.jdoFlags <= 0)
      return paramBecaFamiliar.tramiteBeca;
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
      return paramBecaFamiliar.tramiteBeca;
    if (localStateManager.isLoaded(paramBecaFamiliar, jdoInheritedFieldCount + 18))
      return paramBecaFamiliar.tramiteBeca;
    return localStateManager.getStringField(paramBecaFamiliar, jdoInheritedFieldCount + 18, paramBecaFamiliar.tramiteBeca);
  }

  private static final void jdoSettramiteBeca(BecaFamiliar paramBecaFamiliar, String paramString)
  {
    if (paramBecaFamiliar.jdoFlags == 0)
    {
      paramBecaFamiliar.tramiteBeca = paramString;
      return;
    }
    StateManager localStateManager = paramBecaFamiliar.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaFamiliar.tramiteBeca = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaFamiliar, jdoInheritedFieldCount + 18, paramBecaFamiliar.tramiteBeca, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}