package sigefirrhh.bienestar.beca;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.bienestar.NivelBeca;
import sigefirrhh.base.personal.Profesion;
import sigefirrhh.personal.trabajador.Trabajador;

public class BecaTrabajador
  implements Serializable, PersistenceCapable
{
  private long idBecaTrabajador;
  private double montoBeca;
  private Date fechaInicio;
  private Date fechaFin;
  private String periodoCursando;
  private String unidad;
  private String estatusBeca;
  private NivelBeca nivelBeca;
  private Profesion profesion;
  private Trabajador trabajador;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "estatusBeca", "fechaFin", "fechaInicio", "idBecaTrabajador", "montoBeca", "nivelBeca", "periodoCursando", "profesion", "trabajador", "unidad" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("java.util.Date"), Long.TYPE, Double.TYPE, sunjdo$classForName$("sigefirrhh.base.bienestar.NivelBeca"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.personal.Profesion"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 21, 21, 24, 21, 26, 21, 26, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String getEstatusBeca()
  {
    return jdoGetestatusBeca(this);
  }

  public Date getFechaFin()
  {
    return jdoGetfechaFin(this);
  }

  public Date getFechaInicio()
  {
    return jdoGetfechaInicio(this);
  }

  public long getIdBecaTrabajador()
  {
    return jdoGetidBecaTrabajador(this);
  }

  public double getMontoBeca()
  {
    return jdoGetmontoBeca(this);
  }

  public NivelBeca getNivelBeca()
  {
    return jdoGetnivelBeca(this);
  }

  public String getPeriodoCursando()
  {
    return jdoGetperiodoCursando(this);
  }

  public Profesion getProfesion()
  {
    return jdoGetprofesion(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public String getUnidad()
  {
    return jdoGetunidad(this);
  }

  public void setEstatusBeca(String string)
  {
    jdoSetestatusBeca(this, string);
  }

  public void setFechaFin(Date date)
  {
    jdoSetfechaFin(this, date);
  }

  public void setFechaInicio(Date date)
  {
    jdoSetfechaInicio(this, date);
  }

  public void setIdBecaTrabajador(long l)
  {
    jdoSetidBecaTrabajador(this, l);
  }

  public void setMontoBeca(double d)
  {
    jdoSetmontoBeca(this, d);
  }

  public void setNivelBeca(NivelBeca beca)
  {
    jdoSetnivelBeca(this, beca);
  }

  public void setPeriodoCursando(String string)
  {
    jdoSetperiodoCursando(this, string);
  }

  public void setProfesion(Profesion profesion)
  {
    jdoSetprofesion(this, profesion);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setUnidad(String string)
  {
    jdoSetunidad(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.bienestar.beca.BecaTrabajador"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new BecaTrabajador());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    BecaTrabajador localBecaTrabajador = new BecaTrabajador();
    localBecaTrabajador.jdoFlags = 1;
    localBecaTrabajador.jdoStateManager = paramStateManager;
    return localBecaTrabajador;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    BecaTrabajador localBecaTrabajador = new BecaTrabajador();
    localBecaTrabajador.jdoCopyKeyFieldsFromObjectId(paramObject);
    localBecaTrabajador.jdoFlags = 1;
    localBecaTrabajador.jdoStateManager = paramStateManager;
    return localBecaTrabajador;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.estatusBeca);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaFin);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaInicio);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idBecaTrabajador);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoBeca);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.nivelBeca);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.periodoCursando);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.profesion);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.unidad);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.estatusBeca = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaFin = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaInicio = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idBecaTrabajador = localStateManager.replacingLongField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoBeca = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nivelBeca = ((NivelBeca)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.periodoCursando = localStateManager.replacingStringField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.profesion = ((Profesion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidad = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(BecaTrabajador paramBecaTrabajador, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramBecaTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.estatusBeca = paramBecaTrabajador.estatusBeca;
      return;
    case 1:
      if (paramBecaTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaFin = paramBecaTrabajador.fechaFin;
      return;
    case 2:
      if (paramBecaTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.fechaInicio = paramBecaTrabajador.fechaInicio;
      return;
    case 3:
      if (paramBecaTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.idBecaTrabajador = paramBecaTrabajador.idBecaTrabajador;
      return;
    case 4:
      if (paramBecaTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.montoBeca = paramBecaTrabajador.montoBeca;
      return;
    case 5:
      if (paramBecaTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.nivelBeca = paramBecaTrabajador.nivelBeca;
      return;
    case 6:
      if (paramBecaTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.periodoCursando = paramBecaTrabajador.periodoCursando;
      return;
    case 7:
      if (paramBecaTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.profesion = paramBecaTrabajador.profesion;
      return;
    case 8:
      if (paramBecaTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramBecaTrabajador.trabajador;
      return;
    case 9:
      if (paramBecaTrabajador == null)
        throw new IllegalArgumentException("arg1");
      this.unidad = paramBecaTrabajador.unidad;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof BecaTrabajador))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    BecaTrabajador localBecaTrabajador = (BecaTrabajador)paramObject;
    if (localBecaTrabajador.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localBecaTrabajador, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new BecaTrabajadorPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new BecaTrabajadorPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BecaTrabajadorPK))
      throw new IllegalArgumentException("arg1");
    BecaTrabajadorPK localBecaTrabajadorPK = (BecaTrabajadorPK)paramObject;
    localBecaTrabajadorPK.idBecaTrabajador = this.idBecaTrabajador;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof BecaTrabajadorPK))
      throw new IllegalArgumentException("arg1");
    BecaTrabajadorPK localBecaTrabajadorPK = (BecaTrabajadorPK)paramObject;
    this.idBecaTrabajador = localBecaTrabajadorPK.idBecaTrabajador;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BecaTrabajadorPK))
      throw new IllegalArgumentException("arg2");
    BecaTrabajadorPK localBecaTrabajadorPK = (BecaTrabajadorPK)paramObject;
    localBecaTrabajadorPK.idBecaTrabajador = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 3);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof BecaTrabajadorPK))
      throw new IllegalArgumentException("arg2");
    BecaTrabajadorPK localBecaTrabajadorPK = (BecaTrabajadorPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 3, localBecaTrabajadorPK.idBecaTrabajador);
  }

  private static final String jdoGetestatusBeca(BecaTrabajador paramBecaTrabajador)
  {
    if (paramBecaTrabajador.jdoFlags <= 0)
      return paramBecaTrabajador.estatusBeca;
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramBecaTrabajador.estatusBeca;
    if (localStateManager.isLoaded(paramBecaTrabajador, jdoInheritedFieldCount + 0))
      return paramBecaTrabajador.estatusBeca;
    return localStateManager.getStringField(paramBecaTrabajador, jdoInheritedFieldCount + 0, paramBecaTrabajador.estatusBeca);
  }

  private static final void jdoSetestatusBeca(BecaTrabajador paramBecaTrabajador, String paramString)
  {
    if (paramBecaTrabajador.jdoFlags == 0)
    {
      paramBecaTrabajador.estatusBeca = paramString;
      return;
    }
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaTrabajador.estatusBeca = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaTrabajador, jdoInheritedFieldCount + 0, paramBecaTrabajador.estatusBeca, paramString);
  }

  private static final Date jdoGetfechaFin(BecaTrabajador paramBecaTrabajador)
  {
    if (paramBecaTrabajador.jdoFlags <= 0)
      return paramBecaTrabajador.fechaFin;
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramBecaTrabajador.fechaFin;
    if (localStateManager.isLoaded(paramBecaTrabajador, jdoInheritedFieldCount + 1))
      return paramBecaTrabajador.fechaFin;
    return (Date)localStateManager.getObjectField(paramBecaTrabajador, jdoInheritedFieldCount + 1, paramBecaTrabajador.fechaFin);
  }

  private static final void jdoSetfechaFin(BecaTrabajador paramBecaTrabajador, Date paramDate)
  {
    if (paramBecaTrabajador.jdoFlags == 0)
    {
      paramBecaTrabajador.fechaFin = paramDate;
      return;
    }
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaTrabajador.fechaFin = paramDate;
      return;
    }
    localStateManager.setObjectField(paramBecaTrabajador, jdoInheritedFieldCount + 1, paramBecaTrabajador.fechaFin, paramDate);
  }

  private static final Date jdoGetfechaInicio(BecaTrabajador paramBecaTrabajador)
  {
    if (paramBecaTrabajador.jdoFlags <= 0)
      return paramBecaTrabajador.fechaInicio;
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramBecaTrabajador.fechaInicio;
    if (localStateManager.isLoaded(paramBecaTrabajador, jdoInheritedFieldCount + 2))
      return paramBecaTrabajador.fechaInicio;
    return (Date)localStateManager.getObjectField(paramBecaTrabajador, jdoInheritedFieldCount + 2, paramBecaTrabajador.fechaInicio);
  }

  private static final void jdoSetfechaInicio(BecaTrabajador paramBecaTrabajador, Date paramDate)
  {
    if (paramBecaTrabajador.jdoFlags == 0)
    {
      paramBecaTrabajador.fechaInicio = paramDate;
      return;
    }
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaTrabajador.fechaInicio = paramDate;
      return;
    }
    localStateManager.setObjectField(paramBecaTrabajador, jdoInheritedFieldCount + 2, paramBecaTrabajador.fechaInicio, paramDate);
  }

  private static final long jdoGetidBecaTrabajador(BecaTrabajador paramBecaTrabajador)
  {
    return paramBecaTrabajador.idBecaTrabajador;
  }

  private static final void jdoSetidBecaTrabajador(BecaTrabajador paramBecaTrabajador, long paramLong)
  {
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaTrabajador.idBecaTrabajador = paramLong;
      return;
    }
    localStateManager.setLongField(paramBecaTrabajador, jdoInheritedFieldCount + 3, paramBecaTrabajador.idBecaTrabajador, paramLong);
  }

  private static final double jdoGetmontoBeca(BecaTrabajador paramBecaTrabajador)
  {
    if (paramBecaTrabajador.jdoFlags <= 0)
      return paramBecaTrabajador.montoBeca;
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramBecaTrabajador.montoBeca;
    if (localStateManager.isLoaded(paramBecaTrabajador, jdoInheritedFieldCount + 4))
      return paramBecaTrabajador.montoBeca;
    return localStateManager.getDoubleField(paramBecaTrabajador, jdoInheritedFieldCount + 4, paramBecaTrabajador.montoBeca);
  }

  private static final void jdoSetmontoBeca(BecaTrabajador paramBecaTrabajador, double paramDouble)
  {
    if (paramBecaTrabajador.jdoFlags == 0)
    {
      paramBecaTrabajador.montoBeca = paramDouble;
      return;
    }
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaTrabajador.montoBeca = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramBecaTrabajador, jdoInheritedFieldCount + 4, paramBecaTrabajador.montoBeca, paramDouble);
  }

  private static final NivelBeca jdoGetnivelBeca(BecaTrabajador paramBecaTrabajador)
  {
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramBecaTrabajador.nivelBeca;
    if (localStateManager.isLoaded(paramBecaTrabajador, jdoInheritedFieldCount + 5))
      return paramBecaTrabajador.nivelBeca;
    return (NivelBeca)localStateManager.getObjectField(paramBecaTrabajador, jdoInheritedFieldCount + 5, paramBecaTrabajador.nivelBeca);
  }

  private static final void jdoSetnivelBeca(BecaTrabajador paramBecaTrabajador, NivelBeca paramNivelBeca)
  {
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaTrabajador.nivelBeca = paramNivelBeca;
      return;
    }
    localStateManager.setObjectField(paramBecaTrabajador, jdoInheritedFieldCount + 5, paramBecaTrabajador.nivelBeca, paramNivelBeca);
  }

  private static final String jdoGetperiodoCursando(BecaTrabajador paramBecaTrabajador)
  {
    if (paramBecaTrabajador.jdoFlags <= 0)
      return paramBecaTrabajador.periodoCursando;
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramBecaTrabajador.periodoCursando;
    if (localStateManager.isLoaded(paramBecaTrabajador, jdoInheritedFieldCount + 6))
      return paramBecaTrabajador.periodoCursando;
    return localStateManager.getStringField(paramBecaTrabajador, jdoInheritedFieldCount + 6, paramBecaTrabajador.periodoCursando);
  }

  private static final void jdoSetperiodoCursando(BecaTrabajador paramBecaTrabajador, String paramString)
  {
    if (paramBecaTrabajador.jdoFlags == 0)
    {
      paramBecaTrabajador.periodoCursando = paramString;
      return;
    }
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaTrabajador.periodoCursando = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaTrabajador, jdoInheritedFieldCount + 6, paramBecaTrabajador.periodoCursando, paramString);
  }

  private static final Profesion jdoGetprofesion(BecaTrabajador paramBecaTrabajador)
  {
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramBecaTrabajador.profesion;
    if (localStateManager.isLoaded(paramBecaTrabajador, jdoInheritedFieldCount + 7))
      return paramBecaTrabajador.profesion;
    return (Profesion)localStateManager.getObjectField(paramBecaTrabajador, jdoInheritedFieldCount + 7, paramBecaTrabajador.profesion);
  }

  private static final void jdoSetprofesion(BecaTrabajador paramBecaTrabajador, Profesion paramProfesion)
  {
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaTrabajador.profesion = paramProfesion;
      return;
    }
    localStateManager.setObjectField(paramBecaTrabajador, jdoInheritedFieldCount + 7, paramBecaTrabajador.profesion, paramProfesion);
  }

  private static final Trabajador jdoGettrabajador(BecaTrabajador paramBecaTrabajador)
  {
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramBecaTrabajador.trabajador;
    if (localStateManager.isLoaded(paramBecaTrabajador, jdoInheritedFieldCount + 8))
      return paramBecaTrabajador.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramBecaTrabajador, jdoInheritedFieldCount + 8, paramBecaTrabajador.trabajador);
  }

  private static final void jdoSettrabajador(BecaTrabajador paramBecaTrabajador, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaTrabajador.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramBecaTrabajador, jdoInheritedFieldCount + 8, paramBecaTrabajador.trabajador, paramTrabajador);
  }

  private static final String jdoGetunidad(BecaTrabajador paramBecaTrabajador)
  {
    if (paramBecaTrabajador.jdoFlags <= 0)
      return paramBecaTrabajador.unidad;
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
      return paramBecaTrabajador.unidad;
    if (localStateManager.isLoaded(paramBecaTrabajador, jdoInheritedFieldCount + 9))
      return paramBecaTrabajador.unidad;
    return localStateManager.getStringField(paramBecaTrabajador, jdoInheritedFieldCount + 9, paramBecaTrabajador.unidad);
  }

  private static final void jdoSetunidad(BecaTrabajador paramBecaTrabajador, String paramString)
  {
    if (paramBecaTrabajador.jdoFlags == 0)
    {
      paramBecaTrabajador.unidad = paramString;
      return;
    }
    StateManager localStateManager = paramBecaTrabajador.jdoStateManager;
    if (localStateManager == null)
    {
      paramBecaTrabajador.unidad = paramString;
      return;
    }
    localStateManager.setStringField(paramBecaTrabajador, jdoInheritedFieldCount + 9, paramBecaTrabajador.unidad, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}