package sigefirrhh.bienestar.beca;

import java.io.Serializable;

public class BecaTrabajadorPK
  implements Serializable
{
  public long idBecaTrabajador;

  public BecaTrabajadorPK()
  {
  }

  public BecaTrabajadorPK(long idBecaTrabajador)
  {
    this.idBecaTrabajador = idBecaTrabajador;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((BecaTrabajadorPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(BecaTrabajadorPK thatPK)
  {
    return 
      this.idBecaTrabajador == thatPK.idBecaTrabajador;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idBecaTrabajador)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idBecaTrabajador);
  }
}