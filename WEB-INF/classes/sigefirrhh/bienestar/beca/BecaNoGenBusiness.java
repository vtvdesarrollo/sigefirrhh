package sigefirrhh.bienestar.beca;

import eforserver.common.Resource;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import eforserver.sequence.IdentityGenerator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.log4j.Logger;
import sigefirrhh.base.bienestar.NivelBeca;
import sigefirrhh.base.bienestar.ParametroBeca;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;

public class BecaNoGenBusiness extends BecaBusiness
{
  Logger log = Logger.getLogger(BecaNoGenBusiness.class.getName());

  private TxnManager txn = TxnManagerFactory.makeTransactionManager();

  public boolean aprobarBecaFamiliar(long idTipoPersonal) {
    Connection connection = null;
    PreparedStatement sParametroBeca = null;
    ResultSet rsParametroBeca = null;
    PreparedStatement sBecaFamiliar = null;
    ResultSet rsBecaFamiliar = null;
    Statement sActualizarBecaFamiliar = null;
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      ParametroBeca parametroBeca = null;
      NivelBeca nivelBeca = null;

      StringBuffer sql = new StringBuffer();
      sql = new StringBuffer();
      sql.append("SELECT * FROM parametrobeca, nivelbeca WHERE ")
        .append("parametrobeca.id_nivel_beca = nivelbeca.id_nivel_beca ")
        .append("AND parametrobeca.id_tipo_personal = ?");
      sParametroBeca = 
        connection.prepareStatement(
        sql.toString(), 
        1004, 
        1007);
      sParametroBeca.setLong(1, idTipoPersonal);
      rsParametroBeca = sParametroBeca.executeQuery();

      Collection colParametroBeca = parseParametroBeca(rsParametroBeca);

      sql = new StringBuffer();
      sql.append("SELECT bf.id_beca_familiar, bf.nivel_educativo, bf.grado, f.fecha_nacimiento, ")
        .append("t.anio_ingreso, t.sueldo_basico, bf.promedio_notas, f.fecha_nacimiento, f.nino_excepcional ")
        .append("FROM becafamiliar bf, familiar f, personal p, trabajador t WHERE ")
        .append("bf.id_familiar = f.id_familiar AND ")
        .append("bf.id_personal = p.id_personal AND ")
        .append("t.id_personal = p.id_personal AND ")
        .append("bf.estatus_beca = 'S' AND ")
        .append("bf.trajo_recaudos = 'S' AND ")
        .append("t.id_tipo_personal = ?");
      this.log.error("ID " + idTipoPersonal);
      this.log.error(sql.toString());
      sBecaFamiliar = 
        connection.prepareStatement(
        sql.toString(), 
        1004, 
        1007);
      sBecaFamiliar.setLong(1, idTipoPersonal);
      rsBecaFamiliar = sBecaFamiliar.executeQuery();

      sActualizarBecaFamiliar = connection.createStatement();
      this.log.error("Paso 0");
      if (rsBecaFamiliar != null) {
        this.log.error("Paso 0.1");
        while (rsBecaFamiliar.next()) {
          parametroBeca = verificarParametroBeca(rsBecaFamiliar, colParametroBeca);
          sql = new StringBuffer();
          this.log.error("Paso 0.2");
          if (parametroBeca != null) {
            this.log.error("Paso 0.3");
            sql.append("update becafamiliar set estatus_beca = 'A', monto_beca = ")
              .append(parametroBeca.getMontoBeca())
              .append(", id_concepto_tipo_personal = ")
              .append(parametroBeca.getConceptoTipoPersonal().getIdConceptoTipoPersonal())
              .append(" where id_beca_familiar = ")
              .append(rsBecaFamiliar.getLong("id_beca_familiar"));
          } else {
            this.log.error("Paso 0.4");
            sql.append("update becafamiliar set estatus_beca = 'R'")
              .append(" where id_beca_familiar = ")
              .append(rsBecaFamiliar.getLong("id_beca_familiar"));
          }
          if (rsBecaFamiliar.getString("nino_excepcional").equals("S")) {
            sql = new StringBuffer();
            sql.append("update becafamiliar set estatus_beca = 'A', monto_beca = ")
              .append(parametroBeca.getMontoBeca())
              .append(", id_concepto_tipo_personal = ")
              .append(parametroBeca.getConceptoTipoPersonal().getIdConceptoTipoPersonal())
              .append(" where id_beca_familiar = ")
              .append(rsBecaFamiliar.getLong("id_beca_familiar"));
          }
          this.log.error("Paso 0.5");
          sActualizarBecaFamiliar.addBatch(sql.toString());
        }
        sActualizarBecaFamiliar.executeBatch();
      }
      connection.commit();
      return true;
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    } finally {
      if (rsParametroBeca != null) try { rsParametroBeca.close(); } catch (Exception localException13) {
        } if (sParametroBeca != null) try { sParametroBeca.close(); } catch (Exception localException14) {
        } if (rsBecaFamiliar != null) try { rsBecaFamiliar.close(); } catch (Exception localException15) {
        } if (sBecaFamiliar != null) try { sBecaFamiliar.close(); } catch (Exception localException16) {
        } if (sActualizarBecaFamiliar != null) try { sActualizarBecaFamiliar.close(); } catch (Exception localException17) {
        } if (connection != null) try { connection.close(); connection = null; } catch (Exception localException18)
        {
        } 
    }
  }

  private ParametroBeca verificarParametroBeca(ResultSet rsBecaFamiliar, Collection colParametroBeca)
    throws SQLException
  {
    this.log.error("Paso 1");

    BecaFamiliar becaFamiliar = null;

    String nivelEducativo = rsBecaFamiliar.getString("nivel_educativo");
    int grado = rsBecaFamiliar.getInt("grado");
    int anios_servicio_trabajador = 0;
    int edad_familiar = 0;
    int promedio_notas = 0;
    double sueldo_trabajador = 0.0D;

    Iterator iteParametroBeca = colParametroBeca.iterator();
    ParametroBeca parametroBeca = null;
    this.log.error("Paso 2");
    while (iteParametroBeca.hasNext())
    {
      this.log.error("Paso 3");
      parametroBeca = (ParametroBeca)iteParametroBeca.next();

      anios_servicio_trabajador = Calendar.getInstance().get(1) - rsBecaFamiliar.getInt("anio_ingreso");

      Calendar fechaNacimiento = Calendar.getInstance();
      fechaNacimiento.setTime(rsBecaFamiliar.getDate("fecha_nacimiento"));
      edad_familiar = Calendar.getInstance().get(1) - fechaNacimiento.get(1);
      promedio_notas = rsBecaFamiliar.getInt("promedio_notas");

      sueldo_trabajador = rsBecaFamiliar.getDouble("sueldo_basico");

      this.log.error("Paso 4");

      if ((parametroBeca.getNivelbeca().getNivelEducativo().equals(nivelEducativo)) && 
        (parametroBeca.getAniosServicio() <= anios_servicio_trabajador) && 
        (parametroBeca.getEdadMaxima() >= edad_familiar) && 
        (parametroBeca.getSueldoMaximo() >= sueldo_trabajador) && 
        (parametroBeca.getPromedioNotas() <= promedio_notas)) {
        this.log.error("Paso 5");
        return parametroBeca;
      }

    }

    return null;
  }

  private Collection parseParametroBeca(ResultSet rsParametroBeca) throws SQLException
  {
    ParametroBeca parametroBeca = null;
    NivelBeca nivelBeca = null;
    ConceptoTipoPersonal conceptoTipoPersonal = null;
    Collection colParametroBeca = new ArrayList();

    String ID_NIVEL_BECA = "id_nivel_beca";
    String INICIO_NIVEL = "inicio_nivel";
    String FIN_NIVEL = "fin_nivel";
    String NIVEL_EDUCATIVO = "nivel_educativo";
    String PROXIMO_NIVEL = "proximo_nivel";
    String ID_PARAMETRO_BECA = "id_parametro_beca";
    String ID_CONCEPTO_TIPO_PERSONAL = "id_concepto_tipo_personal";
    String ANIOS_SERVICIO = "anios_servicio";
    String EDAD_MAXIMA = "edad_maxima";
    String MAXIMO_TRABAJADOR = "maximo_trabajador";
    String MESES_SERVICIO = "meses_servicio";
    String MONTO_BECA = "monto_beca";
    String MONTO_EXCEPCIONAL = "monto_excepcional";
    String NUMERO_BECAS = "numero_becas";
    String PROMEDIO_NOTAS = "promedio_notas";
    String SUELDO = "sueldo";
    String SUELDO_MAXIMO = "sueldo_maximo";

    if (rsParametroBeca != null) {
      while (rsParametroBeca.next()) {
        nivelBeca = new NivelBeca();
        parametroBeca = new ParametroBeca();
        conceptoTipoPersonal = new ConceptoTipoPersonal();
        conceptoTipoPersonal.setIdConceptoTipoPersonal(rsParametroBeca.getLong("id_concepto_tipo_personal"));
        nivelBeca.setIdNivelBeca(rsParametroBeca.getLong("id_nivel_beca"));
        nivelBeca.setInicioNivel(rsParametroBeca.getInt("inicio_nivel"));
        nivelBeca.setFinNivel(rsParametroBeca.getInt("fin_nivel"));
        nivelBeca.setNivelEducativo(rsParametroBeca.getString("nivel_educativo"));
        nivelBeca.setProximoNivel(rsParametroBeca.getString("proximo_nivel"));

        parametroBeca.setIdParametroBeca(rsParametroBeca.getLong("id_parametro_beca"));
        parametroBeca.setAniosServicio(rsParametroBeca.getInt("anios_servicio"));
        parametroBeca.setEdadMaxima(rsParametroBeca.getInt("edad_maxima"));
        parametroBeca.setMaximoTrabajador(rsParametroBeca.getInt("maximo_trabajador"));
        parametroBeca.setMesesServicio(rsParametroBeca.getInt("meses_servicio"));
        parametroBeca.setMontoBeca(rsParametroBeca.getDouble("monto_beca"));
        parametroBeca.setMontoExcepcional(rsParametroBeca.getDouble("monto_excepcional"));
        parametroBeca.setNivelbeca(nivelBeca);
        parametroBeca.setNumeroBecas(rsParametroBeca.getInt("numero_becas"));
        parametroBeca.setPromedioNotas(rsParametroBeca.getInt("promedio_notas"));
        parametroBeca.setSueldo(rsParametroBeca.getString("sueldo"));
        parametroBeca.setSueldoMaximo(rsParametroBeca.getDouble("sueldo_maximo"));
        parametroBeca.setConceptoTipoPersonal(conceptoTipoPersonal);

        colParametroBeca.add(parametroBeca);
      }
    }

    return colParametroBeca;
  }

  public boolean pagarBecaFamiliar(long idTipoPersonal, String tipoConcepto) {
    Connection connection = null;
    PreparedStatement sParametroBeca = null;
    ResultSet rsParametroBeca = null;
    PreparedStatement sBecaFamiliar = null;
    ResultSet rsBecaFamiliar = null;
    Statement sActualizarBecaFamiliar = null;
    Map trabajadores = new HashMap();
    AuxCantidad auxCantidad = null;
    try {
      connection = Resource.getConnection();
      connection.setAutoCommit(false);
      ParametroBeca parametroBeca = null;
      NivelBeca nivelBeca = null;

      Collection colParametroBeca = parseParametroBeca(rsParametroBeca);

      StringBuffer sql = new StringBuffer();
      sql.append("SELECT bf.id_beca_familiar, bf.nivel_educativo, bf.grado, f.fecha_nacimiento, ")
        .append("t.anio_ingreso, t.sueldo_basico, bf.promedio_notas, f.fecha_nacimiento, bf.monto_beca, ")
        .append("t.id_trabajador, bf.id_concepto_tipo_personal, ctp.id_frecuencia_tipo_personal ")
        .append("FROM becafamiliar bf, familiar f, personal p, trabajador t, conceptotipopersonal ctp WHERE ")
        .append("bf.id_familiar = f.id_familiar AND ")
        .append("bf.id_personal = p.id_personal AND ")
        .append("bf.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal AND ")
        .append("bf.estatus_beca = 'A' AND ")
        .append("bf.pagada = 'N' AND ")
        .append("t.id_personal = p.id_personal AND ")
        .append("t.id_tipo_personal = ? ORDER BY t.id_trabajador");
      this.log.error("ID " + idTipoPersonal);
      this.log.error(sql.toString());
      sBecaFamiliar = 
        connection.prepareStatement(
        sql.toString(), 
        1004, 
        1007);
      sBecaFamiliar.setLong(1, idTipoPersonal);
      rsBecaFamiliar = sBecaFamiliar.executeQuery();

      sActualizarBecaFamiliar = connection.createStatement();
      this.log.error("Paso 0");
      int cantidad = 0;
      long idConceptoVariable = 0L;
      long idTrabajador = 0L;
      if (rsBecaFamiliar != null) {
        this.log.error("Paso 0.1");
        while (rsBecaFamiliar.next()) {
          this.log.error("Paso 0.3");

          if (idTrabajador != rsBecaFamiliar.getLong("id_trabajador")) {
            if (idTrabajador != 0L) {
              sActualizarBecaFamiliar.addBatch(sql.toString());
            }
            this.log.error("Paso 0.4");
            sql = new StringBuffer();
            sql.append("update becafamiliar set pagada = 'S' ")
              .append(" where id_beca_familiar = ")
              .append(rsBecaFamiliar.getLong("id_beca_familiar"));
            sActualizarBecaFamiliar.addBatch(sql.toString());

            trabajadores.clear();
          }

          idTrabajador = rsBecaFamiliar.getLong("id_trabajador");

          if (trabajadores.containsKey(
            new Long(rsBecaFamiliar.getLong("id_concepto_tipo_personal")))) {
            auxCantidad = (AuxCantidad)trabajadores.get(new Long(rsBecaFamiliar.getLong("id_concepto_tipo_personal")));
            auxCantidad.put();
            auxCantidad.addMonto(rsBecaFamiliar.getLong("monto_beca"));
          } else {
            auxCantidad = new AuxCantidad(1, rsBecaFamiliar.getLong("monto_beca"), null);
            trabajadores.put(
              new Long(rsBecaFamiliar.getLong("id_concepto_tipo_personal")), 
              auxCantidad);
          }
          if (tipoConcepto.equals("V")) {
            IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
            idConceptoVariable = identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoVariable");
            sql = new StringBuffer();
            sql.append("insert into conceptovariable (id_concepto_variable, unidades, monto, fecha_registro, documento_soporte, estatus, id_concepto_tipo_personal, id_frecuencia_tipo_personal, id_trabajador) ")
              .append("values(")
              .append(idConceptoVariable)
              .append(",")
              .append(auxCantidad.cantidad)
              .append(",")
              .append(auxCantidad.monto)
              .append(", '")
              .append(new Date())
              .append("', '', 'A', ")
              .append(rsBecaFamiliar.getLong("id_concepto_tipo_personal"))
              .append(", ")
              .append(rsBecaFamiliar.getLong("id_frecuencia_tipo_personal"))
              .append(", ")
              .append(rsBecaFamiliar.getLong("id_trabajador"))
              .append(")");
            this.log.error("Paso 0.5");
          } else {
            IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
            idConceptoVariable = identityGenerator.getNextSequenceNumber("sigefirrhh.personal.trabajador.ConceptoFijo");
            sql = new StringBuffer();
            sql.append("insert into conceptofijo (id_concepto_fijo, unidades, monto, fecha_registro, documento_soporte, estatus, id_concepto_tipo_personal, id_frecuencia_tipo_personal, id_trabajador) ")
              .append("values(")
              .append(idConceptoVariable)
              .append(",")
              .append(auxCantidad.cantidad)
              .append(",")
              .append(auxCantidad.monto)
              .append(", '")
              .append(new Date())
              .append("', '', 'A', ")
              .append(rsBecaFamiliar.getLong("id_concepto_tipo_personal"))
              .append(", ")
              .append(rsBecaFamiliar.getLong("id_frecuencia_tipo_personal"))
              .append(", ")
              .append(rsBecaFamiliar.getLong("id_trabajador"))
              .append(")");
            this.log.error("Paso 0.5");
          }
        }
        if (idTrabajador != 0L) {
          sActualizarBecaFamiliar.addBatch(sql.toString());
        }
        sActualizarBecaFamiliar.executeBatch();
      }
      connection.commit();
      return true;
    } catch (Exception e) {
      this.log.error("Excepcion controlada:", e);
      return false;
    } finally {
      if (rsParametroBeca != null) try { rsParametroBeca.close(); } catch (Exception localException13) {
        } if (sParametroBeca != null) try { sParametroBeca.close(); } catch (Exception localException14) {
        } if (rsBecaFamiliar != null) try { rsBecaFamiliar.close(); } catch (Exception localException15) {
        } if (sBecaFamiliar != null) try { sBecaFamiliar.close(); } catch (Exception localException16) {
        } if (sActualizarBecaFamiliar != null) try { sActualizarBecaFamiliar.close(); } catch (Exception localException17) {
        } if (connection != null) try { connection.close(); connection = null; } catch (Exception localException18) {
        } 
    }
  }

  private class AuxCantidad {
    private int cantidad;
    private double monto;

    private AuxCantidad(int cantidad, double monto) {
      this.cantidad = cantidad;
      this.monto = monto;
    }
    private void put() {
      this.cantidad += 1;
    }
    private void addMonto(double monto) {
      this.monto += monto;
    }

    AuxCantidad(int paramDouble, double arg3, AuxCantidad arg5)
    {
      this(paramDouble, ???);
    }
  }
}