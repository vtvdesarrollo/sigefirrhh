package sigefirrhh.bienestar.beca;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.bienestar.NivelBeca;
import sigefirrhh.base.bienestar.NivelBecaBeanBusiness;
import sigefirrhh.base.personal.Profesion;
import sigefirrhh.base.personal.ProfesionBeanBusiness;
import sigefirrhh.personal.trabajador.Trabajador;
import sigefirrhh.personal.trabajador.TrabajadorBeanBusiness;

public class BecaTrabajadorBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addBecaTrabajador(BecaTrabajador becaTrabajador)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    BecaTrabajador becaTrabajadorNew = 
      (BecaTrabajador)BeanUtils.cloneBean(
      becaTrabajador);

    NivelBecaBeanBusiness nivelBecaBeanBusiness = new NivelBecaBeanBusiness();

    if (becaTrabajadorNew.getNivelBeca() != null) {
      becaTrabajadorNew.setNivelBeca(
        nivelBecaBeanBusiness.findNivelBecaById(
        becaTrabajadorNew.getNivelBeca().getIdNivelBeca()));
    }

    ProfesionBeanBusiness profesionBeanBusiness = new ProfesionBeanBusiness();

    if (becaTrabajadorNew.getProfesion() != null) {
      becaTrabajadorNew.setProfesion(
        profesionBeanBusiness.findProfesionById(
        becaTrabajadorNew.getProfesion().getIdProfesion()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (becaTrabajadorNew.getTrabajador() != null) {
      becaTrabajadorNew.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        becaTrabajadorNew.getTrabajador().getIdTrabajador()));
    }
    pm.makePersistent(becaTrabajadorNew);
  }

  public void updateBecaTrabajador(BecaTrabajador becaTrabajador) throws Exception
  {
    BecaTrabajador becaTrabajadorModify = 
      findBecaTrabajadorById(becaTrabajador.getIdBecaTrabajador());

    NivelBecaBeanBusiness nivelBecaBeanBusiness = new NivelBecaBeanBusiness();

    if (becaTrabajador.getNivelBeca() != null) {
      becaTrabajador.setNivelBeca(
        nivelBecaBeanBusiness.findNivelBecaById(
        becaTrabajador.getNivelBeca().getIdNivelBeca()));
    }

    ProfesionBeanBusiness profesionBeanBusiness = new ProfesionBeanBusiness();

    if (becaTrabajador.getProfesion() != null) {
      becaTrabajador.setProfesion(
        profesionBeanBusiness.findProfesionById(
        becaTrabajador.getProfesion().getIdProfesion()));
    }

    TrabajadorBeanBusiness trabajadorBeanBusiness = new TrabajadorBeanBusiness();

    if (becaTrabajador.getTrabajador() != null) {
      becaTrabajador.setTrabajador(
        trabajadorBeanBusiness.findTrabajadorById(
        becaTrabajador.getTrabajador().getIdTrabajador()));
    }

    BeanUtils.copyProperties(becaTrabajadorModify, becaTrabajador);
  }

  public void deleteBecaTrabajador(BecaTrabajador becaTrabajador) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    BecaTrabajador becaTrabajadorDelete = 
      findBecaTrabajadorById(becaTrabajador.getIdBecaTrabajador());
    pm.deletePersistent(becaTrabajadorDelete);
  }

  public BecaTrabajador findBecaTrabajadorById(long idBecaTrabajador) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idBecaTrabajador == pIdBecaTrabajador";
    Query query = pm.newQuery(BecaTrabajador.class, filter);

    query.declareParameters("long pIdBecaTrabajador");

    parameters.put("pIdBecaTrabajador", new Long(idBecaTrabajador));

    Collection colBecaTrabajador = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colBecaTrabajador.iterator();
    return (BecaTrabajador)iterator.next();
  }

  public Collection findBecaTrabajadorAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent becaTrabajadorExtent = pm.getExtent(
      BecaTrabajador.class, true);
    Query query = pm.newQuery(becaTrabajadorExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }
}