package sigefirrhh.bienestar.beca;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class BecaBusiness extends AbstractBusiness
  implements Serializable
{
  private BecaFamiliarBeanBusiness becaFamiliarBeanBusiness = new BecaFamiliarBeanBusiness();

  private BecaTrabajadorBeanBusiness becaTrabajadorBeanBusiness = new BecaTrabajadorBeanBusiness();

  public void addBecaFamiliar(BecaFamiliar becaFamiliar)
    throws Exception
  {
    this.becaFamiliarBeanBusiness.addBecaFamiliar(becaFamiliar);
  }

  public void updateBecaFamiliar(BecaFamiliar becaFamiliar) throws Exception {
    this.becaFamiliarBeanBusiness.updateBecaFamiliar(becaFamiliar);
  }

  public void deleteBecaFamiliar(BecaFamiliar becaFamiliar) throws Exception {
    this.becaFamiliarBeanBusiness.deleteBecaFamiliar(becaFamiliar);
  }

  public BecaFamiliar findBecaFamiliarById(long becaFamiliarId) throws Exception {
    return this.becaFamiliarBeanBusiness.findBecaFamiliarById(becaFamiliarId);
  }

  public Collection findAllBecaFamiliar() throws Exception {
    return this.becaFamiliarBeanBusiness.findBecaFamiliarAll();
  }

  public Collection findBecaFamiliarByPersonal(long idPersonal)
    throws Exception
  {
    return this.becaFamiliarBeanBusiness.findByPersonal(idPersonal);
  }

  public void addBecaTrabajador(BecaTrabajador becaTrabajador)
    throws Exception
  {
    this.becaTrabajadorBeanBusiness.addBecaTrabajador(becaTrabajador);
  }

  public void updateBecaTrabajador(BecaTrabajador becaTrabajador) throws Exception {
    this.becaTrabajadorBeanBusiness.updateBecaTrabajador(becaTrabajador);
  }

  public void deleteBecaTrabajador(BecaTrabajador becaTrabajador) throws Exception {
    this.becaTrabajadorBeanBusiness.deleteBecaTrabajador(becaTrabajador);
  }

  public BecaTrabajador findBecaTrabajadorById(long becaTrabajadorId) throws Exception {
    return this.becaTrabajadorBeanBusiness.findBecaTrabajadorById(becaTrabajadorId);
  }

  public Collection findAllBecaTrabajador() throws Exception {
    return this.becaTrabajadorBeanBusiness.findBecaTrabajadorAll();
  }
}