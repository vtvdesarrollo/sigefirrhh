package sigefirrhh.bienestar.beca;

import java.io.Serializable;

public class BecaFamiliarPK
  implements Serializable
{
  public long idBecaFamiliar;

  public BecaFamiliarPK()
  {
  }

  public BecaFamiliarPK(long idBecaFamiliar)
  {
    this.idBecaFamiliar = idBecaFamiliar;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((BecaFamiliarPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(BecaFamiliarPK thatPK)
  {
    return 
      this.idBecaFamiliar == thatPK.idBecaFamiliar;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idBecaFamiliar)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idBecaFamiliar);
  }
}