package sigefirrhh.bienestar.beca;

public class BecaNoGenFacade extends BecaFacade
{
  private BecaNoGenBusiness becaNoGenBusiness = new BecaNoGenBusiness();

  public boolean aprobarBecaFamiliar(long idTipoPersonal) {
    return this.becaNoGenBusiness.aprobarBecaFamiliar(idTipoPersonal);
  }
  public boolean pagarBecaFamiliar(long idTipoPersonal, String tipoConcepto) {
    return this.becaNoGenBusiness.pagarBecaFamiliar(idTipoPersonal, tipoConcepto);
  }
}