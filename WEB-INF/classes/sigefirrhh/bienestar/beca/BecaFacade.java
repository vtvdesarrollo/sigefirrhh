package sigefirrhh.bienestar.beca;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class BecaFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private BecaBusiness becaBusiness = new BecaBusiness();

  public void addBecaFamiliar(BecaFamiliar becaFamiliar)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.becaBusiness.addBecaFamiliar(becaFamiliar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateBecaFamiliar(BecaFamiliar becaFamiliar) throws Exception
  {
    try { this.txn.open();
      this.becaBusiness.updateBecaFamiliar(becaFamiliar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteBecaFamiliar(BecaFamiliar becaFamiliar) throws Exception
  {
    try { this.txn.open();
      this.becaBusiness.deleteBecaFamiliar(becaFamiliar);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public BecaFamiliar findBecaFamiliarById(long becaFamiliarId) throws Exception
  {
    try { this.txn.open();
      BecaFamiliar becaFamiliar = 
        this.becaBusiness.findBecaFamiliarById(becaFamiliarId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(becaFamiliar);
      return becaFamiliar;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllBecaFamiliar() throws Exception
  {
    try { this.txn.open();
      return this.becaBusiness.findAllBecaFamiliar();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findBecaFamiliarByPersonal(long idPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.becaBusiness.findBecaFamiliarByPersonal(idPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addBecaTrabajador(BecaTrabajador becaTrabajador)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.becaBusiness.addBecaTrabajador(becaTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateBecaTrabajador(BecaTrabajador becaTrabajador) throws Exception
  {
    try { this.txn.open();
      this.becaBusiness.updateBecaTrabajador(becaTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteBecaTrabajador(BecaTrabajador becaTrabajador) throws Exception
  {
    try { this.txn.open();
      this.becaBusiness.deleteBecaTrabajador(becaTrabajador);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public BecaTrabajador findBecaTrabajadorById(long becaTrabajadorId) throws Exception
  {
    try { this.txn.open();
      BecaTrabajador becaTrabajador = 
        this.becaBusiness.findBecaTrabajadorById(becaTrabajadorId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(becaTrabajador);
      return becaTrabajador;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllBecaTrabajador() throws Exception
  {
    try { this.txn.open();
      return this.becaBusiness.findAllBecaTrabajador();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}