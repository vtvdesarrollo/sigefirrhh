package sigefirrhh.bienestar.beca;

import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class AprobarBecaFamiliarForm
  implements Serializable
{
  static Logger log = Logger.getLogger(AprobarBecaFamiliarForm.class.getName());
  private String selectTipoPersonal;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private BecaNoGenFacade becaNoGenFacade = new BecaNoGenFacade();

  public AprobarBecaFamiliarForm()
  {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh() {
    try {
      this.listTipoPersonal = 
        this.definicionesFacade.findAllTipoPersonal();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String execute() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      boolean estado = this.becaNoGenFacade.aprobarBecaFamiliar(this.idTipoPersonal);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      if (estado)
        context.addMessage("success_process", new FacesMessage("Se aprobaron las becas familiares con éxito"));
      else
        context.addMessage("error_process", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error aprobando las becas", ""));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
    this.idTipoPersonal = Integer.parseInt(string);
  }
}