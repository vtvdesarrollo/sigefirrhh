package sigefirrhh.bienestar.beca;

import eforserver.presentation.ListUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.login.LoginSession;
import sigefirrhh.sistema.RegistrarAuditoria;

public class PagarBecaFamiliarForm
  implements Serializable
{
  static Logger log = Logger.getLogger(PagarBecaFamiliarForm.class.getName());
  private String selectTipoPersonal;
  private long idTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private LoginSession login;
  private BecaNoGenFacade becaNoGenFacade = new BecaNoGenFacade();
  private String tipoConcepto = "V";

  public PagarBecaFamiliarForm()
  {
    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh() {
    try {
      this.listTipoPersonal = 
        this.definicionesFacade.findAllTipoPersonal();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String execute() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      boolean estado = this.becaNoGenFacade.pagarBecaFamiliar(this.idTipoPersonal, this.tipoConcepto);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'P');

      if (estado)
        context.addMessage("success_process", new FacesMessage("Se pagaron las becas familiares con éxito"));
      else
        context.addMessage("error_process", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error aprobando las becas", ""));
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error: " + e, "")); log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }

  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }

  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
    if (string != null)
      this.idTipoPersonal = Long.parseLong(string);
  }

  public String getTipoConcepto()
  {
    return this.tipoConcepto;
  }

  public void setTipoConcepto(String tipoConcepto) {
    this.tipoConcepto = tipoConcepto;
  }
}