package sigefirrhh.bienestar.beca;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.ConceptoTipoPersonal;
import sigefirrhh.base.definiciones.ConceptoTipoPersonalBeanBusiness;
import sigefirrhh.personal.expediente.Familiar;
import sigefirrhh.personal.expediente.FamiliarBeanBusiness;
import sigefirrhh.personal.expediente.Personal;
import sigefirrhh.personal.expediente.PersonalBeanBusiness;

public class BecaFamiliarBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addBecaFamiliar(BecaFamiliar becaFamiliar)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    BecaFamiliar becaFamiliarNew = 
      (BecaFamiliar)BeanUtils.cloneBean(
      becaFamiliar);

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (becaFamiliarNew.getPersonal() != null) {
      becaFamiliarNew.setPersonal(
        personalBeanBusiness.findPersonalById(
        becaFamiliarNew.getPersonal().getIdPersonal()));
    }

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (becaFamiliarNew.getFamiliar() != null) {
      becaFamiliarNew.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        becaFamiliarNew.getFamiliar().getIdFamiliar()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (becaFamiliarNew.getConceptoTipoPersonal() != null) {
      becaFamiliarNew.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        becaFamiliarNew.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }
    pm.makePersistent(becaFamiliarNew);
  }

  public void updateBecaFamiliar(BecaFamiliar becaFamiliar) throws Exception
  {
    BecaFamiliar becaFamiliarModify = 
      findBecaFamiliarById(becaFamiliar.getIdBecaFamiliar());

    PersonalBeanBusiness personalBeanBusiness = new PersonalBeanBusiness();

    if (becaFamiliar.getPersonal() != null) {
      becaFamiliar.setPersonal(
        personalBeanBusiness.findPersonalById(
        becaFamiliar.getPersonal().getIdPersonal()));
    }

    FamiliarBeanBusiness familiarBeanBusiness = new FamiliarBeanBusiness();

    if (becaFamiliar.getFamiliar() != null) {
      becaFamiliar.setFamiliar(
        familiarBeanBusiness.findFamiliarById(
        becaFamiliar.getFamiliar().getIdFamiliar()));
    }

    ConceptoTipoPersonalBeanBusiness conceptoTipoPersonalBeanBusiness = new ConceptoTipoPersonalBeanBusiness();

    if (becaFamiliar.getConceptoTipoPersonal() != null) {
      becaFamiliar.setConceptoTipoPersonal(
        conceptoTipoPersonalBeanBusiness.findConceptoTipoPersonalById(
        becaFamiliar.getConceptoTipoPersonal().getIdConceptoTipoPersonal()));
    }

    BeanUtils.copyProperties(becaFamiliarModify, becaFamiliar);
  }

  public void deleteBecaFamiliar(BecaFamiliar becaFamiliar) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    BecaFamiliar becaFamiliarDelete = 
      findBecaFamiliarById(becaFamiliar.getIdBecaFamiliar());
    pm.deletePersistent(becaFamiliarDelete);
  }

  public BecaFamiliar findBecaFamiliarById(long idBecaFamiliar) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idBecaFamiliar == pIdBecaFamiliar";
    Query query = pm.newQuery(BecaFamiliar.class, filter);

    query.declareParameters("long pIdBecaFamiliar");

    parameters.put("pIdBecaFamiliar", new Long(idBecaFamiliar));

    Collection colBecaFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colBecaFamiliar.iterator();
    return (BecaFamiliar)iterator.next();
  }

  public Collection findBecaFamiliarAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent becaFamiliarExtent = pm.getExtent(
      BecaFamiliar.class, true);
    Query query = pm.newQuery(becaFamiliarExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByPersonal(long idPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "personal.idPersonal == pIdPersonal";

    Query query = pm.newQuery(BecaFamiliar.class, filter);

    query.declareParameters("long pIdPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdPersonal", new Long(idPersonal));

    Collection colBecaFamiliar = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colBecaFamiliar);

    return colBecaFamiliar;
  }
}