package sigefirrhh.sistema;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class UsuarioOrganismoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(UsuarioOrganismoForm.class.getName());
  private UsuarioOrganismo usuarioOrganismo;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SistemaFacade sistemaFacade = new SistemaFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showUsuarioOrganismoByUsuario;
  private boolean showUsuarioOrganismoByOrganismo;
  private String findSelectUsuario;
  private String findSelectOrganismo;
  private Collection findColUsuario;
  private Collection findColOrganismo;
  private Collection colUsuario;
  private Collection colOrganismo;
  private String selectUsuario;
  private String selectOrganismo;
  private Object stateResultUsuarioOrganismoByUsuario = null;

  private Object stateResultUsuarioOrganismoByOrganismo = null;

  public String getFindSelectUsuario()
  {
    return this.findSelectUsuario;
  }
  public void setFindSelectUsuario(String valUsuario) {
    this.findSelectUsuario = valUsuario;
  }

  public Collection getFindColUsuario() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUsuario.iterator();
    Usuario usuario = null;
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      col.add(new SelectItem(
        String.valueOf(usuario.getIdUsuario()), 
        usuario.toString()));
    }
    return col;
  }
  public String getFindSelectOrganismo() {
    return this.findSelectOrganismo;
  }
  public void setFindSelectOrganismo(String valOrganismo) {
    this.findSelectOrganismo = valOrganismo;
  }

  public Collection getFindColOrganismo() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColOrganismo.iterator();
    Organismo organismo = null;
    while (iterator.hasNext()) {
      organismo = (Organismo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(organismo.getIdOrganismo()), 
        organismo.toString()));
    }
    return col;
  }

  public String getSelectUsuario()
  {
    return this.selectUsuario;
  }
  public void setSelectOrganismo(String valOrganismo) {
    Iterator iterator = this.colOrganismo.iterator();
    Organismo organismo = null;
    this.usuarioOrganismo.setOrganismo(null);
    while (iterator.hasNext()) {
      organismo = (Organismo)iterator.next();
      if (String.valueOf(organismo.getIdOrganismo()).equals(
        valOrganismo)) {
        this.usuarioOrganismo.setOrganismo(
          organismo);
        break;
      }
    }
    this.selectOrganismo = valOrganismo;
  }
  public String getSelectOrganismo() {
    return this.selectOrganismo;
  }
  public void setSelectUsuario(String valUsuario) {
    Iterator iterator = this.colUsuario.iterator();
    Usuario usuario = null;
    this.usuarioOrganismo.setUsuario(null);
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      if (String.valueOf(usuario.getIdUsuario()).equals(
        valUsuario)) {
        this.usuarioOrganismo.setUsuario(
          usuario);
        break;
      }
    }
    this.selectUsuario = valUsuario;
  }
  public Collection getResult() {
    return this.result;
  }

  public UsuarioOrganismo getUsuarioOrganismo() {
    if (this.usuarioOrganismo == null) {
      this.usuarioOrganismo = new UsuarioOrganismo();
    }
    return this.usuarioOrganismo;
  }

  public UsuarioOrganismoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColUsuario()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUsuario.iterator();
    Usuario usuario = null;
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      col.add(new SelectItem(
        String.valueOf(usuario.getIdUsuario()), 
        usuario.toString()));
    }
    return col;
  }
  public Collection getColOrganismo() {
    Collection col = new ArrayList();
    Iterator iterator = this.colOrganismo.iterator();
    Organismo organismo = null;
    while (iterator.hasNext()) {
      organismo = (Organismo)iterator.next();
      col.add(new SelectItem(
        String.valueOf(organismo.getIdOrganismo()), 
        organismo.toString()));
    }
    return col;
  }

  public void refresh() {
    try {
      this.findColUsuario = 
        this.sistemaFacade.findAllUsuario();
      this.findColOrganismo = 
        this.estructuraFacade.findAllOrganismo();

      this.colUsuario = 
        this.sistemaFacade.findAllUsuario();

      this.colOrganismo = 
        this.estructuraFacade.findAllOrganismo();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findUsuarioOrganismoByUsuario()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      long idOrganismo = this.login.getOrganismo().getIdOrganismo();
      this.result = 
        this.sistemaFacade.findUsuarioOrganismoByUsuario(Long.valueOf(this.findSelectUsuario).longValue(), idOrganismo);
      this.showUsuarioOrganismoByUsuario = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUsuarioOrganismoByUsuario)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUsuario = null;
    this.findSelectOrganismo = null;

    return null;
  }

  public String findUsuarioOrganismoByOrganismo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sistemaFacade.findUsuarioOrganismoByOrganismo(Long.valueOf(this.findSelectOrganismo).longValue());
      this.showUsuarioOrganismoByOrganismo = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUsuarioOrganismoByOrganismo)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUsuario = null;
    this.findSelectOrganismo = null;

    return null;
  }

  public boolean isShowUsuarioOrganismoByUsuario() {
    return this.showUsuarioOrganismoByUsuario;
  }
  public boolean isShowUsuarioOrganismoByOrganismo() {
    return this.showUsuarioOrganismoByOrganismo;
  }

  public String selectUsuarioOrganismo()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUsuario = null;

    long idUsuarioOrganismo = 
      Long.parseLong((String)requestParameterMap.get("idUsuarioOrganismo"));
    try
    {
      this.usuarioOrganismo = 
        this.sistemaFacade.findUsuarioOrganismoById(
        idUsuarioOrganismo);
      if (this.usuarioOrganismo.getUsuario() != null) {
        this.selectUsuario = 
          String.valueOf(this.usuarioOrganismo.getUsuario().getIdUsuario());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.usuarioOrganismo = null;
    this.showUsuarioOrganismoByUsuario = false;
    this.showUsuarioOrganismoByOrganismo = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sistemaFacade.addUsuarioOrganismo(
          this.usuarioOrganismo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.usuarioOrganismo);

        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sistemaFacade.updateUsuarioOrganismo(
          this.usuarioOrganismo);
        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.usuarioOrganismo);

        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sistemaFacade.deleteUsuarioOrganismo(
        this.usuarioOrganismo);
      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.usuarioOrganismo);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.usuarioOrganismo = new UsuarioOrganismo();

    this.selectUsuario = null;

    this.usuarioOrganismo.setOrganismo(
      this.login.getOrganismo());
    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.usuarioOrganismo.setIdUsuarioOrganismo(identityGenerator.getNextSequenceNumber("sigefirrhh.sistema.UsuarioOrganismo"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.usuarioOrganismo = new UsuarioOrganismo();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}