package sigefirrhh.sistema;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class OpcionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addOpcion(Opcion opcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Opcion opcionNew = 
      (Opcion)BeanUtils.cloneBean(
      opcion);

    pm.makePersistent(opcionNew);
  }

  public void updateOpcion(Opcion opcion) throws Exception
  {
    Opcion opcionModify = 
      findOpcionById(opcion.getIdOpcion());
    BeanUtils.copyProperties(opcionModify, opcion);
  }

  public void deleteOpcion(Opcion opcion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Opcion opcionDelete = 
      findOpcionById(opcion.getIdOpcion());
    pm.deletePersistent(opcionDelete);
  }

  public Opcion findOpcionById(long idOpcion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idOpcion == pIdOpcion";
    Query query = pm.newQuery(Opcion.class, filter);

    query.declareParameters("long pIdOpcion");

    parameters.put("pIdOpcion", new Long(idOpcion));

    Collection colOpcion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colOpcion.iterator();
    return (Opcion)iterator.next();
  }

  public Collection findOpcionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent opcionExtent = pm.getExtent(
      Opcion.class, true);
    Query query = pm.newQuery(opcionExtent);
    query.setOrdering("descripcion ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodigoOpcion(String codigoOpcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codigoOpcion == pCodigoOpcion";

    Query query = pm.newQuery(Opcion.class, filter);

    query.declareParameters("java.lang.String pCodigoOpcion");
    HashMap parameters = new HashMap();

    parameters.put("pCodigoOpcion", new String(codigoOpcion));

    Collection colOpcion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colOpcion);

    return colOpcion;
  }

  public Collection findByDescripcion(String descripcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "descripcion.startsWith(pDescripcion)";

    Query query = pm.newQuery(Opcion.class, filter);
    query.setOrdering("descripcion ascending");

    query.declareParameters("java.lang.String pDescripcion");
    HashMap parameters = new HashMap();

    parameters.put("pDescripcion", new String(descripcion));

    Collection colOpcion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colOpcion);

    return colOpcion;
  }
}