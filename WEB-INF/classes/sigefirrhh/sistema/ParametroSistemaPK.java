package sigefirrhh.sistema;

import java.io.Serializable;

public class ParametroSistemaPK
  implements Serializable
{
  public String nombreParametro;

  public ParametroSistemaPK()
  {
  }

  public ParametroSistemaPK(String nombreParametro)
  {
    this.nombreParametro = nombreParametro;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((ParametroSistemaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(ParametroSistemaPK thatPK)
  {
    return 
      this.nombreParametro.equals(thatPK.nombreParametro);
  }

  public int hashCode()
  {
    return 
      this.nombreParametro
      .hashCode();
  }
  public String toString() {
    return 
      this.nombreParametro;
  }
}