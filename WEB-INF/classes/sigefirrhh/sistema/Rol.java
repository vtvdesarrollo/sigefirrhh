package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Rol
  implements Serializable, PersistenceCapable
{
  private long idRol;
  private String codigoRol;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codigoRol", "idRol", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetcodigoRol(this) + " - " + jdoGetnombre(this);
  }

  public String getCodigoRol()
  {
    return jdoGetcodigoRol(this);
  }

  public long getIdRol()
  {
    return jdoGetidRol(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodigoRol(String string)
  {
    jdoSetcodigoRol(this, string);
  }

  public void setIdRol(long l)
  {
    jdoSetidRol(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.Rol"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Rol());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Rol localRol = new Rol();
    localRol.jdoFlags = 1;
    localRol.jdoStateManager = paramStateManager;
    return localRol;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Rol localRol = new Rol();
    localRol.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRol.jdoFlags = 1;
    localRol.jdoStateManager = paramStateManager;
    return localRol;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoRol);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRol);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoRol = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRol = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Rol paramRol, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRol == null)
        throw new IllegalArgumentException("arg1");
      this.codigoRol = paramRol.codigoRol;
      return;
    case 1:
      if (paramRol == null)
        throw new IllegalArgumentException("arg1");
      this.idRol = paramRol.idRol;
      return;
    case 2:
      if (paramRol == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramRol.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Rol))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Rol localRol = (Rol)paramObject;
    if (localRol.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRol, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RolPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RolPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RolPK))
      throw new IllegalArgumentException("arg1");
    RolPK localRolPK = (RolPK)paramObject;
    localRolPK.idRol = this.idRol;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RolPK))
      throw new IllegalArgumentException("arg1");
    RolPK localRolPK = (RolPK)paramObject;
    this.idRol = localRolPK.idRol;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RolPK))
      throw new IllegalArgumentException("arg2");
    RolPK localRolPK = (RolPK)paramObject;
    localRolPK.idRol = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RolPK))
      throw new IllegalArgumentException("arg2");
    RolPK localRolPK = (RolPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localRolPK.idRol);
  }

  private static final String jdoGetcodigoRol(Rol paramRol)
  {
    if (paramRol.jdoFlags <= 0)
      return paramRol.codigoRol;
    StateManager localStateManager = paramRol.jdoStateManager;
    if (localStateManager == null)
      return paramRol.codigoRol;
    if (localStateManager.isLoaded(paramRol, jdoInheritedFieldCount + 0))
      return paramRol.codigoRol;
    return localStateManager.getStringField(paramRol, jdoInheritedFieldCount + 0, paramRol.codigoRol);
  }

  private static final void jdoSetcodigoRol(Rol paramRol, String paramString)
  {
    if (paramRol.jdoFlags == 0)
    {
      paramRol.codigoRol = paramString;
      return;
    }
    StateManager localStateManager = paramRol.jdoStateManager;
    if (localStateManager == null)
    {
      paramRol.codigoRol = paramString;
      return;
    }
    localStateManager.setStringField(paramRol, jdoInheritedFieldCount + 0, paramRol.codigoRol, paramString);
  }

  private static final long jdoGetidRol(Rol paramRol)
  {
    return paramRol.idRol;
  }

  private static final void jdoSetidRol(Rol paramRol, long paramLong)
  {
    StateManager localStateManager = paramRol.jdoStateManager;
    if (localStateManager == null)
    {
      paramRol.idRol = paramLong;
      return;
    }
    localStateManager.setLongField(paramRol, jdoInheritedFieldCount + 1, paramRol.idRol, paramLong);
  }

  private static final String jdoGetnombre(Rol paramRol)
  {
    if (paramRol.jdoFlags <= 0)
      return paramRol.nombre;
    StateManager localStateManager = paramRol.jdoStateManager;
    if (localStateManager == null)
      return paramRol.nombre;
    if (localStateManager.isLoaded(paramRol, jdoInheritedFieldCount + 2))
      return paramRol.nombre;
    return localStateManager.getStringField(paramRol, jdoInheritedFieldCount + 2, paramRol.nombre);
  }

  private static final void jdoSetnombre(Rol paramRol, String paramString)
  {
    if (paramRol.jdoFlags == 0)
    {
      paramRol.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramRol.jdoStateManager;
    if (localStateManager == null)
    {
      paramRol.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramRol, jdoInheritedFieldCount + 2, paramRol.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}