package sigefirrhh.sistema.sitp;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class SitpFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private SitpBusiness sitpBusiness = new SitpBusiness();

  public void addInhabilitado(Inhabilitado inhabilitado)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sitpBusiness.addInhabilitado(inhabilitado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateInhabilitado(Inhabilitado inhabilitado) throws Exception
  {
    try { this.txn.open();
      this.sitpBusiness.updateInhabilitado(inhabilitado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteInhabilitado(Inhabilitado inhabilitado) throws Exception
  {
    try { this.txn.open();
      this.sitpBusiness.deleteInhabilitado(inhabilitado);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Inhabilitado findInhabilitadoById(long inhabilitadoId) throws Exception
  {
    try { this.txn.open();
      Inhabilitado inhabilitado = 
        this.sitpBusiness.findInhabilitadoById(inhabilitadoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(inhabilitado);
      return inhabilitado;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllInhabilitado() throws Exception
  {
    try { this.txn.open();
      return this.sitpBusiness.findAllInhabilitado();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findInhabilitadoByCedula(int cedula)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sitpBusiness.findInhabilitadoByCedula(cedula);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findInhabilitadoByPrimerApellido(String primerApellido)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sitpBusiness.findInhabilitadoByPrimerApellido(primerApellido);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findInhabilitadoByPrimerNombre(String primerNombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sitpBusiness.findInhabilitadoByPrimerNombre(primerNombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}