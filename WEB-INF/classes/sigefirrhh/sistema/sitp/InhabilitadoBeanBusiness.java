package sigefirrhh.sistema.sitp;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class InhabilitadoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addInhabilitado(Inhabilitado inhabilitado)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Inhabilitado inhabilitadoNew = 
      (Inhabilitado)BeanUtils.cloneBean(
      inhabilitado);

    pm.makePersistent(inhabilitadoNew);
  }

  public void updateInhabilitado(Inhabilitado inhabilitado) throws Exception
  {
    Inhabilitado inhabilitadoModify = 
      findInhabilitadoById(inhabilitado.getIdInhabilitado());

    BeanUtils.copyProperties(inhabilitadoModify, inhabilitado);
  }

  public void deleteInhabilitado(Inhabilitado inhabilitado) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Inhabilitado inhabilitadoDelete = 
      findInhabilitadoById(inhabilitado.getIdInhabilitado());
    pm.deletePersistent(inhabilitadoDelete);
  }

  public Inhabilitado findInhabilitadoById(long idInhabilitado) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idInhabilitado == pIdInhabilitado";
    Query query = pm.newQuery(Inhabilitado.class, filter);

    query.declareParameters("long pIdInhabilitado");

    parameters.put("pIdInhabilitado", new Long(idInhabilitado));

    Collection colInhabilitado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colInhabilitado.iterator();
    return (Inhabilitado)iterator.next();
  }

  public Collection findInhabilitadoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent inhabilitadoExtent = pm.getExtent(
      Inhabilitado.class, true);
    Query query = pm.newQuery(inhabilitadoExtent);
    query.setOrdering("cedula ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCedula(int cedula)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "cedula == pCedula";

    Query query = pm.newQuery(Inhabilitado.class, filter);

    query.declareParameters("int pCedula");
    HashMap parameters = new HashMap();

    parameters.put("pCedula", new Integer(cedula));

    query.setOrdering("cedula ascending");

    Collection colInhabilitado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colInhabilitado);

    return colInhabilitado;
  }

  public Collection findByPrimerApellido(String primerApellido)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "primerApellido.startsWith(pPrimerApellido)";

    Query query = pm.newQuery(Inhabilitado.class, filter);

    query.declareParameters("java.lang.String pPrimerApellido");
    HashMap parameters = new HashMap();

    parameters.put("pPrimerApellido", new String(primerApellido));

    query.setOrdering("cedula ascending");

    Collection colInhabilitado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colInhabilitado);

    return colInhabilitado;
  }

  public Collection findByPrimerNombre(String primerNombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "primerNombre.startsWith(pPrimerNombre)";

    Query query = pm.newQuery(Inhabilitado.class, filter);

    query.declareParameters("java.lang.String pPrimerNombre");
    HashMap parameters = new HashMap();

    parameters.put("pPrimerNombre", new String(primerNombre));

    query.setOrdering("cedula ascending");

    Collection colInhabilitado = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colInhabilitado);

    return colInhabilitado;
  }
}