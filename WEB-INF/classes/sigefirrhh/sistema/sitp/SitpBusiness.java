package sigefirrhh.sistema.sitp;

import eforserver.business.AbstractBusiness;
import java.io.Serializable;
import java.util.Collection;

public class SitpBusiness extends AbstractBusiness
  implements Serializable
{
  private InhabilitadoBeanBusiness inhabilitadoBeanBusiness = new InhabilitadoBeanBusiness();

  public void addInhabilitado(Inhabilitado inhabilitado)
    throws Exception
  {
    this.inhabilitadoBeanBusiness.addInhabilitado(inhabilitado);
  }

  public void updateInhabilitado(Inhabilitado inhabilitado) throws Exception {
    this.inhabilitadoBeanBusiness.updateInhabilitado(inhabilitado);
  }

  public void deleteInhabilitado(Inhabilitado inhabilitado) throws Exception {
    this.inhabilitadoBeanBusiness.deleteInhabilitado(inhabilitado);
  }

  public Inhabilitado findInhabilitadoById(long inhabilitadoId) throws Exception {
    return this.inhabilitadoBeanBusiness.findInhabilitadoById(inhabilitadoId);
  }

  public Collection findAllInhabilitado() throws Exception {
    return this.inhabilitadoBeanBusiness.findInhabilitadoAll();
  }

  public Collection findInhabilitadoByCedula(int cedula)
    throws Exception
  {
    return this.inhabilitadoBeanBusiness.findByCedula(cedula);
  }

  public Collection findInhabilitadoByPrimerApellido(String primerApellido)
    throws Exception
  {
    return this.inhabilitadoBeanBusiness.findByPrimerApellido(primerApellido);
  }

  public Collection findInhabilitadoByPrimerNombre(String primerNombre)
    throws Exception
  {
    return this.inhabilitadoBeanBusiness.findByPrimerNombre(primerNombre);
  }
}