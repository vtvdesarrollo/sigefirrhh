package sigefirrhh.sistema.sitp;

import java.io.Serializable;

public class InhabilitadoPK
  implements Serializable
{
  public long idInhabilitado;

  public InhabilitadoPK()
  {
  }

  public InhabilitadoPK(long idInhabilitado)
  {
    this.idInhabilitado = idInhabilitado;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((InhabilitadoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(InhabilitadoPK thatPK)
  {
    return 
      this.idInhabilitado == thatPK.idInhabilitado;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idInhabilitado)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idInhabilitado);
  }
}