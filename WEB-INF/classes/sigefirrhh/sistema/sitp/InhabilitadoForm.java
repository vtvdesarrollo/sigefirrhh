package sigefirrhh.sistema.sitp;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class InhabilitadoForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(InhabilitadoForm.class.getName());
  private Inhabilitado inhabilitado;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SitpFacade sitpFacade = new SitpFacade();
  private boolean showInhabilitadoByCedula;
  private boolean showInhabilitadoByPrimerApellido;
  private boolean showInhabilitadoByPrimerNombre;
  private int findCedula;
  private String findPrimerApellido;
  private String findPrimerNombre;
  private Object stateResultInhabilitadoByCedula = null;

  private Object stateResultInhabilitadoByPrimerApellido = null;

  private Object stateResultInhabilitadoByPrimerNombre = null;

  public int getFindCedula()
  {
    return this.findCedula;
  }
  public void setFindCedula(int findCedula) {
    this.findCedula = findCedula;
  }
  public String getFindPrimerApellido() {
    return this.findPrimerApellido;
  }
  public void setFindPrimerApellido(String findPrimerApellido) {
    this.findPrimerApellido = findPrimerApellido;
  }
  public String getFindPrimerNombre() {
    return this.findPrimerNombre;
  }
  public void setFindPrimerNombre(String findPrimerNombre) {
    this.findPrimerNombre = findPrimerNombre;
  }

  public Collection getResult()
  {
    return this.result;
  }

  public Inhabilitado getInhabilitado() {
    if (this.inhabilitado == null) {
      this.inhabilitado = new Inhabilitado();
    }
    return this.inhabilitado;
  }

  public InhabilitadoForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public void refresh()
  {
  }

  public String findInhabilitadoByCedula()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sitpFacade.findInhabilitadoByCedula(this.findCedula);
      this.showInhabilitadoByCedula = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showInhabilitadoByCedula)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCedula = 0;
    this.findPrimerApellido = null;
    this.findPrimerNombre = null;

    return null;
  }

  public String findInhabilitadoByPrimerApellido()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sitpFacade.findInhabilitadoByPrimerApellido(this.findPrimerApellido);
      this.showInhabilitadoByPrimerApellido = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showInhabilitadoByPrimerApellido)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCedula = 0;
    this.findPrimerApellido = null;
    this.findPrimerNombre = null;

    return null;
  }

  public String findInhabilitadoByPrimerNombre()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sitpFacade.findInhabilitadoByPrimerNombre(this.findPrimerNombre);
      this.showInhabilitadoByPrimerNombre = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showInhabilitadoByPrimerNombre)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findCedula = 0;
    this.findPrimerApellido = null;
    this.findPrimerNombre = null;

    return null;
  }

  public boolean isShowInhabilitadoByCedula() {
    return this.showInhabilitadoByCedula;
  }
  public boolean isShowInhabilitadoByPrimerApellido() {
    return this.showInhabilitadoByPrimerApellido;
  }
  public boolean isShowInhabilitadoByPrimerNombre() {
    return this.showInhabilitadoByPrimerNombre;
  }

  public String selectInhabilitado()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    long idInhabilitado = 
      Long.parseLong((String)requestParameterMap.get("idInhabilitado"));
    try
    {
      this.inhabilitado = 
        this.sitpFacade.findInhabilitadoById(
        idInhabilitado);
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.inhabilitado = null;
    this.showInhabilitadoByCedula = false;
    this.showInhabilitadoByPrimerApellido = false;
    this.showInhabilitadoByPrimerNombre = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sitpFacade.addInhabilitado(
          this.inhabilitado);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sitpFacade.updateInhabilitado(
          this.inhabilitado);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sitpFacade.deleteInhabilitado(
        this.inhabilitado);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.inhabilitado = new Inhabilitado();

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.inhabilitado.setIdInhabilitado(identityGenerator.getNextSequenceNumber("sigefirrhh.sistema.sitp.Inhabilitado"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.inhabilitado = new Inhabilitado();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}