package sigefirrhh.sistema.sitp;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Inhabilitado
  implements Serializable, PersistenceCapable
{
  private long idInhabilitado;
  private int cedula;
  private String primerApellido;
  private String segundoApellido;
  private String primerNombre;
  private String segundoNombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "cedula", "idInhabilitado", "primerApellido", "primerNombre", "segundoApellido", "segundoNombre" };
  private static final Class[] jdoFieldTypes = { Integer.TYPE, Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public Inhabilitado()
  {
    jdoSetcedula(this, 0);
  }

  public String toString()
  {
    return jdoGetcedula(this) + " - " + 
      jdoGetprimerApellido(this) + ", " + jdoGetprimerNombre(this);
  }

  public int getCedula()
  {
    return jdoGetcedula(this);
  }
  public void setCedula(int cedula) {
    jdoSetcedula(this, cedula);
  }
  public long getIdInhabilitado() {
    return jdoGetidInhabilitado(this);
  }
  public void setIdInhabilitado(long idInhabilitado) {
    jdoSetidInhabilitado(this, idInhabilitado);
  }
  public String getPrimerApellido() {
    return jdoGetprimerApellido(this);
  }
  public void setPrimerApellido(String primerApellido) {
    jdoSetprimerApellido(this, primerApellido);
  }
  public String getPrimerNombre() {
    return jdoGetprimerNombre(this);
  }
  public void setPrimerNombre(String primerNombre) {
    jdoSetprimerNombre(this, primerNombre);
  }
  public String getSegundoApellido() {
    return jdoGetsegundoApellido(this);
  }
  public void setSegundoApellido(String segundoApellido) {
    jdoSetsegundoApellido(this, segundoApellido);
  }
  public String getSegundoNombre() {
    return jdoGetsegundoNombre(this);
  }
  public void setSegundoNombre(String segundoNombre) {
    jdoSetsegundoNombre(this, segundoNombre);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 6;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.sitp.Inhabilitado"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Inhabilitado());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Inhabilitado localInhabilitado = new Inhabilitado();
    localInhabilitado.jdoFlags = 1;
    localInhabilitado.jdoStateManager = paramStateManager;
    return localInhabilitado;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Inhabilitado localInhabilitado = new Inhabilitado();
    localInhabilitado.jdoCopyKeyFieldsFromObjectId(paramObject);
    localInhabilitado.jdoFlags = 1;
    localInhabilitado.jdoStateManager = paramStateManager;
    return localInhabilitado;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idInhabilitado);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerApellido);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.primerNombre);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoApellido);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.segundoNombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idInhabilitado = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.primerNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoApellido = localStateManager.replacingStringField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.segundoNombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Inhabilitado paramInhabilitado, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramInhabilitado == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramInhabilitado.cedula;
      return;
    case 1:
      if (paramInhabilitado == null)
        throw new IllegalArgumentException("arg1");
      this.idInhabilitado = paramInhabilitado.idInhabilitado;
      return;
    case 2:
      if (paramInhabilitado == null)
        throw new IllegalArgumentException("arg1");
      this.primerApellido = paramInhabilitado.primerApellido;
      return;
    case 3:
      if (paramInhabilitado == null)
        throw new IllegalArgumentException("arg1");
      this.primerNombre = paramInhabilitado.primerNombre;
      return;
    case 4:
      if (paramInhabilitado == null)
        throw new IllegalArgumentException("arg1");
      this.segundoApellido = paramInhabilitado.segundoApellido;
      return;
    case 5:
      if (paramInhabilitado == null)
        throw new IllegalArgumentException("arg1");
      this.segundoNombre = paramInhabilitado.segundoNombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Inhabilitado))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Inhabilitado localInhabilitado = (Inhabilitado)paramObject;
    if (localInhabilitado.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localInhabilitado, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new InhabilitadoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new InhabilitadoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof InhabilitadoPK))
      throw new IllegalArgumentException("arg1");
    InhabilitadoPK localInhabilitadoPK = (InhabilitadoPK)paramObject;
    localInhabilitadoPK.idInhabilitado = this.idInhabilitado;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof InhabilitadoPK))
      throw new IllegalArgumentException("arg1");
    InhabilitadoPK localInhabilitadoPK = (InhabilitadoPK)paramObject;
    this.idInhabilitado = localInhabilitadoPK.idInhabilitado;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof InhabilitadoPK))
      throw new IllegalArgumentException("arg2");
    InhabilitadoPK localInhabilitadoPK = (InhabilitadoPK)paramObject;
    localInhabilitadoPK.idInhabilitado = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof InhabilitadoPK))
      throw new IllegalArgumentException("arg2");
    InhabilitadoPK localInhabilitadoPK = (InhabilitadoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localInhabilitadoPK.idInhabilitado);
  }

  private static final int jdoGetcedula(Inhabilitado paramInhabilitado)
  {
    if (paramInhabilitado.jdoFlags <= 0)
      return paramInhabilitado.cedula;
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
      return paramInhabilitado.cedula;
    if (localStateManager.isLoaded(paramInhabilitado, jdoInheritedFieldCount + 0))
      return paramInhabilitado.cedula;
    return localStateManager.getIntField(paramInhabilitado, jdoInheritedFieldCount + 0, paramInhabilitado.cedula);
  }

  private static final void jdoSetcedula(Inhabilitado paramInhabilitado, int paramInt)
  {
    if (paramInhabilitado.jdoFlags == 0)
    {
      paramInhabilitado.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
    {
      paramInhabilitado.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramInhabilitado, jdoInheritedFieldCount + 0, paramInhabilitado.cedula, paramInt);
  }

  private static final long jdoGetidInhabilitado(Inhabilitado paramInhabilitado)
  {
    return paramInhabilitado.idInhabilitado;
  }

  private static final void jdoSetidInhabilitado(Inhabilitado paramInhabilitado, long paramLong)
  {
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
    {
      paramInhabilitado.idInhabilitado = paramLong;
      return;
    }
    localStateManager.setLongField(paramInhabilitado, jdoInheritedFieldCount + 1, paramInhabilitado.idInhabilitado, paramLong);
  }

  private static final String jdoGetprimerApellido(Inhabilitado paramInhabilitado)
  {
    if (paramInhabilitado.jdoFlags <= 0)
      return paramInhabilitado.primerApellido;
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
      return paramInhabilitado.primerApellido;
    if (localStateManager.isLoaded(paramInhabilitado, jdoInheritedFieldCount + 2))
      return paramInhabilitado.primerApellido;
    return localStateManager.getStringField(paramInhabilitado, jdoInheritedFieldCount + 2, paramInhabilitado.primerApellido);
  }

  private static final void jdoSetprimerApellido(Inhabilitado paramInhabilitado, String paramString)
  {
    if (paramInhabilitado.jdoFlags == 0)
    {
      paramInhabilitado.primerApellido = paramString;
      return;
    }
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
    {
      paramInhabilitado.primerApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramInhabilitado, jdoInheritedFieldCount + 2, paramInhabilitado.primerApellido, paramString);
  }

  private static final String jdoGetprimerNombre(Inhabilitado paramInhabilitado)
  {
    if (paramInhabilitado.jdoFlags <= 0)
      return paramInhabilitado.primerNombre;
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
      return paramInhabilitado.primerNombre;
    if (localStateManager.isLoaded(paramInhabilitado, jdoInheritedFieldCount + 3))
      return paramInhabilitado.primerNombre;
    return localStateManager.getStringField(paramInhabilitado, jdoInheritedFieldCount + 3, paramInhabilitado.primerNombre);
  }

  private static final void jdoSetprimerNombre(Inhabilitado paramInhabilitado, String paramString)
  {
    if (paramInhabilitado.jdoFlags == 0)
    {
      paramInhabilitado.primerNombre = paramString;
      return;
    }
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
    {
      paramInhabilitado.primerNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramInhabilitado, jdoInheritedFieldCount + 3, paramInhabilitado.primerNombre, paramString);
  }

  private static final String jdoGetsegundoApellido(Inhabilitado paramInhabilitado)
  {
    if (paramInhabilitado.jdoFlags <= 0)
      return paramInhabilitado.segundoApellido;
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
      return paramInhabilitado.segundoApellido;
    if (localStateManager.isLoaded(paramInhabilitado, jdoInheritedFieldCount + 4))
      return paramInhabilitado.segundoApellido;
    return localStateManager.getStringField(paramInhabilitado, jdoInheritedFieldCount + 4, paramInhabilitado.segundoApellido);
  }

  private static final void jdoSetsegundoApellido(Inhabilitado paramInhabilitado, String paramString)
  {
    if (paramInhabilitado.jdoFlags == 0)
    {
      paramInhabilitado.segundoApellido = paramString;
      return;
    }
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
    {
      paramInhabilitado.segundoApellido = paramString;
      return;
    }
    localStateManager.setStringField(paramInhabilitado, jdoInheritedFieldCount + 4, paramInhabilitado.segundoApellido, paramString);
  }

  private static final String jdoGetsegundoNombre(Inhabilitado paramInhabilitado)
  {
    if (paramInhabilitado.jdoFlags <= 0)
      return paramInhabilitado.segundoNombre;
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
      return paramInhabilitado.segundoNombre;
    if (localStateManager.isLoaded(paramInhabilitado, jdoInheritedFieldCount + 5))
      return paramInhabilitado.segundoNombre;
    return localStateManager.getStringField(paramInhabilitado, jdoInheritedFieldCount + 5, paramInhabilitado.segundoNombre);
  }

  private static final void jdoSetsegundoNombre(Inhabilitado paramInhabilitado, String paramString)
  {
    if (paramInhabilitado.jdoFlags == 0)
    {
      paramInhabilitado.segundoNombre = paramString;
      return;
    }
    StateManager localStateManager = paramInhabilitado.jdoStateManager;
    if (localStateManager == null)
    {
      paramInhabilitado.segundoNombre = paramString;
      return;
    }
    localStateManager.setStringField(paramInhabilitado, jdoInheritedFieldCount + 5, paramInhabilitado.segundoNombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}