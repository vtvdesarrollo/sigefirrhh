package sigefirrhh.sistema;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import eforserver.tools.PasswordTools;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.login.LoginSession;

public class UsuarioForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(UsuarioForm.class.getName());
  private Usuario usuario;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private SistemaFacade sistemaFacade = new SistemaFacade();
  private boolean showUsuarioByUsuario;
  private boolean showUsuarioByApellidos;
  private String findUsuario;
  private String findApellidos;
  private Collection colRegion;
  private String selectRegion;
  private Object stateResultUsuarioByUsuario = null;

  private Object stateResultUsuarioByApellidos = null;

  public String getFindUsuario()
  {
    return this.findUsuario;
  }
  public void setFindUsuario(String findUsuario) {
    this.findUsuario = findUsuario;
  }
  public String getFindApellidos() {
    return this.findApellidos;
  }
  public void setFindApellidos(String findApellidos) {
    this.findApellidos = findApellidos;
  }

  public String getSelectRegion()
  {
    return this.selectRegion;
  }
  public void setSelectRegion(String valRegion) {
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    this.usuario.setRegion(null);
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      if (String.valueOf(region.getIdRegion()).equals(
        valRegion)) {
        this.usuario.setRegion(
          region);
        break;
      }
    }
    this.selectRegion = valRegion;
  }
  public Collection getResult() {
    return this.result;
  }

  public Usuario getUsuario() {
    if (this.usuario == null) {
      this.usuario = new Usuario();
    }
    return this.usuario;
  }

  public UsuarioForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRegion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRegion.iterator();
    Region region = null;
    while (iterator.hasNext()) {
      region = (Region)iterator.next();
      col.add(new SelectItem(
        String.valueOf(region.getIdRegion()), 
        region.toString()));
    }
    return col;
  }

  public Collection getListAdministrador()
  {
    Collection col = new ArrayList();

    Iterator iterEntry = Usuario.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListActivo() {
    Collection col = new ArrayList();

    Iterator iterEntry = Usuario.LISTA_SINO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try
    {
      this.colRegion = 
        this.estructuraFacade.findRegionByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findUsuarioByUsuario()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sistemaFacade.findUsuarioByUsuario(this.findUsuario);
      this.showUsuarioByUsuario = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUsuarioByUsuario)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findUsuario = null;
    this.findApellidos = null;

    return null;
  }

  public String findUsuarioByApellidos()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sistemaFacade.findUsuarioByApellidos(this.findApellidos);
      this.showUsuarioByApellidos = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUsuarioByApellidos)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findUsuario = null;
    this.findApellidos = null;

    return null;
  }

  public boolean isShowUsuarioByUsuario() {
    return this.showUsuarioByUsuario;
  }
  public boolean isShowUsuarioByApellidos() {
    return this.showUsuarioByApellidos;
  }

  public String selectUsuario()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRegion = null;

    long idUsuario = 
      Long.parseLong((String)requestParameterMap.get("idUsuario"));
    try
    {
      this.usuario = 
        this.sistemaFacade.findUsuarioById(
        idUsuario);
      if (this.usuario.getRegion() != null) {
        this.selectRegion = 
          String.valueOf(this.usuario.getRegion().getIdRegion());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.usuario = null;
    this.showUsuarioByUsuario = false;
    this.showUsuarioByApellidos = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;
    try
    {
      int minLen = 6;
      try {
        minLen = this.sistemaFacade.getParametroMinLongitudClave();
      } catch (Exception ex) {
        minLen = 6;
      }
      if (this.usuario.getPassword().length() < minLen) {
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_INFO, "La longitud de la contraseña debe ser por lo menos de " + String.valueOf(minLen) + " caracteres", ""));
        error = true;
      }
    } catch (Exception e) {
      log.error(e.getMessage(), e.getCause());
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_INFO, "Validando longitud de la contraseña.   " + e.getMessage(), e.getMessage()));
      error = true;
    }

    if (error)
      return null;
    try
    {
      this.usuario.setPassword(PasswordTools.getInstance().cifrar(this.usuario.getPassword()));
      if (this.adding) {
        this.usuario.setIntentos(0);
        this.usuario.setCambioPassword(new Date());
        this.sistemaFacade.addUsuario(
          this.usuario);

        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'A', this.usuario);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      }
      else
      {
        log.debug("=-> activo =" + this.usuario.getActivo());
        log.debug("=-> activo =" + String.valueOf(this.usuario.getIntentos()));
        if ((this.usuario.getActivo().equals("S")) && (this.usuario.getIntentos() > 0)) {
          this.usuario.setIntentos(0);
        }
        this.sistemaFacade.updateUsuario(
          this.usuario);

        RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'M', this.usuario);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sistemaFacade.deleteUsuario(
        this.usuario);

      RegistrarAuditoria.registrar(context, this.login.getUsuarioObject(), 'E', this.usuario);

      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.usuario = new Usuario();

    int diasVigenciaUsuario = 95;
    try {
      diasVigenciaUsuario = this.sistemaFacade.getParametroDiasVigenciaUsuario();
    } catch (Exception ex) {
      diasVigenciaUsuario = 95;
    }

    Calendar c = Calendar.getInstance();
    c.add(5, diasVigenciaUsuario);
    this.usuario.setFechaVence(c.getTime());

    this.selectRegion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.usuario.setIdUsuario(identityGenerator.getNextSequenceNumber("sigefirrhh.sistema.Usuario"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.usuario = new Usuario();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}