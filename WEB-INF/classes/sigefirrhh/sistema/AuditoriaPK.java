package sigefirrhh.sistema;

import java.io.Serializable;

public class AuditoriaPK
  implements Serializable
{
  public long idAuditoria;

  public AuditoriaPK()
  {
  }

  public AuditoriaPK(long idAuditoria)
  {
    this.idAuditoria = idAuditoria;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AuditoriaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AuditoriaPK thatPK)
  {
    return 
      this.idAuditoria == thatPK.idAuditoria;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAuditoria)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAuditoria);
  }
}