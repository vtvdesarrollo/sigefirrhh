package sigefirrhh.sistema;

import java.io.Serializable;

public class UsuarioPK
  implements Serializable
{
  public long idUsuario;

  public UsuarioPK()
  {
  }

  public UsuarioPK(long idUsuario)
  {
    this.idUsuario = idUsuario;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UsuarioPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UsuarioPK thatPK)
  {
    return 
      this.idUsuario == thatPK.idUsuario;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUsuario)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUsuario);
  }
}