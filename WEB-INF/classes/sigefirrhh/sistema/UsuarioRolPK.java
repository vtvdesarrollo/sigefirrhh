package sigefirrhh.sistema;

import java.io.Serializable;

public class UsuarioRolPK
  implements Serializable
{
  public long idUsuarioRol;

  public UsuarioRolPK()
  {
  }

  public UsuarioRolPK(long idUsuarioRol)
  {
    this.idUsuarioRol = idUsuarioRol;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UsuarioRolPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UsuarioRolPK thatPK)
  {
    return 
      this.idUsuarioRol == thatPK.idUsuarioRol;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUsuarioRol)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUsuarioRol);
  }
}