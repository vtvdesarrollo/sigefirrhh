package sigefirrhh.sistema;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class UsuarioTipoPersonalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(UsuarioUnidadFuncionalForm.class.getName());
  private UsuarioTipoPersonal usuarioTipoPersonal;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SistemaFacade sistemaFacade = new SistemaFacade();
  private DefinicionesFacade definicionesFacade = new DefinicionesFacade();
  private boolean showUsuarioTipoPersonalByUsuario;
  private boolean showUsuarioTipoPersonalByTipoPersonal;
  private String findSelectUsuario;
  private String findSelectTipoPersonal;
  private Collection findColUsuario;
  private Collection findColTipoPersonal;
  private Collection colUsuario;
  private Collection colTipoPersonal;
  private String selectUsuario;
  private String selectTipoPersonal;
  private Object stateResultUsuarioTipoPersonalByUsuario = null;

  private Object stateResultUsuarioTipoPersonalByTipoPersonal = null;

  public String getFindSelectUsuario()
  {
    return this.findSelectUsuario;
  }
  public void setFindSelectUsuario(String valUsuario) {
    this.findSelectUsuario = valUsuario;
  }

  public Collection getFindColUsuario() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUsuario.iterator();
    Usuario usuario = null;
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      col.add(new SelectItem(
        String.valueOf(usuario.getIdUsuario()), 
        usuario.toString()));
    }
    return col;
  }
  public String getFindSelectTipoPersonal() {
    return this.findSelectTipoPersonal;
  }
  public void setFindSelectTipoPersonal(String valTipoPersonal) {
    this.findSelectTipoPersonal = valTipoPersonal;
  }

  public Collection getFindColTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public String getSelectUsuario()
  {
    return this.selectUsuario;
  }
  public void setSelectUsuario(String valUsuario) {
    Iterator iterator = this.colUsuario.iterator();
    Usuario usuario = null;
    this.usuarioTipoPersonal.setUsuario(null);
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      if (String.valueOf(usuario.getIdUsuario()).equals(
        valUsuario)) {
        this.usuarioTipoPersonal.setUsuario(
          usuario);
        break;
      }
    }
    this.selectUsuario = valUsuario;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String valTipoPersonal) {
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    this.usuarioTipoPersonal.setTipoPersonal(null);
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      if (String.valueOf(tipoPersonal.getIdTipoPersonal()).equals(
        valTipoPersonal)) {
        this.usuarioTipoPersonal.setTipoPersonal(
          tipoPersonal);
        break;
      }
    }
    this.selectTipoPersonal = valTipoPersonal;
  }
  public Collection getResult() {
    return this.result;
  }

  public UsuarioTipoPersonal getUsuarioTipoPersonal() {
    if (this.usuarioTipoPersonal == null) {
      this.usuarioTipoPersonal = new UsuarioTipoPersonal();
    }
    return this.usuarioTipoPersonal;
  }

  public UsuarioTipoPersonalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColUsuario()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUsuario.iterator();
    Usuario usuario = null;
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      col.add(new SelectItem(
        String.valueOf(usuario.getIdUsuario()), 
        usuario.toString()));
    }
    return col;
  }

  public Collection getColTipoPersonal()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colTipoPersonal.iterator();
    TipoPersonal tipoPersonal = null;
    while (iterator.hasNext()) {
      tipoPersonal = (TipoPersonal)iterator.next();
      col.add(new SelectItem(
        String.valueOf(tipoPersonal.getIdTipoPersonal()), 
        tipoPersonal.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColUsuario = 
        this.sistemaFacade.findAllUsuario();
      this.findColTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colUsuario = 
        this.sistemaFacade.findAllUsuario();
      this.colTipoPersonal = 
        this.definicionesFacade.findTipoPersonalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findUsuarioTipoPersonalByUsuario()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sistemaFacade.findUsuarioTipoPersonalByUsuario(Long.valueOf(this.findSelectUsuario).longValue());
      this.showUsuarioTipoPersonalByUsuario = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUsuarioTipoPersonalByUsuario)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUsuario = null;
    this.findSelectTipoPersonal = null;

    return null;
  }

  public String findUsuarioTipoPersonalByTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sistemaFacade.findUsuarioTipoPersonalByTipoPersonal(Long.valueOf(this.findSelectTipoPersonal).longValue());
      this.showUsuarioTipoPersonalByTipoPersonal = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUsuarioTipoPersonalByTipoPersonal)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUsuario = null;
    this.findSelectTipoPersonal = null;

    return null;
  }

  public boolean isShowUsuarioTipoPersonalByUsuario() {
    return this.showUsuarioTipoPersonalByUsuario;
  }
  public boolean isShowUsuarioTipoPersonalByTipoPersonal() {
    return this.showUsuarioTipoPersonalByTipoPersonal;
  }

  public String selectUsuarioTipoPersonal()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUsuario = null;
    this.selectTipoPersonal = null;

    long idUsuarioTipoPersonal = 
      Long.parseLong((String)requestParameterMap.get("idUsuarioTipoPersonal"));
    try
    {
      this.usuarioTipoPersonal = 
        this.sistemaFacade.findUsuarioTipoPersonalById(
        idUsuarioTipoPersonal);
      if (this.usuarioTipoPersonal.getUsuario() != null) {
        this.selectUsuario = 
          String.valueOf(this.usuarioTipoPersonal.getUsuario().getIdUsuario());
      }
      if (this.usuarioTipoPersonal.getTipoPersonal() != null) {
        this.selectTipoPersonal = 
          String.valueOf(this.usuarioTipoPersonal.getTipoPersonal().getIdTipoPersonal());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.usuarioTipoPersonal = null;
    this.showUsuarioTipoPersonalByUsuario = false;
    this.showUsuarioTipoPersonalByTipoPersonal = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sistemaFacade.addUsuarioTipoPersonal(
          this.usuarioTipoPersonal);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sistemaFacade.updateUsuarioTipoPersonal(
          this.usuarioTipoPersonal);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sistemaFacade.deleteUsuarioTipoPersonal(
        this.usuarioTipoPersonal);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.usuarioTipoPersonal = new UsuarioTipoPersonal();

    this.selectUsuario = null;

    this.selectTipoPersonal = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.usuarioTipoPersonal.setIdUsuarioTipoPersonal(identityGenerator.getNextSequenceNumber("sigefirrhh.sistema.UsuarioTipoPersonal"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.usuarioTipoPersonal = new UsuarioTipoPersonal();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}