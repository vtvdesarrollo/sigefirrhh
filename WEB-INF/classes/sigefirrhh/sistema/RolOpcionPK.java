package sigefirrhh.sistema;

import java.io.Serializable;

public class RolOpcionPK
  implements Serializable
{
  public long idRolOpcion;

  public RolOpcionPK()
  {
  }

  public RolOpcionPK(long idRolOpcion)
  {
    this.idRolOpcion = idRolOpcion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RolOpcionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RolOpcionPK thatPK)
  {
    return 
      this.idRolOpcion == thatPK.idRolOpcion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRolOpcion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRolOpcion);
  }
}