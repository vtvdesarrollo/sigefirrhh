package sigefirrhh.sistema;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.OrganismoBeanBusiness;

public class UsuarioOrganismoBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addUsuarioOrganismo(UsuarioOrganismo usuarioOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    UsuarioOrganismo usuarioOrganismoNew = 
      (UsuarioOrganismo)BeanUtils.cloneBean(
      usuarioOrganismo);

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (usuarioOrganismoNew.getUsuario() != null) {
      usuarioOrganismoNew.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        usuarioOrganismoNew.getUsuario().getIdUsuario()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (usuarioOrganismoNew.getOrganismo() != null) {
      usuarioOrganismoNew.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        usuarioOrganismoNew.getOrganismo().getIdOrganismo()));
    }
    pm.makePersistent(usuarioOrganismoNew);
  }

  public void updateUsuarioOrganismo(UsuarioOrganismo usuarioOrganismo) throws Exception
  {
    UsuarioOrganismo usuarioOrganismoModify = 
      findUsuarioOrganismoById(usuarioOrganismo.getIdUsuarioOrganismo());

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (usuarioOrganismo.getUsuario() != null) {
      usuarioOrganismo.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        usuarioOrganismo.getUsuario().getIdUsuario()));
    }

    OrganismoBeanBusiness organismoBeanBusiness = new OrganismoBeanBusiness();

    if (usuarioOrganismo.getOrganismo() != null) {
      usuarioOrganismo.setOrganismo(
        organismoBeanBusiness.findOrganismoById(
        usuarioOrganismo.getOrganismo().getIdOrganismo()));
    }

    BeanUtils.copyProperties(usuarioOrganismoModify, usuarioOrganismo);
  }

  public void deleteUsuarioOrganismo(UsuarioOrganismo usuarioOrganismo) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    UsuarioOrganismo usuarioOrganismoDelete = 
      findUsuarioOrganismoById(usuarioOrganismo.getIdUsuarioOrganismo());
    pm.deletePersistent(usuarioOrganismoDelete);
  }

  public UsuarioOrganismo findUsuarioOrganismoById(long idUsuarioOrganismo) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUsuarioOrganismo == pIdUsuarioOrganismo";
    Query query = pm.newQuery(UsuarioOrganismo.class, filter);

    query.declareParameters("long pIdUsuarioOrganismo");

    parameters.put("pIdUsuarioOrganismo", new Long(idUsuarioOrganismo));

    Collection colUsuarioOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUsuarioOrganismo.iterator();
    return (UsuarioOrganismo)iterator.next();
  }

  public Collection findUsuarioOrganismoAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent usuarioOrganismoExtent = pm.getExtent(
      UsuarioOrganismo.class, true);
    Query query = pm.newQuery(usuarioOrganismoExtent);
    query.setOrdering("usuario.usuario ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUsuario(long idUsuario, long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "usuario.idUsuario == pIdUsuario &&  organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(UsuarioOrganismo.class, filter);

    query.declareParameters("long pIdUsuario, long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdUsuario", new Long(idUsuario));
    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("usuario.usuario ascending");

    Collection colUsuarioOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUsuarioOrganismo);

    return colUsuarioOrganismo;
  }

  public Collection findByOrganismo(long idOrganismo)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "organismo.idOrganismo == pIdOrganismo";

    Query query = pm.newQuery(UsuarioOrganismo.class, filter);

    query.declareParameters("long pIdOrganismo");
    HashMap parameters = new HashMap();

    parameters.put("pIdOrganismo", new Long(idOrganismo));

    query.setOrdering("usuario.usuario ascending");

    Collection colUsuarioOrganismo = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUsuarioOrganismo);

    return colUsuarioOrganismo;
  }
}