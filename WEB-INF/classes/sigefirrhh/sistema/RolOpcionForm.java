package sigefirrhh.sistema;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class RolOpcionForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(RolOpcionForm.class.getName());
  private RolOpcion rolOpcion;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SistemaFacade sistemaFacade = new SistemaFacade();
  private boolean showRolOpcionByRol;
  private String findSelectRol;
  private Collection findColRol;
  private Collection colRol;
  private Collection colOpcion;
  private String selectRol;
  private String selectOpcion;
  private Object stateResultRolOpcionByRol = null;

  public String getFindSelectRol()
  {
    return this.findSelectRol;
  }
  public void setFindSelectRol(String valRol) {
    this.findSelectRol = valRol;
  }

  public Collection getFindColRol() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRol.iterator();
    Rol rol = null;
    while (iterator.hasNext()) {
      rol = (Rol)iterator.next();
      col.add(new SelectItem(
        String.valueOf(rol.getIdRol()), 
        rol.toString()));
    }
    return col;
  }

  public String getSelectRol()
  {
    return this.selectRol;
  }
  public void setSelectRol(String valRol) {
    Iterator iterator = this.colRol.iterator();
    Rol rol = null;
    this.rolOpcion.setRol(null);
    while (iterator.hasNext()) {
      rol = (Rol)iterator.next();
      if (String.valueOf(rol.getIdRol()).equals(
        valRol)) {
        this.rolOpcion.setRol(
          rol);
        break;
      }
    }
    this.selectRol = valRol;
  }
  public String getSelectOpcion() {
    return this.selectOpcion;
  }
  public void setSelectOpcion(String valOpcion) {
    Iterator iterator = this.colOpcion.iterator();
    Opcion opcion = null;
    this.rolOpcion.setOpcion(null);
    while (iterator.hasNext()) {
      opcion = (Opcion)iterator.next();
      if (String.valueOf(opcion.getIdOpcion()).equals(
        valOpcion)) {
        this.rolOpcion.setOpcion(
          opcion);
        break;
      }
    }
    this.selectOpcion = valOpcion;
  }
  public Collection getResult() {
    return this.result;
  }

  public RolOpcion getRolOpcion() {
    if (this.rolOpcion == null) {
      this.rolOpcion = new RolOpcion();
    }
    return this.rolOpcion;
  }

  public RolOpcionForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColRol()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRol.iterator();
    Rol rol = null;
    while (iterator.hasNext()) {
      rol = (Rol)iterator.next();
      col.add(new SelectItem(
        String.valueOf(rol.getIdRol()), 
        rol.toString()));
    }
    return col;
  }

  public Collection getColOpcion()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colOpcion.iterator();
    Opcion opcion = null;
    while (iterator.hasNext()) {
      opcion = (Opcion)iterator.next();
      col.add(new SelectItem(
        String.valueOf(opcion.getIdOpcion()), 
        opcion.toString()));
    }
    return col;
  }

  public Collection getListConsultar() {
    Collection col = new ArrayList();

    Iterator iterEntry = RolOpcion.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListAgregar() {
    Collection col = new ArrayList();

    Iterator iterEntry = RolOpcion.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListModificar() {
    Collection col = new ArrayList();

    Iterator iterEntry = RolOpcion.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEliminar() {
    Collection col = new ArrayList();

    Iterator iterEntry = RolOpcion.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public Collection getListEjecutar() {
    Collection col = new ArrayList();

    Iterator iterEntry = RolOpcion.LISTA_SI_NO.entrySet().iterator();
    Map.Entry entry = null;

    while (iterEntry.hasNext()) {
      entry = (Map.Entry)iterEntry.next();
      col.add(new SelectItem(
        entry.getKey().toString(), entry.getValue().toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColRol = 
        this.sistemaFacade.findAllRol();

      this.colRol = 
        this.sistemaFacade.findAllRol();
      this.colOpcion = 
        this.sistemaFacade.findAllOpcion();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findRolOpcionByRol()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sistemaFacade.findRolOpcionByRol(Long.valueOf(this.findSelectRol).longValue());
      this.showRolOpcionByRol = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showRolOpcionByRol)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectRol = null;

    return null;
  }

  public boolean isShowRolOpcionByRol() {
    return this.showRolOpcionByRol;
  }

  public String selectRolOpcion()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectRol = null;
    this.selectOpcion = null;

    long idRolOpcion = 
      Long.parseLong((String)requestParameterMap.get("idRolOpcion"));
    try
    {
      this.rolOpcion = 
        this.sistemaFacade.findRolOpcionById(
        idRolOpcion);
      if (this.rolOpcion.getRol() != null) {
        this.selectRol = 
          String.valueOf(this.rolOpcion.getRol().getIdRol());
      }
      if (this.rolOpcion.getOpcion() != null) {
        this.selectOpcion = 
          String.valueOf(this.rolOpcion.getOpcion().getIdOpcion());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.rolOpcion = null;
    this.showRolOpcionByRol = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sistemaFacade.addRolOpcion(
          this.rolOpcion);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sistemaFacade.updateRolOpcion(
          this.rolOpcion);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sistemaFacade.deleteRolOpcion(
        this.rolOpcion);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.rolOpcion = new RolOpcion();

    this.selectRol = null;

    this.selectOpcion = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.rolOpcion.setIdRolOpcion(identityGenerator.getNextSequenceNumber("sigefirrhh.sistema.RolOpcion"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.rolOpcion = new RolOpcion();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}