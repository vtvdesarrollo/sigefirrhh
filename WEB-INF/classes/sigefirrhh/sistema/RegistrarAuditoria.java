package sigefirrhh.sistema;

import eforserver.common.Resource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.util.Calendar;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.personal.expediente.Personal;

public class RegistrarAuditoria
{
  static Logger log = Logger.getLogger(RegistrarAuditoria.class.getName());
  public static final char MODIFICAR = 'M';
  public static final char AGREGAR = 'A';
  public static final char ELIMINAR = 'E';
  public static final char PROCESAR = 'P';
  public static final char LISTAR = 'L';
  public static final char ANULAR = 'N';
  public static final char APROBAR = 'R';
  public static final char INGRESAR = 'I';
  public static final char SALIR = 'S';

  public static void registrar(FacesContext context, Usuario usuario, char tipoAccion, Object registro, Personal personal, Concepto concepto, double montoConcepto, FrecuenciaPago frecuenciaPago)
  {
    Connection connection = null;
    PreparedStatement sInsertarAuditoria = null;
    StringBuffer sql = new StringBuffer();

    Calendar tiempo = Calendar.getInstance();
    tiempo.setTime(new java.util.Date());
    Time time = new Time(tiempo.getTimeInMillis());

    java.sql.Date tiempoSql = new java.sql.Date(tiempo.getTime().getYear(), tiempo.getTime().getMonth(), tiempo.getTime().getDate());

    String ruta = context.getExternalContext().getRequestServletPath().substring(1, context.getExternalContext().getRequestServletPath().length() - 4);
    try
    {
      connection = Resource.getConnection();
      connection.setAutoCommit(true);
      sql.append("INSERT INTO auditoria VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
      sInsertarAuditoria = 
        connection.prepareStatement(
        sql.toString(), 
        1004, 
        1007);
      sInsertarAuditoria.setLong(1, usuario.getIdUsuario());
      sInsertarAuditoria.setLong(2, usuario.getIdOrganismo());
      sInsertarAuditoria.setTime(3, time);
      sInsertarAuditoria.setDate(4, tiempoSql);
      sInsertarAuditoria.setString(5, usuario.toString());
      sInsertarAuditoria.setString(6, ruta);
      sInsertarAuditoria.setString(7, Character.toString(tipoAccion));
      sInsertarAuditoria.setString(8, registro != null ? registro.toString() : "");
      sInsertarAuditoria.setInt(9, personal != null ? personal.getCedula() : 0);
      sInsertarAuditoria.setString(10, concepto != null ? concepto.getCodConcepto() : "");
      sInsertarAuditoria.setDouble(11, montoConcepto);
      sInsertarAuditoria.setInt(12, frecuenciaPago != null ? frecuenciaPago.getCodFrecuenciaPago() : 0);

      sInsertarAuditoria.execute();
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);

      if (sInsertarAuditoria != null) try { sInsertarAuditoria.close(); } catch (Exception localException1) {
        } if (connection != null) try { connection.close(); connection = null;
        }
        catch (Exception localException2)
        {
        }
    }
    finally
    {
      if (sInsertarAuditoria != null) try { sInsertarAuditoria.close(); } catch (Exception localException3) {
        } if (connection != null) try { connection.close(); connection = null;
        }
        catch (Exception localException4)
        {
        }
    }
  }

  public static void registrar(FacesContext context, Usuario usuario, char tipoAccion, Object registro, Personal personal, Concepto concepto, double montoConcepto)
  {
    registrar(context, usuario, tipoAccion, registro, personal, concepto, montoConcepto, null);
  }

  public static void registrar(FacesContext context, Usuario usuario, char tipoAccion, Object registro, Personal personal)
  {
    registrar(context, usuario, tipoAccion, registro, personal, null, 0.0D, null);
  }

  public static void registrar(FacesContext context, Usuario usuario, char tipoAccion, Object registro)
  {
    registrar(context, usuario, tipoAccion, registro, null);
  }

  public static void registrar(FacesContext context, Usuario usuario, char tipoAccion)
  {
    registrar(context, usuario, tipoAccion, null);
  }
}