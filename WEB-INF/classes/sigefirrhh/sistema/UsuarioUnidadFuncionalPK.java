package sigefirrhh.sistema;

import java.io.Serializable;

public class UsuarioUnidadFuncionalPK
  implements Serializable
{
  public long idUsuarioUnidadFuncional;

  public UsuarioUnidadFuncionalPK()
  {
  }

  public UsuarioUnidadFuncionalPK(long idUsuarioUnidadFuncional)
  {
    this.idUsuarioUnidadFuncional = idUsuarioUnidadFuncional;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UsuarioUnidadFuncionalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UsuarioUnidadFuncionalPK thatPK)
  {
    return 
      this.idUsuarioUnidadFuncional == thatPK.idUsuarioUnidadFuncional;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUsuarioUnidadFuncional)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUsuarioUnidadFuncional);
  }
}