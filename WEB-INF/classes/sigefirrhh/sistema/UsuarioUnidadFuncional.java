package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.UnidadFuncional;

public class UsuarioUnidadFuncional
  implements Serializable, PersistenceCapable
{
  private long idUsuarioUnidadFuncional;
  private Usuario usuario;
  private UnidadFuncional unidadFuncional;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idUsuarioUnidadFuncional", "unidadFuncional", "usuario" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.UnidadFuncional"), sunjdo$classForName$("sigefirrhh.sistema.Usuario") };
  private static final byte[] jdoFieldFlags = { 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetusuario(this).toString() + " - " + jdoGetunidadFuncional(this).getNombre();
  }

  public long getIdUsuarioUnidadFuncional() {
    return jdoGetidUsuarioUnidadFuncional(this);
  }

  public void setIdUsuarioUnidadFuncional(long idUsuarioUnidadFuncional) {
    jdoSetidUsuarioUnidadFuncional(this, idUsuarioUnidadFuncional);
  }

  public UnidadFuncional getUnidadFuncional() {
    return jdoGetunidadFuncional(this);
  }

  public void setUnidadFuncional(UnidadFuncional unidadFuncional) {
    jdoSetunidadFuncional(this, unidadFuncional);
  }

  public Usuario getUsuario() {
    return jdoGetusuario(this);
  }

  public void setUsuario(Usuario usuario) {
    jdoSetusuario(this, usuario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.UsuarioUnidadFuncional"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UsuarioUnidadFuncional());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UsuarioUnidadFuncional localUsuarioUnidadFuncional = new UsuarioUnidadFuncional();
    localUsuarioUnidadFuncional.jdoFlags = 1;
    localUsuarioUnidadFuncional.jdoStateManager = paramStateManager;
    return localUsuarioUnidadFuncional;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UsuarioUnidadFuncional localUsuarioUnidadFuncional = new UsuarioUnidadFuncional();
    localUsuarioUnidadFuncional.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUsuarioUnidadFuncional.jdoFlags = 1;
    localUsuarioUnidadFuncional.jdoStateManager = paramStateManager;
    return localUsuarioUnidadFuncional;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUsuarioUnidadFuncional);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.unidadFuncional);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUsuarioUnidadFuncional = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.unidadFuncional = ((UnidadFuncional)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = ((Usuario)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UsuarioUnidadFuncional paramUsuarioUnidadFuncional, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUsuarioUnidadFuncional == null)
        throw new IllegalArgumentException("arg1");
      this.idUsuarioUnidadFuncional = paramUsuarioUnidadFuncional.idUsuarioUnidadFuncional;
      return;
    case 1:
      if (paramUsuarioUnidadFuncional == null)
        throw new IllegalArgumentException("arg1");
      this.unidadFuncional = paramUsuarioUnidadFuncional.unidadFuncional;
      return;
    case 2:
      if (paramUsuarioUnidadFuncional == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramUsuarioUnidadFuncional.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UsuarioUnidadFuncional))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UsuarioUnidadFuncional localUsuarioUnidadFuncional = (UsuarioUnidadFuncional)paramObject;
    if (localUsuarioUnidadFuncional.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUsuarioUnidadFuncional, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UsuarioUnidadFuncionalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UsuarioUnidadFuncionalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UsuarioUnidadFuncionalPK))
      throw new IllegalArgumentException("arg1");
    UsuarioUnidadFuncionalPK localUsuarioUnidadFuncionalPK = (UsuarioUnidadFuncionalPK)paramObject;
    localUsuarioUnidadFuncionalPK.idUsuarioUnidadFuncional = this.idUsuarioUnidadFuncional;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UsuarioUnidadFuncionalPK))
      throw new IllegalArgumentException("arg1");
    UsuarioUnidadFuncionalPK localUsuarioUnidadFuncionalPK = (UsuarioUnidadFuncionalPK)paramObject;
    this.idUsuarioUnidadFuncional = localUsuarioUnidadFuncionalPK.idUsuarioUnidadFuncional;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UsuarioUnidadFuncionalPK))
      throw new IllegalArgumentException("arg2");
    UsuarioUnidadFuncionalPK localUsuarioUnidadFuncionalPK = (UsuarioUnidadFuncionalPK)paramObject;
    localUsuarioUnidadFuncionalPK.idUsuarioUnidadFuncional = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UsuarioUnidadFuncionalPK))
      throw new IllegalArgumentException("arg2");
    UsuarioUnidadFuncionalPK localUsuarioUnidadFuncionalPK = (UsuarioUnidadFuncionalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localUsuarioUnidadFuncionalPK.idUsuarioUnidadFuncional);
  }

  private static final long jdoGetidUsuarioUnidadFuncional(UsuarioUnidadFuncional paramUsuarioUnidadFuncional)
  {
    return paramUsuarioUnidadFuncional.idUsuarioUnidadFuncional;
  }

  private static final void jdoSetidUsuarioUnidadFuncional(UsuarioUnidadFuncional paramUsuarioUnidadFuncional, long paramLong)
  {
    StateManager localStateManager = paramUsuarioUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioUnidadFuncional.idUsuarioUnidadFuncional = paramLong;
      return;
    }
    localStateManager.setLongField(paramUsuarioUnidadFuncional, jdoInheritedFieldCount + 0, paramUsuarioUnidadFuncional.idUsuarioUnidadFuncional, paramLong);
  }

  private static final UnidadFuncional jdoGetunidadFuncional(UsuarioUnidadFuncional paramUsuarioUnidadFuncional)
  {
    StateManager localStateManager = paramUsuarioUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
      return paramUsuarioUnidadFuncional.unidadFuncional;
    if (localStateManager.isLoaded(paramUsuarioUnidadFuncional, jdoInheritedFieldCount + 1))
      return paramUsuarioUnidadFuncional.unidadFuncional;
    return (UnidadFuncional)localStateManager.getObjectField(paramUsuarioUnidadFuncional, jdoInheritedFieldCount + 1, paramUsuarioUnidadFuncional.unidadFuncional);
  }

  private static final void jdoSetunidadFuncional(UsuarioUnidadFuncional paramUsuarioUnidadFuncional, UnidadFuncional paramUnidadFuncional)
  {
    StateManager localStateManager = paramUsuarioUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioUnidadFuncional.unidadFuncional = paramUnidadFuncional;
      return;
    }
    localStateManager.setObjectField(paramUsuarioUnidadFuncional, jdoInheritedFieldCount + 1, paramUsuarioUnidadFuncional.unidadFuncional, paramUnidadFuncional);
  }

  private static final Usuario jdoGetusuario(UsuarioUnidadFuncional paramUsuarioUnidadFuncional)
  {
    StateManager localStateManager = paramUsuarioUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
      return paramUsuarioUnidadFuncional.usuario;
    if (localStateManager.isLoaded(paramUsuarioUnidadFuncional, jdoInheritedFieldCount + 2))
      return paramUsuarioUnidadFuncional.usuario;
    return (Usuario)localStateManager.getObjectField(paramUsuarioUnidadFuncional, jdoInheritedFieldCount + 2, paramUsuarioUnidadFuncional.usuario);
  }

  private static final void jdoSetusuario(UsuarioUnidadFuncional paramUsuarioUnidadFuncional, Usuario paramUsuario)
  {
    StateManager localStateManager = paramUsuarioUnidadFuncional.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioUnidadFuncional.usuario = paramUsuario;
      return;
    }
    localStateManager.setObjectField(paramUsuarioUnidadFuncional, jdoInheritedFieldCount + 2, paramUsuarioUnidadFuncional.usuario, paramUsuario);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}