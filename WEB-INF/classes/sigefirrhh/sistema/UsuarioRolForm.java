package sigefirrhh.sistema;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class UsuarioRolForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(UsuarioRolForm.class.getName());
  private UsuarioRol usuarioRol;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SistemaFacade sistemaFacade = new SistemaFacade();
  private boolean showUsuarioRolByUsuario;
  private boolean showUsuarioRolByRol;
  private String findSelectUsuario;
  private String findSelectRol;
  private Collection findColUsuario;
  private Collection findColRol;
  private Collection colUsuario;
  private Collection colRol;
  private String selectUsuario;
  private String selectRol;
  private Object stateResultUsuarioRolByUsuario = null;

  private Object stateResultUsuarioRolByRol = null;

  public String getFindSelectUsuario()
  {
    return this.findSelectUsuario;
  }
  public void setFindSelectUsuario(String valUsuario) {
    this.findSelectUsuario = valUsuario;
  }

  public Collection getFindColUsuario() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUsuario.iterator();
    Usuario usuario = null;
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      col.add(new SelectItem(
        String.valueOf(usuario.getIdUsuario()), 
        usuario.toString()));
    }
    return col;
  }
  public String getFindSelectRol() {
    return this.findSelectRol;
  }
  public void setFindSelectRol(String valRol) {
    this.findSelectRol = valRol;
  }

  public Collection getFindColRol() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColRol.iterator();
    Rol rol = null;
    while (iterator.hasNext()) {
      rol = (Rol)iterator.next();
      col.add(new SelectItem(
        String.valueOf(rol.getIdRol()), 
        rol.toString()));
    }
    return col;
  }

  public String getSelectUsuario()
  {
    return this.selectUsuario;
  }
  public void setSelectUsuario(String valUsuario) {
    Iterator iterator = this.colUsuario.iterator();
    Usuario usuario = null;
    this.usuarioRol.setUsuario(null);
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      if (String.valueOf(usuario.getIdUsuario()).equals(
        valUsuario)) {
        this.usuarioRol.setUsuario(
          usuario);
        break;
      }
    }
    this.selectUsuario = valUsuario;
  }
  public String getSelectRol() {
    return this.selectRol;
  }
  public void setSelectRol(String valRol) {
    Iterator iterator = this.colRol.iterator();
    Rol rol = null;
    this.usuarioRol.setRol(null);
    while (iterator.hasNext()) {
      rol = (Rol)iterator.next();
      if (String.valueOf(rol.getIdRol()).equals(
        valRol)) {
        this.usuarioRol.setRol(
          rol);
        break;
      }
    }
    this.selectRol = valRol;
  }
  public Collection getResult() {
    return this.result;
  }

  public UsuarioRol getUsuarioRol() {
    if (this.usuarioRol == null) {
      this.usuarioRol = new UsuarioRol();
    }
    return this.usuarioRol;
  }

  public UsuarioRolForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColUsuario()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUsuario.iterator();
    Usuario usuario = null;
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      col.add(new SelectItem(
        String.valueOf(usuario.getIdUsuario()), 
        usuario.toString()));
    }
    return col;
  }

  public Collection getColRol()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colRol.iterator();
    Rol rol = null;
    while (iterator.hasNext()) {
      rol = (Rol)iterator.next();
      col.add(new SelectItem(
        String.valueOf(rol.getIdRol()), 
        rol.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColUsuario = 
        this.sistemaFacade.findAllUsuario();
      this.findColRol = 
        this.sistemaFacade.findAllRol();

      this.colUsuario = 
        this.sistemaFacade.findAllUsuario();
      this.colRol = 
        this.sistemaFacade.findAllRol();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findUsuarioRolByUsuario()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sistemaFacade.findUsuarioRolByUsuario(Long.valueOf(this.findSelectUsuario).longValue());
      this.showUsuarioRolByUsuario = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUsuarioRolByUsuario)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUsuario = null;
    this.findSelectRol = null;

    return null;
  }

  public String findUsuarioRolByRol()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sistemaFacade.findUsuarioRolByRol(Long.valueOf(this.findSelectRol).longValue());
      this.showUsuarioRolByRol = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUsuarioRolByRol)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUsuario = null;
    this.findSelectRol = null;

    return null;
  }

  public boolean isShowUsuarioRolByUsuario() {
    return this.showUsuarioRolByUsuario;
  }
  public boolean isShowUsuarioRolByRol() {
    return this.showUsuarioRolByRol;
  }

  public String selectUsuarioRol()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUsuario = null;
    this.selectRol = null;

    long idUsuarioRol = 
      Long.parseLong((String)requestParameterMap.get("idUsuarioRol"));
    try
    {
      this.usuarioRol = 
        this.sistemaFacade.findUsuarioRolById(
        idUsuarioRol);
      if (this.usuarioRol.getUsuario() != null) {
        this.selectUsuario = 
          String.valueOf(this.usuarioRol.getUsuario().getIdUsuario());
      }
      if (this.usuarioRol.getRol() != null) {
        this.selectRol = 
          String.valueOf(this.usuarioRol.getRol().getIdRol());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.usuarioRol = null;
    this.showUsuarioRolByUsuario = false;
    this.showUsuarioRolByRol = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sistemaFacade.addUsuarioRol(
          this.usuarioRol);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sistemaFacade.updateUsuarioRol(
          this.usuarioRol);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sistemaFacade.deleteUsuarioRol(
        this.usuarioRol);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.usuarioRol = new UsuarioRol();

    this.selectUsuario = null;

    this.selectRol = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.usuarioRol.setIdUsuarioRol(identityGenerator.getNextSequenceNumber("sigefirrhh.sistema.UsuarioRol"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.usuarioRol = new UsuarioRol();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}