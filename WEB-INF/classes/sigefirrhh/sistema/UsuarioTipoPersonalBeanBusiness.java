package sigefirrhh.sistema;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.definiciones.TipoPersonalBeanBusiness;

public class UsuarioTipoPersonalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addUsuarioTipoPersonal(UsuarioTipoPersonal usuarioTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    UsuarioTipoPersonal usuarioTipoPersonalNew = 
      (UsuarioTipoPersonal)BeanUtils.cloneBean(
      usuarioTipoPersonal);

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (usuarioTipoPersonalNew.getUsuario() != null) {
      usuarioTipoPersonalNew.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        usuarioTipoPersonalNew.getUsuario().getIdUsuario()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (usuarioTipoPersonalNew.getTipoPersonal() != null) {
      usuarioTipoPersonalNew.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        usuarioTipoPersonalNew.getTipoPersonal().getIdTipoPersonal()));
    }
    pm.makePersistent(usuarioTipoPersonalNew);
  }

  public void updateUsuarioTipoPersonal(UsuarioTipoPersonal usuarioTipoPersonal) throws Exception
  {
    UsuarioTipoPersonal usuarioTipoPersonalModify = 
      findUsuarioTipoPersonalById(usuarioTipoPersonal.getIdUsuarioTipoPersonal());

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (usuarioTipoPersonal.getUsuario() != null) {
      usuarioTipoPersonal.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        usuarioTipoPersonal.getUsuario().getIdUsuario()));
    }

    TipoPersonalBeanBusiness tipoPersonalBeanBusiness = new TipoPersonalBeanBusiness();

    if (usuarioTipoPersonal.getTipoPersonal() != null) {
      usuarioTipoPersonal.setTipoPersonal(
        tipoPersonalBeanBusiness.findTipoPersonalById(
        usuarioTipoPersonal.getTipoPersonal().getIdTipoPersonal()));
    }

    BeanUtils.copyProperties(usuarioTipoPersonalModify, usuarioTipoPersonal);
  }

  public void deleteUsuarioTipoPersonal(UsuarioTipoPersonal usuarioTipoPersonal) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    UsuarioTipoPersonal usuarioTipoPersonalDelete = 
      findUsuarioTipoPersonalById(usuarioTipoPersonal.getIdUsuarioTipoPersonal());
    pm.deletePersistent(usuarioTipoPersonalDelete);
  }

  public UsuarioTipoPersonal findUsuarioTipoPersonalById(long idUsuarioTipoPersonal) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUsuarioTipoPersonal == pIdUsuarioTipoPersonal";
    Query query = pm.newQuery(UsuarioTipoPersonal.class, filter);

    query.declareParameters("long pIdUsuarioTipoPersonal");

    parameters.put("pIdUsuarioTipoPersonal", new Long(idUsuarioTipoPersonal));

    Collection colUsuarioTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUsuarioTipoPersonal.iterator();
    return (UsuarioTipoPersonal)iterator.next();
  }

  public Collection findUsuarioTipoPersonalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent usuarioTipoPersonalExtent = pm.getExtent(
      UsuarioTipoPersonal.class, true);
    Query query = pm.newQuery(usuarioTipoPersonalExtent);
    query.setOrdering("usuario.apellidos ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUsuario(long idUsuario)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "usuario.idUsuario == pIdUsuario";

    Query query = pm.newQuery(UsuarioTipoPersonal.class, filter);

    query.declareParameters("long pIdUsuario");
    HashMap parameters = new HashMap();

    parameters.put("pIdUsuario", new Long(idUsuario));

    query.setOrdering("usuario.apellidos ascending");

    Collection colUsuarioTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUsuarioTipoPersonal);

    return colUsuarioTipoPersonal;
  }

  public Collection findByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "tipoPersonal.idTipoPersonal == pIdTipoPersonal";

    Query query = pm.newQuery(UsuarioTipoPersonal.class, filter);

    query.declareParameters("long pIdTipoPersonal");
    HashMap parameters = new HashMap();

    parameters.put("pIdTipoPersonal", new Long(idTipoPersonal));

    query.setOrdering("usuario.apellidos ascending");

    Collection colUsuarioTipoPersonal = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUsuarioTipoPersonal);

    return colUsuarioTipoPersonal;
  }
}