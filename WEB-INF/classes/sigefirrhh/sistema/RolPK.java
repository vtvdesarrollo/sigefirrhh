package sigefirrhh.sistema;

import java.io.Serializable;

public class RolPK
  implements Serializable
{
  public long idRol;

  public RolPK()
  {
  }

  public RolPK(long idRol)
  {
    this.idRol = idRol;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((RolPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(RolPK thatPK)
  {
    return 
      this.idRol == thatPK.idRol;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idRol)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idRol);
  }
}