package sigefirrhh.sistema;

import eforserver.business.AbstractBusiness;
import java.util.Collection;
import org.apache.log4j.Logger;

public class SistemaBusiness extends AbstractBusiness
{
  static Logger log = Logger.getLogger(SistemaBusiness.class.getName());

  private OpcionBeanBusiness opcionBeanBusiness = new OpcionBeanBusiness();

  private RolBeanBusiness rolBeanBusiness = new RolBeanBusiness();

  private RolOpcionBeanBusiness rolOpcionBeanBusiness = new RolOpcionBeanBusiness();

  private UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

  private UsuarioOrganismoBeanBusiness usuarioOrganismoBeanBusiness = new UsuarioOrganismoBeanBusiness();

  private UsuarioRolBeanBusiness usuarioRolBeanBusiness = new UsuarioRolBeanBusiness();

  private UsuarioTipoPersonalBeanBusiness usuarioTipoPersonalBeanBusiness = new UsuarioTipoPersonalBeanBusiness();

  private UsuarioUnidadFuncionalBeanBusiness usuarioUnidadFuncionalBeanBusiness = new UsuarioUnidadFuncionalBeanBusiness();

  private ParametroSistemaBeanBusiness parametroSistemaBeanBusiness = new ParametroSistemaBeanBusiness();

  public void addOpcion(Opcion opcion)
    throws Exception
  {
    this.opcionBeanBusiness.addOpcion(opcion);
  }

  public void updateOpcion(Opcion opcion) throws Exception {
    this.opcionBeanBusiness.updateOpcion(opcion);
  }

  public void deleteOpcion(Opcion opcion) throws Exception {
    this.opcionBeanBusiness.deleteOpcion(opcion);
  }

  public Opcion findOpcionById(long opcionId) throws Exception {
    return this.opcionBeanBusiness.findOpcionById(opcionId);
  }

  public Collection findAllOpcion() throws Exception {
    return this.opcionBeanBusiness.findOpcionAll();
  }

  public Collection findOpcionByCodigoOpcion(String codigoOpcion)
    throws Exception
  {
    return this.opcionBeanBusiness.findByCodigoOpcion(codigoOpcion);
  }

  public Collection findOpcionByDescripcion(String descripcion)
    throws Exception
  {
    return this.opcionBeanBusiness.findByDescripcion(descripcion);
  }

  public void addRol(Rol rol)
    throws Exception
  {
    this.rolBeanBusiness.addRol(rol);
  }

  public void updateRol(Rol rol) throws Exception {
    this.rolBeanBusiness.updateRol(rol);
  }

  public void deleteRol(Rol rol) throws Exception {
    this.rolBeanBusiness.deleteRol(rol);
  }

  public Rol findRolById(long rolId) throws Exception {
    return this.rolBeanBusiness.findRolById(rolId);
  }

  public Collection findAllRol() throws Exception {
    return this.rolBeanBusiness.findRolAll();
  }

  public Collection findRolByCodigoRol(String codigoRol)
    throws Exception
  {
    return this.rolBeanBusiness.findByCodigoRol(codigoRol);
  }

  public Collection findRolByNombre(String nombre)
    throws Exception
  {
    return this.rolBeanBusiness.findByNombre(nombre);
  }

  public void addRolOpcion(RolOpcion rolOpcion)
    throws Exception
  {
    this.rolOpcionBeanBusiness.addRolOpcion(rolOpcion);
  }

  public void updateRolOpcion(RolOpcion rolOpcion) throws Exception {
    this.rolOpcionBeanBusiness.updateRolOpcion(rolOpcion);
  }

  public void deleteRolOpcion(RolOpcion rolOpcion) throws Exception {
    this.rolOpcionBeanBusiness.deleteRolOpcion(rolOpcion);
  }

  public RolOpcion findRolOpcionById(long rolOpcionId) throws Exception {
    return this.rolOpcionBeanBusiness.findRolOpcionById(rolOpcionId);
  }

  public Collection findAllRolOpcion() throws Exception {
    return this.rolOpcionBeanBusiness.findRolOpcionAll();
  }

  public Collection findRolOpcionByRol(long idRol)
    throws Exception
  {
    return this.rolOpcionBeanBusiness.findByRol(idRol);
  }

  public void addUsuario(Usuario usuario)
    throws Exception
  {
    this.usuarioBeanBusiness.addUsuario(usuario);
  }

  public void updateUsuario(Usuario usuario) throws Exception {
    this.usuarioBeanBusiness.updateUsuario(usuario);
  }

  public void deleteUsuario(Usuario usuario) throws Exception {
    this.usuarioBeanBusiness.deleteUsuario(usuario);
  }

  public Usuario findUsuarioById(long usuarioId) throws Exception {
    return this.usuarioBeanBusiness.findUsuarioById(usuarioId);
  }

  public Collection findAllUsuario() throws Exception {
    return this.usuarioBeanBusiness.findUsuarioAll();
  }

  public Collection findUsuarioByUsuario(String usuario)
    throws Exception
  {
    return this.usuarioBeanBusiness.findByUsuario(usuario);
  }

  public Collection findUsuarioByApellidos(String apellidos)
    throws Exception
  {
    return this.usuarioBeanBusiness.findByApellidos(apellidos);
  }

  public void addUsuarioOrganismo(UsuarioOrganismo usuarioOrganismo)
    throws Exception
  {
    this.usuarioOrganismoBeanBusiness.addUsuarioOrganismo(usuarioOrganismo);
  }

  public void updateUsuarioOrganismo(UsuarioOrganismo usuarioOrganismo) throws Exception {
    this.usuarioOrganismoBeanBusiness.updateUsuarioOrganismo(usuarioOrganismo);
  }

  public void deleteUsuarioOrganismo(UsuarioOrganismo usuarioOrganismo) throws Exception {
    this.usuarioOrganismoBeanBusiness.deleteUsuarioOrganismo(usuarioOrganismo);
  }

  public UsuarioOrganismo findUsuarioOrganismoById(long usuarioOrganismoId) throws Exception {
    return this.usuarioOrganismoBeanBusiness.findUsuarioOrganismoById(usuarioOrganismoId);
  }

  public Collection findAllUsuarioOrganismo() throws Exception {
    return this.usuarioOrganismoBeanBusiness.findUsuarioOrganismoAll();
  }

  public Collection findUsuarioOrganismoByUsuario(long idUsuario, long idOrganismo)
    throws Exception
  {
    return this.usuarioOrganismoBeanBusiness.findByUsuario(idUsuario, idOrganismo);
  }

  public Collection findUsuarioOrganismoByOrganismo(long idOrganismo)
    throws Exception
  {
    return this.usuarioOrganismoBeanBusiness.findByOrganismo(idOrganismo);
  }

  public void addUsuarioRol(UsuarioRol usuarioRol)
    throws Exception
  {
    this.usuarioRolBeanBusiness.addUsuarioRol(usuarioRol);
  }

  public void updateUsuarioRol(UsuarioRol usuarioRol) throws Exception {
    this.usuarioRolBeanBusiness.updateUsuarioRol(usuarioRol);
  }

  public void deleteUsuarioRol(UsuarioRol usuarioRol) throws Exception {
    this.usuarioRolBeanBusiness.deleteUsuarioRol(usuarioRol);
  }

  public UsuarioRol findUsuarioRolById(long usuarioRolId) throws Exception {
    return this.usuarioRolBeanBusiness.findUsuarioRolById(usuarioRolId);
  }

  public Collection findAllUsuarioRol() throws Exception {
    return this.usuarioRolBeanBusiness.findUsuarioRolAll();
  }

  public Collection findUsuarioRolByUsuario(long idUsuario)
    throws Exception
  {
    return this.usuarioRolBeanBusiness.findByUsuario(idUsuario);
  }

  public Collection findUsuarioRolByRol(long idRol)
    throws Exception
  {
    return this.usuarioRolBeanBusiness.findByRol(idRol);
  }

  public void addUsuarioTipoPersonal(UsuarioTipoPersonal usuarioTipoPersonal)
    throws Exception
  {
    this.usuarioTipoPersonalBeanBusiness.addUsuarioTipoPersonal(usuarioTipoPersonal);
  }

  public void updateUsuarioTipoPersonal(UsuarioTipoPersonal usuarioTipoPersonal) throws Exception {
    this.usuarioTipoPersonalBeanBusiness.updateUsuarioTipoPersonal(usuarioTipoPersonal);
  }

  public void deleteUsuarioTipoPersonal(UsuarioTipoPersonal usuarioTipoPersonal) throws Exception {
    this.usuarioTipoPersonalBeanBusiness.deleteUsuarioTipoPersonal(usuarioTipoPersonal);
  }

  public UsuarioTipoPersonal findUsuarioTipoPersonalById(long usuarioTipoPersonalId) throws Exception {
    return this.usuarioTipoPersonalBeanBusiness.findUsuarioTipoPersonalById(usuarioTipoPersonalId);
  }

  public Collection findAllUsuarioTipoPersonal() throws Exception {
    return this.usuarioTipoPersonalBeanBusiness.findUsuarioTipoPersonalAll();
  }

  public Collection findUsuarioTipoPersonalByUsuario(long idUsuario)
    throws Exception
  {
    return this.usuarioTipoPersonalBeanBusiness.findByUsuario(idUsuario);
  }

  public Collection findUsuarioTipoPersonalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    return this.usuarioTipoPersonalBeanBusiness.findByTipoPersonal(idTipoPersonal);
  }

  public Collection findUsuarioByPassword(String usuario, String password)
    throws Exception
  {
    return this.usuarioBeanBusiness.findByPassword(usuario, password);
  }

  public Collection findRolOpcionByRolAndOpcion(long idRol, String ruta)
    throws Exception
  {
    return this.rolOpcionBeanBusiness.findByRolAndOpcion(idRol, ruta);
  }

  public void addUsuarioUnidadFuncional(UsuarioUnidadFuncional usuarioUnidadFuncional)
    throws Exception
  {
    this.usuarioUnidadFuncionalBeanBusiness.addUsuarioUnidadFuncional(usuarioUnidadFuncional);
  }

  public void updateUsuarioUnidadFuncional(UsuarioUnidadFuncional usuarioUnidadFuncional) throws Exception {
    this.usuarioUnidadFuncionalBeanBusiness.updateUsuarioUnidadFuncional(usuarioUnidadFuncional);
  }

  public void deleteUsuarioUnidadFuncional(UsuarioUnidadFuncional usuarioUnidadFuncional) throws Exception {
    this.usuarioUnidadFuncionalBeanBusiness.deleteUsuarioUnidadFuncional(usuarioUnidadFuncional);
  }

  public UsuarioUnidadFuncional findUsuarioUnidadFuncionalById(long usuarioUnidadFuncionalId) throws Exception {
    return this.usuarioUnidadFuncionalBeanBusiness.findUsuarioUnidadFuncionalById(usuarioUnidadFuncionalId);
  }

  public Collection findAllUsuarioUnidadFuncional() throws Exception {
    return this.usuarioUnidadFuncionalBeanBusiness.findUsuarioUnidadFuncionalAll();
  }

  public Collection findUsuarioUnidadFuncionalByUsuario(long idUsuario)
    throws Exception
  {
    return this.usuarioUnidadFuncionalBeanBusiness.findByUsuario(idUsuario);
  }

  public Collection findUsuarioUnidadFuncionalByUnidadFuncional(long idUnidadFuncional)
    throws Exception
  {
    return this.usuarioUnidadFuncionalBeanBusiness.findByUnidadFuncional(idUnidadFuncional);
  }

  public ParametroSistema findParametroSistemaByName(String nombreParametro)
    throws Exception
  {
    return this.parametroSistemaBeanBusiness.findParametroSistemaByName(nombreParametro);
  }

  public int getParametroDiasVigenciaUsuario()
  {
    int dias = 95;
    ParametroSistema p = new ParametroSistema();
    try {
      p = this.parametroSistemaBeanBusiness.findParametroSistemaByName("diasVigenciaUsuario");
    } catch (Exception e) {
      try {
        p = new ParametroSistema();
        p.setNombreParametro("diasVigenciaUsuario");
        p.setTipoParametro("entero");
        p.setValorParametro("95");
        p.setDescripcionParametro("Días de vigencia por defecto del usuario, se sumarán a la fecha de creación del usuario y se guardará en la fecha de vencimiento");
        this.parametroSistemaBeanBusiness.addParametroSistema(p);
      } catch (Exception ex) {
        dias = 95;
        log.error("==> Error Creando parametro 'diasVigenciaUsuario'", ex);
      }
    }
    if (p == null)
      try {
        p = new ParametroSistema();
        p.setNombreParametro("diasVigenciaUsuario");
        p.setTipoParametro("entero");
        p.setValorParametro("6");
        p.setDescripcionParametro("Días de vigencia por defecto del usuario, se sumarán a la fecha de creación del usuario y se guardará en la fecha de vencimiento");
        this.parametroSistemaBeanBusiness.addParametroSistema(p);
      } catch (Exception ex) {
        dias = 95;
        log.error("==> *Error Creando parametro 'diasVigenciaUsuario'", ex);
      }
    else {
      dias = Integer.parseInt(p.getValorParametro());
    }
    return dias;
  }

  public int getParametroMinLongitudClave() {
    int minLen = 6;
    ParametroSistema p = new ParametroSistema();
    try {
      p = this.parametroSistemaBeanBusiness.findParametroSistemaByName("minlongitudclave");
    } catch (Exception e) {
      try {
        p = new ParametroSistema();
        p.setNombreParametro("minlongitudclave");
        p.setTipoParametro("entero");
        p.setValorParametro("6");
        p.setDescripcionParametro("Longitud minima para la contraseña de usuarios");
        this.parametroSistemaBeanBusiness.addParametroSistema(p);
      } catch (Exception ex) {
        minLen = 6;
        log.error("==> Error Creando parametro 'minlongitudclave'", ex);
      }
    }
    if (p == null)
      try {
        p = new ParametroSistema();
        p.setNombreParametro("minlongitudclave");
        p.setTipoParametro("entero");
        p.setValorParametro("6");
        p.setDescripcionParametro("Longitud minima para la contraseña de usuarios");
        this.parametroSistemaBeanBusiness.addParametroSistema(p);
      } catch (Exception ex) {
        minLen = 6;
        log.error("==> *Error Creando parametro 'minlongitudclave'", ex);
      }
    else {
      minLen = Integer.parseInt(p.getValorParametro());
    }
    return minLen;
  }

  public int getParametroMaxIntentos()
  {
    int maxIntentos = 4;
    ParametroSistema p = new ParametroSistema();
    try {
      log.debug("==> Cargando parametro 'maxintentos'");
      p = this.parametroSistemaBeanBusiness.findParametroSistemaByName("maxintentos");
    } catch (Exception e) {
      try {
        log.error("==> Creando parametro 'maxintentos'", e);
        p = new ParametroSistema();
        p.setNombreParametro("maxintentos");
        p.setTipoParametro("entero");
        p.setValorParametro("4");
        p.setDescripcionParametro("Maxima cantidad de intentos de login fallidos antes de desactivar el usuario");
        this.parametroSistemaBeanBusiness.addParametroSistema(p);
        log.debug("==> Creado parametro 'maxintentos'");
      } catch (Exception ex) {
        maxIntentos = 4;
        log.error("==> Error Creando parametro 'maxintentos'", ex);
      }
    }
    if (p == null)
      try {
        log.debug("==> *Creando parametro 'maxintentos'");
        p = new ParametroSistema();
        p.setNombreParametro("maxintentos");
        p.setTipoParametro("entero");
        p.setValorParametro("4");
        p.setDescripcionParametro("Maxima cantidad de intentos de login fallidos antes de desactivar el usuario");
        this.parametroSistemaBeanBusiness.addParametroSistema(p);
        log.debug("==> *Creado parametro 'maxintentos'");
      } catch (Exception ex) {
        maxIntentos = 4;
        log.error("==> *Error Creando parametro 'maxintentos'", ex);
      }
    else {
      maxIntentos = Integer.parseInt(p.getValorParametro());
    }
    return maxIntentos;
  }

  public int getParametroDiasDuracionClave()
  {
    int diasDuracionClave = 90;
    ParametroSistema p = new ParametroSistema();
    try {
      log.debug("==> Cargando parametro 'diasduracionclave'");
      p = this.parametroSistemaBeanBusiness.findParametroSistemaByName("diasduracionclave");
    } catch (Exception e) {
      try {
        log.error("==> * Error: " + e.getMessage(), e.getCause());
        log.debug("==> Agregando parametro 'diasduracionclave'");
        p = new ParametroSistema();
        log.debug(" -> setNombreParametro 'diasduracionclave'");
        p.setNombreParametro("diasduracionclave");
        log.debug(" -> setTipoParametro 'entero'");
        p.setTipoParametro("entero");
        log.debug(" -> setValorParametro '90'");
        p.setValorParametro("90");
        log.debug(" -> setDescripcionParametro 'Cantidad de días ...'");
        p.setDescripcionParametro("Cantidad de días de vigencia de la clave");
        log.debug(" -> addParametroSistema(p)");
        this.parametroSistemaBeanBusiness.addParametroSistema(p);
        log.debug("==> Parametro agregado 'diasduracionclave'");
      } catch (Exception ex) {
        diasDuracionClave = 90;
        log.error("==> Error agregando parametro 'diasduracionclave'", ex);
      }
    }
    if (p == null)
      try {
        log.debug("==> *Agregando parametro 'diasduracionclave'");
        p = new ParametroSistema();
        p.setNombreParametro("diasduracionclave");
        p.setTipoParametro("entero");
        p.setValorParametro("90");
        p.setDescripcionParametro("Cantidad de días de vigencia de la clave, si pasan mas dias desde el ultimo cambio de clave se obliga a cambiarla");
        this.parametroSistemaBeanBusiness.addParametroSistema(p);
      } catch (Exception ex) {
        diasDuracionClave = 90;
      }
    else {
      diasDuracionClave = Integer.parseInt(p.getValorParametro());
    }
    return diasDuracionClave;
  }

  public int getParametroLongitudMinimaClave() {
    int diasDuracionClave = 90;
    ParametroSistema p = new ParametroSistema();
    try {
      p = this.parametroSistemaBeanBusiness.findParametroSistemaByName("longitudminimaclave");
    } catch (Exception e) {
      try {
        log.error("==> Error getParametroLongitudMinimaClave.findParametroSistemaById, agregando registro...", e);
        p = new ParametroSistema();
        p.setNombreParametro("longitudminimaclave");
        p.setTipoParametro("entero");
        p.setValorParametro("5");
        p.setDescripcionParametro("Longitud minima permitida para la clave de usuario");
        this.parametroSistemaBeanBusiness.addParametroSistema(p);
        log.debug("==> En getParametroLongitudMinimaClave, registro agregado.");
      } catch (Exception ex) {
        diasDuracionClave = 90;
      }
    }
    if (p == null)
      try {
        log.debug("==> Parametro es null en getParametroLongitudMinimaClave, agregando registro...");
        p = new ParametroSistema();
        p.setNombreParametro("longitudminimaclave");
        p.setTipoParametro("entero");
        p.setValorParametro("5");
        p.setDescripcionParametro("Longitud minima permitida para la clave de usuario");
        this.parametroSistemaBeanBusiness.addParametroSistema(p);
        log.debug("==> En getParametroLongitudMinimaClave, registro agregado.");
      } catch (Exception ex) {
        diasDuracionClave = 90;
        log.error("==> En getParametroLongitudMinimaClave, error al agregar registro.", ex);
      }
    else {
      diasDuracionClave = Integer.parseInt(p.getValorParametro());
    }
    log.debug("==> En getParametroLongitudMinimaClave, diasDuracionClave --> " + String.valueOf(diasDuracionClave));
    return diasDuracionClave;
  }
}