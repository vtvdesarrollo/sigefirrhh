package sigefirrhh.sistema;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class RolBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addRol(Rol rol)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Rol rolNew = 
      (Rol)BeanUtils.cloneBean(
      rol);

    pm.makePersistent(rolNew);
  }

  public void updateRol(Rol rol) throws Exception
  {
    Rol rolModify = 
      findRolById(rol.getIdRol());

    BeanUtils.copyProperties(rolModify, rol);
  }

  public void deleteRol(Rol rol) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    Rol rolDelete = 
      findRolById(rol.getIdRol());
    pm.deletePersistent(rolDelete);
  }

  public Rol findRolById(long idRol) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRol == pIdRol";
    Query query = pm.newQuery(Rol.class, filter);

    query.declareParameters("long pIdRol");

    parameters.put("pIdRol", new Long(idRol));

    Collection colRol = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRol.iterator();
    return (Rol)iterator.next();
  }

  public Collection findRolAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent rolExtent = pm.getExtent(
      Rol.class, true);
    Query query = pm.newQuery(rolExtent);
    query.setOrdering("nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByCodigoRol(String codigoRol)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "codigoRol == pCodigoRol";

    Query query = pm.newQuery(Rol.class, filter);

    query.setOrdering("nombre ascending");

    query.declareParameters("java.lang.String pCodigoRol");
    HashMap parameters = new HashMap();

    parameters.put("pCodigoRol", new String(codigoRol));

    Collection colRol = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRol);

    return colRol;
  }

  public Collection findByNombre(String nombre)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "nombre.startsWith(pNombre)";

    Query query = pm.newQuery(Rol.class, filter);

    query.setOrdering("nombre ascending");

    query.declareParameters("java.lang.String pNombre");
    HashMap parameters = new HashMap();

    parameters.put("pNombre", new String(nombre));

    Collection colRol = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRol);

    return colRol;
  }
}