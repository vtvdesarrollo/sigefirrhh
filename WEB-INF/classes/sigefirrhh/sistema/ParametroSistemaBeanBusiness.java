package sigefirrhh.sistema;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

public class ParametroSistemaBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  Logger log = Logger.getLogger(ParametroSistemaBeanBusiness.class.getName());

  public void addParametroSistema(ParametroSistema parametro)
    throws Exception
  {
    try
    {
      PersistenceManager pm = PMThread.getPM();

      ParametroSistema NuevoP = (ParametroSistema)BeanUtils.cloneBean(parametro);

      pm.makePersistent(NuevoP);
    } catch (Exception ex) {
      this.log.error("--> Error al agregar " + parametro.getNombreParametro() + ", valor: '" + parametro.getValorParametro() + "'\n" + ex.getMessage());
    }
  }

  public void updateParametroSistema(ParametroSistema parametro)
    throws Exception
  {
    ParametroSistema parametroModificar = 
      findParametroSistemaByName(parametro.getNombreParametro());

    BeanUtils.copyProperties(parametroModificar, parametro);
  }

  public void deleteParametroSistema(ParametroSistema parametro)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    ParametroSistema parametroBorrar = 
      findParametroSistemaByName(parametro.getNombreParametro());
    pm.deletePersistent(parametroBorrar);
  }

  public ParametroSistema findParametroSistemaByName(String nombreParametro)
    throws Exception
  {
    this.log.debug("-=> Nombre del parametro: " + nombreParametro);
    try
    {
      HashMap parameters = new HashMap();

      PersistenceManager pm = PMThread.getPM();
      String filter = "nombreParametro == pnombreParametro";
      Query query = pm.newQuery(ParametroSistema.class, filter);

      query.declareParameters("String pnombreParametro");

      parameters.put("pnombreParametro", nombreParametro);

      Collection colParametros = 
        new ArrayList((Collection)query.executeWithMap(parameters));

      Iterator it = colParametros.iterator();
      if (it.hasNext()) {
        return (ParametroSistema)it.next();
      }
      this.log.error("Error al buscar parámetro " + nombreParametro);
      throw new Exception("No se encontró el parámetro " + nombreParametro);
    }
    catch (Exception ex) {
      this.log.error("Error al buscar parámetro " + nombreParametro, ex.getCause());
      throw new Exception("Error al buscar parámetro " + nombreParametro, ex);
    }
  }

  public Collection findParametroSistemaAll()
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent extent = pm.getExtent(
      ParametroSistema.class, true);

    Query query = pm.newQuery(extent);

    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  private Connection getConnection() {
    return Resource.getConnection();
  }
}