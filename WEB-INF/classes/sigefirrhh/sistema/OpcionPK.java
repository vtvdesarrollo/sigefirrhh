package sigefirrhh.sistema;

import java.io.Serializable;

public class OpcionPK
  implements Serializable
{
  public long idOpcion;

  public OpcionPK()
  {
  }

  public OpcionPK(long idOpcion)
  {
    this.idOpcion = idOpcion;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((OpcionPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(OpcionPK thatPK)
  {
    return 
      this.idOpcion == thatPK.idOpcion;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idOpcion)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idOpcion);
  }
}