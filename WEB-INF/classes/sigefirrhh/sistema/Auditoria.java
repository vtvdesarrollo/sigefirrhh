package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.Concepto;
import sigefirrhh.base.definiciones.FrecuenciaPago;
import sigefirrhh.personal.trabajador.Trabajador;

public class Auditoria
  implements Serializable, PersistenceCapable
{
  private long idAuditoria;
  private Date fechaTransaccion;
  private double montoConcepto;
  private String tipoAccion;
  private String valorAntes;
  private String campoActualizado;
  private Trabajador trabajador;
  private AccionAuditoria accionAuditoria;
  private Concepto concepto;
  private FrecuenciaPago frecuenciaPago;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "accionAuditoria", "campoActualizado", "concepto", "fechaTransaccion", "frecuenciaPago", "idAuditoria", "montoConcepto", "tipoAccion", "trabajador", "valorAntes" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("sigefirrhh.sistema.AccionAuditoria"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.definiciones.Concepto"), sunjdo$classForName$("java.util.Date"), sunjdo$classForName$("sigefirrhh.base.definiciones.FrecuenciaPago"), Long.TYPE, Double.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.personal.trabajador.Trabajador"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 26, 21, 26, 21, 26, 24, 21, 21, 26, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public AccionAuditoria getAccionAuditoria()
  {
    return jdoGetaccionAuditoria(this);
  }

  public String getCampoActualizado()
  {
    return jdoGetcampoActualizado(this);
  }

  public Concepto getConcepto()
  {
    return jdoGetconcepto(this);
  }

  public Date getFechaTransaccion()
  {
    return jdoGetfechaTransaccion(this);
  }

  public FrecuenciaPago getFrecuenciaPago()
  {
    return jdoGetfrecuenciaPago(this);
  }

  public long getIdAuditoria()
  {
    return jdoGetidAuditoria(this);
  }

  public double getMontoConcepto()
  {
    return jdoGetmontoConcepto(this);
  }

  public String getTipoAccion()
  {
    return jdoGettipoAccion(this);
  }

  public Trabajador getTrabajador()
  {
    return jdoGettrabajador(this);
  }

  public String getValorAntes()
  {
    return jdoGetvalorAntes(this);
  }

  public void setAccionAuditoria(AccionAuditoria auditoria)
  {
    jdoSetaccionAuditoria(this, auditoria);
  }

  public void setCampoActualizado(String string)
  {
    jdoSetcampoActualizado(this, string);
  }

  public void setConcepto(Concepto concepto)
  {
    jdoSetconcepto(this, concepto);
  }

  public void setFechaTransaccion(Date date)
  {
    jdoSetfechaTransaccion(this, date);
  }

  public void setFrecuenciaPago(FrecuenciaPago pago)
  {
    jdoSetfrecuenciaPago(this, pago);
  }

  public void setIdAuditoria(long l)
  {
    jdoSetidAuditoria(this, l);
  }

  public void setMontoConcepto(double d)
  {
    jdoSetmontoConcepto(this, d);
  }

  public void setTipoAccion(String string)
  {
    jdoSettipoAccion(this, string);
  }

  public void setTrabajador(Trabajador trabajador)
  {
    jdoSettrabajador(this, trabajador);
  }

  public void setValorAntes(String string)
  {
    jdoSetvalorAntes(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 10;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.Auditoria"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Auditoria());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Auditoria localAuditoria = new Auditoria();
    localAuditoria.jdoFlags = 1;
    localAuditoria.jdoStateManager = paramStateManager;
    return localAuditoria;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Auditoria localAuditoria = new Auditoria();
    localAuditoria.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAuditoria.jdoFlags = 1;
    localAuditoria.jdoStateManager = paramStateManager;
    return localAuditoria;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.accionAuditoria);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.campoActualizado);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.concepto);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaTransaccion);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.frecuenciaPago);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAuditoria);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedDoubleField(this, paramInt, this.montoConcepto);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoAccion);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.trabajador);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.valorAntes);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.accionAuditoria = ((AccionAuditoria)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.campoActualizado = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.concepto = ((Concepto)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaTransaccion = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.frecuenciaPago = ((FrecuenciaPago)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAuditoria = localStateManager.replacingLongField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.montoConcepto = localStateManager.replacingDoubleField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoAccion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.trabajador = ((Trabajador)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.valorAntes = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Auditoria paramAuditoria, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.accionAuditoria = paramAuditoria.accionAuditoria;
      return;
    case 1:
      if (paramAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.campoActualizado = paramAuditoria.campoActualizado;
      return;
    case 2:
      if (paramAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.concepto = paramAuditoria.concepto;
      return;
    case 3:
      if (paramAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.fechaTransaccion = paramAuditoria.fechaTransaccion;
      return;
    case 4:
      if (paramAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.frecuenciaPago = paramAuditoria.frecuenciaPago;
      return;
    case 5:
      if (paramAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.idAuditoria = paramAuditoria.idAuditoria;
      return;
    case 6:
      if (paramAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.montoConcepto = paramAuditoria.montoConcepto;
      return;
    case 7:
      if (paramAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.tipoAccion = paramAuditoria.tipoAccion;
      return;
    case 8:
      if (paramAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.trabajador = paramAuditoria.trabajador;
      return;
    case 9:
      if (paramAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.valorAntes = paramAuditoria.valorAntes;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Auditoria))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Auditoria localAuditoria = (Auditoria)paramObject;
    if (localAuditoria.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAuditoria, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AuditoriaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AuditoriaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AuditoriaPK))
      throw new IllegalArgumentException("arg1");
    AuditoriaPK localAuditoriaPK = (AuditoriaPK)paramObject;
    localAuditoriaPK.idAuditoria = this.idAuditoria;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AuditoriaPK))
      throw new IllegalArgumentException("arg1");
    AuditoriaPK localAuditoriaPK = (AuditoriaPK)paramObject;
    this.idAuditoria = localAuditoriaPK.idAuditoria;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AuditoriaPK))
      throw new IllegalArgumentException("arg2");
    AuditoriaPK localAuditoriaPK = (AuditoriaPK)paramObject;
    localAuditoriaPK.idAuditoria = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 5);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AuditoriaPK))
      throw new IllegalArgumentException("arg2");
    AuditoriaPK localAuditoriaPK = (AuditoriaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 5, localAuditoriaPK.idAuditoria);
  }

  private static final AccionAuditoria jdoGetaccionAuditoria(Auditoria paramAuditoria)
  {
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAuditoria.accionAuditoria;
    if (localStateManager.isLoaded(paramAuditoria, jdoInheritedFieldCount + 0))
      return paramAuditoria.accionAuditoria;
    return (AccionAuditoria)localStateManager.getObjectField(paramAuditoria, jdoInheritedFieldCount + 0, paramAuditoria.accionAuditoria);
  }

  private static final void jdoSetaccionAuditoria(Auditoria paramAuditoria, AccionAuditoria paramAccionAuditoria)
  {
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAuditoria.accionAuditoria = paramAccionAuditoria;
      return;
    }
    localStateManager.setObjectField(paramAuditoria, jdoInheritedFieldCount + 0, paramAuditoria.accionAuditoria, paramAccionAuditoria);
  }

  private static final String jdoGetcampoActualizado(Auditoria paramAuditoria)
  {
    if (paramAuditoria.jdoFlags <= 0)
      return paramAuditoria.campoActualizado;
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAuditoria.campoActualizado;
    if (localStateManager.isLoaded(paramAuditoria, jdoInheritedFieldCount + 1))
      return paramAuditoria.campoActualizado;
    return localStateManager.getStringField(paramAuditoria, jdoInheritedFieldCount + 1, paramAuditoria.campoActualizado);
  }

  private static final void jdoSetcampoActualizado(Auditoria paramAuditoria, String paramString)
  {
    if (paramAuditoria.jdoFlags == 0)
    {
      paramAuditoria.campoActualizado = paramString;
      return;
    }
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAuditoria.campoActualizado = paramString;
      return;
    }
    localStateManager.setStringField(paramAuditoria, jdoInheritedFieldCount + 1, paramAuditoria.campoActualizado, paramString);
  }

  private static final Concepto jdoGetconcepto(Auditoria paramAuditoria)
  {
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAuditoria.concepto;
    if (localStateManager.isLoaded(paramAuditoria, jdoInheritedFieldCount + 2))
      return paramAuditoria.concepto;
    return (Concepto)localStateManager.getObjectField(paramAuditoria, jdoInheritedFieldCount + 2, paramAuditoria.concepto);
  }

  private static final void jdoSetconcepto(Auditoria paramAuditoria, Concepto paramConcepto)
  {
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAuditoria.concepto = paramConcepto;
      return;
    }
    localStateManager.setObjectField(paramAuditoria, jdoInheritedFieldCount + 2, paramAuditoria.concepto, paramConcepto);
  }

  private static final Date jdoGetfechaTransaccion(Auditoria paramAuditoria)
  {
    if (paramAuditoria.jdoFlags <= 0)
      return paramAuditoria.fechaTransaccion;
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAuditoria.fechaTransaccion;
    if (localStateManager.isLoaded(paramAuditoria, jdoInheritedFieldCount + 3))
      return paramAuditoria.fechaTransaccion;
    return (Date)localStateManager.getObjectField(paramAuditoria, jdoInheritedFieldCount + 3, paramAuditoria.fechaTransaccion);
  }

  private static final void jdoSetfechaTransaccion(Auditoria paramAuditoria, Date paramDate)
  {
    if (paramAuditoria.jdoFlags == 0)
    {
      paramAuditoria.fechaTransaccion = paramDate;
      return;
    }
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAuditoria.fechaTransaccion = paramDate;
      return;
    }
    localStateManager.setObjectField(paramAuditoria, jdoInheritedFieldCount + 3, paramAuditoria.fechaTransaccion, paramDate);
  }

  private static final FrecuenciaPago jdoGetfrecuenciaPago(Auditoria paramAuditoria)
  {
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAuditoria.frecuenciaPago;
    if (localStateManager.isLoaded(paramAuditoria, jdoInheritedFieldCount + 4))
      return paramAuditoria.frecuenciaPago;
    return (FrecuenciaPago)localStateManager.getObjectField(paramAuditoria, jdoInheritedFieldCount + 4, paramAuditoria.frecuenciaPago);
  }

  private static final void jdoSetfrecuenciaPago(Auditoria paramAuditoria, FrecuenciaPago paramFrecuenciaPago)
  {
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAuditoria.frecuenciaPago = paramFrecuenciaPago;
      return;
    }
    localStateManager.setObjectField(paramAuditoria, jdoInheritedFieldCount + 4, paramAuditoria.frecuenciaPago, paramFrecuenciaPago);
  }

  private static final long jdoGetidAuditoria(Auditoria paramAuditoria)
  {
    return paramAuditoria.idAuditoria;
  }

  private static final void jdoSetidAuditoria(Auditoria paramAuditoria, long paramLong)
  {
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAuditoria.idAuditoria = paramLong;
      return;
    }
    localStateManager.setLongField(paramAuditoria, jdoInheritedFieldCount + 5, paramAuditoria.idAuditoria, paramLong);
  }

  private static final double jdoGetmontoConcepto(Auditoria paramAuditoria)
  {
    if (paramAuditoria.jdoFlags <= 0)
      return paramAuditoria.montoConcepto;
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAuditoria.montoConcepto;
    if (localStateManager.isLoaded(paramAuditoria, jdoInheritedFieldCount + 6))
      return paramAuditoria.montoConcepto;
    return localStateManager.getDoubleField(paramAuditoria, jdoInheritedFieldCount + 6, paramAuditoria.montoConcepto);
  }

  private static final void jdoSetmontoConcepto(Auditoria paramAuditoria, double paramDouble)
  {
    if (paramAuditoria.jdoFlags == 0)
    {
      paramAuditoria.montoConcepto = paramDouble;
      return;
    }
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAuditoria.montoConcepto = paramDouble;
      return;
    }
    localStateManager.setDoubleField(paramAuditoria, jdoInheritedFieldCount + 6, paramAuditoria.montoConcepto, paramDouble);
  }

  private static final String jdoGettipoAccion(Auditoria paramAuditoria)
  {
    if (paramAuditoria.jdoFlags <= 0)
      return paramAuditoria.tipoAccion;
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAuditoria.tipoAccion;
    if (localStateManager.isLoaded(paramAuditoria, jdoInheritedFieldCount + 7))
      return paramAuditoria.tipoAccion;
    return localStateManager.getStringField(paramAuditoria, jdoInheritedFieldCount + 7, paramAuditoria.tipoAccion);
  }

  private static final void jdoSettipoAccion(Auditoria paramAuditoria, String paramString)
  {
    if (paramAuditoria.jdoFlags == 0)
    {
      paramAuditoria.tipoAccion = paramString;
      return;
    }
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAuditoria.tipoAccion = paramString;
      return;
    }
    localStateManager.setStringField(paramAuditoria, jdoInheritedFieldCount + 7, paramAuditoria.tipoAccion, paramString);
  }

  private static final Trabajador jdoGettrabajador(Auditoria paramAuditoria)
  {
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAuditoria.trabajador;
    if (localStateManager.isLoaded(paramAuditoria, jdoInheritedFieldCount + 8))
      return paramAuditoria.trabajador;
    return (Trabajador)localStateManager.getObjectField(paramAuditoria, jdoInheritedFieldCount + 8, paramAuditoria.trabajador);
  }

  private static final void jdoSettrabajador(Auditoria paramAuditoria, Trabajador paramTrabajador)
  {
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAuditoria.trabajador = paramTrabajador;
      return;
    }
    localStateManager.setObjectField(paramAuditoria, jdoInheritedFieldCount + 8, paramAuditoria.trabajador, paramTrabajador);
  }

  private static final String jdoGetvalorAntes(Auditoria paramAuditoria)
  {
    if (paramAuditoria.jdoFlags <= 0)
      return paramAuditoria.valorAntes;
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAuditoria.valorAntes;
    if (localStateManager.isLoaded(paramAuditoria, jdoInheritedFieldCount + 9))
      return paramAuditoria.valorAntes;
    return localStateManager.getStringField(paramAuditoria, jdoInheritedFieldCount + 9, paramAuditoria.valorAntes);
  }

  private static final void jdoSetvalorAntes(Auditoria paramAuditoria, String paramString)
  {
    if (paramAuditoria.jdoFlags == 0)
    {
      paramAuditoria.valorAntes = paramString;
      return;
    }
    StateManager localStateManager = paramAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAuditoria.valorAntes = paramString;
      return;
    }
    localStateManager.setStringField(paramAuditoria, jdoInheritedFieldCount + 9, paramAuditoria.valorAntes, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}