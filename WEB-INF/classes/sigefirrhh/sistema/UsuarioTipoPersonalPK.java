package sigefirrhh.sistema;

import java.io.Serializable;

public class UsuarioTipoPersonalPK
  implements Serializable
{
  public long idUsuarioTipoPersonal;

  public UsuarioTipoPersonalPK()
  {
  }

  public UsuarioTipoPersonalPK(long idUsuarioTipoPersonal)
  {
    this.idUsuarioTipoPersonal = idUsuarioTipoPersonal;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UsuarioTipoPersonalPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UsuarioTipoPersonalPK thatPK)
  {
    return 
      this.idUsuarioTipoPersonal == thatPK.idUsuarioTipoPersonal;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUsuarioTipoPersonal)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUsuarioTipoPersonal);
  }
}