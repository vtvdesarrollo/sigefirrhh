package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.definiciones.TipoPersonal;

public class UsuarioTipoPersonal
  implements Serializable, PersistenceCapable
{
  private long idUsuarioTipoPersonal;
  private Usuario usuario;
  private TipoPersonal tipoPersonal;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idUsuarioTipoPersonal", "tipoPersonal", "usuario" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("sigefirrhh.base.definiciones.TipoPersonal"), sunjdo$classForName$("sigefirrhh.sistema.Usuario") };
  private static final byte[] jdoFieldFlags = { 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetusuario(this).toString() + " - " + jdoGettipoPersonal(this).getNombre();
  }

  public long getIdUsuarioTipoPersonal()
  {
    return jdoGetidUsuarioTipoPersonal(this);
  }

  public TipoPersonal getTipoPersonal()
  {
    return jdoGettipoPersonal(this);
  }

  public Usuario getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setIdUsuarioTipoPersonal(long l)
  {
    jdoSetidUsuarioTipoPersonal(this, l);
  }

  public void setTipoPersonal(TipoPersonal personal)
  {
    jdoSettipoPersonal(this, personal);
  }

  public void setUsuario(Usuario usuario)
  {
    jdoSetusuario(this, usuario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.UsuarioTipoPersonal"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UsuarioTipoPersonal());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UsuarioTipoPersonal localUsuarioTipoPersonal = new UsuarioTipoPersonal();
    localUsuarioTipoPersonal.jdoFlags = 1;
    localUsuarioTipoPersonal.jdoStateManager = paramStateManager;
    return localUsuarioTipoPersonal;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UsuarioTipoPersonal localUsuarioTipoPersonal = new UsuarioTipoPersonal();
    localUsuarioTipoPersonal.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUsuarioTipoPersonal.jdoFlags = 1;
    localUsuarioTipoPersonal.jdoStateManager = paramStateManager;
    return localUsuarioTipoPersonal;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUsuarioTipoPersonal);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.tipoPersonal);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUsuarioTipoPersonal = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoPersonal = ((TipoPersonal)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = ((Usuario)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UsuarioTipoPersonal paramUsuarioTipoPersonal, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUsuarioTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.idUsuarioTipoPersonal = paramUsuarioTipoPersonal.idUsuarioTipoPersonal;
      return;
    case 1:
      if (paramUsuarioTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.tipoPersonal = paramUsuarioTipoPersonal.tipoPersonal;
      return;
    case 2:
      if (paramUsuarioTipoPersonal == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramUsuarioTipoPersonal.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UsuarioTipoPersonal))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UsuarioTipoPersonal localUsuarioTipoPersonal = (UsuarioTipoPersonal)paramObject;
    if (localUsuarioTipoPersonal.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUsuarioTipoPersonal, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UsuarioTipoPersonalPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UsuarioTipoPersonalPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UsuarioTipoPersonalPK))
      throw new IllegalArgumentException("arg1");
    UsuarioTipoPersonalPK localUsuarioTipoPersonalPK = (UsuarioTipoPersonalPK)paramObject;
    localUsuarioTipoPersonalPK.idUsuarioTipoPersonal = this.idUsuarioTipoPersonal;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UsuarioTipoPersonalPK))
      throw new IllegalArgumentException("arg1");
    UsuarioTipoPersonalPK localUsuarioTipoPersonalPK = (UsuarioTipoPersonalPK)paramObject;
    this.idUsuarioTipoPersonal = localUsuarioTipoPersonalPK.idUsuarioTipoPersonal;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UsuarioTipoPersonalPK))
      throw new IllegalArgumentException("arg2");
    UsuarioTipoPersonalPK localUsuarioTipoPersonalPK = (UsuarioTipoPersonalPK)paramObject;
    localUsuarioTipoPersonalPK.idUsuarioTipoPersonal = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UsuarioTipoPersonalPK))
      throw new IllegalArgumentException("arg2");
    UsuarioTipoPersonalPK localUsuarioTipoPersonalPK = (UsuarioTipoPersonalPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localUsuarioTipoPersonalPK.idUsuarioTipoPersonal);
  }

  private static final long jdoGetidUsuarioTipoPersonal(UsuarioTipoPersonal paramUsuarioTipoPersonal)
  {
    return paramUsuarioTipoPersonal.idUsuarioTipoPersonal;
  }

  private static final void jdoSetidUsuarioTipoPersonal(UsuarioTipoPersonal paramUsuarioTipoPersonal, long paramLong)
  {
    StateManager localStateManager = paramUsuarioTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioTipoPersonal.idUsuarioTipoPersonal = paramLong;
      return;
    }
    localStateManager.setLongField(paramUsuarioTipoPersonal, jdoInheritedFieldCount + 0, paramUsuarioTipoPersonal.idUsuarioTipoPersonal, paramLong);
  }

  private static final TipoPersonal jdoGettipoPersonal(UsuarioTipoPersonal paramUsuarioTipoPersonal)
  {
    StateManager localStateManager = paramUsuarioTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramUsuarioTipoPersonal.tipoPersonal;
    if (localStateManager.isLoaded(paramUsuarioTipoPersonal, jdoInheritedFieldCount + 1))
      return paramUsuarioTipoPersonal.tipoPersonal;
    return (TipoPersonal)localStateManager.getObjectField(paramUsuarioTipoPersonal, jdoInheritedFieldCount + 1, paramUsuarioTipoPersonal.tipoPersonal);
  }

  private static final void jdoSettipoPersonal(UsuarioTipoPersonal paramUsuarioTipoPersonal, TipoPersonal paramTipoPersonal)
  {
    StateManager localStateManager = paramUsuarioTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioTipoPersonal.tipoPersonal = paramTipoPersonal;
      return;
    }
    localStateManager.setObjectField(paramUsuarioTipoPersonal, jdoInheritedFieldCount + 1, paramUsuarioTipoPersonal.tipoPersonal, paramTipoPersonal);
  }

  private static final Usuario jdoGetusuario(UsuarioTipoPersonal paramUsuarioTipoPersonal)
  {
    StateManager localStateManager = paramUsuarioTipoPersonal.jdoStateManager;
    if (localStateManager == null)
      return paramUsuarioTipoPersonal.usuario;
    if (localStateManager.isLoaded(paramUsuarioTipoPersonal, jdoInheritedFieldCount + 2))
      return paramUsuarioTipoPersonal.usuario;
    return (Usuario)localStateManager.getObjectField(paramUsuarioTipoPersonal, jdoInheritedFieldCount + 2, paramUsuarioTipoPersonal.usuario);
  }

  private static final void jdoSetusuario(UsuarioTipoPersonal paramUsuarioTipoPersonal, Usuario paramUsuario)
  {
    StateManager localStateManager = paramUsuarioTipoPersonal.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioTipoPersonal.usuario = paramUsuario;
      return;
    }
    localStateManager.setObjectField(paramUsuarioTipoPersonal, jdoInheritedFieldCount + 2, paramUsuarioTipoPersonal.usuario, paramUsuario);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}