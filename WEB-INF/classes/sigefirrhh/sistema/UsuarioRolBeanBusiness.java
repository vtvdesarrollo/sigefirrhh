package sigefirrhh.sistema;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;

public class UsuarioRolBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addUsuarioRol(UsuarioRol usuarioRol)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    UsuarioRol usuarioRolNew = 
      (UsuarioRol)BeanUtils.cloneBean(
      usuarioRol);

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (usuarioRolNew.getUsuario() != null) {
      usuarioRolNew.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        usuarioRolNew.getUsuario().getIdUsuario()));
    }

    RolBeanBusiness rolBeanBusiness = new RolBeanBusiness();

    if (usuarioRolNew.getRol() != null) {
      usuarioRolNew.setRol(
        rolBeanBusiness.findRolById(
        usuarioRolNew.getRol().getIdRol()));
    }
    pm.makePersistent(usuarioRolNew);
  }

  public void updateUsuarioRol(UsuarioRol usuarioRol) throws Exception
  {
    UsuarioRol usuarioRolModify = 
      findUsuarioRolById(usuarioRol.getIdUsuarioRol());

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (usuarioRol.getUsuario() != null) {
      usuarioRol.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        usuarioRol.getUsuario().getIdUsuario()));
    }

    RolBeanBusiness rolBeanBusiness = new RolBeanBusiness();

    if (usuarioRol.getRol() != null) {
      usuarioRol.setRol(
        rolBeanBusiness.findRolById(
        usuarioRol.getRol().getIdRol()));
    }

    BeanUtils.copyProperties(usuarioRolModify, usuarioRol);
  }

  public void deleteUsuarioRol(UsuarioRol usuarioRol) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    UsuarioRol usuarioRolDelete = 
      findUsuarioRolById(usuarioRol.getIdUsuarioRol());
    pm.deletePersistent(usuarioRolDelete);
  }

  public UsuarioRol findUsuarioRolById(long idUsuarioRol) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUsuarioRol == pIdUsuarioRol";
    Query query = pm.newQuery(UsuarioRol.class, filter);

    query.declareParameters("long pIdUsuarioRol");

    parameters.put("pIdUsuarioRol", new Long(idUsuarioRol));

    Collection colUsuarioRol = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUsuarioRol.iterator();
    return (UsuarioRol)iterator.next();
  }

  public Collection findUsuarioRolAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent usuarioRolExtent = pm.getExtent(
      UsuarioRol.class, true);
    Query query = pm.newQuery(usuarioRolExtent);
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUsuario(long idUsuario)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "usuario.idUsuario == pIdUsuario";

    Query query = pm.newQuery(UsuarioRol.class, filter);

    query.declareParameters("long pIdUsuario");
    HashMap parameters = new HashMap();

    parameters.put("pIdUsuario", new Long(idUsuario));

    Collection colUsuarioRol = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUsuarioRol);

    return colUsuarioRol;
  }

  public Collection findByRol(long idRol)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "rol.idRol == pIdRol";

    Query query = pm.newQuery(UsuarioRol.class, filter);

    query.declareParameters("long pIdRol");
    HashMap parameters = new HashMap();

    parameters.put("pIdRol", new Long(idRol));

    Collection colUsuarioRol = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUsuarioRol);

    return colUsuarioRol;
  }
}