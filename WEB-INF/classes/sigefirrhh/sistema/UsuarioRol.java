package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class UsuarioRol
  implements Serializable, PersistenceCapable
{
  private long idUsuarioRol;
  private Usuario usuario;
  private Rol rol;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idUsuarioRol", "rol", "usuario" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("sigefirrhh.sistema.Rol"), sunjdo$classForName$("sigefirrhh.sistema.Usuario") };
  private static final byte[] jdoFieldFlags = { 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetusuario(this).getNombres() + " " + jdoGetusuario(this).getApellidos() + " - " + jdoGetrol(this).getNombre();
  }

  public long getIdUsuarioRol()
  {
    return jdoGetidUsuarioRol(this);
  }

  public Rol getRol()
  {
    return jdoGetrol(this);
  }

  public Usuario getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setIdUsuarioRol(long l)
  {
    jdoSetidUsuarioRol(this, l);
  }

  public void setRol(Rol rol)
  {
    jdoSetrol(this, rol);
  }

  public void setUsuario(Usuario usuario)
  {
    jdoSetusuario(this, usuario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.UsuarioRol"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UsuarioRol());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UsuarioRol localUsuarioRol = new UsuarioRol();
    localUsuarioRol.jdoFlags = 1;
    localUsuarioRol.jdoStateManager = paramStateManager;
    return localUsuarioRol;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UsuarioRol localUsuarioRol = new UsuarioRol();
    localUsuarioRol.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUsuarioRol.jdoFlags = 1;
    localUsuarioRol.jdoStateManager = paramStateManager;
    return localUsuarioRol;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUsuarioRol);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.rol);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUsuarioRol = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.rol = ((Rol)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = ((Usuario)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UsuarioRol paramUsuarioRol, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUsuarioRol == null)
        throw new IllegalArgumentException("arg1");
      this.idUsuarioRol = paramUsuarioRol.idUsuarioRol;
      return;
    case 1:
      if (paramUsuarioRol == null)
        throw new IllegalArgumentException("arg1");
      this.rol = paramUsuarioRol.rol;
      return;
    case 2:
      if (paramUsuarioRol == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramUsuarioRol.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UsuarioRol))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UsuarioRol localUsuarioRol = (UsuarioRol)paramObject;
    if (localUsuarioRol.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUsuarioRol, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UsuarioRolPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UsuarioRolPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UsuarioRolPK))
      throw new IllegalArgumentException("arg1");
    UsuarioRolPK localUsuarioRolPK = (UsuarioRolPK)paramObject;
    localUsuarioRolPK.idUsuarioRol = this.idUsuarioRol;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UsuarioRolPK))
      throw new IllegalArgumentException("arg1");
    UsuarioRolPK localUsuarioRolPK = (UsuarioRolPK)paramObject;
    this.idUsuarioRol = localUsuarioRolPK.idUsuarioRol;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UsuarioRolPK))
      throw new IllegalArgumentException("arg2");
    UsuarioRolPK localUsuarioRolPK = (UsuarioRolPK)paramObject;
    localUsuarioRolPK.idUsuarioRol = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UsuarioRolPK))
      throw new IllegalArgumentException("arg2");
    UsuarioRolPK localUsuarioRolPK = (UsuarioRolPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localUsuarioRolPK.idUsuarioRol);
  }

  private static final long jdoGetidUsuarioRol(UsuarioRol paramUsuarioRol)
  {
    return paramUsuarioRol.idUsuarioRol;
  }

  private static final void jdoSetidUsuarioRol(UsuarioRol paramUsuarioRol, long paramLong)
  {
    StateManager localStateManager = paramUsuarioRol.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioRol.idUsuarioRol = paramLong;
      return;
    }
    localStateManager.setLongField(paramUsuarioRol, jdoInheritedFieldCount + 0, paramUsuarioRol.idUsuarioRol, paramLong);
  }

  private static final Rol jdoGetrol(UsuarioRol paramUsuarioRol)
  {
    StateManager localStateManager = paramUsuarioRol.jdoStateManager;
    if (localStateManager == null)
      return paramUsuarioRol.rol;
    if (localStateManager.isLoaded(paramUsuarioRol, jdoInheritedFieldCount + 1))
      return paramUsuarioRol.rol;
    return (Rol)localStateManager.getObjectField(paramUsuarioRol, jdoInheritedFieldCount + 1, paramUsuarioRol.rol);
  }

  private static final void jdoSetrol(UsuarioRol paramUsuarioRol, Rol paramRol)
  {
    StateManager localStateManager = paramUsuarioRol.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioRol.rol = paramRol;
      return;
    }
    localStateManager.setObjectField(paramUsuarioRol, jdoInheritedFieldCount + 1, paramUsuarioRol.rol, paramRol);
  }

  private static final Usuario jdoGetusuario(UsuarioRol paramUsuarioRol)
  {
    StateManager localStateManager = paramUsuarioRol.jdoStateManager;
    if (localStateManager == null)
      return paramUsuarioRol.usuario;
    if (localStateManager.isLoaded(paramUsuarioRol, jdoInheritedFieldCount + 2))
      return paramUsuarioRol.usuario;
    return (Usuario)localStateManager.getObjectField(paramUsuarioRol, jdoInheritedFieldCount + 2, paramUsuarioRol.usuario);
  }

  private static final void jdoSetusuario(UsuarioRol paramUsuarioRol, Usuario paramUsuario)
  {
    StateManager localStateManager = paramUsuarioRol.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioRol.usuario = paramUsuario;
      return;
    }
    localStateManager.setObjectField(paramUsuarioRol, jdoInheritedFieldCount + 2, paramUsuarioRol.usuario, paramUsuario);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}