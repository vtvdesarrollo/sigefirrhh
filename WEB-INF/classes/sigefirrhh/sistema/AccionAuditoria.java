package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class AccionAuditoria
  implements Serializable, PersistenceCapable
{
  private long idAccionAuditoria;
  private String codigoAccionAuditoria;
  private String nombre;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "codigoAccionAuditoria", "idAccionAuditoria", "nombre" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String getCodigoAccionAuditoria()
  {
    return jdoGetcodigoAccionAuditoria(this);
  }

  public long getIdAccionAuditoria()
  {
    return jdoGetidAccionAuditoria(this);
  }

  public String getNombre()
  {
    return jdoGetnombre(this);
  }

  public void setCodigoAccionAuditoria(String string)
  {
    jdoSetcodigoAccionAuditoria(this, string);
  }

  public void setIdAccionAuditoria(long l)
  {
    jdoSetidAccionAuditoria(this, l);
  }

  public void setNombre(String string)
  {
    jdoSetnombre(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.AccionAuditoria"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new AccionAuditoria());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    AccionAuditoria localAccionAuditoria = new AccionAuditoria();
    localAccionAuditoria.jdoFlags = 1;
    localAccionAuditoria.jdoStateManager = paramStateManager;
    return localAccionAuditoria;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    AccionAuditoria localAccionAuditoria = new AccionAuditoria();
    localAccionAuditoria.jdoCopyKeyFieldsFromObjectId(paramObject);
    localAccionAuditoria.jdoFlags = 1;
    localAccionAuditoria.jdoStateManager = paramStateManager;
    return localAccionAuditoria;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoAccionAuditoria);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idAccionAuditoria);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombre);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoAccionAuditoria = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idAccionAuditoria = localStateManager.replacingLongField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombre = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(AccionAuditoria paramAccionAuditoria, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramAccionAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.codigoAccionAuditoria = paramAccionAuditoria.codigoAccionAuditoria;
      return;
    case 1:
      if (paramAccionAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.idAccionAuditoria = paramAccionAuditoria.idAccionAuditoria;
      return;
    case 2:
      if (paramAccionAuditoria == null)
        throw new IllegalArgumentException("arg1");
      this.nombre = paramAccionAuditoria.nombre;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof AccionAuditoria))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    AccionAuditoria localAccionAuditoria = (AccionAuditoria)paramObject;
    if (localAccionAuditoria.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localAccionAuditoria, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new AccionAuditoriaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new AccionAuditoriaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AccionAuditoriaPK))
      throw new IllegalArgumentException("arg1");
    AccionAuditoriaPK localAccionAuditoriaPK = (AccionAuditoriaPK)paramObject;
    localAccionAuditoriaPK.idAccionAuditoria = this.idAccionAuditoria;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof AccionAuditoriaPK))
      throw new IllegalArgumentException("arg1");
    AccionAuditoriaPK localAccionAuditoriaPK = (AccionAuditoriaPK)paramObject;
    this.idAccionAuditoria = localAccionAuditoriaPK.idAccionAuditoria;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AccionAuditoriaPK))
      throw new IllegalArgumentException("arg2");
    AccionAuditoriaPK localAccionAuditoriaPK = (AccionAuditoriaPK)paramObject;
    localAccionAuditoriaPK.idAccionAuditoria = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof AccionAuditoriaPK))
      throw new IllegalArgumentException("arg2");
    AccionAuditoriaPK localAccionAuditoriaPK = (AccionAuditoriaPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 1, localAccionAuditoriaPK.idAccionAuditoria);
  }

  private static final String jdoGetcodigoAccionAuditoria(AccionAuditoria paramAccionAuditoria)
  {
    if (paramAccionAuditoria.jdoFlags <= 0)
      return paramAccionAuditoria.codigoAccionAuditoria;
    StateManager localStateManager = paramAccionAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAccionAuditoria.codigoAccionAuditoria;
    if (localStateManager.isLoaded(paramAccionAuditoria, jdoInheritedFieldCount + 0))
      return paramAccionAuditoria.codigoAccionAuditoria;
    return localStateManager.getStringField(paramAccionAuditoria, jdoInheritedFieldCount + 0, paramAccionAuditoria.codigoAccionAuditoria);
  }

  private static final void jdoSetcodigoAccionAuditoria(AccionAuditoria paramAccionAuditoria, String paramString)
  {
    if (paramAccionAuditoria.jdoFlags == 0)
    {
      paramAccionAuditoria.codigoAccionAuditoria = paramString;
      return;
    }
    StateManager localStateManager = paramAccionAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionAuditoria.codigoAccionAuditoria = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionAuditoria, jdoInheritedFieldCount + 0, paramAccionAuditoria.codigoAccionAuditoria, paramString);
  }

  private static final long jdoGetidAccionAuditoria(AccionAuditoria paramAccionAuditoria)
  {
    return paramAccionAuditoria.idAccionAuditoria;
  }

  private static final void jdoSetidAccionAuditoria(AccionAuditoria paramAccionAuditoria, long paramLong)
  {
    StateManager localStateManager = paramAccionAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionAuditoria.idAccionAuditoria = paramLong;
      return;
    }
    localStateManager.setLongField(paramAccionAuditoria, jdoInheritedFieldCount + 1, paramAccionAuditoria.idAccionAuditoria, paramLong);
  }

  private static final String jdoGetnombre(AccionAuditoria paramAccionAuditoria)
  {
    if (paramAccionAuditoria.jdoFlags <= 0)
      return paramAccionAuditoria.nombre;
    StateManager localStateManager = paramAccionAuditoria.jdoStateManager;
    if (localStateManager == null)
      return paramAccionAuditoria.nombre;
    if (localStateManager.isLoaded(paramAccionAuditoria, jdoInheritedFieldCount + 2))
      return paramAccionAuditoria.nombre;
    return localStateManager.getStringField(paramAccionAuditoria, jdoInheritedFieldCount + 2, paramAccionAuditoria.nombre);
  }

  private static final void jdoSetnombre(AccionAuditoria paramAccionAuditoria, String paramString)
  {
    if (paramAccionAuditoria.jdoFlags == 0)
    {
      paramAccionAuditoria.nombre = paramString;
      return;
    }
    StateManager localStateManager = paramAccionAuditoria.jdoStateManager;
    if (localStateManager == null)
    {
      paramAccionAuditoria.nombre = paramString;
      return;
    }
    localStateManager.setStringField(paramAccionAuditoria, jdoInheritedFieldCount + 2, paramAccionAuditoria.nombre, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}