package sigefirrhh.sistema;

import java.io.Serializable;

public class UsuarioOrganismoPK
  implements Serializable
{
  public long idUsuarioOrganismo;

  public UsuarioOrganismoPK()
  {
  }

  public UsuarioOrganismoPK(long idUsuarioOrganismo)
  {
    this.idUsuarioOrganismo = idUsuarioOrganismo;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((UsuarioOrganismoPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(UsuarioOrganismoPK thatPK)
  {
    return 
      this.idUsuarioOrganismo == thatPK.idUsuarioOrganismo;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idUsuarioOrganismo)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idUsuarioOrganismo);
  }
}