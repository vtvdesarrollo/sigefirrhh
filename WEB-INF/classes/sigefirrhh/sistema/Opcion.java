package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class Opcion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_TIPO;
  private long idOpcion;
  private String codigoOpcion;
  private String descripcion;
  private String ruta;
  private String tipo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "codigoOpcion", "descripcion", "idOpcion", "ruta", "tipo" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 24, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.Opcion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Opcion());

    LISTA_TIPO = 
      new LinkedHashMap();

    LISTA_TIPO.put("D", "DATOS");
    LISTA_TIPO.put("R", "REPORTE");
    LISTA_TIPO.put("P", "PROCESOS");
  }

  public String toString()
  {
    return jdoGetcodigoOpcion(this) + " - " + jdoGetdescripcion(this) + " - " + jdoGettipo(this);
  }

  public String getCodigoOpcion()
  {
    return jdoGetcodigoOpcion(this);
  }

  public long getIdOpcion()
  {
    return jdoGetidOpcion(this);
  }

  public void setCodigoOpcion(String string)
  {
    jdoSetcodigoOpcion(this, string);
  }

  public void setIdOpcion(long l)
  {
    jdoSetidOpcion(this, l);
  }

  public String getDescripcion()
  {
    return jdoGetdescripcion(this);
  }

  public String getRuta() {
    return jdoGetruta(this);
  }

  public String getTipo() {
    return jdoGettipo(this);
  }

  public void setDescripcion(String string) {
    jdoSetdescripcion(this, string);
  }

  public void setRuta(String string) {
    jdoSetruta(this, string);
  }

  public void setTipo(String string) {
    jdoSettipo(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 5;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Opcion localOpcion = new Opcion();
    localOpcion.jdoFlags = 1;
    localOpcion.jdoStateManager = paramStateManager;
    return localOpcion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Opcion localOpcion = new Opcion();
    localOpcion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localOpcion.jdoFlags = 1;
    localOpcion.jdoStateManager = paramStateManager;
    return localOpcion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.codigoOpcion);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcion);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idOpcion);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.ruta);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipo);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.codigoOpcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcion = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idOpcion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ruta = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipo = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Opcion paramOpcion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.codigoOpcion = paramOpcion.codigoOpcion;
      return;
    case 1:
      if (paramOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.descripcion = paramOpcion.descripcion;
      return;
    case 2:
      if (paramOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.idOpcion = paramOpcion.idOpcion;
      return;
    case 3:
      if (paramOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.ruta = paramOpcion.ruta;
      return;
    case 4:
      if (paramOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.tipo = paramOpcion.tipo;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Opcion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Opcion localOpcion = (Opcion)paramObject;
    if (localOpcion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localOpcion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new OpcionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new OpcionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof OpcionPK))
      throw new IllegalArgumentException("arg1");
    OpcionPK localOpcionPK = (OpcionPK)paramObject;
    localOpcionPK.idOpcion = this.idOpcion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof OpcionPK))
      throw new IllegalArgumentException("arg1");
    OpcionPK localOpcionPK = (OpcionPK)paramObject;
    this.idOpcion = localOpcionPK.idOpcion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof OpcionPK))
      throw new IllegalArgumentException("arg2");
    OpcionPK localOpcionPK = (OpcionPK)paramObject;
    localOpcionPK.idOpcion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 2);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof OpcionPK))
      throw new IllegalArgumentException("arg2");
    OpcionPK localOpcionPK = (OpcionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 2, localOpcionPK.idOpcion);
  }

  private static final String jdoGetcodigoOpcion(Opcion paramOpcion)
  {
    if (paramOpcion.jdoFlags <= 0)
      return paramOpcion.codigoOpcion;
    StateManager localStateManager = paramOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramOpcion.codigoOpcion;
    if (localStateManager.isLoaded(paramOpcion, jdoInheritedFieldCount + 0))
      return paramOpcion.codigoOpcion;
    return localStateManager.getStringField(paramOpcion, jdoInheritedFieldCount + 0, paramOpcion.codigoOpcion);
  }

  private static final void jdoSetcodigoOpcion(Opcion paramOpcion, String paramString)
  {
    if (paramOpcion.jdoFlags == 0)
    {
      paramOpcion.codigoOpcion = paramString;
      return;
    }
    StateManager localStateManager = paramOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramOpcion.codigoOpcion = paramString;
      return;
    }
    localStateManager.setStringField(paramOpcion, jdoInheritedFieldCount + 0, paramOpcion.codigoOpcion, paramString);
  }

  private static final String jdoGetdescripcion(Opcion paramOpcion)
  {
    if (paramOpcion.jdoFlags <= 0)
      return paramOpcion.descripcion;
    StateManager localStateManager = paramOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramOpcion.descripcion;
    if (localStateManager.isLoaded(paramOpcion, jdoInheritedFieldCount + 1))
      return paramOpcion.descripcion;
    return localStateManager.getStringField(paramOpcion, jdoInheritedFieldCount + 1, paramOpcion.descripcion);
  }

  private static final void jdoSetdescripcion(Opcion paramOpcion, String paramString)
  {
    if (paramOpcion.jdoFlags == 0)
    {
      paramOpcion.descripcion = paramString;
      return;
    }
    StateManager localStateManager = paramOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramOpcion.descripcion = paramString;
      return;
    }
    localStateManager.setStringField(paramOpcion, jdoInheritedFieldCount + 1, paramOpcion.descripcion, paramString);
  }

  private static final long jdoGetidOpcion(Opcion paramOpcion)
  {
    return paramOpcion.idOpcion;
  }

  private static final void jdoSetidOpcion(Opcion paramOpcion, long paramLong)
  {
    StateManager localStateManager = paramOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramOpcion.idOpcion = paramLong;
      return;
    }
    localStateManager.setLongField(paramOpcion, jdoInheritedFieldCount + 2, paramOpcion.idOpcion, paramLong);
  }

  private static final String jdoGetruta(Opcion paramOpcion)
  {
    if (paramOpcion.jdoFlags <= 0)
      return paramOpcion.ruta;
    StateManager localStateManager = paramOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramOpcion.ruta;
    if (localStateManager.isLoaded(paramOpcion, jdoInheritedFieldCount + 3))
      return paramOpcion.ruta;
    return localStateManager.getStringField(paramOpcion, jdoInheritedFieldCount + 3, paramOpcion.ruta);
  }

  private static final void jdoSetruta(Opcion paramOpcion, String paramString)
  {
    if (paramOpcion.jdoFlags == 0)
    {
      paramOpcion.ruta = paramString;
      return;
    }
    StateManager localStateManager = paramOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramOpcion.ruta = paramString;
      return;
    }
    localStateManager.setStringField(paramOpcion, jdoInheritedFieldCount + 3, paramOpcion.ruta, paramString);
  }

  private static final String jdoGettipo(Opcion paramOpcion)
  {
    if (paramOpcion.jdoFlags <= 0)
      return paramOpcion.tipo;
    StateManager localStateManager = paramOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramOpcion.tipo;
    if (localStateManager.isLoaded(paramOpcion, jdoInheritedFieldCount + 4))
      return paramOpcion.tipo;
    return localStateManager.getStringField(paramOpcion, jdoInheritedFieldCount + 4, paramOpcion.tipo);
  }

  private static final void jdoSettipo(Opcion paramOpcion, String paramString)
  {
    if (paramOpcion.jdoFlags == 0)
    {
      paramOpcion.tipo = paramString;
      return;
    }
    StateManager localStateManager = paramOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramOpcion.tipo = paramString;
      return;
    }
    localStateManager.setStringField(paramOpcion, jdoInheritedFieldCount + 4, paramOpcion.tipo, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}