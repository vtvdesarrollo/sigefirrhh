package sigefirrhh.sistema;

import eforserver.presentation.Form;
import eforserver.sequence.IdentityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.EstructuraFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.base.estructura.UnidadFuncional;
import sigefirrhh.login.LoginSession;

public class UsuarioUnidadFuncionalForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(UsuarioUnidadFuncionalForm.class.getName());
  private UsuarioUnidadFuncional usuarioUnidadFuncional;
  private Collection result;
  private boolean editing;
  private boolean adding;
  private boolean deleting;
  private boolean selected;
  private LoginSession login;
  private SistemaFacade sistemaFacade = new SistemaFacade();
  private EstructuraFacade estructuraFacade = new EstructuraFacade();
  private boolean showUsuarioUnidadFuncionalByUsuario;
  private boolean showUsuarioUnidadFuncionalByUnidadFuncional;
  private String findSelectUsuario;
  private String findSelectUnidadFuncional;
  private Collection findColUsuario;
  private Collection findColUnidadFuncional;
  private Collection colUsuario;
  private Collection colUnidadFuncional;
  private String selectUsuario;
  private String selectUnidadFuncional;
  private Object stateResultUsuarioUnidadFuncionalByUsuario = null;

  private Object stateResultUsuarioUnidadFuncionalByUnidadFuncional = null;

  public String getFindSelectUsuario()
  {
    return this.findSelectUsuario;
  }
  public void setFindSelectUsuario(String valUsuario) {
    this.findSelectUsuario = valUsuario;
  }

  public Collection getFindColUsuario() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUsuario.iterator();
    Usuario usuario = null;
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      col.add(new SelectItem(
        String.valueOf(usuario.getIdUsuario()), 
        usuario.toString()));
    }
    return col;
  }
  public String getFindSelectUnidadFuncional() {
    return this.findSelectUnidadFuncional;
  }
  public void setFindSelectUnidadFuncional(String valUnidadFuncional) {
    this.findSelectUnidadFuncional = valUnidadFuncional;
  }

  public Collection getFindColUnidadFuncional() {
    Collection col = new ArrayList();
    Iterator iterator = this.findColUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncional.getIdUnidadFuncional()), 
        unidadFuncional.toString()));
    }
    return col;
  }

  public String getSelectUsuario()
  {
    return this.selectUsuario;
  }
  public void setSelectUsuario(String valUsuario) {
    Iterator iterator = this.colUsuario.iterator();
    Usuario usuario = null;
    this.usuarioUnidadFuncional.setUsuario(null);
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      if (String.valueOf(usuario.getIdUsuario()).equals(
        valUsuario)) {
        this.usuarioUnidadFuncional.setUsuario(
          usuario);
        break;
      }
    }
    this.selectUsuario = valUsuario;
  }
  public String getSelectUnidadFuncional() {
    return this.selectUnidadFuncional;
  }
  public void setSelectUnidadFuncional(String valUnidadFuncional) {
    Iterator iterator = this.colUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    this.usuarioUnidadFuncional.setUnidadFuncional(null);
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      if (String.valueOf(unidadFuncional.getIdUnidadFuncional()).equals(
        valUnidadFuncional)) {
        this.usuarioUnidadFuncional.setUnidadFuncional(
          unidadFuncional);
        break;
      }
    }
    this.selectUnidadFuncional = valUnidadFuncional;
  }
  public Collection getResult() {
    return this.result;
  }

  public UsuarioUnidadFuncional getUsuarioUnidadFuncional() {
    if (this.usuarioUnidadFuncional == null) {
      this.usuarioUnidadFuncional = new UsuarioUnidadFuncional();
    }
    return this.usuarioUnidadFuncional;
  }

  public UsuarioUnidadFuncionalForm()
    throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();
  }

  public Collection getColUsuario()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUsuario.iterator();
    Usuario usuario = null;
    while (iterator.hasNext()) {
      usuario = (Usuario)iterator.next();
      col.add(new SelectItem(
        String.valueOf(usuario.getIdUsuario()), 
        usuario.toString()));
    }
    return col;
  }

  public Collection getColUnidadFuncional()
  {
    Collection col = new ArrayList();
    Iterator iterator = this.colUnidadFuncional.iterator();
    UnidadFuncional unidadFuncional = null;
    while (iterator.hasNext()) {
      unidadFuncional = (UnidadFuncional)iterator.next();
      col.add(new SelectItem(
        String.valueOf(unidadFuncional.getIdUnidadFuncional()), 
        unidadFuncional.toString()));
    }
    return col;
  }

  public void refresh()
  {
    try {
      this.findColUsuario = 
        this.sistemaFacade.findAllUsuario();
      this.findColUnidadFuncional = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());

      this.colUsuario = 
        this.sistemaFacade.findAllUsuario();
      this.colUnidadFuncional = 
        this.estructuraFacade.findUnidadFuncionalByOrganismo(
        this.login.getOrganismo().getIdOrganismo());
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public String findUsuarioUnidadFuncionalByUsuario()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sistemaFacade.findUsuarioUnidadFuncionalByUsuario(Long.valueOf(this.findSelectUsuario).longValue());
      this.showUsuarioUnidadFuncionalByUsuario = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUsuarioUnidadFuncionalByUsuario)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUsuario = null;
    this.findSelectUnidadFuncional = null;

    return null;
  }

  public String findUsuarioUnidadFuncionalByUnidadFuncional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpSession session = 
      (HttpSession)context.getExternalContext().getSession(true);
    session.setAttribute("pageIndex", new Integer(0));
    try {
      resetResult();
      this.result = 
        this.sistemaFacade.findUsuarioUnidadFuncionalByUnidadFuncional(Long.valueOf(this.findSelectUnidadFuncional).longValue());
      this.showUsuarioUnidadFuncionalByUnidadFuncional = 
        ((this.result != null) && (!this.result.isEmpty()));
      if (!this.showUsuarioUnidadFuncionalByUnidadFuncional)
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
    }
    catch (Exception e) {
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No hay información con el criterio solicitado", ""));
      log.error("Excepcion controlada:", e);
    }

    this.findSelectUsuario = null;
    this.findSelectUnidadFuncional = null;

    return null;
  }

  public boolean isShowUsuarioUnidadFuncionalByUsuario() {
    return this.showUsuarioUnidadFuncionalByUsuario;
  }
  public boolean isShowUsuarioUnidadFuncionalByUnidadFuncional() {
    return this.showUsuarioUnidadFuncionalByUnidadFuncional;
  }

  public String selectUsuarioUnidadFuncional()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    Map requestParameterMap = 
      context.getExternalContext().getRequestParameterMap();

    this.selectUsuario = null;
    this.selectUnidadFuncional = null;

    long idUsuarioUnidadFuncional = 
      Long.parseLong((String)requestParameterMap.get("idUsuarioUnidadFuncional"));
    try
    {
      this.usuarioUnidadFuncional = 
        this.sistemaFacade.findUsuarioUnidadFuncionalById(
        idUsuarioUnidadFuncional);
      if (this.usuarioUnidadFuncional.getUsuario() != null) {
        this.selectUsuario = 
          String.valueOf(this.usuarioUnidadFuncional.getUsuario().getIdUsuario());
      }
      if (this.usuarioUnidadFuncional.getUnidadFuncional() != null) {
        this.selectUnidadFuncional = 
          String.valueOf(this.usuarioUnidadFuncional.getUnidadFuncional().getIdUnidadFuncional());
      }

    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
    this.selected = true;

    return null;
  }

  private void resetResult() {
    this.result = null;
    this.selected = false;
    this.usuarioUnidadFuncional = null;
    this.showUsuarioUnidadFuncionalByUsuario = false;
    this.showUsuarioUnidadFuncionalByUnidadFuncional = false;
  }

  public String edit()
  {
    this.editing = true;

    return null;
  }

  public String save() throws Exception
  {
    FacesContext context = FacesContext.getCurrentInstance();

    boolean error = false;

    if (error)
      return null;
    try
    {
      if (this.adding) {
        this.sistemaFacade.addUsuarioUnidadFuncional(
          this.usuarioUnidadFuncional);
        context.addMessage("success_add", new FacesMessage("Se agregó con éxito"));
      } else {
        this.sistemaFacade.updateUsuarioUnidadFuncional(
          this.usuarioUnidadFuncional);
        context.addMessage("success_modify", new FacesMessage("Se modificó con éxito"));
      }
      this.adding = false;
      this.editing = false;
      this.selected = false;
      resetResult();
    }
    catch (Exception e) {
      if (this.adding) {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Registro ya existe ", ""));
      } else {
        log.error("Excepcion controlada:", e);
        context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hubo un error de inconsistencia de datos al modificar ", ""));
      }
    }
    return "cancel";
  }
  public String delete() {
    this.deleting = true;

    return null;
  }
  public String deleteOk() throws Exception {
    FacesContext context = FacesContext.getCurrentInstance();
    try {
      this.sistemaFacade.deleteUsuarioUnidadFuncional(
        this.usuarioUnidadFuncional);
      context.addMessage("success_delete", new FacesMessage("Se eliminó con éxito"));
      this.deleting = false;
      this.selected = false;
      resetResult();
    } catch (Exception e) {
      log.error("Excepcion controlada:", e);
      context.addMessage("error_data", new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se puede eliminar por tener información asociada ", ""));
    }
    return null;
  }

  public String add() {
    this.adding = true;
    this.selected = false;
    this.deleting = false;
    this.editing = true;
    this.usuarioUnidadFuncional = new UsuarioUnidadFuncional();

    this.selectUsuario = null;

    this.selectUnidadFuncional = null;

    IdentityGenerator identityGenerator = IdentityGenerator.getInstance();
    this.usuarioUnidadFuncional.setIdUsuarioUnidadFuncional(identityGenerator.getNextSequenceNumber("sigefirrhh.sistema.UsuarioUnidadFuncional"));

    return null;
  }

  public String abort() {
    this.adding = false;
    this.editing = false;
    this.deleting = false;
    this.selected = false;
    resetResult();
    this.usuarioUnidadFuncional = new UsuarioUnidadFuncional();
    return "cancel";
  }

  public boolean isAdding()
  {
    return this.adding;
  }

  public boolean isDeleting() {
    return this.deleting;
  }

  public boolean isEditing() {
    return this.editing;
  }
  public boolean isShowData() {
    return (this.selected) || (this.adding);
  }

  public LoginSession getLogin() {
    return this.login;
  }
}