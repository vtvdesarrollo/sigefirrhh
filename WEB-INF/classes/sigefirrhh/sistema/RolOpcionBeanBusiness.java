package sigefirrhh.sistema;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

public class RolOpcionBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(RolOpcionBeanBusiness.class.getName());

  public void addRolOpcion(RolOpcion rolOpcion)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    RolOpcion rolOpcionNew = 
      (RolOpcion)BeanUtils.cloneBean(
      rolOpcion);

    RolBeanBusiness rolBeanBusiness = new RolBeanBusiness();

    if (rolOpcionNew.getRol() != null) {
      rolOpcionNew.setRol(
        rolBeanBusiness.findRolById(
        rolOpcionNew.getRol().getIdRol()));
    }

    OpcionBeanBusiness opcionBeanBusiness = new OpcionBeanBusiness();

    if (rolOpcionNew.getOpcion() != null) {
      rolOpcionNew.setOpcion(
        opcionBeanBusiness.findOpcionById(
        rolOpcionNew.getOpcion().getIdOpcion()));
    }
    pm.makePersistent(rolOpcionNew);
  }

  public void updateRolOpcion(RolOpcion rolOpcion) throws Exception
  {
    RolOpcion rolOpcionModify = 
      findRolOpcionById(rolOpcion.getIdRolOpcion());

    RolBeanBusiness rolBeanBusiness = new RolBeanBusiness();

    if (rolOpcion.getRol() != null) {
      rolOpcion.setRol(
        rolBeanBusiness.findRolById(
        rolOpcion.getRol().getIdRol()));
    }

    OpcionBeanBusiness opcionBeanBusiness = new OpcionBeanBusiness();

    if (rolOpcion.getOpcion() != null) {
      rolOpcion.setOpcion(
        opcionBeanBusiness.findOpcionById(
        rolOpcion.getOpcion().getIdOpcion()));
    }

    BeanUtils.copyProperties(rolOpcionModify, rolOpcion);
  }

  public void deleteRolOpcion(RolOpcion rolOpcion) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    RolOpcion rolOpcionDelete = 
      findRolOpcionById(rolOpcion.getIdRolOpcion());
    pm.deletePersistent(rolOpcionDelete);
  }

  public RolOpcion findRolOpcionById(long idRolOpcion) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idRolOpcion == pIdRolOpcion";
    Query query = pm.newQuery(RolOpcion.class, filter);

    query.declareParameters("long pIdRolOpcion");

    parameters.put("pIdRolOpcion", new Long(idRolOpcion));

    Collection colRolOpcion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colRolOpcion.iterator();
    return (RolOpcion)iterator.next();
  }

  public Collection findRolOpcionAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent rolOpcionExtent = pm.getExtent(
      RolOpcion.class, true);

    Query query = pm.newQuery(rolOpcionExtent);
    query.setOrdering("rol.nombre ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByRol(long idRol)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "rol.idRol == pIdRol";

    Query query = pm.newQuery(RolOpcion.class, filter);
    query.setOrdering("opcion.descripcion ascending");
    query.declareParameters("long pIdRol");
    HashMap parameters = new HashMap();

    parameters.put("pIdRol", new Long(idRol));

    Collection colRolOpcion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colRolOpcion);

    return colRolOpcion;
  }

  public Collection findByRolAndOpcion(long idRol, String ruta)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "rol.idRol == pIdRol && opcion.ruta == pRuta";

    Query query = pm.newQuery(RolOpcion.class, filter);

    query.declareParameters("long pIdRol, String pRuta");
    HashMap parameters = new HashMap();

    parameters.put("pIdRol", new Long(idRol));
    parameters.put("pRuta", ruta);

    Collection colRolOpcion = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    this.log.error("size" + colRolOpcion.size());
    pm.makeTransientAll(colRolOpcion);

    return colRolOpcion;
  }
}