package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class RolOpcion
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SI_NO;
  private long idRolOpcion;
  private Rol rol;
  private Opcion opcion;
  private String consultar;
  private String agregar;
  private String modificar;
  private String eliminar;
  private String ejecutar;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "agregar", "consultar", "ejecutar", "eliminar", "idRolOpcion", "modificar", "opcion", "rol" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), Long.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.sistema.Opcion"), sunjdo$classForName$("sigefirrhh.sistema.Rol") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 24, 21, 26, 26 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.RolOpcion"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new RolOpcion());

    LISTA_SI_NO = 
      new LinkedHashMap();

    LISTA_SI_NO.put("S", "SI");
    LISTA_SI_NO.put("N", "NO");
  }

  public RolOpcion()
  {
    jdoSetconsultar(this, "N");

    jdoSetagregar(this, "N");

    jdoSetmodificar(this, "N");

    jdoSeteliminar(this, "N");

    jdoSetejecutar(this, "N");
  }

  public String toString() {
    return jdoGetrol(this).getNombre() + " - " + jdoGetopcion(this).getDescripcion();
  }

  public long getIdRolOpcion()
  {
    return jdoGetidRolOpcion(this);
  }

  public Opcion getOpcion()
  {
    return jdoGetopcion(this);
  }

  public Rol getRol()
  {
    return jdoGetrol(this);
  }

  public void setIdRolOpcion(long l)
  {
    jdoSetidRolOpcion(this, l);
  }

  public void setOpcion(Opcion opcion)
  {
    jdoSetopcion(this, opcion);
  }

  public void setRol(Rol rol)
  {
    jdoSetrol(this, rol);
  }

  public String getAgregar() {
    return jdoGetagregar(this);
  }

  public String getConsultar() {
    return jdoGetconsultar(this);
  }

  public String getEjecutar() {
    return jdoGetejecutar(this);
  }

  public String getEliminar() {
    return jdoGeteliminar(this);
  }

  public String getModificar() {
    return jdoGetmodificar(this);
  }

  public void setAgregar(String string) {
    jdoSetagregar(this, string);
  }

  public void setConsultar(String string) {
    jdoSetconsultar(this, string);
  }

  public void setEjecutar(String string) {
    jdoSetejecutar(this, string);
  }

  public void setEliminar(String string) {
    jdoSeteliminar(this, string);
  }

  public void setModificar(String string) {
    jdoSetmodificar(this, string);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 8;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    RolOpcion localRolOpcion = new RolOpcion();
    localRolOpcion.jdoFlags = 1;
    localRolOpcion.jdoStateManager = paramStateManager;
    return localRolOpcion;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    RolOpcion localRolOpcion = new RolOpcion();
    localRolOpcion.jdoCopyKeyFieldsFromObjectId(paramObject);
    localRolOpcion.jdoFlags = 1;
    localRolOpcion.jdoStateManager = paramStateManager;
    return localRolOpcion;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.agregar);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.consultar);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.ejecutar);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.eliminar);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idRolOpcion);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.modificar);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.opcion);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.rol);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.agregar = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.consultar = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.ejecutar = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.eliminar = localStateManager.replacingStringField(this, paramInt);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idRolOpcion = localStateManager.replacingLongField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.modificar = localStateManager.replacingStringField(this, paramInt);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.opcion = ((Opcion)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.rol = ((Rol)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(RolOpcion paramRolOpcion, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramRolOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.agregar = paramRolOpcion.agregar;
      return;
    case 1:
      if (paramRolOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.consultar = paramRolOpcion.consultar;
      return;
    case 2:
      if (paramRolOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.ejecutar = paramRolOpcion.ejecutar;
      return;
    case 3:
      if (paramRolOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.eliminar = paramRolOpcion.eliminar;
      return;
    case 4:
      if (paramRolOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.idRolOpcion = paramRolOpcion.idRolOpcion;
      return;
    case 5:
      if (paramRolOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.modificar = paramRolOpcion.modificar;
      return;
    case 6:
      if (paramRolOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.opcion = paramRolOpcion.opcion;
      return;
    case 7:
      if (paramRolOpcion == null)
        throw new IllegalArgumentException("arg1");
      this.rol = paramRolOpcion.rol;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof RolOpcion))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    RolOpcion localRolOpcion = (RolOpcion)paramObject;
    if (localRolOpcion.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localRolOpcion, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new RolOpcionPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new RolOpcionPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RolOpcionPK))
      throw new IllegalArgumentException("arg1");
    RolOpcionPK localRolOpcionPK = (RolOpcionPK)paramObject;
    localRolOpcionPK.idRolOpcion = this.idRolOpcion;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof RolOpcionPK))
      throw new IllegalArgumentException("arg1");
    RolOpcionPK localRolOpcionPK = (RolOpcionPK)paramObject;
    this.idRolOpcion = localRolOpcionPK.idRolOpcion;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RolOpcionPK))
      throw new IllegalArgumentException("arg2");
    RolOpcionPK localRolOpcionPK = (RolOpcionPK)paramObject;
    localRolOpcionPK.idRolOpcion = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 4);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof RolOpcionPK))
      throw new IllegalArgumentException("arg2");
    RolOpcionPK localRolOpcionPK = (RolOpcionPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 4, localRolOpcionPK.idRolOpcion);
  }

  private static final String jdoGetagregar(RolOpcion paramRolOpcion)
  {
    if (paramRolOpcion.jdoFlags <= 0)
      return paramRolOpcion.agregar;
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramRolOpcion.agregar;
    if (localStateManager.isLoaded(paramRolOpcion, jdoInheritedFieldCount + 0))
      return paramRolOpcion.agregar;
    return localStateManager.getStringField(paramRolOpcion, jdoInheritedFieldCount + 0, paramRolOpcion.agregar);
  }

  private static final void jdoSetagregar(RolOpcion paramRolOpcion, String paramString)
  {
    if (paramRolOpcion.jdoFlags == 0)
    {
      paramRolOpcion.agregar = paramString;
      return;
    }
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRolOpcion.agregar = paramString;
      return;
    }
    localStateManager.setStringField(paramRolOpcion, jdoInheritedFieldCount + 0, paramRolOpcion.agregar, paramString);
  }

  private static final String jdoGetconsultar(RolOpcion paramRolOpcion)
  {
    if (paramRolOpcion.jdoFlags <= 0)
      return paramRolOpcion.consultar;
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramRolOpcion.consultar;
    if (localStateManager.isLoaded(paramRolOpcion, jdoInheritedFieldCount + 1))
      return paramRolOpcion.consultar;
    return localStateManager.getStringField(paramRolOpcion, jdoInheritedFieldCount + 1, paramRolOpcion.consultar);
  }

  private static final void jdoSetconsultar(RolOpcion paramRolOpcion, String paramString)
  {
    if (paramRolOpcion.jdoFlags == 0)
    {
      paramRolOpcion.consultar = paramString;
      return;
    }
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRolOpcion.consultar = paramString;
      return;
    }
    localStateManager.setStringField(paramRolOpcion, jdoInheritedFieldCount + 1, paramRolOpcion.consultar, paramString);
  }

  private static final String jdoGetejecutar(RolOpcion paramRolOpcion)
  {
    if (paramRolOpcion.jdoFlags <= 0)
      return paramRolOpcion.ejecutar;
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramRolOpcion.ejecutar;
    if (localStateManager.isLoaded(paramRolOpcion, jdoInheritedFieldCount + 2))
      return paramRolOpcion.ejecutar;
    return localStateManager.getStringField(paramRolOpcion, jdoInheritedFieldCount + 2, paramRolOpcion.ejecutar);
  }

  private static final void jdoSetejecutar(RolOpcion paramRolOpcion, String paramString)
  {
    if (paramRolOpcion.jdoFlags == 0)
    {
      paramRolOpcion.ejecutar = paramString;
      return;
    }
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRolOpcion.ejecutar = paramString;
      return;
    }
    localStateManager.setStringField(paramRolOpcion, jdoInheritedFieldCount + 2, paramRolOpcion.ejecutar, paramString);
  }

  private static final String jdoGeteliminar(RolOpcion paramRolOpcion)
  {
    if (paramRolOpcion.jdoFlags <= 0)
      return paramRolOpcion.eliminar;
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramRolOpcion.eliminar;
    if (localStateManager.isLoaded(paramRolOpcion, jdoInheritedFieldCount + 3))
      return paramRolOpcion.eliminar;
    return localStateManager.getStringField(paramRolOpcion, jdoInheritedFieldCount + 3, paramRolOpcion.eliminar);
  }

  private static final void jdoSeteliminar(RolOpcion paramRolOpcion, String paramString)
  {
    if (paramRolOpcion.jdoFlags == 0)
    {
      paramRolOpcion.eliminar = paramString;
      return;
    }
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRolOpcion.eliminar = paramString;
      return;
    }
    localStateManager.setStringField(paramRolOpcion, jdoInheritedFieldCount + 3, paramRolOpcion.eliminar, paramString);
  }

  private static final long jdoGetidRolOpcion(RolOpcion paramRolOpcion)
  {
    return paramRolOpcion.idRolOpcion;
  }

  private static final void jdoSetidRolOpcion(RolOpcion paramRolOpcion, long paramLong)
  {
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRolOpcion.idRolOpcion = paramLong;
      return;
    }
    localStateManager.setLongField(paramRolOpcion, jdoInheritedFieldCount + 4, paramRolOpcion.idRolOpcion, paramLong);
  }

  private static final String jdoGetmodificar(RolOpcion paramRolOpcion)
  {
    if (paramRolOpcion.jdoFlags <= 0)
      return paramRolOpcion.modificar;
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramRolOpcion.modificar;
    if (localStateManager.isLoaded(paramRolOpcion, jdoInheritedFieldCount + 5))
      return paramRolOpcion.modificar;
    return localStateManager.getStringField(paramRolOpcion, jdoInheritedFieldCount + 5, paramRolOpcion.modificar);
  }

  private static final void jdoSetmodificar(RolOpcion paramRolOpcion, String paramString)
  {
    if (paramRolOpcion.jdoFlags == 0)
    {
      paramRolOpcion.modificar = paramString;
      return;
    }
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRolOpcion.modificar = paramString;
      return;
    }
    localStateManager.setStringField(paramRolOpcion, jdoInheritedFieldCount + 5, paramRolOpcion.modificar, paramString);
  }

  private static final Opcion jdoGetopcion(RolOpcion paramRolOpcion)
  {
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramRolOpcion.opcion;
    if (localStateManager.isLoaded(paramRolOpcion, jdoInheritedFieldCount + 6))
      return paramRolOpcion.opcion;
    return (Opcion)localStateManager.getObjectField(paramRolOpcion, jdoInheritedFieldCount + 6, paramRolOpcion.opcion);
  }

  private static final void jdoSetopcion(RolOpcion paramRolOpcion, Opcion paramOpcion)
  {
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRolOpcion.opcion = paramOpcion;
      return;
    }
    localStateManager.setObjectField(paramRolOpcion, jdoInheritedFieldCount + 6, paramRolOpcion.opcion, paramOpcion);
  }

  private static final Rol jdoGetrol(RolOpcion paramRolOpcion)
  {
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
      return paramRolOpcion.rol;
    if (localStateManager.isLoaded(paramRolOpcion, jdoInheritedFieldCount + 7))
      return paramRolOpcion.rol;
    return (Rol)localStateManager.getObjectField(paramRolOpcion, jdoInheritedFieldCount + 7, paramRolOpcion.rol);
  }

  private static final void jdoSetrol(RolOpcion paramRolOpcion, Rol paramRol)
  {
    StateManager localStateManager = paramRolOpcion.jdoStateManager;
    if (localStateManager == null)
    {
      paramRolOpcion.rol = paramRol;
      return;
    }
    localStateManager.setObjectField(paramRolOpcion, jdoInheritedFieldCount + 7, paramRolOpcion.rol, paramRol);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}