package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Region;

public class Usuario
  implements Serializable, PersistenceCapable
{
  protected static final Map LISTA_SINO;
  private long idUsuario;
  private String usuario;
  private String password;
  private int cedula;
  private String apellidos;
  private String nombres;
  private Region region;
  private String telefono;
  private String administrador;
  private Date cambioPassword;
  private String activo;
  private int intentos;
  private long idOrganismo;
  private Date fechaVence;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0; private static final String[] jdoFieldNames = { "activo", "administrador", "apellidos", "cambioPassword", "cedula", "fechaVence", "idOrganismo", "idUsuario", "intentos", "nombres", "password", "region", "telefono", "usuario" }; private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.util.Date"), Integer.TYPE, sunjdo$classForName$("java.util.Date"), Long.TYPE, Long.TYPE, Integer.TYPE, sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("sigefirrhh.base.estructura.Region"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") }; private static final byte[] jdoFieldFlags = { 21, 21, 21, 21, 21, 21, 21, 24, 21, 21, 21, 26, 21, 21 }; private static final Class jdoPersistenceCapableSuperclass = null;

  static { JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.Usuario"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new Usuario());

    LISTA_SINO = 
      new LinkedHashMap();

    LISTA_SINO.put("S", "SI");
    LISTA_SINO.put("N", "NO");
  }

  public Usuario()
  {
    jdoSetadministrador(this, "N");

    jdoSetactivo(this, "S");

    jdoSetintentos(this, 0);
  }

  public int getIntentos()
  {
    return jdoGetintentos(this);
  }

  public void setIntentos(int intentos)
  {
    jdoSetintentos(this, intentos);
  }

  public void setFechaVence(Date fecha)
  {
    jdoSetfechaVence(this, fecha);
  }

  public Date getFechaVence()
  {
    return jdoGetfechaVence(this);
  }

  public String toString() {
    return jdoGetusuario(this) + "  -  " + 
      jdoGetapellidos(this) + "  -  " + 
      jdoGetnombres(this) + " - " + jdoGetcedula(this);
  }

  public String getAdministrador()
  {
    return jdoGetadministrador(this);
  }

  public String getApellidos()
  {
    return jdoGetapellidos(this);
  }

  public int getCedula()
  {
    return jdoGetcedula(this);
  }

  public String getUsuario()
  {
    return jdoGetusuario(this);
  }

  public long getIdUsuario()
  {
    return jdoGetidUsuario(this);
  }

  public String getNombres()
  {
    return jdoGetnombres(this);
  }

  public String getPassword()
  {
    return jdoGetpassword(this);
  }

  public Date getCambioPassword() {
    return jdoGetcambioPassword(this);
  }

  public void setAdministrador(String string)
  {
    jdoSetadministrador(this, string);
  }

  public void setApellidos(String string)
  {
    jdoSetapellidos(this, string);
  }

  public void setCedula(int i)
  {
    jdoSetcedula(this, i);
  }

  public void setUsuario(String string)
  {
    jdoSetusuario(this, string);
  }

  public void setIdUsuario(long l)
  {
    jdoSetidUsuario(this, l);
  }

  public void setNombres(String string)
  {
    jdoSetnombres(this, string);
  }

  public void setPassword(String string)
  {
    jdoSetpassword(this, string);
  }

  public String getActivo()
  {
    return jdoGetactivo(this);
  }

  public String getTelefono()
  {
    return jdoGettelefono(this);
  }

  public void setActivo(String string)
  {
    jdoSetactivo(this, string);
  }

  public void setTelefono(String string)
  {
    jdoSettelefono(this, string);
  }

  public Region getRegion()
  {
    return jdoGetregion(this);
  }

  public void setRegion(Region region)
  {
    jdoSetregion(this, region);
  }

  public long getIdOrganismo() {
    return jdoGetidOrganismo(this);
  }

  public void setIdOrganismo(long idOrganismo) {
    jdoSetidOrganismo(this, idOrganismo);
  }

  public void setCambioPassword(Date fecha) {
    jdoSetcambioPassword(this, fecha);
  }

  public boolean cambiaPassword()
  {
    Calendar cal = Calendar.getInstance();

    cal.setTime(new Date());
    int minuendMonth = cal.get(2);
    int minuendYear = cal.get(1);
    int minuedDay = cal.get(5);
    cal.setTime(jdoGetcambioPassword(this));
    int subtrahendMonth = cal.get(2);
    int subtrahendYear = cal.get(1);
    int subtraDay = cal.get(5);

    int ajuste = 0;
    if (minuedDay < subtraDay) {
      ajuste = -1;
    }

    int diferencia = (minuendYear - subtrahendYear) * cal.getMaximum(2) + (minuendMonth - subtrahendMonth) + ajuste;

    if (diferencia >= 3) {
      return true;
    }
    return false;
  }

  public boolean cambiaPasswordDias(int diasDuracionClave)
  {
    Calendar c = Calendar.getInstance();
    Date hoy = c.getTime();

    c.setTime(jdoGetcambioPassword(this));

    c.add(5, diasDuracionClave);

    Date fechaVenceClave = c.getTime();

    if (hoy.after(fechaVenceClave))
    {
      return true;
    }

    return false;
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 14;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    Usuario localUsuario = new Usuario();
    localUsuario.jdoFlags = 1;
    localUsuario.jdoStateManager = paramStateManager;
    return localUsuario;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    Usuario localUsuario = new Usuario();
    localUsuario.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUsuario.jdoFlags = 1;
    localUsuario.jdoStateManager = paramStateManager;
    return localUsuario;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.activo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.administrador);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.apellidos);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.cambioPassword);
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.cedula);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.fechaVence);
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idOrganismo);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUsuario);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedIntField(this, paramInt, this.intentos);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombres);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.password);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.region);
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.telefono);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.activo = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.administrador = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.apellidos = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cambioPassword = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 4:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.cedula = localStateManager.replacingIntField(this, paramInt);
      return;
    case 5:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.fechaVence = ((Date)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 6:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idOrganismo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 7:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUsuario = localStateManager.replacingLongField(this, paramInt);
      return;
    case 8:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.intentos = localStateManager.replacingIntField(this, paramInt);
      return;
    case 9:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombres = localStateManager.replacingStringField(this, paramInt);
      return;
    case 10:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.password = localStateManager.replacingStringField(this, paramInt);
      return;
    case 11:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.region = ((Region)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 12:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.telefono = localStateManager.replacingStringField(this, paramInt);
      return;
    case 13:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(Usuario paramUsuario, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.activo = paramUsuario.activo;
      return;
    case 1:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.administrador = paramUsuario.administrador;
      return;
    case 2:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.apellidos = paramUsuario.apellidos;
      return;
    case 3:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.cambioPassword = paramUsuario.cambioPassword;
      return;
    case 4:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.cedula = paramUsuario.cedula;
      return;
    case 5:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.fechaVence = paramUsuario.fechaVence;
      return;
    case 6:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.idOrganismo = paramUsuario.idOrganismo;
      return;
    case 7:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.idUsuario = paramUsuario.idUsuario;
      return;
    case 8:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.intentos = paramUsuario.intentos;
      return;
    case 9:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.nombres = paramUsuario.nombres;
      return;
    case 10:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.password = paramUsuario.password;
      return;
    case 11:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.region = paramUsuario.region;
      return;
    case 12:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.telefono = paramUsuario.telefono;
      return;
    case 13:
      if (paramUsuario == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramUsuario.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof Usuario))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    Usuario localUsuario = (Usuario)paramObject;
    if (localUsuario.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUsuario, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UsuarioPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UsuarioPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UsuarioPK))
      throw new IllegalArgumentException("arg1");
    UsuarioPK localUsuarioPK = (UsuarioPK)paramObject;
    localUsuarioPK.idUsuario = this.idUsuario;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UsuarioPK))
      throw new IllegalArgumentException("arg1");
    UsuarioPK localUsuarioPK = (UsuarioPK)paramObject;
    this.idUsuario = localUsuarioPK.idUsuario;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UsuarioPK))
      throw new IllegalArgumentException("arg2");
    UsuarioPK localUsuarioPK = (UsuarioPK)paramObject;
    localUsuarioPK.idUsuario = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 7);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UsuarioPK))
      throw new IllegalArgumentException("arg2");
    UsuarioPK localUsuarioPK = (UsuarioPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 7, localUsuarioPK.idUsuario);
  }

  private static final String jdoGetactivo(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.activo;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.activo;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 0))
      return paramUsuario.activo;
    return localStateManager.getStringField(paramUsuario, jdoInheritedFieldCount + 0, paramUsuario.activo);
  }

  private static final void jdoSetactivo(Usuario paramUsuario, String paramString)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.activo = paramString;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.activo = paramString;
      return;
    }
    localStateManager.setStringField(paramUsuario, jdoInheritedFieldCount + 0, paramUsuario.activo, paramString);
  }

  private static final String jdoGetadministrador(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.administrador;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.administrador;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 1))
      return paramUsuario.administrador;
    return localStateManager.getStringField(paramUsuario, jdoInheritedFieldCount + 1, paramUsuario.administrador);
  }

  private static final void jdoSetadministrador(Usuario paramUsuario, String paramString)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.administrador = paramString;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.administrador = paramString;
      return;
    }
    localStateManager.setStringField(paramUsuario, jdoInheritedFieldCount + 1, paramUsuario.administrador, paramString);
  }

  private static final String jdoGetapellidos(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.apellidos;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.apellidos;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 2))
      return paramUsuario.apellidos;
    return localStateManager.getStringField(paramUsuario, jdoInheritedFieldCount + 2, paramUsuario.apellidos);
  }

  private static final void jdoSetapellidos(Usuario paramUsuario, String paramString)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.apellidos = paramString;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.apellidos = paramString;
      return;
    }
    localStateManager.setStringField(paramUsuario, jdoInheritedFieldCount + 2, paramUsuario.apellidos, paramString);
  }

  private static final Date jdoGetcambioPassword(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.cambioPassword;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.cambioPassword;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 3))
      return paramUsuario.cambioPassword;
    return (Date)localStateManager.getObjectField(paramUsuario, jdoInheritedFieldCount + 3, paramUsuario.cambioPassword);
  }

  private static final void jdoSetcambioPassword(Usuario paramUsuario, Date paramDate)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.cambioPassword = paramDate;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.cambioPassword = paramDate;
      return;
    }
    localStateManager.setObjectField(paramUsuario, jdoInheritedFieldCount + 3, paramUsuario.cambioPassword, paramDate);
  }

  private static final int jdoGetcedula(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.cedula;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.cedula;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 4))
      return paramUsuario.cedula;
    return localStateManager.getIntField(paramUsuario, jdoInheritedFieldCount + 4, paramUsuario.cedula);
  }

  private static final void jdoSetcedula(Usuario paramUsuario, int paramInt)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.cedula = paramInt;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.cedula = paramInt;
      return;
    }
    localStateManager.setIntField(paramUsuario, jdoInheritedFieldCount + 4, paramUsuario.cedula, paramInt);
  }

  private static final Date jdoGetfechaVence(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.fechaVence;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.fechaVence;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 5))
      return paramUsuario.fechaVence;
    return (Date)localStateManager.getObjectField(paramUsuario, jdoInheritedFieldCount + 5, paramUsuario.fechaVence);
  }

  private static final void jdoSetfechaVence(Usuario paramUsuario, Date paramDate)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.fechaVence = paramDate;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.fechaVence = paramDate;
      return;
    }
    localStateManager.setObjectField(paramUsuario, jdoInheritedFieldCount + 5, paramUsuario.fechaVence, paramDate);
  }

  private static final long jdoGetidOrganismo(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.idOrganismo;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.idOrganismo;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 6))
      return paramUsuario.idOrganismo;
    return localStateManager.getLongField(paramUsuario, jdoInheritedFieldCount + 6, paramUsuario.idOrganismo);
  }

  private static final void jdoSetidOrganismo(Usuario paramUsuario, long paramLong)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.idOrganismo = paramLong;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.idOrganismo = paramLong;
      return;
    }
    localStateManager.setLongField(paramUsuario, jdoInheritedFieldCount + 6, paramUsuario.idOrganismo, paramLong);
  }

  private static final long jdoGetidUsuario(Usuario paramUsuario)
  {
    return paramUsuario.idUsuario;
  }

  private static final void jdoSetidUsuario(Usuario paramUsuario, long paramLong)
  {
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.idUsuario = paramLong;
      return;
    }
    localStateManager.setLongField(paramUsuario, jdoInheritedFieldCount + 7, paramUsuario.idUsuario, paramLong);
  }

  private static final int jdoGetintentos(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.intentos;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.intentos;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 8))
      return paramUsuario.intentos;
    return localStateManager.getIntField(paramUsuario, jdoInheritedFieldCount + 8, paramUsuario.intentos);
  }

  private static final void jdoSetintentos(Usuario paramUsuario, int paramInt)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.intentos = paramInt;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.intentos = paramInt;
      return;
    }
    localStateManager.setIntField(paramUsuario, jdoInheritedFieldCount + 8, paramUsuario.intentos, paramInt);
  }

  private static final String jdoGetnombres(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.nombres;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.nombres;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 9))
      return paramUsuario.nombres;
    return localStateManager.getStringField(paramUsuario, jdoInheritedFieldCount + 9, paramUsuario.nombres);
  }

  private static final void jdoSetnombres(Usuario paramUsuario, String paramString)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.nombres = paramString;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.nombres = paramString;
      return;
    }
    localStateManager.setStringField(paramUsuario, jdoInheritedFieldCount + 9, paramUsuario.nombres, paramString);
  }

  private static final String jdoGetpassword(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.password;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.password;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 10))
      return paramUsuario.password;
    return localStateManager.getStringField(paramUsuario, jdoInheritedFieldCount + 10, paramUsuario.password);
  }

  private static final void jdoSetpassword(Usuario paramUsuario, String paramString)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.password = paramString;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.password = paramString;
      return;
    }
    localStateManager.setStringField(paramUsuario, jdoInheritedFieldCount + 10, paramUsuario.password, paramString);
  }

  private static final Region jdoGetregion(Usuario paramUsuario)
  {
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.region;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 11))
      return paramUsuario.region;
    return (Region)localStateManager.getObjectField(paramUsuario, jdoInheritedFieldCount + 11, paramUsuario.region);
  }

  private static final void jdoSetregion(Usuario paramUsuario, Region paramRegion)
  {
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.region = paramRegion;
      return;
    }
    localStateManager.setObjectField(paramUsuario, jdoInheritedFieldCount + 11, paramUsuario.region, paramRegion);
  }

  private static final String jdoGettelefono(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.telefono;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.telefono;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 12))
      return paramUsuario.telefono;
    return localStateManager.getStringField(paramUsuario, jdoInheritedFieldCount + 12, paramUsuario.telefono);
  }

  private static final void jdoSettelefono(Usuario paramUsuario, String paramString)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.telefono = paramString;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.telefono = paramString;
      return;
    }
    localStateManager.setStringField(paramUsuario, jdoInheritedFieldCount + 12, paramUsuario.telefono, paramString);
  }

  private static final String jdoGetusuario(Usuario paramUsuario)
  {
    if (paramUsuario.jdoFlags <= 0)
      return paramUsuario.usuario;
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
      return paramUsuario.usuario;
    if (localStateManager.isLoaded(paramUsuario, jdoInheritedFieldCount + 13))
      return paramUsuario.usuario;
    return localStateManager.getStringField(paramUsuario, jdoInheritedFieldCount + 13, paramUsuario.usuario);
  }

  private static final void jdoSetusuario(Usuario paramUsuario, String paramString)
  {
    if (paramUsuario.jdoFlags == 0)
    {
      paramUsuario.usuario = paramString;
      return;
    }
    StateManager localStateManager = paramUsuario.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuario.usuario = paramString;
      return;
    }
    localStateManager.setStringField(paramUsuario, jdoInheritedFieldCount + 13, paramUsuario.usuario, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}