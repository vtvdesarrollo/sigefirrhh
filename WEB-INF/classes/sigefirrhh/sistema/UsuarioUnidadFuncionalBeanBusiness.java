package sigefirrhh.sistema;

import eforserver.business.AbstractBeanBusiness;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import sigefirrhh.base.estructura.UnidadFuncional;
import sigefirrhh.base.estructura.UnidadFuncionalBeanBusiness;

public class UsuarioUnidadFuncionalBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  public void addUsuarioUnidadFuncional(UsuarioUnidadFuncional usuarioUnidadFuncional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    UsuarioUnidadFuncional usuarioUnidadFuncionalNew = 
      (UsuarioUnidadFuncional)BeanUtils.cloneBean(
      usuarioUnidadFuncional);

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (usuarioUnidadFuncionalNew.getUsuario() != null) {
      usuarioUnidadFuncionalNew.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        usuarioUnidadFuncionalNew.getUsuario().getIdUsuario()));
    }

    UnidadFuncionalBeanBusiness unidadFuncionalBeanBusiness = new UnidadFuncionalBeanBusiness();

    if (usuarioUnidadFuncionalNew.getUnidadFuncional() != null) {
      usuarioUnidadFuncionalNew.setUnidadFuncional(
        unidadFuncionalBeanBusiness.findUnidadFuncionalById(
        usuarioUnidadFuncionalNew.getUnidadFuncional().getIdUnidadFuncional()));
    }
    pm.makePersistent(usuarioUnidadFuncionalNew);
  }

  public void updateUsuarioUnidadFuncional(UsuarioUnidadFuncional usuarioUnidadFuncional) throws Exception
  {
    UsuarioUnidadFuncional usuarioUnidadFuncionalModify = 
      findUsuarioUnidadFuncionalById(usuarioUnidadFuncional.getIdUsuarioUnidadFuncional());

    UsuarioBeanBusiness usuarioBeanBusiness = new UsuarioBeanBusiness();

    if (usuarioUnidadFuncional.getUsuario() != null) {
      usuarioUnidadFuncional.setUsuario(
        usuarioBeanBusiness.findUsuarioById(
        usuarioUnidadFuncional.getUsuario().getIdUsuario()));
    }

    UnidadFuncionalBeanBusiness unidadFuncionalBeanBusiness = new UnidadFuncionalBeanBusiness();

    if (usuarioUnidadFuncional.getUnidadFuncional() != null) {
      usuarioUnidadFuncional.setUnidadFuncional(
        unidadFuncionalBeanBusiness.findUnidadFuncionalById(
        usuarioUnidadFuncional.getUnidadFuncional().getIdUnidadFuncional()));
    }

    BeanUtils.copyProperties(usuarioUnidadFuncionalModify, usuarioUnidadFuncional);
  }

  public void deleteUsuarioUnidadFuncional(UsuarioUnidadFuncional usuarioUnidadFuncional) throws Exception {
    PersistenceManager pm = PMThread.getPM();
    UsuarioUnidadFuncional usuarioUnidadFuncionalDelete = 
      findUsuarioUnidadFuncionalById(usuarioUnidadFuncional.getIdUsuarioUnidadFuncional());
    pm.deletePersistent(usuarioUnidadFuncionalDelete);
  }

  public UsuarioUnidadFuncional findUsuarioUnidadFuncionalById(long idUsuarioUnidadFuncional) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUsuarioUnidadFuncional == pIdUsuarioUnidadFuncional";
    Query query = pm.newQuery(UsuarioUnidadFuncional.class, filter);

    query.declareParameters("long pIdUsuarioUnidadFuncional");

    parameters.put("pIdUsuarioUnidadFuncional", new Long(idUsuarioUnidadFuncional));

    Collection colUsuarioUnidadFuncional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUsuarioUnidadFuncional.iterator();
    return (UsuarioUnidadFuncional)iterator.next();
  }

  public Collection findUsuarioUnidadFuncionalAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent usuarioUnidadFuncionalExtent = pm.getExtent(
      UsuarioUnidadFuncional.class, true);
    Query query = pm.newQuery(usuarioUnidadFuncionalExtent);
    query.setOrdering("usuario.apellidos ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUsuario(long idUsuario)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "usuario.idUsuario == pIdUsuario";

    Query query = pm.newQuery(UsuarioUnidadFuncional.class, filter);

    query.declareParameters("long pIdUsuario");
    HashMap parameters = new HashMap();

    parameters.put("pIdUsuario", new Long(idUsuario));

    query.setOrdering("usuario.apellidos ascending");

    Collection colUsuarioUnidadFuncional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUsuarioUnidadFuncional);

    return colUsuarioUnidadFuncional;
  }

  public Collection findByUnidadFuncional(long idUnidadFuncional)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "unidadFuncional.idUnidadFuncional == pIdUnidadFuncional";

    Query query = pm.newQuery(UsuarioUnidadFuncional.class, filter);

    query.declareParameters("long pIdUnidadFuncional");
    HashMap parameters = new HashMap();

    parameters.put("pIdUnidadFuncional", new Long(idUnidadFuncional));

    query.setOrdering("usuario.apellidos ascending");

    Collection colUsuarioUnidadFuncional = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUsuarioUnidadFuncional);

    return colUsuarioUnidadFuncional;
  }
}