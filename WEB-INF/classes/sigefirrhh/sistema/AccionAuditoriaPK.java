package sigefirrhh.sistema;

import java.io.Serializable;

public class AccionAuditoriaPK
  implements Serializable
{
  public long idAccionAuditoria;

  public AccionAuditoriaPK()
  {
  }

  public AccionAuditoriaPK(long idAccionAuditoria)
  {
    this.idAccionAuditoria = idAccionAuditoria;
  }

  public boolean equals(Object thatPK) {
    try {
      return equals((AccionAuditoriaPK)thatPK); } catch (ClassCastException e) {
    }
    return false;
  }

  public boolean equals(AccionAuditoriaPK thatPK)
  {
    return 
      this.idAccionAuditoria == thatPK.idAccionAuditoria;
  }

  public int hashCode()
  {
    return 
      String.valueOf(this.idAccionAuditoria)
      .hashCode();
  }
  public String toString() {
    return 
      String.valueOf(this.idAccionAuditoria);
  }
}