package sigefirrhh.sistema;

import eforserver.presentation.ListUtil;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;

public class ReportAdministracionIdForm
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportAdministracionIdForm.class.getName());
  private int tipoReporte;
  private int reportId;
  private String reportName;
  private String selectTipoPersonal;
  private Collection listTipoPersonal;
  private DefinicionesFacade definicionesFacade;
  private LoginSession login;

  public ReportAdministracionIdForm()
  {
    this.reportName = "gruponominaid";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    this.definicionesFacade = new DefinicionesFacade();
    this.selectTipoPersonal = null;
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportAdministracionIdForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  private void cambiarNombreAReporte()
  {
    if (this.tipoReporte == 1)
      this.reportName = "gruponominaid";
    else if (this.tipoReporte == 2)
      this.reportName = "tipospersonalid";
    else if (this.tipoReporte == 3)
      this.reportName = "ctpconceptoid";
    else if (this.tipoReporte == 4)
      this.reportName = "trabpercedsid";
    else if (this.tipoReporte == 5)
      this.reportName = "trabperalfsid";
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findAllTipoPersonal();
    }
    catch (Exception e) {
      log.error(e.getCause());
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String runReport() {
    FacesContext context = FacesContext.getCurrentInstance();

    Map parameters = new Hashtable();
    parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
    parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));
    parameters.put("id_tipo_personal", Long.valueOf(String.valueOf(this.selectTipoPersonal)));

    if (this.tipoReporte == 1)
      this.reportName = "gruponominaid";
    else if (this.tipoReporte == 2)
      this.reportName = "tipospersonalid";
    else if (this.tipoReporte == 3)
      this.reportName = "ctpconceptoid";
    else if (this.tipoReporte == 4)
      this.reportName = "trabpercedsid";
    else if (this.tipoReporte == 5) {
      this.reportName = "trabperalfsid";
    }
    JasperForWeb report = new JasperForWeb();

    report.setReportName(this.reportName);
    report.setParameters(parameters);
    report.setPath(
      ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/sistema");

    report.start();
    ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

    this.reportId = JasperForWeb.newReportId(this.reportId);

    return null;
  }

  public Collection getListTipoPersonal()
  {
    return ListUtil.convertCollectionToSelectItemsWithId(
      this.listTipoPersonal, "sigefirrhh.base.definiciones.TipoPersonal");
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String string) {
    this.selectTipoPersonal = string;
  }
  public int getTipoReporte() {
    return this.tipoReporte;
  }
  public void setTipoReporte(int i) {
    this.tipoReporte = i;
  }
  public LoginSession getLogin() {
    return this.login;
  }
  public int getReportId() {
    return this.reportId;
  }
  public void setReportId(int i) {
    this.reportId = i;
  }
  public String getReportName() {
    return this.reportName;
  }
  public void setReportName(String string) {
    this.reportName = string;
  }
}