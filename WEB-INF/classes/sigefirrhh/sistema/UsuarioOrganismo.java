package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;
import sigefirrhh.base.estructura.Organismo;

public class UsuarioOrganismo
  implements Serializable, PersistenceCapable
{
  private long idUsuarioOrganismo;
  private Usuario usuario;
  private Organismo organismo;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "idUsuarioOrganismo", "organismo", "usuario" };
  private static final Class[] jdoFieldTypes = { Long.TYPE, sunjdo$classForName$("sigefirrhh.base.estructura.Organismo"), sunjdo$classForName$("sigefirrhh.sistema.Usuario") };
  private static final byte[] jdoFieldFlags = { 24, 26, 26 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String toString()
  {
    return jdoGetusuario(this).toString() + " - " + jdoGetorganismo(this).getNombreOrganismo();
  }

  public long getIdUsuarioOrganismo()
  {
    return jdoGetidUsuarioOrganismo(this);
  }

  public Organismo getOrganismo()
  {
    return jdoGetorganismo(this);
  }

  public Usuario getUsuario()
  {
    return jdoGetusuario(this);
  }

  public void setIdUsuarioOrganismo(long l)
  {
    jdoSetidUsuarioOrganismo(this, l);
  }

  public void setOrganismo(Organismo organismo)
  {
    jdoSetorganismo(this, organismo);
  }

  public void setUsuario(Usuario usuario)
  {
    jdoSetusuario(this, usuario);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 3;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.UsuarioOrganismo"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UsuarioOrganismo());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    UsuarioOrganismo localUsuarioOrganismo = new UsuarioOrganismo();
    localUsuarioOrganismo.jdoFlags = 1;
    localUsuarioOrganismo.jdoStateManager = paramStateManager;
    return localUsuarioOrganismo;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    UsuarioOrganismo localUsuarioOrganismo = new UsuarioOrganismo();
    localUsuarioOrganismo.jdoCopyKeyFieldsFromObjectId(paramObject);
    localUsuarioOrganismo.jdoFlags = 1;
    localUsuarioOrganismo.jdoStateManager = paramStateManager;
    return localUsuarioOrganismo;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedLongField(this, paramInt, this.idUsuarioOrganismo);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.organismo);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedObjectField(this, paramInt, this.usuario);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.idUsuarioOrganismo = localStateManager.replacingLongField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.organismo = ((Organismo)localStateManager.replacingObjectField(this, paramInt));
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.usuario = ((Usuario)localStateManager.replacingObjectField(this, paramInt));
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(UsuarioOrganismo paramUsuarioOrganismo, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramUsuarioOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.idUsuarioOrganismo = paramUsuarioOrganismo.idUsuarioOrganismo;
      return;
    case 1:
      if (paramUsuarioOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.organismo = paramUsuarioOrganismo.organismo;
      return;
    case 2:
      if (paramUsuarioOrganismo == null)
        throw new IllegalArgumentException("arg1");
      this.usuario = paramUsuarioOrganismo.usuario;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof UsuarioOrganismo))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    UsuarioOrganismo localUsuarioOrganismo = (UsuarioOrganismo)paramObject;
    if (localUsuarioOrganismo.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localUsuarioOrganismo, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new UsuarioOrganismoPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new UsuarioOrganismoPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UsuarioOrganismoPK))
      throw new IllegalArgumentException("arg1");
    UsuarioOrganismoPK localUsuarioOrganismoPK = (UsuarioOrganismoPK)paramObject;
    localUsuarioOrganismoPK.idUsuarioOrganismo = this.idUsuarioOrganismo;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof UsuarioOrganismoPK))
      throw new IllegalArgumentException("arg1");
    UsuarioOrganismoPK localUsuarioOrganismoPK = (UsuarioOrganismoPK)paramObject;
    this.idUsuarioOrganismo = localUsuarioOrganismoPK.idUsuarioOrganismo;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UsuarioOrganismoPK))
      throw new IllegalArgumentException("arg2");
    UsuarioOrganismoPK localUsuarioOrganismoPK = (UsuarioOrganismoPK)paramObject;
    localUsuarioOrganismoPK.idUsuarioOrganismo = paramObjectIdFieldSupplier.fetchLongField(jdoInheritedFieldCount + 0);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof UsuarioOrganismoPK))
      throw new IllegalArgumentException("arg2");
    UsuarioOrganismoPK localUsuarioOrganismoPK = (UsuarioOrganismoPK)paramObject;
    paramObjectIdFieldConsumer.storeLongField(jdoInheritedFieldCount + 0, localUsuarioOrganismoPK.idUsuarioOrganismo);
  }

  private static final long jdoGetidUsuarioOrganismo(UsuarioOrganismo paramUsuarioOrganismo)
  {
    return paramUsuarioOrganismo.idUsuarioOrganismo;
  }

  private static final void jdoSetidUsuarioOrganismo(UsuarioOrganismo paramUsuarioOrganismo, long paramLong)
  {
    StateManager localStateManager = paramUsuarioOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioOrganismo.idUsuarioOrganismo = paramLong;
      return;
    }
    localStateManager.setLongField(paramUsuarioOrganismo, jdoInheritedFieldCount + 0, paramUsuarioOrganismo.idUsuarioOrganismo, paramLong);
  }

  private static final Organismo jdoGetorganismo(UsuarioOrganismo paramUsuarioOrganismo)
  {
    StateManager localStateManager = paramUsuarioOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramUsuarioOrganismo.organismo;
    if (localStateManager.isLoaded(paramUsuarioOrganismo, jdoInheritedFieldCount + 1))
      return paramUsuarioOrganismo.organismo;
    return (Organismo)localStateManager.getObjectField(paramUsuarioOrganismo, jdoInheritedFieldCount + 1, paramUsuarioOrganismo.organismo);
  }

  private static final void jdoSetorganismo(UsuarioOrganismo paramUsuarioOrganismo, Organismo paramOrganismo)
  {
    StateManager localStateManager = paramUsuarioOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioOrganismo.organismo = paramOrganismo;
      return;
    }
    localStateManager.setObjectField(paramUsuarioOrganismo, jdoInheritedFieldCount + 1, paramUsuarioOrganismo.organismo, paramOrganismo);
  }

  private static final Usuario jdoGetusuario(UsuarioOrganismo paramUsuarioOrganismo)
  {
    StateManager localStateManager = paramUsuarioOrganismo.jdoStateManager;
    if (localStateManager == null)
      return paramUsuarioOrganismo.usuario;
    if (localStateManager.isLoaded(paramUsuarioOrganismo, jdoInheritedFieldCount + 2))
      return paramUsuarioOrganismo.usuario;
    return (Usuario)localStateManager.getObjectField(paramUsuarioOrganismo, jdoInheritedFieldCount + 2, paramUsuarioOrganismo.usuario);
  }

  private static final void jdoSetusuario(UsuarioOrganismo paramUsuarioOrganismo, Usuario paramUsuario)
  {
    StateManager localStateManager = paramUsuarioOrganismo.jdoStateManager;
    if (localStateManager == null)
    {
      paramUsuarioOrganismo.usuario = paramUsuario;
      return;
    }
    localStateManager.setObjectField(paramUsuarioOrganismo, jdoInheritedFieldCount + 2, paramUsuarioOrganismo.usuario, paramUsuario);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}