package sigefirrhh.sistema;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.jdo.PersistenceManager;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldConsumer;
import javax.jdo.spi.PersistenceCapable.ObjectIdFieldSupplier;
import javax.jdo.spi.StateManager;

public class ParametroSistema
  implements Serializable, PersistenceCapable
{
  private static final long serialVersionUID = 3L;
  private String nombreParametro;
  private String valorParametro;
  private String tipoParametro;
  private String descripcionParametro;
  protected transient StateManager jdoStateManager;
  protected transient byte jdoFlags;
  private static final int jdoInheritedFieldCount = 0;
  private static final String[] jdoFieldNames = { "descripcionParametro", "nombreParametro", "tipoParametro", "valorParametro" };
  private static final Class[] jdoFieldTypes = { sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String"), sunjdo$classForName$("java.lang.String") };
  private static final byte[] jdoFieldFlags = { 21, 24, 21, 21 };
  private static final Class jdoPersistenceCapableSuperclass = null;

  public String getNombreParametro()
  {
    return jdoGetnombreParametro(this);
  }

  public void setNombreParametro(String nombreParametro) {
    jdoSetnombreParametro(this, nombreParametro);
  }

  public String getValorParametro() {
    return jdoGetvalorParametro(this);
  }

  public void setValorParametro(String valorParametro) {
    jdoSetvalorParametro(this, valorParametro);
  }

  public String getTipoParametro() {
    return jdoGettipoParametro(this);
  }

  public void setTipoParametro(String tipoParametro) {
    jdoSettipoParametro(this, tipoParametro);
  }

  public String getDescripcionParametro() {
    return jdoGetdescripcionParametro(this);
  }

  public void setDescripcionParametro(String descripcionParametro) {
    jdoSetdescripcionParametro(this, descripcionParametro);
  }

  public final void jdoReplaceFlags()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      this.jdoFlags = localStateManager.replacingFlags(this);
  }

  public final boolean jdoIsPersistent()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isPersistent(this);
    return false;
  }

  public final boolean jdoIsTransactional()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isTransactional(this);
    return false;
  }

  public final boolean jdoIsNew()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isNew(this);
    return false;
  }

  public final boolean jdoIsDeleted()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDeleted(this);
    return false;
  }

  public final boolean jdoIsDirty()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.isDirty(this);
    return false;
  }

  public final void jdoMakeDirty(String paramString)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.makeDirty(this, paramString);
  }

  protected final void jdoPreSerialize()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      localStateManager.preSerialize(this);
  }

  public final PersistenceManager jdoGetPersistenceManager()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getPersistenceManager(this);
    return null;
  }

  public final Object jdoGetObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getObjectId(this);
    return null;
  }

  public final Object jdoGetTransactionalObjectId()
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
      return localStateManager.getTransactionalObjectId(this);
    return null;
  }

  public final synchronized void jdoReplaceStateManager(StateManager paramStateManager)
  {
    StateManager localStateManager = this.jdoStateManager;
    if (localStateManager != null)
    {
      this.jdoStateManager = localStateManager.replacingStateManager(this, paramStateManager);
      return;
    }
    JDOImplHelper.checkAuthorizedStateManager(paramStateManager);
    this.jdoStateManager = paramStateManager;
    this.jdoFlags = 1;
  }

  public final void jdoProvideFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoProvideField(paramArrayOfInt[j]);
  }

  public final void jdoReplaceFields(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg1");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoReplaceField(paramArrayOfInt[j]);
  }

  protected static final Class sunjdo$classForName$(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
    }
  }

  protected static int jdoGetManagedFieldCount()
  {
    return jdoInheritedFieldCount + 4;
  }

  static
  {
    JDOImplHelper.registerClass(sunjdo$classForName$("sigefirrhh.sistema.ParametroSistema"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ParametroSistema());
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager)
  {
    ParametroSistema localParametroSistema = new ParametroSistema();
    localParametroSistema.jdoFlags = 1;
    localParametroSistema.jdoStateManager = paramStateManager;
    return localParametroSistema;
  }

  public PersistenceCapable jdoNewInstance(StateManager paramStateManager, Object paramObject)
  {
    ParametroSistema localParametroSistema = new ParametroSistema();
    localParametroSistema.jdoCopyKeyFieldsFromObjectId(paramObject);
    localParametroSistema.jdoFlags = 1;
    localParametroSistema.jdoStateManager = paramStateManager;
    return localParametroSistema;
  }

  public void jdoProvideField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.descripcionParametro);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.nombreParametro);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.tipoParametro);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      localStateManager.providedStringField(this, paramInt, this.valorParametro);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  public void jdoReplaceField(int paramInt)
  {
    StateManager localStateManager = this.jdoStateManager;
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.descripcionParametro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 1:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.nombreParametro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 2:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.tipoParametro = localStateManager.replacingStringField(this, paramInt);
      return;
    case 3:
      if (localStateManager == null)
        throw new IllegalStateException("arg0.jdoStateManager");
      this.valorParametro = localStateManager.replacingStringField(this, paramInt);
      return;
    }
    throw new IllegalArgumentException("arg1");
  }

  protected final void jdoCopyField(ParametroSistema paramParametroSistema, int paramInt)
  {
    switch (paramInt - jdoInheritedFieldCount)
    {
    case 0:
      if (paramParametroSistema == null)
        throw new IllegalArgumentException("arg1");
      this.descripcionParametro = paramParametroSistema.descripcionParametro;
      return;
    case 1:
      if (paramParametroSistema == null)
        throw new IllegalArgumentException("arg1");
      this.nombreParametro = paramParametroSistema.nombreParametro;
      return;
    case 2:
      if (paramParametroSistema == null)
        throw new IllegalArgumentException("arg1");
      this.tipoParametro = paramParametroSistema.tipoParametro;
      return;
    case 3:
      if (paramParametroSistema == null)
        throw new IllegalArgumentException("arg1");
      this.valorParametro = paramParametroSistema.valorParametro;
      return;
    }
    throw new IllegalArgumentException("arg2");
  }

  public void jdoCopyFields(Object paramObject, int[] paramArrayOfInt)
  {
    if (this.jdoStateManager == null)
      throw new IllegalStateException("arg0.jdoStateManager");
    if (!(paramObject instanceof ParametroSistema))
      throw new IllegalArgumentException("arg1");
    if (paramArrayOfInt == null)
      throw new IllegalArgumentException("arg2");
    ParametroSistema localParametroSistema = (ParametroSistema)paramObject;
    if (localParametroSistema.jdoStateManager != this.jdoStateManager)
      throw new IllegalArgumentException("arg1.jdoStateManager");
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      jdoCopyField(localParametroSistema, paramArrayOfInt[j]);
  }

  public Object jdoNewObjectIdInstance()
  {
    return new ParametroSistemaPK();
  }

  public Object jdoNewObjectIdInstance(String paramString)
  {
    return new ParametroSistemaPK(paramString);
  }

  public void jdoCopyKeyFieldsToObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroSistemaPK))
      throw new IllegalArgumentException("arg1");
    ParametroSistemaPK localParametroSistemaPK = (ParametroSistemaPK)paramObject;
    localParametroSistemaPK.nombreParametro = this.nombreParametro;
  }

  protected void jdoCopyKeyFieldsFromObjectId(Object paramObject)
  {
    if (!(paramObject instanceof ParametroSistemaPK))
      throw new IllegalArgumentException("arg1");
    ParametroSistemaPK localParametroSistemaPK = (ParametroSistemaPK)paramObject;
    this.nombreParametro = localParametroSistemaPK.nombreParametro;
  }

  public void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier paramObjectIdFieldSupplier, Object paramObject)
  {
    if (paramObjectIdFieldSupplier == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroSistemaPK))
      throw new IllegalArgumentException("arg2");
    ParametroSistemaPK localParametroSistemaPK = (ParametroSistemaPK)paramObject;
    localParametroSistemaPK.nombreParametro = paramObjectIdFieldSupplier.fetchStringField(jdoInheritedFieldCount + 1);
  }

  public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer paramObjectIdFieldConsumer, Object paramObject)
  {
    if (paramObjectIdFieldConsumer == null)
      throw new IllegalArgumentException("arg1");
    if (!(paramObject instanceof ParametroSistemaPK))
      throw new IllegalArgumentException("arg2");
    ParametroSistemaPK localParametroSistemaPK = (ParametroSistemaPK)paramObject;
    paramObjectIdFieldConsumer.storeStringField(jdoInheritedFieldCount + 1, localParametroSistemaPK.nombreParametro);
  }

  private static final String jdoGetdescripcionParametro(ParametroSistema paramParametroSistema)
  {
    if (paramParametroSistema.jdoFlags <= 0)
      return paramParametroSistema.descripcionParametro;
    StateManager localStateManager = paramParametroSistema.jdoStateManager;
    if (localStateManager == null)
      return paramParametroSistema.descripcionParametro;
    if (localStateManager.isLoaded(paramParametroSistema, jdoInheritedFieldCount + 0))
      return paramParametroSistema.descripcionParametro;
    return localStateManager.getStringField(paramParametroSistema, jdoInheritedFieldCount + 0, paramParametroSistema.descripcionParametro);
  }

  private static final void jdoSetdescripcionParametro(ParametroSistema paramParametroSistema, String paramString)
  {
    if (paramParametroSistema.jdoFlags == 0)
    {
      paramParametroSistema.descripcionParametro = paramString;
      return;
    }
    StateManager localStateManager = paramParametroSistema.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroSistema.descripcionParametro = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroSistema, jdoInheritedFieldCount + 0, paramParametroSistema.descripcionParametro, paramString);
  }

  private static final String jdoGetnombreParametro(ParametroSistema paramParametroSistema)
  {
    return paramParametroSistema.nombreParametro;
  }

  private static final void jdoSetnombreParametro(ParametroSistema paramParametroSistema, String paramString)
  {
    StateManager localStateManager = paramParametroSistema.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroSistema.nombreParametro = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroSistema, jdoInheritedFieldCount + 1, paramParametroSistema.nombreParametro, paramString);
  }

  private static final String jdoGettipoParametro(ParametroSistema paramParametroSistema)
  {
    if (paramParametroSistema.jdoFlags <= 0)
      return paramParametroSistema.tipoParametro;
    StateManager localStateManager = paramParametroSistema.jdoStateManager;
    if (localStateManager == null)
      return paramParametroSistema.tipoParametro;
    if (localStateManager.isLoaded(paramParametroSistema, jdoInheritedFieldCount + 2))
      return paramParametroSistema.tipoParametro;
    return localStateManager.getStringField(paramParametroSistema, jdoInheritedFieldCount + 2, paramParametroSistema.tipoParametro);
  }

  private static final void jdoSettipoParametro(ParametroSistema paramParametroSistema, String paramString)
  {
    if (paramParametroSistema.jdoFlags == 0)
    {
      paramParametroSistema.tipoParametro = paramString;
      return;
    }
    StateManager localStateManager = paramParametroSistema.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroSistema.tipoParametro = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroSistema, jdoInheritedFieldCount + 2, paramParametroSistema.tipoParametro, paramString);
  }

  private static final String jdoGetvalorParametro(ParametroSistema paramParametroSistema)
  {
    if (paramParametroSistema.jdoFlags <= 0)
      return paramParametroSistema.valorParametro;
    StateManager localStateManager = paramParametroSistema.jdoStateManager;
    if (localStateManager == null)
      return paramParametroSistema.valorParametro;
    if (localStateManager.isLoaded(paramParametroSistema, jdoInheritedFieldCount + 3))
      return paramParametroSistema.valorParametro;
    return localStateManager.getStringField(paramParametroSistema, jdoInheritedFieldCount + 3, paramParametroSistema.valorParametro);
  }

  private static final void jdoSetvalorParametro(ParametroSistema paramParametroSistema, String paramString)
  {
    if (paramParametroSistema.jdoFlags == 0)
    {
      paramParametroSistema.valorParametro = paramString;
      return;
    }
    StateManager localStateManager = paramParametroSistema.jdoStateManager;
    if (localStateManager == null)
    {
      paramParametroSistema.valorParametro = paramString;
      return;
    }
    localStateManager.setStringField(paramParametroSistema, jdoInheritedFieldCount + 3, paramParametroSistema.valorParametro, paramString);
  }

  private void writeObject(ObjectOutputStream paramObjectOutputStream)
    throws IOException
  {
    jdoPreSerialize();
    paramObjectOutputStream.defaultWriteObject();
  }
}