package sigefirrhh.sistema.exceptions;

public class ErrorSistema extends RuntimeException
{
  private String description;

  public String getDescription()
  {
    return this.description;
  }

  public void setDescription(String string)
  {
    this.description = string;
  }
}