package sigefirrhh.sistema;

import eforserver.business.AbstractBeanBusiness;
import eforserver.common.Resource;
import eforserver.jdo.PMThread;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import sigefirrhh.base.estructura.Region;
import sigefirrhh.base.estructura.RegionBeanBusiness;

public class UsuarioBeanBusiness extends AbstractBeanBusiness
  implements Serializable
{
  static Logger log = Logger.getLogger(UsuarioBeanBusiness.class.getName());

  public void addUsuario(Usuario usuario)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Usuario usuarioNew = 
      (Usuario)BeanUtils.cloneBean(
      usuario);

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (usuarioNew.getRegion() != null) {
      usuarioNew.setRegion(
        regionBeanBusiness.findRegionById(
        usuarioNew.getRegion().getIdRegion()));
    }
    pm.makePersistent(usuarioNew);
  }

  public void updateUsuario(Usuario usuario) throws Exception
  {
    Usuario usuarioModify = 
      findUsuarioById(usuario.getIdUsuario());

    RegionBeanBusiness regionBeanBusiness = new RegionBeanBusiness();

    if (usuario.getRegion() != null) {
      usuario.setRegion(
        regionBeanBusiness.findRegionById(
        usuario.getRegion().getIdRegion()));
    }

    BeanUtils.copyProperties(usuarioModify, usuario);
  }

  public void deleteUsuario(Usuario usuario) throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Usuario usuarioDelete = 
      findUsuarioById(usuario.getIdUsuario());
    pm.deletePersistent(usuarioDelete);
  }

  public Usuario findUsuarioById(long idUsuario) throws Exception {
    HashMap parameters = new HashMap();

    PersistenceManager pm = PMThread.getPM();
    String filter = "idUsuario == pIdUsuario";
    Query query = pm.newQuery(Usuario.class, filter);

    query.declareParameters("long pIdUsuario");

    parameters.put("pIdUsuario", new Long(idUsuario));

    Collection colUsuario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    Iterator iterator = colUsuario.iterator();
    return (Usuario)iterator.next();
  }

  public Collection findUsuarioAll() throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Extent usuarioExtent = pm.getExtent(
      Usuario.class, true);
    Query query = pm.newQuery(usuarioExtent);
    query.setOrdering("apellidos ascending");
    Collection collection = new ArrayList((Collection)query.execute());
    return collection;
  }

  public Collection findByUsuario(String usuario)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();
    Collection colUsuario = new ArrayList();

    Connection cn = null;
    PreparedStatement reg = null;
    ResultSet rst = null;
    SistemaFacade sistemaFacade = new SistemaFacade();
    try {
      String sql = "select id_usuario from usuario where UPPER(usuario) like ?";
      cn = Resource.getConnection();
      cn.setAutoCommit(true);
      reg = cn.prepareStatement(sql, 1005, 1007);

      reg.setString(1, usuario.toUpperCase() + "%");

      rst = reg.executeQuery();

      while (rst.next()) {
        long idUsuario = rst.getLong("id_usuario");
        Usuario usr = sistemaFacade.findUsuarioById(idUsuario);
        if (usr != null) colUsuario.add(usr); 
      }
    }
    catch (Exception ex) { throw new Exception("Error al buscar usuario " + usuario, ex);
    } finally {
      if (rst != null) try { rst.close(); } catch (Exception e) { log.error(e.getMessage()); }
      if (reg != null) try { reg.close(); } catch (Exception e) { log.error(e.getMessage()); }
      if (cn != null) try { cn.close(); cn = null; } catch (Exception e) { log.error(e.getMessage()); }


    }

    pm.makeTransientAll(colUsuario);

    return colUsuario;
  }

  public Collection findByApellidos(String apellidos)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    String filter = "apellidos.startsWith(pApellidos)";

    Query query = pm.newQuery(Usuario.class, filter);

    query.declareParameters("java.lang.String pApellidos");
    HashMap parameters = new HashMap();

    parameters.put("pApellidos", new String(apellidos));

    query.setOrdering("apellidos ascending");

    Collection colUsuario = 
      new ArrayList((Collection)query.executeWithMap(parameters));

    pm.makeTransientAll(colUsuario);

    return colUsuario;
  }

  public Collection findByPassword(String usuario, String password)
    throws Exception
  {
    PersistenceManager pm = PMThread.getPM();

    Collection colUsuario = new ArrayList();
    Connection cn = null;
    PreparedStatement reg = null;
    ResultSet rst = null;
    SistemaFacade sistemaFacade = new SistemaFacade();
    try {
      String sql = "select id_usuario from usuario where UPPER(usuario) = ?";
      cn = Resource.getConnection();
      cn.setAutoCommit(true);
      reg = cn.prepareStatement(sql, 1005, 1007);

      reg.setString(1, usuario.toUpperCase());

      rst = reg.executeQuery();

      while (rst.next()) {
        long idUsuario = rst.getLong("id_usuario");
        Usuario usr = sistemaFacade.findUsuarioById(idUsuario);
        if (usr != null) colUsuario.add(usr); 
      }
    }
    catch (Exception ex) { throw new Exception("Error al buscar usuario " + usuario, ex);
    } finally {
      if (rst != null) try { rst.close(); } catch (Exception e) { log.error(e.getMessage()); }
      if (reg != null) try { reg.close(); } catch (Exception e) { log.error(e.getMessage()); }
      if (cn != null) try { cn.close(); cn = null; } catch (Exception e) { log.error(e.getMessage()); }

    }

    pm.makeTransientAll(colUsuario);

    return colUsuario;
  }
}