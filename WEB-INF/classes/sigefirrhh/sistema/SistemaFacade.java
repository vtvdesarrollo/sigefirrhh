package sigefirrhh.sistema;

import eforserver.business.AbstractFacade;
import eforserver.jdo.PMThread;
import eforserver.jdo.TxnManager;
import eforserver.jdo.TxnManagerFactory;
import java.io.Serializable;
import java.util.Collection;
import javax.jdo.PersistenceManager;

public class SistemaFacade extends AbstractFacade
  implements Serializable
{
  private TxnManager txn = TxnManagerFactory.makeTransactionManager();
  private SistemaBusiness sistemaBusiness = new SistemaBusiness();

  public void addOpcion(Opcion opcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sistemaBusiness.addOpcion(opcion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateOpcion(Opcion opcion) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.updateOpcion(opcion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteOpcion(Opcion opcion) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.deleteOpcion(opcion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Opcion findOpcionById(long opcionId) throws Exception
  {
    try { this.txn.open();
      Opcion opcion = 
        this.sistemaBusiness.findOpcionById(opcionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(opcion);
      return opcion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllOpcion() throws Exception
  {
    try { this.txn.open();
      return this.sistemaBusiness.findAllOpcion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findOpcionByCodigoOpcion(String codigoOpcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findOpcionByCodigoOpcion(codigoOpcion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findOpcionByDescripcion(String descripcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findOpcionByDescripcion(descripcion);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRol(Rol rol)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sistemaBusiness.addRol(rol);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRol(Rol rol) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.updateRol(rol);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRol(Rol rol) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.deleteRol(rol);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Rol findRolById(long rolId) throws Exception
  {
    try { this.txn.open();
      Rol rol = 
        this.sistemaBusiness.findRolById(rolId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(rol);
      return rol;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRol() throws Exception
  {
    try { this.txn.open();
      return this.sistemaBusiness.findAllRol();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRolByCodigoRol(String codigoRol)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findRolByCodigoRol(codigoRol);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRolByNombre(String nombre)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findRolByNombre(nombre);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addRolOpcion(RolOpcion rolOpcion)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sistemaBusiness.addRolOpcion(rolOpcion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateRolOpcion(RolOpcion rolOpcion) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.updateRolOpcion(rolOpcion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteRolOpcion(RolOpcion rolOpcion) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.deleteRolOpcion(rolOpcion);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public RolOpcion findRolOpcionById(long rolOpcionId) throws Exception
  {
    try { this.txn.open();
      RolOpcion rolOpcion = 
        this.sistemaBusiness.findRolOpcionById(rolOpcionId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(rolOpcion);
      return rolOpcion;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllRolOpcion() throws Exception
  {
    try { this.txn.open();
      return this.sistemaBusiness.findAllRolOpcion();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRolOpcionByRol(long idRol)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findRolOpcionByRol(idRol);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addUsuario(Usuario usuario)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sistemaBusiness.addUsuario(usuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUsuario(Usuario usuario) throws Exception
  {
    try { this.txn.open();

      this.sistemaBusiness.updateUsuario(usuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUsuario(Usuario usuario) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.deleteUsuario(usuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Usuario findUsuarioById(long usuarioId) throws Exception
  {
    try { this.txn.open();
      Usuario usuario = 
        this.sistemaBusiness.findUsuarioById(usuarioId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(usuario);
      return usuario;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUsuario() throws Exception
  {
    try { this.txn.open();
      return this.sistemaBusiness.findAllUsuario();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUsuarioByUsuario(String usuario)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioByUsuario(usuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUsuarioByApellidos(String apellidos)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioByApellidos(apellidos);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addUsuarioOrganismo(UsuarioOrganismo usuarioOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sistemaBusiness.addUsuarioOrganismo(usuarioOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUsuarioOrganismo(UsuarioOrganismo usuarioOrganismo) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.updateUsuarioOrganismo(usuarioOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUsuarioOrganismo(UsuarioOrganismo usuarioOrganismo) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.deleteUsuarioOrganismo(usuarioOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public UsuarioOrganismo findUsuarioOrganismoById(long usuarioOrganismoId) throws Exception
  {
    try { this.txn.open();
      UsuarioOrganismo usuarioOrganismo = 
        this.sistemaBusiness.findUsuarioOrganismoById(usuarioOrganismoId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(usuarioOrganismo);
      return usuarioOrganismo;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUsuarioOrganismo() throws Exception
  {
    try { this.txn.open();
      return this.sistemaBusiness.findAllUsuarioOrganismo();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUsuarioOrganismoByUsuario(long idUsuario, long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioOrganismoByUsuario(idUsuario, idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUsuarioOrganismoByOrganismo(long idOrganismo)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioOrganismoByOrganismo(idOrganismo);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addUsuarioRol(UsuarioRol usuarioRol)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sistemaBusiness.addUsuarioRol(usuarioRol);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUsuarioRol(UsuarioRol usuarioRol) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.updateUsuarioRol(usuarioRol);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUsuarioRol(UsuarioRol usuarioRol) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.deleteUsuarioRol(usuarioRol);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public UsuarioRol findUsuarioRolById(long usuarioRolId) throws Exception
  {
    try { this.txn.open();
      UsuarioRol usuarioRol = 
        this.sistemaBusiness.findUsuarioRolById(usuarioRolId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(usuarioRol);
      return usuarioRol;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUsuarioRol() throws Exception
  {
    try { this.txn.open();
      return this.sistemaBusiness.findAllUsuarioRol();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUsuarioRolByUsuario(long idUsuario)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioRolByUsuario(idUsuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUsuarioRolByRol(long idRol)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioRolByRol(idRol);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public void addUsuarioTipoPersonal(UsuarioTipoPersonal usuarioTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sistemaBusiness.addUsuarioTipoPersonal(usuarioTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUsuarioTipoPersonal(UsuarioTipoPersonal usuarioTipoPersonal) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.updateUsuarioTipoPersonal(usuarioTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUsuarioTipoPersonal(UsuarioTipoPersonal usuarioTipoPersonal) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.deleteUsuarioTipoPersonal(usuarioTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public UsuarioTipoPersonal findUsuarioTipoPersonalById(long usuarioTipoPersonalId) throws Exception
  {
    try { this.txn.open();
      UsuarioTipoPersonal usuarioTipoPersonal = 
        this.sistemaBusiness.findUsuarioTipoPersonalById(usuarioTipoPersonalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(usuarioTipoPersonal);
      return usuarioTipoPersonal;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUsuarioTipoPersonal() throws Exception
  {
    try { this.txn.open();
      return this.sistemaBusiness.findAllUsuarioTipoPersonal();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUsuarioTipoPersonalByUsuario(long idUsuario)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioTipoPersonalByUsuario(idUsuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUsuarioTipoPersonalByTipoPersonal(long idTipoPersonal)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioTipoPersonalByTipoPersonal(idTipoPersonal);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public Collection findUsuarioByPassword(String usuario, String password)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioByPassword(
        usuario, password);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findRolOpcionByRolAndOpcion(long idRol, String ruta) throws Exception
  {
    try {
      this.txn.open();
      return this.sistemaBusiness.findRolOpcionByRolAndOpcion(
        idRol, ruta);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void addUsuarioUnidadFuncional(UsuarioUnidadFuncional usuarioUnidadFuncional)
    throws Exception
  {
    try
    {
      this.txn.open();
      this.sistemaBusiness.addUsuarioUnidadFuncional(usuarioUnidadFuncional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void updateUsuarioUnidadFuncional(UsuarioUnidadFuncional usuarioUnidadFuncional) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.updateUsuarioUnidadFuncional(usuarioUnidadFuncional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public void deleteUsuarioUnidadFuncional(UsuarioUnidadFuncional usuarioUnidadFuncional) throws Exception
  {
    try { this.txn.open();
      this.sistemaBusiness.deleteUsuarioUnidadFuncional(usuarioUnidadFuncional);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public UsuarioUnidadFuncional findUsuarioUnidadFuncionalById(long usuarioUnidadFuncionalId) throws Exception
  {
    try { this.txn.open();
      UsuarioUnidadFuncional usuarioUnidadFuncional = 
        this.sistemaBusiness.findUsuarioUnidadFuncionalById(usuarioUnidadFuncionalId);
      PersistenceManager pm = PMThread.getPM();
      pm.makeTransient(usuarioUnidadFuncional);
      return usuarioUnidadFuncional;
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findAllUsuarioUnidadFuncional() throws Exception
  {
    try { this.txn.open();
      return this.sistemaBusiness.findAllUsuarioUnidadFuncional();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUsuarioUnidadFuncionalByUsuario(long idUsuario)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioUnidadFuncionalByUsuario(idUsuario);
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public Collection findUsuarioUnidadFuncionalByUnidadFuncional(long idUnidadFuncional)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findUsuarioUnidadFuncionalByUnidadFuncional(idUnidadFuncional);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public ParametroSistema findParametroSistemaByName(String nombreParametro)
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.findParametroSistemaByName(nombreParametro);
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public int getParametroMaxIntentos()
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.getParametroMaxIntentos();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public int getParametroDiasVigenciaUsuario() throws Exception
  {
    try {
      this.txn.open();
      return this.sistemaBusiness.getParametroDiasVigenciaUsuario();
    } finally {
      if (this.txn != null) this.txn.close(); 
    }
  }

  public int getParametroMinLongitudClave() throws Exception
  {
    try {
      this.txn.open();
      return this.sistemaBusiness.getParametroMinLongitudClave();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }

  public int getParametroDiasDuracionClave()
    throws Exception
  {
    try
    {
      this.txn.open();
      return this.sistemaBusiness.getParametroDiasDuracionClave();
    } finally {
      if (this.txn != null) this.txn.close();
    }
  }
}