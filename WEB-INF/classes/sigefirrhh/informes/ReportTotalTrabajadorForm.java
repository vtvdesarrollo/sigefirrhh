package sigefirrhh.informes;

import eforserver.presentation.Form;
import eforserver.report.JasperForWeb;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.estructura.Organismo;
import sigefirrhh.login.LoginSession;
import sigefirrhh.personal.procesoNomina.ProcesoNominaNoGenFacade;

public class ReportTotalTrabajadorForm extends Form
  implements Serializable
{
  static Logger log = Logger.getLogger(ReportTotalTrabajadorForm.class.getName());
  private int reportId;
  private String formato = "1";
  private long idTipoPersonal;
  private String reportName;
  private String uel = "S";
  private Collection listTipoPersonal;
  private DefinicionesNoGenFacade definicionesFacade;
  private ProcesoNominaNoGenFacade procesoNominaFacade;
  private LoginSession login;
  private String sexo = "S";

  public ReportTotalTrabajadorForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.login = ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(context, "loginSession"));

    this.definicionesFacade = new DefinicionesNoGenFacade();
    this.procesoNominaFacade = new ProcesoNominaNoGenFacade();
    this.reportName = "totaltrab";
    this.reportId = JasperForWeb.newReportId(this.reportId);

    refresh();

    LifecycleFactory lifecycleFactory = (LifecycleFactory)
      FactoryFinder.getFactory("javax.faces.lifecycle.LifecycleFactory");

    Lifecycle lifecycle = lifecycleFactory.getLifecycle("DEFAULT");

    lifecycle.addPhaseListener(
      new PhaseListener()
    {
      public void beforePhase(PhaseEvent event)
      {
      }

      public void afterPhase(PhaseEvent event)
      {
        ReportTotalTrabajadorForm.this.cambiarNombreAReporte();
      }

      public PhaseId getPhaseId() {
        return PhaseId.UPDATE_MODEL_VALUES;
      }
    });
  }

  public void refresh()
  {
    try
    {
      this.listTipoPersonal = this.definicionesFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getOrganismo().getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public void cambiarNombreAReporte()
  {
    this.reportName = "totaltrab";

    if (this.uel.equals("S")) {
      this.reportName += "uel";
    }
    if (this.sexo.equals("S")) {
      this.reportName += "sex";
    }
    if (this.formato.equals("2"))
      this.reportName = ("a_" + this.reportName);
  }

  public String runReport()
  {
    this.reportName = "";
    Map parameters = new Hashtable();
    try
    {
      this.reportName = "totaltrab";

      if (this.uel.equals("S")) {
        this.reportName += "uel";
      }
      if (this.sexo.equals("S")) {
        this.reportName += "sex";
      }
      if (this.formato.equals("2")) {
        this.reportName = ("a_" + this.reportName);
      }
      FacesContext context = FacesContext.getCurrentInstance();
      parameters.put("nombre_organismo", this.login.getOrganismo().getNombreOrganismo());
      parameters.put("logo", ((ServletContext)context.getExternalContext().getContext()).getRealPath(this.login.getURLLogo()));

      JasperForWeb report = new JasperForWeb();

      if (this.formato.equals("2")) {
        report.setType(3);
      }

      report.setReportName(this.reportName);
      report.setParameters(parameters);
      report.setPath(
        ((ServletContext)context.getExternalContext().getContext()).getRealPath("/") + "/reports/sigefirrhh/informes");

      report.start();

      ((HttpServletRequest)context.getExternalContext().getRequest()).getSession().setAttribute(this.reportName + this.reportId, report);

      this.reportId = JasperForWeb.newReportId(this.reportId);
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
    return null;
  }

  public String getIdTipoPersonal()
  {
    return String.valueOf(this.idTipoPersonal);
  }

  public void setIdTipoPersonal(String l) {
    this.idTipoPersonal = Long.parseLong(l);
  }

  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public int getReportId() {
    return this.reportId;
  }

  public void setReportId(int i) {
    this.reportId = i;
  }

  public String getReportName() {
    return this.reportName;
  }

  public void setReportName(String string) {
    this.reportName = string;
  }

  public String getFormato()
  {
    return this.formato;
  }

  public void setFormato(String string) {
    this.formato = string;
  }

  public String getSexo() {
    return this.sexo;
  }

  public void setSexo(String sexo) {
    this.sexo = sexo;
  }

  public String getUel()
  {
    return this.uel;
  }
  public void setUel(String uel) {
    this.uel = uel;
  }
}