package sigefirrhh.informes;

public class Query
{
  private long id;
  private String nombre;
  private long total;
  private String codigo;
  private double totalCosto;
  private double porcentaje;

  public long getId()
  {
    return this.id;
  }
  public void setId(long id) {
    this.id = id;
  }

  public String getNombre()
  {
    return this.nombre;
  }

  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }

  public long getTotal()
  {
    return this.total;
  }

  public void setTotal(long total)
  {
    this.total = total;
  }
  public String getCodigo() {
    return this.codigo;
  }
  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public void setPorcentaje(float porcentaje)
  {
    this.porcentaje = porcentaje;
  }

  public void setTotalCosto(float totalCosto)
  {
    this.totalCosto = totalCosto;
  }
  public double getPorcentaje() {
    return this.porcentaje;
  }
  public void setPorcentaje(double porcentaje) {
    this.porcentaje = porcentaje;
  }
  public double getTotalCosto() {
    return this.totalCosto;
  }
  public void setTotalCosto(double totalCosto) {
    this.totalCosto = totalCosto;
  }
}