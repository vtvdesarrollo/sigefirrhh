package sigefirrhh.informes;

import java.io.Serializable;
import java.util.Collection;

public class InformesFacade
  implements Serializable
{
  private InformesBusiness informesBusiness = new InformesBusiness();

  public Collection consultarTrabajadores(long idOrganismo, long idEstado) {
    return this.informesBusiness.consultarTrabajadores(
      idOrganismo, idEstado);
  }
  public Collection consultarClasificacionDependencia(long idOrganismo, long idEstado, String tipo) {
    return this.informesBusiness.consultarClasificacionDependencia(
      idOrganismo, idEstado, tipo);
  }

  public Collection consultarCostoNomina(long idOrganismo, long idEstado, long idTipoPersonal, int anio, int mesDesde, int mesHasta, String periodicidad) {
    return this.informesBusiness.consultarCostoNomina(
      idOrganismo, idEstado, idTipoPersonal, anio, mesDesde, mesHasta, periodicidad);
  }
}