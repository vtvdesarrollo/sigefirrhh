package sigefirrhh.informes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.login.LoginSession;

public class QueryClasificacionDependenciaForm
  implements Serializable
{
  static Logger log = Logger.getLogger(QueryClasificacionDependenciaForm.class.getName());
  private LoginSession login;
  private InformesFacade informesFacade;
  private long idEstado;
  private Collection colResultado = new ArrayList();
  private int total;
  private String agrupado = "1";

  public int getTotal()
  {
    return this.total;
  }
  public QueryClasificacionDependenciaForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.informesFacade = new InformesFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
  }

  public void changeAgrupado(ValueChangeEvent event)
  {
    String agrupado = (String)event.getNewValue();

    FacesContext context = FacesContext.getCurrentInstance();
    try
    {
      this.agrupado = agrupado;
    }
    catch (Exception e) {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeIdEstado(ValueChangeEvent event) {
    log.error("NEW VALUE " + event.getNewValue());
    long idEstado = Long.parseLong(
      event.getNewValue().toString());

    this.colResultado = this.informesFacade.consultarClasificacionDependencia(
      this.login.getIdOrganismo(), idEstado, this.agrupado);

    Iterator iterator = this.colResultado.iterator();
    this.total = 0;
    while (iterator.hasNext()) {
      this.total = ((int)(this.total + ((Query)iterator.next()).getTotal()));
    }

    iterator = this.colResultado.iterator();
    Query query = null;
    while (iterator.hasNext()) {
      query = (Query)iterator.next();
      query.setPorcentaje(
        (float)(query.getTotal() * 100L / this.total));
    }
  }

  public boolean isResult() {
    return this.colResultado != null;
  }

  public Collection getColResultado()
  {
    return this.colResultado;
  }
  public void setColResultado(Collection colResultado) {
    this.colResultado = colResultado;
  }

  public long getIdEstado()
  {
    return this.idEstado;
  }

  public void setIdEstado(long idEstado)
  {
    this.idEstado = idEstado;
  }
  public String getAgrupado() {
    return this.agrupado;
  }
  public void setAgrupado(String agrupado) {
    this.agrupado = agrupado;
  }
}