package sigefirrhh.informes;

public class Trabajadores
{
  private long idTipoPersonal;
  private String nombre;
  private long total;
  private float porcentaje;

  public void setPorcentaje(long porcentaje)
  {
    this.porcentaje = ((float)porcentaje);
  }

  public long getIdTipoPersonal()
  {
    return this.idTipoPersonal;
  }

  public void setIdTipoPersonal(long idTipoPersonal)
  {
    this.idTipoPersonal = idTipoPersonal;
  }

  public String getNombre()
  {
    return this.nombre;
  }

  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }

  public long getTotal()
  {
    return this.total;
  }

  public void setTotal(long total)
  {
    this.total = total;
  }
  public float getPorcentaje() {
    return this.porcentaje;
  }
  public void setPorcentaje(float porcentaje) {
    this.porcentaje = porcentaje;
  }
}