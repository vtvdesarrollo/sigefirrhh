package sigefirrhh.informes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Logger;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;

public class TrabajadoresForm
  implements Serializable
{
  static Logger log = Logger.getLogger(TrabajadoresForm.class.getName());
  private LoginSession login;
  private InformesFacade informesFacade;
  private long idEstado;
  private Collection colTrabajadores = new ArrayList();
  private int total;
  private String nombreEstado = "Nivel Nacional";
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();

  public int getTotal()
  {
    return this.total;
  }
  public TrabajadoresForm() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.informesFacade = new InformesFacade();

    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
  }
  public String nivelNacional() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.idEstado = 0L;
    this.nombreEstado = "Nivel Nacional";
    consultar();
    return null;
  }

  public void consultar() {
    this.colTrabajadores = this.informesFacade.consultarTrabajadores(
      this.login.getIdOrganismo(), this.idEstado);

    Iterator iterator = this.colTrabajadores.iterator();
    this.total = 0;
    while (iterator.hasNext()) {
      this.total = ((int)(this.total + ((Trabajadores)iterator.next()).getTotal()));
    }

    iterator = this.colTrabajadores.iterator();
    Trabajadores trabajadores = null;
    while (iterator.hasNext()) {
      trabajadores = (Trabajadores)iterator.next();
      trabajadores.setPorcentaje(
        trabajadores.getTotal() * 100L / this.total);
    }
  }

  public void changeIdEstado(ValueChangeEvent event) { log.error("NEW VALUE " + event.getNewValue());
    long idEstado = Long.parseLong(
      event.getNewValue().toString());
    this.idEstado = idEstado;
    if (idEstado != 0L)
      try {
        this.nombreEstado = this.ubicacionFacade.findEstadoById(idEstado).getNombre();
      }
      catch (Exception localException)
      {
      }
    consultar();
  }

  public boolean isResult()
  {
    return this.colTrabajadores != null;
  }

  public Collection getColTrabajadores()
  {
    return this.colTrabajadores;
  }

  public void setColTrabajadores(Collection colTrabajadores)
  {
    this.colTrabajadores = colTrabajadores;
  }

  public long getIdEstado()
  {
    return this.idEstado;
  }

  public void setIdEstado(long idEstado)
  {
    this.idEstado = idEstado;
  }
  public String getNombreEstado() {
    return this.nombreEstado;
  }
  public void setNombreEstado(String nombreEstado) {
    this.nombreEstado = nombreEstado;
  }
}