package sigefirrhh.informes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import sigefirrhh.base.definiciones.DefinicionesNoGenFacade;
import sigefirrhh.base.definiciones.GrupoNomina;
import sigefirrhh.base.definiciones.TipoPersonal;
import sigefirrhh.base.ubicacion.Estado;
import sigefirrhh.base.ubicacion.UbicacionFacade;
import sigefirrhh.login.LoginSession;

public class MapCostoNominaForm
  implements Serializable
{
  static Logger log = Logger.getLogger(MapCostoNominaForm.class.getName());
  private LoginSession login;
  private InformesFacade informesFacade;
  private long idEstado;
  private String nombreEstado = "Nivel Nacional";
  private Collection colResultado = new ArrayList();
  private double total;
  private TipoPersonal tipoPersonal;
  private int anio;
  private int mesDesde = 0;
  private int mesHasta = 0;
  private String selectTipoPersonal;
  private DefinicionesNoGenFacade definicionesNoGenFacade = new DefinicionesNoGenFacade();
  private Collection listTipoPersonal;
  private UbicacionFacade ubicacionFacade = new UbicacionFacade();

  public MapCostoNominaForm()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    this.informesFacade = new InformesFacade();
    this.anio = Calendar.getInstance().get(1);
    log.error("mes********************* pasa por el form");
    this.login = 
      ((LoginSession)context.getApplication().getVariableResolver().resolveVariable(
      context, 
      "loginSession"));
    try {
      this.listTipoPersonal = this.definicionesNoGenFacade.findTipoPersonalWithSeguridad(this.login.getIdUsuario(), this.login.getIdOrganismo(), this.login.getAdministrador());
    } catch (Exception e) {
      this.listTipoPersonal = new ArrayList();
    }
  }

  public String actualizar() {
    FacesContext context = FacesContext.getCurrentInstance();

    log.error("Act Desde********************* " + this.mesDesde);
    log.error("Act Hasta********************* " + this.mesHasta);
    log.error("anio********************** " + this.anio);
    if (this.mesDesde > this.mesHasta) {
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mes Hasta no puede ser mayor a Mes Desde", ""));
      return null;
    }
    consultar();
    return null;
  }
  public String nivelNacional() {
    FacesContext context = FacesContext.getCurrentInstance();
    this.idEstado = 0L;
    this.nombreEstado = "Nivel Nacional";

    log.error("idEstado********************* " + this.idEstado);
    log.error("Nac Desde********************* " + this.mesDesde);
    log.error("Nac Hasta********************* " + this.mesHasta);
    log.error("anio********************** " + this.anio);
    consultar();
    if (this.mesDesde > this.mesHasta) {
      context.addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mes Hasta no puede ser mayor a Mes Desde", ""));
      return null;
    }
    return null;
  }

  public void changeTipoPersonal(ValueChangeEvent event) {
    long idTipoPersonal = Long.valueOf(
      (String)event.getNewValue()).longValue();
    try {
      this.tipoPersonal = null;
      if (idTipoPersonal > 0L) {
        this.tipoPersonal = this.definicionesNoGenFacade.findTipoPersonalById(idTipoPersonal);
      }
      consultar();
    }
    catch (Exception e)
    {
      log.error("Excepcion controlada:", e);
    }
  }

  public void changeIdEstado(ValueChangeEvent event) {
    log.error("NEW VALUE " + event.getNewValue());
    long idEstado = Long.parseLong(
      event.getNewValue().toString());

    this.idEstado = idEstado;
    if (idEstado != 0L)
      try {
        this.nombreEstado = this.ubicacionFacade.findEstadoById(idEstado).getNombre();
      }
      catch (Exception localException)
      {
      }
    consultar();
  }

  public void consultar()
  {
    log.error("Nac Desde********************* " + this.mesDesde);
    log.error("Nac Hasta********************* " + this.mesHasta);
    log.error("anio********************** " + this.anio);
    if (this.tipoPersonal == null)
      this.colResultado = this.informesFacade.consultarCostoNomina(this.login.getIdOrganismo(), this.idEstado, 0L, this.anio, this.mesDesde, this.mesHasta, "0");
    else {
      this.colResultado = this.informesFacade.consultarCostoNomina(
        this.login.getIdOrganismo(), this.idEstado, this.tipoPersonal.getIdTipoPersonal(), this.anio, this.mesDesde, this.mesHasta, this.tipoPersonal.getGrupoNomina().getPeriodicidad());
    }
    Iterator iterator = this.colResultado.iterator();
    this.total = 0.0D;
    while (iterator.hasNext()) {
      this.total += ((Query)iterator.next()).getTotalCosto();
    }
    if (this.total > 0.0D) {
      iterator = this.colResultado.iterator();
      Query query = null;
      while (iterator.hasNext()) {
        query = (Query)iterator.next();
        query.setPorcentaje(
          query.getTotalCosto() * 100.0D / this.total);
      }
    }
    Query queryTotal = new Query();
    queryTotal.setNombre("TOTAL ASIGNACIONES");
    queryTotal.setTotalCosto(this.total);
    queryTotal.setPorcentaje(100.0F);
    this.colResultado.add(queryTotal);
  }
  public boolean isResult() {
    return this.colResultado != null;
  }
  public Collection getListTipoPersonal() {
    Collection col = new ArrayList();
    Iterator iterator = this.listTipoPersonal.iterator();

    while (iterator.hasNext()) {
      Long id = (Long)iterator.next();
      String nombre = (String)iterator.next();
      col.add(new SelectItem(
        String.valueOf(id), 
        nombre));
    }
    return col;
  }

  public Collection getColResultado()
  {
    return this.colResultado;
  }
  public void setColResultado(Collection colResultado) {
    this.colResultado = colResultado;
  }

  public long getIdEstado()
  {
    return this.idEstado;
  }

  public void setIdEstado(long idEstado)
  {
    this.idEstado = idEstado;
  }
  public int getAnio() {
    return this.anio;
  }
  public void setAnio(int anio) {
    this.anio = anio;
  }
  public int getMesDesde() {
    return this.mesDesde;
  }
  public void setMesDesde(int mesDesde) {
    this.mesDesde = mesDesde;
  }
  public int getMesHasta() {
    return this.mesHasta;
  }
  public void setMesHasta(int mesHasta) {
    this.mesHasta = mesHasta;
  }
  public String getSelectTipoPersonal() {
    return this.selectTipoPersonal;
  }
  public void setSelectTipoPersonal(String selectTipoPersonal) {
    this.selectTipoPersonal = selectTipoPersonal;
  }
  public String getNombreEstado() {
    return this.nombreEstado;
  }
  public void setNombreEstado(String nombreEstado) {
    this.nombreEstado = nombreEstado;
  }
}