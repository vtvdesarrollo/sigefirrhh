package sigefirrhh.informes;

import eforserver.common.Resource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.log4j.Logger;

public class InformesBusiness
  implements Serializable
{
  Logger log = Logger.getLogger(InformesBusiness.class.getName());

  public Collection consultarTrabajadores(long idOrganismo, long idEstado)
  {
    ResultSet rsTrabajadores = null;
    PreparedStatement stTrabajadores = null;
    Connection connection = null;
    Collection result = new ArrayList();
    try {
      connection = Resource.getConnection();

      StringBuffer sql = new StringBuffer();
      sql.append("select ");
      sql.append("\tcount(id_trabajador) as total, ");
      sql.append("\ttipopersonal.id_tipo_personal, ");
      sql.append("\ttipopersonal.nombre ");
      sql.append("from ");
      sql.append("\ttrabajador, ");
      sql.append("\ttipopersonal, ");
      sql.append("\tdependencia, ");
      sql.append("\tsede, ");
      sql.append("\tciudad, ");
      sql.append("\testado ");
      sql.append("where ");
      sql.append("\ttrabajador.id_tipo_personal = tipopersonal.id_tipo_personal and ");
      sql.append("\ttipopersonal.id_organismo = ? and ");

      sql.append("\ttrabajador.id_dependencia = dependencia.id_dependencia and ");
      sql.append("\tdependencia.id_sede = sede.id_sede and ");
      sql.append("\tsede.id_ciudad = ciudad.id_ciudad and ");
      sql.append("\tciudad.id_estado = estado.id_estado and ");
      if (idEstado != 0L)
        sql.append("\testado.id_estado = ? and trabajador.estatus = 'A' ");
      else {
        sql.append("\ttrabajador.estatus = 'A' ");
      }

      sql.append("group by ");
      sql.append("\ttipopersonal.id_tipo_personal, ");
      sql.append("\ttipopersonal.nombre ");
      sql.append("order by ");
      sql.append("\ttipopersonal.id_tipo_personal");

      stTrabajadores = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stTrabajadores.setLong(1, idOrganismo);
      if (idEstado != 0L) {
        stTrabajadores.setLong(2, idEstado);
      }
      rsTrabajadores = stTrabajadores.executeQuery();

      Trabajadores trabajadores = null;
      while (rsTrabajadores.next()) {
        trabajadores = new Trabajadores();
        trabajadores.setIdTipoPersonal(rsTrabajadores.getLong("id_tipo_personal"));
        trabajadores.setNombre(rsTrabajadores.getString("nombre"));
        trabajadores.setTotal(rsTrabajadores.getLong("total"));
        result.add(trabajadores);
      }
    } catch (Exception e) {
      this.log.error("InformesBusiness / consultarTrabajadores(): Excepcion controlada:", e);
      return null;
    } finally {
      if (rsTrabajadores != null) try { rsTrabajadores.close(); } catch (Exception localException4) {
        } if (stTrabajadores != null) try { stTrabajadores.close(); } catch (Exception localException5) {
        } if (connection != null) try { connection.close(); connection = null; } catch (Exception localException6) {  }
 
    }
    return result;
  }

  public Collection consultarClasificacionDependencia(long idOrganismo, long idEstado, String tipo)
  {
    ResultSet rsResultado = null;
    PreparedStatement stResultado = null;
    Connection connection = null;
    Collection result = new ArrayList();
    try {
      connection = Resource.getConnection();

      StringBuffer sql = new StringBuffer();
      sql.append("select ");
      sql.append("\tcount(d.id_dependencia) as total, ");
      sql.append("\tcad.id_caracteristica_dependencia, ");
      sql.append("\tcad.nombre ");
      sql.append("from ");
      sql.append("\tdependencia d, ");
      sql.append("\tclasificaciondependencia cld, ");
      sql.append("\tcaracteristicadependencia cad, ");
      sql.append("\tsede s, ");
      sql.append("\tciudad c, ");
      sql.append("\testado e ");
      sql.append("where ");
      sql.append("\td.id_dependencia = cld.id_dependencia and ");
      sql.append("\tcld.id_caracteristica_dependencia = cad.id_caracteristica_dependencia and ");
      sql.append("\td.id_organismo = ? and ");
      sql.append("\td.id_sede = s.id_sede and ");
      sql.append("\ts.id_ciudad = c.id_ciudad and ");
      sql.append("\tc.id_estado = e.id_estado and ");
      sql.append("\te.id_estado = ? and d.vigente = 'S' and ");
      sql.append("\tcad.tipo = ?  ");
      sql.append("group by ");
      sql.append("\tcad.id_caracteristica_dependencia, ");
      sql.append("\tcad.nombre ");
      sql.append("order by ");
      sql.append("\tcad.nombre");

      stResultado = connection.prepareStatement(
        sql.toString(), 
        1003, 
        1007);

      stResultado.setLong(1, idOrganismo);
      stResultado.setLong(2, idEstado);
      stResultado.setString(3, tipo);
      this.log.error("InformesBusiness / consultarClasificacionDependencia() : ooooooooooooooooooooooooo - " + tipo);
      this.log.error(sql.toString());
      rsResultado = stResultado.executeQuery();

      Query query = null;
      while (rsResultado.next()) {
        query = new Query();
        query.setId(rsResultado.getLong("id_caracteristica_dependencia"));
        query.setNombre(rsResultado.getString("nombre"));
        query.setTotal(rsResultado.getLong("total"));
        result.add(query);
      }
    } catch (Exception e) {
      this.log.error("InformesBusiness / consultarClasificacionDependencia() : Excepcion controlada:", e);
      return null;
    } finally {
      if (rsResultado != null) try { rsResultado.close(); } catch (Exception localException4) {
        } if (stResultado != null) try { stResultado.close(); } catch (Exception localException5) {
        } if (connection != null) try { connection.close(); connection = null; } catch (Exception localException6) {  }
 
    }
    return result;
  }

  public Collection consultarCostoNomina(long idOrganismo, long idEstado, long idTipoPersonal, int anio, int mesDesde, int mesHasta, String periodicidad)
  {
    ResultSet rsConsulta = null;
    PreparedStatement stConsulta = null;
    Connection connection = null;
    Collection result = new ArrayList();
    try
    {
      connection = Resource.getConnection();
      StringBuffer sql = new StringBuffer();

      if (idTipoPersonal != 0L) {
        sql.append("select ");
        sql.append("\tsum(h.monto_asigna) as total, ");
        sql.append("\tmin(co.cod_concepto) as codigo, ");
        sql.append("\tmin(co.descripcion) as nombre ");
        sql.append("from ");
        if (periodicidad.equals("S"))
          sql.append("\thistoricosemana h, ");
        else {
          sql.append("\thistoricoquincena h, ");
        }
        sql.append("\thistoriconomina hn, ");
        sql.append("\tconceptotipopersonal ctp, ");
        sql.append("\ttipopersonal tp, ");
        sql.append("\tconcepto co, ");
        sql.append("\tdependencia d, ");
        sql.append("\tsede s, ");
        sql.append("\tciudad c, ");
        sql.append("\testado e ");
        sql.append("where ");
        sql.append("\th.id_tipo_personal = ? and ");
        sql.append("\th.id_tipo_personal = tp.id_tipo_personal and ");
        sql.append("\ttp.id_organismo = ? and ");
        sql.append("\th.id_historico_nomina = hn.id_historico_nomina and ");
        sql.append("\thn.id_dependencia = d.id_dependencia and ");
        sql.append("\td.id_sede = s.id_sede and ");
        sql.append("\ts.id_ciudad = c.id_ciudad and ");
        sql.append("\tc.id_estado = e.id_estado and ");
        if (idEstado > 0L) {
          sql.append("\te.id_estado = ? and ");
        }

        sql.append("\th.mes between ? and ? and ");

        sql.append("\th.anio = ? and ");
        sql.append("\th.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal and ");
        sql.append("\tctp.id_concepto = co.id_concepto and co.cod_concepto < '5000' ");
        sql.append("group by ");
        sql.append("\tco.cod_concepto ");
        sql.append("order by ");
        sql.append("\tco.cod_concepto ");

        stConsulta = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConsulta.setLong(1, idTipoPersonal);
        stConsulta.setLong(2, idOrganismo);
        if (idEstado > 0L) {
          stConsulta.setLong(3, idEstado);
          stConsulta.setLong(4, mesDesde);
          stConsulta.setLong(5, mesHasta);
          stConsulta.setLong(6, anio);
        }
        else {
          stConsulta.setLong(3, mesDesde);
          stConsulta.setLong(4, mesHasta);
          stConsulta.setLong(5, anio);
        }
      }
      else {
        sql.append("select sum(total) as total, max(codigo) as codigo, max(nombre) as nombre from ( ");
        sql.append("select ");
        sql.append("sum(h.monto_asigna) as total, ");
        sql.append("max(co.cod_concepto) as codigo, ");
        sql.append("max(co.descripcion) as nombre ");
        sql.append("from ");
        sql.append("historicosemana h, ");
        sql.append("historiconomina hn, ");
        sql.append("conceptotipopersonal ctp, ");
        sql.append("tipopersonal tp, ");
        sql.append("concepto co, ");
        sql.append("dependencia d, ");
        sql.append("sede s, ");
        sql.append("ciudad c, ");
        sql.append("estado e ");
        sql.append("where ");
        sql.append("h.id_tipo_personal = tp.id_tipo_personal and ");
        sql.append("tp.id_organismo = ? and ");
        sql.append("h.id_historico_nomina = hn.id_historico_nomina and ");
        sql.append("hn.id_dependencia = d.id_dependencia and ");
        sql.append("d.id_sede = s.id_sede and ");
        sql.append("s.id_ciudad = c.id_ciudad and ");
        sql.append("c.id_estado = e.id_estado and ");
        if (idEstado > 0L) {
          sql.append("e.id_estado = ? and ");
        }
        sql.append("h.mes between ? and ? and ");

        sql.append("h.anio = ? and ");
        sql.append("h.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal and ");
        sql.append("ctp.id_concepto = co.id_concepto and co.cod_concepto < '5000' group by co.cod_concepto ");
        sql.append("union ");
        sql.append("select ");
        sql.append("sum(h.monto_asigna) as total, ");
        sql.append("max(co.cod_concepto) as codigo, ");
        sql.append("max(co.descripcion) as nombre ");
        sql.append("from ");
        sql.append("historicoquincena h, ");
        sql.append("historiconomina hn, ");
        sql.append("conceptotipopersonal ctp, ");
        sql.append("tipopersonal tp, ");
        sql.append("concepto co, ");
        sql.append("dependencia d, ");
        sql.append("sede s, ");
        sql.append("ciudad c, ");
        sql.append("estado e ");
        sql.append("where ");
        sql.append("h.id_tipo_personal = tp.id_tipo_personal and ");
        sql.append("tp.id_organismo = ? and ");
        sql.append("h.id_historico_nomina = hn.id_historico_nomina and ");
        sql.append("hn.id_dependencia = d.id_dependencia and ");
        sql.append("d.id_sede = s.id_sede and ");
        sql.append("s.id_ciudad = c.id_ciudad and ");
        sql.append("c.id_estado = e.id_estado and ");
        if (idEstado > 0L) {
          sql.append("e.id_estado = ? and ");
        }
        sql.append("h.mes between ? and ? and ");
        sql.append("h.anio = ? and ");
        sql.append("h.id_concepto_tipo_personal = ctp.id_concepto_tipo_personal and ");
        sql.append("ctp.id_concepto = co.id_concepto and co.cod_concepto < '5000' group by co.cod_concepto ) as a ");
        sql.append("group by a.codigo ");
        sql.append("order by a.codigo ");

        this.log.error("InformesBusiness / consultarCostoNomina() : sql " + sql.toString());

        stConsulta = connection.prepareStatement(
          sql.toString(), 
          1003, 
          1007);
        stConsulta.setLong(1, idOrganismo);
        if (idEstado > 0L) {
          stConsulta.setLong(2, idEstado);
          stConsulta.setLong(3, mesDesde);
          stConsulta.setLong(4, mesHasta);
          stConsulta.setLong(5, anio);
          stConsulta.setLong(6, idOrganismo);
          stConsulta.setLong(7, idEstado);
          stConsulta.setLong(8, mesDesde);
          stConsulta.setLong(9, mesHasta);
          stConsulta.setLong(10, anio);
        } else {
          stConsulta.setLong(2, mesDesde);
          stConsulta.setLong(3, mesHasta);
          stConsulta.setLong(4, anio);
          stConsulta.setLong(5, idOrganismo);
          stConsulta.setLong(6, mesDesde);
          stConsulta.setLong(7, mesHasta);
          stConsulta.setLong(8, anio);
        }

      }

      rsConsulta = stConsulta.executeQuery();

      Query query = null;
      while (rsConsulta.next()) {
        query = new Query();
        query.setNombre(rsConsulta.getString("nombre"));
        query.setCodigo(rsConsulta.getString("codigo"));
        query.setTotalCosto(rsConsulta.getFloat("total"));
        result.add(query);
      }
    } catch (Exception e) {
      this.log.error("InformesBusiness / consultarCostoNomina() :  Excepcion controlada:", e);
      return null;
    } finally {
      if (rsConsulta != null) try { rsConsulta.close(); } catch (Exception localException4) {
        } if (stConsulta != null) try { stConsulta.close(); } catch (Exception localException5) {
        } if (connection != null) try { connection.close(); connection = null; } catch (Exception localException6) {  }
 
    }
    return result;
  }
}