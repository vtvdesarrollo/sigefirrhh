<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>

	<jsp:include page="/inc/top.jsp" />

	<table width="800"  border="0" cellspacing="0" cellpadding="0" align="left">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top" align="center">
					<tr>	
						<!-- Login -->
						<td width="800" valign="top">
						<h:form>
							<f:verbatim><table class="datatable" width="260" align="center">
								<tr>
									<td colspan="2">
										<b>Servicios para el Personal</b><br>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
								<tr>
            						<td>
            							C�dula de Identidad
            						</td>
            						<td></f:verbatim>
    	                        		<h:inputText
    	                        			id="CedulaIdentidad"
                							size="10"
                							maxlength="10"
            								value="#{loginServicioPersonalForm.cedula}" />
            						<f:verbatim></td>
            					</tr>
            					<tr>
            						<td>
            							A�o de Ingreso
            						</td>
            						<td></f:verbatim>
    	                        		<h:inputText
    	                        			id="AnioIngresoOrganismo"
                							size="4"
                							maxlength="4"
            								value="#{loginServicioPersonalForm.anioInicio}" />            					
											<f:verbatim>(aaaa)<span class="required"> </span></f:verbatim>
            						<f:verbatim></td>
            					</tr>
            					<tr>
            						<td>
            							Fecha Nacimiento
            						</td>
            						<td></f:verbatim>
    	                        		<h:inputText
    	                        			id="FechaNacimiento"
                							size="10"
                							maxlength="10"
                							onblur="javascript:check_date(this)"
            								value="#{loginServicioPersonalForm.fechaNacimiento}" >
            							    
            								<f:convertDateTime timeZone="#{dateTime.timeZone}" 
											pattern="dd-MM-yyyy" />
											
											</h:inputText>
            								<f:verbatim>(dd-mm-aaaa)<span class="required"> </span></f:verbatim>
            						<f:verbatim></td>
            					</tr>
								<tr>
            						<td>
            							Contrase�a
            						</td>
            						<td></f:verbatim>
    	                        		<h:inputSecret
    	                        			id="Contrasenia"
                							size="20"
                							maxlength="20"
            								value="#{loginServicioPersonalForm.password}" />
            						<f:verbatim></td>
            					</tr>
            					<tr>
            						<td>
            							Confirmar Contrase�a
            						</td>
            						<td></f:verbatim>
    	                        		<h:inputSecret
    	                        			id="ConfirmacionContrasenia"   	                    
                							size="20"
                							maxlength="20"
            								value="#{loginServicioPersonalForm.passwordConfirm}" />
            						<f:verbatim></td>
            					</tr>
            					<tr>
            						<td colpsan="2" align="center"></f:verbatim>
										<h:commandButton value="Registrar" 
                                    		action="#{loginServicioPersonalForm.sendRegistro}" />
									<f:verbatim></td>
            					</tr>
            					<tr>
            						<td colspan="2" align="center"></f:verbatim>
										<h:graphicImage url="/images/home/de-todos-65x40.gif" />
            						<f:verbatim></td>
            					</tr>
            				</table></f:verbatim>
						</h:form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>