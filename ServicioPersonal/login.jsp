<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<script language=javascript> 
	
		function firstFocus(){
			var paso = "false";			
			for (i = 0; i < document.forms.length; i++){
				for (j = 0; j < document.forms[i].elements.length; j++){					
					if (document.forms[i].elements[j].type =="text" || document.forms[i].elements[j].type =="select-one"){					
						paso = "true";
						document.forms[i].elements[j].focus();					
						break;
					}
				}				
				if (paso =="true"){
					break;
				}
			}
			
		}
		
    </script>
</head>

<body onload="firstFocus();">>
<f:view>

	<jsp:include page="/inc/top.jsp" />

	<table width="800"  border="0" cellspacing="0" cellpadding="0" align="left">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top" align="center">
					<tr>	
						<!-- Login -->
						<td width="800" valign="top">
						<h:form>
							<f:verbatim><table class="datatable" width="260" align="center">
								<tr>
									<td colspan="2">
										<b>Servicios para el Personal</b><br>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
								<tr>
            						<td>
            							C�dula de Identidad
            						</td>
            						<td></f:verbatim>
    	                        		<h:inputText
                							size="10"
                							maxlength="10"
            								value="#{loginServicioPersonalForm.cedula}" />
            						<f:verbatim></td>
            					</tr>
								<tr>
            						<td>
            							Contrase�a
            						</td>
            						<td>
									</f:verbatim>
    	                        		<h:inputSecret
                							size="20"
                							maxlength="20"
            								value="#{loginServicioPersonalForm.password}" />
            						<f:verbatim>
									</td>
            					</tr>
            					<tr>
            						<td align="center">
									</f:verbatim>
										<h:commandButton value="Iniciar Sesi�n" 
                                    		action="#{loginServicioPersonalForm.send}" />
									<f:verbatim>
									</td>
									<td align="right">
										<a href="registrar.jsf" class="link">Registrarse</a>
									</td>
            					</tr>
            					<tr>
            						<td colspan="2" align="center">
									</f:verbatim>
										<h:graphicImage url="/images/home/de-todos-65x40.gif" />
            						<f:verbatim>
									</td>
            					</tr>
            				</table></f:verbatim>
						</h:form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>