<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ page import="eforserver.report.JasperForWeb" %>
<%@ page import="sigefirrhh.login.LoginSession" %>

<%
	int state=0;
	JasperForWeb report = null;
	String mensaje = "";
	String clase = "";
	Integer conteo = new Integer(1);
	System.out.println("Reporte en Pagina: "+request.getParameter("reportName"));
	if (session.getAttribute(request.getParameter("reportName"))==null) {
		
		System.out.println("session.getAttribute(" +request.getParameter("reportName")+")==null)");
		
		state = JasperForWeb.STATE_BUILDING;
		if(session.getAttribute(request.getParameter("reportName")+"Count")==null){
			System.out.println("session.getAttribute(" +request.getParameter("reportName")+"Count)==null");
			session.setAttribute(request.getParameter("reportName")+"Count", conteo);
			System.out.println("session.setAttribute(" +request.getParameter("reportName")+"Count),"+conteo.toString()+")");
		} else {
			conteo = (Integer)session.getAttribute(request.getParameter("reportName")+"Count"); 
			System.out.println("conteo = "+conteo.toString());
			conteo = new Integer(conteo.intValue()+1);
			session.setAttribute(request.getParameter("reportName")+"Count", conteo);
			System.out.println("session.setAttribute(" +request.getParameter("reportName")+"Count),"+conteo.toString()+")");
		}
	} else {
		System.out.println("--> reportName = " + request.getParameter("reportName"));
		report = (JasperForWeb)session.getAttribute(request.getParameter("reportName"));
		state = report.getStatus();
	}
	if (state==JasperForWeb.STATE_READY) {
		report.print(response);
		clase="mensajeEspecial";
		mensaje = "El reporte se ha generado con exito<br>Por favor cierre esta ventana";

		session.removeAttribute(request.getParameter("reportName")); 
		session.removeAttribute(request.getParameter("reportName")+"Count"); 
		return;
	} else if (state==JasperForWeb.STATE_BUILDING) {
		if(conteo.intValue()>3){
			clase="mensajeError";
			mensaje = "Ha ocurrido un error en la construcci�n del reporte,<br>por favor contacte al administrador e indique la referencia " + request.getParameter("reportName");
			session.removeAttribute(request.getParameter("reportName")+"Count"); 

		}else{
			clase="mensajeEspecial";
			mensaje = "El reporte se est� construyendo, por favor espere.";
			if (conteo.intValue()!=1){
				mensaje = mensaje + "<br> ( Intento N� "+ conteo + " )";
			}
		}

	} else if (state==JasperForWeb.STATE_PROCESSING) {
		clase="mensajeEspecial";
		mensaje = "El reporte se est� generando, por favor espere...";
	} else if (state==JasperForWeb.STATE_ERROR) {
		session.removeAttribute("reportName");
		clase="mensajeError";
		mensaje = "Ha ocurrido un error en la generaci�n del reporte,<br>por favor contacte al administrador e indique  la referencia " + request.getParameter("reportName");
		System.out.println("==> JasperForWeb.STATE_ERROR -->  report.getError() = "+report.getError().getMessage());
	
	} else if (state==JasperForWeb.STATE_EMPTY){
		session.removeAttribute(request.getParameter("reportName")); 
		session.removeAttribute(request.getParameter("reportName")+"Count"); 
		clase="mensajeError";
		mensaje = "Lo sentimos, el reporte solicitado no ha devuelto registros,<br> por favor modifique sus criterios<br> e intente de nuevo";
		
	}
%>
<html>
<head>
<title>Generando Reporte</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%
	if (state==JasperForWeb.STATE_PROCESSING || (state==JasperForWeb.STATE_BUILDING && conteo.intValue()<=3)) {
%>
	<meta http-equiv="refresh" content="5">
<%}%> 

<style type="text/css">
<!--
.mensajeEspecial {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-style: normal;
	color: #336600;
}
.mensajeError {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-style: normal;
	color: #FF5500;
}
.menutitle {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
	font-size: 12px;
	font-weight: bold;
	text-decoration: none;
	background-color: #AFBEE1;
}

-->
</style>

</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
    <tr>
	    <td align="left" width="100%">
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="22%" align="left">
						<img src="/sigefirrhh/images/logo-sigefirrhh-web.jpg" />
					</td>
					<td width="64%" align="center">
					
					</td>
					<td width="78%" align="right">
						
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
    		<table width="100%" height="22" border="0" align="right" cellpadding="0" cellspacing="0"  class="menutitle">
    			<tr>
    				<td width="10%" align="center">
    					&nbsp;
    				</td>
    				<td width="80%" align="center">
    					Sistema de Gesti�n Financiera de los Recursos Humanos
    				</td>
    				<td width="10%" align="center">
    					<a href="javascript:self.close();"><img src="../images/buttons/exit.gif"/></a>
    				</td>
    			</tr>
    		</table>		  
    	</td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="200"> 
    <tr valign="center">
	    <td align="center" width="100%" class="<%=clase%>"><%=mensaje%>
	    
	        						
	    </td>
    </tr>
</table>
	    

</body>
</html>