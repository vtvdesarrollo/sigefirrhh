<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Cargos</strong></td>
								</tr>
							</table>
							<!----------->
							
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center"><a href="ManualCargo.jsp" class="linktext">Clasificadores de Cargos</a></td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center"><a href="SerieCargo.jsp" class="linktext">Series de Cargos</a></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Codificaci�n que representa un grupo de posiciones de cargo similares en cuanto a 
													obligaciones, responsabilidades, dificultad o importancia, de manera que el mismo t�tulo 
													descriptivo, los mismos requisitos m�nimos y el mismo nivel de remuneraci�n puede ser 
													aplicado a cada uno de los cargos comprendidos en la clase.  El nivel de remuneraci�n que 
													se asigna a un c�digo de cargo (clase) determinado en una escala de sueldos es denominado 
													�grado�. En esta opci�n SIGEFIRRHH incluye el concepto de �subgrado� para poder aplicar 
													el mismo tratamiento a los cargos de �grado 99�, donde el grado no representa el nivel de 
													remuneraci�n, sino que representa otra caracter�stica (tipo de cargo). Codificaci�n 
													suministrada por los �rganos rectores y por organismos (OAF).
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Manual</div></td>
												<td width="70%"><div align="left">Manual o lista de cargo al que pertenece el cargo </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo Clase/Cargo</div></td>
												<td><div align="left">C�digo que se asigna a clase de cargos, o a un cargo determinado.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Descripci�n</div></td>
												<td><div align="left">Denominaci�n o nombre de la clase o cargo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo Cargo</div></td>
												<td><div align="left">Clasificaci�n del cargo. Empleados: Alto Nivel, Administrativo, Tecnico Profesional, No Clasificado. Obreros: Calificado y No Calificado</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grado</div></td>
												<td><div align="left">Nivel de remuneraci�n asociado al cargo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Subgrado</div></td>
												<td><div align="left">Nivel de remuneraci�n asociado al cargo</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													Muestra: Descripci�n + C�digo.
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>