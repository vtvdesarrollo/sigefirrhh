<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Clasificadores de Cargos</strong><strong></strong></td>
								</tr>
							</table>
							<!----------->
							
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Debe estar creada la siguiente tabla para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Tabulador.jsp" class="linktext">Tabulador</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/estructura/Organismo.jsp" class="linktext">Organismos</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de los 
													Clasificadores, Manuales o Listas de Cargos que	est&aacute;n asociados a los diferentes 
													tipos de personal que se procesan en el organismo. Se puede incluir el manejo de series 
													adelantadas, al igual que manuales de cargo propios del organismo. Se recomienda crear 
													un manual para todos los tipos de personal, con el objetivo de poder clasificar los 
													c&oacute;dig&oacute;s de cargo que se asocian a los trabajadores. Permite indicar que 
													se pueden repetir descripciones para un mismo c&oacute;digo de cargo, como ocurre con 
													el manual de cargos de alto nivel.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">C�digo</div></td>
												<td width="70%"><div align="left">C�digo de clasificador, manual o lista de cargos </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre</div></td>
												<td><div align="left">Nombre que identifica al clasificador, manual o lista de cargos </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha de Vigencia</div></td>
												<td><div align="left">Fecha en que se aprueba la utilizaci�n del clasificador, manual o lista indicada</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Manual/Lista</div></td>
												<td><div align="left">Se clasifican en: Manuales Aprobado por MPD, Serie Adelantada, Manual Propio del Organismo, Otro </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">M�ltiples Descripciones?</div></td>
												<td><div align="left">Para indicar que se permite registrar bajo un mismo c�digo de clase de cargo varias descripciones </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tabulador</div></td>
												<td><div align="left">Si este clasificador, manual o lista de cargos tiene relacionado un tabulador de sueldos y/o salarios </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Se utiliza en procesos de selecci�n ?</div></td>
												<td><div align="left">Si este clasificador, manual o lista de cargos debe ser utilizado en procesos de selecci�n de personal, para validar requerimentos de los cargos que comprende </div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													Muestra:
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>



<body>