<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Entidades Educativas</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<td align="left">
												Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:
												<table width="30%" align="center">
													<tr>
														<td width="50%"class="datatableborder"  align="center">
															<a href="TipoEntidad.jsp" class="linktext">
																Tipo de Entidad Educativa
															</a>
														</td>
													</tr>
													<tr>
														<td width="50%"class="datatableborder"  align="center">
															<a href="../ubicacion/Pais.jsp" class="linktext">
																Pa�s
															</a>
														</td>
													</tr>
													<tr>
														<td width="50%"class="datatableborder"  align="center">
															<a href="../ubicacion/Estado.jsp" class="linktext">
																Estado
															</a>
														</td>
													</tr>
												</table>
											</td>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de las entidades 
													educativas que han de ser utilizadas para el registro de la educaci&oacute;n formal e informal de 
													los trabajadores, y para la elaboraci&oacute;n de los planes de adiestramiento y/o capacitaci&oacute;n 
													del personal del organismo.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">C�digo</div></td>
												<td width="70%"><div align="left">C�digo de entidad educativa </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Descripci�n</div></td>
												<td><div align="left">Descripci�n de entidad educativa </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Direcci�n</div></td>
												<td><div align="left">Direcci�n de la entidad educativa</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Telefonos</div></td>
												<td><div align="left">Telefonos</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Contacto</div></td>
												<td><div align="left">Persona contacto de la entidad</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr><tr>
												<td><div align="left">C�digo INCE</div></td>
												<td><div align="left">C�digo de registro en el INCE</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo INCE</div></td>
												<td><div align="left">C�digo de registro en el INCE</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">RIF N�</div></td>
												<td><div align="left">RIF de la entidad educativa</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo MED</div></td>
												<td><div align="left">C�digo de registro en el MED</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sector</div></td>
												<td><div align="left">Sector</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Entidad</div></td>
												<td><div align="left">Relaci�n con Tipo de Entidad</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Pa�s</div></td>
												<td><div align="left">Pa�s</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Estado</div></td>
												<td><div align="left">Estado del Pais</div></td>
											</tr>
										</table>
										<!------>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Muestra: Nombre + C�digo.
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>

<body>