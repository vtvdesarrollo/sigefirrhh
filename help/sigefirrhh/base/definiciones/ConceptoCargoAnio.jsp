<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Conceptos Cargo y A�o</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="TipoPersonal.jsp" class="linktext">
																	Tipo de Personal
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Concepto.jsp" class="linktext">
																	Concepto
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center"
																<a href="../cargo/ManualCargo.jsp" class="linktext">
																	Manuales de Cargos
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../cargo/Cargo.jsp" class="linktext">
																	Cargos
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td>
													Se parte de una definici&oacute;n general de conceptos, que luego son 
													asociados a los diferentes tipos de personal, con las caracter&iacute;sticas 
													propias de acuerdo al tipo, y a&uacute;n dentro de un tipo de personal, 
													se ueden definir tratamientos y/o caracter&iacute;sticas especiales del 
													mismo concepto para determinados cargos, tomando en cuenta los a&ntilde;os de 
													servicio de los trabajadores.<br>
													A trav&eacute;s de esta opci&oacute;n el sistema determinar&aacute; que conceptos 
													no pueden aplicarse bajo ning&uacute;n criterio a trabajadores con determinado cargo. 
													Igualmente determinar&aacute; si de acuerdo a los a&ntilde;os de servicio, var&iacute;a 
													el monto o porcentaje del concepto.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%" height="22"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo Personal</div></td>
												<td width="70%"><div align="left"> Tipo de Personal al que se est&aacute; asociando conceptos </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Concepto</div></td>
												<td><div align="left">Concepto que se est&aacute; asociando al tipo de personal seleccionado </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Manual de Cargos </div></td>
												<td><div align="left">Manual o relaci&oacute;n de cargos. </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cargo</div></td>
												<td><div align="left">C&oacute;digo de cargo o clase para el cual el concepto tiene caracter&iacute;sticas especiales.</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Excluir</div></td>
												<td><div align="left">Indica si este concepto se excluye para el cargo  y el n&uacute;mero de a&ntilde;os indicado. </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Ingreso Autom&aacute;tico</div></td>
												<td><div align="left">Campo que indica si el concepto debe asociarse autom&aacute;ticamente al trabajador en el momento de su ingreso, si cumple con los criterios de cargo y a&ntilde;os de servicio indicados. Por ejemplo: Bono por Jerarqu&iacute;a, Bono por Responsabilidad, etc. </div></td></tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">A&ntilde;os de Servicio </div></td>
												<td><div align="left"> Se indican los a&ntilde;os de servicio a considerar para este proceso. El valor se tomara como mayor o igual a los a&ntilde;os indicados. </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Unidades</div></td>
												<td><div align="left"> Unidades a aplicar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Monto</div></td>
												<td><div align="left"> Monto a aplicar por este concepto si cumple con los criterios de cargo y a&ntilde;os de servicio indicados. </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Porcentaje</div></td>
												<td><div align="left">Porcentaje a aplicar por este concepto si cumple con los criterios de cargo y a&ntilde;os de servicio indicados. En este caso el concepto debe estar formulado a trav&eacute;s de la opci&oacute;n de conceptos asociados (link) </div></td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: C&oacute;digo Concepto + Descripci&oacute;n Concepto
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>