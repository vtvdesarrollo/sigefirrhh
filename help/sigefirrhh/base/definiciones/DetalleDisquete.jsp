<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Detalle Archivos(Disquetes)</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de B&uacute;squeda</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Disquete.jsp" class="linktext">Disquetes</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/expediente/Personal.jsp" class="linktext">Datos Personales</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/expediente/Personal.jsp" class="linktext">Datos Trabajadores</a>
															</td>
														</tr>
													</table>
													<p>De acuerdo al tipo de archivo o disquete que se est&aacute; definiendo, deben estar creadas otras tablas tales como:</p>
													<table width="37%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Disquete.jsp" class="linktext">Ultimas N&oacute;minas Procesadas</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/expediente/Personal.jsp" class="linktext">Conceptos Fijos</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/expediente/Personal.jsp" class="linktext">Hist&oacute;rico de N&oacute;minas Procesadas</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/expediente/Personal.jsp" class="linktext">Prestaciones Mensuales</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td>
													Opci&oacute;n para el registro y actualizaci&oacute;n de la 
													estructura de los archivos o disquetes que se generan luego 
													de la ejecuci&oacute;n de determinados procesos.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%" height="22"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripción</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Disquete</div></td>
												<td width="70%">
													<div align="left">
														Seleccionar el archivo (disquete) definido previamente cuya 
														estructura se desea registrar
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Registro</div></td>
												<td>
													<div align="left">
														Seleccionar tipo de registro que se va a registrar, a saber: 
														Encabezado, Detalle o Totales
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nro. Campo</div></td>
												<td>
													<div align="justify">
														N&uacute;mero de campo dentro del registro. Es un consecutivo 
														dentro de cada tipo de registro que el sistema genera de forma 
														autom&aacute;tica.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Campo </div></td>
												<td>
													<div align="justify">
														<p>Seleccionar tipo de campo, a saber:</p>
														<p>Fijo :  Indica un valor fijo o constante ya prefijado.</p>
														<p>Base de Datos : Indica que corresponde al valor de un campo de la Base de Datos</p>
														<p>
															Entrada: Indica que se trata de un campo cuyo valor el usuario lo indicar&aacute; 
															en la forma de generaci&oacute;n del disquete.
														</p>
														<p>Contador: Indica que se trata de un contador de registros.</p>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Longitud del Campo </div></td>
												<td> <div align="justify">Longitud del campo que se esta definiendo</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Campo Valor Base Datos </div></td>
												<td>
													<div align="justify">
														Si en tipo de campo, se indic&oacute; que se trataba de un campo de la base de 
														datos, el usuario debe seleccionar el campo correspondiente. Esta selecci&oacute;n 
														es para los registros detalle. Los campos que representan una fecha, deben indicarse 
														como tres campos, a saber: dia, mes y a&ntilde;o en el orden que el usuario lo 
														requiera. Si se requiere un separador dentro de los campos fechas, este debe 
														representarse como otro campo tipo fijo.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Campo Valor BD Totales </div></td>
												<td>
													<div align="justify">
														Si el registro es tipo encabezado o total, y el campo es tipo base de datos, 
														el usuario debe seleccionar el campo correspondiente.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Campo Valor Fijo </div></td>
												<td>
													<div align="justify">
														Si en tipo de campo, se indic&oacute; que se trataba de un campo de valor fijo, 
														el usuario debe indicar el valor a registrar.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Campo Entrada </div></td>
												<td>
													<div align="justify">
														Si en tipo de campo, se indic&oacute; que se trataba de un campo de entrada, el 
														usuario debe seleccionar el campo correspondiente. Los campos que representan una 
														fecha, deben indicarse como tres campos, a saber: dia, mes y a&ntilde;o en el 
														orden que el usuario lo requiera. Si se requiere un separador dentro de los campos 
														fechas, este debe representarse como otro campo tipo fijo.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Separador Decimales </div></td>
												<td>
													<div align="justify">
														Si el campo es numerico, se debe indicar si tiene separador de decimales, a saber: 
														punto (.) o coma (,) , o no aplica.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Alineacion</div></td>
												<td><div align="justify">Indicar como se desea alinear el campo, hacia la izquierda o a la derecha.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Se rellena con </div></td>
												<td><div align="justify">Indicar como se rellena el campo hasta obtener la longitud indicada, con ceros o blancos. </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Multiplicador</div></td>
												<td>
													<div align="justify">
														Si el campo a registrar se trata de un monto numerico, que se desea registrar sin 
														separador decimal, debe indicarse 100 en este campo, a efectos de que el sistema 
														multiplique y tome los decimales.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Concepto</div></td>
												<td>
													<div align="justify">
														Si se desea registrar los montos correspondientes a un concepto, ya sea de los conceptos 
														fijos de los trabajadores, del hist&oacute;rico de n&oacute;minas procesadas, se debe 
														seleccionar el concepto.
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de B&uacute;squeda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: Nombre + C&oacute;digo Archivo (disquete)
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>