<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Par&aacute;metros Varios </strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de B&uacute;squeda</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
								  <td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite registrar valores e indicadores que son 
													utilizados para la ejecuci&oacute;n de otros procesos, tales como 
													c&aacute;lculo del bono vacacional, bono de fin de a&ntilde;o, 
													prestaciones mensuales, entre otros.
											    </td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%" height="22"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal </td>
												<td width="70%"><div align="left">Relaci&oacute;n con Tipo de Personal </td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grupo de Organismo </td>
												<td><div align="left">Relaci&oacute;n con Grupo de Organismo </td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Se calculan prestaciones con Nuevo Regimen?</td>
												<td><div align="left">Indicar si el  c&aacute;lculo de las prestaciones se realiza bajo el Nuevo Regimen.</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C&aacute;lculo de prestaciones mensuales toma alicuota del Bono Fin de A&ntilde;o? </td>
												<td><div align="left">Indicar si en el c&aacute;lculo de las prestaciones mensuales se imputa la alicuota correspondiente al bono de fin de a&ntilde;o. La alicuota del bono de fin de a&ntilde;o es un concepto reservado por SIGEFIRRHH c&oacute;digo 1601 </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo C&aacute;lculo Bono de Fin de A&ntilde;o? </td>
												<td><div align="left">Indicar si el tipo de C&aacute;lculo es en base a dias o porcentaje.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C&aacute;lculo de Bono Vacacional toma alicuota del Bono Fin de A&ntilde;o? </td>
												<td><div align="left">Indicar si en el c&aacute;lculo del Bono Vacacional se imputa la alicuota correspondiente al bono de fin de a&ntilde;o. La alicuota del bono de fin de a&ntilde;o es un concepto reservado por SIGEFIRRHH c&oacute;digo 1601 </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�lculo alicuota Bono Fin de A�o es sobre?</td>
												<td><div align="left">Indicar si el c�lculo de la alicuota del bono de fin de a�o, se debe realizar sobre los conceptos fijos del trabajador o sobre el devengado del mes.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C&aacute;lculo de prestaciones mensuales toma alicuota del Bono Vacacional? </td>
												<td><div align="left">Indicar si en el c&aacute;lculo de las prestaciones mensuales se imputa la alicuota correspondiente al bono vacacional. La alicuota del bono vacacional es un concepto reservado por SIGEFIRRHH c&oacute;digo 1501 </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Apertura Fideicomiso</td>
												<td><div align="left">Si el organismo ya apertur� el fideicomiso para las prestaciones de los trabajadores, debe indicar la fecha de apertura.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C&aacute;lculo de Bono Fin de A&ntilde;o toma alicuota del Bono Vacacional? </td>
												<td><div align="left">Indicar si en el c&aacute;lculo del Bono de Fin de A&ntilde;o se imputa la alicuota correspondiente al bono vacacional. La alicuota del bono vacacional es un concepto reservado por SIGEFIRRHH c&oacute;digo 1501</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">D&iacute;as del a&ntilde;o para c&aacute;lculos </td>
												<td><div align="left">N&uacute;mero de d&iacute;as del a&ntilde;o a ser utilizado en los c&aacute;lculos, ej: 360, 365. </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">D&iacute;as a tomar para el c&aacute;lculo del Bono de Fin de A&ntilde;o </td>
												<td><div align="left">N&uacute;mero de d&iacute;as del a&ntilde;o a ser utilizado para el c&aacute;lculo del devengado base para el Bono de Fin de A&ntilde;o </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Ingreso Tope Bono Fin A�o</td>
												<td><div align="left">Se debe indicar la fecha de ingreso tope que debe evaluarse en el proceso de calculo del bono de fin de a�o.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cuando se paga el Bono Vacacional? </td>
												<td><div align="left">Indicar si el Bono Vacacional se cancela en el mes aniversario, o enel per&iacute;odo de disfrute. El proceso utilizar&aacute; la fecha registrada en los datos del trabajador como &quot;fecha vacaciones&quot;. </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Se otorga d&iacute;as extras de Bono Vacacional? </td>
												<td><div align="left">Indicar si adem&aacute;s del Bono Vacacional, se cancelan d&iacute;as extras al trabajador en su fecha aniversario. Si la respuesta es s&iacute;, el monto a cancelar por Bono Extra es un concepto reservado por SIGEFIRRHH c&oacute;digo 1502 </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">A Fecha de Vacaciones se suman los a�os APN?</td>
												<td><div align="left">Se debe indicar si para el calculo del bono vacacional, se debe evaluar la fecha indicada como fecha de vacaciones, o se debe sumar el n�mero de a�os en APN.</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Proces&oacute deuda de r&eacute;gimen derogado?</td>
												<td><div align="left">Aplicar el indicador respectivo.</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C&aacute;lculo de Intereses Adicionales?</td>
												<td><div align="left">Indicar si se calculan intereses adicionales.</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Descuento por Ausencia Injustificada?</td>
												<td><div align="left">Se�alar si se aplican descuentos por ausencia injustificada para este tipo de personal.</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C&aacute;lculo de Bono Vacacional toma alicuota del Bono Petrolero? </td>
												<td><div align="left">Indicar si en el c&aacute;lculo del Bono Vacacional se imputa la alicuota correspondiente al bono petrolero. La alicuota del bono petrolero es un concepto reservado por SIGEFIRRHH c&oacute;digo 1701 </td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Constante (1) </td>
												<td><div align="left">Constante utilizada en la f&oacute;rmula del Bono Petrolero </td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Constante (2) </td>
												<td><div align="left">Constante utilizada en la f&oacute;rmula del Bono Petrolero </td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Constante (3) </td>
												<td><div align="left">Constante utilizada en la f&oacute;rmula del Bono Petrolero </td>
											</tr>
											
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cambio Oficial Moneda</td>
												<td><div align="left"></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tope Horas Extra Anual</td>
												<td><div align="left">Se�alar el tope de horas extras anuales que le es permitido a este tipo de personal.</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tope Horas Extras en Per&iacute;odo</td>
												<td><div align="left">Se�alar el tope de Horas Extras Mensuales que le es permitido a este tipo de personal.</td>
											</tr>
											
									  </table>
								  </td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de B&uacute;squeda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: Tipo de Personal
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>