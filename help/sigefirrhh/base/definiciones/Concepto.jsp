<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Conceptos</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de B&uacute;squeda</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>No existen requisitos previos para la ejecuci&oacute;n de esta opci&oacute;n:</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td>
													Definici&oacute;n de las diferentes asignaciones que devengan los trabajadores 
													de un organismo, y las diferentes deducciones que pueden ser aplicadas en sus 
													procesos de pago. En pro de lograr al m&aacute;ximo la estandarizaci&oacute;n 
													de los procesos en la Administraci&oacute;n P&uacute;blica se han 
													codificado previamente los principales conceptos que utiliza el sistema. Las 
													caracter&iacute;sticas que se describen a continuaci&oacute;n son comunes para 
													el proceso de pago de todos los tipos de personal
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">C&oacute;digo</div></td>
												<td width="70%"><div align="left">
												  <p>C&oacute;digo que identifica al concepto. Son cuatro (4) d&iacute;gitos con el siguiente rango: </p>
												  <p>0001 - 5000 Asignaciones<br>
												    5001 - 9999 Deducciones</p>
												  <p>El c&oacute;digo 0000 se utiliza para representar al sueldo/salario integral mensual </p>
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Descripci&oacute;n</div></td>
												<td><div align="left">Nombre del concepto asociado eje: sueldo, compensacion, etc.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Unidad</div></td>
												<td><div align="left">Indicar si el concepto representa horas, d&iacute;as o un monto fijo. Por ejemplo el concepto sueldo representa el pago de XX d&iacute;as trabajados, el concepto horas extras, representa el pago de XX horas trabajadas, el concepto de Prima por hijos, representa el pago de un monto fijo XXXX por cada hijo </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C&oacute;digo Reservado por SEGEFIRRHH?</div></td>
												<td>
													<div align="left">
														<p>Indicar si el concepto que se est&aacute; definiendo es un c&oacute;digo reservado por el sistema para diferentes procesos. Por ejemplo: 0001 � Sueldo<br>
											    		5001 � Retenci&oacute;n SSO</p>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Forma parte de Sueldo Integral</div></td>
												<td><div align="left">Indicar si el concepto es considerado componente del sueldo integral.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Forma parte de Sueldo B&aacute;sico</div></td>
												<td><div align="left">Indicar si el concepto corresponde al sueldo/salario b�sico.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Ajustes del Sueldo B�sico?</div></td>
												<td><div align="left">Indicar si el concepto corresponde a alg�n ajuste del sueldo/salario b�sico.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Forma parte de la Compensaci�n?</div></td>
												<td><div align="left">Indicar si el concepto corresponde a una compensacion propia del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Corresponde a una Prima del Cargo?</div></td>
												<td><div align="left">En la columna de primas asociadas al cargo que desempe&ntilde;a el trabajador. Por ejemplo; Prima por Jerarqu&iacute;a, Prima por Responsabilidad </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Corresponde a una Prima del Trabajador?</div></td>
												<td><div align="left">En la columna de primas asociadas al trabajador propiamente de acuerdo a una condici&oacute;n suya. Por ejemplo; Prima Profesionalizaci&oacute;n, Prima por Antig&uuml;edad </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Es Gravable?</div></td>
												<td><div align="left">Indicar si el concepto que se est&aacute; definiendo esta sujeto a retenci&oacute;n del ISLR </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo</div></td>
												<td><div align="left">Indicar si el concepto es: pr�stamo, aporte patronal, retroactivo, alicuota de otro concepto, ticket, ocurre al aniversario, deduccion de caja de ahorro</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Es una Cuota Sindicato?</div></td>
												<td><div align="left">Indicar si el concepto de deducci&oacute;n que se est&aacute; definiendo est&aacute; asociado a un sindicato </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Es una Cuota Gremio?</div></td>
												<td><div align="left">Indicar si el concepto de deducci&oacute;n que se est&aacute; definiendo est&aacute; asociado a un gremio </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Es un Sobretiempo?</div></td>
												<td><div align="left">Representa un sobretiempo (S/N)</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Es un Beneficio?</div></td>
												<td><div align="left">Representa un beneficio (S/N)</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Es base para el proceso de Jubilaci�n?</div></td>
												<td><div align="left">Se incluye base para jubilaci&oacute;n (S/N)</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Relacionado a una Caja Ahorros?</div></td>
												<td><div align="left">Indicar si el concepto esta relacionado a una caja de ahorros (S/N) </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Caja Ahorros Asociada</div></td>
												<td><div align="left">Indicar a que caja de ahorros esta relacionado el concepto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tiene Aporte Patronal Asociado</div></td>
												<td><div align="left">Indicar si el concepto que se est&aacute; definiendo tiene asociado un aporte patronal (S/N)</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Aporte Patronal Asociado</div></td>
												<td><div align="left">Si el concepto tiene aporte patronal asociado, se debe seleccionar el concepto correspondiente al aporte, el cual debe estar ya registrado.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Tiene Retroactivo Asociado?</div></td>
												<td><div align="left">Se debe indicar si tiene retroactivo asociado.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Retroactivo Asociado?</div></td>
												<td><div align="left">Si el concepto tiene retroactivo asociado, se debe seleccionar el concepto correspondiente al retroactivo, el cual ya debe estar ya registrado.</div></td>
											</tr>
																						<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Aplica Descuento por Ausencia?</div></td>
												<td><div align="left">Indicar si este concepto esta asociado al descuento.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Ausencia</div></td>
												<td><div align="left">Se�alar el tipo de descuento por ausencia.</div></td>
											</tr>
											
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de B&uacute;squeda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: C&oacute;digo + Descripci&oacute;n
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>