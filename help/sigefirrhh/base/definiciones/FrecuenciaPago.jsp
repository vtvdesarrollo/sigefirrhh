<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Frecuencias de Pago</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													<p>No existen requisitos previos para la ejecuci&oacute;n de esta opci&oacute;n</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Definicion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_definicion">Definici&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%">
											<tr>
												<td align="center">
													<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
														<tr>
															<td>
																<p>Se dnomina Frecuencia a la ocurrencia de una asignaci&oacute;n o deducci&oacute;n en los 
																per&iacute;odos de pago, viene determinada por la frecuencia de pago 
																que se le asocia, a saber</p>
																
																<table width="200%"  border="0" cellspacing="2" cellpadding="0" class="datatable">
																	<tr>
																		<td width="20%"><strong>C&oacute;digo</strong></td>
																		<td width="80%"><strong>Descripci&oacute;n</strong></td>
																	</tr>
																	<tr>
																		<td>1</td>
																		<td>1ra quincena </td>
																	</tr>
																	<tr>
																		<td>2</td>
																		<td>2da quincena</td>
																	</tr>
																	<tr>
																		<td>3</td>
																		<td>Ambas quincenas</td>
																	</tr>
																	<tr>
																		<td>4</td>
																		<td>Semanal</td>
																	</tr>
																	<tr>
																		<td>5</td>
																		<td>1ra. semana del mes</td>
																	</tr>
																	<tr>
																		<td>6</td>
																		<td>2da. semana del mes</td>
																	</tr>
																	<tr>
																		<td>7</td>
																		<td>3ra. semana del mes</td>
																	</tr>
																	<tr>
																		<td>8</td>
																		<td>4ta. semana del mes</td>
																	</tr>
																	<tr>
																		<td>9</td>
																		<td>5ta. semana del mes o &uacute;ltima semana</td>
																	</tr>
																	<tr>
																		<td>10</td>
																		<td>semanal, pero solo las primeras 4 semanas del mes, es decir no ocurre en la 5ta. semana.</td>
																	</tr>
																</table>
																
																<p>Para registrar pagos que se realizar&aacute;n fuera de los 
																per&iacute;odos ordinarios de pagos (quincenas, semanas o meses), 
																y ser&aacute;n registrados como n&oacute;minas especiales, se utilizan 
																valores de frecuencias mayores a 10. Estas &uacute;ltimas son definidas 
																por el usuario a su criterio</p>
															</td>
														</tr>
													</table>
													
													<table class="bandtable" width="95%" align="center">
														<tr>
															<td width="30%"><div align="left">Campo</div></td>
															<td width="70%"><div align="left">Descripci�n</div></td>
														</tr>
													</table>
													<table width="95%" class="datatableborder" align="center">
														<tr>
															<td width="30%"><div align="left">C�digo</div></td>
															<td width="70%"><div align="left">C&oacute;digo de Frecuencia de Pago</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Descripci�n</div></td>
															<td><div align="left">Descripci�n de la Frecuencia</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Reservado</div></td>
															<td><div align="left">Es reservada por SIGEFIRRHH (S/N)</div></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">Muestra: C�digo de Frecuencia + Nombre</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
