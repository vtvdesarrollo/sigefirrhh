<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>F&oacute;rmula Bono Vacacional </strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="TipoPersonal.jsp" class="linktext">Tipo Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="ConceptoTipoPersonal.jsp" class="linktext">Concepto Tipo Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="ParametroVarios.jsp" class="linktext">Par�metros Varios</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite parametrizar la forma de c&aacute;lculo y pago 
													del Bono Vacacional de los trabajadores. Los criterios registrados en esta 
													opci&oacute;n son utilizados por el proceso de c&aacute;lculo del bono vacacional. 
													Debe estar definido previamente el concepto de alicuota de bono de fin de a&ntilde;o, 
													para que sea tomada en cuenta si as&iacute; lo indica el usuario.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%" height="22"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo Personal</div></td>
												<td width="70%"><div align="left">Relacion con Tipo Personal</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Concepto</div></td>
												<td>
													<div align="left">
														Concepto a procesar para el c&aacute;lculo del Bono Vacacional. 
														SIGEFIRRHH reserva para el Bono Vacacional el c&oacute;digo '1500', 
														y para la alicuota del bono vacacional a ser tomada en cuenta para el 
														c&aacute;lculo de otros conceptos, SIGEFIRRHH reserva el c&oacute;digo '1501'
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo C&aacute;lculo</div></td>
												<td>
													<div align="left">
														<p>Tratamiento que se va a dar el concepto seleccionado, a saber: </p>
														<p>B-B&aacute;sico, se toma el valor que tiene el concepto registrado en los conceptos 
														fijos del trabajador, </p>
														<p>P-Promedio, se busca el promedio devengado por el trabajador de ese concepto en el 
														periodo indicado,</p>
														<p>D-Devengado, se busca el monto devengado de ese concepto en el a&ntilde;o. Los 
														d&iacute;as del a&ntilde;o los toma del registro de par&aacute;metros varios para ese 
														tipo de personal.</p>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">A partir del &uacute;ltimo mes cerrado?</div></td>
												<td>
													<div align="left">
														Si se va a procesar el promedio de un concepto, debe indicarse si se recorre desde 
														el &uacute;ltimo mes cerrado, &oacute; &uacute;ltima n&oacute;mina.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Meses de 30 d&iacute;as?</td>
												<td>
													<div align="left">
														Indicar si para el c&aacute;lculo del promedio, los meses a procesar se consideran 
														de 30 d&iacute;as, y no d&iacute;as calendario.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Meses a promediar</div></td>
												<td><div align="left">N&uacute;mero de meses a promediar.</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Factor</div></td>
												<td><div align="left">Factor a multiplicar</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tope Monto</div></td>
												<td><div align="left">Tope del concepto en monto</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tope Unidades</div></td>
												<td><div align="left">Tope de unidades del concepto</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cruce de Alicuotas?</div></td>
												<td>
													<div align="left">
														Se va a imputar la alicuota del concepto que se est� registrando. Y debe indicarse 
														si hay un cruce de alicuotas, para que se registre de forma separada al momento del 
														pago. En el caso de que se deba imputar la alicuota correspondiente al bono de fin 
														de a�o, debe indicarse en la opci�n de par�metros varios.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Concepto de Alicuotas</td>
												<td>
													<div align="left">
														Si la respuesta anterior es SI, debe seleccionarse el concepto correspondiente a la alicuota
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: Descripci&oacute;n de Concepto Tipo Personal + Tipo Calculo
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>