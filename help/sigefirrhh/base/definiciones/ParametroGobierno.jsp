<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Par&aacute;metros Retenciones </strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="ConceptoTipoPersonal.jsp" class="linktext">Concepto Tipo Personal</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
								  <td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite registrar todos los valores predeterminados para ser 
													utilizados en el c&aacute;lculo de las retenciones de ley, a saber:<br>
													<br>
													L&iacute;mites Semanales<br>
												    L&iacute;mites Mensuales<br>
												    Edades m&aacute;ximas para poder cotizar, tanto hombres como mujeres.<br>
												    Porcentajes a aplicar para la cotizaci&oacute;n del trabajador<br>
											        Porcentajes a aplicar para el c&aacute;lculo del aporte patronal
											    </td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%" height="22"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripción</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Grupo Organismo</td>
												<td width="70%"><div align="left">Relaci&oacute;n con GrupoOrganismo</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite semanal SSO</td>
												<td><div align="left">L&iacute;mite semanal SSO</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite mensual SSO</td>
												<td><div align="left">L&iacute;mite mensual SSO</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite edad masculino SSO</td>
												<td><div align="left">Edad maxima SSO masculino</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite edad femenino SSO</td>
												<td><div align="left">Edad maxima SSO femenino</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% Trabajador Regimen Integral</td>
												<td><div align="left">Porcentaje regimen integral</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% Trabajador Regimen Parcial </td>
												<td><div align="left">Porcentaje regimen parcial</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% patr&oacute;n riesgo bajo SSO</td>
												<td><div align="left">Porcentaje SSO patr&oacute;n Riesgo Bajo</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% patr&oacute;n riesgo medio SSO</td>
												<td><div align="left">Porcentaje SSO patr&oacute;n Riesgo medio</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% trabajador riesgo alto SSO</td>
												<td><div align="left">Porcentaje SSO patr&oacute;n Riesgo alto</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite semanal SPF</td>
												<td><div align="left">l&iacute;mite semanal para SPF</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite mensual SPF</td>
												<td><div align="left">l&iacute;mite mensual para SPF</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% trabajador SPF</td>
												<td><div align="left">Porcentaje SPF trabajador</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% patr&oacute;n SPF</td>
												<td><div align="left">Porcentaje SPF Patr&oacute;n</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite semanal LPH</td>
												<td><div align="left">l&iacute;mite semanal para LPH</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite mensual LPH</td>
												<td><div align="left">l&iacute;mite mensual para LPH</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite Edad masculino LPH</td>
												<td><div align="left">Edad maxima LPH masculino</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite Edad femenino LPH</td>
												<td><div align="left">Edad maxima LPH femenino</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% trabajador LPH</td>
												<td><div align="left">Porcentaje LPH trabajador</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% patr&oacute;n LPH</td>
												<td><div align="left">Porcentaje LPH patr&oacute;n</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite semanal FJU</td>
												<td><div align="left">l&iacute;mite semanal para FJU</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite mensual FJU</td>
												<td><div align="left">l&iacute;mite mensual para FJU</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite Edad masculino FJU</td>
												<td><div align="left">Edad maxima FJU masculino</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">L&iacute;mite Edad femenino FJU</td>
												<td><div align="left">Edad maxima FJU femenino</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% trabajador FJU</td>
												<td><div align="left">Porcentaje FJU trabajador</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% patr&oacute;n FJU</td>
												<td><div align="left">Porcentaje FJU patr&oacute;n</td>
											</tr>
									  </table>
								  </td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: L&iacute;mite mensual SSO + L&iacute;mite semanal SSO
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>