<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Tipos de Personal</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
													<li><a class="Link" href="#_procedimiento">Procedimiento</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../estructura/GrupoOrganismo.jsp" class="linktext">Grupo Organismo</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="GrupoNomina.jsp" class="linktext">Grupo N&oacute;mina</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Turno.jsp" class="linktext">Turno</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td>
													Agrupaci&oacute;n que se hace a un grupo de trabajadores de un organismo, 
													que tengan caracter&iacute;sticas comunes tales como: clasificaci&oacute;n 
													de personal asociada, estructura de cargos a la cual pertenecen, beneficios 
													colectivos, modalidades de pago, etc.<br>
													En el caso de fusi&oacute;n de organismos que manejen a&uacute;n internamente 
													estructuras de cargos diferentes para el mismo tipo de personal que provienen 
													de las estructuras anteriores, deben seguir registr&aacute;ndose de forma 
													separada, hasta que la nueva estructura sea implementada.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">C�digo</div></td>
												<td width="70%"><div align="left">Codificaci&oacute;n que se da al tipo de personal para su identificaci&oacute;n</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre</div></td>
												<td><div align="left">Nombre que se asigna al tipo de personal </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Clasificaci&oacute;n de Personal</div></td>
												<td><div align="left">Seleccionar la clasificaci&oacute;n de personal de acuerdo a la lista.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grupo de N�mina</div></td>
												<td><div align="left">Seleccionar el Grupo de N�mina de acuerdo a la lista.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grupo de Organismo</div></td>
												<td><div align="left">Seleccionar el Grupo de Organismo de acuerdo a la lista.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Movimiento sujeto a LEFP?</div></td>
												<td><div align="left">Indicar si los movimientos de personal relacionados a este tipo de personal estan sujetos a la Ley del Estatuto de la Funci�n Publica.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Maneja Registro de Cargos/Puestos?</div></td>
												<td>
													<div align="left">
														Campo que nos indica si los trabajadores asociados a este tipo de personal 
														est&aacute;n asociados a una estructura o registro de cargos, sin que esto 
														implique aprobaci&oacute;n alguna por parte de MPD
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Se calculan prestaciones?</div></td>
												<td>
													<div align="left">
														Campo que indica si ese tipo de personal debe ser inclu&iacute;do 
														en el proceso de c&aacute;lculo de abono mensual de prestaciones sociales
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Se asignan dotaciones?</div></td>
												<td><div align="left"></div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Se realizan aumentos por Evaluaci�n?</div></td>
												<td><div align="left"></div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Goza Beneficio de Cesta Ticket?</div></td>
												<td><div align="left"></div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">F�rmula Salario Integral?</div></td>
												<td>
													<div align="left">
														Si el tipo de personal pertenece a un grupo con procesos semanales de c&aacute;lculo 
														y pago de n&oacute;minas, se debe definir el procedimiento que utiliza el organismo 
														para el c&aacute;lculo del salario Integral Mensual, a saber:<br>
														(1) Salario integral = (pagos semanales / 7) * 30) + pagos mensuales<br>
														(2) Salario semanal = pagos semanales + pagos mensuales que ocurren esa semana<br>
														(3) Salario semanal = (pagos semanales * 52) / 12) + pagos mensuales </p>
												  </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">F�rmula Salario Semanal?  </div></td>
												<td>
													<div align="left">
														Si el tipo de personal pertenece a un grupo con procesos semanales de c&aacute;lculo 
														y pago de n&oacute;minas, se debe definir el procedimiento que utiliza el organismo para 
														el c&aacute;lculo del salario semanal, a saber:<br>
														(1) Salario semanal = pagos semanales + ((pagos mensuales / 30) *7)<br>
														(2) Salario semanal = pagos semanales + pagos mensuales que ocurren esa semana<br>
														(3) Salario semanal = pagos semanales + ((pagos mensuales *12) / 52) </p>
												  </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Permite varios registros de un trabajador activo?</div></td>
												<td>
													<div align="left">
														En la mayor&iacute;a de los casos, un trabajador solo puede estar asociado, 
														como trabajador activo, a un tipo de personal, sin embargo en el caso del personal 
														docente puede estar registrado tantas veces como funciones docentes est&eacute; 
														desempe&ntilde;ando en diferentes planteles, ya que los procesos de pago de nominas 
														reflejaran a estos trabajadores en cada dependencia educativa de forma separada. 
														Para el manejo de esta situaci&oacute;n se incorpor&oacute; este indicador
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Cotiza SSO?</div></td>
												<td><div align="left">Indicar si los trabajadores asociados a este tipo de personal cotizan al SSO.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Cotiza SPF?</div></td>
												<td><div align="left">Indicar si los trabajadores asociados a este tipo de personal cotizan al SPF.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Cotiza LPH?</div></td>
												<td><div align="left">Indicar si los trabajadores asociados a este tipo de personal cotizan al LPH.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Cotiza FJU?</div></td>
												<td><div align="left">Indicar si los trabajadores asociados a este tipo de personal cotizan al FJU.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Disfruta Vacaciones</div></td>
												<td><div align="left"></div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Turno</div></td>
												<td><div align="left">Indicar el turno que corresponde a este tipo de personal.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Tiene deuda de regimen derogado?</div></td>
												<td><div align="left">Indicar si la deuda del regimen derogado de prestaciones ya fue cancelada para este tipo de personal</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Banco N�mina</div></td>
												<td><div align="left">Seleccionar el banco donde se realizan los dep�sitos de n�mina</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Forma de Pago</div></td>
												<td><div align="left">Seleccionar la forma de pago por defecto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Banco LPH</div></td>
												<td><div align="left">Seleccionar el banco donde se depositan las cotizaciones de pol�tica habitacional</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr> 
											<tr>
												<td><div align="left">Banco Fideicomiso</div></td>
												<td><div align="left">Seleccionar el banco donde se depositan las prestaciones en fideicomiso</div></td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">Muestra: Nombre + C�digo Tipo personal</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Procedimiento-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_procedimiento">Procedimiento</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="center">
													<img src="/sigefirrhh/help/images/base/definiciones/TipoPersonal.gif" border="0">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>