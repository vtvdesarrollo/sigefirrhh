<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Conceptos por Tipo de Personal</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Concepto.jsp" class="linktext">Concepto</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="FrecuenciaTipoPersonal.jsp" class="linktext">Frecuencia  Tipo de Personal</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td>
													<p>Se parte de una definici&oacute;n general de conceptos, que luego son asociados a los 
													diferentes tipos de personal, con las caracter&iacute;sticas propias de acuerdo al tipo. 
													Por ejemplo: el concepto de sueldo mensual y/o quincenal no se puede asociar a un tipo de 
													personal que tiene procesos de pago semanales, o a la inversa, el concepto de salario semanal, 
													no debe asociarse a un tipo de personal con procesos de pago quincenales o mensuales. Igualmente 
													se podr&aacute; definir un mismo concepto con condiciones distintas para los diferentes tipos 
													de personal. Por ejemplo: Prima antiguedad para empleados se paga en la 2da quincena, y a los 
													obreros se les paga semanal, la retenci&oacute;n del SSO a los empleados se realiza quincenal, 
													y a los obreros se realiza semanal.</p>
													<p>A trav&eacute;s de esta opci&oacute;n el sistema determinar&aacute; a que tipos de personal 
													se les va a calcular las retenciones de ley , ya que verificar&aacute; si est&aacute;n asociadas 
													o no. Por ejemplo: los empleados tendr&aacute;n asociados el concepto de retenci&oacute;n SSO 
													y el personal jubilado no lo tendr&aacute; asociado</p>
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%" height="22"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo Personal</div></td>
												<td width="70%"><div align="left">Tipo de Personal al que se est&aacute; asociando conceptos</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Concepto</div></td>
												<td><div align="left">Concepto que se est&aacute; asociando al tipo de personal seleccionado</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Frecuencia</div></td>
												<td><div align="left">Relaci&oacute;n con FrecuenciaTipoPersonal</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo</div></td>
												<td>
													<div align="left">
														<p>Campo que indica como calcular el monto del concepto en caso de que est&eacute; 
														asociado a alguna f&oacute;rmula, a saber:</p>
														<p>P � Porcentaje Indica que el monto del concepto se calcula al aplicar un porcentaje 
														a la sumatoria de montos de otros conceptos (link conceptos asociados). Por ejemplo: Bono 
														Nocturno es igual a 30% del sueldo.</p>
														<p>M � Monto Fijo Indica que el monto del concepto es un monto fijo.<br>
														Por ejemplo: Prima de Transporte son Bs. 200 diarios.</p>
														<p>I � Porcentaje Individual - Indica que el monto del concepto se calcula al aplicar 
														un porcentaje a la sumatoria de montos de otros conceptos (link conceptos asociados), 
														a diferencia del tipo 'P', el porcentaje a utilizar es individual de un trabajador. 
														Por ejemplo: Embargo de Sueldo es un porcentaje individual de acuerdo a la sentencia</p>
														<p>D� Diferencial - Indica que el monto del concepto se calcula al aplicar un diferencial 
														en los conceptos asociados (link). Es decir este c&aacute;lculo resulta al indicar que la 
														suma de varios conceptos debe llegar a un l&iacute;mite establecido. Ejemplo, al determinar 
														el salario m&iacute;nimo, y se establece que debe ser completado a trav&eacute;s de un 
														concepto de ajuste, ya que no se ha indicado que se var&iacute;e el sueldo b&aacute;sico.</p>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Monto</div></td>
												<td>
													<div align="left">
														Indica el porcentaje a aplicar, el monto fijo del concepto, o el tope para calcular 
														el diferencial
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Unidades</div></td>
												<td>
													<div align="left">
														Indica las cantidad o unidades a pagar de este concepto, por defecto, al tipo de 
														personal seleccionado
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Ingreso Autom&aacute;tico</div></td>
												<td>
													<div align="left">
														Campo que indica si el concepto debe asociarse autom&aacute;ticamente al 
														trabajador en el momento de su ingreso. Por ejemplo: sueldo, caja de ahorro, etc
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Se realiza rec�lculo autom�tico?</div></td>
												<td><div align="left">
													Se debe indicar si este concepto debe recalcularse de forma automatica al 
													solicitarlo en el proceso de prenomina. Solo los conceptos con este indicador 
													en "SI" ser�n rcalculados en cualquiera de los procesos de Actualizaci�n de 
													Conceptos. Los conceptos de Sueldo, Salario, SSO, SPF, LPH, FJU, Prima 
													Antiguedad y Bono Vacacional no deben tener este indicador en "SI".
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tope M&aacute;ximo </div></td>
												<td><div align="left">Monto m&aacute;ximo permitido de este concepto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tope M&iacute;nimo</div></td>
												<td><div align="left">Monto m&iacute;nimo permitido de este concepto </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tope Anual</div></td>
												<td><div align="left">Monto m&aacute;ximo anual permitido de este concepto </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Es base jubilacion?</div></td>
												<td><div align="left">Se debe indicar si este concepto es tomado como base de c�lculo para el proceso de c�lculo de jubilaci�n.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Formulaci�n Anual</div></td>
												<td>
													<div align="left">
														Campo que indica si este concepto ser incluido en el proceso de formulaci&oacute;n 
														de presupuesto
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Multiplicador</div></td>
												<td><div align="left">Multiplicador en formula</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Se refleja en movimientos?</div></td>
												<td><div align="left">Indicar si este concepto debe reflejarse en los formatos de movimientos de personal</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Lo aprueba MPD?</div></td>
												<td><div align="left">Indicar si la aplicaci�n de este concepto debe ser aprobada por el organo rector MPD</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Distribuci�n Presupuestaria</div></td>
												<td><div align="left">
													Se debe indicar el tratamiento que debe tener este concepto para los procesos 
													relacionados con la formulaci�n y ejecuci�n presupuestaria. El sistema ofrece 
													las siguientes opciones: El monto se distribuye a las mismas acciones espec�ficas 
													que tiene asociadas el trabajador que devenga el concepto, o tiene una 
													distribuci�n global independiente a la distribuci�n del trabajador.
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Contrato Colectivo</div></td>
												<td><div align="left">Si el concepto est� asociado a un contrato colectivo, se selecciona de la lista</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Base Legal</div></td>
												<td><div align="left">Base Legal o cl&aacute;usula que soporta este concepto</div></td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: C&oacute;digo Concepto + Descripci&oacute;n Concepto
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>