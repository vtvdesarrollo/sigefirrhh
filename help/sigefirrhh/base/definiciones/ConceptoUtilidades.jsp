<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>F&oacute;rmula Bono Fin de A&ntilde;o </strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="TipoPersonal.jsp" class="linktext">Tipo Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="ConceptoTipoPersonal.jsp" class="linktext">Concepto Tipo Personal</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="ParametroVarios.jsp" class="linktext">Par�metros Varios</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, actualizaci�n  y consulta de los conceptos que son utilizados para el c�lculo del bono de fin de a�o. El sistema ofrece las siguientes modalidades:<br>
B�sico: el sistema toma los conceptos del trabajador registrados en conceptos fijos.<br>
Promedio: el sistema lee del hist�rico de nominas los conceptos que fueron devengados en el n�mero de meses indicados, partiendo de la �ltima n�mina cerrada o mes cerrado, de acuerdo a lo que indique el usuario.<br>
Devengado Anual: el sistema lee del hist�rico de n�minas los montos devengados por ese concepto, y los divide entre el n�mero de d�as indicados en el campo 'cantidad de d�as para dividir devengados anuales'<br>
Devengado Anual y Proyectado: el sistema lee del hist�rico de n�minas los montos devengados por ese concepto, y le suma los montos actuales (conceptos fijos) multiplicados por el numero de meses a proyectar que se indic�.<br>
Al Mes: el sistema lee del hist�rico de n�minas los montos devengados por ese concepto en el mes indicado. Este monto se divide entre los dias del a�o<br>
Pendientes por Pagar: el sistema lee de conceptos variables los montos registrados por estos conceptos y los divide entre los dias del a�o<br>
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%" height="22"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo Personal</div></td>
												<td width="70%"><div align="left">Relaci�n con Tipo Personal</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr><tr>
												<td><div align="left">Concepto</div></td>
												<td><div align="left">Relaci�n con Concepto Tipo Personal</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo C&aacute;lculo</div></td>
												<td>
													<div align="left">
														Tipo de C&aacute;lculo:<br>
														B&aacute;sico<br>
														Promedio en un per&iacute;odo<br>
														Devengado en el a&ntilde;o<br>
														Devengado anual y proyectado<br>
														Al Mes<br>
														Pendientes por Pagar(Variables)
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">A partir del mes?</div></td>
												<td><div align="left">Mes inicial a promediar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Hasta el mes?</div></td>
												<td><div align="left">Mes final a promediar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N�mero de d�as a dividir </div></td>
												<td><div align="left">N�mero de d�as a dividir para obtener el promedio entre los meses indicados</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N�mero de meses a proyectar </div></td>
												<td><div align="left">N�mero de meses en el a�o que se va a proyectar un concepto para obtener el devengado anual</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Devengado en el mes</div></td>
												<td><div align="left">Indicar el mes para buscar el monto devengado del concepto indicado, en el caso de las n�minas quincenales</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Devengado en la semana (del a�o)</div></td>
												<td><div align="left">Indicar la semana para buscar el monto devengado del concepto indicado, para las n�minas semanales</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Factor</div></td>
												<td><div align="left">Factor a multiplicar en la f�rmula</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tope Monto</div></td>
												<td><div align="left">Monto m�ximo a comparar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tope Unidades</div></td>
												<td><div align="left">M�ximo n�mero de unidades a comparar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: Descripci&oacute;n Concepto Tipo Personal + Tipo
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
