<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		<a  name="_inicio">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Categoria de Personal</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>No existen requisitos previos para la ejecuci&oacute;n de esta opci&oacute;n:</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%">
											<tr>
												<td align="center">
													<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
														<tr>
															<td>
																Opci&oacute;n que permite el registro de los Tipos de Categorias de Personal
															</td>
														</tr>

													</table>
													
													<table class="bandtable" width="95%" align="center">
														<tr>
														  <td width="20%" height="22"><div align="left">Campo</div></td>
															<td width="31%"><div align="left">Descripción</div></td>
														</tr>
													</table>
													<table width="95%" class="datatableborder" align="center">
														<tr>
															<td width="20%"><div align="left">Concepto</div></td>
															<td width="31%"><div align="left">Relaci&oacute;n con Concepto</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Beneficiario</div></td>
															<td><div align="left">Nombre Beneficiario</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">C&eacute;dula</div></td>
															<td><div align="left">C&eacute;dula beneficiario</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">N&ordm; RIF</div></td>
															<td><div align="left">N&uacute;mero RIF Beneficiario</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Cuenta Bancaria</div></td>
															<td><div align="left">N&uacute;mero de cuenta bancaria</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Banco</div></td>
															<td><div align="left">Relaci&oacute;n con Banco</div></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="right">
													<a href="#_inicio" class="Link">Ir a Inicio</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: C&oacute;digo + Descripci&oacute;n
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>