<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Primas por Planes</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Debe estar creada la siguiente tabla para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Poliza.jsp" class="linktext">P�lizas</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="PlanPoliza.jsp" class="linktext">Planes</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite el registro, actualizaci�n y consulta de las primas a pagar de acuerdo al plan, parentesco, edad y sexo. 
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">P�liza</div></td>
												<td width="70%"><div align="left">Seleccionar una  p�liza</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Plan</div></td>
												<td><div align="left">Seleccionar un  plan</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Parentesco/Titular</div></td>
												<td><div align="left">seleccionar un parentesco o al titular</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Edad m�nima</div></td>
												<td><div align="left">Edad minima para permitir la inclusi�n del beneficiario en este plan</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Edad m�xima</div></td>
												<td><div align="left">Edad m�xima para permitir la inclusi�n del beneficiario en este plan</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Prima para sexo femenino</div></td>
												<td><div align="left">Registrar el monto correspondiente a la prima si el beneficario es femenino</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Prima para sexo masculino</div></td>
												<td><div align="left">Registrar el monto correspondiente a la prima si el beneficario es masculino</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% porcentaje patron</div></td>
												<td><div align="left">Registrar el monto correspondiente a un porcentaje de la prima a  pagar por el patron</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% porcentaje trabajador</div></td>
												<td><div align="left">Registrar el monto correspondiente a un porcentaje de la prima a  pagar por el trabajador</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cobertura</div></td>
												<td><div align="left">Cobertura que contempla el plan que se est� registrando para ese tipo de beneficiario/div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N�mero m�ximo de beneficiarios</div></td>
												<td><div align="left">N�mero m�ximo de personal por ese parentesco que el titular puede incluir como beneficio.</div></td>
											</tr>

<tr>
<tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: C�digo  + Descripci�n.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
