<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Organismos</strong></td>
								</tr>
							</table>
							<!----------->
							
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Debe estar creada la siguiente tabla para la ejecuci&oacute;n de esta opci&oacute;n: </p>
													<table width="30%" align="center">
														<tr>
															<td class="datatableborder"  align="center">
																<a href="../ubicacion/Ciudad.jsp" class="linktext">Ciudad</a>
															</td>
														</tr>
												  </table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de 
													los organismos que utilizar&aacute;n SIGEFIRRHH.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">C�digo</div></td>
												<td width="70%"><div align="left">C�digo del Organismo. C�digos suministrados por MPD </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre</div></td>
												<td><div align="left">Nombre o denominaci�n del organismo. Informaci�n suministrada por MPD</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Siglas</div></td>
												<td><div align="left">Siglas con las cuales se conoce al organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N� RIF</div></td>
												<td><div align="left">N�mero de RIF del organismo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N� NIT</div></td>
												<td><div align="left">N�mero de NIT del organismo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Direcci�n</div></td>
												<td><div align="left">Direcci�n de la sede principal del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Zona Postal</div></td>
												<td><div align="left">Zona Postal del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tel�fono</div></td>
												<td><div align="left">Tel�fono del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre Agente Retenci�n</div></td>
												<td><div align="left">Nombre del agente de retenci�n</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�dula Agente Retenci�n</div></td>
												<td><div align="left">C�dula de identidad del agente de retenci�n</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N� RIF Agente Retenci�n</div></td>
												<td><div align="left">N�mero de RIF del agente de retenci�n</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo Anterior MPD</div></td>
												<td><div align="left">C�digo asignado al organismo por el MPD para ser procesado en los sistemas anteriores</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo SIGECOF</div></td>
												<td><div align="left">C�digo asignado al organismo por ONAPRE para los procesos del sistema SIGECOF</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">UEL</div></td>
												<td><div align="left">C�digo de Unidad Ejecutora Local que representa el organismo en s�</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre Director RRHH</div></td>
												<td><div align="left">Apellidos y nombres del Director de Recursos Humanos</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�dula Director RRHH</div></td>
												<td><div align="left">C�dula de Identidad del Director de Recursos Humanos</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tel�fono RRHH</div></td>
												<td><div align="left">Tel�fono del Director de Recursos Humanos</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Gaceta RRHH</div></td>
												<td><div align="left">Gaceta y fecha de vigencia del nombramiento del Director de RRHH</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre Director Inform�tica</div></td>
												<td><div align="left">Apellidos y nombres del Director de Inform�tica o su equivalente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�dula Director Inform�tica</div></td>
												<td><div align="left">C�dula de Identidad del Director de Inform�tica o su equivalente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tel�fono Inform�tica</div></td>
												<td><div align="left">Tel�fono del Director de Inform�tica o su equivalente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Gaceta Inform�tica</div></td>
												<td><div align="left">Gaceta y fecha de vigencia del nombramiento del Director de Inform�tica o su equivalente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre M�xima Autoridad</div></td>
												<td><div align="left">Apellidos y nombres dela m�xima autoridad del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�dula M�xima Autoridad</div></td>
												<td><div align="left">C�dula de Identidad de la m�xima autoridad del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tel�fono M�xima Autoridad</div></td>
												<td><div align="left">Tel�fono de la m�xima autoridad del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Gaceta M�xima Autoridad</div></td>
												<td><div align="left">Gaceta y fecha de vigencia del nombramiento de la m�xima autoridad del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Es un �rgano rector ?</div></td>
												<td><div align="left">Indicar si se trata de un �rgano rector de los lineamientos a seguir en los procesos de SIGEFIRRHH</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Aprueba MPD ?</div></td>
												<td><div align="left">Indicar si se trata de un organismo cuyos movimientos de personal y/o planes de personal deben ser aprobados por MPD, en caso contrario se considera que solo se llevar� a cabo procedimientos de registro de informaci�n</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Es un organismo adscrito ?</div></td>
												<td><div align="left">Indicar si se trata de un organismo adscrito a otro organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Pa�s, estado y ciudad</div></td>
												<td><div align="left">Indicar el pa�s, estado y ciudad donde se esta ubicada la sede principal del organismo</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													Muestra:  Codigo  + Nombre.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
