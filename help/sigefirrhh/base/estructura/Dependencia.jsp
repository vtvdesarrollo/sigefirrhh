<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Dependencias Administrativas</strong></td>
								</tr>
							</table>
							<!----------->
							
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="UnidadEjecutora.jsp" class="linktext">Unidades Ejecutoras Locales</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="UnidadFuncional.jsp" class="linktext">Unidades Funcionales</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="TipoDependencia.jsp" class="linktext">Tipo de Dependencia</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="GrupoOrganismo.jsp" class="linktext">Grupos de Organismo</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Dependencia.jsp" class="linktext">Dependencia</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n  de las 
													dependencias administrativas del organismo. Mientras se est&eacute; manejando distintos 
													grupos de organismos en uno (fusi&oacute;n de organismos), ha de mantenerse la respectiva 
													estructura para cada grupo hasta que se apruebe la nueva estructura administrativa. 
													Si el organismo se rige bajo LEFP, debe utilizar la codificaci&oacute;n de estructura que 
													le asigna MPD.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">C�digo</div></td>
												<td width="70%"><div align="left">C�digo de dependedncia administrativa. En el caso de los organismos sujetos por LEFP, deben utilizar los c�digos suministrados por MPD. </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre</div></td>
												<td><div align="left">Nombre o denominaci�n de la dependencia. En el caso de los organismos sujetos por LEFP, deben utilizar las descripciones suministradas por MPD.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">U.E.L.</div></td>
												<td><div align="left">Relaci�n con unidad ejecutora local. Informaci�n suministrada por los organismos</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Unidad Funcional</div></td>
												<td><div align="left">Relaci�n con unidad funcional. Informaci�n suministrada por los organismos</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo</div></td>
												<td><div align="left">Relaci�n con tipo de dependencia. Informaci�n suministrada por los organismos</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Localidad</div></td>
												<td><div align="left">Se debe indicar si se trata de una dependencia que est� localizada a nivel central, o a nivel regional</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grupo de Organismo</div></td>
												<td><div align="left">Relaci�n con un grupo de organismo. En caso de organismos que se fusionan para conformar una nueva estructura organizativa, se debe conservar cada estructura por separado, hata que la nueva estructura sea aprobada. Informaci�n suministrada por MPD</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Vigente ? </div></td>
												<td><div align="left">Debe indicarse si es una dependencia administrativa vigente en la estructura actual del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Staff ? </div></td>
												<td><div align="left">Debe indicarse si se trata de una dependencia staff dentro de la estructura. Informaci�n suministrada por los organismos</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Aprobada por MPD ?</div></td>
												<td><div align="left">Debe indicarse si se trata de una dependencia que a�n no ha sido aprobada por MPD. Informaci�n suministrada por los organismos</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sede Diplom�tica ?</div></td>
												<td><div align="left">Debe indicarse si es una dependencia que representa una sede diplom�tica. Informaci�n suministrada por los organismos</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													Muestra: Nombre + Codigo.
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>