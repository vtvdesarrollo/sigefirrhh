<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		<a  name="_inicio">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Sub Areas de Profesi&oacute;n</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<tr>
								  <td align="left">

								  </td>
								</tr>
								<!----------->
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>

											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:
												  <table width="30%" align="center">
													<tr>
														<td width="50%"class="datatableborder"  align="center">
														<a href="AreaProfesion.jsp" class="linktext">Area Profesion </a></td>
													</tr>
												</table>
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="right">
													<a href="#_inicio" class="Link">Ir a Inicio</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%">
											<tr>
												<td align="center">
													<table width="100%" class="datatable">
														<tr>
															<td>
																Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de las 
																diferentes sub-&aacute;reas en que se clasifican las profesiones. Esta 
																informaci&oacute;n es suministrada por la OPSU.
															</td>
														</tr>
														<tr>
															<td>
																<div align="center">
																	<img src="../../../../images/help/sigefirrhh/base/personal/SubAreaProfesion.gif" width="521" height="78">
																</div>
															</td>
														</tr>

													</table>
													
													<table class="bandtable" width="95%" align="center">
														<tr>
															<td width="17%"><div align="left">Campo</div></td>
															<td width="34%"><div align="left">Descripci�n</div></td>
															<td width="10%"><div align="center">Tama&ntilde;o</div></td>
															<td width="13%"><div align="center">Requerido</div></td>
															<td width="14%"><div align="center">Modificable</div></td>
															<td width="12%"><div align="center">Busqueda</div></td>
														</tr>
													</table>
													<table width="95%" class="datatableborder" align="center">
													<tr>
															<td><div align="left">Area</div></td>
															<td><div align="left">Seleccionar el <a href="AreaProfesion.jsp" class="linktext">Area Profesion</a> de acuerdo a la lista.</div></td>
															<td><div align="center"></div></td>
															<td><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">C�digo</div></td>
															<td width="34%"><div align="left">C�digo SubArea de Profesi&oacute;n </div></td>
															<td width="10%"><div align="center">3</div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"></div></td>
															<td width="12%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Descripcion</div></td>
															<td><div align="left">Descripcion</div></td>
															<td><div align="center">100</div></td>
															<td><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>

													</table>
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="right">
													<a href="#_inicio" class="Link">Ir a Inicio</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													Muestra: Descripci�n + C�digo SubArea Profesi&oacute;n + Descripci&oacute;n Area Profesi&oacute;n
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="right">
													<a href="#_inicio" class="Link">Ir a Inicio</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>