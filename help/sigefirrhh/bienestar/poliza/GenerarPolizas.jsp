<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Generar Titulares/Beneficiarios</strong></td>
								</tr>
							</table>
<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que genera a los titulares y beneficiarios del plan de una  p&oacute;liza. Verifica las condiciones establecidas en las primas de acuerdo al sexo, parentesco y edad, y registra a cada titular y/o beneficiario con los montos correspondientes a pagar, tanto el trabajador como el patr&oacute;n.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Seleccionar el tipo de Personal a procesar</div></td>
											</tr>
											
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
                                            <tr>
												<td width="30%"><div align="left">P�liza</div></td>
												<td width="70%"><div align="left">Seleccionar la p�liza que se va a procesar</div></td>
											</tr>
											
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Plan</div></td>
												<td width="70%"><div align="left">Seleccionar el plan, dentro de la p�liza seleccionada, que se va a procesar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha de Inclusi�n</div></td>
												<td width="70%"><div align="left">Se indica la fecha de inclusi�n que se va a registrar para cada beneficiario</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Procesar a:</div></td>
												<td width="70%"><div align="left">Se indica si se va a procesar a los titulares o a los familiares beneficiarios de la p�liza</div></td>
											</tr>
										
										</table>
							<!----------->
							
							<table width="100%" class="toptable">
								
								<!--Procedimiento-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_procedimiento">Procedimiento</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="center">
													<img src="/sigefirrhh/help/images/bienestar/poliza/GenerarPolizas.gif" border="0">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
