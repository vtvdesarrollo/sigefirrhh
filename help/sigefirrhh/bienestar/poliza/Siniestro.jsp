<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Siniestros</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Debe estar creada la siguiente tabla para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/bienestar/Poliza.jsp" class="linktext">P�lizas</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/bienestar/PlanPoliza.jsp" class="linktext">Planes</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../bienestar/poliza/Titular.jsp" class="linktext">Titular</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../bienestar/poliza/Beneficiario.jsp" class="linktext">Beneficiarios</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/bienestar/TipoSiniestro.jsp" class="linktext">Tipo Siniestros</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/bienestar/EstablecimientoSalud.jsp" class="linktext">Establecimientos Salud</a>
															</td>
														</tr>

													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite el registro, actualizaci�n y consulta de los siniestros reportados por titulares y/o beneficiarios de p�lizas de seguros contratadas
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">P�liza</div></td>
												<td width="70%"><div align="left">Seleccionar una  p�liza</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Plan</div></td>
												<td><div align="left">Seleccionar un  plan</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Titular</div></td>
												<td><div align="left">Seleccionar un  titular de esa p�liza</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Beneficiario</div></td>
												<td><div align="left">Seleccionar un  beneficiario del titular de esa p�liza</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Siniestro</div></td>
												<td><div align="left">Seleccionar un  tipo de siniestro</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N�mero de siniestro</div></td>
												<td><div align="left">N�mero que identifica el siniestro. Puede ser suministrado por la compa��a aseguradora</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Siniestro</div></td>
												<td><div align="left">Fecha que ocurri� el siniestro</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Reporte</div></td>
												<td><div align="left">Fecha que se report� el siniestro</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Recepci�n Recaudos</div></td>
												<td><div align="left">Fecha que se recibi� todos los recaudos</div></td>
											</tr>

<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Estatus</div></td>
												<td><div align="left">Estatus del siniestro, a saber:<br>
Pendiente por recaudos<br>
En Proceso<br>
Rechazado<br>
Pagado<br></div></td>
											</tr>


<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Monto Reclamo</div></td>
												<td><div align="left">Monto que reclam� el titular de la p�liza</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Monto Pagado</div></td>
												<td><div align="left">Monto que pag� la compa��a aseguradora</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Cierre</div></td>
												<td><div align="left">Fecha que se cierra el proceso del siniestro</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Diagn�stico</div></td>
												<td><div align="left">Diagn�stico dictado por los especialistas</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Observaciones</div></td>
												<td><div align="left">Observaciones</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Establecimiento Salud</div></td>
												<td><div align="left">Seleccionar un establecimiento de salud</div></td>
											</tr>


<tr>
<tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: C�digo  + Descripci�n.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
