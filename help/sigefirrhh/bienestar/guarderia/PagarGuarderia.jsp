<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Proceso Generar Pagos</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
							<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Proceso que genera el pago a las guarder�as, ya sea correspondiente a la mensualidad o al pago de la inscripci�n. El monto a pagar se registra en los conceptos fijos o variables del trabajador, seg�n se indique y con la frecuencia que est� asociada al concepto por tipo de personal.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Seleccionar el tipo de Personal a procesar</div></td>
											</tr>
											
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
                                             <tr>
												<td width="30%"><div align="left">Grabar en</div></td>
												<td width="70%"><div align="left">Indicar si el pago se va a registrar en conceptos fijos o variables</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
                                             <tr>
												<td width="30%"><div align="left">Fecha de Vigencia</div></td>
												<td width="70%"><div align="left">Indicar la fecha de vigencia del pago</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
                                             <tr>
												<td width="30%"><div align="left">Pago de Inscripci�n</div></td>
												<td width="70%"><div align="left">Indicar si el pago que se va a generar corresponde a una mensualidad o a la inscripci�n</div></td>
											</tr>
										
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								
								<!--Procedimiento-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_procedimiento">Procedimiento: Proceso que genera los pagos correspondientes al reglamento de guarder�as</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="center">
													<img src="/sigefirrhh/help/images/bienestar/guarderia/PagarGuarderia.gif" border="0">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>