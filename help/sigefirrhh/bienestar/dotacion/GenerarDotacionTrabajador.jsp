<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Asignar Dotaciones</strong></td>
								</tr>
							</table>
							<!----------->
							
							<table width="100%" class="toptable">
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Bas�ndose en la asociaci�n de dotaciones a los cargos, este proceso 
													selecciona a los trabajadores del tipo de personal indicado, y de 
													acuerdo al cargo que est�n desempe�ando, les asigna las dotaciones ya 
													registradas para ese cargo.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Seleccionar el tipo de personal a procesar</div></td>
											</tr>
											
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
                                            <tr>
												<td width="30%"><div align="left">Tipo de Dotaci�n</div></td>
												<td width="70%"><div align="left">Seleccionar el tipo de dotaci�n que se va asignar</div></td>
											</tr>
											
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Tipo de Sub-Dotaci�n</div></td>
												<td width="70%"><div align="left">Seleccionar la sub-dotaci�n, dentro del tipo seleccionado, que se va asignar</div></td>
											</tr>
										
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>