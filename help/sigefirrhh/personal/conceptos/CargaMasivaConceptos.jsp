<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Actualizaci�n Masiva de Conceptos</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													</p> Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n: 
													<table width="30%" align="center">
														
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">
																	Tipo de Personal
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/Concepto.jsp" class="linktext">
																	Concepto
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/FrecuenciaPago.jsp" class="linktext">
																	Frecuencia de Pago
																</a>
															</td>
														</tr>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Proceso que permite registrar y/o actualizar un concepto a un grupo de 
													trabajadores en la misma opci�n.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">

											<tr>
												<td><div align="left">Acci�n</div></td>
												<td>
													<div align="left">
														El usuario debe seleccionar el tipo de acci�n que desea ejecutar, a saber:<br>
														<br>
														<b>Ingresar conceptos:</b><br>
														El sistema registrar� a los trabajadores el concepto procesado.<br>
														<br>
														<b>Sustituir conceptos:</b><br>
														El sistema reemplazar� el concepto indicado a los trabajadores. En caso que el 
														trabajador no posea el concepto, el sistema lo ingresar�. Esta acci�n solo es 
														permitida para conceptos fijos.<br>
														<br>
														<b>Sumar Conceptos:</b><br>
														El sistema sumar� el monto indicado o calculado, al concepto correspondiente del 
														trabajador. En caso que el trabajador no posea el concepto indicado, el 
														sistema lo registrar� con los valores indicados. Esta acci�n solo es permitida 
														para conceptos fijos.<br>
														<br>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Personal</div></td>
												<td><div align="left">El usuario debe seleccionar el tipo de personal que se va a procesar.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Concepto</div></td>
												<td><div align="left">Se selecciona el concepto a procesar.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Frecuencia de Pago</div></td>
												<td><div align="left">Se selecciona la frecuencia de pago a asociar al concepto.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grabar en Conceptos</div></td>
												<td>
													<div align="left">
														El usuario debe seleccionar si el registro y/o actualizaci�n ser� sobre los conceptos 
														fijos o variables del trabajador. En caso de seleccionar conceptos variables, el 
														sistema solo permitir� ingresar.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�dula</div></td>
												<td><div align="left">N�mero de c�dula del trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Unidades</div></td>
												<td>
													<div align="left">
														Si se indica un valor en este campo, y el concepto est� formulado, multiplicar� el 
														resultado por el valor indicado en unidades.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Monto</div></td>
												<td>
													<div align="left">
														El primer registro tiene el monto en cero, si se trata de un concepto formulado, al 
														pulsar "AGREGAR" , el sistema calcular� el monto correspondiente. Si no se trata de un 
														concepto formulado, el usuario debe introducir el monto a registrar.<br>
														En los registros posteriores, se mantendr� el valor del monto anterior, en caso que el 
														usuario desee mantener el mismo valor para todos los trabajadores que va a procesar.
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>