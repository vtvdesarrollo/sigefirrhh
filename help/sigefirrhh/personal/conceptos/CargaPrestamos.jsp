<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Carga Externa de Prestamos(Archivo)</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													</p> Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n: 
													<table width="49%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">
																	Tipo de Personal
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/FrecuenciaTipoPersonal.jsp" class="linktext">
																	Frecuencia de Pago por Tipo de Personal
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Proceso que permite registrar, eliminar y/o actualizar los pr�stamos 
													vigentes de los trabajadores, con informaci�n proveniente de un archivo externo.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">El usuario debe seleccionar el tipo de personal que se va a procesar.</div></td>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Frecuencia de Pago</div></td>
												<td><div align="left">El usuario debe seleccionar la frecuencia de pago que desea asociar a los pr�stamos.</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Archivo</div></td>
												<td><div align="left">
													Se indica el nombre del archivo con los conceptos a procesar, y se busca 
													en el computador al pulsar el bot�n "EXAMINAR".<br>
													El archivo para importar, debe ser un archivo de tipo texto con la extensi�n 
													".TXT". Los campos deben estar distribuidos en columnas y separados por un 
													tabulador (tecla TAB) en el siguiente orden:<br>
													<br>
													Para suspender o eliminar un pr�stamo, el registro debe ser:<br>
													<table width="100%" class="datatable">
														<tr>
															<td width="25%">Concepto(*)</td>
															<td width="20%">C�dula</td>
															<td width="15%">Monto</td>
															<td width="40%">(*) Concepto tipo pr�stamo</td>
														</tr>
														<tr>
															<td colspan="4">Por ejemplo:</td>
														<tr>
														<tr>
															<td>XXXX</td>
															<td>3878909</td>
															<td>0</td>
															<td></td>
														</tr>
													</table>
													
													Para registrar o actualizar un pr�stamo, el registro debe ser:<br>
													<table width="100%" class="datatable">
														<tr>
															<td width="17%">Concepto</td>
															<td width="19%">C�dula</td>
															<td width="15%">Monto Cuota</td>
															<td width="15%">N�mero Cuotas</td>
															<td width="17%">Monto del prestamo</td>
															<td width="17%">Fecha Inicio Descuento</td>
														</tr>
														<tr>
															<td colspan="6">Por ejemplo:</td>
														</tr>
														<tr>
															<td>XXXX</td>
															<td>3878909</td>
															<td>1000.00</td>
															<td>10</td>
															<td>1000000.00</td>
															<td>01/03/2006</td>
														</tr>
													</table>
													
													Una vez seleccionado el archivo, se pulsa el bot�n "CARGAR ARCHIVO" y el 
													sistema recorrer� los registros del archivo, realizando las siguientes 
													validaciones:<br>
													<ul>
														<li>El concepto (tipo pr�stamo) indicado debe estar asociado al tipo 
														de personal seleccionado.</li>
														<li>La c�dula indicada debe pertenecer a un trabajador del tipo de 
														personal seleccionado</li>
													</ul>
													En caso de error se muestra el siguiente mensaje:<br>
													<br>													
													Se carg� con �xito. Pero hay registros con datos incoherentes. Este archivo 
													podr� ser procesado, sin embargo, las incoherencias no ser�n procesadas. 
													A continuaci�n, baje el archivo para obtener mas detalle de los errores 
													(pulse el bot�n derecho del mouse).<br>
													<br>
													El usuario podr� revisar el archivo indicado y guardarlo para un an�lisis 
													posterior. Para procesar los registros validados, se pulsa el bot�n "EJECUTAR" 
													y se realizar�n las actualizaciones indicadas.
												</div></td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>