<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Carga Externa de Conceptos(Archivo)</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													</p> Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n: 
													<table width="49%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">
																	Tipo de Personal
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/ConceptoTipoPersonal.jsp" class="linktext">
																	Conceptos por Tipo de Personal
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/FrecuenciaTipoPersonal.jsp" class="linktext">
																	Frecuencia de Pago por Tipo de Personal
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Proceso que permite registrar y/o actualizar los conceptos de los trabajadores, 
													con informaci�n proveniente de un archivo externo.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Se selecciona el tipo de personal a procesar</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Acci�n</div></td>
												<td>
													<div align="left">El usuario debe seleccionar el tipo de acci�n que desea ejecutar, a saber:<br>													
													  <br>
													Ingresar conceptos:<br>
													El sistema registrar� a los trabajadores los conceptos indicados en el archivo.<br>													
													<br>
													Sustituir conceptos:<br>
													El sistema reemplazar� los conceptos indicados a los trabajadores. En caso que el 
														trabajador no posea el concepto indicado, el sistema lo registrar� con los valores 
														indicados.<br>													
														<br>
													Sumar Conceptos:<br>
													El sistema sumar� el monto indicado en el archivo, al concepto correspondiente del 
														trabajador. En caso que el trabajador no posea el concepto indicado, el sistema lo 
														registrar� con los valores indicados.<br>													
														<br>											  
												  </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Personal</div></td>
												<td><div align="left">El usuario debe seleccionar el tipo de personal que se va a procesar.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Frecuencia de Pago</div></td>
												<td>
													<div align="left">
														De acuerdo a la acci�n seleccionada significar� lo siguiente:<br>
														<br>
														Ingresar conceptos:<br>
														El concepto se registra con la frecuencia seleccionada.<br>
														<br>
														Sustituir conceptos:<br>
														Se sustituir� el concepto que tenga la frecuencia seleccionada.<br>
														<br>
														Sumar Conceptos:<br>
														Se sumar� al concepto que tenga la frecuencia seleccionada.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grabar en Conceptos</div></td>
												<td>
													<div align="left">
														El usuario debe seleccionar si el registro y/o actualizaci�n ser� sobre los conceptos 
														fijos o variables del trabajador. 
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Archivo</div></td>
												<td><div align="left">
														Se indica el nombre del archivo con los conceptos a procesar, o se busca en el 
														computador al pulsar el bot�n "EXAMINAR".<br>
														El archivo para importar, debe ser un archivo de tipo texto con la extensi�n 
														".TXT". Los campos debe estar distribuidos en columnas y separados por un tabulador 
														(tecla TAB) en el siguiente orden: <br>
														<table width="100%" class="dataTable">
															<tr>
																<td width="20%" align="left">Concepto</td>
																<td width="20%" align="left">C�dula</td>
																<td width="20%" align="left">Monto</td>
																<td width="40%" align="left"></td>
															</tr>
															<tr>
																<td colsapan="4" align="left">Por ejemplo:</td>
															</tr>
															<tr>
																<td align="left">0001</td>
																<td align="left">13878909</td>
																<td align="left">250000.24</td>
																<td align="left"></td>
														</table>
														<div align="justify"><br>
														  Una vez seleccionado el archivo, se pulsa el bot�n "CARGAR ARCHIVO" y el  sistema 
														recorrer� los registros del archivo, realizando las siguientes validaciones:<br>
														1.El concepto indicado debe estar asociado al tipo de  personal seleccionado.<br>
														2.La c�dula indicada debe pertenecer a un trabajador del tipo de personal seleccionado<br>
														<br>
														En caso de error se muestra el siguiente mensaje:<br>
														Se carg� con �xito. Pero hay registros con datos incoherentes. Este archivo podr� ser 
														procesado, sin embargo, las incoherencias no ser�n procesadas. A continuaci�n, baje el 
														archivo para obtener mas detalle de los errores (pulse el bot�n derecho del mouse).<br>
														<br>
														El usuario podr� revisar el archivo indicado y guardarlo para un an�lisis posterior.<br>
														Para procesar los registros validados, se pulsa el bot�n "EJECUTAR" y se realizar�n las 
														actualizaciones indicadas. </div>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>