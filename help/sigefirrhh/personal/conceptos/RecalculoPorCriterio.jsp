<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Carga por Criterios</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													</p> Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n: 
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">
																	Tipo de Personal
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/Concepto.jsp" class="linktext">
																	Concepto
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/FrecuenciaPago.jsp" class="linktext">
																	Frecuencia de Pago
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Proceso que permite registrar y/o actualizar un  concepto a un grupo de trabajadores 
													en la misma opci�n, siempre que cumplan con el criterio fijado.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">El usuario debe seleccionar el tipo de personal que se va a procesar.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Concepto</div></td>
												<td><div align="left">Se selecciona el concepto a procesar.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Frecuencia de Pago</div></td>
												<td>
													<div align="left">Se selecciona la frecuencia de pago a asociar al concepto.</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Unidades</div></td>
												<td>
													<div align="left">Se indica el tipo de unidad (horas/d�as/cantidad) a aplicar al concepto.</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grabar en Conceptos</div></td>
												<td>
													<div align="left">
														El usuario debe seleccionar si el registro y/o actualizaci�n ser� sobre los 
														conceptos fijos o variables del trabajador. En caso de seleccionar conceptos 
														variables, el sistema solo permitir� ingresar.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Criterios</div></td>
												<td>
													<div align="left">
														Condiciones que se establecen para seleccionar al grupo de trabajadores a los cuales 
														se les procesar� el concepto indicado, a saber:<br>
														<table width="100%" class="dataTable">
															<tr>
																<td width="50%" align="left">Regi�n:</td>
																<td width="50%" align="left">Todas o una</td>
															</tr>
															<tr>
																<td align="left">Manual de Cargos:</td>
																<td align="left">Seleccionar el respectivo manual</td>
															</tr>
															<tr>
																<td align="left">Cargo:</td>
																<td align="left">Todos o uno</td>
															</tr>
															<tr>
																<td align="left">Grado del cargo:</td>
																<td align="left">Desde - Hasta</td>
															</tr>
															<tr>
																<td align="left">Fecha de Ingreso:</td>
																<td align="left">Desde - Hasta</td>
															</tr>
															<tr>
																<td align="left">Sueldo B�sico:</td>
																<td align="left">Desde - Hasta</td>
															</tr>
															<tr>
																<td align="left">Sueldo Integral:</td>
																<td align="left">Desde - Hasta</td>
															</tr>
														</table>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>