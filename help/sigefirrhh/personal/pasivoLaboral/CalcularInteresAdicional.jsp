<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Calcular Intereses Adicionales</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/trabajador/Trabajador.jsp" class="linktext">Datos del Trabajador</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/trabajador/Anticipo.jsp" class="linktext">Anticipos</a>
															</td>
														</tr>	
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/pasivoLaboral/HistoricoDevengadoIntegral.jsp" class="linktext">Historico Devengados Integral</a>
															</td>
														</tr>	
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/pasivoLaboral/CalcularInteresViejoRegimen.jsp" class="linktext">Deuda Viejo Regimen</a>
															</td>
														</tr>	
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/pasivoLaboral/TasaBcv.jsp" class="linktext">Tasas BCV</a>
															</td>
														</tr>														
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite el c�lculo de la deuda por intereses adicionales generados por el saldo de la deuda del regimen derogado e intereses hasta el 18/06/1997. El sistema aplica la tasa de interes promedio hasta Julio 2002 que comienza a aplicar la tasa activa. Este proceso se ejecuta hasta el a�o y mes que indique el usuario.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Seleccionar el tipo de personal</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">A�o </div></td>
												<td width="70%"><div align="left">Indicar hasta que a�o debe calcularse</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Mes </div></td>
												<td width="70%"><div align="left">Indicar hasta que mes debe calcularse</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Proceso </div></td>
												<td width="70%"><div align="left">Indicar si se corre modo prueba o modo definitivo</div></td>
											</tr>

											
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: a�o, mes y monto.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
