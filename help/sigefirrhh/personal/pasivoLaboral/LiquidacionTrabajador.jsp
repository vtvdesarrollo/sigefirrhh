<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Liquidaci�n de Prestaciones Sociales</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/trabajador/Trabajador.jsp" class="linktext">Datos del Trabajador</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/trabajador/Anticipo.jsp" class="linktext">Anticipos</a>
															</td>
														</tr>	
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/prestacionesMensuales/Fideicomiso.jsp" class="linktext">Fideicomiso</a>
															</td>
														</tr>	
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/pasivoLaboral/HistoricoDevengadoIntegral.jsp" class="linktext">Historico Devengados Integral</a>
															</td>
														</tr>	
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/pasivoLaboral/CalcularInteresViejoRegimen.jsp" class="linktext">Deuda Regimen Derogado</a>
															</td>
														</tr>	
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/pasivoLaboral/CalcularInteresAdicional.jsp" class="linktext">Intereses Adicionales</a>
															</td>
														</tr>	
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/prestacionesMensuales/PrestacionesMensuales.jsp" class="linktext">Prestaciones Mensuales</a>
															</td>
														</tr>	
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/pasivoLaboral/TasaBcv.jsp" class="linktext">Tasas BCV</a>
															</td>
														</tr>														
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite el c�lculo de la liquidaci�n de prestaciones sociales al momento de egresar a un trabajador. El sistema calcula la deuda del regimen derogado, los intereses adicionales, y las prestaciones mensuales del nuevo regimen. El trabajador a liquidar debe tener estatus de 'EGRESADO' y tener la fecha de egreso registrada.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Trabajador</div></td>
												<td width="70%"><div align="left">Seleccionar el trabajador a liquidar</div></td>
											</tr>


											
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: a�o, mes y monto.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
