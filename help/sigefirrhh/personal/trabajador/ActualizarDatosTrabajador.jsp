<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Actualizar Datos del Trabajador</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/cargo/Cargo.jsp" class="linktext">Cargo</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/estructura/Dependencia.jsp" class="linktext">Dependencia</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opcion que permite al usuario actualizar datos relacionados la nomina del 
													trabajador, que no sean consecuencia del proceso de un movimiento de personal.<br>
													<p></p>
													Lugar de Pago<br>
													<ul>
														Turno<br>
														Nivel de riesgo del SSO<br>
														Regimen de cotizaci�n<br>
														Forma de pago <br>
														Banco para dep�sitos de n�mina<br>
														Tipo de cuenta<br>
														Nro. Cuenta Bancaria de n�mina<br>
														Banco para aportes de LPH<br>
														Banco para dep�sito del fideicomiso<br>
														Porcentaje (%) ISLR<br>
														Cotiza SSO (S/N)<br>
														Cotiza SPF (S/N)<br>
														Cotiza LPH (S/N)<br>
														Cotiza FJU (S/N)<br>
														C�digo n�mina real<br>
														Dependencia real<br>
														Clasificador y cargo real<br>
														Trajo fe de vida (S/N)<br>
														Fecha que trajo Fe de Vida
													</ul>
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Trabajador</div></td>
												<td width="70%"><div align="left">Nombre del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Lugar de Pago</div></td>
												<td><div align="left">Lugar donde se realiza el pago al trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Turno</div></td>
												<td><div align="left">Descripci�n del turno</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Riesgo SSO</div></td>
												<td><div align="left">Nivel de riesgo, para el c�lculo el aporte patronal del SSO</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Regimen</div></td>
												<td><div align="left">R�gimen que se aplica al trabajador en el c�lculo de la retenci�n del SSO</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Forma de Pago</div></td>
												<td><div align="left">Forma en que se realiza el pago de n�mina al trabajador, a saber: Dep�sito Bancario, Cheque, Transferencia.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Banco N�mina</div></td>
												<td><div align="left">Entidad bancaria donde se deposita el pago correspondiente a la n�mina.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Cuenta</div></td>
												<td><div align="left">Tipo de cuenta del trabajador, a saber: ahorro o corriente.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N� Cuenta</div></td>
												<td><div align="left">Cuenta bancaria asignada al trabajador para el pago de la n�mina.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Banco LPH</div></td>
												<td><div align="left">Entidad bancaria donde se deposita los aportes y retenciones correspondientes a LPH</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Banco Fideicomiso</div></td>
												<td><div align="left">Entidad bancaria donde se deposita el fideicomiso correspondiente al abono mensual de las prestaciones.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">% I.S.L.R.</div></td>
												<td><div align="left">Porcentaje a retener por concepto del ISLR.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cotiza SSO</div></td>
												<td><div align="left">Indicar (S/N) si el trabajador cotiza al Seguro Social Obligatorio.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cotiza SPF</div></td>
												<td><div align="left">Indicar (S/N) si el trabajador cotiza al Seguro Paro Forzoso.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cotiza LPH</div></td>
												<td><div align="left">Indicar (S/N) si el trabajador cotiza a la Ley Pol�tica Habitacional.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cotiza FJU</div></td>
												<td><div align="left">Indicar (S/N) si el trabajador cotiza al Fondo de Jubilaciones.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo N�mina Real</div></td>
												<td><div align="left">C�digo o posici�n del cargo dentro de la plantilla de cargos o puestos de trabajo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Dependencia Real</div></td>
												<td><div align="left">C�digo y descripci�n de la dependencia administrativa adonde pertenece el trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Manual Cargo</div></td>
												<td><div align="left"></div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cargo Real</div></td>
												<td><div align="left">C�digo de clase y descripci�n del cargo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">F� de Vida</div></td>
												<td><div align="left">Indicar (S/N) si el trabajador present� el documento de F� de Vida.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha F� de Vida</div></td>
												<td><div align="left"></div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: C�dula + Nombre y Apellido + Tipo de Personal + Estatus.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>