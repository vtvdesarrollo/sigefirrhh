<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Sueldos Promedios</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Debe estar creada la siguiente tabla para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/ConceptoTipoPersonal.jsp" class="linktext">Conceptos Tipo Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/ParametroGobierno.jsp" class="linktext">Par�metros Gobierno</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Muestra los promedios y bases de c�lculo que utiliza el sistema en varios procesos. 
													Son actualizados por  el sistema previo a la ejecuci�n de la pren�mina y/o n�mina, 
													o el usuario los puede actualizar a traves de la opci�n "actualizar sueldo promedios" 
													en el menu de Procesos de N�mina.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Sueldo Integral</div></td>
												<td width="70%"><div align="left">Monto calculado al sumar los conceptos fijos del trabajador que tengan ese indicador en la definici�n del concepto en la tabla de conceptos.</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sueldo B�sico</div></td>
												<td><div align="left">Monto calculado al sumar los conceptos fijos del trabajador que tengan ese indicador en la definici�n del concepto en la tabla de conceptos.</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Ajustes de Sueldo</div></td>
												<td><div align="left">Monto calculado al sumar los conceptos fijos del trabajador que tengan ese indicador en la definici�n del concepto en la tabla de conceptos.</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Compensaciones</div></td>
												<td><div align="left">Monto calculado al sumar los conceptos fijos del trabajador que tengan ese indicador en la definici�n del concepto en la tabla de conceptos.</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primas por Cargo</div></td>
												<td><div align="left">Monto calculado al sumar los conceptos fijos del trabajador que tengan ese indicador en la definici�n del concepto en la tabla de conceptos.</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primas por Trabajador</div></td>
												<td><div align="left">Monto calculado al sumar los conceptos fijos del trabajador que tengan ese indicador en la definici�n del concepto en la tabla de conceptos.</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Base Semanal para la Retenci�n SSO</div></td>
												<td>
													<div align="left">
														Sueldo/salario semanal del trabajador, que se utiliza para el c�lculo de la 
														retenci�n del SSO. Este monto es calculado tomando en cuenta los conceptos asociados 
														al concepto "5001" para el tipo de personal al cual pertenece el trabajador. En el 
														caso de trabajadores con pago quincenal y/o mensual, los pagos mensuales se multiplican 
														por 12 y se dividen entre 52. En el caso de trabajadores con pago semanal, se aplica la 
														formula semanal que se haya indicado cuando se defini� el tipo de personal.
														El monto calculado, se compara con los l�mites indicados en la opci�n de "Parametros de Gobierno"
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Retenci�n Semanal SSO</div></td>
												<td><div align="left">Indicar Retenci�n Semanal SSO</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Aporte Patronal Semanal SSO</div></td>
												<td><div align="left">Indicar Aporte Patronal Semanal SSO</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Retenci�n Semanal SPF</div></td>
												<td><div align="left">Indicar Retenci�n Semanal SPF</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Aporte Patronal Semanal SPF</div></td>
												<td><div align="left">Indicar Aporte Patronal Semanal SPF</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Base Mensual de Retenci�n LPH</div></td>
												<td><div align="left">Indicar Base Mensual de Retenci�n LPH</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Retenci�n Mensual LPH</div></td>
												<td><div align="left">Indicar Retenci�n Mensual LPH</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Aporte Patronal Mensual LPH</div></td>
												<td><div align="left">Indicar Aporte Patronal Mensual LPH</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Base Mensual de Retenci�n FJU</div></td>
												<td><div align="left">Indicar Base Mensual de Retenci�n FJU</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Retenci�n Mensual FJU</div></td>
												<td><div align="left">Indicar Retenci�n Mensual FJU</div></td>
											</tr>
											<tr>
												<td colspan="2"  class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Aporte Patronal Mensual FJU</div></td>
												<td><div align="left">Indicar Aporte Patronal Mensual FJU</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: .</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>