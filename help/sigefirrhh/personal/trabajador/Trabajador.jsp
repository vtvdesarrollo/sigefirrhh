<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Consultar Datos N�mina</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/cargo/Cargo.jsp" class="linktext">Cargo</a>
															</td>
														</tr>
														
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/estructura/Dependencia.jsp" class="linktext">Dependencia</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite la consulta de los datos de n�mina asociados a un trabajador.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Tipo de personal que pertenece el trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Estatus</div></td>
												<td width="70%"><div align="left">Estatus actual del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Registro Cargos/RPT</div></td>
												<td width="70%"><div align="left">Plantilla de cargos o puestos de trabajo a la cual pertenece el trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Cargo</div></td>
												<td width="70%"><div align="left">C�digo de clase y descripci�n del cargo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Grado</div></td>
												<td width="70%"><div align="left">Grado del cargo en la escala o tabulador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Dependencia</div></td>
												<td width="70%"><div align="left">C�digo y descripci�n de la dependencia administrativa adonde pertenece el trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Lugar de Pago</div></td>
												<td width="70%"><div align="left">Lugar donde se realiza el pago al trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr><tr>
												<td width="30%"><div align="left">C�digo N�mina</div></td>
												<td width="70%"><div align="left">C�digo o posici�n del cargo dentro de la plantilla de cargos o puestos de trabajo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Sueldo B�sico</div></td>
												<td width="70%"><div align="left">Sueldo b�sico del trabajador, ya sea de acuerdo a una escala o tabulador,o fijado directamente.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Paso</div></td>
												<td width="70%"><div align="left">Paso en la escala o tabulador.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Turno</div></td>
												<td width="70%"><div align="left">Descripci�n del turno</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Riesgo SSO</div></td>
												<td width="70%"><div align="left">Nivel de riesgo, para el c�lculo el aporte patronal del SSO</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr><tr>
												<td width="30%"><div align="left">Regimen</div></td>
												<td width="70%"><div align="left">R�gimen que se aplica al trabajador en el c�lculo de la retenci�n del SSO</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha Ingreso Organismo</div></td>
												<td width="70%"><div align="left">Fecha en que el trabajador ingres� al organismo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha Ingreso APN</div></td>
												<td width="70%"><div align="left">Fecha en que el trabajador ingres� a la Administraci�n P�blica Nacional.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha C�lculo Vacaciones</div></td>
												<td width="70%"><div align="left">Fecha en que nace el derecho a vacaciones de un trabajador. Se utiliza para el c�lculo del bono vacacional.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha C�culo Prima Antiguedad</div></td>
												<td width="70%"><div align="left">Fecha que se utiliza para el c�lculo de la prima por antiguedad, en aquellos organismos donde se realiza este pago.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr><tr>
												<td width="30%"><div align="left">Fecha C�lculo Prestaciones</div></td>
												<td width="70%"><div align="left">Fecha que se utiliza para el determinar el tiempo de servicio en el proceso de c�lculo de prestaciones.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Forma de Pago</div></td>
												<td width="70%"><div align="left">Forma en que se realiza el pago de n�mina al trabajador, a saber: Dep�sito Bancario, Cheque, Transferencia.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Banco N�mina</div></td>
												<td width="70%"><div align="left">Entidad bancaria donde se deposita el pago correspondiente a la n�mina.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Tipo de Cuenta</div></td>
												<td width="70%"><div align="left">Tipo de cuenta del trabajador, a saber: ahorro o corriente.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">N� Cuenta</div></td>
												<td width="70%"><div align="left">Cuenta bancaria asignada al trabajador para el pago de la n�mina.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr><tr>
												<td width="30%"><div align="left">Banco LPH</div></td>
												<td width="70%"><div align="left">Entidad bancaria donde se deposita los aportes y retenciones correspondientes a LPH</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Banco Fideicomiso</div></td>
												<td width="70%"><div align="left">Entidad bancaria donde se deposita el fideicomiso correspondiente al abono mensual de las prestaciones.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">% I.S.L.R.</div></td>
												<td width="70%"><div align="left">Porcentaje a retener por concepto del ISLR.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Cotiza SSO</div></td>
												<td width="70%"><div align="left">Indicar (S/N) si el trabajador cotiza al Seguro Social Obligatorio.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Cotiza SPF</div></td>
												<td width="70%"><div align="left">Indicar (S/N) si el trabajador cotiza al Seguro Paro Forzoso.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Cotiza LPH</div></td>
												<td width="70%"><div align="left">Indicar (S/N) si el trabajador cotiza a la Ley Pol�tica Habitacional.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Cotiza FJU</div></td>
												<td width="70%"><div align="left">Indicar (S/N) si el trabajador cotiza al Fondo de Jubilaciones.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha Egreso</div></td>
												<td width="70%"><div align="left">Fecha de egreso del organismo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">F� de Vida</div></td>
												<td width="70%"><div align="left">Indicar (S/N) si el trabajador present� el documento de F� de Vida.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Ultimo Movimiento</div></td>
												<td width="70%"><div align="left">Ultimo movimiento de personal que afect� la trayectoria del trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha de Ultimo Movimiento</div></td>
												<td width="70%"><div align="left">Fecha en que se realiz� el �ltimo movimiento de personal.</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: .</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
