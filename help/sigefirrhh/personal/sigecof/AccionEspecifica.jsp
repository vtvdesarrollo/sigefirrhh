<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Acciones Espec�ficas</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Proyecto.jsp" class="linktext">
																	Proyectos
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="AccionCentralizada.jsp" class="linktext">
																	Acciones Centralizadas
																</a>
															</td>
														</tr>
														
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Son aquellas operaciones concretas, suficientes, medibles y cuantificables anualmente, necesarias para lograr los resultados de los proyectos y/o acciones centralizadas. El registro de las acciones espec�ficas define las categor�as presupuestarias y representan la ejecuci�n de cr�ditos presupuestarios con un fin. Las acciones espec�ficas de las acciones centralizadas est�n predeterminadas, a saber:<br>
													
													<br>
													0001 - Direcci�n y Coordinaci�n de los Gastos de los Trabajadores<br>
													<ul>
														<u>Espec�ficas</u>><br>
														001 - Asignaci�n y control de los recursos para los gastos de los trabajadores.<br>
													</ul>
													0002 - Gesti�n Administrativa.<br>
													<ul>
														<u>Espec�ficas</u>:<br>
														001 - Apoyo institucional de las acciones espec�ficas de los    proyectos de los organismos.<br>
														002 - Apoyo institucional al sector privado y externo.<br>
														003 - Apoyo institucional al sector p�blico.<br>
													</ul>
													0003 - Previsi�n y Protecci�n Social<br>
													<ul>
														<u>Espec�ficas</u>:<br>
														001 - Asignaci�n y control de los recursos para gastos de los pensionados y jubilados.<br>
													</ul>
													0004 - Asignaciones Predeterminadas<br>
													<ul>
														<u>Espec�ficas</u>:<br>
														001 - Asignaci�n y control de los aportes constitucionales y legales.<br>
													</ul>
													0005 - Direcci�n y coordinaci�n del servicio de la deuda p�blica nacional<br>
													<ul>
														<u>Espec�ficas</u>:<br>
														001 - Asignaci�n y control de los recursos para el servicio de la deuda p�blica nacional.<br>
													</ul>
													0006 - Otras
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">A�o</div></td>
												<td width="70%"><div align="left">A�o presupuestario</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">C�digo</div></td>
												<td width="70%"><div align="left">C�digo de la acci�n cehtralizada. Inicia en '0001' y termina con '0006'.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Descripci�n</div></td>
												<td width="70%"><div align="left">Descripci�n de la acci�n</div></td>
											</tr>
										</table>
										-->
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: C�digo + Nombre</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
