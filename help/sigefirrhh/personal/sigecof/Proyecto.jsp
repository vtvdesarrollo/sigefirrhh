<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Proyectos</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/ubicacion/Ciudad.jsp" class="linktext">
																	Ciudad
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/ubicacion/Estado.jsp" class="linktext">
																	Estado
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/ubicacion/Pais.jsp" class="linktext">
																	Pa�s
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Consiste en un conjunto delimitado de acciones y recursos que permiten, 
													en un tiempo determinado, el logro de un resultado especifico para el 
													cual fue concebido. 
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">A�o</div></td>
												<td width="70%"><div align="left">A�o de ejecuci�n del proyecto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo</div></td>
												<td><div align="left">Debe ser un c�digo num�rico que comience a partir de '0007'.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Enunciado</div></td>
												<td><div align="left">Denominaci�n del proyecto.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Descripci�n</div></td>
												<td><div align="left">Descripci�n detallada del proyecto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Objetivo Estrat�gico</div></td>
												<td><div align="left">
														Logros que el ejecutivo se propone alcanzar en relaci�n con el
														desarrollo del pais y atender los problemas o demandas de la sociedad,
														en los cuales se enmarca el proyecto que se est� registrando.
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Objetivos Espec�ficos</div></td>
												<td><div align="left">
														Es la situaci�n objetivo que se desea alcanzar como consecuencia de la 
														ejecuci�n del proyecto. Creaci�n, transformaci�n o mejora de la
														siuaci�n actual.
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Resultado</div></td>
												<td><div align="left">
														Es el producto, bien o servicio que se va a 
														materializar con la ejecuci�n
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Meta</div></td>
												<td><div align="left">
														Es la determinaci�n cualitativa y cuantitativa del bien, servicio o
														indicador que se espera obtener
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Estatus</div></td>
												<td>
													<div align="left">
														<li>Por iniciar: Proyecto nuevo factible al cual se solicita que aprueben y asignen 
														los recursos para que se inicien las acciones espec�ficas del mismo.
														<br>
														<li>Por reactivar: Proyecto factible cuya ejecuci�n fue paralizada durante el ejercicio 
														vigente o anteriores, al cual se solicita aprueben y asignen recursos para la 
														consecuci�n de las acciones espec�ficas suspendidas.
														<br>
														<li>En ejecuci�n: Proyecto que se encuentra en ejecuci�n activa de al menos una de sus 
														acciones espec�ficas, al cual solicita le aprueben y asignen recursos para su continuidad, 
														seg�n el cronograma de actividades.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Inicio</div></td>
												<td><div align="left">Fecha estimada de inicio del proyecto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Fin</div></td>
												<td><div align="left">Fecha estimada de finalizaci�n del proyecto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Suspension</div></td>
												<td><div align="left">Si el proyecto hubiera que ser suspendido por alguna causa de fuerza mayor, se ha de indicar la fecha de suspensi�n.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Pais</div></td>
												<td><div align="left">Pais donde se llevar� a cabo el proyecto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Estado</div></td>
												<td><div align="left">Estado donde se llevar� a cabo el proyecto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Ciudad</div></td>
												<td><div align="left">Ciudad donde se llevar� a cabo el proyecto</div></td>
											</tr>
										</table>
										-->
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: Enunciado + A�o.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>