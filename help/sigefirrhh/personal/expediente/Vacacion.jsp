<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Vacaciones Pendientes</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Personal.jsp" class="linktext">Datos Personales</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipos de Personal</a>
															</td>														
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%">
											<tr>
												<td align="center">
													<table width="100%" class="datatable">
														<tr>
															<td>
																Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de las 
																vacaciones pendientes de un trabajador. Una vez realizada 
																la carga inicial de este m&oacute;dulo, el sistema actualizar&aacute; esta 
																informaci&oacute;n de forma autom&aacute;tica a trav&eacute;s de diferentes procesos.
															</td>
														</tr>
													</table>
													
													<table class="bandtable" width="95%" align="center">
														<tr>
															<td width="30%"><div align="left">Campo</div></td>
															<td width="70%"><div align="left">Descripci�n</div></td>
														</tr>
													</table>
													<table width="95%" class="datatableborder" align="center">
														<tr>
															<td width="30%"><div align="left">Trabajador</div></td>
															<td width="70%"><div align="left">Apellidos, Nombres  y C&eacute;dula del Trabajador Seleccionado</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Tipo Personal</div></td>
															<td><div align="left">Seleccionar Tipo de Personal de acuerdo a la lista.</div></td>
														</tr>
														
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">A�o</div></td>
															<td><div align="left">A�o</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Fecha Derecho Vacaciones</div></td>
															<td><div align="left">Fecha que el trabajador obtiene el derecho a vacaciones</div></td>
														</tr>
<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">D�as Derecho</div></td>
															<td><div align="left">D�as que corresponden al trabajador por derecho en ese per�odo</div></td>
														</tr>														
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">D�as Disfrute</div></td>
															<td><div align="left">D�as Disfrutados por el trabajador</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">D�as Pendientes</div></td>
															<td><div align="left">D�as Pendientes que a�n le quedan por disfrutar correspondientes a ese a�o</div></td>
														</tr>
														
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Observaci�n</div></td>
															<td><div align="left">Observaci�n</div></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: A�o Inicio + A�o Fin + Tip de Vacaci�n + D�as Habiles
											  </td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
