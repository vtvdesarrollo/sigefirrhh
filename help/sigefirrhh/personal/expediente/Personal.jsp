<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Datos Personales</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/ubicacion/Ciudad.jsp" class="linktext">
																	Ciudad
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/ubicacion/Parroquia.jsp" class="linktext">
																	Parroquia
																</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/bienestar/EstablecimientoSalud.jsp" class="linktext">
																	Establecimiento Salud
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												
                      <td> Opci�n que permite registrar los datos personales del 
                        trabajador . 
                        <!--
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de 
													los datos personales de los trabajadores.
													-->
                      </td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										
                  <table width="96%" class="datatableborder" align="center">
                    <tr> 
                      <td width="30%"><div align="left">Cedula</div></td>
                      <td width="70%"><div align="left">Numero de Cedula del trabajador.</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Primer Apellido</div></td>
                      <td width="70%"><div align="left">Primer Apellido del trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Segundo Apellido </div></td>
                      <td width="70%"><div align="left">Segundo Apellido del trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Primer Nombre </div></td>
                      <td width="70%"><div align="left">Primer Nombre del Trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Segundo Nombre </div></td>
                      <td width="70%"><div align="left">Segundo Nombre del Trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Sexo</div></td>
                      <td width="70%"><div align="left">Identifca el sexo del 
                          Trabajador a saber F: Femenino M: Masculino</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Estado Civil</div></td>
                      <td width="70%"><div align="left">Estado Civil del Trabajador 
                          a saber Soltero,Casado, Divorciado, Viudo</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Es padre o madre</div></td>
                      <td width="70%"><div align="left">Indicar (S/N) si el trabajador 
                          es padre o madre.</div></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Fecha Nacimiento</div></td>
                      <td width="70%"><div align="left">Fecha de nacimiento del 
                          trabajador expresado en dd-mm-aaaa</div></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Nacionalidad</div></td>
                      <td width="70%"><div align="left">Indica la nacionalidad 
                          del trabajador V: Venezolano E: Extranjero</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Pa&iacute;s Nacimiento</div></td>
                      <td width="70%"><div align="left"> Pais de Nacimiento del 
                          trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Estado Nacimiento</div></td>
                      <td width="70%"><div align="left">Estado de Nacimiento del 
                          trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Ciudad Nacimiento</div></td>
                      <td width="70%"><div align="left">Ciudad de Nacimiento del 
                          trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Fecha Fallecimiento </div></td>
                      <td width="70%"><div align="left">Fecha en que el trabajador 
                          fallece</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Doble Nacionalidad </div></td>
                      <td width="70%"><div align="left">Indicar (S/N) si el trabajador 
                          posee doble nacionalidad</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Otra Nacionalidad </div></td>
                      <td width="70%"><div align="left">Indicar la segunda nacionalidad 
                          del trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Nacionalizado</div></td>
                      <td width="70%"><div align="left">Indicar (S/N) si el trabajador 
                          es nacionalizado</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Fecha Nacionalizacion</div></td>
                      <td width="70%"><div align="left">Fecha en que se le fue 
                          otorgada la nacionalidad</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Gaceta</div></td>
                      <td width="70%"><div align="left">Numero de la gaceta oficial 
                          donde quedo registrada la nacionalidad</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Otra Normativa</div></td>
                      <td width="70%"><div align="left">Normativa legal distinta 
                          a la gaceta donde se encuentra registrada la nacionalidad 
                          del trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Direccion</div></td>
                      <td width="70%"><div align="left">Direccion del Habitacion 
                          del Trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Zona Postal</div></td>
                      <td width="70%"><div align="left">N&uacute;mero del zona postal.</td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Pa&iacute;s de Residencia</div></td>
                      <td width="70%"><div align="left">Pa&iacute;s al que pertenece 
                          la residencia del trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Estado de Residencia</div></td>
                      <td width="70%"><div align="left">Estado al que pertenece 
                          la residencia del trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%" height="24"> <div align="left">Ciudad de 
                          Residencia</div></td>
                      <td width="70%"><div align="left"> 
                          <p>Ciudad a la que pertenece la residencia del trabajador</p>
                        </div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Municipio de Residencia</div></td>
                      <td width="70%"><div align="left">Municipio al que pertenece 
                          la residencia del trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Parroquia de Residencia</div></td>
                      <td width="70%"><div align="left">Parroquia a la que pertenece 
                          la residencia del trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Telefono</div></td>
                      <td width="70%"><div align="left">N&uacute;mero telef&oacute;nico 
                          de habitaci&oacute;n del trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Celular</div></td>
                      <td width="70%"><div align="left">N&uacute;mero del movil 
                          celular del trabajador Ej: 0412-8858962</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Telefono Oficina</div></td>
                      <td width="70%"><div align="left">N&uacute;mero telefonico 
                          de la oficina del trabajador Ej: 0212-5623698</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Email</div></td>
                      <td width="70%"><div align="left">Direcci&oacute;n electronica 
                          del trabajador ya sea institucional o no.</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">A&ntilde;os de Servicio</div></td>
                      <td width="70%"><div align="left">Dato que se utiliza para 
                          el determinar el tiempo de servicio en el proceso de 
                          c�lculo de prestaciones.</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Tipo Vivienda</div></td>
                      <td width="70%"><div align="left">Tipo de Vivienda donde 
                          pernota el trabajador</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Tenencia Vivienda </div></td>
                      <td width="70%"><div align="left">Indicar (P/A) la tenencia 
                          de la vivienda</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Maneja</div></td>
                      <td width="70%"><div align="left">Indicar (S/N) si el trabajador 
                          maneja .</div></td>
                    </tr>
                    <tr> 
                      <td colspan="6" class="bgtable2"></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Grado Licencia </div></td>
                      <td width="70%"><div align="left">Grado de Licencia que 
                          porta el trabajador a saber 1era, 2da, 3era, 4ta o 5ta</div></td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Vehiculo</td>
                      <td width="70%"><div align="left">Indicar el tipo de vehiculo que posee el trabajador</td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Marca</td>
                      <td width="70%"><div align="left">Marca del Veh&iacute;culo del Trabajador</td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Modelo</td>
                      <td width="70%"><div align="left">Modelo del Veh&iacute;culo del Trabajador</td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Placa</td>
                      <td width="70%"><div align="left">Placa del Veh&iacute;culo del trabajador</td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">N&ordm; SSO</td>
                      <td width="70%"><div align="left">N&uacute;mero del SSO que identifica al trabajador</td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">N&ordm; RIF</td>
                      <td width="70%"><div align="left">N&uacute;mero del RIF del trabajador como persona natural</td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">N&ordm; Libreta Militar</td>
                      <td width="70%"><div align="left">N&uacute;mero de la libreta militar</td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Estatura</td>
                      <td width="70%"><div align="left">Indica la estatura del trabajador en mtos Ej: 1.80mtos</td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Peso</td>
                      <td width="70%"><div align="left">Indica el peso del trabajador en Kg, Ej:55 Kg</td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Diestralidad</td>
                      <td width="70%"><div align="left">Indica si el trabajador es diestro o izquierdo o ambidiestro</td>
                    </tr>
                    <tr> 
                      <td width="30%"><div align="left">Grupo Sanguineo</td>
                      <td width="70%"><div align="left">Indica que grupo sanguineo del trabajador </td>
                    </tr>
                  </table>
										<!--
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">C�dula</div></td>
												<td width="70%"><div align="left">N�mero de c�dula de identidad. Este campo no puede ser modificado</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primer Apellido</div></td>
												<td><div align="left">Primer apellido del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Segundo Apellido</div></td>
												<td><div align="left">Segundo apellido del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primer Nombre</div></td>
												<td><div align="left">Primer nombre del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Segundo Nombre</div></td>
												<td><div align="left">Segundo nombre del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sexo</div></td>
												<td><div align="left">Sexo del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Estado Civil</div></td>
												<td><div align="left">Estado civil del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha de Nacimiento</div></td>
												<td><div align="left">Fecha de Nacimiento del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nacionalidad</div></td>
												<td><div align="left">Nacionalidad del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Pais, estado y ciudad de Nacimiento</div></td>
												<td><div align="left">Pa�s, estado y ciudad de nacimiento del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Doble nacionalidad ?</div></td>
												<td><div align="left">Indicar si el trabajador tiene otra nacionalidad</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Otra nacionalidad</div></td>
												<td><div align="left">Si la respuesta anterior es afirmativa, indicar el pais de la segunda nacionalidad</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nacionalizado ?</div></td>
												<td><div align="left">Indicar si el trabajador es nacionalizado</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha de nacionalizaci�n</div></td>
												<td><div align="left">Si es nacionalizado, indicar la fecha en que se nacionaliz�</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Gaceta</div></td>
												<td><div align="left">Si es nacionalizado, indicar n�mero de gaceta donde se public�</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Otra Normativa</div></td>
												<td><div align="left">Si es nacionalizado, indicar otra normativa por la que se haya nacionalizado diferente a gaceta</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Direcci�n de residencia</div></td>
												<td><div align="left">Direcci�n de residencia del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Zona Postal</div></td>
												<td><div align="left">Zona Postal</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Pa�s, estado, ciudad, municipio y parroquia de residencia</div></td>
												<td><div align="left">Pa�s, estado, ciudad, municipio y parroquia de residencia del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tel�fonos</div></td>
												<td><div align="left">Tel�fono de residencia, tel�fono celular y tel�fono de oficina del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">email</div></td>
												<td><div align="left">Direcci�n electr�nica del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�dula del conyuge</div></td>
												<td><div align="left">Si el estado civil indicado es casado o unido, se debe indicar la c�dula de identidad del conyuge</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre del conyuge</div></td>
												<td><div align="left">Si el estado civil indicado es casado o unido, se debe indicar el nombre del conyuge</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sector donde trabaja ?</div></td>
												<td><div align="left">Si el estado civil indicado es casado o unido, se debe indicar el sector donde trabaja el c�nyuge, a saber: p�blico, privado o ninguno</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Trabaja en el mismo organismo ?</div></td>
												<td><div align="left">Si el estado civil indicado es casado o unido, se debe indicar si el conyuge trabaja en el mismo organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Vivienda</div></td>
												<td><div align="left">Tipo de vivienda del trabajador, a saber: casa, apartamento, habitaci�n u hotel</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tenencia de Vivienda</div></td>
												<td><div align="left">Tenencia de vivienda del trabajador, a saber: alquilada, propia, pagando o de un familiar</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Maneja ?</div></td>
												<td><div align="left">Indicar si el trabajador conduce</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grado de licencia</div></td>
												<td><div align="left">Indicar el grado de licencia de conducir del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tiene veh�culo ?</div></td>
												<td><div align="left">Indicar si el trabajador tiene veh�culo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Marca</div></td>
												<td><div align="left">Si la respuesta anterior fu� afirmativa, debe indicar la marca del veh�culo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Modelo</div></td>
												<td><div align="left">Si la respuesta anterior fu� afirmativa, debe indicar el modelo del veh�culo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Placa</div></td>
												<td><div align="left">Si la respuesta anterior fu� afirmativa, debe indicar la placa del veh�culo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nro. SSO</div></td>
												<td><div align="left">N�mero del Seguro Social Obligatorio</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nro. RIF</div></td>
												<td><div align="left">N�mero del RIF</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nro. Libreta Militar</div></td>
												<td><div align="left">N�mero de libreta militar</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Peso</div></td>
												<td><div align="left">Peso del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Estatura</div></td>
												<td><div align="left">Estatura del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Diestralidad</div></td>
												<td><div align="left">Diestralidad</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grupo Sanguineo</div></td>
												<td><div align="left">Grupo sanguineo</div></td>
											</tr>
										</table>
										-->
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: Primer Apellido + Segundo Apellido + Primer Nombre + Segundo Nombre + Cedula.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
