<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		<a  name="_inicio">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Historial de Cargos/Sueldos</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<tr>
								  <td align="left">

								  </td>
								</tr>
								<!----------->
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>

											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</td>
											</tr>
											<tr>
												<td align="center">
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center"><a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo Personal</a></td>
														</tr>
														<tr>														
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/registro/CausaMovimiento.jsp" class="linktext">Causas de Movimiento</a>
															</td>
														</tr>
														<tr>														
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/estructura/Dependencia.jsp" class="linktext">Dependencias</a>
															</td>
														</tr>
														<tr>														
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/cargo/Cargo.jsp" class="linktext">Cargos</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="right">
													<a href="#_inicio" class="Link">Ir a Inicio</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%">
											<tr>
												<td align="center">
													<table width="100%" class="datatable">
														<tr>
															<td>
																Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n del historial de cargos de un trabajador dentro del organismo. Una vez realizada la carga inicial de este m&oacute;dulo, el sistema actualizar&aacute; esta informaci&oacute;n de forma autom&aacute;tica a trav&eacute;s de diferentes procesos. </td>
														</tr>
														<tr>
															<td>
																<div align="center">
																	<img src="../../../../images/ayuda/ciudad/screenshot.gif" width="560" height="150">
																</div>
															</td>
														</tr>

													</table>
													
													<table class="bandtable" width="95%" align="center">
														<tr>
															<td width="17%"><div align="left">Campo</div></td>
															<td width="34%"><div align="left">Descripci�n</div></td>
															<td width="10%"><div align="center">Tama&ntilde;o</div></td>
															<td width="13%"><div align="center">Requerido</div></td>
															<td width="14%"><div align="center">Modificable</div></td>
															<td width="12%"><div align="center">Busqueda</div></td>
														</tr>
													</table>
													<table width="95%" class="datatableborder" align="center">
														<tr>
															<td width="17%"><div align="left">Fecha</div></td>
															<td width="34%"><div align="left">Fecha Movimiento</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Remesa</div></td>
															<td width="34%"><div align="left">Numero remesa</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">N� Movimiento</div></td>
															<td width="34%"><div align="left">Numero Movimiento</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Movimiento</div></td>
															<td width="34%"><div align="left">Seleccionar el <a href="../../base/personal/MovimientoPersonal.jsp" class="linktext">Movimiento de Personal</a> de acuerdo a la lista.</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Causa</div></td>
															<td width="34%"><div align="left">Seleccionar la <a href="../../base/registro/CausaMovimiento.jsp" class="linktext">Causa de Movimiento</a> de acuerdo a la lista.</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Aprueba MPD</div></td>
															<td width="34%"><div align="left">Indicar si este movimiento debe ser aprobado por MPD (S/N)</div></td>
															<td width="10%"><div align="center">1</div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Cargo</div></td>
															<td width="34%"><div align="left">Seleccionar el <a href="../../base/cargo/Cargo.jsp" class="linktext">Cargo</a> de acuerdo a la lista.</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">C�digo RAC/RPT</div></td>
															<td width="34%"><div align="left">C�digo RAC</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Sueldo</div></td>
															<td width="34%"><div align="left">Sueldo Basico</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Compensaci�n</div></td>
															<td width="34%"><div align="left">Compensacion</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Primas Cargo</div></td>
															<td width="34%"><div align="left">Primas inherentes al Cargo</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Primas Trabajador</div></td>
															<td width="34%"><div align="left">Primas propias del Trabajador</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Grado</div></td>
															<td width="34%"><div align="left">Grado</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Paso</div></td>
															<td width="34%"><div align="left">Paso</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Dependencia</div></td>
															<td width="34%"><div align="left">Nombre de la Dependencia Administrativa </div></td>
															<td width="10%"><div align="center">100</div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Tipo Personal</div></td>
															<td width="34%"><div align="left">Seleccionar el <a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo Personal</a> de acuerdo a la lista.</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Personal</div></td>
															<td width="34%"><div align="left">Tipo Personal (descripcion)</div></td>
															<td width="10%"><div align="center">40</div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														
														<tr>
															<td width="17%"><div align="left">Estatus</div></td>
															<td width="34%"><div align="left">Estatus (1)aprobado (2)Tramite </div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Soporte</div></td>
															<td width="34%"><div align="left">Documento soporte</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Dependencia</div></td>
															<td width="34%"><div align="left">Seleccionar la <a href="../../base/estructura/Dependencia.jsp" class="linktext">Dependencia</a> de acuerdo a la lista.</div></td>
															<td width="10%"><div align="center"></div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Localidad</div></td>
															<td width="34%"><div align="left">Dependencia (C) Central/(R)Regional </div></td>
															<td width="10%"><div align="center">1</div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
														<tr>
															<td width="17%"><div align="left">Afecta Sueldo</div></td>
															<td width="34%"><div align="left">Afecta sueldo (S/N)</div></td>
															<td width="10%"><div align="center">1</div></td>
															<td width="13%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="14%"><div align="center"><img src="../../../../images/buttons/check.gif" width="20" height="20"></div></td>
															<td width="12%"><div align="center"></div></td>
														</tr>
														<tr>
															<td colspan="6" class="bgtable2"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="right">
													<a href="#_inicio" class="Link">Ir a Inicio</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: Fecha Movimiento + Descripci�n de Cargo + Descripci�n de Causa Movimiento
											  </td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="right">
													<a href="#_inicio" class="Link">Ir a Inicio</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>