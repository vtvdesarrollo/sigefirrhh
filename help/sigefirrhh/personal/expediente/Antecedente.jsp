<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Antecedentes de Servicio sujeto a LEFP</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													<p>Debe estar creada la siguiente tabla para la ejecuci&oacute;n 
													de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%" class="datatableborder"  align="center">
																<a href="Personal.jsp" class="linktext">Datos Personales</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de 
													los antecedentes de servicios de un trabajador, durante su desempe&ntilde;o 
													en la Administraci&oacute;n P&uacute;blica en todos aquellos organismos que 
													se rigen bajo LEFP.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Trabajador</div></td>
												<td width="70%"><div align="left">Apellidos y Nombres del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Instituci�n</div></td>
												<td><div align="left">Instituci�n donde labor� el trabajador</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Personal al Ingreso</div></td>
												<td><div align="left">Tipo de Personal del trabajador al momento que ingreso al organismo</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Ingreso</div></td>
												<td><div align="left">Fecha de ingreso al organismo indicado</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo Cargo al ingreso</div></td>
												<td><div align="left">C�digo de cargo al ingresar,seg�n el manual de cargos</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cargo al ingreso</div></td>
												<td><div align="left">Cargo al ingresar, seg�n el manual de cargos
</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo RAC al ingreso</div></td>
												<td><div align="left">C�digo de RAC al ingresar al Organismo
</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sueldo al Ingreso</div></td>
												<td><div align="left">Sueldo al ingreso, Al tabulador correspondiente</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Compensaci�n al Ingreso</div></td>
												<td><div align="left">Compensaci�n al ingreso, de Si le corresponde</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primas al Ingreso</div></td>
												<td><div align="left">Primas propias del cargo y del trabajador al  momento del ingreso del trabajador
</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Personal al Egresar</div></td>
												<td><div align="left">Tipo de Personal del trabajador al momento que egres� del organismo</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Egreso</div></td>
												<td><div align="left">Fecha de egreso al organismo indicado</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tiempo de servicio</div></td>
												<td><div align="left">Al hacer click en el boton calcular, el sistema reflejara el tiempo de servicio que estuvo en el organismo</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo Cargo al egreso</div></td>
												<td><div align="left">C�digo de cargo al egresar, seg�n el manual de cargos</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cargo al egreso</div></td>
												<td><div align="left">Corgo al egresar seg�n el manual de cargos</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Causa de egreso</div></td>
												<td><div align="left"> Ingresar la causa de engreso</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo RAC al egreso</div></td>
												<td><div align="left"> C�digo de RAC al egresar del organismo</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sueldo al Egreso</div></td>
												<td><div align="left">Sueldo al egreso, Al tabulador correspondiente</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Compensaci�n Egreso</div></td>
												<td><div align="left">Indicar la compensaci�n al egreso del trabajador si la tubiese, de acuerdo a tabuladores suministrados por MPD</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primas del Cargo Egreso</div></td>
												<td><div align="left">Primas propias del cargo y del trabajador para el momento del egreso del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Prestaciones pendientes ?</div></td>
												<td><div align="left">Indicador si el trabajador egres� y qued� pendiente el pago de prestaciones</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Vacaciones pendientes ?</div></td>
												<td><div align="left">Indicador si el trabajador egres� y qued� pendiente el pago y/o disfrute de vacaciones</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">D�as de vacaciones</div></td>
												<td><div align="left">Si la repuesta anterior es afirmativa, indicar los dias de vacaciones que le quedaron pendiente al trabajador</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Observaciones</div></td>
												<td><div align="left">Observaciones generales</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: Nombre Instituci�n + Fecha Ingreso + Fecha Egreso
											  </td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
