<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Estudios Informales</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Personal.jsp" class="linktext">Datos Personales </a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/adiestramiento/TipoCurso.jsp" class="linktext">Tipo Curso</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/adiestramiento/MateriaAdiestramiento.jsp" class="linktext">Materia Adiestramiento</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/ubicacion/Pais.jsp" class="linktext">Pais</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de 
													los Estudios Informales del trabajador que haya realizado por procesos de 
													capacitaci&oacute;n o por iniciativa propia.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Trabajador</div></td>
												<td width="70%"><div align="left">Apellidos y Nombres del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo</div></td>
												<td><div align="left">Relaci�n con tipo de curso, seminario, taller, ponencia, etc</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Area de Conocimiento</div></td>
												<td><div align="left">Relaci�n de un area de conocimiento con la informaci�n que se va a registrar</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre Curso/Ponencia</div></td>
												<td><div align="left">Denominaci�n del curso, taller, seminario, ponencia, etc. a registrar</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">A�o</div></td>
												<td><div align="left">A�o en que el trabajador asisti� al evento que se est� registrando</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Entidad Educativa</div></td>
												<td><div align="left">Instituci�n o entidad donde se dict� el curso, taller, etc.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Duraci�n</div></td>
												<td><div align="left">Duraci�n del evento</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Unidad de Tiempo</div></td>
												<td><div align="left">Indicar si la duraci�n fue: horas, dias, semanas o meses</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Origen Curso</div></td>
												<td><div align="left">Indicar si el trabajador asisti� por iniciativa propia o enviado por el organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Participaci�n</div></td>
												<td><div align="justify">Indicar si el trabajador asisti� como: participante, instructor, oyente, ponente o coordinador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Obtuvo Certificado?</div></td>
												<td><div align="left">Indicar si el trabajador obtuvo alg�n certificado por su participaci�n</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Pa�s</div></td>
												<td><div align="left">Seleccionar el pa�s donde ocurri� el evento</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Becado ?</div></td>
												<td><div align="left">Indicar si el trabajador fu� becado</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Financiamiento</div></td>
												<td><div align="justify">Indicar si el financiamiento de los costos fue: el organismo, financiamiento externo, financiamiento parcial o ninguno</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Observaciones</div></td>
												<td><div align="left">Observaciones generales</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: Nombre de Curso + A&ntilde;o</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>