<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Historial en el Organismo</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n: </p>
													<table width="30%" align="center">
														<tr>
															<td width="50%" class="datatableborder"  align="center">
																<a href="../../base/definiciones/ClasificacionPersonal.jsp" class="linktext">Clasificaci�n Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%" class="datatableborder"  align="center">
																<a href="../../base/registro/CausaMovimiento.jsp" class="linktext">Causa Movimiento</a>
															</td>
														</tr>
														<tr>
															<td width="50%" class="datatableborder"  align="center">
																<a href="../../base/estructura/Organismo.jsp" class="linktext">Organismo</a>
															</td>
														</tr>
														<tr>
															<td width="50%" class="datatableborder"  align="center">
																<a href="NivelEducativo.jsp" class="linktext">Personal</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n 
													de la trayectoria del trabajador en el organismo. La actualizaci&oacute;n de 
													este historial, se ir&aacute; realizando autom&aacute;ticamente una vez que 
													el sistema est&eacute; en producci&oacute;n, a medida que se procesen los 
													movimientos de personal por las opciones correspondientes. En el caso de los 
													organismos sujetos a LEFP, el historial inicial que se migrar� ser� el 
													registrado para la fecha por MPD
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Trabajador</div></td>
												<td width="70%"><div align="left">Apellidos y Nombres del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Personal</div></td>
												<td><div align="left">Relaci�n con la clasificaci�n de personal que se maneja en el sistema.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo y causa de movimiento</div></td>
												<td><div align="left">Tipo y causa del movimiento, de acuerdo a c�digos suministrados por MPD</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha del movimiento</div></td>
												<td><div align="left">Fecha de vigencia del movimiento</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Remesa</div></td>
												<td><div align="left">Remesa donde se registra el movimiento</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N� de movimiento</div></td>
												<td><div align="left">N�mero asignado al movimiento</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Manual/Lista de cargos</div></td>
												<td><div align="left">Manual o lista de cargos, de acuerdo a c�digos suministrados por MPD o c�digos propios del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo (clase) de Cargo</div></td>
												<td><div align="left">C�digo (clase) de cargo, de acuerdo a c�digos suministrados por MPD o c�digos propios del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Descripci�n de Cargo</div></td>
												<td><div align="left">Denominaci�n del cargo, de acuerdo a tablas suministradas por MPD o tablas propias del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo de Dependencia</div></td>
												<td><div align="left">C�digo de Dependencia, de acuerdo a c�digos suministrados por MPD o c�digos propios del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nombre de dependencia</div></td>
												<td><div align="left">Nombre o descripci�n de la dependencia, de acuerdo a tablas suministradas por MPD o tablas propias del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sueldo/salario B�sico</div></td>
												<td><div align="left">Sueldo o salario b�sico, de acuerdo a tabuladores suministrados por MPD o tabuladores propios del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Compensaci�n</div></td>
												<td><div align="left">Compensaci�n, de acuerdo a tabuladores suministrados por MPD o tabuladores propios del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primas del Cargo</div></td>
												<td><div align="left">Primas propias del cargo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primas del Trabajador</div></td>
												<td><div align="left">Primas propias del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Observaciones</div></td>
												<td><div align="left">Observaciones generales</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													Muestra: Fecha Movimiento + Descripci&oacute;n Movimiento + Descripci&oacute;n Cargo + Sueldo.
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>