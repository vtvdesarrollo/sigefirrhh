<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Ausencias(Unidad Funcional)</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Personal.jsp" class="linktext">Datos Personales</a>
															</td>
														</tr>		
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/personal/TipoAusencia.jsp" class="linktext">Tipo Ausencia</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%">
											<tr>
												<td align="center">
													<table width="100%" class="datatable">
														<tr>
															<td>
																Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de 
																todas las ausencias, permisos y/o reposos de un trabajador.
															</td>
														</tr>
													</table>
													
													<table class="bandtable" width="95%" align="center">
														<tr>
															<td width="30%"><div align="left">Campo</div></td>
															<td width="70%"><div align="left">Descripci�n</div></td>
														</tr>
													</table>
													<table width="95%" class="datatableborder" align="center">
														<tr>
															<td width="30%"><div align="left">Trabajador</div></td>
															<td width="70%"><div align="left">Apellidos, Nombres  y C&eacute;dula del Trabajador Seleccionado</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Tipo</div></td>
															<td><div align="left">Seleccionar el Tipo de Ausencia de acuerdo a la lista.</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Fecha Inicio</div></td>
															<td><div align="left">Fecha de Inicio</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Fecha Fin</div></td>
															<td><div align="left">Fecha Final</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Horas/Dias</div></td>
															<td><div align="left">Indicar si el per&iacute;odo a registrar ser&aacute;n Horas o D&iacute;as </div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">D�as Continuos</div></td>
															<td><div align="left">Dias Continuos</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">D�as H�biles</div></td>
															<td><div align="left">D�as H�biles</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Afecta Cesta Ticket</div></td>
															<td><div align="left">Indicar si el per&iacute;odo de falta que se est&aacute; registrando debe afectar la asignaci&oacute;n de Tickets de Alimentaci�n (S/N)</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Mes a descontar Cesta Ticket</div></td>
															<td><div align="left">Si la respuesta anterios es afirmativa, indique Mes </div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">A&ntilde;o a descontar Cesta Ticket</div></td>
															<td><div align="left">Si la respuesta anterior a la anterior es afirmativa, indique A&ntilde;o </div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>	
														<tr>
															<td><div align="left">Nombre Jefe Inmediato</div></td>
															<td><div align="left">Nombre Jefe que autoriza</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>													
														<tr>
															<td><div align="left">Nombre Supervisor</div></td>
															<td><div align="left">Nombre del Supervisor que autoriza</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Nombre Director</div></td>
															<td><div align="left">Nombre del Director de RRHH que autoriza</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Estatus </div></td>
															<td><div align="left">Seleccionar el Estatus de acuerdo a la lista</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Remunerado </div></td>
															<td><div align="left">Seleccionar si es Remunerado de acuerdo a la lista</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Clase </div></td>
															<td><div align="left">Seleccionar la clase de Ausencia de acuerdo a la lista</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">Observaciones</div></td>
															<td><div align="left">Observaciones</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">C�dula Jefe</div></td>
															<td><div align="left">C&eacute;dula del Jefe Inmediato que autoriza</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">C�dula Supervisor</div></td>
															<td><div align="left">Cedula del Supervisor que autoriza</div></td>
														</tr>
														<tr>
															<td colspan="2" class="bgtable2"></td>
														</tr>
														<tr>
															<td><div align="left">C�dula Director</div></td>
															<td><div align="left">Cedula del Director de RRHH que autoriza</div></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: Descripci�n de Tipo Ausencia + Fecha Inicio
											  </td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>