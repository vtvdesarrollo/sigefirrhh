<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Calculo Prima Antiguedad</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
													<li><a class="Link" href="#_procedimiento">Procedimiento</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/PrimaAntiguedad.jsp" class="linktext">Par�metros Prima Antig�edad</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="SeguridadAniversario.jsp" class="linktext">Seguridad Procesos Aniversario</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" class="datatable">
											<tr>
												<td>
													Proceso que permite registrar y/o actualizar el concepto de pago por Prima de Antig�edad 
													en el mes aniversario de los trabajadores, bas�ndose en los criterios fijados en los 
													par�metros de Prima por Antig�edad, ya descritos en la secci�n de Par�metros y F�rmulas 
													de N�mina.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">El usuario debe seleccionar el tipo de personal que se va a procesar.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Desde-Hasta</div></td>
												<td>
													<div align="left">
														El sistema mostrar� la fecha desde, tomado del registro de Seguridad Aniversario 
														la �ltima fecha que se realiz� el proceso. La fecha hasta ser� indicada por el usuario.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Proceso</div></td>
												<td>
													<div align="left">
														El sistema permite realizar el proceso en modo  prueba o modo definitivo.<br>
														<br>
														El modo prueba no realiza actualizaci�n alguna, el modo definitivo, registra o actualiza 
														el concepto correspondiente en los conceptos fijos del trabajador, aplicando la frecuencia 
														y formulaci�n indicada en la definici�n del concepto en Conceptos por Tipo de Personal, y 
														utilizando los criterios indicados en par�metros y f�rmula, de acuerdo a los años de 
														servicio que cumple el trabajador.<br>
														<br>
														En modo definitivo el sistema actualiza la �ltima fecha que se ejecut� el proceso, 
														en seguridad aniversario.<br>
														<br>
														El sistema tomara como fecha base para el proceso, la fecha registrada como fecha 
														c�lculo antig�edad? en los datos del trabajador.<br>
														<br>
														Si el concepto se encuentra registrado en Conceptos por Tipo de Cargo? con alguna 
														caracter�stica especial est� ser� tomada en cuenta para el proceso.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Reporte</div></td>
												<td>
													<div align="left">
														Permite generar los siguientes reportes:<br>
														<ul>
															<li>Prima de Antig�edad</li>
															<li>Prima de Antig�edad Anual</li>
														</ul>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								<!----------->
								
								<!--Procedimiento-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_procedimiento">Procedimiento</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="center">
													<img src="/sigefirrhh/help/images/personal/procesoAniversario/CalcularPrimaAntiguedad.gif" border="0">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>