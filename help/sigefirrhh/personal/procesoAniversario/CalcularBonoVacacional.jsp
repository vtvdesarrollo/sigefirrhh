<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Calculo Bono Vacacional</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/ConceptoVacaciones.jsp" class="linktext">F�rmula Bono Vacacional (Conceptos Vacaciones)</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/ConceptoVacaciones.jsp" class="linktext">Par�metros Varios</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/VacacionesPorAnio.jsp" class="linktext">D�as Vacaciones por A�o</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" class="datatable">
											<tr>
												<td>
													Proceso que permite registrar el concepto de pago por Bono Vacacional en el mes aniversario 
													de los trabajadores o en el mes de disfrute, de acuerdo a lo que se haya fijado en el registro 
													de Par&aacute;metros Varios. El c&aacute;lculo lo realiza  bas&aacute;ndose en los criterios 
													fijados en los par&aacute;metros de F&oacute;rmula del Bono Vacacional, ya descritos en la 
													secci&oacute;n de Par&aacute;metros y F&oacute;rmulas de N&oacute;mina. Si se indic&oacute; 
													que debe tomarse la alicuota correspondiente al bono de fin de a&ntilde;o, el sistema har&aacute; 
													los c&aacute;lculos correspondientes, y registrar&aacute; el valor obtenido en el concepto '1601' 
													que debe estar registrado y reservado por SIGEFIRRHH. Si se indic&oacute; que debe tomarse la 
													alicuota correspondiente al bono petrolero, el sistema har&aacute; los c&aacute;lculos 
													correspondientes, y registrar&aacute; el valor obtenido en el concepto '1701' que debe estar 
													registrado y reservado por SIGEFIRRHH. Si se indic&oacute; que se pagan d&iacute;as extras, el 
													sistema har&aacute; los c&aacute;lculos correspondientes, y registrar&aacute; el valor obtenido 
													en el concepto '1502' que debe estar registrado y reservado por SIGEFIRRHH.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci&oacute;n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">El usuario debe seleccionar el tipo de personal que se va a procesar.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Desde-Hasta</div></td>
												<td><div align="left">El sistema mostrar&aacute; la fecha desde, tomado del registro de Seguridad Aniversario la &uacute;ltima fecha que se realiz&oacute; el proceso. La fecha hasta ser&aacute; indicada por el usuario.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Proceso</div></td>
												<td>
													<div align="left">
														El sistema permite realizar el proceso en modo  prueba o modo definitivo.<br>
														El modo prueba no realiza actualizaci&oacute;n alguna.<br>
														El modo definitivo, registra el concepto correspondiente en los conceptos variables del 
														trabajador, aplicando la frecuencia y formulaci&oacute;n indicada en la definici&oacute;n 
														del concepto en Conceptos por Tipo de Personal, y utilizando los criterios indicados en 
														par&aacute;metros y f&oacute;rmula, de acuerdo a los a&ntilde;os de servicio que cumple el 
														trabajador.<br>
														En modo definitivo el sistema actualiza la &uacute;ltima fecha que se ejecut&oacute; el 
														proceso, en seguridad aniversario.<br>
														El sistema tomara como fecha base para el proceso, la fecha registrada como &quot;fecha 
														c&aacute;lculo vacaciones&quot; en los datos del trabajador.
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Reporte</div></td>
												<td>
													<div align="left">
														Permite generar los siguientes reportes:<br>
														<br>
														&nbsp;&nbsp;&nbsp;Bono Vacacional<br>
														&nbsp;&nbsp;&nbsp;Bono Vacacional por UEL<br>
														&nbsp;&nbsp;&nbsp;Bono Vacacional con Detalle Alicuotas<br>
														&nbsp;&nbsp;&nbsp;Bono Vacacional Anual - Este	reporte	puede generarse a comienzos 
														del ejercicio, para estimar los gastos mensuales a incurrir por este concepto
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: </td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>