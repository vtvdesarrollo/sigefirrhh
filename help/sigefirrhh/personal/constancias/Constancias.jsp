<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Constancias</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de B&uacute;squeda</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/expediente/Personal.jsp" class="linktext">Datos Trabajador</a>
															</td>
														</tr>	
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
                                                        </tr>																										
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/Concepto.jsp" class="linktext">Concepto</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/estructura/Dependencia.jsp" class="linktext">Dependencias Administrativas</a>
															</td>
														</tr>
														
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite la definici&oacute;n de los diferentes tipos de constancias de trabajo que genera el Organismo.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%" height="22"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Seleccionar el tipo de personal.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Constancia</div></td>
												<td><div align="left">Seleccionar el tipo de constancia que se va a definir (Mensual, anual, sin sueldo, etc.)</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Oficina/Direcci&oacute;n</div></td>
												<td><div align="left">Indicar la Unidad Administrativa, responsable de la emisi&oacute;n de la constancia de trabajo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>	
											<tr>
												<td><div align="left">Apellidos y Nombres Firmantes</div></td>
												<td><div align="left">Se deben colocar los Apellidos y Nombres del funcionario responsable de emitir la constancia de trabajo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Cargo</div></td>
												<td><div align="left">Indicar el cargo del funcionario responsable de la emisi&oacute;n de la constancia de trabajo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>																																																						
											<tr>
												<td><div align="left">Nombramiento</div></td>
												<td><div align="left">Indicar el N� y fecha de la Gaceta Oficial, Resoluci&oacute;n o Delegaci&oacute;n de firma del funcionario responsable de la emisi&oacute;n de la constancia de trabajo.</div></td>
											</tr>
										    <tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>

											<tr>
												<td><div align="left">Pie de P�gina 1</div></td>
												<td><div align="left">Indicar la direcci&oacute;n de la sede administrativa del Organismo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Pie de P�gina 2</div></td>
												<td><div align="left">Indicar los n&uacute;meros telef&oacute;nicos y el fax del Organismo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Pie de P�gina 3</div></td>
												<td><div align="left">Indicar la direcci&oacute;n de correo electr&oacute;nico del Organismo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Iniciales</div></td>
												<td><div align="left">Colocar las iniciales del Analista responsable de la preparaci&oacute;n de la constancia.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Observaci�n</div></td>
												<td><div align="left">Se�alar alg&uacute;n tipo de informaci&oacute;n que se considere relevante que deba reflejarse en la constancia.</div></td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de B&uacute;squeda</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="left">
													Muestra: Nombre + C&oacute;digo Banco
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>