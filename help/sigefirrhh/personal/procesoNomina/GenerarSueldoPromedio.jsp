<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Calcular Sueldos Promedios</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
											Opci�n que permite actualizar los sueldos promedios de los trabajadores
											del grupo de n�mina seleccionado.<br>
											<br>
														Este proceso actualiza los siguientes campos asociados a cada trabajador:<br>
														<ul>
															<li>Sueldo Integral</li>
															<li>Sueldo B�sico</li>
															<li>Ajustes de Sueldo</li>
															<li>Compensaciones</li>
															<li>Primas por Cargo</li>
															<li>Primas por Trabajador</li>
															<li>Base Semanal de Retenci�n SSO</li>
															<li>Retenci�n Semanal SSO</li>
															<li>Aporte Patronal Semanal SSO</li>
															<li>Retenci�n Semanal SPF</li>
															<li>Aporte Patronal Semanal SPF</li>
															<li>Base Mensual de Retenci�n LPH</li>
															<li>Retenci�n Mensual LPH</li>
															<li>Aporte Patronal Mensual LPH</li>
															<li>Base Mensual de Retenci�n FJU</li>
															<li>Retenci�n Mensual FJU</li>
															<li>Aporte Patronal Mensual FJU</li>
														</ul>
														<br>
														Para los c�lculos utiliza los indicadores que estan asociados a los
														conceptos en su definici�n.<br>
														<br>
														Esta opci�n realiza el rec�lculo de todos los conceptos asociados al
														grupo de n�mina seleccionado, que tengan el indicador de "rec�lculo
														autom�tico" en "SI". Hay que recordar que la ejecuci�n de esta opci�n
														puede tomarse un tiempo considerable.
			
												</td>
											</tr>
										</table>
									
									</td>
								</tr>
								<!----------->
								
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>