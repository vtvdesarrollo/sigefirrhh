<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Reportes de Prenomina Especial</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n para generar todos los listados correspondientes a una pren�mina
													especial procesada
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Grupo N�mina</div></td>
												<td width="70%"><div align="left">Se selecciona el grupo de n�mina</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N�mina Especial</div></td>
												<td><div align="left">Se selecciona la n�mina especial a listar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Desde</div></td>
												<td><div align="left">El sistema mostrar� el inicio del per�odo a procesar, el cual se indico
																en la definici�n de la n�mina especial</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Hasta</div></td>
												<td><div align="left">El sistema mostrar� el final del per�odo a procesar, el cual se indico
																en la definici�n de la n�mina especial</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Unidad Administradora</div></td>
												<td><div align="left">Se  puede seleccionar listar una o todas las unidades administradoras</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Criterios</div></td>
												<td><div align="left">
													Se puede generar los siguiente listados:<br>
													<ul>
														<li>Prenomina por Codigo</li>
														<li>Prenomina Alfabetico</li>
														<li>Prenomina por UEL</li>
														<li>Prenomina por Region</li>
														<li>Prenomina por Dependencia</li>
														<li>Detalle de Conceptos Alfab�tico</li>
														<li>Detalle de Conceptos por C�digo</li>
														<li>Detalle de Conceptos por C�dula</li>
														<li>Resumen de Conceptos General</li>
														<li>Resumen de Conceptos por UEL</li>
														<li>Resumen de Conceptos por Categoria Presupuestaria</li>
														<li>Resumen de Conceptos por Categoria Presupuestaria/UE</li>
														<li>Resumen de Conceptos por Unidad Administradora</li>
														<li>Dep�sitos Bancarios Alfab�tico</li>
														<li>Dep�sitos Bancarios por C�digo</li>
														<li>Dep�sitos Bancarios por C�dula</li>
														<li>Dep�sitos Bancarios por Unidad Administradora</li>
														<li>Dep�sitos Bancarios por Tipo de Cuenta</li>
														<li>Listado de Cheques</li>
														<li>Sobregirados</li>
													</ul>
												</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>