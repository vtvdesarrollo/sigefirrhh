<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Proceso: Generar Prenomina Ordinaria</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite ejecutar el proceso de la pren�mina ordinaria
													para un grupo de n�mina.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Grupo N�mina</div></td>
												<td width="70%"><div align="left">Seleccionar el grupo de n�mina a procesar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Desde</div></td>
												<td><div align="left">El sistema mostrar� el inicio del per�odo a procesar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Hasta</div></td>
												<td><div align="left">El sistema mostrar� el final del per�odo a procesar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Semana del A�o</div></td>
												<td><div align="left">En el caso de pren�minas semanales mostrar� la semana del a�o que corresponde</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Rec�lculo Conceptos?</div></td>
												<td><div align="left">Indicar si se desea recalcular los conceptos de los trabajadores
																		que tengan el indicador de "rec�lculo autom�tico" en "SI". 
																		Hay que recordar que la ejecuci�n de esta opci�n puede 
																		tomarse un tiempo considerable
												</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Procedimiento-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_procedimiento">Procedimiento</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="center">
													<img src="/sigefirrhh/help/images/personal/procesoNomina/RequisitosPrenominaOrdinaria.gif" border="0">
													<img src="/sigefirrhh/help/images/personal/procesoNomina/PrenominaOrdinaria.gif" border="0">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>