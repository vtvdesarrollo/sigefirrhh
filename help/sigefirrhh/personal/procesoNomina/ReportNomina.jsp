<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Reportes de Nomina</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n para generar todos los listados correspondientes a la �ltima
													n�mina ordinaria procesada
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Grupo N�mina</div></td>
												<td width="70%"><div align="left">Se selecciona el grupo de n�mina</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Desde</div></td>
												<td><div align="left">El sistema muestra el inicio del per�odo correspondiente a la �ltima
														n�mina procesada</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Hasta</div></td>
												<td><div align="left">El sistema muestra el final del per�odo correspondiente a la �ltima
														n�mina procesada</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Semana del A�o</div></td>
												<td><div align="left">En el caso de n�minas semanales, el sistema muestra el n�mero de semana
														del a�o correspondiente.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Unidad Administradora</div></td>
												<td><div align="left">Se  puede seleccionar listar una o todas las unidades administradoras.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primera Firma</div></td>
												<td><div align="left">Seleccionar entre las firmas la primera firma</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Segunda Firma</div></td>
												<td><div align="left">Seleccionar entre las firmas la segunda firma</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tercera Firma</div></td>
												<td><div align="left">Seleccionar entre las firmas la tercera firma</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Criterios</div></td>
												<td><div align="left">
													Se puede generar los siguiente listados:<br>
													<ul>
														<li>Nomina por Codigo</li>
														<li>Nomina Alfabetico</li>
														<li>Nomina por UEL</li>
														<li>Nomina por Region</li>
														<li>Nomina por Dependencia</li>
														<li>Detalle de Conceptos Alfab�tico</li>
														<li>Detalle de Conceptos por C�digo</li>
														<li>Detalle de Conceptos por C�dula</li>
														<li>Resumen de Conceptos General</li>
														<li>Resumen de Conceptos por UEL</li>
														<li>Resumen de Conceptos por Categoria Presupuestaria</li>
														<li>Resumen de Conceptos por Categoria Presupuestaria/UEL</li>
														<li>Resumen de Conceptos con aportes tama�o carta</li>
														<li>Resumen de Conceptos con aportes por UEL</li>
														<li>Resumen de Conceptos por Unidad Administradora</li>
														<li>Dep�sitos Bancarios Alfab�tico</li>
														<li>Dep�sitos Bancarios por C�digo</li>
														<li>Dep�sitos Bancarios por C�dula</li>
														<li>Dep�sitos Bancarios por Unidad Administradora</li>
														<li>Dep�sitos Bancarios por Tipo de Cuenta</li>
														<li>Listado de Cheques</li>
														<li>Listado de Cheques por Unidad Administradora</li>
														<li>Listado de Cheques por Dependencia</li>
														<li>Lista de Pagos a Beneficiarios</li>
														<li>Netos a Pagar</li>
														<li>Aceptaci�n de Pagos por Unidad Administradora</li>
														<li>Aceptaci�n de Pagos por Lugar de Pago</li>
														<li>Aceptaci�n de Pagos por Dependencia con salto</li>
														<li>Aceptaci�n de Pagos por Dependencia sin salto</li>
														<li>Aceptaci�n de Pagos por Dependencia Sin Monto</li>
														<li>Aceptaci�n de Pagos por Uel Sin Monto</li>
														<li>Recibos de Pago Alfab�tico</li>
														<li>Recibos de Pago por C�digo</li>
														<li>Recibos de Pago por C�dula</li>
													</ul>
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Recibos de Pago (distribuci�n)</div></td>
												<td><div align="left">
													Si se selecciona un reporte de recibos de pago, hay que indicar la
													distribuci�n que se desea, a saber:<br>
													<ul>
														<li>1 por p�gina tama�o carta</li>
														<li>2 por p�gina tama�o carta</li>
														<li>3 por p�gina tama�o carta</li>
														<li>4 por p�gina tama�o carta</li>
													</ul>
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Recibos de Pago (agrupaci�n)</div></td>
												<td><div align="left">
													Si se selecciona un reporte de recibos de pago, hay que indicar la
													agrupaci�n que se desea, a saber:<br>
													<ul>
														<li>Por Unidad Ejecutora</li>
														<li>Por Dependencia</li>
													</ul>
													Si se selecciona los reportes de detalle de conceptos, se puede 
													indicar un rango de conceptos.
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Concepto Desde</div></td>
												<td><div align="left">Se indica listar el reporte de detalle a partir de este concepto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Concepto Hasta</div></td>
												<td><div align="left">Se indica listar el reporte de detalle hasta este concepto</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Mensaje</div></td>
												<td><div align="left">El usuario puede indicar un mensaje que se imprimir� en todos los
															recibos de pago.</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: .</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>