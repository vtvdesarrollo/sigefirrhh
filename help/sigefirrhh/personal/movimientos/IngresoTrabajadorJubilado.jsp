<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Ingreso a N�mina Jubilado/Pensionado</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de B&uacute;squeda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/registro/RegistroCargos.jsp" class="linktext">Registro  Cargos</a>
															</td>
														</tr>
														
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/expediente/Personal.jsp" class="linktext">Datos Personales</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Se refiere al movimiento de personal originado por el otorgamiento  de la jubilaci&oacute;n a un funcionario que ha cumplido con los requisitos establecidos en la ley del Estatuto sobre el R&eacute;gimen de Jubilaciones y Pensiones de los empleados de Administraci&oacute;n P&uacute;blica Nacional de los Estados y de los Municipios.<br>
                                                    Al realizar el movimiento, el sistema realiza las siguientes actualizaciones, a saber:<br>
                                                    Actualiza los datos del trabajador, y lo desincorpora de la n&oacute;mina de personal activo.<br>
                                                    Deja vacante la posici&oacute;n del registro que ocupaba el trabajador.<br>
                                                    Graba el movimiento en la trayectoria del trabajador<br>
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Trabajador</div></td>
												<td width="70%"><div align="left">C&eacute;dula y nombre del trabajador</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Seleccionar el tipo de personal que corresponda</div></td>
											</tr>
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Movimiento</div></td>
												<td width="70%"><div align="left">Indicar el tipo de movimiento</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Regi&oacute;n</div></td>
												<td><div align="left">Indicar la regi&oacute;n que corresponda</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Sede</div></td>
												<td><div align="left">Indicar la sede respectiva</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Dependencia</div></td>
												<td><div align="left">Se�alar la dependencia administrativa</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>	
											<tr>
												<td><div align="left">Clasificador</div></td>
												<td><div align="left">Indicar el clasificador correspondiente</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>																																
											<tr>
												<td width="30%"><div align="left">Cargo</div></td>
												<td width="70%"><div align="left">Indicar el tipo de personal correspondiente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Sueldo/Salario B&aacute;sico</div></td>
												<td width="70%"><div align="left">Indicar el monto correspondiente a la jubilaci&oacute;n</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">C�digo de N&oacute;mina</div></td>
												<td width="70%"><div align="left">El sistema asigna el C�digo por defecto</div></td>
											</tr>
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Horas</div></td>
												<td width="70%"><div align="left"></div></td>
											</tr>
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha de Ingreso</div></td>
												<td width="70%"><div align="left">Indicar la fecha de ingreso como personal jubilado o pensionado</div></td>
											</tr>
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha de Punto de Cuenta</div></td>
												<td width="70%"><div align="left">Indicar la fecha del punto de cuenta</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
														
											<tr>
												<td width="30%"><div align="left">Punto de cuenta</div></td>
												<td width="70%"><div align="left">Indicar el n�mero del punto de cuenta</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Observaciones</div></td>
												<td width="70%"><div align="left"></div></td>
											</tr>
											
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de B&uacute;squeda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: .</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
