<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Aumento de Sueldo No Sujeto a LEFP</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/registro/RegistroCargos.jsp" class="linktext">Registro  Cargos</a>
															</td>
														</tr>
														
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/trabajador/Trabajador.jsp" class="linktext">Datos Trabajador</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite el aumento de sueldo a un trabajador. Al realizar el aumento a un trabajador, el sistema realiza las siguientes actualizaciones, a saber:<br>
Actualiza los datos del trabajador<br>
Actualiza la compensaci�n del trabajador<br>
Graba el movimiento en la trayectoria del trabajador<br>
Registra las asignaciones y/o deducciones que le corresponden al trabajador de acuerdo al tipo de personal y cargo<br>
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Tipo de personal del trabajador</div></td>
											</tr>
											
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Cargo</div></td>
												<td width="70%"><div align="left">C�digo de clase y descripci�n del cargo de la posici�n seleccionada</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Grado</div></td>
												<td width="70%"><div align="left">Grado del cargo en la escala o tabulador del cargo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Dependencia</div></td>
												<td width="70%"><div align="left">C�digo y descripci�n de la dependencia administrativa </div></td>
											</tr>
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Sueldo B�sico</div></td>
												<td width="70%"><div align="left">Sueldo b�sico del trabajador de acuerdo a una escala o tabulador</div></td>
											</tr>
<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Aumentar por?</div></td>
												<td width="70%"><div align="left">Indicar si el aumento es:<br>
Porcentaje<br>
Nuevo Paso</div></td>
											</tr>
<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Porcentaje</div></td>
												<td width="70%"><div align="left">Si se indic� por porcentaje, se indica el porcentaje a aumentar</div></td>
											</tr>
<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Nuevo Paso</div></td>
												<td width="70%"><div align="left">Si se indic� nuevo paso, se indica el nuevo paso en la escala</div></td>
											</tr>
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha Vigencia</div></td>
												<td width="70%"><div align="left">Fecha del movimiento. </div></td>
											</tr>
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr><tr>
												<td width="30%"><div align="left">Fecha Punto de Cuenta</div></td>
												<td width="70%"><div align="left">Fecha que firm� el punto de cuenta</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Punto de cuenta</div></td>
												<td width="70%"><div align="left">N�mero de punto de cuenta</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Observaciones</div></td>
												<td width="70%"><div align="left">Observaciones</div></td>
											</tr>
											
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: .</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
