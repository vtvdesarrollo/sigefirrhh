<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Reingreso de Personal Sin Registro</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de B&uacute;squeda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/cargo/Cargo.jsp" class="linktext">Cargos</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/estructura/Dependencia.jsp" class="linktext">Dependencias</a>
															</td>
														</tr>											
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/expediente/Personal.jsp" class="linktext">Datos Personales</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite el reingreso de un trabajador a la n�mina del organismo, como tipo de personal que no tiene asociado un registro de cargos. Los datos personales tienen que estar registrados en el expediente. Al ingresar a un trabajador, el sistema realiza las siguientes actualizaciones, a saber:<br>
Registra los datos del trabajador<br>
Asigna los bancos y otros valores de acuerdo al tipo de personal<br>
Graba el movimiento en la trayectoria del trabajador<br>
Registra las asignaciones y/o deducciones que le corresponden al trabajador por el tipo de personal y cargo<br>
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Trabajador</div></td>
												<td width="70%"><div align="left">Indicar la c&eacute;dula del trabajador</div></td>
											</tr>
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Indicar el tipo de personal</div></td>
											</tr>
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>											
											<tr>
												<td width="30%"><div align="left">Movimiento</div></td>
												<td width="70%"><div align="left">Se�alar el tipo de movimiento</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Regi&oacute;n</div></td>
												<td width="70%"><div align="left">Indicar la regi&oacute;n donde esta ubicado el cargo</div></td>
											</tr>
											
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Sede</div></td>
												<td width="70%"><div align="left">Indicar la sede fisica donde el trabajador prestara sus servicios</div></td>
											</tr>
											
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Dependencia</div></td>
												<td width="70%"><div align="left">Indicar la dependencia administrativa, en la que se encuentra el cargo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Clasificador</div></td>
												<td width="70%"><div align="left">Indicar el manual de cargos correspondiente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>											
											<tr>
												<td width="30%"><div align="left">Cargo</div></td>
												<td width="70%"><div align="left">Seleccionar el cargo asignado</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>											
											<tr>
												<td width="30%"><div align="left">Sueldo/Salario B&aacute;sico</div></td>
												<td width="70%"><div align="left">Colocar el Monto correspondiente al sueldo b&aacute;sico</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>											
											<tr>
												<td width="30%"><div align="left">Pagar Retroactivo?</div></td>
												<td width="70%"><div align="left">Indicar si/no le corresponde retroactivo de acuerdo a la fecha de vigencia</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>											
											<tr>
												<td width="30%"><div align="left">C&oacute;digo N&oacute;mina</div></td>
												<td width="70%"><div align="left">El sistema por defecto asigna un c&oacute;digo de n&oacute;mina</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>											
											<tr>
												<td width="30%"><div align="left">Horas</div></td>
												<td width="70%"><div align="left">Indicar el correspondiente n&uacute;mero de horas mensuales</div></td>
											</tr>
		
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>	
											<tr>
												<td width="30%"><div align="left">Fecha de Ingreso</div></td>
												<td width="70%"><div align="left">Indicar la fecha de ingreso</div></td>
											</tr>
		
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>												
											<tr>
												<td width="30%"><div align="left">Fecha Punto de Cuenta</div></td>
												<td width="70%"><div align="left">Indicar la fecha del punto de cuenta</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Punto de cuenta</div></td>
												<td width="70%"><div align="left">Indicar el N�mero de punto de cuenta</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Observaciones</div></td>
												<td width="70%"><div align="left">El analista colocara las observaciones a que hubiese lugar, si lo considere pertinente</div></td>
											</tr>
											
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de B&uacute;squeda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: .</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
