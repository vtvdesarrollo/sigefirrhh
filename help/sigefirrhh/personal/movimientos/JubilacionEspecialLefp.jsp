<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Jubilaci�n Especial sujeto a LEJP</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de B&uacute;squeda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/registro/RegistroCargos.jsp" class="linktext">Registro  Cargos</a>
															</td>
														</tr>
														
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/trabajador/Trabajador.jsp" class="linktext">Datos Trabajador</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="CausaMovimiento.jsp" class="linktext">Causas de Egreso</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Se refiere al movimiento de personal originado por el otorgamiento de la jubilaci&oacute;n en forma discrecional por el ciudadano Presidente de la Rep&uacute;blica a un funcionario que no cumple con los requisitos establecidos en el articulo 3 de la Ley de Estatuto sobre el R&eacute;gimen de Jubilaciones y Pensiones de los funcionarios o empleados de la Administraci&oacute;n P&uacute;blica Nacional de los Estados y de los Municipios.<br>
                                                    Al realizar el movimiento, el sistema realiza las siguientes actualizaciones a saber:<br>
                                                    Actualiza los datos del trabajador, y lo desincorpora de la n&oacute;mina de personal activo.<br>
                                                    Deja vacante la posicion del registro que ocupa el trabajador.<br>
                                                    Graba el movimiento en la trayectoria del trabajador.<br> 
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Sueldo Promedio</div></td>
												<td width="70%"><div align="left">El sistema calcula el sueldo promedio del trabajador basado en los �ltimos 24 salarios devengados.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">A�os APN</div></td>
												<td width="70%"><div align="left">El sistema calcula los a�os de servicio en la Administraci&oacute;n P&uacute;blica Nacional.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Meses APN</div></td>
												<td width="70%"><div align="left">El sistema calcula los meses de servicio en la Administraci&oacute;n P&uacute;blica Nacional.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Dias APN</div></td>
												<td width="70%"><div align="left">El sistema calcula los dias de servicio en la Administraci&oacute;n P&uacute;blica Nacional.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">A�os Organismo</div></td>
												<td width="70%"><div align="left">El sistema calcula los a�os de servicio en el Organismo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Meses Organismo</div></td>
												<td width="70%"><div align="left">El sistema calcula los meses de servicio en el Organismo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">D�as Organismo</div></td>
												<td width="70%"><div align="left">El sistema calcula los d�as de servicio en el Organismo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Total A�os</div></td>
												<td width="70%"><div align="left">El sistema calcula el total de a�os de servicios.</div></td>
											</tr>																																	
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Total Meses</div></td>
												<td width="70%"><div align="left">El sistema calcula el total de meses de servicios.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>																																																			
											<tr>
												<td width="30%"><div align="left">Total D�as</div></td>
												<td width="70%"><div align="left">El sistema calcula el total de d�as de servicios.</div></td>
											</tr>
																																												
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>																																																			
											<tr>
												<td width="30%"><div align="left">Porcentaje Jubilaci&oacute;n</div></td>
												<td width="70%"><div align="left">El sistema calcula el porcentaje de jubilaci&oacute;n que corresponda.</div></td>
											</tr>																																	
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>																																																			
											<tr>
												<td width="30%"><div align="left">Monto Jubilaci&oacute;n</div></td>
												<td width="70%"><div align="left">El sistema calcula el monto mensual por concepto de jubilaci&oacute;n que le corresponda al trabajador.</div></td>
											</tr>																																	
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>																																																			
											<tr>
												<td width="30%"><div align="left">Fecha Vigencia</div></td>
												<td width="70%"><div align="left">El sistema arroja la fecha de vigencia de la jubilaci&oacute;n, que se indico al principio del proceso.</div></td>
											</tr>																																	
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>											
											<tr>
												<td width="30%"><div align="left">Fecha Punto de Cuenta</div></td>
												<td width="70%"><div align="left">Indicar la fecha del punto de cuenta.</div></td>
											</tr>			
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Punto de cuenta</div></td>
												<td width="70%"><div align="left">Indicar el n�mero del punto de cuenta</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Observaciones</div></td>
												<td width="70%"><div align="left"></div></td>
											</tr>
											
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de B&uacute;squeda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: .</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
