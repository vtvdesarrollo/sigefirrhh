<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Remesas</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Indice-->
							<table width="100%" class="toptable">
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de B&uacute;squeda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left">Requisitos</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>No tiene requisitos previos.</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite el registro, actualizaci�n y consulta de las remesas de movimientos de personal elaboradas por los usuarios   												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">A�o</div></td>
												<td width="70%"><div align="left">A�o que corresponde la remesa</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
<tr>
												<td width="30%"><div align="left">N�mero</div></td>
												<td width="70%"><div align="left">N�mero de remesa. Se genera automaticamente.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
<tr>
												<td width="30%"><div align="left">Fecha Creaci�n</div></td>
												<td width="70%"><div align="left">Fecha creaci�n de la remesa. Registra la fecha del d�a</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
<tr>
												<td width="30%"><div align="left">Fecha Env�o</div></td>
												<td width="70%"><div align="left">Fecha que la remesa es enviada al MPD</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
<tr>
												<td width="30%"><div align="left">Fecha Cierre</div></td>
												<td width="70%"><div align="left">Fecha que la remesa se cierra para no permitir que se asocie m&aacute;s movimientos</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
<tr>
												<td width="30%"><div align="left">Estatus</div></td>
												<td width="70%"><div align="left">Estatus de la remesa, a saber:
<br>
														&nbsp;&nbsp;&nbsp;&nbsp;Abierta<br>
														&nbsp;&nbsp;&nbsp;&nbsp;Cerrada<br>
														&nbsp;&nbsp;&nbsp;&nbsp;Enviada</div></td>
											</tr>
											
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Observaciones</div></td>
												<td><div align="left">Observaciones</div></td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de B&uacute;squeda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													Muestra: Descripci�n + C&oacute;digo
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
