<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Traslado Art. 35</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/registro/RegistroCargos.jsp" class="linktext">Registro  Cargos</a>
															</td>
														</tr>
														
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../personal/trabajador/Trabajador.jsp" class="linktext">Datos Trabajador</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite ingresar a un trabajador que renunci� el d�a inmediatamente anterior por el Art. 35 en otro organismo del sector p�blico. Al realizar el movimiento, el sistema realiza las siguientes actualizaciones, a saber:<br>
Actualiza los datos del cargo en el registro<br>
Actualiza los datos del trabajador<br>
Registra los conceptos fijos correspondientes al cargo<br>
Graba el movimiento en la trayectoria del trabajador<br>

												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Tipo de Personal del trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Posiciones Vacantes</div></td>
												<td><div align="left">El Sistema muestra las posiciones vacantes del registro.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Regi&oacute;n</div></td>
												<td><div align="left">Muestra la regi&oacute;n geografica donde esta ubicado el cargo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Sede</div></td>
												<td><div align="left">Muestra la sede fisica donde esta ubicado el cargo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Dependencia</div></td>
												<td><div align="left">C&oacute;digo y descripci&oacute;n de la dependencia administrativa.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Cargo</div></td>
												<td><div align="left">C&oacute;digo de clase y descripci&oacute;n del cargo de la posici&oacute;n seleccionada.</div></td>
											</tr>	
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Grado</div></td>
												<td><div align="left">Grado del cargo en la escala o tabulador del cargo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Sueldo B&aacute;sico</div></td>
												<td><div align="left">Sueldo B&aacute;sico del trabajador de acuerdo a una escala o tabulador.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Fecha Vigencia</div></td>
												<td><div align="left">Fecha del Movimiento. Se registra como fecha de ingreso del trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Fecha Punto de Cuenta</div></td>
												<td><div align="left">Fecha en que se firmo el Punto de Cuenta.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Punto de Cuenta</div></td>
												<td><div align="left">Numero de Punto de Cuenta.</div></td>
											</tr>
												<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Observaciones</div></td>
												<td><div align="left">Observaciones.</div></td>
											</tr>	
											
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: .</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
