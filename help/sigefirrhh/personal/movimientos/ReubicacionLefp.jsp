<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Reingreso Personal a un Cargo de Carrera</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de B&uacute;squeda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Debe esta creada la siguiente tabla para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href=".jsp" class="linktext"></a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite reubicar a un funcionario de carrera afectado por la medida de reducci&oacute;n de personal o removido de un cargo de libre nombramiento;  as&iacute como tamb&iacute;en cuando el organismo se encuentra en reestructuraci&oacute;n o eliminaci&oacute;n, a una posici&oacute;n del registro de cargos que este vacante que corresponda a un cargo de carrera. Los datos personales tienen que estar registrados en el expediente. Al reubicar a un trabajador; el sistema realiza las siguientes actualizaciones, a saber: Registra los datos del trabajador, Asigna los bancos y otros valores de acuerdo al tipo de personal, Ocupa la posici&oacute;n vacante del registro con los datos del trabajador, Graba el movimiento en la trayectoria del trabajador , registra las asignaciones y/o deducciones que le corresponden al trabajador de acuerdo al tipo de personal y cargo.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Tipo de Personal del trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Posiciones Vacantes</div></td>
												<td><div align="left">El Sistema muestra las posiciones vacantes del registro.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Regi&oacute;n</div></td>
												<td><div align="left">Muestra la regi&oacute;n geografica donde esta ubicado el cargo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Sede</div></td>
												<td><div align="left">Muestra la sede fisica donde esta ubicado el cargo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Dependencia</div></td>
												<td><div align="left">C&oacute;digo y descripci&oacute;n de la dependencia administrativa.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Cargo</div></td>
												<td><div align="left">C&oacute;digo de clase y descripci&oacute;n del cargo de la posici&oacute;n seleccionada.</div></td>
											</tr>	
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Grado</div></td>
												<td><div align="left">Grado del cargo en la escala o tabulador del cargo.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Sueldo B&aacute;sico</div></td>
												<td><div align="left">Sueldo B&aacute;sico del trabajador de acuerdo a una escala o tabulador.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Fecha Vigencia</div></td>
												<td><div align="left">Fecha del Movimiento. Se registra como fecha de ingreso del trabajador.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Fecha Punto de Cuenta</div></td>
												<td><div align="left">Fecha en que se firmo el Punto de Cuenta.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Punto de Cuenta</div></td>
												<td><div align="left">Numero de Punto de Cuenta.</div></td>
											</tr>
												<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											
											<tr>
												<td><div align="left">Observaciones</div></td>
												<td><div align="left">Observaciones.</div></td>
											</tr>																																																																																																																						
										</table>										

										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de B&uacute;squeda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: C�dula + Nombre y Apellido.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>