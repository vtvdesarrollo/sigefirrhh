<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Generar Archivos/Disquete de N�mina</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que se utiliza para generar los archivos que se han de enviar al 
													banco con los dep�sitos a realizar a los trabajadores. 
													Previamente se debe haber ejecutado la n�mina y definido la estructura 
													del archivo a trav�s de las opciones de disquetes y detalle de disquetes.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Grupo N�mina</div></td>
												<td width="70%"><div align="left">Se selecciona un grupo de n�mina</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de N�mina</div></td>
												<td><div align="left">Se indica si es una n�mina ordinaria o especial.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">N�mina Especial</div></td>
												<td><div align="left">Si se indic� que es una n�mina especial, se debe seleccionar la n�mina</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Desde</div></td>
												<td><div align="left">El sistema mostrar� el inicio del per�odo a procesar, el cual se indico
															en la definici�n de la n�mina especial.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Hasta</div></td>
												<td><div align="left">El sistema mostrar� el final del per�odo a procesar, el cual se indico
																en la definici�n de la n�mina especial.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Disquete</div></td>
												<td><div align="left">Se selecciona el disquete/archivo a generar. El sistema solo muestra
																los disquetes que fueron definidos con tipo "N".</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Regi�n</div></td>
												<td><div align="left">Se puede seleccionar una o todas las regiones.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Unidad Administradora</div></td>
												<td><div align="left">Se puede seleccionar una o todas las unidades administradoras.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Numero Orden de Pago</div></td>
												<td><div align="left">Se debe indicar el n�mero de orden de pago cuando el archivo lo requiera</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Abono</div></td>
												<td><div align="left">Se debe indicar la fecha de abono cuando el archivo lo requiera, el 
																sistema muestra la fecha del d�a</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Proceso</div></td>
												<td><div align="left">
													Se debe indicar la fecha de proceso cuando el archivo lo requiera, el
													sistema muestra la fecha del dia<br>
													Al presionar el bot�n de "EJECUTAR", el sistema genera un archivo, y
													muestra el siguiente mensaje:<br>
													<br>
													Archivo Generado (Abrir nueva ventana con el bot�n derecho del mouse)<br>
													Al abrir ese archivo, se debe guardar para ser enviado al banco.
												</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>