<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Aumento Por Evaluaci�n</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_procedimiento">Procedimiento</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../trabajador/Trabajador.jsp" class="linktext">Trabajadores</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																Tipos de Resultados de Evaluaciones
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																Evaluaciones de Trabajadores
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																Conceptos a Evaluar
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../personal/trabajador/ConceptoFijo.jsp" class="linktext">Conceptos Fijos</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite al usuario realizar aumentos a los trabajadores de acuerdo a
													los resultados de las evaluaciones de desempe�o de un per�odo. Para ello tomar� 
													como base los resultados registrados de las respectivas evaluaciones, y la 
													parametrizaci�n de los resultados de las mismas.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Se selecciona el tipo de personal</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Mes</div></td>
												<td><div align="left">Mes de evaluaci�n</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">A�o</div></td>
												<td><div align="left">A�o de evaluaci�n</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Criterio Aplicar</div></td>
												<td><div align="left">
														Aumentar por porcentaje:<br>
														<ul>
															El sistema suma los montos de los conceptos a evaluar, determina el 
															porcentaje a aplicar de acuerdo al resultado de la evaluaci�n obtenida,
															y calcula el monto a aumentar.<br>
														</ul>		
														Aumentar por Monto:<br>
														<ul>
															El sistema suma los montos de los conceptos a evaluar, le suma el
															monto indicado de acuerdo al resultado de la evaluaci�n obtenida y
															calcula el monto a aumentar.<br>
														</ul>
														Incrementa el n�mero de pasos:<br>
														<ul>
															El sistema busca en la escala el aumento que le corresponde al 
															incrementar el numero de pasos que se indica de acuerdo al resultado
															de la evaluaci�n obtenida.<br>
														</ul>
														Asignar Monto Unico:<br>
														<ul>
															El sistema asigna un monto �nico, dependiendo del resultado de la
															evaluaci�n obtenida.
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Reflejar Aumento en</div></td>
												<td><div align="left">
														Sueldo B�sico:<br>
														<ul>
															Se suma al sueldo b�sico el monto a aumentar calculado<br>
														</ul>
														Compensaci�n sin escala:<br>
														<ul>
															Se suma a la compensaci�n el monto a aumentar calculado<br>
														</ul>
														Compensaci�n base escala:<br>
														<ul>
															Se suma a la compensaci�n el monto a aumentar calculado, y se realiza un
															ajuste a escala.<br>
														</ul>
														Otro concepto:<br>
														<ul>
															El monto a aumentar calculado se registra en un nuevo concepto.
														</ul>
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Proceso</div></td>
												<td><div align="left">
														Prueba:<br>
														<ul>
												 			Genera un reporte que muestra los resultados de aplicar el nuevo tabulador 
															o escala, reflejando los nuevos sueldos y compensaciones, sin que se 
															realice alguna actualizaci�n. Ofrece al usuario la posibilidad de 
															proyectar los conceptos asociados al sueldo y/o compensaci�n para que 
															el usuario pueda medir el impacto.
														</ul>
														Definitivo:
														<ul>
															Se actualizan los sueldos, compensaciones y pasos de los trabajadores. 
															Se eliminan los conceptos que se hayan registrado en conceptos a evaluar, 
															con esa indicaci�n.
														</ul>
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Concepto</div></td>
												<td><div align="left">
														Si la opci�n de donde reflejar el aumento indica que es en otro concepto, 
														el usuario debe seleccionar el concepto.
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Registrar en</div></td>
												<td><div align="left">Indicar si registra el concepto como concepto fijo o variable.</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Procedimiento-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_procedimiento">Procedimiento</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="center">
													<img src="/sigefirrhh/help/images/personal/otrosProcesos/ProcesoAumentoPorEvaluacion.gif" border="0">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>