<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Aplicar Nuevo Tabulador</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_procedimiento">Procedimiento</a></li><br>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/TipoPersonal.jsp" class="linktext">Tipo de Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../trabajador/Trabajador.jsp" class="linktext">Trabajadores</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/cargo/Tabulador.jsp" class="linktext">Tabulador</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																Conceptos a Evaluar
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../personal/trabajador/ConceptoFijo.jsp" class="linktext">Conceptos Fijos</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite al usuario aplicar un nuevo tabulador o escala de sueldos a un 
													determinado tipo de personal. El proceso se puede realizar en modo prueba o 
													definitivo. El sistema habr� de evaluar los conceptos que se hayan registrado en 
													la opci�n de conceptos a evaluar.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Se selecciona el tipo de personal a procesar</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tabulador Actual</div></td>
												<td><div align="left">Se selecciona el tabulador o escala actual que se est� aplicando</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tabulador Nuevo</div></td>
												<td><div align="left">Se selecciona el nuevo tabulador o escala que se desea aplicar </div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Condici�n</div></td>
												<td><div align="left">
														Este campo es requerido al procesar a empleados.<br>
														Se ajusta la Compensaci�n?
														<ul>
															Una vez que se aplica la nueva escala o tabulador, el sistema ajusta
															la compensaci�n del trabajador a la nueva escala, partiendo de la 
															compensaci�n que ya posee.
														</ul>
														Se mantiene el paso?
														<ul>
															Una vez que se aplica la nueva escala o tabulador, el sistema calcula la 
															nueva compensaci�n, de acuerdo al paso que ya posee el trabajador.
														</ul>
														Se ajusta la diferencia?
														<ul>
															Una vez que se aplica la nueva escala o tabulador, el sistema ajusta la 
															diferencia que hay entre el total de remuneraciones anteriores y las nuevs.
														</ul>
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Se mantiene el paso?</div></td>
												<td><div align="left">
													Este campo es requerido al procesar a obreros.<br>
													Si. El proceso mantiene el paso del obrero, y busca el salario diario
													    en el tabulador de acuerdo al paso.<br>
													<br>
													No. El proceso asigna el salario que indica el paso 1 del tabulador.
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Proceso</div></td>
												<td><div align="left">
														Prueba:<br>
														<ul>
												 			Genera un reporte que muestra los resultados de aplicar el nuevo tabulador 
															o escala, reflejando los nuevos sueldos y compensaciones, sin que se 
															realice alguna actualizaci�n. Ofrece al usuario la posibilidad de 
															proyectar los conceptos asociados al sueldo y/o compensaci�n para que 
															el usuario pueda medir el impacto.
														</ul>	
														Definitivo:<br>
														<ul>
															Se actualizan los sueldos, compensaciones y pasos de los trabajadores. 
															Se eliminan los conceptos que se hayan registrado en conceptos a evaluar, 
															con esa indicaci�n.
														</ul>
												</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								
								<!--Procedimiento-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_procedimiento">Procedimiento</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="center">
													<img src="/sigefirrhh/help/images/personal/otrosProcesos/ProcesoAumentoPorTabulador.gif" border="0">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>