<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Pagos Fuera del Sistema</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/ConceptoTipoPersonal.jsp" class="linktext">Concepto Tipo Personal</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/FrecuenciaTipoPersonal.jsp" class="linktext">Frecuencia Tipo Personal</a>
															</td>
														</tr>
														
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite el registro y actualizaci�n de las asignaciones y/o deducciones del trabajador que no fueron aplicadas a trav�s de opciones o procesos del sistema, por ejemplo: el registro del monto percibido por concepto de comisi�n de servicio en otro organismo. Estos conceptos ser�n registrados en los historicos de quincenas y/o semanas, con la frecuencia '999' que debe estar registrada en las frecuencias por cada tipo de personal.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Concepto</div></td>
												<td width="70%"><div align="left">Concepto (Asignaci�n / Deducci�n) que se registrar� en el hist�rico del trabajador.</div></td>
											</tr>
											
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">A�o</div></td>
												<td width="70%">
													<div align="left">
														A�o a registrar.
													</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Mes</div></td>
												<td width="70%">
													<div align="left">
														Mes a registrar.
													</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Semana/Quincena</div></td>
												<td width="70%">
													<div align="left">
														Semana o quincena a registrar. En el caso de semana, debe indicarse la semana del a�o.
													</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Fecha</div></td>
												<td width="70%">
													<div align="left">
														Fecha registro
													</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Unidades</div></td>
												<td width="70%">
													<div align="left">
														Unidades a registrar.
													</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Asignaci�n o Deducci�n</div></td>
												<td width="70%"><div align="left">Monto que introduce el usuario</div></td>
											</tr>
											
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: C�digo y descripcion del concepto,monto y frecuencia de pago.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
