<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Calculo Bono Fin de A�o</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/ConceptoUtilidades.jsp" class="linktext">F�rmula Bono Fin de A�o (Conceptos Bono Fin A�o - Utilidades)</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/ParametrosVarios.jsp" class="linktext">Par�metros Varios</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/definiciones/UtilidadesPorAnio.jsp" class="linktext">D�as Bono Fin de A�o por A�o</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" class="datatable">
											<tr>
												<td>
													Proceso que permite registrar el concepto de pago por Bono de Fin de A�o a los trabajadores. El c&aacute;lculo lo realiza  bas&aacute;ndose en los criterios 
													fijados en los par&aacute;metros de F&oacute;rmula del Bono Fin de A�o, ya descritos en la 
													secci&oacute;n de Par&aacute;metros y F&oacute;rmulas de N&oacute;mina. Si se indic&oacute; 
													que debe tomarse la alicuota correspondiente al bono vacacional, el sistema har&aacute; 
													los c&aacute;lculos correspondientes, buscando en los hist�ricos el monto pagado por bono vacacional, y proyect�ndolo a aquellos trabajadores que a�n no lo hayan percibido. Si se indic&oacute; que debe tomarse la alicuota correspondiente al bono petrolero, el sistema har&aacute; los c&aacute;lculos correspondientes. Todos estos conceptos deben estar registrados en los conceptos por tipo de personal.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci&oacute;n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">El usuario debe seleccionar el tipo de personal que se va a procesar.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Desde-Hasta</div></td>
												<td><div align="left">El usuario debe indicar el per�odo que corresponde del bono a pagar.</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Proceso</div></td>
												<td>
													<div align="left">
														El sistema permite realizar el proceso en modo  prueba o modo definitivo.<br>
														El modo prueba no realiza actualizaci&oacute;n alguna.<br>
														El modo definitivo, registra el concepto correspondiente en los conceptos variables del 
														trabajador, aplicando la frecuencia y formulaci&oacute;n indicada en la definici&oacute;n 
														del concepto en Conceptos por Tipo de Personal, y utilizando los criterios indicados en 
														par&aacute;metros y f&oacute;rmula, de acuerdo a los a&ntilde;os de servicio que cumple el 
														trabajador.<br>
														
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Reporte</div></td>
												<td>
													<div align="left">
														Permite generar los siguientes reportes:<br>
														<br>
														&nbsp;&nbsp;&nbsp;Bono Fin de A�o<br>
														&nbsp;&nbsp;&nbsp;Bono Vacacional por UEL<br>
														
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: </td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
