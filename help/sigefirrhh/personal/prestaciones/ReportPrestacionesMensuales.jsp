<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Reporte Prestaciones Mensuales</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n para generar el reporte de las prestaciones mensuales.
													Incluye los montos por concepto del abono de los cinco(5) d�as y los
													montos por concepto de los d�as adicionales.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Seleccionar un tipo de personal</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Regi�n</div></td>
												<td><div align="left">Seleccionar una o todas las regiones</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">A�o</div></td>
												<td><div align="left">Indicar a�o</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Mes</div></td>
												<td><div align="left">Indicar mes</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Reporte</div></td>
												<td><div align="left">
													Permite generar el reporte, mostrando la informaci�n, a saber:<br>
													<ul>
														<li>General		- mostrando totales por trabajador</li>
														<li>Detallada	- mostrando el detalle del c�lculo</li>
														<li>Resum�n		- mostrando un resumen por tipo de personal</li>
													</ul>
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Ordenado por</div></td>
												<td><div align="left">
													<ul>
														<li>Alfab�tico</li>
														<li>C�dula</li>
														<li>C�digo de n�mina</li>
													</ul>
												</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primera Firma</div></td>
												<td><div align="left">Seleccionar entre las firmas la primera firma</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Segunda Firma</div></td>
												<td><div align="left">Seleccionar entre las firmas la segunda firma</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tercera Firma</div></td>
												<td><div align="left">Seleccionar entre las firmas la tercera firma</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Formato</div></td>
												<td><div align="left">Seleccionar si se desea el reporte en formato .pdf o hoja de c�lculo</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>