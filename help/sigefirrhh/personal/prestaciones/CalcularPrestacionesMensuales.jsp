<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Proceso Mensual Prestaciones</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_procedimiento">Procedimiento</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n para realizar el c�lculo mensual de los cinco (5) d�as a abonar por
													concepto de prestaciones, y el c�lculo del monto a pagar por d�as adicionales.
													El proceso debe ejecutarse luego de haber procesado todos los pagos 
													correspondientes al per�odo, ya sea n�minas ordinarias o n�minas especiales.
													El sistema toma los conceptos definidos para el c�lculo de prestaciones que 
													fueron aplicados en el per�odo indicado, ya sea en los conceptos fijos o en los
													hist�ricos, seg�n se haya definido, y calcula la base de c�lculo para determinar
													el monto a abonar correspondeinte a cinco(5) d�as. Se seleccionan los trabajadores,
													activos o de permiso no remunerado, a partir del 4to. mes de su ingreso. 
													En el caso de que el trabajador est� de aniversario y se encuentra cumpliendo m�s
													de un a�o de servicios, el sistema calcula los d�as adicionales que le corresponde,
													usando como base de c�lculo, el promedio de las prestaciones calculadas en los doce
													meses anteriores.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Tipo de Personal</div></td>
												<td width="70%"><div align="left">Seleccionar un tipo de personal</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Mes</div></td>
												<td><div align="left">Indicar mes</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">A�o</div></td>
												<td><div align="left">Indicar a�o</div></td>
											</tr>
											<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Proceso</div></td>
												<td><div align="left">Seleccionar modo "prueba" o "definitivo". Una vez que se ejecute en modo
															"definitivo", se actualiza el �ltimo mes procesado.</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Procedimiento-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_procedimiento">Procedimiento</a></td>
											</tr>
										</table>
										
										<table width="100%" border="0" cellpadding="3" cellspacing="1" class="datatable">
											<tr>
												<td align="center">
													<img src="/sigefirrhh/help/images/personal/prestaciones/CalcularPrestacionesMensuales.gif" border="0">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>