<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center">
				<jsp:include flush="true" page="/help/include/top.jsp" />
			</td>
		</tr>
	</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Consultar/Eliminar Posici�n Registro</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/registro/Registro.jsp" class="linktext">
																	Registros de Cargos
																</a>
															</td>
														</tr>
														
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci�n que permite consultar/eliminar los datos asociados a una posici�n de un registro de cargos y/o puestos de trabajo.
													S�lo se podr� eliminar posiciones vacantes,  siempre que estas no hayan estado ocupadas previamente por alg�n trabajador.
												</td>
											</tr>
										</table>
										
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
																						<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Registro</div></td>
												<td><div align="left">Registro de Cargos a consultar</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo n�mina</div></td>
												<td><div align="left">C�digo de n�mina o posici�n del registro que se desea consultar</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Situaci�n</div></td>
												<td><div align="left">Situaci�n de la posici�n, a saber:<br>
Ocupado<br>
Vacante<br></div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Manual y Clase de Cargo</div></td>
												<td><div align="left">Manual, c�digo de clase y descripci�n del cargo que corresponde a esa posici�n</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grado</div></td>
												<td><div align="left">Grado del cargo</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Dependencia</div></td>
												<td><div align="left">Dependencia donde se encuentra adscrito esa posici�n del registro</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sede</div></td>
												<td><div align="left">Sede donde se encuentra adscrito esa posici�n del registro</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Horas</div></td>
												<td><div align="left">Horas de trabajo</div></td>
											</tr>
<tr>
												<td colspan="2" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Reporta a</div></td>
												<td><div align="left">Indicar a que posici�n dentro del registro le reporta la posici�n que se est� consultando</div></td>
											</tr>

										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">Muestra: Tipos de personal</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
