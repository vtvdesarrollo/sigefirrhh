<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Registro de Oferentes: Antecedentes de Servicios</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n 
													de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Elegible.jsp" class="linktext">Datos Personales</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n 
													de los antecedentes de servicios de un oferente, durante su desempe&ntilde;o 
													en la Administraci&oacute;n P&uacute;blica en todos aquellos organismos que 
													se rigen bajo LEFP.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Oferente</div></td>
												<td width="70%"><div align="left">Apellidos y Nombres del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Organismo</div></td>
												<td><div align="left">Organismo donde labor� el oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Personal al Ingreso</div></td>
												<td><div align="left">Tipo de Personal del oferente al momento que ingreso al organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Ingreso</div></td>
												<td><div align="left">Fecha de ingreso al organismo indicado</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Manual,C�digo (clase) y descripci�n de Cargo al ingreso</div></td>
												<td><div align="left">Manual, c�digo (clase) y descripci�n de cargo al ingresar, de acuerdo a c�digos suministrados por MPD</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo de n�mina al ingreso</div></td>
												<td><div align="left">C�digo de n�mina al ingresar</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sueldo B�sico Ingreso</div></td>
												<td><div align="left">Sueldo b�sico al ingreso, de acuerdo a tabuladores suministrados por MPD</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Compensaci�n Ingreso</div></td>
												<td><div align="left">Compensaci�n al ingreso, de acuerdo a tabuladores suministrados por MPD</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primas del Cargo Ingreso</div></td>
												<td><div align="left">Primas propias del cargo para el momento del ingreso del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primas del oferente Ingreso</div></td>
												<td><div align="left">Primas propias del oferente para el momento del ingreso</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Personal al Egreso</div></td>
												<td><div align="left">Tipo de Personal del oferente al momento que egres� del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha Egreso</div></td>
												<td><div align="left">Fecha de egreso al organismo indicado</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Manual,C�digo (clase) y descripci�n de Cargo al egreso</div></td>
												<td><div align="left">Manual, c�digo (clase) y descripci�n de cargo al egresar, de acuerdo a c�digos suministrados por MPD</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">C�digo de n�mina al egreso</div></td>
												<td><div align="left">C�digo de n�mina al egresar</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sueldo B�sico Egreso</div></td>
												<td><div align="left">Sueldo b�sico al egreso, de acuerdo a tabuladores suministrados por MPD</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Compensaci�n Egreso</div></td>
												<td><div align="left">Compensaci�n al egreso, de acuerdo a tabuladores suministrados por MPD</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primas del Cargo Egreso</div></td>
												<td><div align="left">Primas propias del cargo para el momento del egreso del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primas del oferente Egreso</div></td>
												<td><div align="left">Primas propias del oferente para el momento del egreso</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Causa de Retiro</div></td>
												<td><div align="left">Causa de retiro del organismo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Prestaciones pendientes ?</div></td>
												<td><div align="left">Indicador si el oferente egres� y qued� pendiente el pago de prestaciones</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Vacaciones pendientes ?</div></td>
												<td><div align="left">Indicador si el oferente egres� y qued� pendiente el pago y/o disfrute de vacaciones</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">D�as de vacaciones pendientes</div></td>
												<td><div align="left">D�as de vacaciones pendientes</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Observaciones</div></td>
												<td><div align="left">Observaciones generales</div></td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: Nombre Instituci�n + Fecha Ingreso + Fecha Egreso
											  </td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
