<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Registro de Oferentes: Datos Personales</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<p>Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:</p>
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/ubicacion/Ciudad.jsp" class="linktext">Ciudad</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/ubicacion/Parroquia.jsp" class="linktext">Parroquia</a>
															</td>
														</tr>
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="../../base/bienestar/EstablecimientoSalud.jsp" class="linktext">Establecimiento Salud </a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de 
													los datos personales del oferente.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">C�dula</div></td>
												<td width="70%"><div align="left">N�mero de c�dula de identidad. Este campo no puede ser modificado</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primer Apellido</div></td>
												<td><div align="left">Primer apellido del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Segundo Apellido</div></td>
												<td><div align="left">Segundo apellido del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Primer Nombre</div></td>
												<td><div align="left">Primer nombre del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Segundo Nombre</div></td>
												<td><div align="left">Segundo nombre del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Sexo</div></td>
												<td><div align="left">Sexo del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Estado Civil</div></td>
												<td><div align="left">Estado civil del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha de Nacimiento</div></td>
												<td><div align="left">Fecha de Nacimiento del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nacionalidad</div></td>
												<td><div align="left">Nacionalidad del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Pais, estado y ciudad de Nacimiento</div></td>
												<td><div align="left">Pa�s, estado y ciudad de nacimiento del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Doble nacionalidad ?</div></td>
												<td><div align="left">Indicar si el oferente tiene otra nacionalidad</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Otra nacionalidad</div></td>
												<td><div align="left">Si la respuesta anterior es afirmativa, indicar el pais de la segunda nacionalidad</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nacionalizado ?</div></td>
												<td><div align="left">Indicar si el oferente es nacionalizado</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Fecha de nacionalizaci�n</div></td>
												<td><div align="left">Si es nacionalizado, indicar la fecha en que se nacionaliz�</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Gaceta</div></td>
												<td><div align="left">Si es nacionalizado, indicar n�mero de gaceta donde se public�</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Otra Normativa</div></td>
												<td><div align="left">Si es nacionalizado, indicar otra normativa por la que se haya nacionalizado diferente a gaceta</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Direcci�n de residencia</div></td>
												<td><div align="left">Direcci�n de residencia del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Zona Postal</div></td>
												<td><div align="left">Zona Postal</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Pa�s, estado, ciudad, municipio y parroquia de residencia</div></td>
												<td><div align="left">Pa�s, estado, ciudad, municipio y parroquia de residencia del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tel�fonos</div></td>
												<td><div align="left">Tel�fono de residencia, tel�fono celular y tel�fono de oficina del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">email</div></td>
												<td><div align="left">Direcci�n electr�nica del oferente</div></td>
											</tr>  
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left"></div></td>
												<td><div align="left">A�os de Servicio en APN antes de Ingreso en Organismo.</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tipo de Vivienda</div></td>
												<td><div align="left">Tipo de vivienda del oferente, a saber: casa, apartamento, habitaci�n u hotel</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tenencia de Vivienda</div></td>
												<td><div align="left">Tenencia de vivienda del oferente, a saber: alquilada, propia, pagando o de un familiar</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Maneja ?</div></td>
												<td><div align="left">Indicar si el oferente conduce</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grado de licencia</div></td>
												<td><div align="left">Indicar el grado de licencia de conducir del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Tiene veh�culo ?</div></td>
												<td><div align="left">Indicar si el oferente tiene veh�culo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Marca</div></td>
												<td><div align="left">Si la respuesta anterior fu� afirmativa, debe indicar la marca del veh�culo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Modelo</div></td>
												<td><div align="left">Si la respuesta anterior fu� afirmativa, debe indicar el modelo del veh�culo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Placa</div></td>
												<td><div align="left">Si la respuesta anterior fu� afirmativa, debe indicar la placa del veh�culo</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nro. SSO</div></td>
												<td><div align="left">N�mero del Seguro Social Obligatorio</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nro. RIF</div></td>
												<td><div align="left">N�mero del RIF</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Nro. Libreta Militar</div></td>
												<td><div align="left">N�mero de libreta militar</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Peso</div></td>
												<td><div align="left">Peso del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Estatura</div></td>
												<td><div align="left">Estatura del oferente</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Diestralidad</div></td>
												<td><div align="left">Diestralidad</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td><div align="left">Grupo Sanguineo</div></td>
												<td><div align="left">Grupo sanguineo</div></td>
											</tr>
											<tr>
												<td><div align="left">Cargo que Aspira</div></td>
												<td><div align="left"></div></td>
											</tr>
											<tr>
												<td><div align="left">Sueldo que Aspira</div></td>
												<td><div align="left"></div></td>
											</tr>
											<tr>
												<td><div align="left">Disponibilidad </div></td>
												<td><div align="left"></div></td>
											</tr>
											<tr>
												<td><div align="left">�Trabaja Actualmente? </div></td>
												<td><div align="left"></div></td>
											</tr>
											<tr>
												<td><div align="left">Donde trabaja o trabaj� la ultima vez</div></td>
												<td><div align="left"></div></td>
											</tr>		
											<tr>
												<td><div align="left">Motivo Retiro</div></td>
												<td><div align="left">Indique el motivo de retiro</div><td>       								
											</tr>
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: Primer Apellido + Segundo Apellido + Primer Nombre + Segundo Nombre + Cedula.</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
