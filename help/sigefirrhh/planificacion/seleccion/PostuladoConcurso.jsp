<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Postulados a Cargos por Concursos</strong>
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Menu-->
							<table width="100%" class="toptable">
								
								<!--Indice-->
								<tr>
									<td>
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													<li><a class="Link" href="#_requisitos">Requisitos Previos</a></li><br>
													<li><a class="Link" href="#_descripcion">Descripci&oacute;n</a></li><br>
													<li><a class="Link" href="#_resultado">Resultados de Busqueda</a></li><br>
											  	</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Requisitos-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_requisitos">Requisitos</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td align="left">
													
													Deben estar creadas las siguientes tablas para la ejecuci&oacute;n de esta opci&oacute;n:
													<table width="30%" align="center">
														<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Concurso.jsp" class="linktext">Concursos</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Concurso.jsp" class="linktext">Cargos por Concursos</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Concurso.jsp" class="linktext">Datos Personales de Trabajadores</a>
															</td>
														</tr>
<tr>
															<td width="50%"class="datatableborder"  align="center">
																<a href="Concurso.jsp" class="linktext">Datos Personales de Registro de Oferentes</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
								<!--Descripccion-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
												<td>
													Opci&oacute;n que permite el registro, consulta y/o actualizaci&oacute;n de las personas que se postulan para ocupar cargos  asociados a un concurso
													.
												</td>
											</tr>
										</table>
										<!--------------------------------------------------------------------->
										<table class="bandtable" width="95%" align="center">
											<tr>
												<td width="30%"><div align="left">Campo</div></td>
												<td width="70%"><div align="left">Descripci�n</div></td>
											</tr>
										</table>
										<table width="95%" class="datatableborder" align="center">
											<tr>
												<td width="30%"><div align="left">Concurso</div></td>
												<td width="70%"><div align="left">Selecciona un concurso</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">Cargo</div></td>
												<td width="70%"><div align="left">Seleccionar el cargo al que se quiere postular</div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											<tr>
												<td width="30%"><div align="left">C�dula</div></td>
												<td width="70%"><div align="left">Indique la c�dula de la persona a postularse. El sistema muestra los apellidos y nombres del postulante. </div></td>
											</tr>
											<tr>
												<td colspan="6" class="bgtable2"></td>
											</tr>
											
										</table>
										<!--------------------------------------------------------------------->
									
									</td>
								</tr>
								<!----------->
								
								<!--Busqueda-->
								<tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td align="left"><a  name="_resultado">Resultados de Busqueda</a></td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
											  <td align="left">
													Muestra: .</td>
											</tr>
										</table>
									</td>
								</tr>
								<!----------->
								
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>
