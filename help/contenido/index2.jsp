<!--****************************************************************************************************
	Estructura Organizativa
	**************************************************************************************************** -->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Estructura Organizativa</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
					    <li><!--a href="../sigefirrhh/EEEE/.jsp" class="contenidolink" -->Regiones</a><br>
					    <li><!--a href="../sigefirrhh/EEEE/.jsp" class="contenidolink" -->Lugares de Pago</a><br>
						<li><!--a href="../sigefirrhh/EEEE/AdministradoraUel.jsp" class="contenidolink" -->Relaci�n UADM-UEL</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	

<!--****************************************************************************************************
	Estructura Organizativa Judicial
	**************************************************************************************************** -->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Estructura Organizativa Judicial</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/base/EEEE/Operacion.jsp" class="contenidolink"> -->Tipo de Operaci�n</a><br>
						<li><!-- <a href="../sigefirrhh/base/EEEE/Clasificacion.jsp" class="contenidolink"> -->Clasificaci�n</a><br>
						<li><!-- <a href="../sigefirrhh/base/EEEE/Materia.jsp" class="contenidolink"> -->Materia Judicial</a><br>
						<li><!-- <a href="../sigefirrhh/base/EEEE/Instancia.jsp" class="contenidolink"> -->Instancia</a><br>
						<li><!-- <a href="../sigefirrhh/base/EEEE/DependenciaJudicial.jsp" class="contenidolink"> -->Dependencias Judiciales</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Clases de Cargos y Tabuladores
	**************************************************************************************************** -->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Clases de Cargos y Tabuladores</td>
		</tr>
	</table>
	
<!--****************************************************************************************************
	Clases de Cargos y Tabuladores (Docente)
	**************************************************************************************************** -->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Clases de Cargos y Tabuladores (Docente)</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!--a href="../sigefirrhh/EEEE/GradoDocente.jsp" class="contenidolink" -->Grados</a><br>
						<li><!--a href="../sigefirrhh/EEEE/JerarquiaDocente.jsp" class="contenidolink" -->Jerarquias</a><br>
						<li><!--a href="../sigefirrhh/EEEE/NivelDocente.jsp" class="contenidolink" -->Niveles</a><br>
						<li><!--a href="../sigefirrhh/EEEE/CategoriaDocente.jsp" class="contenidolink" -->Categorias</a><br>
						<li><!--a href="../sigefirrhh/EEEE/TurnoDocente.jsp" class="contenidolink" -->Turnos</a><br>
						<li><!--a href="../sigefirrhh/EEEE/DedicacionDocente.jsp" class="contenidolink" -->Dedicacion</a><br>
						<li><!--a href="../sigefirrhh/EEEE/Tabulador.jsp" class="contenidolink" -->Tabulador</a><br>
						<li><!--a href="../sigefirrhh/EEEE/DetalleTabuladorMed.jsp" class="contenidolink" -->Detalle Tabulador</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
<!--****************************************************************************************************
	Programaci�n Presupuestaria
	**************************************************************************************************** -->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Programaci�n Presupuestaria</td>
		</tr>
	</table>
	
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Proyectos</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Acciones Centralizadas</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Acciones Especificas/Proyectos</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Acciones Especificas/Acciones</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Area Proyectos</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Clasificaci�n Sectorial</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Unidades Medida</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>

	
<!--****************************************************************************************************
	Selecci�n y Reclutamiento
	**************************************************************************************************** -->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Selecci�n y Reclutamiento</td>
		</tr>
	</table>
	
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!--a href="../sigefirrhh/EEEE/PostuladoConcurso.jsp" class="contenidolink"-->Postulados a Concursos</a><br>
						<li><!--a href="../sigefirrhh/EEEE/PostuladoExterno.jsp" class="contenidolink"-->Registro Externo de Postulados</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
<!--****************************************************************************************************
	Capacitacion
	**************************************************************************************************** -->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Capacitaci�n</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		
		<tr>
			<td align="left" width="50%">
				<p><!---->
					<li>Planificaci�n<b>
				</p><!---->
			</td>
			<td width="50%">
				<div align="left">
					<p><!---->
						<li><!--a href="../sigefirrhh/EEEEEE/PlanAdiestramiento.jsp" class="contenidolink"-->Planes por Unidad Funcional</a><br>
						<li><!--a href="../sigefirrhh/EEEEEE/Participante.jsp" class="contenidolink"-->Plan Adiestramiento por Trabajador</a><br>
						<li><!--a href="../sigefirrhh/EEEEEE/ActualizarParticipante.jsp" class="contenidolink"-->Actualizar Participante</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Evaluaciones de Desempe�o
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Evaluaciones de Desempe�o</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td colspan="2">
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEEEE/ParametroEvaluacion.jsp" class="contenidolink"> -->Par�metro Evaluaci�n</a><br>
						<li><!-- <a href="../sigefirrhh/EEEEEE/ObjetivoDepartamental.jsp" class="contenidolink"> -->Objetivos por Unidad Funcional</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
		
		<tr>
			<td align="left" width="50%">
				<p><!---->
					<li>Objetivos/Compet. Trabajador<b>
				</p><!---->
			</td>
			<td width="50%">
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEEEE/ObjetivoFuncionalEmp.jsp" class="contenidolink"> -->Objetivos Unidad Funcional</a><br>
						<li><!-- <a href="../sigefirrhh/EEEEEE/ObjetivoIndividualEmp.jsp" class="contenidolink"> -->Objetivos Individuales</a><br>
						<li><!-- <a href="../sigefirrhh/EEEEEE/CompetenciaEmpleado.jsp" class="contenidolink"> -->Competencias</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEEEE/AccionEvaluacion.jsp" class="contenidolink"> -->Tipos de Acciones Resultantes</a><br>
						<li><!-- <a href="../sigefirrhh/EEEEEE/ResultadoEvaluacion.jsp" class="contenidolink"> -->Tipos de Resultados de Evaluaci�n</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Planes de Personal
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Planes de Personal</td>
		</tr>
	</table>
	
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!--a href="../sigefirrhh/EEEE/Subsistema.jsp" class="contenidolink"-->Subsistemas</a><br>
						<li><!--a href="../sigefirrhh/EEEE/PlanPersonal.jsp" class="contenidolink"-->Planes</a><br>
						<li><!--a href="../sigefirrhh/EEEE/ProyectoPersonal.jsp" class="contenidolink"-->Proyectos por Plan</a><br>
						<li><!--a href="../sigefirrhh/EEEE/AccionObjetivo.jsp" class="contenidolink"-->Acciones por Objetivo</a><br>
						<li><!--a href="../sigefirrhh/EEEE/Actividad.jsp" class="contenidolink"-->Actividades</a><br>
						<li><!--a href="../sigefirrhh/EEEE/Meta.jsp" class="contenidolink"-->Metas</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Tablas Basicas Expediente
	****************************************************************************************************-->		
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Tablas Basicas Expediente</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/base/personal/GrupoProfesion.jsp" class="contenidolink"> -->Grupo de Profesiones</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	ADMINISTRACION DE PERSONAL
	****************************************************************************************************-->
	
	<table width="100%" class="bandtable">
		<tr>
			<td align="left"><span class="style4">ADMINISTRACION DE PERSONAL</span></td>
		</tr>
	</table>
	<p></p>
	
	
<!--****************************************************************************************************
	Definiciones
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Parametrizacion</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		
		<tr>
			<td>
				<p><!---->
					<li>Parametros y F�rmulas
				</p><!---->
			</td>
			<td>
				<div align="left">
					<p><!---->
						<li><!--a href="../sigefirrhh/base/EEEEEE/VacacionesPorAnio.jsp" class="contenidolink"-->Vacaciones por A�os</a><br>
						<li><!--a href="../sigefirrhh/base/EEEEEE/UtilidadesPorAnio.jsp" class="contenidolink"-->D�as Bono Fin de A�o</a><br>
						<!--
						<li><a href="../sigefirrhh/base/definiciones/ParametroAri.jsp" class="contenidolink">Par�metros ARI</a><br>
						-->
					</p><!---->
				</div>
			</td>
		</tr>
	</table>

<!--****************************************************************************************************
	Datos del Trabajador
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Datos del Trabajador</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td align="left" width="30%">
				<p><!---->
					<li>Trabajador<b>
				</p><!---->
			</td>
			<td width="70%">
				<div align="left">
					<p><!---->
						<li><!-- a href="../sigefirrhh/personal/trabajador/UbicarTrabajador.jsp" class="contenidolink" -->UbicarTrabajador</a><br>
						<li><!-- a href="../sigefirrhh/personal/trabajador/ActualizarDatosTrabajador.jsp" class="contenidolink" -->Actualizar Datos</a><br>
						<li><!-- a href="../sigefirrhh/personal/trabajador/ActualizarFechasTrabajador.jsp" class="contenidolink" -->Actualizar Fechas</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div align="left">
					<p><!---->
						<li><!--a href="../sigefirrhh/personal/trabajador/PlanillaAri.jsp" class="contenidolink"-->Planilla ARI</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Movimientos de Personal
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Movimientos de Personal</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		
		<tr>
			<td colspan="2">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
					<tr>
						<td colspan="2" align="left">
							<p><!---->
								<li>Personal Bajo LEFP<b>
							</p><!---->
						</td>
					</tr>
					<tr>
						<td width="30%" align="left">
							<p><!---->
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ingresos
							</p><!---->
						</td>
						<td width="70%">
							<div align="left">
								<p><!---->
									<li><!--a href="../sigefirrhh/EEEEEE/IngresoCargoCarreraLefp.jsp" class="contenidolink"-->Cargo de Carrera</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/IngresoTrabajadorLefp.jsp" class="contenidolink"-->Cargo de Libre Nombramiento</a><br>
								</p><!---->
							</div>
						</td>
					</tr>
					<tr>
						<td align="left">
							<p><!---->
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reingresos
							</p><!---->
						</td>
						<td>
							<div align="left">
								<p><!---->
									<li><!--a href="../sigefirrhh/EEEEEE/ReingresoCargoCarreraLefp.jsp" class="contenidolink"-->Cargo de Carrera</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/ReingresoTrabajadorLefp.jsp" class="contenidolink"-->Cargo de Libre Nombramiento</a><br>
								</p><!---->
							</div>
						</td>
					</tr>
					<tr>
						<td align="left">
							<p><!---->
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Actualizaciones
							</p><!---->
						</td>
						<td>
							<div align="left">
								<p><!---->
									<li><!--a href="../sigefirrhh/EEEEEE/CambioCargoLefp" class="contenidolink"-->Cambio en el cargo</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/DesignacionLefp.jsp" class="contenidolink"-->Designaci�n</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/AscensoLefp.jsp" class="contenidolink"-->Ascenso</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/ClasificacionLefp.jsp" class="contenidolink"-->Clasificaci�n</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/CambioClasificacionLefp.jsp" class="contenidolink"-->Cambio Clasificaci�n</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/AumentoSueldoLefp.jsp" class="contenidolink"-->Aumento de Sueldo</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/LicenciaSinSueldoLefp.jsp" class="contenidolink"-->Licencia sin Sueldo</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/LicenciaConSueldoLefp.jsp" class="contenidolink"-->Licencia con Sueldo</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/SuspensionLefp.jsp" class="contenidolink"-->Suspensi�n</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/ReubicacionLefp.jsp" class="contenidolink"-->Reubicaci�n</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/ReincorporacionLefp.jsp" class="contenidolink"-->Reincorporaci�n</a><br>
									<!--
									<li><!--a href="../sigefirrhh/EEEEEE/ReincorporacionPorSentenciaLefp.jsp" class="contenidolink">Reincorporaci�n por Sentencia</a><br>
									-->
									<li><!--a href="../sigefirrhh/EEEEEE/TrasladoLefp.jsp" class="contenidolink"-->Traslado</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/TrasladoArt35Lefp.jsp" class="contenidolink"-->Traslado Art. 35</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/ModificacionHorarioLefp.jsp" class="contenidolink"-->Modificaci�on de Horario</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/CorreccionMovimientoLefp.jsp" class="contenidolink"-->Correci�n de Movimiento</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/AnulacionMovimientoLefp.jsp" class="contenidolink"-->Anulacion de Movimiento</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/ActualizacionDatosLefp.jsp" class="contenidolink"-->Actualizaci�on Datos Personales</a><br>
								</p><!---->
							</div>
						</td>
					</tr>
					<tr>
						<td align="left">
							<p><!---->
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jubilaciones y<br>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pensiones
							</p><!---->
						</td>
						<td>
							<div align="left">
								<p><!---->
									<li><!--a href="../sigefirrhh/EEEEEE/EEEEEEE.jsp" class="contenidolink"-->Calculo de Jubilaci�n/Pensi�n</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/IngresoTrabajadorJubilado.jsp" class="contenidolink"-->Ingreso a N�mina Jubilado/Pensionado</a><br>
								</p><!---->
							</div>
						</td>
					</tr>
					<tr>
						<td align="left">
							<p><!---->
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Egresos
							</p><!---->
						</td>
						<td>
							<div align="left">
								<p><!---->
									<li><!--a href="../sigefirrhh/EEEEEE/EgresoTrabajadorLefp.jsp" class="contenidolink"-->Egresos</a><br>
								</p><!---->
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
					<tr>
						<td colspan="2" align="left">
							<p><!---->
								<li>Personal no sujeto a LEFP<b>
							</p><!---->
						</td>
					</tr>
					<tr>
						<td width="30%" align="left">
							<p><!---->
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Con Registro
							</p><!---->
						</td>
						<td width="70%">
							<div align="left">
								<p><!---->
									<li><!--a href="../sigefirrhh/EEEEEE/IngresoTrabajadorRegistro.jsp" class="contenidolink"-->Ingresos</a><br>
									<li><!--a href="../sigefirrhh/EEEEEE/ReingresoTrabajadorRegistro.jsp" class="contenidolink"-->Reingresos</a><br>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
										<tr>
											<td width="40%" align="left">
												<li>Actualizaciones<br>
											</td>
											<td width="60%" align="left">
												<li><!--a href="../sigefirrhh/EEEEEE/CambioCargoNoLefp.jsp" class="contenidolink"-->Cambio en el Cargo</a><br>
												<li><!--a href="../sigefirrhh/EEEEEE/AscensoConRegistrop.jsp" class="contenidolink"-->Ascenso</a><br>
												<li><!--a href="../sigefirrhh/EEEEEE/AumentoSueldoConRegistro.jsp" class="contenidolink"-->Aumento de Sueldo</a><br>
												<li><!--a href="../sigefirrhh/EEEEEE/LicenciaSinSueldoConRegistro.jsp" class="contenidolink"-->Licencia sin Sueldo</a><br>
												<li><!--a href="../sigefirrhh/EEEEEE/LicenciaConSueldoConRegistro.jsp" class="contenidolink"-->Licencia con Sueldo</a><br>
												<li><!--a href="../sigefirrhh/EEEEEE/SuspensionConRegistro.jsp" class="contenidolink"-->Suspensi�n</a><br>
												<li><!--a href="../sigefirrhh/EEEEEE/ReincorporacionConRegistro.jsp" class="contenidolink"-->Reincorporacion</a><br>
												<li><!--a href="../sigefirrhh/EEEEEE/TrasladoConRegistro.jsp" class="contenidolink"-->Traslado</a><br>
												<li><!--a href="../sigefirrhh/EEEEEE/ActualizacionDatosConRegistro.jsp" class="contenidolink"-->Actualizaci�n Datos Personales</a><br>
											</td>
										</tr>
									</table>
								</p><!---->
							</div>
						</td>
					</tr>
					<tr>
						<td align="left">
							<p><!---->
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</p><!---->
						</td>
						<td>
							<div align="left">
								<p><!---->
									
								</p><!---->
							</div>
						</td>
					</tr>
					
					
					<tr>
						<td align="left">
							<p><!---->
								<li>Personal no Sujeto a LEFP<b>
							</p><!---->
						</td>
						<td>
							<div align="left">
								<p><!---->
									<li><!-- <a href="../sigefirrhh/EEEEEE/IngresoTrabajadorRegistro.jsp" class="contenidolink"> -->Ingresos con Registro</a><br>
									<li><!-- <a href="../sigefirrhh/EEEEEE/IngresoTrabajadorSinRegistro.jsp" class="contenidolink"> -->Ingresos sin Registro</a><br>
									<li><!-- <a href="../sigefirrhh/EEEEEE/IngresoTrabajadorRegistro.jsp" class="contenidolink"> -->Reingresos</a><br>
									<li><!-- <a href="../sigefirrhh/EEEEEE/IngresoTrabajadorSinRegistro.jsp" class="contenidolink"> -->Actualizaciones</a><br>
									<li><!-- <a href="../sigefirrhh/EEEEEE/JubilacionTrabajadorSinRegistro.jsp" class="contenidolink"> -->Jubilaciones y Pensiones</a><br>
									<li><!-- <a href="../sigefirrhh/EEEEEE/EgresoTrabajadorRegistro.jsp" class="contenidolink"> -->Egresos con Registro</a><br>
									<li><!-- <a href="../sigefirrhh/EEEEEE/EgresoTrabajadorSinRegistro.jsp" class="contenidolink"> -->Egresos sin Registro</a><br>
								</p><!---->
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	
<!--****************************************************************************************************
	Actualizaci�n de Conceptos
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Actualizaci�n de Conceptos</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/CargaConceptos.jsp" class="contenidolink"> -->Carga Externa de Archivo</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/CargaMasivaConceptos.jsp" class="contenidolink"> -->Carga Masiva</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/RecalculoPorCriterio.jsp" class="contenidolink"> -->Actualizar por Criterios</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/CambioFrecuencia.jsp" class="contenidolink"> -->Cambio de Frecuencia</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/CambioEstatus.jsp" class="contenidolink"> -->Cambio de Estatus</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/CambioDocumentoSoporte.jsp" class="contenidolink"> -->Actualizar Documento Soporte</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ActualizarMonto.jsp" class="contenidolink"> -->Actualizar Monto y Unidades</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/EliminarConceptos.jsp" class="contenidolink"> -->Eliminar</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/Recalculo.jsp" class="contenidolink"> -->Rec�lculo de un concepto</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/RecalculoPorAsociado.jsp" class="contenidolink"> -->Rec�lculo de conceptos por variaci�n</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
	
<!--****************************************************************************************************
	Procesos de N�mina
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Procesos de N�mina</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td colspan="2">
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/seguridadOrdinaria.jsp" class="contenidolink"> -->N�minas Ordinarias Procesadas</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/seguridadEspecial.jsp" class="contenidolink"> -->N�minas Especiales Procesadas</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/nominaEspecial.jsp" class="contenidolink"> -->Creaci�n de N�mina Especial</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
		
		<tr>
			<td align="left">
				<p><!---->
					<li>Pren�mina Ordinaria<b>
				</p><!---->
			</td>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEEEE/GenerarPrenomina.jsp" class="contenidolink"> -->Proceso</a><br>
						<li><!-- <a href="../sigefirrhh/EEEEEE/ReportPrenomina.jsp" class="contenidolink"> -->Reportes</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
		
		<tr>
			<td align="left">
				<p><!---->
					<li>N�mina Ordinaria<b>
				</p><!---->
			</td>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/GenerarNomina.jsp" class="contenidolink"> -->Proceso</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportNomina.jsp" class="contenidolink"> -->Reportes</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
		
		<tr>
			<td align="left">
				<p><!---->
					<li>Pren�mina Especial<b>
				</p><!---->
			</td>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/GenerarPrenominaEspecial.jsp" class="contenidolink"> -->Proceso</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportPrenominaEspecial.jsp" class="contenidolink"> -->Reportes</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
		
		<tr>
			<td align="left">
				<p><!---->
					<li>N�mina Especial<b>
				</p><!---->
			</td>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/GenerarNominaEspecial.jsp" class="contenidolink"> -->Proceso</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportNominaEspecial.jsp" class="contenidolink"> -->Reportes</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
		
		<tr>
			<td align="left">
				<p><!---->
					<li>Generar Disquete<b>
				</p><!---->
			</td>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/DisqueteNomina.jsp" class="contenidolink"> -->Generar Disquete</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
<!--****************************************************************************************************
	Aportes Patronales
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Aportes Patronales</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/reportAportes.jsp" class="contenidolink"> -->Proceso</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/generarDisqueteAporte.jsp" class="contenidolink"> -->Reportes</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Registro Cargos/Puestos
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Registro Cargos/Puestos</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/Registro.jsp" class="contenidolink"> -->Definici�n de Registros</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/RegistroPersonal.jsp" class="contenidolink"> -->Registros por Tipo Personal</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/RegistroCargos.jsp" class="contenidolink"> -->Registros Cargos/Puestos</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Prestaciones Sociales Mensuales
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Prestaciones Sociales Mensuales</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/PrestacionesMensuales.jsp" class="contenidolink"> -->Prestaciones por Trabajador</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/GenerarPrestaciones.jsp" class="contenidolink"> -->Proceso Mensual</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/RegistroFideicomiso.jsp" class="contenidolink"> -->Registro Fideicomiso</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>  
	
	
<!--****************************************************************************************************
	Constancias de Trabajo
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Constancias de Trabajo</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/Constancia.jsp" class="contenidolink"> -->Definici�n de Constancias</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/DetalleConstancia.jsp" class="contenidolink"> -->Detalle de Constancias</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/SolicitudConstancia.jsp" class="contenidolink"> -->Solicitud de Constancia</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/GenerarConstancias.jsp" class="contenidolink"> -->Generar Constancias</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportConstancia.jsp" class="contenidolink"> -->Reportes</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Reportes - Trabajadores
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Reportes - Trabajadores</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/ReportTrabajadorTipoPersonal.jsp" class="contenidolink"> -->Por Tipo de Personal</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportTrabajadorSede.jsp" class="contenidolink"> -->Por Sede</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportTrabajadorRegion.jsp" class="contenidolink"> -->Por Regi�n</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportTrabajadorUnidadFuncional.jsp" class="contenidolink"> -->Por Unidad Funcional</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportTrabajadorDependencia.jsp" class="contenidolink"> -->Por Dependencia</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportIngreso.jsp" class="contenidolink"> -->Ingresos por Per�odo</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportEgreso.jsp" class="contenidolink"> -->Egresos por Per�odo</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportFechasRegion.jsp" class="contenidolink"> -->Fechas por Regi�n</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportFechasSede.jsp" class="contenidolink"> -->Fechas por Sede</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportFechasUnidadFuncional.jsp" class="contenidolink"> -->Fechas por Unidad Funcional</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ReportFechasDependencia.jsp" class="contenidolink"> -->Fechas por Dependencia</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	BIENESTAR SOCIAL
	****************************************************************************************************-->
	
	<table width="100%" class="bandtable">
		<tr>
			<td align="left"><span class="style4">BIENESTAR SOCIAL</span></td>
		</tr>
	</table>
	<p></p>
	
	
<!--****************************************************************************************************
	Juguetes
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Juguetes</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/ParametroJuguete.jsp" class="contenidolink"> -->Par�metros</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Utiles Escolares
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Utiles Escolares</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/ParametroUtiles.jsp" class="contenidolink"> -->Par�metros</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Becas a Familiares
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Becas a Familiares</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/NivelBeca.jsp" class="contenidolink"> -->Niveles Educativos</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ParametroBeca.jsp" class="contenidolink"> -->Par�metros</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Guarder�as
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Guarder�as</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/ParametroGuarderia.jsp" class="contenidolink"> -->Par�metros</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/Guarderia.jsp" class="contenidolink"> -->Guarder�as</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Dotaciones
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Dotaciones</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/TipoDotacion.jsp" class="contenidolink"> -->Tipos Dotaci�n</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/SubtipoDotacion.jsp" class="contenidolink"> -->Subtipos Dotaci�n</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/DotacionCargo.jsp" class="contenidolink"> -->Dotaci�n por Cargo</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/ParametroDotacion.jsp" class="contenidolink"> -->Par�metros Dotaci�n</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	P�lizas de Seguro
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">P�lizas de Seguro</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/Poliza.jsp" class="contenidolink"> -->P�liza</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/PlanPoliza.jsp" class="contenidolink"> -->Planes</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/PrimasPlan.jsp" class="contenidolink"> -->Primas Pr Planes</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Tickets
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Tickets</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Proveedores de Tickets</a><br>
						<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Parametros de Tickets</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>
	
	
<!--****************************************************************************************************
	Indicadores de Gesti�n
	****************************************************************************************************-->
	<table width="95%" class="bandtable" align="center">
		<tr>
			<td align="left">Indicadores de Gesti�n</td>
		</tr>
	</table>
	
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
		<tr>
			<td>
				<div align="left">
					<p><!---->
						<li><!-- <a href="../sigefirrhh/EEEE/Trabajadores.jsp" class="contenidolink"> -->Trabajadores por Tipo Personal</a><br>
					</p><!---->
				</div>
			</td>
		</tr>
	</table>