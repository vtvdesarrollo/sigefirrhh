<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	    <style type="text/css">
	
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

		<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td>
				<!--****************************************************************************************************
					Titulo
					**************************************************************************************************** -->
					<table width="100%" class="toptable">
						<tr>
							<td align="left">
								<b>
									Tabla de Contenido
								</b>
							</td>
						</tr>
					</table>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="toptable">
						<tr>
							<td>
							<!--****************************************************************************************************
								Planificaci�n y Desarrollo
								**************************************************************************************************** -->
								<table width="100%" class="bandtable">
									<tr>
										<td align="left"><span class="style4">PLANIFICACION Y DESARROLLO</span></td>
									</tr>
								</table>
								<p></p>
								
								
							<!--****************************************************************************************************
								Estructura Geografica
								**************************************************************************************************** -->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Estructura Geogr&aacute;fica</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/base/ubicacion/Continente.jsp" class="contenidolink">Continentes</a><br>
													<li><a href="../sigefirrhh/base/ubicacion/RegionContinente.jsp" class="contenidolink">Regiones Continentales</a><br>
													<li><a href="../sigefirrhh/base/ubicacion/Pais.jsp" class="contenidolink">Pa�s</a><br>
													<li><a href="../sigefirrhh/base/ubicacion/Estado.jsp" class="contenidolink">Estado</a><br>
													<li><a href="../sigefirrhh/base/ubicacion/Ciudad.jsp" class="contenidolink">Ciudad</a><br>
													<li><a href="../sigefirrhh/base/ubicacion/Municipio.jsp" class="contenidolink">Municipio</a><br>
													<li><a href="../sigefirrhh/base/ubicacion/Parroquia.jsp" class="contenidolink">Parroquia</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Estructura Organizativa
								**************************************************************************************************** -->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Estructura Organizativa</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/base/estructura/Organismo.jsp" class="contenidolink">Organismo</a><br>
													<li><a href="../sigefirrhh/base/estructura/NombreOrganismo.jsp" class="contenidolink">Nombres del Organismo</a><br>
													<li><a href="../sigefirrhh/base/estructura/GrupoOrganismo.jsp" class="contenidolink">Grupos del Organismo</a><br>
												    <li><a href="../sigefirrhh/base/estructura/Region.jsp" class="contenidolink">Regiones</a><br>
												    <li><a href="../sigefirrhh/base/estructura/Sede.jsp" class="contenidolink">Sedes</a><br>
												    <li><a href="../sigefirrhh/base/estructura/LugarPago.jsp" class="contenidolink">Lugares de Pago</a><br>
													<li><a href="../sigefirrhh/base/estructura/UnidadAdministradora.jsp" class="contenidolink">Unidades Administradoras</a><br>
													<li><a href="../sigefirrhh/base/estructura/Estructura.jsp" class="contenidolink">Estructuras</a><br>
													<li><a href="../sigefirrhh/base/estructura/UnidadEjecutora.jsp" class="contenidolink">Unidades Ejecutoras</a><br>
													<li><a href="../sigefirrhh/base/estructura/AdministradoraUel.jsp" class="contenidolink">Relaci�n UADM-UEL</a><br>
													<li><a href="../sigefirrhh/base/estructura/UnidadFuncional.jsp" class="contenidolink">Unidades Funcionales</a><br>
													<li><a href="../sigefirrhh/base/estructura/TipoDependencia.jsp" class="contenidolink">Tipo Dependencia</a><br>
													<li><a href="../sigefirrhh/base/estructura/Dependencia.jsp" class="contenidolink">Dependencias</a><br>
													<li><a href="../sigefirrhh/base/mre/SedeDiplomatica.jsp" class="contenidolink">Sedes Diplom�ticas</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
            
           					<!--****************************************************************************************************
								Estructura Organizativa Judicial
								**************************************************************************************************** -->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Estructura Organizativa Judicial</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/base/EEEE/Operacion.jsp" class="contenidolink"> -->Tipo de Operaci�n</a><br>
													<li><!-- <a href="../sigefirrhh/base/EEEE/Clasificacion.jsp" class="contenidolink"> -->Clasificaci�n</a><br>
													<li><!-- <a href="../sigefirrhh/base/EEEE/Materia.jsp" class="contenidolink"> -->Materia Judicial</a><br>
													<li><!-- <a href="../sigefirrhh/base/EEEE/Instancia.jsp" class="contenidolink"> -->Instancia</a><br>
													<li><!-- <a href="../sigefirrhh/base/EEEE/DependenciaJudicial.jsp" class="contenidolink"> -->Dependencias Judiciales</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
           					<!--****************************************************************************************************
								Estructura Organizativa Docente
								**************************************************************************************************** -->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Estructura Organizativa Docente</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/base/EEEE/CaracteristicaDependencia.jsp" class="contenidolink"> -->Caracter�sticas de Entidades</a><br>
													<li><!-- <a href="../sigefirrhh/base/EEEE/ClasificacionDependencia.jsp" class="contenidolink"> -->Clasificaci�n Entidades Educativas</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Clases de Cargos y Tabuladores
								**************************************************************************************************** -->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Clases de Cargos y Tabuladores</td>
									</tr>
								</table>
								
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/base/cargo/RamoOcupacional.jsp" class="contenidolink">Ramos Ocupacionales</a><br>
													<li><a href="../sigefirrhh/base/cargo/GrupoOcupacional.jsp" class="contenidolink">Grupos Ocupacionales</a><br>
													<li><a href="../sigefirrhh/base/cargo/SerieCargo.jsp" class="contenidolink">Series de Cargos</a><br>
													<li><a href="../sigefirrhh/base/cargo/Tabulador.jsp" class="contenidolink">Tabuladores</a><br>
													<li><a href="../sigefirrhh/base/cargo/DetalleTabulador.jsp" class="contenidolink">Detalle de Tabuladores</a><br>
													<li><a href="../sigefirrhh/base/cargo/ManualCargo.jsp" class="contenidolink">Manuales de Cargos</a><br>
													<li><a href="../sigefirrhh/base/cargo/Cargo.jsp" class="contenidolink">Cargos</a><br>
													<!--
													<li><a href="../sigefirrhh/base/cargo/SerieManual.jsp" class="contenidolink">// Series por Manual</a><br>
													<li><a href="../sigefirrhh/base/cargo/TipoManual.jsp" class="contenidolink">// Tipos de Manuales</a><br>
													<li><a href="../sigefirrhh/base/cargo/ManualOrganismo.jsp" class="contenidolink">// Manuales Vigentes del Organismo</a><br>
													<li><a href="../sigefirrhh/base/mre/DetalleTabuladorMre.jsp" class="contenidolink">Tabulador MRE</a><br>
													<li><a href="../sigefirrhh/base/mre/DetalleTabuladorOnu.jsp" class="contenidolink">Tabulador Onu</a><br>
													-->
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
							<!--****************************************************************************************************
								Clases de Cargos y Tabuladores (Docente)
								**************************************************************************************************** -->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Clases de Cargos y Tabuladores (Docente)</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!--a href="../sigefirrhh/EEEE/GradoDocente.jsp" class="contenidolink" -->Grados</a><br>
													<li><!--a href="../sigefirrhh/EEEE/JerarquiaDocente.jsp" class="contenidolink" -->Jerarquias</a><br>
													<li><!--a href="../sigefirrhh/EEEE/NivelDocente.jsp" class="contenidolink" -->Niveles</a><br>
													<li><!--a href="../sigefirrhh/EEEE/CategoriaDocente.jsp" class="contenidolink" -->Categorias</a><br>
													<li><!--a href="../sigefirrhh/EEEE/TurnoDocente.jsp" class="contenidolink" -->Turnos</a><br>
													<li><!--a href="../sigefirrhh/EEEE/DedicacionDocente.jsp" class="contenidolink" -->Dedicacion</a><br>
													<li><!--a href="../sigefirrhh/EEEE/Tabulador.jsp" class="contenidolink" -->Tabulador</a><br>
													<li><!--a href="../sigefirrhh/EEEE/DetalleTabuladorMed.jsp" class="contenidolink" -->Detalle Tabulador</a><br>
													<li><!--a href="../sigefirrhh/EEEE/CargoDocente.jsp" class="contenidolink" -->Cargos</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
							<!--****************************************************************************************************
								Clasificacion
								**************************************************************************************************** -->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Clasificacion</td>
									</tr>
								</table>
								
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td colspan="2">
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/planificacion/clasificacion/Perfil.jsp" class="contenidolink">Perfil de Cargos</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									
									<tr>
										<td align="left" width="40%">
											<p><!---->
												<li>Requisitos de Cargos<b>
											</p><!---->
										</td>
										<td width="60%">
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/planificacion/clasificacion/ProfesionCargo.jsp" class="contenidolink">Profesiones</a><br>
													<li><a href="../sigefirrhh/planificacion/clasificacion/ExperienciaCargo.jsp" class="contenidolink">Experiencia Laboral</a><br>
													<li><a href="../sigefirrhh/planificacion/clasificacion/AdiestramientoCargo.jsp" class="contenidolink">Adiestramiento</a><br>
													<li><a href="../sigefirrhh/planificacion/clasificacion/HabilidadCargo.jsp" class="contenidolink">Habilidades/Competencias</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
							<!--****************************************************************************************************
								Registro de Elegibles
								**************************************************************************************************** -->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Registro de Oferentes</td>
									</tr>
								</table>
								
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/planificacion/elegible/Elegible.jsp" class="contenidolink">Datos Personales</a><br>
											        <li><a href="../sigefirrhh/planificacion/elegible/ElegibleEducacion.jsp" class="contenidolink">Educaci�n Formal</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegibleProfesion.jsp" class="contenidolink">Profesiones</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegibleEstudio.jsp" class="contenidolink">Estudios Informales</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegibleCertificacion.jsp" class="contenidolink">Certificaciones</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegibleExperiencia.jsp" class="contenidolink">Experiencia Laboral (S. Privado)</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegibleIdioma.jsp" class="contenidolink">Idiomas</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegibleFamiliar.jsp" class="contenidolink">Grupo Familiar</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegibleAfiliacion.jsp" class="contenidolink">Afiliaciones a Gremios</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegibleActividadDocente.jsp" class="contenidolink">Actividades Docentes</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegibleOtraActividad.jsp" class="contenidolink">Otras Actividades</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegibleHabilidad.jsp" class="contenidolink">Habilidades/Competencias</a><br>
										            <li><a href="../sigefirrhh/planificacion/elegible/ElegiblePublicacion.jsp" class="contenidolink">Publicaciones</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>

								
							<!--****************************************************************************************************
								Selecci�n y Reclutamiento
								**************************************************************************************************** -->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Selecci�n y Reclutamiento</td>
									</tr>
								</table>
								
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
			            
													<li><a href="../sigefirrhh/planificacion/seleccion/Concurso.jsp" class="contenidolink">Concursos P�blicos</a><br>
													<li><a href="../sigefirrhh/planificacion/seleccion/ConcursoCargo.jsp" class="contenidolink">Cargos por Concursos</a><br>
													<li><a href="../sigefirrhh/planificacion/seleccion/PostuladoConcurso.jsp" class="contenidolink">Postulados a Concursos</a><br>
													
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
							<!--****************************************************************************************************
								Capacitacion
								**************************************************************************************************** -->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Capacitaci�n</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td align="left" colspan="2">
											<p><!---->
												<li>Tablas B�sicas<b>
											</p><!---->
										</td>
									<tr>
										<td colspan="2">
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/base/adiestramiento/AreaConocimiento.jsp" class="contenidolink">Areas de Conocimiento</a><br>
													<li><a href="../sigefirrhh/base/adiestramiento/TipoCurso.jsp" class="contenidolink">Tipos de Cursos</a><br>
													<li><a href="../sigefirrhh/base/adiestramiento/Curso.jsp" class="contenidolink">Cursos</a><br>
													<li><a href="../sigefirrhh/base/adiestramiento/TipoEntidad.jsp" class="contenidolink">Tipos de Entidades</a><br>
													<li><a href="../sigefirrhh/base/adiestramiento/EntidadEducativa.jsp" class="contenidolink">Entidades Educativas</a><br>
													<li><a href="../sigefirrhh/base/adiestramiento/CursoEntidad.jsp" class="contenidolink">Cursos por Entidad</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									
									<tr>
										<td align="left" width="50%">
											<p><!---->
												<li>Planificaci�n<b>
											</p><!---->
										</td>
										<td width="50%">
											<div align="left">
												<p><!---->
													<li><!--a href="../sigefirrhh/EEEEEE/PlanAdiestramiento.jsp" class="contenidolink"-->Planes por Unidad Funcional</a><br>
													<li><!--a href="../sigefirrhh/EEEEEE/Participante.jsp" class="contenidolink"-->Plan Adiestramiento por Trabajador</a><br>
													<li><!--a href="../sigefirrhh/EEEEEE/ActualizarParticipante.jsp" class="contenidolink"-->Actualizar Participante</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Evaluaciones de Desempe�o
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Evaluaciones de Desempe�o</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td colspan="2">
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEEEE/Evaluacion.jsp" class="contenidolink"> -->Evaluaciones</a><br>
													<li><!-- <a href="../sigefirrhh/EEEEEE/AccionEvaluacion.jsp" class="contenidolink"> -->Tipos de Acciones Resultantes</a><br>
													<li><!-- <a href="../sigefirrhh/EEEEEE/ResultadoEvaluacion.jsp" class="contenidolink"> -->Tipos de Resultados de Evaluaci�n</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Planes de Personal
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Planes de Personal</td>
									</tr>
								</table>
								
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!--a href="../sigefirrhh/EEEE/PlanPersonal.jsp" class="contenidolink"-->Planes</a><br>
													<li><!--a href="../sigefirrhh/EEEE/MovimientoPlan.jsp" class="contenidolink"-->Movimientos de Personal</a><br>
													<li><!--a href="../sigefirrhh/EEEE/RegistroCargosAprobado.jsp" class="contenidolink"-->Acciones Administrativas</a><br>
													<li><!--a href="../sigefirrhh/EEEE/ContratosPlan.jsp" class="contenidolink"-->Contratos de Personal</a><br>
													<!--
													<li><a href="../sigefirrhh/EEEE/ReportPlanPersonal.jsp" class="contenidolink">Plan Personal</a><br>
													<li><a href="../sigefirrhh/EEEE/ReportPlanPersonalEjecutado.jsp" class="contenidolink">Ejecuci�n Plan Personal</a><br>
													-->
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								EXPEDIENTE DEL TRABAJADOR
								****************************************************************************************************-->
								
								<table width="100%" class="bandtable">
									<tr>
										<td align="left"><span class="style4">EXPEDIENTE DEL TRABAJADOR</span></td>
									</tr>
								</table>
								<p></p>
								
								
							<!--****************************************************************************************************
								Expediente del Trabajador
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Expediente del Trabajador</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/personal/expediente/Personal.jsp" class="contenidolink">Datos Personales</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Educacion.jsp" class="contenidolink">Educaci�n Formal</a><br>
													<li><a href="../sigefirrhh/personal/expediente/ProfesionTrabajador.jsp" class="contenidolink">Profesiones</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Estudio.jsp" class="contenidolink">Estudios Informales</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Certificacion.jsp" class="contenidolink">Certificaciones</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Experiencia.jsp" class="contenidolink">Experiencia Laboral</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Idioma.jsp" class="contenidolink">Idiomas</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Familiar.jsp" class="contenidolink">Grupo Familiar</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Afiliacion.jsp" class="contenidolink">Afiliaciones a Gremios</a><br>
													<li><a href="../sigefirrhh/personal/expediente/ActividadDocente.jsp" class="contenidolink">Actividad Docente</a><br>
													<li><a href="../sigefirrhh/personal/expediente/OtraActividad.jsp" class="contenidolink">Otras Actividades</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Habilidad.jsp" class="contenidolink">Habilidades/Competencias</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Reconocimiento.jsp" class="contenidolink">Reconocimientos</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Sancion.jsp" class="contenidolink">Sanciones</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Averiguacion.jsp" class="contenidolink">Averiguaciones Administrativas</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Certificado.jsp" class="contenidolink">Certificado de Carrera</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Publicacion.jsp" class="contenidolink">Publicaciones</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Pasantia.jsp" class="contenidolink">Pasant�as</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Declaracion.jsp" class="contenidolink">Declaraciones Juradas</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Historial
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Historial</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/personal/expediente/HistorialOrganismo.jsp" class="contenidolink">Historial en el Organismo</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Encargaduria.jsp" class="contenidolink">Encargadur�as</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Suplencia.jsp" class="contenidolink">Suplencias</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Contrato.jsp" class="contenidolink">Contratos</a><br>
													<li><a href="../sigefirrhh/personal/expediente/HistorialApn.jsp" class="contenidolink">Trayectoria en APN</a><br>
													<li><a href="../sigefirrhh/personal/expediente/Antecedente.jsp" class="contenidolink">Antecedentes/Servicio sujeto a LEFP</a><br>
													<li><a href="../sigefirrhh/personal/expediente/experienciaNoEst.jsp" class="contenidolink">Antecedentes/Servicio no sujeto a LEFP</a><br>
													<li><a href="../sigefirrhh/personal/expediente/ComisionServicio.jsp" class="contenidolink">Comision/Servicio (personal interno)</a><br>
													<li><a href="../sigefirrhh/personal/expediente/ServicioExterior.jsp" class="contenidolink">Servicio Exterior</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Tablas Basicas Expediente
								****************************************************************************************************-->		
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Tablas Basicas Expediente</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/base/personal/GrupoProfesion.jsp" class="contenidolink"> -->Grupo de Profesiones</a><br>
													<li><a href="../sigefirrhh/base/personal/SubGrupoProfesion.jsp" class="contenidolink">Subgrupo de Profesiones</a><br>
													<li><a href="../sigefirrhh/base/personal/Profesion.jsp" class="contenidolink">Profesiones</a><br>
													<li><a href="../sigefirrhh/base/personal/AreaCarrera.jsp" class="contenidolink">Areas de Carrera</a><br>
													<li><a href="../sigefirrhh/base/personal/Carrera.jsp" class="contenidolink">Carreras</a><br>
													<li><a href="../sigefirrhh/base/personal/CarrerraArea.jsp" class="contenidolink">Carreras por Area</a><br>
													<li><a href="../sigefirrhh/base/personal/Titulo.jsp" class="contenidolink">T�tulos</a><br>
													<li><a href="../sigefirrhh/base/personal/TipoIdioma.jsp" class="contenidolink">Idiomas</a><br>
													<li><a href="../sigefirrhh/base/personal/TipoAmonestacion.jsp" class="contenidolink">Tipos Sanciones</a><br>
													<li><a href="../sigefirrhh/base/personal/TipoReconocimiento.jsp" class="contenidolink">Tipos Reconocimientos</a><br>
													<li><a href="../sigefirrhh/base/personal/TipoHabilidad.jsp" class="contenidolink">Habilidades/Competencias</a><br>
													<li><a href="../sigefirrhh/base/personal/TipoOtraActividad.jsp" class="contenidolink">Otras Actividades</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								ADMINISTRACION DE PERSONAL
								****************************************************************************************************-->
								
								<table width="100%" class="bandtable">
									<tr>
										<td align="left"><span class="style4">ADMINISTRACION DE PERSONAL</span></td>
									</tr>
								</table>
								<p></p>
								
								
							<!--****************************************************************************************************
								Definiciones
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Parametrizacion</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td align="left" width="35%">
											<p><!---->
												<li>Definiciones
											</p><!---->
										</td>
										<td width="65%">
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/base/definiciones/CategoriaPersonal.jsp" class="contenidolink">Categor�a de Personal</a><br>
													<li><a href="../sigefirrhh/base/definiciones/RelacionPersonal.jsp" class="contenidolink">Relaci�n Laboral</a><br>
													<li><a href="../sigefirrhh/base/definiciones/ClasificacionPersonal.jsp" class="contenidolink">Clasificaci�n de Personal</a><br>
													<li><a href="../sigefirrhh/base/definiciones/GrupoNomina.jsp" class="contenidolink">Grupos de Nomina</a><br>
													<li><a href="../sigefirrhh/base/definiciones/TipoPersonal.jsp" class="contenidolink">Tipos de Personal</a><br>
													<li><a href="../sigefirrhh/base/definiciones/Restringido.jsp" class="contenidolink">Restricciones de Personal</a><br>
													<li><a href="../sigefirrhh/base/definiciones/ManualPersonal.jsp" class="contenidolink">Clasificadores Tipo de Personal</a><br>
													<li><a href="../sigefirrhh/base/definiciones/FrecuenciaPago.jsp" class="contenidolink">Frecuencia de Pago</a><br>
													<li><a href="../sigefirrhh/base/definiciones/Concepto.jsp" class="contenidolink">Conceptos</a><br>
													<li><a href="../sigefirrhh/base/definiciones/FrecuenciaTipoPersonal.jsp" class="contenidolink">Fecuencias por Tipo de Personal</a><br>
													<li><a href="../sigefirrhh/base/definiciones/ConceptoTipoPersonal.jsp" class="contenidolink">Conceptos por Tipo de Personal</a><br>
													<li><a href="../sigefirrhh/base/definiciones/ConceptoCargoAnio.jsp" class="contenidolink">Conceptos Cargo y A�o</a><br>
													<li><a href="../sigefirrhh/base/definiciones/ConceptoCargo.jsp" class="contenidolink">Conceptos Cargo</a><br>
													<li><a href="../sigefirrhh/base/definiciones/Turno.jsp" class="contenidolink">Turno</a><br>
													<li><a href="../sigefirrhh/base/definiciones/TipoContrato.jsp" class="contenidolink">Tipos de Contrato</a><br>
													<li><a href="../sigefirrhh/base/definiciones/Disquete.jsp" class="contenidolink">Archivos (Disquetes)</a><br>
													<li><a href="../sigefirrhh/base/definiciones/DetalleDisquete.jsp" class="contenidolink">Detalle Archivos (Disquetes)</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									
									<tr>
										<td>
											<p><!---->
												<li>Parametros y F�rmulas
											</p><!---->
										</td>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/base/definiciones/ConceptoAsociado.jsp" class="contenidolink">Conceptos Asociados</a><br>
													<li><a href="../sigefirrhh/base/definiciones/PrimaAntiguedad.jsp" class="contenidolink">Prima Antiguedad</a><br>
													<li><a href="../sigefirrhh/base/definiciones/PrimaHijo.jsp" class="contenidolink">Prima Por Hijo</a><br>
													<li><a href="../sigefirrhh/base/definiciones/ParametroGobierno.jsp" class="contenidolink">Par�metros Retenciones</a><br>
													<li><a href="../sigefirrhh/base/definiciones/ConceptoVacaciones.jsp" class="contenidolink">F�rmula Bono Vacacional</a><br>
													<li><a href="../sigefirrhh/base/definiciones/VacacionesPorAnio.jsp" class="contenidolink">Vacaciones por A�os</a><br>
													<li><a href="../sigefirrhh/base/definiciones/ConceptoUtilidades.jsp" class="contenidolink">F�rmula Bono Fin de A�o</a><br>
													<li><!--a href="../sigefirrhh/base/EEEEEE/UtilidadesPorAnio.jsp" class="contenidolink"-->D�as Bono Fin de A�o</a><br>
													<li><a href="../sigefirrhh/base/definiciones/ParametroVarios.jsp" class="contenidolink">Par�metros Varios</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									
									<tr>
										<td>
											<p><!---->
												<li>Tablas B�sicas N�mina
											</p><!---->
										</td>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/base/definiciones/Banco.jsp" class="contenidolink">Bancos</a><br>
													<li><a href="../sigefirrhh/base/definiciones/CuentaBanco.jsp" class="contenidolink">Cuentas de Banco</a><br>
													<li><a href="../sigefirrhh/base/definiciones/Semana.jsp" class="contenidolink">Per�odos Semanales</a><br>
													<li><a href="../sigefirrhh/base/definiciones/Mes.jsp" class="contenidolink">Meses</a><br>
													<li><a href="../sigefirrhh/base/definiciones/Sindicato.jsp" class="contenidolink">Sindicatos</a><br>
													<li><a href="../sigefirrhh/base/definiciones/ContratoColectivo.jsp" class="contenidolink">Contratos Colectivos</a><br>
													<li><a href="../sigefirrhh/base/definiciones/TarifaAri.jsp" class="contenidolink">Tarifas ARI</a><br>
													<li><a href="../sigefirrhh/base/definiciones/FirmasReportes.jsp" class="contenidolink">Firmas para Reportes</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
            
							<!--****************************************************************************************************
								Datos del Trabajador
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Datos del Trabajador</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td align="left" width="30%">
											<p><!---->
												<li>Trabajador<b>
											</p><!---->
										</td>
										<td width="70%">
											<div align="left">
												<p><!---->
													<li><!-- a href="../sigefirrhh/personal/trabajador/UbicarTrabajador.jsp" class="contenidolink" -->Ubicar Trabajador</a><br>
													<li><!-- a href="../sigefirrhh/personal/trabajador/UbicarTrabajadorDocente.jsp" class="contenidolink" -->Ubicar Trabajador Docente</a><br>
													<li><a href="../sigefirrhh/personal/trabajador/Trabajador.jsp" class="contenidolink">Consultar Datos N�mina</a><br>
													<li><!-- a href="../sigefirrhh/personal/trabajador/ActualizarDatosTrabajador.jsp" class="contenidolink" -->Actualizar Datos</a><br>
													<li><!-- a href="../sigefirrhh/personal/trabajador/Jubilado.jsp" class="contenidolink" -->Datos Personal Jubilado</a><br>
													<li><!-- a href="../sigefirrhh/personal/trabajador/ActualizarFechasTrabajador.jsp" class="contenidolink" -->Actualizar Fechas</a><br>
													<li><!-- a href="../sigefirrhh/personal/trabajador/ActualizarEstatusFeVidaTrabajador.jsp" class="contenidolink" -->Actualizar Estatus y Fe de Vida</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/personal/trabajador/ConceptoFijo.jsp" class="contenidolink">Conceptos Fijos</a><br>
													<li><a href="../sigefirrhh/personal/trabajador/ConceptoVariable.jsp" class="contenidolink">Conceptos Variables</a><br>
													<li><a href="../sigefirrhh/personal/trabajador/Prestamo.jsp" class="contenidolink">Pr�stamos</a><br>
													<li><a href="../sigefirrhh/personal/trabajador/SueldoPromedio.jsp" class="contenidolink">Sueldos Promedios</a><br>
													<li><a href="../sigefirrhh/personal/trabajador/Anticipo.jsp" class="contenidolink">Anticipos</a><br>
													<li><a href="../sigefirrhh/personal/trabajador/Vacacion.jsp" class="contenidolink">Vacaciones</a><br>
													<li><a href="../sigefirrhh/personal/trabajador/VacacionProgramada.jsp" class="contenidolink">Vacaciones Programadas</a><br>
													<li><a href="../sigefirrhh/personal/trabajador/Ausencia.jsp" class="contenidolink">Ausencias</a><br>
													<li><!--a href="../sigefirrhh/personal/trabajador/PlanillaAri.jsp" class="contenidolink"-->Planilla ARI</a><br>
													<li><a href="../sigefirrhh/personal/trabajador/PlanillaArc.jsp" class="contenidolink">Planilla ARC</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									<tr>
										<td align="left">
											<p><!---->
												<li>Embargos<b>
											</p><!---->
										</td>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/personal/trabajador/Embargo.jsp" class="contenidolink">Sentencias</a><br>
													<li><a href="../sigefirrhh/personal/trabajador/EmbargoConcepto.jsp" class="contenidolink">Detalle Embargo</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Movimientos de Personal
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Movimientos de Personal</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td align="left" width="45%">
											<p><!---->
												<li>Tipos de Movimientos y Causas<b>
											</p><!---->
										</td>
										<td width="55%">
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/base/registro/MovimientoPersonal.jsp" class="contenidolink">Tipos de Movimiento</a><br>
													<li><a href="../sigefirrhh/base/registro/CausaMovimiento.jsp" class="contenidolink">Causas del Movimiento</a><br>
													<li><a href="../sigefirrhh/base/registro/CausaPersonal.jsp" class="contenidolink">Causas por Tipo Personal</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									
									<tr>
										<td colspan="2">
											<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
												<tr>
													<td colspan="2" align="left">
														<p><!---->
															<li>Personal Bajo LEFP<b>
														</p><!---->
													</td>
												</tr>
												<tr>
													<td width="30%" align="left">
														<p><!---->
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ingresos
														</p><!---->
													</td>
													<td width="70%">
														<div align="left">
															<p><!---->
																<li><!--a href="../sigefirrhh/EEEEEE/IngresoCargoCarreraLefp.jsp" class="contenidolink"-->Cargo de Carrera</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/IngresoTrabajadorLefp.jsp" class="contenidolink"-->Cargo de Libre Nombramiento</a><br>
															</p><!---->
														</div>
													</td>
												</tr>
												<tr>
													<td align="left">
														<p><!---->
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reingresos
														</p><!---->
													</td>
													<td>
														<div align="left">
															<p><!---->
																<li><!--a href="../sigefirrhh/EEEEEE/ReingresoCargoCarreraLefp.jsp" class="contenidolink"-->Cargo de Carrera</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/ReingresoTrabajadorLefp.jsp" class="contenidolink"-->Cargo de Libre Nombramiento</a><br>
															</p><!---->
														</div>
													</td>
												</tr>
												<tr>
													<td align="left">
														<p><!---->
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Actualizaciones
														</p><!---->
													</td>
													<td>
														<div align="left">
															<p><!---->
																<li><!--a href="../sigefirrhh/EEEEEE/CambioCargoLefp" class="contenidolink"-->Cambio en el cargo</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/DesignacionLefp.jsp" class="contenidolink"-->Designaci�n</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/AscensoLefp.jsp" class="contenidolink"-->Ascenso</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/ClasificacionLefp.jsp" class="contenidolink"-->Clasificaci�n</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/CambioClasificacionLefp.jsp" class="contenidolink"-->Cambio Clasificaci�n</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/AumentoSueldoLefp.jsp" class="contenidolink"-->Aumento de Sueldo</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/LicenciaSinSueldoLefp.jsp" class="contenidolink"-->Licencia sin Sueldo</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/LicenciaConSueldoLefp.jsp" class="contenidolink"-->Licencia con Sueldo</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/SuspensionLefp.jsp" class="contenidolink"-->Suspensi�n</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/ReubicacionLefp.jsp" class="contenidolink"-->Reubicaci�n</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/ReincorporacionLefp.jsp" class="contenidolink"-->Reincorporaci�n</a><br>
																<!--
																<li><!--a href="../sigefirrhh/EEEEEE/ReincorporacionPorSentenciaLefp.jsp" class="contenidolink">Reincorporaci�n por Sentencia</a><br>
																-->
																<li><!--a href="../sigefirrhh/EEEEEE/TrasladoLefp.jsp" class="contenidolink"-->Traslado</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/TrasladoArt35Lefp.jsp" class="contenidolink"-->Traslado Art. 35</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/ModificacionHorarioLefp.jsp" class="contenidolink"-->Modificaci�on de Horario</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/CorreccionMovimientoLefp.jsp" class="contenidolink"-->Correci�n de Movimiento</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/AnulacionMovimientoLefp.jsp" class="contenidolink"-->Anulacion de Movimiento</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/ActualizacionDatosLefp.jsp" class="contenidolink"-->Actualizaci�on Datos Personales</a><br>
															</p><!---->
														</div>
													</td>
												</tr>
												<tr>
													<td align="left">
														<p><!---->
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jubilaciones y<br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pensiones
														</p><!---->
													</td>
													<td>
														<div align="left">
															<p><!---->
																<li><!--a href="../sigefirrhh/EEEEEE/EEEEEEE.jsp" class="contenidolink"-->Calculo de Jubilaci�n/Pensi�n</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/IngresoTrabajadorJubilado.jsp" class="contenidolink"-->Ingreso a N�mina Jubilado/Pensionado</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/EgresoTrabajadorJubilado.jsp" class="contenidolink"-->Egreso de N�mina Jubilado/Pensionado</a><br>
															</p><!---->
														</div>
													</td>
												</tr>
												<tr>
													<td colspan="2" align="left">
														<p><!---->
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<li><!--a href="../sigefirrhh/EEEEEE/EgresoTrabajadorLefp.jsp" class="contenidolink"-->Egresos</a><br>
														</p><!---->
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
												<tr>
													<td colspan="2" align="left">
														<p><!---->
															<li>Personal no sujeto a LEFP<b>
														</p><!---->
													</td>
												</tr>
												<tr>
													<td width="30%" align="left">
														<p><!---->
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Con Registro
														</p><!---->
													</td>
													<td width="70%">
														<div align="left">
															<p><!---->
																<li><!--a href="../sigefirrhh/EEEEEE/IngresoTrabajadorRegistro.jsp" class="contenidolink"-->Ingresos</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/ReingresoTrabajadorRegistro.jsp" class="contenidolink"-->Reingresos</a><br>
																<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
																	<tr>
																		<td width="40%" align="left">
																			<li>Actualizaciones<br>
																		</td>
																		<td width="60%" align="left">
																			<li><!--a href="../sigefirrhh/EEEEEE/CambioCargoNoLefp.jsp" class="contenidolink"-->Cambio en el Cargo</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/ClasificacionNoLefp.jsp" class="contenidolink"-->Clasificacion</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/AscensoConRegistrop.jsp" class="contenidolink"-->Ascenso</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/AumentoSueldoConRegistro.jsp" class="contenidolink"-->Aumento de Sueldo</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/LicenciaSinSueldoConRegistro.jsp" class="contenidolink"-->Licencia sin Sueldo</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/LicenciaConSueldoConRegistro.jsp" class="contenidolink"-->Licencia con Sueldo</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/SuspensionConRegistro.jsp" class="contenidolink"-->Suspensi�n</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/ReincorporacionConRegistro.jsp" class="contenidolink"-->Reincorporacion</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/TrasladoConRegistro.jsp" class="contenidolink"-->Traslado</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/ActualizacionDatosConRegistro.jsp" class="contenidolink"-->Actualizaci�n Datos Personales</a><br>
																		</td>
																	</tr>
																</table><br>
																<li><!--a href="../sigefirrhh/EEEEEE/EgresoTrabajadorRegistro.jsp" class="contenidolink"-->Egresos</a><br>
															</p><!---->
														</div>
													</td>
												</tr>
												<tr>
													<td width="30%" align="left">
														<p><!---->
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sin Registro
														</p><!---->
													</td>
													<td width="70%">
														<div align="left">
															<p><!---->
																<li><!--a href="../sigefirrhh/EEEEEE/ingresoTrabajadorSinRegistro.jsp" class="contenidolink"-->Ingresos</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/reingresoTrabajadorSinRegistro.jsp" class="contenidolink"-->Reingresos</a><br>
																<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
																	<tr>
																		<td width="40%" align="left">
																			<li>Actualizaciones<br>
																		</td>
																		<td width="60%" align="left">
																			<li><!--a href="../sigefirrhh/EEEEEE/AumentoSueldoSinRegistro.jsp" class="contenidolink"-->Aumento de Sueldo</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/LicenciaSinSueldoSinRegistro.jsp" class="contenidolink"-->Licencia sin Sueldo</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/LicenciaConSueldoSinRegistro.jsp" class="contenidolink"-->Licencia con Sueldo</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/SuspensionSinRegistro.jsp" class="contenidolink"-->Suspensi�n sin Sueldo</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/ReincorporacionSinRegistro.jsp" class="contenidolink"-->Reincorporacion</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/TrasladoSinRegistro.jsp" class="contenidolink"-->Traslado</a><br>
																			<li><!--a href="../sigefirrhh/EEEEEE/ActualizacionDatosSinRegistro.jsp" class="contenidolink"-->Actualizaci�n Datos Personales</a><br>
																		</td>
																	</tr>
																</table><br>
																<li><!--a href="../sigefirrhh/EEEEEE/EgresoTrabajadorSinRegistro.jsp" class="contenidolink"-->Egresos</a><br>
															</p><!---->
														</div>
													</td>
												</tr>
												<tr>
													<td width="30%" align="left">
														<p><!---->
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Manejo de Remesas
														</p><!---->
													</td>   
													<td width="70%">
														<div align="left">
															<p><!---->
																<li><!--a href="../sigefirrhh/EEEEEE/Remesas.jsp" class="contenidolink"-->Remesas</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/RemesaMovimientoSitp.jsp" class="contenidolink"-->Asociar Movimientos</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/CerrarRemesa.jsp" class="contenidolink"-->Cerrar Remesa</a><br>
																<li><!--a href="../sigefirrhh/EEEEEE/EnviarRemesa.jsp" class="contenidolink"-->Enviar Remesa</a><br>
															</p><!---->
														</div>
													</td>
												</tr>
												<tr>
													<td width="30%" align="left">
														<p><!---->
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<li><!--a href="../sigefirrhh/EEEE/Inhabilitado.jsp" class="contenidolink"11-->Personal Inhabilitado</a><br>
														</p><!---->
													</td>
												<tr>
											</table>
										</td>
									</tr>
								
							<!--****************************************************************************************************
								Actualizaci�n de Conceptos
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Actualizaci�n de Conceptos</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/CargaConceptos.jsp" class="contenidolink"> -->Carga Externa de Conceptos (Archivo)</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/CargaPrestamos.jsp" class="contenidolink"> -->Carga Externa de Prestamos (Archivo)</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/CargaMasivaConceptos.jsp" class="contenidolink"> -->Actualizaci�n Masiva</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/RecalculoPorCriterio.jsp" class="contenidolink"> -->Actualizar por Criterios</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/CambioFrecuencia.jsp" class="contenidolink"> -->Cambio de Frecuencia</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/CambioEstatus.jsp" class="contenidolink"> -->Cambio de Estatus</a><br>
													<!--
													<li><a href="../sigefirrhh/EEEE/CambioDocumentoSoporte.jsp" class="contenidolink">Actualizar Documento Soporte</a><br>
													-->
													<li><!-- <a href="../sigefirrhh/EEEE/ActualizarMonto.jsp" class="contenidolink"> -->Actualizar Monto y Unidades</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/EliminarConceptos.jsp" class="contenidolink"> -->Eliminar</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/Recalculo.jsp" class="contenidolink"> -->Rec�lculo de un concepto</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/RecalculoPorAsociado.jsp" class="contenidolink"> -->Rec�lculo de conceptos por variaci�n</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/IngresarConceptos.jsp" class="contenidolink"> -->Carga Relacionada a un Concepto</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/CargaMasivaPrestamos.jsp" class="contenidolink"> -->Carga Masiva Pr�stamos</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/CargaSobretiempo.jsp" class="contenidolink"> -->Carga Sobretiempo</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Procesos Aniversario
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Procesos Aniversario</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/personal/procesoAniversario/CalcularPrimaAntiguedad.jsp" class="contenidolink">Calculo Prima Antiguedad</a><br>
													<li><a href="../sigefirrhh/personal/procesoAniversario/CalcularBonoVacacional.jsp" class="contenidolink">Calculo Bono Vacacional</a><br>
													<li><a href="../sigefirrhh/personal/procesoAniversario/SeguridadAniversario.jsp" class="contenidolink">Seguridad Procesos Aniversario</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Procesos de N�mina
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Procesos de N�mina</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td colspan="2">
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/SeguridadOrdinaria.jsp" class="contenidolink"> -->N�minas Ordinarias Procesadas</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/SeguridadEspecial.jsp" class="contenidolink"> -->N�minas Especiales Procesadas</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/NominaEspecial.jsp" class="contenidolink"> -->Definici�n de N�mina Especial</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/GenerarSueldoPromedio.jsp" class="contenidolink"> -->Calcular Sueldos Promedios</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									
									<tr>
										<td align="left">
											<p><!---->
												<li>Pren�mina Ordinaria<b>
											</p><!---->
										</td>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEEEE/GenerarPrenomina.jsp" class="contenidolink"> -->Proceso</a><br>
													<li><!-- <a href="../sigefirrhh/EEEEEE/ReportVariacionesPrenomina.jsp" class="contenidolink"> -->Variaciones</a><br>
													<li><!-- <a href="../sigefirrhh/EEEEEE/ReportPrenomina.jsp" class="contenidolink"> -->Reportes</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									
									<tr>
										<td align="left">
											<p><!---->
												<li>N�mina Ordinaria<b>
											</p><!---->
										</td>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/GenerarNomina.jsp" class="contenidolink"> -->Proceso</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportNomina.jsp" class="contenidolink"> -->Reportes</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									
									<tr>
										<td align="left">
											<p><!---->
												<li>Pren�mina Especial<b>
											</p><!---->
										</td>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/GenerarPrenominaEspecial.jsp" class="contenidolink"> -->Proceso</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportPrenominaEspecial.jsp" class="contenidolink"> -->Reportes</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									
									<tr>
										<td align="left">
											<p><!---->
												<li>N�mina Especial<b>
											</p><!---->
										</td>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/GenerarNominaEspecial.jsp" class="contenidolink"> -->Proceso</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportNominaEspecial.jsp" class="contenidolink"> -->Reportes</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									
									<tr>
										<td align="left">
											<p><!---->
												<li>Generar Disquete<b>
											</p><!---->
										</td>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- a href="../sigefirrhh/EEEE/DisqueteNomina.jsp" class="contenidolink"-->Generar Disquete</a><br>
													<li><!-- a href="../sigefirrhh/EEEE/ReversarNomina.jsp" class="contenidolink"-->Reversar N�mina</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
							<!--****************************************************************************************************
								Aportes Patronales
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Aportes</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/ReportAportesPatronales.jsp" class="contenidolink"> -->Reportes</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/DisqueteAporte.jsp" class="contenidolink"> -->Archivos Retenciones y Aportes</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
							<!--****************************************************************************************************
								Otros Procesos
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Otros Procesos</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/ProcesoAumentoPorPorcentaje.jsp" class="contenidolink"> -->Aumento por Porcentaje</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ProcesoAumentoPorEvaluacion.jsp" class="contenidolink"> -->Aumento por Evaluacion</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/SuspenderTrabajadores.jsp" class="contenidolink"> -->Suspender Trabajadores</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/CalcularBonoFinAnio.jsp" class="contenidolink"> -->C�lculo Bono Fin A�o</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/GenerarPlanillaArc.jsp" class="contenidolink"> -->Generar Planilla ARC</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportPlanillaArc.jsp" class="contenidolink"> -->Reporte Planilla ARC</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/PagoFueraNomina.jsp" class="contenidolink"> -->Pagos Fuera del Sistema</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
							<!--****************************************************************************************************
								Registro Cargos/Puestos
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Registro Cargos/Puestos</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/Registro.jsp" class="contenidolink"> -->Definici�n de Registros</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/RegistroPersonal.jsp" class="contenidolink"> -->Registros por Tipo Personal</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/RegistroCargos.jsp" class="contenidolink"> -->Consultar Posicion Registro</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/HistoricoCargos.jsp" class="contenidolink"> -->Trayectoria Posicion Registro</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/AgregarRegistroCargos.jsp" class="contenidolink"> -->Agregar Cargos/Puestos</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Prestaciones Sociales Mensuales
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Prestaciones Sociales Mensuales</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/ConceptoPrestaciones.jsp" class="contenidolink"> -->Conceptos para Prestaciones</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/PrestacionesMensuales.jsp" class="contenidolink"> -->Prestaciones por Trabajador</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/CalcularPrestacionesMensuales.jsp" class="contenidolink"> -->Proceso Mensual</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/GenerarPagoDiasAdicionales.jsp" class="contenidolink"> -->Pago Dias Adicionales</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ActualizarFideicomiso.jsp" class="contenidolink"> -->Registro Fideicomiso</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/Fideicomiso.jsp" class="contenidolink"> -->Fideicomiso por Trabajador</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/DisqueteFideicomiso.jsp" class="contenidolink"> -->Generar Disquete Fideicomiso</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>  
								
							<!--****************************************************************************************************
								Jubilaciones y Pensiones
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Jubilaciones y Pensiones</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/personal/sigecof/TabuladorJub.jsp" class="contenidolink">Par�metros Jubilacion</a><br>
													<li><a href="../sigefirrhh/personal/sigecof/PensionInvalidez.jsp" class="contenidolink">Par�metros Pension Invalidez</a><br>
													<li><a href="../sigefirrhh/personal/sigecof/PensionSobreviviente.jsp" class="contenidolink">Par�metros Pension Sobreviviente</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								
							<!--****************************************************************************************************
								Ejecuci�n Presupuestaria
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Ejecuci�n Presupuestaria</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td align="left">
											<p><!---->
												<li>Estructura Presupuestaria<b>
											</p><!---->
										</td>
										<td>
											<div align="left">
												<p><!---->
													<!--li><a href="../sigefirrhh/personal/sigecof/CuentaContable.jsp" class="contenidolink">Cuentas Contables</a><br-->
													<li><a href="../sigefirrhh/personal/sigecof/CuentaPresupuesto.jsp" class="contenidolink">Cuentas Presupuestarias</a><br>
													<li><!--a href="../sigefirrhh/personal/sigecof/Proyecto.jsp" class="contenidolink"-->Proyectos</a><br>
													<li><!--a href="../sigefirrhh/personal/sigecof/AccionCentralizada.jsp" class="contenidolink"-->Acciones Centralizadas</a><br>
													<li><!--a href="../sigefirrhh/personal/sigecof/AccionEspecifica.jsp" class="contenidolink"-->Acciones Especificas</a><br>
													<li><!--a href="../sigefirrhh/personal/sigecof/UelEspecifica.jsp" class="contenidolink"-->Acciones Especificas por Unidades Ejecutoras</a><br>
													<li><a href="../sigefirrhh/personal/sigecof/ConceptoCuenta.jsp" class="contenidolink">Relaci�n Concepto con Cuenta</a><br>
													<li><!--a href="../sigefirrhh/personal/sigecof/ConceptoEspecifica.jsp" class="contenidolink"-->Relaci�n Concepto con Proyecto/Accion</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
									<tr>
										<td align="left">
											<p><!---->
												<li>Asignacion a Proyectos/Acciones<b>
											</p><!---->
										</td>
										<td>
											<div align="left">
												<p><!---->
													<!--li><a href="../sigefirrhh/personal/sigecof/GenerarTrabajadorCargoEspecifica.jsp" class="contenidolink"-->Asignar Trabajadores/Cargos a Proyecto/Accion</a><br>
													<!--li><a href="../sigefirrhh/personal/sigecof/TrabajadorEspecifica.jsp" class="contenidolink"-->Asignar Trabajador a Proyecto/Accion</a><br>
													<!--li><a href="../sigefirrhh/personal/sigecof/CargoEspecifica.jsp" class="contenidolink"-->Asignar Cargo a Proyecto/Accion</a><br>
													<!--li><a href="../sigefirrhh/personal/sigecof/ReportResumenMensualPagos.jsp" class="contenidolink"-->Resumen Mensual Pagos</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
<!-- 
#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
-->
								
							<!--****************************************************************************************************
								Constancias de Trabajo
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Constancias de Trabajo</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/Constancia.jsp" class="contenidolink"> -->Definici�n de Constancias</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/DetalleConstancia.jsp" class="contenidolink"> -->Detalle de Constancias</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/SolicitudConstancia.jsp" class="contenidolink"> -->Solicitud de Constancia</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/GenerarConstancias.jsp" class="contenidolink"> -->Generar Constancias</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportConstancia.jsp" class="contenidolink"> -->Reportes</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Reportes - Trabajadores
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Reportes - Trabajadores</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/ReportTrabajadorTipoPersonal.jsp" class="contenidolink"> -->Por Tipo de Personal</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportTrabajadorSede.jsp" class="contenidolink"> -->Por Sede</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportTrabajadorRegion.jsp" class="contenidolink"> -->Por Regi�n</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportTrabajadorUnidadFuncional.jsp" class="contenidolink"> -->Por Unidad Funcional</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportTrabajadorDependencia.jsp" class="contenidolink"> -->Por Dependencia</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportIngreso.jsp" class="contenidolink"> -->Ingresos por Per�odo</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportEgreso.jsp" class="contenidolink"> -->Egresos por Per�odo</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportFechasRegion.jsp" class="contenidolink"> -->Fechas por Regi�n</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportFechasSede.jsp" class="contenidolink"> -->Fechas por Sede</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportFechasUnidadFuncional.jsp" class="contenidolink"> -->Fechas por Unidad Funcional</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ReportFechasDependencia.jsp" class="contenidolink"> -->Fechas por Dependencia</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								BIENESTAR SOCIAL
								****************************************************************************************************-->
								
								<table width="100%" class="bandtable">
									<tr>
										<td align="left"><span class="style4">BIENESTAR SOCIAL</span></td>
									</tr>
								</table>
								<p></p>
								
								
							<!--****************************************************************************************************
								Juguetes
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Juguetes</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/ParametroJuguete.jsp" class="contenidolink"> -->Par�metros</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Utiles Escolares
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Utiles Escolares</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/ParametroUtiles.jsp" class="contenidolink"> -->Par�metros</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Becas a Familiares
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Becas a Familiares</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/NivelBeca.jsp" class="contenidolink"> -->Niveles Educativos</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ParametroBeca.jsp" class="contenidolink"> -->Par�metros</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Guarder�as
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Guarder�as</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/ParametroGuarderia.jsp" class="contenidolink"> -->Par�metros</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/Guarderia.jsp" class="contenidolink"> -->Guarder�as</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Dotaciones
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Dotaciones</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/TipoDotacion.jsp" class="contenidolink"> -->Tipos Dotaci�n</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/SubtipoDotacion.jsp" class="contenidolink"> -->Subtipos Dotaci�n</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/DotacionCargo.jsp" class="contenidolink"> -->Dotaci�n por Cargo</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/ParametroDotacion.jsp" class="contenidolink"> -->Par�metros Dotaci�n</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								P�lizas de Seguro
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">P�lizas de Seguro</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/Poliza.jsp" class="contenidolink"> -->P�liza</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/PlanPoliza.jsp" class="contenidolink"> -->Planes</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/PrimasPlan.jsp" class="contenidolink"> -->Primas Pr Planes</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Tickets
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Tickets</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Proveedores de Tickets</a><br>
													<li><!-- <a href="../sigefirrhh/EEEE/.jsp" class="contenidolink"> -->Parametros de Tickets</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
								
							<!--****************************************************************************************************
								Indicadores de Gesti�n
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Indicadores de Gesti�n</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><!-- <a href="../sigefirrhh/EEEE/Trabajadores.jsp" class="contenidolink"> -->Trabajadores por Tipo Personal</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
								
							<!--****************************************************************************************************
								ADMINISTRACION DEL SISTEMA
								****************************************************************************************************-->
								
								<table width="100%" class="bandtable">
									<tr>
										<td align="left"><span class="style4">ADMINISTRACION DEL SISTEMA</span></td>
									</tr>
								</table>
								<p></p>
								
								
							<!--****************************************************************************************************
								Usuarios
								****************************************************************************************************-->
								<table width="95%" class="bandtable" align="center">
									<tr>
										<td align="left">Usuarios</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p><!---->
													<li><a href="../sigefirrhh/sistema/Usuario.jsp" class="contenidolink">Usuarios</a><br>
													<li><a href="../sigefirrhh/sistema/UsuarioOrganismo.jsp" class="contenidolink">Usuarios por Organismo</a><br>
													<li><a href="../sigefirrhh/sistema/UsuarioTipoPersonal.jsp" class="contenidolink">Usuarios por Tipo de Personal</a><br>
													<li><a href="../sigefirrhh/sistema/Rol.jsp" class="contenidolink">Roles</a><br>
													<li><a href="../sigefirrhh/sistema/Opcion.jsp" class="contenidolink">Opciones</a><br>
													<li><a href="../sigefirrhh/sistema/RolOpcion.jsp" class="contenidolink">Asegnar Opciones a Roles</a><br>
													<li><a href="../sigefirrhh/sistema/UsuarioRol.jsp" class="contenidolink">Asignar Roles a Usuarios</a><br>
												</p><!---->
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		&nbsp;
	</body>
</html>