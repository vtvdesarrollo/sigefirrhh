<%! String homePatch = "../../"; %>	
<html>
	<head>
		<title>Untitled Document</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">		
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>
		
		<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td>
					<!-- Titulo -->
					<table width="100%" class="toptable">
						<tr>
							<td align="left">
								<b>
									Acciones</b>
							</td>
						</tr>
					</table>
					<!----------->
					
					<table width="100%" class="toptable">
						<tr>
							<td>
							
								<!--Titulo Planificacion y Desarrollo-->
								<table width="100%" class="bandtable">
									<tr>
										<td align="left">Agregar, Modificar, Eliminar y Consultar</td>
									</tr>
								</table>
								<p></p>
								<!---------->
								
								<!--Titulo Estructura Geografica-->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Agregar</td>
									</tr>
								</table>
								<!---------->
								
								<!--Contenido-->
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p>Para agregar un registro haga click sobre la imagen superior derecha <img src="../../images/add.gif" width="84" height="25"></p>
												<p>Inmediatamente se mostrar&aacute; la pantalla para agregar un registro.<br>
											  </p>
										  </div>
										</td>
								  </tr>
								</table>
								<!---------->
								
								<!--Titulo Estructura Administrativa-->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Modificar</td>
									</tr>
								</table>
								<!---------->
								
								<!--Contenido-->
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p>Para modificar un registro, primero escriba el criterio de b&uacute;squeda en la pantalla inicial de b&uacute;squeda. Inmediatamente se mostrar&aacute;n los registros encontrados, haga click sobre el registro que se desea modificar e inmediatamente se mostrar&aacute; el registro. Haga click sobre la imagen <img src="../../images/modify.gif" width="93" height="25">y el registr&oacute; se mostrar&aacute; para la edici&oacute;n. Cuando termine de hacer los cambio haga click en <img src="../../images/save.gif" width="85" height="25">para grabar, o haga click en <img src="../../images/cancel.gif" width="90" height="25"> para deshacer.<br>
										      </p>
										  </div>
										</td>
								  </tr>
								</table>
								<!---------->
								
								<!--Titulo Estructura de Cargos-->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Eliminar</td>
									</tr>
								</table>
								<!---------->
								
								<!--Contenido-->
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p>Para eliminar un registro, primero escriba el criterio de b&uacute;squeda en la pantalla inicial de b&uacute;squeda. Inmediatamente se mostrar&aacute;n los registros encontrados, haga click sobre el registro que se desea eliminar e inmediatamente se mostrar&aacute; el registro. Haga click sobre la imagen <img src="../../images/delete.gif" width="86" height="25">y el sistema le preguntar&aacute; &quot;si esta seguro que desea eliminar el registro&quot;, haga click sobre <img src="../../images/yes.gif" width="50" height="25"> para borrar, o haga click sobre <img src="../../images/cancel.gif" width="90" height="25"> para cancelar. </p>
										  </div>
										</td>
								  </tr>
								</table>
								<!---------->
								
								<!--Titulo Estructura de Cargos Docentes (MED)-->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Consultar </td>
									</tr>
								</table>
								<!---------->
								
								<!--Contenido-->
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
											  <p>Para consultar un registro, primero escriba el criterio de b&uacute;squeda en la pantalla inicial de b&uacute;squeda. Inmediatamente se mostrar&aacute;n los registros encontrados, haga click sobre el registro que se desea consultar e inmediatamente se mostrar&aacute; el registro. </p>
										  </div>
										</td>
								  </tr>
								</table>
								<!---------->
								
							</td>
						</tr>
					</table>
					<table width="100%" class="toptable">
						<tr>
							<td>
							
								<!--Titulo Planificacion y Desarrollo-->
								<table width="100%" class="bandtable">
									<tr>
										<td align="left">Agregar, Modificar, Eliminar y Consultar Registros de Expediente del Trabajador </td>
									</tr>
								</table>
								<p></p>
								<!---------->
								
								<!--Titulo Estructura Geografica-->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Agregar</td>
									</tr>
								</table>
								<!---------->
								
								<!--Contenido-->
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p>Para agregar un registro de expediente al trabajador:</p>
												<p>Haga click sobre la imagen <img src="../../images/buttons/add.gif" width="84" height="25">. Luego busque el trabajador al que desea agregarle la informaci&oacute;n. Esta b&uacute;squeda se puede realizar por:<br> 
											    C&eacute;dula<br>
											    Nombres<br>
											    Apellidos
</p>
												<p><img src="../../images/help/searh.gif" width="566" height="266"> </p>
												<p>Una vez que el sistema muestre la b&uacute;squeda, haga click sobre el trabajador al que desea agregarle la informaci&oacute;n. Inmediatamente el sistema le mostrar&aacute; la pantalla para agregar el registro. <br>
											  </p>
											</div>
										</td>
								  </tr>
								</table>
								<!---------->
								
								<!--Titulo Estructura Administrativa-->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Modificar, Eliminar y Consultar</td>
									</tr>
								</table>
								<!---------->
								
								<!--Contenido-->
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<p>Para modificar, eliminar o consultar un registro de expediente del trabajador</p>
												<p><img src="../../images/help/searh2.gif" width="585" height="286"></p>
												<p>Busque primero el trabajador, ya sea por los nombres, apellidos o la c&eacute;dula, una vez que el sistema muestre los trabajadores resultantes de la b&uacute;squeda, haga click sobre el trabajador e inmediatamente el sistema mostrar&aacute; todos los registros para ese trabajador, haga click sobre el registro deseado e inmediatamente el sistema le mostrar&aacute; la pantalla de consulta del registro. Luego para modificar haga click sobre <img src="../../images/buttons/modify.gif" width="96" height="25">o para eliminar haga click sobre <img src="../../images/buttons/delete.gif" width="86" height="25"><br>
									          </p>
										  </div>
										</td>
								  </tr>
								</table>
								<!---------->
								
								
								
								
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
