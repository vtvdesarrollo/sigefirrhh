<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Creaci&oacute;n de Usuarios </strong></td>
								</tr>
							</table>
							<!----------->
								
							<!--Indice-->
							<table width="100%" class="datatable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/CreacionDeUsuario.gif" width="565" height="323">
									</td>
								</tr>
							</table>
							<!----------->
								
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<div align="justify">
											<p>Para crear un usuario introduzca el nombre como sera conocido el usuario, la contrase&ntilde;a, la c&eacute;dula de identidad, los apellidos y nombres, y especifique si es, o no, un usuario administrador. El usuario administrador es aquel tiene acceso a todas las opciones del sistema sin restricciones, es decir que le est&aacute; permitido Consultar, Agregar, Modificar, Eliminar y Ejecutar Procesos, en todas las opciones de SIGEFIRRHH: </p>
									  	</div>
									  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Pasos Previos-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Pasos Previos</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>&nbsp;</td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Ruta en SIGEFIRRHH-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Ruta en SIGEFIRRHH</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/CreacionDeUsuario-ruta-1.gif" width="209" height="192">
									</td>
								</tr>
							</table>
							<!----------->
								
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>