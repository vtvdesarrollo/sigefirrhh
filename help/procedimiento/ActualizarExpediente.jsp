<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Actualizar un Expediente</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/ConsultarExpediente-1.gif" width="565" height="173">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
								  <p>Para Agregar, Modificar o Eliminar alguno de los componentes del Expediente de un trabajador, primero introduzca el criterio de B&uacute;squeda para ubicar al trabajador y pulse el boton <strong>Buscar.</strong> </p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/ConsultarExpediente-2.gif" width="565" height="271">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Haga Clic sobre el trabajador de su inter&eacute;s y a continuaci&oacute;n se mostrar&aacute; una lista con todos elementos relacionados a ese trabajador </p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/ConsultarExpediente-3.gif" width="564" height="374">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
								  <p>Si desea modificar o eliminar un registro de la lista haga Clic sobre el registro de inter&eacute;s.<br>
								    Si desea agregar un nuevo registro pulse el bot&oacute;n <strong>Agregar</strong> . </p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarExpediente-4.gif" width="564" height="226">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
								  <p>Para modificar alguno de los campos pulse el bot&oacute;n <strong>Modificar,</strong> para elimininar el registro pulse el bot&oacute;n<strong> Eliminar. </strong>Si desea salir de esta opci&oacute;n pulse el bot&oacute;n<strong> Cancelar. </strong></p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/ActualizarExpediente-1.gif" width="564" height="226">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
								  <td><p>Realice los cambios requeridos y pulse el bot&oacute;n<strong> Guardar. </strong>Si no se desea guardar las modificaciones realizadas pulse el bot&oacute;n <strong>Cancelar</strong></p>							      </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/ActualizarExpediente-2.gif" width="564" height="236">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
								  <td> Confirme la eliminaci&oacute;n pulsando el bot&oacute;n <strong>Si</strong>, en caso contrario pulse el bot&oacute;n <strong>Cancelar</strong></td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/ActualizarExpediente-4.gif" width="564" height="227">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Introduzca los campos correspondientes al nuevo registro y pulse el bot&oacute;n <strong>Guardar</strong>. Si desea salir de la opci&oacute;n sin agrgar un nuevo registro pulse el bot&oacute;n <strong>Cancelar</strong></p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ActualizarExpediente-3.gif" width="564" height="267">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Una vez agregado, modificado o eliminado el registro, se regresa a la pantalla inicial notificandose en la parte superior el resultado de la acci&oacute;n.<br>
									    Para ver la  lista de detalle con las nuevas actualizaciones haga Clic sobre el nombre del trabajador.</p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Pasos Previos-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Pasos Previos</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>&nbsp;
										
									</td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Ruta ebn SIGEFIRRHH-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Ruta en SIGEFIRRHH </a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/ConsultarExpediente-ruta-1.gif" width="209" height="215">
									</td>
								</tr>
							</table>
							<!----------->
							
                        </td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>