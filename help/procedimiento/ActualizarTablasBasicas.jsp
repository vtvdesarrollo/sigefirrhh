<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
    </head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Actualizar Tablas B&aacute;sicas </strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarTablasBasica-1.gif" width="564" height="171">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Si se desea agregar un nuevo registro pulse el bot&oacute;n <strong>Agregar.<br>
										</strong>Si se desea Actualizar o Modificar se introduce el criterio de B&uacute;squeda y pulse el boton <strong>Buscar.</strong> </p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarTablasBasica-2.gif" width="564" height="311">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Haciendo Clic sobre el registro, seleccione de la lista el registro que  desea actualizar o eliminar.</p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarTablasBasica-3.gif" width="564" height="316">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>Para modificar alguno de los campos pulse el bot&oacute;n <strong>Modificar,</strong> para elimininar el registro pulse el bot&oacute;n<strong> Eliminar. </strong>Si desea salir de esta opci&oacute;n pulse el bot&oacute;n<strong> Cancelar. </strong></td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/ActualizarTablasBasica-1.gif" width="564" height="287">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
								  <p> Realice los cambios requeridos y pulse el bot&oacute;n<strong> Guardar. </strong>Si no se desea guardar las modificaciones realizadas pulse el bot&oacute;n <strong>Cancelar.</strong></p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/ActualizarTablasBasica-2.gif" width="564" height="287">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
								  <p>Confirme  la eliminaci&oacute;n pulsando el bot&oacute;n <strong>Si</strong>, en caso contrario pulse  el bot&oacute;n <strong>Cancelar.</strong></p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/ActualizarTablasBasica-3.gif" width="564" height="287">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Introduzca los campos correspondientes al nuevo registro y pulse el bot&oacute;n <strong>Guardar</strong>. Si desea salir de la opci&oacute;n pulse el bot&oacute;n <strong>Cancelar.</strong></p>
									</td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ActualizarTablasBasica-4.gif" width="564" height="208">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Una vez agregado, modificado o eliminado el registro, se regresa a la pantalla inicial notificandose en la parte superior el resultado de la acci&oacute;n.<br>
									    Para ver la lista  con las nuevas actualizaciones introduzca el criterio de B&uacute;squeda y pulse el bot&oacute;n <strong>Buscar.</strong></p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Pasos Previos-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Pasos Previos</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>&nbsp;
										
									</td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Ruta en SIGEFIRRHH-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Ruta en SIGEFIRRHH</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ActualizarTablasBasica-ruta.gif" width="209" height="364"> </td>
								</tr>
							</table>
							<!----------->
							
                        </td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>