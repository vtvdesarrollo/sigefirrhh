<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Consultar Tablas B&aacute;sicas </strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarTablasBasica-1.gif" width="564" height="171">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
								  <td><p>Se introduce  el criterio de B&uacute;squeda  y se pulsa el boton Buscar. La consulta a una tabla b&aacute;sica se puede realizar por:<br>
								    C&oacute;digo: Introducir el c&oacute;digo asociado al registro que se desee consultar.<br>
								    Descripci&oacute;n: Introducir la descripci&oacute;n o parte de ella.<br>
								    Como resultado de la b&uacute;squeda se presenta una lista con los registros recuperados (ver siguiente pantalla). La informaci&oacute;n que se muestra  de cada registro corresponde sus datos  mas relevantes. </p>
						          </td>
								</tr>
								<tr>
								  <td>&nbsp;</td>
							  </tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarTablasBasica-2.gif" width="564" height="311">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td height="59">
										<p align="justify">Para Consultar un registro  seleccionelo de la lista  haciendo clic sobre el mismo. </p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarTablasBasica-3.gif" width="564" height="316">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Al seleccionar el registo a consultar,  se muestran todos sus campos.<br>
									    Para salir de esta opci&oacute;n pulse el bot&oacute;n <strong>Cancelar.</strong></p>							      </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Pasos Previos-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Pasos Previos</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>&nbsp;
										
									</td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Ruta en SIGEFIRRHH-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Ruta en SIGEFIRRHH</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarTablasBasica-ruta.gif" width="209" height="364"> </td>
								</tr>
							</table>
							<!----------->
							
                        </td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>