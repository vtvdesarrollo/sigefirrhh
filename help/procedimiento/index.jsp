<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>
		
		<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td>
				<!--****************************************************************************************************
					Titulo
					**************************************************************************************************** -->
					<table width="100%" class="toptable">
						<tr>
							<td align="left">
								<b>Procedimiento</b>
							</td>
						</tr>
					</table>
					
					<table width="100%" class="toptable">
						<tr>
							<td>
							
							<!--****************************************************************************************************
								Servicio Personal
								**************************************************************************************************** -->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Servicio Personal</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<blockquote>
													<li><a href="ServicioPersonal.jsp" class="link">Servicio Personal</a></li>
												</blockquote>
											</div>
										</td>
									</tr>
								</table>
							
							<!--****************************************************************************************************
								Expediente del Trabajador
								**************************************************************************************************** -->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Expediente</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<blockquote>
													<li><a href="ConsultarExpediente.jsp" class="link">Consultar un Expediente</a></li>
													<li><a href="ActualizarExpediente.jsp" class="link">Actualizar un Expediente</a></li>
													<li><a href="GenerarHojaDeVida.jsp" class="link">Generar Hoja de Vida</a></li>
												</blockquote>
											</div>
										</td>
									</tr>
								</table>
								
							<!--****************************************************************************************************
								Tablas B�sicas Expediente
								**************************************************************************************************** -->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Tablas B�sicas Expediente</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<blockquote>
													<li><a href="ConsultarTablasBasicas.jsp" class="link">Consultar Tablas B�sicas</a></li>
													<li><a href="ActualizarTablasBasicas.jsp" class="link">Actualizar Tablas B�sicas</a></li>
												</blockquote>
											</div>
										</td>
									</tr>
								</table>
								
							<!--****************************************************************************************************
								Administraci�n del Sistema - Usuarios
								**************************************************************************************************** -->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Administraci�n del Sistema - Usuarios</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<blockquote>
													<li><a href="IngresarAlSistema.jsp" class="link">Ingreso al Sistema</a></li>
													<li><a href="CreacionDeUsuario.jsp" class="link">Creaci�n de Usuario</a></li>
													<li><a href="DefinicionDeRoles.jsp" class="link">Definici�n de Roles</a></li>
													<li><a href="AsignarPermisologia.jsp" class="link">Asignar Permisolog�a</a></li>
												</blockquote>
											</div>
										</td>
									</tr>
								</table>
							
							<!--****************************************************************************************************
								Movimientos de Personal
								**************************************************************************************************** -->
								<table width="90%" class="bandtable" align="center">
									<tr>
										<td align="left">Movimientos de Personal</td>
									</tr>
								</table>
								
								<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="datatable">
									<tr>
										<td>
											<div align="left">
												<blockquote>
													<li><a href="IngresoTrabajadorLefp.jsp" class="link">Ingreso de un Trabajador sujeto a LEFP</a></li>
												</blockquote>
											</div>
										</td>
									</tr>
								</table>
								
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>