<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
    </head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Consultar un Expediente</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarExpediente-1.gif" width="565" height="173">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Para Consultar alguno de los componentes del Expediente de un trabajador, primero introduzca el criterio de b&uacute;squeda para ubicar al trabajador y pulse el boton <strong>Buscar.<br>
										</strong>La consulta se puede realizar por:<br>
										C&eacute;dula: introduzca la cedula de identidad del trabajador<br>
								  Nombres y Apellidos: introduzca los nombres y/o apellidos, o parte de ellos. </p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarExpediente-2.gif" width="565" height="271">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
								  <p>Haga Clic sobre el trabajador de su inter&eacute;s y a continuaci&oacute;n se mostrar&aacute; una lista con todos elementos relacionados a ese trabajador.  La informaci&oacute;n que se muestra de cada registro corresponde sus datos mas relevantes. </p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarExpediente-3.gif" width="564" height="374">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Para consultar un registro de la lista haga Clic sobre el registro de inter&eacute;s.</p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarExpediente-4.gif" width="564" height="226">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>El sistema mostrar&aacute; todos los campos relacionados al registro seleccionado. Para salir de la opci&oacute;n pulse el bot&oacute;n <strong>Cancelar. </strong></p>
									</td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Pasos Previos-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Pasos Previos</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>&nbsp;
										
									</td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Ruta ebn SIGEFIRRHH-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Ruta en SIGEFIRRHH </a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ConsultarExpediente-ruta-1.gif" width="209" height="215">
									</td>
								</tr>
							</table>
							<!----------->
                        </td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>