<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
    </head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Servicio Personal</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ServicioPersonal-1.gif" width="564" height="169">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
								  <p>Si ya es un Usuario registrado para ingresar al Sistema coloque el nombre de usuarioy la contrase&ntilde;a, pulse el bot&oacute;n <strong>IniciarSesi&oacute;n</strong>.<br>
							      Si desea registrarse por primera vez,  pulse el bot&oacute;n <strong>Registrarse </strong>y se mostrar&aacute; la pantalla a continuaci&oacute;n. </p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ServicioPersonal-2.gif" width="564" height="248">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Introduzca los datos que se le solicitan: c&eacute;dula de identidad, a&ntilde;o de ingreso al organismo, fecha de nacimiento y una palabra hasta de 20 caracteres (letras, n&uacute;mero, caracteres especiales) que ser&aacute; utilizada como contrase&ntilde;a. Reescriba su contrase&ntilde;a para seguridad. Pulse el bot&oacute;n <strong>Registrar</strong>. </p>
								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Pasos Previos-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Pasos Previos</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>&nbsp;
										
									</td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Ruta en SIGEFIRRHH-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Ruta en SIGEFIRRHH</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td align="center"><img src="../../images/help/procedimiento/ServicioPersonal-ruta.gif" width="87" height="87"> </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Error-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Mensajes de error</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td align="center">
									  <p><img src="../../images/help/procedimiento/ServicioPersonal-error.gif" width="564" height="280"> </p>
									</td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td> <p>Si el usuario o la contrase&ntilde;a no coinciden con la informaci&oacute;n registrada,  se muestra  un mensaje de error; si coinciden, se le permitir&aacute;  la entrada al sistema.<br>
  Cuando el usuario se registra por primera vez, el sistema verificar&aacute;  los datos proporcionados, si son correctos su contrase&ntilde;a quedar&aacute; registrada para futuros accesos, si no, mostrar&aacute; el error correspondiente.</p>								    </td>
								</tr>
							</table>
							<!----------->
							
                        </td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>