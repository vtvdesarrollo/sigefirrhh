<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Ingreso al Sistema</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/IngresarAlSistema.gif" width="565" height="157">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p align="justify">Para ingresar al Sistema coloque el nombre de usuario, la contrase&ntilde;a asignada, 
										y seleccione el Organismo con el cual desea trabajar en el sistema. Pulse el bot&oacute;n IniciarSesi&oacute;n.</p>
								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Pasos Previos-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Pasos Previos</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>1. Crear el usuario 
										</td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Ruta en SIGEFIRRHH-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Ruta en SIGEFIRRHH </a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<div align="center"><img src="../../images/help/procedimiento/IngresarAlSistema-ruta.gif" width="87" height="87"></div></td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Error-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Mensajes de error</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/IngresarAlSistema-error.gif" width="564" height="204">
									</td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										Si el usuario o la contraseña no coinciden con la información registrada, 
										se muestra el siguiente mensaje de error:
									</td>
								</tr>
							</table>
							<!----------->
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>