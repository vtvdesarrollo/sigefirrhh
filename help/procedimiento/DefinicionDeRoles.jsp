<html>
	<head>
		<title>Sigefirrhh - Ayuda</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	</head>

	<body>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center">
					<jsp:include flush="true" page="/help/include/top.jsp" />
				</td>
			</tr>
		</table>

	<table width="640px" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<td width="100%" valign="top">
							
							<!-- Titulo -->
							<table width="100%" class="toptable">
								<tr>
									<td align="left"><strong>Definici&oacute;n de Roles</strong></td>
								</tr>
							</table>
							<!----------->
							
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/DefinicionDeRoles-1.gif" width="565" height="226">
									</td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Para agregar un rol o perfil de usuario, se debe indicar un c&oacute;digo y un nombre o denominaci&oacute;n que se desea asignar al rol. </p>
									</td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Imagen-->
							<table width="100%" class="toptable">
								<tr>
									<td align="center">
                                      <img src="../../images/help/procedimiento/DefinicionDeRoles-2.gif" width="565" height="332"> </td>
								</tr>
							</table>
							<!----------->
							
							<!--Descripccion-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Descripci&oacute;n</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>
										<p>Consiste en la asignaci&oacute;n de permisolog&iacute;a en las diferentes opciones del sistema a los roles ya creados. Para una opci&oacute;n del sistema seleccionada, se debe indicar el tipo de acci&oacute;n que le es permitido a ese rol.  En el caso de los &quot;procesos del sistema&quot;,  el permiso consiste en la autorizaci&oacute;n, o no, para su ejecuci&oacute;n. Cuando un rol no tiene permiso para alg&uacute;n tipo de acci&oacute;n, el bot&oacute;n que corresponde a esa acci&oacute;n no le es mostrado en la pantalla de la opci&oacute;n.</p>								  </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Pasos Previos-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Pasos Previos</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td>1. Registrar todas las opciones del sistema. </td>
								</tr>
							</table>
							<!----------->
							&nbsp;
							<!--Ruta en SIGEFIRRHH-->
							<table width="100%" class="bandtable">
								<tr>
									<td align="left"><a  name="_descripcion">Ruta en SIGEFIRRHH</a></td>
								</tr>
							</table>
							
							<table width="100%" class="datatable">
								<tr>
									<td align="center">
										<img src="../../images/help/procedimiento/DefinicionDeRoles-ruta-1.gif" width="209" height="192">
                                        <img src="../../images/help/procedimiento/DefinicionDeRoles-ruta-2.gif" width="209" height="192"> </td>
								</tr>
							</table>
							<!----------->
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>