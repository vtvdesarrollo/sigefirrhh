<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{rolOpcionForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formRolOpcion">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Asignar Opciones a Roles
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/sistema/RolOpcion.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{rolOpcionForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!rolOpcionForm.editing&&!rolOpcionForm.deleting&&rolOpcionForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchRolOpcion"
								rendered="#{!rolOpcionForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Rol
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{rolOpcionForm.findSelectRol}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{rolOpcionForm.findColRol}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{rolOpcionForm.findRolOpcionByRol}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultRolOpcionByRol"
								rendered="#{rolOpcionForm.showRolOpcionByRol&&
								!rolOpcionForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{rolOpcionForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{rolOpcionForm.selectRolOpcion}"
                                						styleClass="listitem">
                                						<f:param name="idRolOpcion" value="#{result.idRolOpcion}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataRolOpcion"
								rendered="#{rolOpcionForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!rolOpcionForm.editing&&!rolOpcionForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{rolOpcionForm.editing&&!rolOpcionForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{rolOpcionForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{rolOpcionForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="rolOpcionForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{rolOpcionForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!rolOpcionForm.editing&&!rolOpcionForm.deleting&&rolOpcionForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{rolOpcionForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!rolOpcionForm.editing&&!rolOpcionForm.deleting&&rolOpcionForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{rolOpcionForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{rolOpcionForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{rolOpcionForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{rolOpcionForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{rolOpcionForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{rolOpcionForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Rol
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{rolOpcionForm.selectRol}"
														id="rol"
                                                    	disabled="#{!rolOpcionForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Rol">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{rolOpcionForm.colRol}" />
                                                    </h:selectOneMenu>													<h:message for="rol" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Opci�n
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{rolOpcionForm.selectOpcion}"
														id="opcion"
                                                    	disabled="#{!rolOpcionForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Opci�n">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{rolOpcionForm.colOpcion}" />
                                                    </h:selectOneMenu>													<h:message for="opcion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Puede Consultar?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{rolOpcionForm.rolOpcion.consultar}"
	                                                    id="consultar"
                                                    	disabled="#{!rolOpcionForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Puede Consultar?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{rolOpcionForm.listConsultar}" />
                                                    </h:selectOneMenu>													<h:message for="consultar" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Puede Agregar?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{rolOpcionForm.rolOpcion.agregar}"
	                                                    id="agregar"
                                                    	disabled="#{!rolOpcionForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Puede Agregar?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{rolOpcionForm.listAgregar}" />
                                                    </h:selectOneMenu>													<h:message for="agregar" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Puede Modificar?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{rolOpcionForm.rolOpcion.modificar}"
	                                                    id="modificar"
                                                    	disabled="#{!rolOpcionForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Puede Modificar?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{rolOpcionForm.listModificar}" />
                                                    </h:selectOneMenu>													<h:message for="modificar" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Puede Eliminar?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{rolOpcionForm.rolOpcion.eliminar}"
	                                                    id="eliminar"
                                                    	disabled="#{!rolOpcionForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Puede Eliminar?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{rolOpcionForm.listEliminar}" />
                                                    </h:selectOneMenu>													<h:message for="eliminar" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Puede Ejecutar?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{rolOpcionForm.rolOpcion.ejecutar}"
	                                                    id="ejecutar"
                                                    	disabled="#{!rolOpcionForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Puede Ejecutar?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{rolOpcionForm.listEjecutar}" />
                                                    </h:selectOneMenu>													<h:message for="ejecutar" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{rolOpcionForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!rolOpcionForm.editing&&!rolOpcionForm.deleting&&rolOpcionForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{rolOpcionForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!rolOpcionForm.editing&&!rolOpcionForm.deleting&&rolOpcionForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{rolOpcionForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{rolOpcionForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{rolOpcionForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{rolOpcionForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{rolOpcionForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{rolOpcionForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>