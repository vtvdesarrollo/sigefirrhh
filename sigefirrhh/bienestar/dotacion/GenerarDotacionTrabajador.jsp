<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
</head>

<body>
<f:view>
	<x:saveState value="#{generarDotacionTrabajadorForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formGenerarDotacionTrabajador">
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Asignar Dotaciones 
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/bienestar/dotacion/GenerarDotacionTrabajador.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarDotacionTrabajadorForm.selectIdTipoPersonal}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarDotacionTrabajadorForm.changeTipoPersonal}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarDotacionTrabajadorForm.listTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td>
    										Tipo Dotaci�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarDotacionTrabajadorForm.selectIdTipoDotacion}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarDotacionTrabajadorForm.changeTipoDotacion}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarDotacionTrabajadorForm.listTipoDotacion}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    								<tr>
    									<td>
    										Subtipo Dotaci�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarDotacionTrabajadorForm.idSubtipoDotacion}"
    											rendered="#{generarDotacionTrabajadorForm.showSubtipoDotacion}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Todos" itemValue="0" />
                                                <f:selectItems value="#{generarDotacionTrabajadorForm.listSubtipoDotacion}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			    														    																
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"	    										
    											action="#{generarDotacionTrabajadorForm.ejecutar}"
    											/>
    									<td>
    								</tr>    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>