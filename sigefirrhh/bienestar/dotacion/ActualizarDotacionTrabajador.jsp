<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
	<script language=javascript> 
		function firstFocus2(){
				var paso = "false";			
				for (i = 0; i < document.forms.length; i++){
					for (j = 0; j < document.forms[i].elements.length; j++){					
						if (document.forms[i].elements[j].type =="text"){																				
							document.forms[i].elements[j].focus();					
							break;
						}
					}				
					if (paso =="true"){
						break;
					}
				}
				
			}	
	</script>
		
</head>

<body onload="firstFocus2();">
<f:view>
	<x:saveState value="#{actualizarDotacionTrabajadorForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200"  >
	                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formActualizarDotacionTrabajador" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Actualizaci�n de Tallas
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/bienestar/dotacion/ActualizarDotacionTrabajador.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
            					image="/images/close.gif"
            					action="go_cancelOption"
            					immediate="true"                        					
            					onclick="javascript:return clickMade()"
    	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">   
        										    																	
        								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td>
    										<h:selectOneMenu value="#{actualizarDotacionTrabajadorForm.selectIdTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{actualizarDotacionTrabajadorForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{actualizarDotacionTrabajadorForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Tipo Dotaci�n
    									</td>
    									<td>
    										<h:selectOneMenu
    											value="#{actualizarDotacionTrabajadorForm.selectIdTipoDotacion}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"
												rendered="#{actualizarDotacionTrabajadorForm.showTipoDotacion}"
												valueChangeListener="#{actualizarDotacionTrabajadorForm.changeTipoDotacion}">
												 <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{actualizarDotacionTrabajadorForm.listTipoDotacion}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Subtipo Dotaci�n
    									</td>
    									<td>
    										<h:selectOneMenu
    											value="#{actualizarDotacionTrabajadorForm.idSubtipoDotacion}"                                            	
												rendered="#{actualizarDotacionTrabajadorForm.showSubtipoDotacion}">
                                                <f:selectItems value="#{actualizarDotacionTrabajadorForm.listSubtipoDotacion}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								    								
    								
        								<f:verbatim>
        								<tr>
        									<td colspan="2">
        										<table width="100%" class="datatable">
        											<tr>
        												<td>
        													C�dula
        												</td>
        												<td>
        													Talla
        												</td>
    													
        											</tr>
        											<tr>
        												<td></f:verbatim>
    						                        		<h:inputText
    		                        							size="8"
    		                        							id="Cedula"
    		                        							maxlength="8"
    	                        								value="#{actualizarDotacionTrabajadorForm.cedula}"   						
    															onfocus="return deleteZero(event, this)"
    															onblur="javascript:fieldEmpty(this)"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyIntegerCheck(event, this)" />
    													<f:verbatim>		
        												</td>
        												
    													<td>
    													</f:verbatim>
    						                        		<h:inputText
    		                        							size="4"
    		                        							id="Talla"
    		                        							maxlength="4"
    	                        								value="#{actualizarDotacionTrabajadorForm.talla}"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															/>
    			    									<f:verbatim>	
    													</td>	
    													<td>
    													</f:verbatim>
    														<h:commandButton value="Agregar"
    			    											image="/images/modify.gif"
        														action="#{actualizarDotacionTrabajadorForm.actualizar}" />
        												<f:verbatim>	
    													</td>
    													
        											</tr>
        											<tr>
        												<td>
        												</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{actualizarDotacionTrabajadorForm.cedulaFinal}" />
    	                        							&nbsp;
    	                        							<h:outputText
    	                        								value="#{actualizarDotacionTrabajadorForm.nombre}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{actualizarDotacionTrabajadorForm.apellido}" />
    	                        							&nbsp;	
    						                        		<h:outputText
    	                        								value="#{actualizarDotacionTrabajadorForm.talla}" />
    	                        							&nbsp;
    	                        						<f:verbatim>
        												</td>
        												
        											</tr>
        										</table>
        									</td>
        									</f:verbatim>
    																
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>