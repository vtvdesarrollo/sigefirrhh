<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{becaFamiliarForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Menï¿½ Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />

				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y Bï¿½squeda -->
						<td valign="top">
							<h:form id="formBecaFamiliar">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Solicitudes de Becas
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/bienestar/beca/BecaFamiliar.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success"  />
										</td>
									</tr>
									
									<f:subview 
										id="searchBecaFamiliar"
										rendered="#{!becaFamiliarForm.showData&&
										!becaFamiliarForm.showAdd&&
										!becaFamiliarForm.login.servicioPersonal}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{becaFamiliarForm.findPersonalCedula}"
																onkeypress="javascript:return keyIntegerCheck(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{becaFamiliarForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{becaFamiliarForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{becaFamiliarForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{becaFamiliarForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{becaFamiliarForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{becaFamiliarForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{becaFamiliarForm.showResultPersonal&&
									!becaFamiliarForm.showData&&
									!becaFamiliarForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <x:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{becaFamiliarForm.resultPersonal}"
    	                                            preserveDataModel="false"
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{becaFamiliarForm.findBecaFamiliarByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </x:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif" />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif" />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{becaFamiliarForm.selectedPersonal&&
									!becaFamiliarForm.showData&&
									!becaFamiliarForm.editing&&
									!becaFamiliarForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{becaFamiliarForm.personal}   <-- clic aqui"
    	                            						action="#{becaFamiliarForm.findBecaFamiliarByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{becaFamiliarForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{becaFamiliarForm.add}"
            													rendered="#{!becaFamiliarForm.editing&&!becaFamiliarForm.deleting&&becaFamiliarForm.login.agregar}" />

                                        	    			<% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{becaFamiliarForm.abort}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                                                    	    <%}%>
                                                    	   <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultBecaFamiliar"
									rendered="#{becaFamiliarForm.showResult&&
									!becaFamiliarForm.showData&&
									!becaFamiliarForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <x:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{becaFamiliarForm.result}"
                                            	    preserveDataModel="false"
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{becaFamiliarForm.selectBecaFamiliar}"
	                                						styleClass="listitem">
    		                            						<f:param name="idBecaFamiliar" value="#{result.idBecaFamiliar}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </x:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataBecaFamiliar"
								rendered="#{becaFamiliarForm.showData||becaFamiliarForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!becaFamiliarForm.editing&&!becaFamiliarForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{becaFamiliarForm.editing&&!becaFamiliarForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{becaFamiliarForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{becaFamiliarForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="becaFamiliarForm">
	    	        							<f:subview 
    	    	    								id="dataBecaFamiliar"
        	    									rendered="#{becaFamiliarForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{becaFamiliarForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!becaFamiliarForm.editing&&!becaFamiliarForm.deleting&&becaFamiliarForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{becaFamiliarForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!becaFamiliarForm.editing&&!becaFamiliarForm.deleting&&becaFamiliarForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{becaFamiliarForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{becaFamiliarForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{becaFamiliarForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{becaFamiliarForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{becaFamiliarForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{becaFamiliarForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{becaFamiliarForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Familiar
	            	            							</td>
		                        							<td></f:verbatim>
									    		                <h:selectOneMenu value="#{becaFamiliarForm.selectFamiliar}"
        		                                                	disabled="#{!becaFamiliarForm.editing}"
            		                                            	immediate="false"
				                        							id="familiar"
                		            								required="false"
    																title="Familiar">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{becaFamiliarForm.colFamiliar}" />
								                                </h:selectOneMenu>																									<h:message for="familiar" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Nivel Educativo
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{becaFamiliarForm.becaFamiliar.nivelEducativo}"
        	                                        	        	disabled="#{!becaFamiliarForm.editing}"
            	                                        	    	immediate="false"
				                        							id="nivelEducativo"
                	            									                    	        										                        	                                	required="false"
    	                        	                            	title="Nivel Educativo">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{becaFamiliarForm.listNivelEducativo}" />
					    	            	                        	            </h:selectOneMenu>																				<h:message for="nivelEducativo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText value="Monto Beca" rendered="#{!becaFamiliarForm.adding}" />
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{becaFamiliarForm.becaFamiliar.montoBeca}"
            	    		        								rendered="#{!becaFamiliarForm.adding}"
                	    		    								                    	    										readonly="#{!becaFamiliarForm.editing}"
                        											id="montoBeca"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message rendered="#{!becaFamiliarForm.adding}" for="montoBeca" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Grado que cursa
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{becaFamiliarForm.becaFamiliar.grado}"
                	    		    								                    	    										readonly="#{!becaFamiliarForm.editing}"
                        											id="grado"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="grado" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Promedio de Notas
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{becaFamiliarForm.becaFamiliar.promedioNotas}"
                	    		    								                    	    										readonly="#{!becaFamiliarForm.editing}"
                        											id="promedioNotas"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="promedioNotas" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText value="Estatus" rendered="#{!becaFamiliarForm.adding}" />
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                    <h:selectOneMenu value="#{becaFamiliarForm.becaFamiliar.estatusBeca}"
        	                                        	        	disabled="#{!becaFamiliarForm.editing}"
            	                                        	    	immediate="false"
				                        							id="estatusBeca"
				                        							rendered="#{!becaFamiliarForm.adding}"
                           											required="false"
    	                        	                            	title="Estatus">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{becaFamiliarForm.listEstatusBeca}" />
					    	            	                 </h:selectOneMenu>																				<h:message rendered="#{!becaFamiliarForm.adding}" for="estatusBeca" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
   																Instituto
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"

            	    		        								value="#{becaFamiliarForm.becaFamiliar.instituto}"
                	    		    								                    	    										readonly="#{!becaFamiliarForm.editing}"
                        											id="instituto"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="instituto" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
													
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Carrera/Especialidad/Menci�n
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{becaFamiliarForm.becaFamiliar.carreraEspecialidad}"
                	    		    								                    	    										readonly="#{!becaFamiliarForm.editing}"
                        											id="carreraEspecialidad"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="carreraEspecialidad" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Tiene otra beca
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	<h:selectOneMenu value="#{becaFamiliarForm.becaFamiliar.otraBeca}"
        	                                        	        	disabled="#{!becaFamiliarForm.editing}"
            	                                        	    	immediate="false"
				                        							id="otraBeca"
                	            									onchange="this.form.submit()"              	        										                        	                                	required="false"
    	                        	                            	title="Tiene otra beca">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{becaFamiliarForm.listOtraBeca}" />
					    	            	                        	            </h:selectOneMenu>																				<h:message for="otraBeca" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Monto Otra Beca
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{becaFamiliarForm.becaFamiliar.montoOtraBeca}"
            	    		        								rendered="#{becaFamiliarForm.showMontoOtraBeca}"
                	    		    								                    	    										readonly="#{!becaFamiliarForm.editing}"
                        											id="montoOtraBeca"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="montoOtraBeca" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    						<f:verbatim><tr>
    	                    								<td>
        	                									Instituto Otra Beca
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{becaFamiliarForm.becaFamiliar.institutoOtraBeca}"
                	    		    								rendered="#{becaFamiliarForm.showMontoOtraBeca}"
                	    		    								                    	    										readonly="#{!becaFamiliarForm.editing}"
                        											id="institutoOtraBeca"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="institutoOtraBeca" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText value="Pagada" rendered="#{!becaFamiliarForm.adding}" />
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{becaFamiliarForm.becaFamiliar.pagada}"
        	                                        	        	disabled="#{!becaFamiliarForm.editing}"
            	                                        	    	immediate="false"
				                        							id="pagada"
                	            									                    	        										                        	                                	required="false"
                	            									                    	        										                        	                                	rendered="#{!becaFamiliarForm.adding}"
    	                        	                            	title="Pagada">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{becaFamiliarForm.listPagada}" />
					    	            	                        	            </h:selectOneMenu>																				<h:message for="pagada" styleClass="error" rendered="#{!becaFamiliarForm.adding}"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText value="Fecha Ultimo Pago" rendered="#{!becaFamiliarForm.adding}" />
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
			                        								rendered="#{!becaFamiliarForm.adding}"
	    		                    								maxlength="10"
	        		                								id="fechaUltimoPago"
                		        									value="#{becaFamiliarForm.becaFamiliar.fechaUltimoPago}"
                    		    									readonly="#{!becaFamiliarForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																		<f:convertDateTime timeZone="#{dateTime.timeZone}" pattern="dd-MM-yyyy" />
                    		    									</h:inputText><h:outputText value="(dd-mm-aaaa)" rendered="#{!becaFamiliarForm.adding}" />
																	<h:message for="fechaUltimoPago" styleClass="error" rendered="#{!becaFamiliarForm.adding}" />
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Trajo Recaudos
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                       <h:selectOneMenu value="#{becaFamiliarForm.becaFamiliar.trajoRecaudos}"
        	                                        	        	disabled="#{!becaFamiliarForm.editing}"
            	                                        	    	immediate="false"
				                        							id="trajoRecaudos"
                     	                                			required="false"
    	                        	                            	title="Trajo Recaudos">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{becaFamiliarForm.listTrajoRecaudos}" />
					    	            	                   </h:selectOneMenu>																				<h:message for="trajoRecaudos" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText value="Tramite" rendered="#{!becaFamiliarForm.adding}" />
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                    <h:selectOneMenu value="#{becaFamiliarForm.becaFamiliar.tramiteBeca}"
        	                                        	        	disabled="#{!becaFamiliarForm.editing}"
            	                                        	    	immediate="false"
				                        							id="tramiteBeca"
				                        							rendered="#{!becaFamiliarForm.adding}"
                           											required="false"
    	                        	                            	title="Tramite">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{becaFamiliarForm.listTramiteBeca}" />
					    	            	                 </h:selectOneMenu>																				<h:message rendered="#{!becaFamiliarForm.adding}" for="estatusBeca" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
													<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsBecaFamiliar"
            										rendered="#{becaFamiliarForm.showData}">
		                        					<f:verbatim><table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{becaFamiliarForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!becaFamiliarForm.editing&&!becaFamiliarForm.deleting&&becaFamiliarForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{becaFamiliarForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!becaFamiliarForm.editing&&!becaFamiliarForm.deleting&&becaFamiliarForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{becaFamiliarForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{becaFamiliarForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{becaFamiliarForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{becaFamiliarForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{becaFamiliarForm.editing}" />
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{becaFamiliarForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>