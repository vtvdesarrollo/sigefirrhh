<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{generarPrestamoPolizaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formAsignarJuguetes">
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Registrar Pr�stamos por Financiamiento
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/bienestar/poliza/GenerarPrestamoPoliza.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarPrestamoPolizaForm.selectTipoPersonal}"    										
	    										valueChangeListener="#{generarPrestamoPolizaForm.changeTipoPersonal}"
	    										onchange="this.form.submit()"         
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarPrestamoPolizaForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    	
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarPrestamoPolizaForm.selectConceptoTipoPersonal}"
	    										rendered="#{generarPrestamoPolizaForm.showConcepto}"
	    										valueChangeListener="#{generarPrestamoPolizaForm.changeConceptoTipoPersonal}"
	    										onchange="this.form.submit()"                                              	
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarPrestamoPolizaForm.listConceptoTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>    					
    								<tr>
    									<td>
    										Poliza
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarPrestamoPolizaForm.selectPoliza}"
	    										onchange="this.form.submit()" 
												valueChangeListener="#{generarPrestamoPolizaForm.changePoliza}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarPrestamoPolizaForm.listPoliza}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    				
    								<tr>
    									<td>
    										Plan Poliza
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarPrestamoPolizaForm.idPlanPoliza}"
    											rendered="#{generarPrestamoPolizaForm.showPlanPoliza}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarPrestamoPolizaForm.listPlanPoliza}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>        								
									<tr>
    									<td>
    										Tipo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarPrestamoPolizaForm.tipo}"	    										
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Titulares" itemValue="T" />
                                            	<f:selectItem itemLabel="Beneficiarios" itemValue="B" />
                                            	<f:selectItem itemLabel="Todos" itemValue="A" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										N�mero de Cuotas
    									</td>
    									<td width="100%">
    									<h:inputText
											size="3"
											maxlength="2"
											value="#{generarPrestamoPolizaForm.numeroCuotas}"											
											onkeypress="return keyIntegerCheck(event, this)"
											required="false" />   
    									</td>
    								</tr>			
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
    											action="#{generarPrestamoPolizaForm.execute}"
    											/>
    									<td>
    								</tr>  	  								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>