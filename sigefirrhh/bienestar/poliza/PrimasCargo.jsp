<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{primasCargoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formPrimasCargo">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Primas por Cargos
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/bienestar/poliza/PrimasCargo.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{primasCargoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!primasCargoForm.editing&&!primasCargoForm.deleting&&primasCargoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchPrimasCargo"
								rendered="#{!primasCargoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
						                        					<f:verbatim><tr>
                        						<td>
                        							Poliza
                        						</td>
                        						<td></f:verbatim>
				                                                    <h:selectOneMenu value="#{primasCargoForm.findSelectPolizaForPrimasPlan}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{primasCargoForm.findChangePolizaForPrimasPlan}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{primasCargoForm.findColPolizaForPrimasPlan}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
			                        					<f:verbatim><tr>
                        						<td>
                        							Plan Poliza
                        						</td>
                        						<td></f:verbatim>
																	<h:selectOneMenu value="#{primasCargoForm.findSelectPlanPolizaForPrimasPlan}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{primasCargoForm.findChangePlanPolizaForPrimasPlan}"
														rendered="#{primasCargoForm.findShowPlanPolizaForPrimasPlan}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{primasCargoForm.findColPlanPolizaForPrimasPlan}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
																<f:verbatim><tr>
												<td>
													Prima
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{primasCargoForm.findSelectPrimasPlan}"
                                                    	rendered="#{primasCargoForm.findShowPrimasPlan}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{primasCargoForm.findColPrimasPlan}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{primasCargoForm.findPrimasCargoByPrimasPlan}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultPrimasCargoByPrimasPlan"
								rendered="#{primasCargoForm.showPrimasCargoByPrimasPlan&&
								!primasCargoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{primasCargoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{primasCargoForm.selectPrimasCargo}"
                                						styleClass="listitem">
                                						<f:param name="idPrimasCargo" value="#{result.idPrimasCargo}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataPrimasCargo"
								rendered="#{primasCargoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!primasCargoForm.editing&&!primasCargoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{primasCargoForm.editing&&!primasCargoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{primasCargoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{primasCargoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="primasCargoForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{primasCargoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!primasCargoForm.editing&&!primasCargoForm.deleting&&primasCargoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{primasCargoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!primasCargoForm.editing&&!primasCargoForm.deleting&&primasCargoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{primasCargoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{primasCargoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{primasCargoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{primasCargoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{primasCargoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{primasCargoForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Manual Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{primasCargoForm.selectManualCargoForCargo}"
                                                    	disabled="#{!primasCargoForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="ManualCargoForCargo"
														valueChangeListener="#{primasCargoForm.changeManualCargoForCargo}"
                        																						immediate="false"
														required="true"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{primasCargoForm.colManualCargoForCargo}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{primasCargoForm.selectCargo}"
	                                                    id="cargo"
                                                    	disabled="#{!primasCargoForm.editing}"
                                                    	rendered="#{primasCargoForm.showCargo
														 }"
                                                    	immediate="false"
                                                    	required="true"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{primasCargoForm.colCargo}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="cargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Poliza
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{primasCargoForm.selectPolizaForPrimasPlan}"
                                                    	disabled="#{!primasCargoForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="PolizaForPrimasPlan"
														valueChangeListener="#{primasCargoForm.changePolizaForPrimasPlan}"
                        																						immediate="false"
														required="true"
														title="Prima">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{primasCargoForm.colPolizaForPrimasPlan}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Plan Poliza
                        						</td>
                        						<td></f:verbatim>
													<h:selectOneMenu value="#{primasCargoForm.selectPlanPolizaForPrimasPlan}"
                                                    	disabled="#{!primasCargoForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="PlanPolizaForPrimasPlan"
														valueChangeListener="#{primasCargoForm.changePlanPolizaForPrimasPlan}"
														rendered="#{primasCargoForm.showPlanPolizaForPrimasPlan
														 }"
														immediate="false"
														required="true"
														title="Prima">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{primasCargoForm.colPlanPolizaForPrimasPlan}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Prima
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{primasCargoForm.selectPrimasPlan}"
	                                                    id="primasPlan"
                                                    	disabled="#{!primasCargoForm.editing}"
                                                    	rendered="#{primasCargoForm.showPrimasPlan
														 }"
                                                    	immediate="false"
                                                    	required="true"
														title="Prima">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{primasCargoForm.colPrimasPlan}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="primasPlan" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							% patr�n
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="porcentajePatron"
	                        							maxlength="10"
                        								value="#{primasCargoForm.primasCargo.porcentajePatron}"
                        								readonly="#{!primasCargoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="porcentajePatron" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							% trabajador
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="porcentajeTrabajador"
	                        							maxlength="10"
                        								value="#{primasCargoForm.primasCargo.porcentajeTrabajador}"
                        								readonly="#{!primasCargoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="porcentajeTrabajador" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{primasCargoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!primasCargoForm.editing&&!primasCargoForm.deleting&&primasCargoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{primasCargoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!primasCargoForm.editing&&!primasCargoForm.deleting&&primasCargoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{primasCargoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{primasCargoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{primasCargoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{primasCargoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{primasCargoForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{primasCargoForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>