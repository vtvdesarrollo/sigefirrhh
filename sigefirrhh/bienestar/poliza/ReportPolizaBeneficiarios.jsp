<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportPolizaBeneficiarios'].elements['formReportPolizaBeneficiarios:reportId'].value;
			var name = 
				document.forms['formReportPolizaBeneficiarios'].elements['formReportPolizaBeneficiarios:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}    	    	
    </script>
	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{reportPolizaBeneficiariosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign                
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportPolizaBeneficiarios">
				
    				<h:inputHidden id="reportId" value="#{reportPolizaBeneficiariosForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportPolizaBeneficiariosForm.reportName}" />
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reporte Beneficiarios/Titulares
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPolizaBeneficiariosForm.idTipoPersonal}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportPolizaBeneficiariosForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Region
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPolizaBeneficiariosForm.idRegion}"
                                            	immediate="false"> 
                                            	<f:selectItem itemLabel="Todos" itemValue="0" />                                           	
                                                <f:selectItems value="#{reportPolizaBeneficiariosForm.listRegion}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Poliza
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPolizaBeneficiariosForm.selectPoliza}"
	    										onchange="this.form.submit()" 
												valueChangeListener="#{reportPolizaBeneficiariosForm.changePoliza}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportPolizaBeneficiariosForm.listPoliza}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    				
    								<tr>
    									<td>
    										Plan Poliza
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPolizaBeneficiariosForm.selectPlanPoliza}"
    											rendered="#{reportPolizaBeneficiariosForm.showPlanPoliza}"
    											onchange="this.form.submit()" 
												valueChangeListener="#{reportPolizaBeneficiariosForm.changePlanPoliza}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportPolizaBeneficiariosForm.listPlanPoliza}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr> 
    								<tr>
    									<td>
    										Listar a
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPolizaBeneficiariosForm.tipo}"
	    										onchange="this.form.submit()" 
												valueChangeListener="#{reportPolizaBeneficiariosForm.changeTipo}"
                                            	immediate="false"> 
                                            	<f:selectItem itemLabel="Titulares" itemValue="T" />    
                                            	<f:selectItem itemLabel="Titulares y Beneficiarios" itemValue="B" />    
                                            	                                         	
                                            </h:selectOneMenu>
    									</td>
    								</tr>  
    								<tr>
    									<td>
    										Sexo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPolizaBeneficiariosForm.sexo}"
                                            	immediate="false"> 
                                            	<f:selectItem itemLabel="Todos" itemValue="T" />  
                                            	<f:selectItem itemLabel="Masculino" itemValue="M" />    
                                            	<f:selectItem itemLabel="Femenino" itemValue="F" />    
                                             </h:selectOneMenu>
    									</td>
    								</tr>  
    								<tr>
    									<td>
    										Parentesco
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPolizaBeneficiariosForm.parentesco}"
    											rendered="#{reportPolizaBeneficiariosForm.showParentesco}"
                                            	immediate="false"> 
                                            	<f:selectItem itemLabel="Todos" itemValue="T" />   
												<f:selectItems value="#{reportPolizaBeneficiariosForm.listParentesco}" />                                       	
                                            </h:selectOneMenu>
    									</td>
    								</tr>  
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPolizaBeneficiariosForm.formato}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	 
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										rendered="#{reportPolizaBeneficiariosForm.showBoton}"
    											action="#{reportPolizaBeneficiariosForm.runReport}"
    											onclick="windowReport()" />
    									<td>
    								</tr>    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>