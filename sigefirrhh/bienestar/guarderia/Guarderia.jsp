<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{guarderiaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formGuarderia">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Centros de Educaci�n Inicial
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/bienestar/guarderia/Guarderia.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{guarderiaForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!guarderiaForm.editing&&!guarderiaForm.deleting&&guarderiaForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchGuarderia"
								rendered="#{!guarderiaForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													C�digo
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="3"
														maxlength="3"
														value="#{guarderiaForm.findCodGuarderia}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{guarderiaForm.findGuarderiaByCodGuarderia}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Nombre
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="90"
														maxlength="90"
														value="#{guarderiaForm.findNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{guarderiaForm.findGuarderiaByNombre}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultGuarderiaByCodGuarderia"
								rendered="#{guarderiaForm.showGuarderiaByCodGuarderia&&
								!guarderiaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{guarderiaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{guarderiaForm.selectGuarderia}"
                                						styleClass="listitem">
                                						<f:param name="idGuarderia" value="#{result.idGuarderia}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultGuarderiaByNombre"
								rendered="#{guarderiaForm.showGuarderiaByNombre&&
								!guarderiaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{guarderiaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{guarderiaForm.selectGuarderia}"
                                						styleClass="listitem">
                                						<f:param name="idGuarderia" value="#{result.idGuarderia}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataGuarderia"
								rendered="#{guarderiaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!guarderiaForm.editing&&!guarderiaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{guarderiaForm.editing&&!guarderiaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{guarderiaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{guarderiaForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
										<h:form id="guarderiaForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{guarderiaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!guarderiaForm.editing&&!guarderiaForm.deleting&&guarderiaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{guarderiaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!guarderiaForm.editing&&!guarderiaForm.deleting&&guarderiaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{guarderiaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{guarderiaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{guarderiaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{guarderiaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{guarderiaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{guarderiaForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�digo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="3"
	                        							id="codGuarderia"
	                        							maxlength="3"
                        								value="#{guarderiaForm.guarderia.codGuarderia}"
                        								readonly="#{!guarderiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="codGuarderia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nombre
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="90"
	                        							id="nombre"
	                        							maxlength="90"
                        								value="#{guarderiaForm.guarderia.nombre}"
                        								readonly="#{!guarderiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="nombre" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Direcci�n
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputTextarea
	                        							cols="100"
	                        							rows="4"
	                        							id="direccion"
                        								value="#{guarderiaForm.guarderia.direccion}"
                        								readonly="#{!guarderiaForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"                        								
                        								onkeypress="return keyEnterCheck(event, this)"                        								
                        								required="false" />													<h:message for="direccion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Pais
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{guarderiaForm.selectPaisForCiudad}"
                                                    	disabled="#{!guarderiaForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="PaisForCiudad"
														valueChangeListener="#{guarderiaForm.changePaisForCiudad}"
                        																						immediate="false"
														required="false"
														title="Ciudad">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{guarderiaForm.colPaisForCiudad}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Estado
                        						</td>
                        						<td></f:verbatim>
													<h:selectOneMenu value="#{guarderiaForm.selectEstadoForCiudad}"
                                                    	disabled="#{!guarderiaForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="EstadoForCiudad"
														valueChangeListener="#{guarderiaForm.changeEstadoForCiudad}"
														rendered="#{guarderiaForm.showEstadoForCiudad}"
														immediate="false"
														required="false"
														title="Ciudad">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{guarderiaForm.colEstadoForCiudad}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Ciudad
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{guarderiaForm.selectCiudad}"
	                                                    id="ciudad"
                                                    	disabled="#{!guarderiaForm.editing}"
                                                    	rendered="#{guarderiaForm.showCiudad
														 }"
                                                    	immediate="false"
                                                    	required="false"
														title="Ciudad">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{guarderiaForm.colCiudad}" />
                                                    </h:selectOneMenu>													<h:message for="ciudad" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tel�fono
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							id="telefono1"
	                        							maxlength="15"
                        								value="#{guarderiaForm.guarderia.telefono1}"
                        								readonly="#{!guarderiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="telefono1" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tel�fono (2)
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							id="telefono2"
	                        							maxlength="15"
                        								value="#{guarderiaForm.guarderia.telefono2}"
                        								readonly="#{!guarderiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="telefono2" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Contacto
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="50"
	                        							id="personaContacto"
	                        							maxlength="50"
                        								value="#{guarderiaForm.guarderia.personaContacto}"
                        								readonly="#{!guarderiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="personaContacto" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nro. RIF
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							id="numeroRif"
	                        							maxlength="15"
                        								value="#{guarderiaForm.guarderia.numeroRif}"
                        								readonly="#{!guarderiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="numeroRif" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Permiso ME
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="permisoInam"
	                        							maxlength="10"
                        								value="#{guarderiaForm.guarderia.permisoInam}"
                        								readonly="#{!guarderiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="permisoInam" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Beneficiario
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="50"
	                        							id="beneficiarioCheque"
	                        							maxlength="50"
                        								value="#{guarderiaForm.guarderia.beneficiarioCheque}"
                        								readonly="#{!guarderiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="beneficiarioCheque" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Seguro M�dico
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{guarderiaForm.guarderia.seguroMedico}"
	                                                    id="seguroMedico"
                                                    	disabled="#{!guarderiaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Seguro M�dico">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{guarderiaForm.listSeguroMedico}" />
                                                    </h:selectOneMenu>													<h:message for="seguroMedico" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Monto Inscripci�n
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="montoInscripcion"
	                        							maxlength="10"
                        								value="#{guarderiaForm.guarderia.montoInscripcion}"
                        								readonly="#{!guarderiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="montoInscripcion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Monto Mensualidad
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="montoMensualidad"
	                        							maxlength="10"
                        								value="#{guarderiaForm.guarderia.montoMensualidad}"
                        								readonly="#{!guarderiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
                        								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        							<h:message for="montoMensualidad" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Mes Inscripci�n
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{guarderiaForm.guarderia.mes}"
	                                                    id="mes"
                                                    	disabled="#{!guarderiaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Mes Inscripci�n">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{guarderiaForm.listMes}" />
                                                    </h:selectOneMenu>													<h:message for="mes" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{guarderiaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!guarderiaForm.editing&&!guarderiaForm.deleting&&guarderiaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{guarderiaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!guarderiaForm.editing&&!guarderiaForm.deleting&&guarderiaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{guarderiaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{guarderiaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{guarderiaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{guarderiaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{guarderiaForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{guarderiaForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>
