<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formAsignarTickets'].elements['formAsignarTickets:reportId'].value;
			var name = 
				document.forms['formAsignarTickets'].elements['formAsignarTickets:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}    	    	
    </script>
		<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{asignarTicketsForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formAsignarTickets">
				    <h:inputHidden id="reportId" value="#{asignarTicketsForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{asignarTicketsForm.reportName}" />
				    					
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Asignaci�n de Tickets
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/bienestar/ticketAlimentacion/AsignarTickets.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Grupo Ticket
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{asignarTicketsForm.selectGrupoTicket}"
	    										onchange="this.form.submit()"  
	    										valueChangeListener="#{asignarTicketsForm.changeGrupoTicket}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{asignarTicketsForm.listGrupoTicket}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											readonly="true"
    											rendered="#{asignarTicketsForm.show}"
    											size="2"
    											id="Mes"
    											maxlength="10"
    											value="#{asignarTicketsForm.mes}"    											
    											required="false" />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{asignarTicketsForm.show}"
    											readonly="true"
    											size="10"
    											id="Anio"
    											maxlength="4"
    											value="#{asignarTicketsForm.anio}"    											
    											required="false" />    								
    									</td>
									</tr>
									<tr>
    									<td>
    										Proceso
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{asignarTicketsForm.proceso}"
	    										rendered="#{asignarTicketsForm.show}"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Prueba" itemValue="1" />
                                            	<f:selectItem itemLabel="Cierre" itemValue="2" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
    											action="#{asignarTicketsForm.execute}"
    											rendered="#{asignarTicketsForm.showExec}"
    											/>
    									<td>
    								</tr>    
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
    											action="#{asignarTicketsForm.runReport}"
    											rendered="#{asignarTicketsForm.show}"
    											onclick="windowReport()" />
    									<td>
    								</tr>    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>