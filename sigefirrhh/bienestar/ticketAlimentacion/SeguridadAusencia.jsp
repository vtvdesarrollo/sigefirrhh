<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	
	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{seguridadAusenciaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
       
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportSeguridadAusencia">
							
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Cerrar Registro de Ausencias
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/bienestar/ticketAlimentacion/SeguridadAusencia.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Proceso
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{seguridadAusenciaForm.proceso}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" /> 
                                            	<f:selectItem itemLabel="Cerrar" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Reversar" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								
    								
    								<tr>
    									<td>
    										Grupo Ticket
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{seguridadAusenciaForm.selectGrupoTicket}"
	    										onchange="this.form.submit()"  
	    										valueChangeListener="#{seguridadAusenciaForm.changeGrupoTicket}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{seguridadAusenciaForm.listGrupoTicket}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td width="20%">
    										A�o Cerrado
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    										    rendered="#{seguridadAusenciaForm.show}"
    											id="Anio"
    											value="#{seguridadAusenciaForm.anio}"
    											>    								
                	                        </h:outputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Mes Cerrado
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    										    rendered="#{seguridadAusenciaForm.show}"
    											id="Mes"
    											value="#{seguridadAusenciaForm.mes}"
 >    								
                	                        </h:outputText>
    									</td>
									</tr>    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
    										    rendered="#{seguridadAusenciaForm.show}"
    											action="#{seguridadAusenciaForm.generar}"
    											onclick="windowReport()" />
    									<td>
    								</tr>    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>