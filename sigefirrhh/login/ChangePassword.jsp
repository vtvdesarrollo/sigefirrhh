<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>




<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
        		
    </script>
</head>

<body onload="scrollToCoordinates(); firstFocus();">

<f:view>
	<x:saveState value="#{changePasswordForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
    <%
    	String mensaje = ""; // dios personame por esto
    %>
<% if ( !((LoginSession)session.getAttribute("loginSession")).isPasswordVencido()) {%>
	<jsp:include page="/inc/top.jsp" />
	<%}else{
		mensaje = "Su clave ha vencido,<br> debe proceder a cambiarla de inmediato";
		%>    
	<jsp:include page="/inc/topnomenu.jsp" />
	<%}%>	

	<table width="800"  border="0" cellspacing="0" cellpadding="0" align="left">
		<tr>
			<td>
				<table width="350" border="0" cellspacing="0" cellpadding="5" valign="top" align="center">
					<tr>	
						<!-- Login -->
						<td width="800" valign="top">
						<table width="280%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Cambiar Contraseņa 
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/sistema/CambiarPassword.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
						<h:form>
						<table class="datatable" width="350" align="center">
						<tr>
							<td>
										<f:verbatim><table class="datatable" width="280" align="center">
											<tr>
												<td colspan="2" class="error" align="center">
													 <%=mensaje%>
												</td>
											</tr>
											<tr>
												<td colspan="2" class="error">
													<h:messages errorClass="error" styleClass="success" />
												</td>
											</tr>
											<tr>
			            						<td>
			            							Usuario
			            						</td>
			            						<td></f:verbatim>
			    	                        		<h:inputText
			    	                        			rendered="#{changePasswordForm.administrador}"
			                							size="10"
			                							readonly="true"
			                							maxlength="10"
			            								value="#{changePasswordForm.usuario}" />
			            						<f:verbatim></td>
			            					</tr>
											<tr>
			            						<td>
			            							Contraseņa Actual
			            						</td>
			            						<td></f:verbatim>
			    	                        		<h:inputSecret
			                							size="10"
			                							maxlength="10"
			            								value="#{changePasswordForm.password}" />
			            						<f:verbatim></td>
			            					</tr>
			            					
			            					<tr>
			            						<td>
			            							Nueva Contraseņa
			            						</td>
			            						<td></f:verbatim>
			    	                        		<h:inputSecret
			                							size="10"
			                							maxlength="10"
			            								value="#{changePasswordForm.newPassword}" />
			            						<f:verbatim></td>
			            					</tr>
			            					
			            					<tr>
			            						<td>
			            							Confirmar Nueva Contraseņa
			            						</td>
			            						<td></f:verbatim>
			    	                        		<h:inputSecret
			                							size="10"
			                							maxlength="10"
			            								value="#{changePasswordForm.repeatNewPassword}" />
			            						<f:verbatim></td>
			            					</tr>
			            					
			            					<tr>
			            						<td colpsan="2" align="center"></f:verbatim>
													<h:commandButton value="Guardar" 
			                        					image="/images/save.gif"
			                        					action="#{changePasswordForm.update}"
			                        					
			                	        				 />
			                	        			<h:commandButton value="Cancelar" 
			                        					image="/images/cancel.gif"
			            								action="#{changePasswordForm.cancel}"
			            								immediate="true"
			            								
			    	        							 />	
												<f:verbatim></td>
			            					</tr>
			            					
			            				</table></f:verbatim>
							
							</td>
						</tr>
							
							
						</h:form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>