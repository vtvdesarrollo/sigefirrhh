<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{habilidadCargoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formHabilidadCargo">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Requisitos para un cargo: Habilidades / Competencias
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/planificacion/clasificacion/HabilidadCargo.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{habilidadCargoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!habilidadCargoForm.editing&&!habilidadCargoForm.deleting&&habilidadCargoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchHabilidadCargo"
								rendered="#{!habilidadCargoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
						                        					<f:verbatim><tr>
                        						<td>
                        							Clasificador de Cargo
                        						</td>
                        						<td></f:verbatim>
				                                                    <h:selectOneMenu value="#{habilidadCargoForm.findSelectManualCargoForCargo}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{habilidadCargoForm.findChangeManualCargoForCargo}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{habilidadCargoForm.findColManualCargoForCargo}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
																<f:verbatim><tr>
												<td>
													Cargo
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{habilidadCargoForm.findSelectCargo}"
                                                    	rendered="#{habilidadCargoForm.findShowCargo}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{habilidadCargoForm.findColCargo}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{habilidadCargoForm.findHabilidadCargoByCargo}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultHabilidadCargoByCargo"
								rendered="#{habilidadCargoForm.showHabilidadCargoByCargo&&
								!habilidadCargoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{habilidadCargoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{habilidadCargoForm.selectHabilidadCargo}"
                                						styleClass="listitem">
                                						<f:param name="idHabilidadCargo" value="#{result.idHabilidadCargo}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataHabilidadCargo"
								rendered="#{habilidadCargoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!habilidadCargoForm.editing&&!habilidadCargoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{habilidadCargoForm.editing&&!habilidadCargoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{habilidadCargoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{habilidadCargoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="habilidadCargoForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{habilidadCargoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!habilidadCargoForm.editing&&!habilidadCargoForm.deleting&&habilidadCargoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{habilidadCargoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!habilidadCargoForm.editing&&!habilidadCargoForm.deleting&&habilidadCargoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{habilidadCargoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{habilidadCargoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{habilidadCargoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{habilidadCargoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{habilidadCargoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{habilidadCargoForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Clasificador de Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{habilidadCargoForm.selectManualCargoForCargo}"
                                                    	disabled="#{!habilidadCargoForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="ManualCargoForCargo"
														valueChangeListener="#{habilidadCargoForm.changeManualCargoForCargo}"
                        																						immediate="false"
														required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{habilidadCargoForm.colManualCargoForCargo}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{habilidadCargoForm.selectCargo}"
	                                                    id="cargo"
                                                    	disabled="#{!habilidadCargoForm.editing}"
                                                    	rendered="#{habilidadCargoForm.showCargo
														 }"
                                                    	immediate="false"
                                                    	required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{habilidadCargoForm.colCargo}" />
                                                    </h:selectOneMenu>													<h:message for="cargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Habilidades
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{habilidadCargoForm.selectTipoHabilidad}"
														id="tipoHabilidad"
                                                    	disabled="#{!habilidadCargoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Habilidades">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{habilidadCargoForm.colTipoHabilidad}" />
                                                    </h:selectOneMenu>													<h:message for="tipoHabilidad" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nivel
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{habilidadCargoForm.habilidadCargo.nivel}"
	                                                    id="nivel"
                                                    	disabled="#{!habilidadCargoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Nivel">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{habilidadCargoForm.listNivel}" />
                                                    </h:selectOneMenu>													<h:message for="nivel" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Peso Ponderado
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="peso"
	                        							maxlength="10"
                        								value="#{habilidadCargoForm.habilidadCargo.peso}"
                        								readonly="#{!habilidadCargoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="peso" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{habilidadCargoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!habilidadCargoForm.editing&&!habilidadCargoForm.deleting&&habilidadCargoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{habilidadCargoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!habilidadCargoForm.editing&&!habilidadCargoForm.deleting&&habilidadCargoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{habilidadCargoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{habilidadCargoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{habilidadCargoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{habilidadCargoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{habilidadCargoForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{habilidadCargoForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>