<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{baremoPreseleccionadoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formBaremoPreseleccionado">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Registrar Resultados Baremos
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/planificacion/seleccion/BaremoPreseleccionado.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{baremoPreseleccionadoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!baremoPreseleccionadoForm.editing&&!baremoPreseleccionadoForm.deleting&&baremoPreseleccionadoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchBaremoPreseleccionado"
								rendered="#{!baremoPreseleccionadoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
						                        					<f:verbatim><tr>
                        						<td>
                        							Concurso
                        						</td>
                        						<td></f:verbatim>
				                                                    <h:selectOneMenu value="#{baremoPreseleccionadoForm.findSelectConcursoForPostuladoConcurso}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{baremoPreseleccionadoForm.findChangeConcursoForPostuladoConcurso}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{baremoPreseleccionadoForm.findColConcursoForPostuladoConcurso}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
			                        					<f:verbatim><tr>
                        						<td>
                        							Concurso Cargo
                        						</td>
                        						<td></f:verbatim>
																	<h:selectOneMenu value="#{baremoPreseleccionadoForm.findSelectConcursoCargoForPostuladoConcurso}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{baremoPreseleccionadoForm.findChangeConcursoCargoForPostuladoConcurso}"
														rendered="#{baremoPreseleccionadoForm.findShowConcursoCargoForPostuladoConcurso}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{baremoPreseleccionadoForm.findColConcursoCargoForPostuladoConcurso}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
																<f:verbatim><tr>
												<td>
													Postulado
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{baremoPreseleccionadoForm.findSelectPostuladoConcurso}"
                                                    	rendered="#{baremoPreseleccionadoForm.findShowPostuladoConcurso}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{baremoPreseleccionadoForm.findColPostuladoConcurso}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{baremoPreseleccionadoForm.findBaremoPreseleccionadoByPostuladoConcurso}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultBaremoPreseleccionadoByPostuladoConcurso"
								rendered="#{baremoPreseleccionadoForm.showBaremoPreseleccionadoByPostuladoConcurso&&
								!baremoPreseleccionadoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{baremoPreseleccionadoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{baremoPreseleccionadoForm.selectBaremoPreseleccionado}"
                                						styleClass="listitem">
                                						<f:param name="idBaremoPreseleccionado" value="#{result.idBaremoPreseleccionado}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataBaremoPreseleccionado"
								rendered="#{baremoPreseleccionadoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!baremoPreseleccionadoForm.editing&&!baremoPreseleccionadoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{baremoPreseleccionadoForm.editing&&!baremoPreseleccionadoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{baremoPreseleccionadoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{baremoPreseleccionadoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="baremoPreseleccionadoForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{baremoPreseleccionadoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!baremoPreseleccionadoForm.editing&&!baremoPreseleccionadoForm.deleting&&baremoPreseleccionadoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{baremoPreseleccionadoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!baremoPreseleccionadoForm.editing&&!baremoPreseleccionadoForm.deleting&&baremoPreseleccionadoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{baremoPreseleccionadoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{baremoPreseleccionadoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{baremoPreseleccionadoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{baremoPreseleccionadoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{baremoPreseleccionadoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{baremoPreseleccionadoForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Concurso
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{baremoPreseleccionadoForm.selectConcursoForPostuladoConcurso}"
                                                    	disabled="#{!baremoPreseleccionadoForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="ConcursoForPostuladoConcurso"
														valueChangeListener="#{baremoPreseleccionadoForm.changeConcursoForPostuladoConcurso}"
                        																						immediate="false"
														required="false"
														title="Postulado">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{baremoPreseleccionadoForm.colConcursoForPostuladoConcurso}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Concurso Cargo
                        						</td>
                        						<td></f:verbatim>
													<h:selectOneMenu value="#{baremoPreseleccionadoForm.selectConcursoCargoForPostuladoConcurso}"
                                                    	disabled="#{!baremoPreseleccionadoForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="ConcursoCargoForPostuladoConcurso"
														valueChangeListener="#{baremoPreseleccionadoForm.changeConcursoCargoForPostuladoConcurso}"
														rendered="#{baremoPreseleccionadoForm.showConcursoCargoForPostuladoConcurso
														 }"
														immediate="false"
														required="false"
														title="Postulado">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{baremoPreseleccionadoForm.colConcursoCargoForPostuladoConcurso}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Postulado
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{baremoPreseleccionadoForm.selectPostuladoConcurso}"
	                                                    id="postuladoConcurso"
                                                    	disabled="#{!baremoPreseleccionadoForm.editing}"
                                                    	rendered="#{baremoPreseleccionadoForm.showPostuladoConcurso
														 }"
                                                    	immediate="false"
                                                    	required="false"
														title="Postulado">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{baremoPreseleccionadoForm.colPostuladoConcurso}" />
                                                    </h:selectOneMenu>													<h:message for="postuladoConcurso" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Componente Baremos
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{baremoPreseleccionadoForm.selectBaremoSeleccion}"
														id="baremoSeleccion"
                                                    	disabled="#{!baremoPreseleccionadoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Baremo/Entrevista">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{baremoPreseleccionadoForm.colBaremoSeleccion}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="baremoSeleccion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Resultado
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="resultado"
	                        							maxlength="10"
                        								value="#{baremoPreseleccionadoForm.baremoPreseleccionado.resultado}"
                        								readonly="#{!baremoPreseleccionadoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="resultado" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Evaluador
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="60"
	                        							id="entrevistador"
	                        							maxlength="60"
                        								value="#{baremoPreseleccionadoForm.baremoPreseleccionado.entrevistador}"
                        								readonly="#{!baremoPreseleccionadoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="entrevistador" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="fecha"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{baremoPreseleccionadoForm.baremoPreseleccionado.fecha}"
                        								readonly="#{!baremoPreseleccionadoForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onblur="javascript:check_date(this)"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
													<h:message for="fecha" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Observaciones
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputTextarea
	                        							cols="100"
	                        							rows="4"
	                        							id="observaciones"
                        								value="#{baremoPreseleccionadoForm.baremoPreseleccionado.observaciones}"
                        								readonly="#{!baremoPreseleccionadoForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"                        								
                        								onkeypress="return keyEnterCheck(event, this)"                        								
                        								required="false" />													<h:message for="observaciones" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{baremoPreseleccionadoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!baremoPreseleccionadoForm.editing&&!baremoPreseleccionadoForm.deleting&&baremoPreseleccionadoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{baremoPreseleccionadoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!baremoPreseleccionadoForm.editing&&!baremoPreseleccionadoForm.deleting&&baremoPreseleccionadoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{baremoPreseleccionadoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{baremoPreseleccionadoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{baremoPreseleccionadoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{baremoPreseleccionadoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{baremoPreseleccionadoForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{baremoPreseleccionadoForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>