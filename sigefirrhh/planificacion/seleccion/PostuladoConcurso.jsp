<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{postuladoConcursoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formPostuladoConcurso">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Postulados a Cargos por Concursos
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/planificacion/seleccion/PostuladoConcurso.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{postuladoConcursoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!postuladoConcursoForm.editing&&!postuladoConcursoForm.deleting&&postuladoConcursoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchPostuladoConcurso"
								rendered="#{!postuladoConcursoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
						                        					<f:verbatim><tr>
                        						<td>
                        							Concurso
                        						</td>
                        						<td></f:verbatim>
				                                                    <h:selectOneMenu value="#{postuladoConcursoForm.findSelectConcursoForConcursoCargo}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{postuladoConcursoForm.findChangeConcursoForConcursoCargo}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{postuladoConcursoForm.findColConcursoForConcursoCargo}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
																<f:verbatim><tr>
												<td>
													Cargo
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{postuladoConcursoForm.findSelectConcursoCargo}"
                                                    	rendered="#{postuladoConcursoForm.findShowConcursoCargo}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{postuladoConcursoForm.findColConcursoCargo}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{postuladoConcursoForm.findPostuladoConcursoByConcursoCargo}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultPostuladoConcursoByConcursoCargo"
								rendered="#{postuladoConcursoForm.showPostuladoConcursoByConcursoCargo&&
								!postuladoConcursoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{postuladoConcursoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{postuladoConcursoForm.selectPostuladoConcurso}"
                                						styleClass="listitem">
                                						<f:param name="idPostuladoConcurso" value="#{result.idPostuladoConcurso}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataPostuladoConcurso"
								rendered="#{postuladoConcursoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!postuladoConcursoForm.editing&&!postuladoConcursoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{postuladoConcursoForm.editing&&!postuladoConcursoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{postuladoConcursoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{postuladoConcursoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="postuladoConcursoForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{postuladoConcursoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!postuladoConcursoForm.editing&&!postuladoConcursoForm.deleting&&postuladoConcursoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{postuladoConcursoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{postuladoConcursoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{postuladoConcursoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{postuladoConcursoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{postuladoConcursoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{postuladoConcursoForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Concurso
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{postuladoConcursoForm.selectConcursoForConcursoCargo}"
                                                    	disabled="#{!postuladoConcursoForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="ConcursoForConcursoCargo"
														valueChangeListener="#{postuladoConcursoForm.changeConcursoForConcursoCargo}"
                        																						immediate="false"
														required="true"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{postuladoConcursoForm.colConcursoForConcursoCargo}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{postuladoConcursoForm.selectConcursoCargo}"
	                                                    id="concursoCargo"
                                                    	disabled="#{!postuladoConcursoForm.editing}"
                                                    	rendered="#{postuladoConcursoForm.showConcursoCargo
														 }"
                                                    	immediate="false"
                                                    	required="true"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{postuladoConcursoForm.colConcursoCargo}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="concursoCargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Busqueda por C�dula
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="8"
	                        							id="findCedula"
	                        							maxlength="8"
                        								value="#{postuladoConcursoForm.findCedula}"
                        								readonly="#{!postuladoConcursoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)">
                        							</h:inputText>
                        								
                        								<h:commandButton image="/images/find.gif" 
														action="#{postuladoConcursoForm.findPostuladoByCedula}"
														onclick="javascript:return clickMade()" />
														
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cedula
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{postuladoConcursoForm.cedula}">                        								                        								                        								                        								                        								                        																						                        								
                        							</h:outputText>                        								
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Primer Apellido
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{postuladoConcursoForm.primerApellido}">                        								                        								                        								                        								                        								                        																						                        								
                        							</h:outputText>                        								
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Segundo Apellido
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{postuladoConcursoForm.segundoApellido}">
                        							</h:outputText>                          								                        																					
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Primer Nombre
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{postuladoConcursoForm.primerNombre}" >                      								                        								                        								                        								                        								                        																						                        								
                        							</h:outputText>                          								
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Segundo Nombre
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{postuladoConcursoForm.segundoNombre}" >                      								                        								                        								                        								                        								                        																						                        								
                        							</h:outputText>                          								                        						
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>                                    			
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{postuladoConcursoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!postuladoConcursoForm.editing&&!postuladoConcursoForm.deleting&&postuladoConcursoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{postuladoConcursoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{postuladoConcursoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{postuladoConcursoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{postuladoConcursoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{postuladoConcursoForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{postuladoConcursoForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>