<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{actualizarParticipanteForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formActualizarParticipante" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Incluir Participantes
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/planificacion/capacitacion/ActualizarParticipante.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
            					image="/images/close.gif"
            					action="go_cancelOption"
            					immediate="true"                        					
            					onclick="javascript:return clickMade()"
    	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
        										    																	
        							<tr>
    									<td width="25%">
    										Plan Capacitaci�n
    									</td>
    									<td width="75%">
    										<h:selectOneMenu value="#{actualizarParticipanteForm.selectIdPlanAdiestramiento}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{actualizarParticipanteForm.changePlanAdiestramiento}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{actualizarParticipanteForm.listPlanAdiestramiento}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    							</table>
    							<f:verbatim>
    							<table width="100%" class="datatable">
        							<tr>
        								<td width="20%">
        									C�dula
        								</td>
        								<td width="30%">
        								</f:verbatim>
											<h:inputText
												size="8"
												id="Cedula"
												maxlength="8"
												value="#{actualizarParticipanteForm.cedula}"   						
												onfocus="return deleteZero(event, this)"
												onblur="javascript:fieldEmpty(this)"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyIntegerCheck(event, this)" />
										<f:verbatim>	
        								</td>
        								<td width="20%">
        									Tipo Asistencia
        								</td>
    									<td width="30%">
    									</f:verbatim>
											<h:selectOneMenu value="#{actualizarParticipanteForm.asistencia}"
												immediate="false">
											<f:selectItems value="#{actualizarParticipanteForm.listAsistencia}" />
											</h:selectOneMenu>
										<f:verbatim>	
										</td>
									</tr>
									<tr>
										<td colspan="3"></td>
										<td>
										</f:verbatim>
											<h:commandButton value="Agregar"
												image="/images/add.gif"
												action="#{actualizarParticipanteForm.actualizar}" />
											<f:verbatim>
										</td>
									</tr>
        							<tr>
        								<td colspan="4">
        								</f:verbatim>
											<h:outputText
												value="#{actualizarParticipanteForm.cedulaFinal}" />
												&nbsp;
											<h:outputText
												value="#{actualizarParticipanteForm.nombre}" />
												&nbsp;
											<h:outputText
												value="#{actualizarParticipanteForm.apellido}" />
												&nbsp;
											<h:outputText
												value="#{actualizarParticipanteForm.asistenciaFinal}" />
												&nbsp;
										<f:verbatim>
										</td>		
   									</tr>
   								</table>
   								</f:verbatim>	
   							</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>