<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{planAdiestramientoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formPlanAdiestramiento">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Planes Capacitaci�n por Unidad Funcional
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/planificacion/capacitacion/PlanAdiestramiento.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{planAdiestramientoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!planAdiestramientoForm.editing&&!planAdiestramientoForm.deleting&&planAdiestramientoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchPlanAdiestramiento"
								rendered="#{!planAdiestramientoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Unidad Funcional
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{planAdiestramientoForm.findSelectUnidadFuncional}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.findColUnidadFuncional}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{planAdiestramientoForm.findPlanAdiestramientoByUnidadFuncional}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Area Conocimiento
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{planAdiestramientoForm.findSelectAreaConocimiento}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.findColAreaConocimiento}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{planAdiestramientoForm.findPlanAdiestramientoByAreaConocimiento}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultPlanAdiestramientoByUnidadFuncional"
								rendered="#{planAdiestramientoForm.showPlanAdiestramientoByUnidadFuncional&&
								!planAdiestramientoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{planAdiestramientoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{planAdiestramientoForm.selectPlanAdiestramiento}"
                                						styleClass="listitem">
                                						<f:param name="idPlanAdiestramiento" value="#{result.idPlanAdiestramiento}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultPlanAdiestramientoByAreaConocimiento"
								rendered="#{planAdiestramientoForm.showPlanAdiestramientoByAreaConocimiento&&
								!planAdiestramientoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{planAdiestramientoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{planAdiestramientoForm.selectPlanAdiestramiento}"
                                						styleClass="listitem">
                                						<f:param name="idPlanAdiestramiento" value="#{result.idPlanAdiestramiento}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataPlanAdiestramiento"
								rendered="#{planAdiestramientoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!planAdiestramientoForm.editing&&!planAdiestramientoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{planAdiestramientoForm.editing&&!planAdiestramientoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{planAdiestramientoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{planAdiestramientoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="planAdiestramientoForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{planAdiestramientoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!planAdiestramientoForm.editing&&!planAdiestramientoForm.deleting&&planAdiestramientoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{planAdiestramientoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!planAdiestramientoForm.editing&&!planAdiestramientoForm.deleting&&planAdiestramientoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{planAdiestramientoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{planAdiestramientoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{planAdiestramientoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{planAdiestramientoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{planAdiestramientoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{planAdiestramientoForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Unidad Funcional
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{planAdiestramientoForm.selectUnidadFuncional}"
														id="unidadFuncional"
                                                    	disabled="#{!planAdiestramientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Unidad Funcional">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.colUnidadFuncional}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="unidadFuncional" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							A�o
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="anio"
	                        							maxlength="4"
                        								value="#{planAdiestramientoForm.planAdiestramiento.anio}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="anio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nro. Plan
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="numeroPlan"
	                        							maxlength="4"
                        								value="#{planAdiestramientoForm.planAdiestramiento.numeroPlan}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="numeroPlan" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{planAdiestramientoForm.selectTipoCurso}"
														id="tipoCurso"
                                                    	disabled="#{!planAdiestramientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Tipo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.colTipoCurso}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="tipoCurso" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Area Conocimiento
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{planAdiestramientoForm.selectAreaConocimiento}"
														id="areaConocimiento"
                                                    	disabled="#{!planAdiestramientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Area Conocimiento">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.colAreaConocimiento}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="areaConocimiento" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nombre Curso/Ponencia
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="90"
	                        							id="nombreCurso"
	                        							maxlength="90"
                        								value="#{planAdiestramientoForm.planAdiestramiento.nombreCurso}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="nombreCurso" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nro. Participantes
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="numeroParticipantes"
	                        							maxlength="4"
                        								value="#{planAdiestramientoForm.planAdiestramiento.numeroParticipantes}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="numeroParticipantes" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Duraci�n
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="duracion"
	                        							maxlength="4"
                        								value="#{planAdiestramientoForm.planAdiestramiento.duracion}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="duracion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Unidad Tiempo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{planAdiestramientoForm.planAdiestramiento.unidadTiempo}"
	                                                    id="unidadTiempo"
                                                    	disabled="#{!planAdiestramientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Unidad Tiempo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.listUnidadTiempo}" />
                                                    </h:selectOneMenu>													<h:message for="unidadTiempo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{planAdiestramientoForm.planAdiestramiento.tipoCargo}"
	                                                    id="tipoCargo"
                                                    	disabled="#{!planAdiestramientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Tipo Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.listTipoCargo}" />
                                                    </h:selectOneMenu>													<h:message for="tipoCargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nivel
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{planAdiestramientoForm.planAdiestramiento.nivel}"
	                                                    id="nivel"
                                                    	disabled="#{!planAdiestramientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Nivel">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.listNivel}" />
                                                    </h:selectOneMenu>													<h:message for="nivel" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Costo Estimado
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="costoEstimado"
	                        							maxlength="16"
                        								value="#{planAdiestramientoForm.planAdiestramiento.costoEstimado}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="costoEstimado" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Costo Aprobado
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="costoAprobado"
	                        							maxlength="16"
                        								value="#{planAdiestramientoForm.planAdiestramiento.costoAprobado}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="costoAprobado" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Costo Real
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="costoReal"
	                        							maxlength="16"
                        								value="#{planAdiestramientoForm.planAdiestramiento.costoReal}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="costoReal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Trimestre
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="1"
	                        							id="trimestre"
	                        							maxlength="1"
                        								value="#{planAdiestramientoForm.planAdiestramiento.trimestre}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="trimestre" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Estatus
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{planAdiestramientoForm.planAdiestramiento.estatus}"
	                                                    id="estatus"
                                                    	disabled="#{!planAdiestramientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Estatus">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.listEstatus}" />
                                                    </h:selectOneMenu>													<h:message for="estatus" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha Inicio
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="fechaInicio"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planAdiestramientoForm.planAdiestramiento.fechaInicio}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onblur="javascript:check_date(this)"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
													<h:message for="fechaInicio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Lugar
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{planAdiestramientoForm.planAdiestramiento.internoExterno}"
	                                                    id="internoExterno"
                                                    	disabled="#{!planAdiestramientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Lugar">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.listInternoExterno}" />
                                                    </h:selectOneMenu>													<h:message for="internoExterno" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Empresa/Entidad Educativa
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="90"
	                        							id="nombreEntidad"
	                        							maxlength="90"
                        								value="#{planAdiestramientoForm.planAdiestramiento.nombreEntidad}"
                        								readonly="#{!planAdiestramientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="nombreEntidad" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Aprueba MPF?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{planAdiestramientoForm.planAdiestramiento.aprobacionMpd}"
	                                                    id="aprobacionMpd"
                                                    	disabled="#{!planAdiestramientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Aprueba MPF?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.listAprobacionMpd}" />
                                                    </h:selectOneMenu>													<h:message for="aprobacionMpd" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Plan
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{planAdiestramientoForm.selectPlanPersonal}"
														id="planPersonal"
                                                    	disabled="#{!planAdiestramientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Plan">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planAdiestramientoForm.colPlanPersonal}" />
                                                    </h:selectOneMenu>													<h:message for="planPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{planAdiestramientoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!planAdiestramientoForm.editing&&!planAdiestramientoForm.deleting&&planAdiestramientoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{planAdiestramientoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!planAdiestramientoForm.editing&&!planAdiestramientoForm.deleting&&planAdiestramientoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{planAdiestramientoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{planAdiestramientoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{planAdiestramientoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{planAdiestramientoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{planAdiestramientoForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{planAdiestramientoForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>
