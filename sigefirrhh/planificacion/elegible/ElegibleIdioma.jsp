<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
	
%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{elegibleIdiomaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	               
	  			 
	  				
			
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formElegibleIdioma">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Registro de Oferentes/Elegibles: Idiomas
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/planificacion/elegible/ElegibleIdioma.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success"  />
										</td>
									</tr>
									
									<f:subview 
										id="searchElegibleIdioma"
										rendered="#{!elegibleIdiomaForm.showData&&
										!elegibleIdiomaForm.showAdd}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Oferente/Elegible
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{elegibleIdiomaForm.findElegibleCedula}"
																onkeypress="javascript:return keyIntegerCheck(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{elegibleIdiomaForm.findElegibleByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{elegibleIdiomaForm.findElegiblePrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{elegibleIdiomaForm.findElegibleSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{elegibleIdiomaForm.findElegiblePrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{elegibleIdiomaForm.findElegibleSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{elegibleIdiomaForm.findElegibleByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultElegible"
									rendered="#{elegibleIdiomaForm.showResultElegible&&
									!elegibleIdiomaForm.showData&&
									!elegibleIdiomaForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Elegible
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <x:dataTable id="tableElegible"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultElegible"
	                                				value="#{elegibleIdiomaForm.resultElegible}"
    	                                            preserveDataModel="false"
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultElegible}"
    	                            						action="#{elegibleIdiomaForm.findElegibleIdiomaByElegible}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idElegible" value="#{resultElegible.idElegible}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </x:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tableElegible"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif" />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif" />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif" />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tableElegible"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedElegible"
									rendered="#{elegibleIdiomaForm.selectedElegible&&
									!elegibleIdiomaForm.showData&&
									!elegibleIdiomaForm.editing&&
									!elegibleIdiomaForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Elegible:&nbsp;</f:verbatim>

            									<h:commandLink value="#{elegibleIdiomaForm.elegible}   <-- clic aqui"
    	                            						action="#{elegibleIdiomaForm.findElegibleIdiomaByElegible}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idElegible" value="#{elegibleIdiomaForm.elegible.idElegible}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{elegibleIdiomaForm.add}"
            													rendered="#{!elegibleIdiomaForm.editing&&!elegibleIdiomaForm.deleting&&elegibleIdiomaForm.login.agregar}" />

                                        	    			
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{elegibleIdiomaForm.abort}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                                                    	    
                                                    	   <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultElegibleIdioma"
									rendered="#{elegibleIdiomaForm.showResult&&
									!elegibleIdiomaForm.showData&&
									!elegibleIdiomaForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <x:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{elegibleIdiomaForm.result}"
                                            	    preserveDataModel="false"
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{elegibleIdiomaForm.selectElegibleIdioma}"
	                                						styleClass="listitem">
    		                            						<f:param name="idElegibleIdioma" value="#{result.idElegibleIdioma}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </x:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataElegibleIdioma"
								rendered="#{elegibleIdiomaForm.showData||elegibleIdiomaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!elegibleIdiomaForm.editing&&!elegibleIdiomaForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{elegibleIdiomaForm.editing&&!elegibleIdiomaForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{elegibleIdiomaForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{elegibleIdiomaForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="elegibleIdiomaForm">
	    	        							<f:subview 
    	    	    								id="dataElegibleIdioma"
        	    									rendered="#{elegibleIdiomaForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{elegibleIdiomaForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!elegibleIdiomaForm.editing&&!elegibleIdiomaForm.deleting&&elegibleIdiomaForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{elegibleIdiomaForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!elegibleIdiomaForm.editing&&!elegibleIdiomaForm.deleting&&elegibleIdiomaForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{elegibleIdiomaForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{elegibleIdiomaForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{elegibleIdiomaForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{elegibleIdiomaForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{elegibleIdiomaForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{elegibleIdiomaForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Elegible
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{elegibleIdiomaForm.elegible}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
																																						                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Idioma
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{elegibleIdiomaForm.selectTipoIdioma}"
        		                                                	disabled="#{!elegibleIdiomaForm.editing}"
            		                                            	immediate="false"
				                        							id="tipoIdioma"
                		            								                    		        								                        		                                	required="true"
    																title="Idioma">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{elegibleIdiomaForm.colTipoIdioma}" />
																							<f:validator validatorId="eforserver.RequiredJSFValidator"/>
								                                    	                </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>																									<h:message for="tipoIdioma" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Habla
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{elegibleIdiomaForm.elegibleIdioma.habla}"
        	                                        	        	disabled="#{!elegibleIdiomaForm.editing}"
            	                                        	    	immediate="false"
				                        							id="habla"
                	            									                    	        										                        	                                	required="true"
    	                        	                            	title="Habla">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{elegibleIdiomaForm.listHabla}" />
																						<f:validator validatorId="eforserver.RequiredJSFValidator"/>
					    	            	                        	            </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>																				<h:message for="habla" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Lee
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{elegibleIdiomaForm.elegibleIdioma.lee}"
        	                                        	        	disabled="#{!elegibleIdiomaForm.editing}"
            	                                        	    	immediate="false"
				                        							id="lee"
                	            									                    	        										                        	                                	required="true"
    	                        	                            	title="Lee">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{elegibleIdiomaForm.listLee}" />
																						<f:validator validatorId="eforserver.RequiredJSFValidator"/>
					    	            	                        	            </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>																				<h:message for="lee" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Escribe
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{elegibleIdiomaForm.elegibleIdioma.escribe}"
        	                                        	        	disabled="#{!elegibleIdiomaForm.editing}"
            	                                        	    	immediate="false"
				                        							id="escribe"
                	            									                    	        										                        	                                	required="true"
    	                        	                            	title="Escribe">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{elegibleIdiomaForm.listEscribe}" />
																						<f:validator validatorId="eforserver.RequiredJSFValidator"/>
					    	            	                        	            </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>																				<h:message for="escribe" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Instituci�n donde estudi�
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="100"
	    	    		                							maxlength="100"
            	    		        								value="#{elegibleIdiomaForm.elegibleIdioma.nombreEntidad}"
                	    		    								                    	    										readonly="#{!elegibleIdiomaForm.editing}"
                        											id="nombreEntidad"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreEntidad" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Examen Suficiencia
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{elegibleIdiomaForm.elegibleIdioma.examenSuficiencia}"
        	                                        	        	disabled="#{!elegibleIdiomaForm.editing}"
            	                                        	    	immediate="false"
				                        							id="examenSuficiencia"
                	            									                    	        									 onchange="this.form.submit()" 	                        	                                	required="false"
    	                        	                            	title="Examen Suficiencia">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{elegibleIdiomaForm.listExamenSuficiencia}" />
					    	            	                        	            </h:selectOneMenu>																				<h:message for="examenSuficiencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Instituci�n Examen Suficiencia
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="100"
	    	    		                							maxlength="100"
            	    		        								value="#{elegibleIdiomaForm.elegibleIdioma.entidadSuficiencia}"
                	    		    								 rendered="#{elegibleIdiomaForm.showEntidadSuficienciaAux}"                     	    										readonly="#{!elegibleIdiomaForm.editing}"
                        											id="entidadSuficiencia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="entidadSuficiencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Examen Suficiencia
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaSuficiencia"
                		        									value="#{elegibleIdiomaForm.elegibleIdioma.fechaSuficiencia}"
                    		    									readonly="#{!elegibleIdiomaForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										 rendered="#{elegibleIdiomaForm.showFechaSuficienciaAux}"         	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="fechaSuficiencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Examen Acad�mico
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{elegibleIdiomaForm.elegibleIdioma.examenAcademico}"
        	                                        	        	disabled="#{!elegibleIdiomaForm.editing}"
            	                                        	    	immediate="false"
				                        							id="examenAcademico"
                	            									                    	        									 onchange="this.form.submit()" 	                        	                                	required="false"
    	                        	                            	title="Examen Acad�mico">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{elegibleIdiomaForm.listExamenAcademico}" />
					    	            	                        	            </h:selectOneMenu>																				<h:message for="examenAcademico" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Instituci�n Examen Acad�mico
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="100"
	    	    		                							maxlength="100"
            	    		        								value="#{elegibleIdiomaForm.elegibleIdioma.entidadAcademica}"
                	    		    								 rendered="#{elegibleIdiomaForm.showEntidadAcademicaAux}"                     	    										readonly="#{!elegibleIdiomaForm.editing}"
                        											id="entidadAcademica"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="entidadAcademica" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Examen Acad�mico
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaAcademica"
                		        									value="#{elegibleIdiomaForm.elegibleIdioma.fechaAcademica}"
                    		    									readonly="#{!elegibleIdiomaForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										 rendered="#{elegibleIdiomaForm.showFechaAcademicaAux}"         	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="fechaAcademica" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																						<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsElegibleIdioma"
            										rendered="#{elegibleIdiomaForm.showData}">
		                        					<f:verbatim><table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{elegibleIdiomaForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!elegibleIdiomaForm.editing&&!elegibleIdiomaForm.deleting&&elegibleIdiomaForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{elegibleIdiomaForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!elegibleIdiomaForm.editing&&!elegibleIdiomaForm.deleting&&elegibleIdiomaForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{elegibleIdiomaForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{elegibleIdiomaForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{elegibleIdiomaForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{elegibleIdiomaForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{elegibleIdiomaForm.editing}" />
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{elegibleIdiomaForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>
