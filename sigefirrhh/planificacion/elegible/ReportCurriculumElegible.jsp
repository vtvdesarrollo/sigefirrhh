<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportCurriculumElegible'].elements['formReportCurriculumElegible:reportId'].value;
			var name = 
				document.forms['formReportCurriculumElegible'].elements['formReportCurriculumElegible:reportName'].value;				
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}    	
    </script>
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>

<f:view>
	<x:saveState value="#{reportCurriculumElegibleForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	               
    	  	
    	  				
    				
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>

						<td valign="top">
							<h:form id="formReportCurriculumElegible">
							<h:inputHidden id="reportId" value="#{reportCurriculumElegibleForm.reportId}" />
							<h:inputHidden id="reportName" value="#{reportCurriculumElegibleForm.reportName}" />
    				
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Curriculum del Oferente/Elegible
											</b>
										</td>
										<td align="right">
											
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									<f:subview 
										id="searchIdioma"
										rendered="#{!reportCurriculumElegibleForm.showData&&
										!reportCurriculumElegibleForm.showAdd}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Oferente/Elegible
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{reportCurriculumElegibleForm.findElegibleCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
																onfocus="return deleteZero(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{reportCurriculumElegibleForm.findElegibleByCedula}" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{reportCurriculumElegibleForm.findElegiblePrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:inputText size="20"
																maxlength="20"
																value="#{reportCurriculumElegibleForm.findElegibleSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{reportCurriculumElegibleForm.findElegiblePrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:inputText size="20"
																maxlength="20"
																value="#{reportCurriculumElegibleForm.findElegibleSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{reportCurriculumElegibleForm.findElegibleByNombresApellidos}" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultElegible"
									rendered="#{reportCurriculumElegibleForm.showResultElegible&&
									!reportCurriculumElegibleForm.showData&&
									!reportCurriculumElegibleForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Elegible
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <h:dataTable id="tableElegible"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultElegible"
	                                				value="#{reportCurriculumElegibleForm.resultElegible}"
    	                                            
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultElegible}"
    	                            						action="#{reportCurriculumElegibleForm.selectElegible}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idElegible" value="#{resultElegible.idElegible}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </h:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tableElegible"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url=".gif" arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url=".gif" arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url=".gif" arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url=".gif" arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url=".gif" arrow-ff.gif"  />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url=".gif" arrow-fr.gif"  />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tableElegible"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedElegible"
									rendered="#{reportCurriculumElegibleForm.selectedElegible}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Elegible:&nbsp;</f:verbatim>
            									<h:commandLink value="#{reportCurriculumElegibleForm.elegible}"
    	                            						action="#{reportCurriculumElegibleForm.findElegibleByCedula}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idElegible" value="#{reportCurriculumElegibleForm.elegible.idElegible}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
    														<h:commandButton image="/images/report.gif"
			    											action="#{reportCurriculumElegibleForm.runReport}"
    														onclick="windowReport()" />            												
                                        	    			
                                        	    			
                                        	    			<f:verbatim>
																<img
                                            	            	onclick="document.forms['formCancel'].elements['formCancel:abort'].click()"
                                                	     		style="cursor:hand"
                                                    	        src=".gif" cancel.gif" />
                                                    	    </f:verbatim>
										    				
                                        	    			<f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								
							</h:form>	
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<h:form id="formCancel">
    	<h:commandButton
    		id="abort"
	    	action="#{reportCurriculumElegibleForm.abort}"
    		style="visibility:hidden"
    		type="submit" />
    	<h:commandButton
    		id="abortUpdate"
	    	action="#{reportCurriculumElegibleForm.abortUpdate}"
    		style="visibility:hidden"
    		type="submit" />
	</h:form>
</f:view>

</body>
</html>
