<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">

<f:view>
	<x:saveState value="#{elegibleEducacionForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	               
    	   
    	  			
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formEducacion">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Registro de Oferentes/Elegibles: Educaci�n Formal
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/planificacion/elegible/ElegibleEducacion.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									<f:subview 
										id="searchEducacion"
										rendered="#{!elegibleEducacionForm.showData&&
										!elegibleEducacionForm.showAdd}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Oferente/Elegible
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{elegibleEducacionForm.findElegibleCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
																onfocus="return deleteZero(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{elegibleEducacionForm.findElegibleByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{elegibleEducacionForm.findElegiblePrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{elegibleEducacionForm.findElegibleSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{elegibleEducacionForm.findElegiblePrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{elegibleEducacionForm.findElegibleSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{elegibleEducacionForm.findElegibleByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultElegible"
									rendered="#{elegibleEducacionForm.showResultElegible&&
									!elegibleEducacionForm.showData&&
									!elegibleEducacionForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Elegible
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <h:dataTable id="tableElegible"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultElegible"
	                                				value="#{elegibleEducacionForm.resultElegible}"
    	                                            
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultElegible}"
    	                            						action="#{elegibleEducacionForm.findElegibleEducacionByElegible}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idElegible" value="#{resultElegible.idElegible}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </h:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tableElegible"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tableElegible"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedElegible"
									rendered="#{elegibleEducacionForm.selectedElegible&&
									!elegibleEducacionForm.showData&&
									!elegibleEducacionForm.editing&&
									!elegibleEducacionForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Elegible:&nbsp;</f:verbatim>
            									<h:commandLink value="#{elegibleEducacionForm.elegible}  <-- clic aqui"
    	                            						action="#{elegibleEducacionForm.findElegibleEducacionByElegible}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idElegible" value="#{elegibleEducacionForm.elegible.idElegible}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{elegibleEducacionForm.add}"
            													onclick="javascript:return clickMade()"
            													rendered="#{!elegibleEducacionForm.editing&&!elegibleEducacionForm.deleting&&elegibleEducacionForm.login.agregar}" />
                                        	    			
                                            	            	
                                        	    			
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{elegibleEducacionForm.abort}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />
			                	        						
                                                    	    <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultEducacion"
									rendered="#{elegibleEducacionForm.showResult&&
									!elegibleEducacionForm.showData&&
									!elegibleEducacionForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <h:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{elegibleEducacionForm.result}"
                                            	    
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{elegibleEducacionForm.selectElegibleEducacion}"
	                                						styleClass="listitem">
    		                            						<f:param name="idElegibleEducacion" value="#{result.idElegibleEducacion}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </h:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataEducacion"
								rendered="#{elegibleEducacionForm.showData||elegibleEducacionForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!elegibleEducacionForm.editing&&!elegibleEducacionForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{elegibleEducacionForm.editing&&!elegibleEducacionForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{elegibleEducacionForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{elegibleEducacionForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="elegibleEducacionForm">
	                                            <h:inputHidden id="scrollx" value="#{elegibleEducacionForm.scrollx}" />
    	                                        <h:inputHidden id="scrolly" value="#{elegibleEducacionForm.scrolly}" />
	    	        							<f:subview 
    	    	    								id="dataEducacion"
        	    									rendered="#{elegibleEducacionForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{elegibleEducacionForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!elegibleEducacionForm.editing&&!elegibleEducacionForm.deleting&&elegibleEducacionForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{elegibleEducacionForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!elegibleEducacionForm.editing&&!elegibleEducacionForm.deleting&&elegibleEducacionForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{elegibleEducacionForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{elegibleEducacionForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{elegibleEducacionForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{elegibleEducacionForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{elegibleEducacionForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
						                        								action="#{elegibleEducacionForm.abortUpdate}"
						                        								immediate="true"
						                        								onclick="javascript:return clickMade()"
						                	        							 />
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Elegible
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{elegibleEducacionForm.elegible}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
																																																																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Nivel Educativo
	            	            							</td>
		                        							<td></f:verbatim>
									    		                    <h:selectOneMenu value="#{elegibleEducacionForm.selectNivelEducativo}"
        		                                                	disabled="#{!elegibleEducacionForm.editing}"
            		                                            	immediate="false"
                		            								onchange="saveScrollCoordinates();this.form.submit()" 
                		            								valueChangeListener="#{elegibleEducacionForm.changeNivelEducativoForTitulo}"                        		                                	required="true"
    																title="Nivel Educativo">
    																
                                    		                    	<f:selectItems value="#{elegibleEducacionForm.colNivelEducativo}" />
																	<f:validator validatorId="eforserver.RequiredJSFValidator"/>
								                                   </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									A�o Inicio
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="4"
	    	    		                							maxlength="4"
            	    		        								value="#{elegibleEducacionForm.elegibleEducacion.anioInicio}"
                	    		    								readonly="#{!elegibleEducacionForm.editing}"
                        											id="AniooInicio"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onblur="javascript:fieldEmpty(this)"
                        											onkeypress="return keyIntegerCheck(event, this)">
																	 
								                        									</h:inputText>
                        			                        								<f:verbatim>(aaaa)<span class="required"> *</span></f:verbatim>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									A�o Fin
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="4"
	    	    		                							maxlength="4"
            	    		        								value="#{elegibleEducacionForm.elegibleEducacion.anioFin}"
                	    		    								                    	    										readonly="#{!elegibleEducacionForm.editing}"
                        											id="AnioFin"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onblur="javascript:fieldEmpty(this)"
                        											onkeypress="return keyIntegerCheck(event, this)">
																	 
								                        									</h:inputText>
								                        									<f:verbatim>(aaaa)<span class="required"> </span></f:verbatim>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Estatus
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{elegibleEducacionForm.elegibleEducacion.estatus}"
        	                                        	        	disabled="#{!elegibleEducacionForm.editing}"
            	                                        	    	immediate="false"
                	            									                    	        									 onchange="saveScrollCoordinates();this.form.submit()" 	                        	                                	required="true"
    	                        	                            	title="Estatus">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{elegibleEducacionForm.listEstatus}" />
																						<f:validator validatorId="eforserver.RequiredJSFValidator"/>
					    	            	                        	            </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>				        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Carrera
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{elegibleEducacionForm.selectCarrera}"
        		                                                	disabled="#{!elegibleEducacionForm.editing}"
            		                                            	immediate="false"
                		            								 rendered="#{elegibleEducacionForm.showCarreraAux}"                     		        								                        		                                	required="false"
    																title="Carrera">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{elegibleEducacionForm.colCarrera}" />
								                                    	                </h:selectOneMenu>									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>													
										                  <f:verbatim><tr>
    	                    								<td>
        	                									T�tulo Obtenido
	            	            							</td>
		                        							<td></f:verbatim>
										                        	                        	    <h:selectOneMenu value="#{elegibleEducacionForm.selectTitulo}"
		                        	                            	disabled="#{!elegibleEducacionForm.editing}"
    		                        	                        	rendered="#{elegibleEducacionForm.showTitulo
																	 && elegibleEducacionForm.showTituloAux  }"
            		                        	                	immediate="false"
                		                        	            	required="false"
																	title="T�tulo Obtenido">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                            		                        		<f:selectItems value="#{elegibleEducacionForm.colTitulo}" />
							    	                                	                </h:selectOneMenu>									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Registr� T�tulo
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{elegibleEducacionForm.elegibleEducacion.registroTitulo}"
        	                                        	        	disabled="#{!elegibleEducacionForm.editing}"
            	                                        	    	immediate="false"
                	            									 rendered="#{elegibleEducacionForm.showRegistroTituloAux}"                     	        									 onchange="saveScrollCoordinates();this.form.submit()" 	                        	                                	required="false"
    	                        	                            	title="Registr� T�tulo">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{elegibleEducacionForm.listRegistroTitulo}" />
					    	            	                        	            </h:selectOneMenu>				        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Registro
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaRegistro"
                		        									value="#{elegibleEducacionForm.elegibleEducacion.fechaRegistro}"
                    		    									readonly="#{!elegibleEducacionForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
    	                    										 rendered="#{elegibleEducacionForm.showFechaRegistroAux}"         	             
    	                    										 required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Entidad Educativa
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{elegibleEducacionForm.elegibleEducacion.nombreEntidad}"
                	    		    								                    	    										readonly="#{!elegibleEducacionForm.editing}"
                        											id="EntidadEducativa"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																	 
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Nombre Postgrado
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{elegibleEducacionForm.elegibleEducacion.nombrePostgrado}"
                	    		    								 rendered="#{elegibleEducacionForm.showNombrePostgradoAux}"                     	    										readonly="#{!elegibleEducacionForm.editing}"
                        											id="NombrePostgrado"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																	 
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																							                        						<f:verbatim><tr>
    		                    							<td>
        		                								Pais
	            		            						</td>
    	            		        						<td></f:verbatim>
						                	        	                            	<h:selectOneMenu value="#{elegibleEducacionForm.selectPaisForCiudad}"
                        	    	                        		disabled="#{!elegibleEducacionForm.editing}"
	                    	            	                    	onchange="saveScrollCoordinates();this.form.submit()"
																	valueChangeListener="#{elegibleEducacionForm.changePaisForCiudad}"
        	                																											required="false"
																	immediate="false"
																	title="Ciudad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleEducacionForm.colPaisForCiudad}" />
																							</h:selectOneMenu>																					<f:verbatim></td>
														</tr></f:verbatim>
							                        						<f:verbatim><tr>
    		                    							<td>
        		                								Estado
	            		            						</td>
    	            		        						<td></f:verbatim>
																						<h:selectOneMenu value="#{elegibleEducacionForm.selectEstadoForCiudad}"
            	            		                            	disabled="#{!elegibleEducacionForm.editing}"
                	            		                        	onchange="saveScrollCoordinates();this.form.submit()"
																	valueChangeListener="#{elegibleEducacionForm.changeEstadoForCiudad}"
																	rendered="#{elegibleEducacionForm.showEstadoForCiudad
																	 }"
																	immediate="false"
																	required="false"
																	title="Ciudad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleEducacionForm.colEstadoForCiudad}" />
																							</h:selectOneMenu>																					<f:verbatim></td>
														</tr></f:verbatim>
										                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Ciudad
	            	            							</td>
		                        							<td></f:verbatim>
										                        	                        	    <h:selectOneMenu value="#{elegibleEducacionForm.selectCiudad}"
		                        	                            	disabled="#{!elegibleEducacionForm.editing}"
    		                        	                        	rendered="#{elegibleEducacionForm.showCiudad
																	 }"
            		                        	                	immediate="false"
                		                        	            	required="false"
																	title="Ciudad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                            		                        		<f:selectItems value="#{elegibleEducacionForm.colCiudad}" />
							    	                                	                </h:selectOneMenu>									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Sector
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{elegibleEducacionForm.elegibleEducacion.sector}"
        	                                        	        	disabled="#{!elegibleEducacionForm.editing}"
            	                                        	    	immediate="false"
                	            									                    	        										                        	                                	required="false"
    	                        	                            	title="Sector">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{elegibleEducacionForm.listSector}" />
					    	            	                        	            </h:selectOneMenu>				        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Escala
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{elegibleEducacionForm.elegibleEducacion.escala}"
                	    		    								                    	    										readonly="#{!elegibleEducacionForm.editing}"
                        											id="Escala"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="Escala" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Calificacion
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{elegibleEducacionForm.elegibleEducacion.calificacion}"
                	    		    								                    	    										readonly="#{!elegibleEducacionForm.editing}"
                        											id="Calificacion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="Calificacion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Mencion
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="25"
	    	    		                							maxlength="25"
            	    		        								value="#{elegibleEducacionForm.elegibleEducacion.mencion}"
                	    		    								                    	    										readonly="#{!elegibleEducacionForm.editing}"
                        											id="Mencion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="Mencion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Becado
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{elegibleEducacionForm.elegibleEducacion.becado}"
        	                                        	        	disabled="#{!elegibleEducacionForm.editing}"
            	                                        	    	immediate="false"
                	            									onchange="saveScrollCoordinates();this.form.submit()" 	
                	            									required="false"
    	                        	                            	title="Becado">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{elegibleEducacionForm.listBecado}" />
					    	            	                        	            </h:selectOneMenu>				        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Organizaci�n que otorga beca
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="100"
	    	    		                							maxlength="100"
            	    		        								value="#{elegibleEducacionForm.elegibleEducacion.organizacionBecaria}"
                	    		    								 rendered="#{elegibleEducacionForm.showOrganizacionBecariaAux}"                     	    										readonly="#{!elegibleEducacionForm.editing}"
                        											id="Organizacionqueotorgabeca"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																	 
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Reembolso por Organismo
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{elegibleEducacionForm.elegibleEducacion.reembolso}"
        	                                        	        	disabled="#{!elegibleEducacionForm.editing}"
            	                                        	    	immediate="false"
                	            									                    	        										                        	                                	required="false"
    	                        	                            	title="Reembolso por Organismo">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{elegibleEducacionForm.listReembolso}" />
					    	            	                        	            </h:selectOneMenu>				        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    							                        		<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="3"
    	        		                							id="Observaciones"
                    		        								                        		    								value="#{elegibleEducacionForm.elegibleEducacion.observaciones}"
                            										readonly="#{!elegibleEducacionForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										onkeypress="return keyEnterCheck(event, this)"
                            										required="false" />
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																								<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsEducacion"
            										rendered="#{elegibleEducacionForm.showData}">
		                        					<f:verbatim><table>
    		                    						<tr>
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{elegibleEducacionForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!elegibleEducacionForm.editing&&!elegibleEducacionForm.deleting&&elegibleEducacionForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{elegibleEducacionForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!elegibleEducacionForm.editing&&!elegibleEducacionForm.deleting&&elegibleEducacionForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{elegibleEducacionForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{elegibleEducacionForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{elegibleEducacionForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{elegibleEducacionForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{elegibleEducacionForm.editing}" />	
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{elegibleEducacionForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>
