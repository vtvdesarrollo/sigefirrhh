<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{elegibleForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                
    	  	 
    	  				
    				
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formElegible">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Registro de Oferentes/Elegibles: Datos Personales
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/planificacion/elegible/Elegible.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{elegibleForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!elegibleForm.editing&&!elegibleForm.deleting&&elegibleForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="search1"
								rendered="#{!elegibleForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{elegibleForm.findElegibleCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{elegibleForm.findElegibleByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td class="datatable">
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{elegibleForm.findElegiblePrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{elegibleForm.findElegibleSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td class="datatable">
                        							Apellidos
                        						</td>
                        						<td>
												</f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{elegibleForm.findElegiblePrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:inputText size="20"
														maxlength="20"
														value="#{elegibleForm.findElegibleSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{elegibleForm.findElegibleByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultElegible"
								rendered="#{elegibleForm.showElegibleByCedula&&
								!elegibleForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{elegibleForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{elegibleForm.selectElegible}"
                                						styleClass="listitem">
                                						<f:param name="idElegible" value="#{result.idElegible}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataElegible"
								rendered="#{elegibleForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!elegibleForm.editing&&!elegibleForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{elegibleForm.editing&&!elegibleForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{elegibleForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{elegibleForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
											<h:form id="elegibleForm">
											<h:inputHidden id="scrollx" value="#{elegibleForm.scrollx}" />
											<h:inputHidden id="scrolly" value="#{elegibleForm.scrolly}" />
                            				<f:verbatim>
										<table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{elegibleForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!elegibleForm.editing&&!elegibleForm.deleting&&elegibleForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{elegibleForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!elegibleForm.editing&&!elegibleForm.deleting&&elegibleForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{elegibleForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{elegibleForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{elegibleForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{elegibleForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{elegibleForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{elegibleForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr>
											</f:verbatim>
                        					<f:verbatim>
											<tr>
												<td width="20%" class="datatable">
													C�dula
												</td>
												<td width="80%">
													</f:verbatim>
														<h:inputText
															size="8"
															id="Cedula"
															rendered="#{elegibleForm.adding}"
															onkeypress="return keyIntegerCheck(event, this)"
															onfocus="return deleteZero(event, this)"
															maxlength="8"
															value="#{elegibleForm.elegible.cedula}"
															readonly="#{!elegibleForm.editing}"
															onchange="javascript:this.value=this.value.toUpperCase()"                        								
															required="true" />
															<h:inputText
															size="8"
															id="CedulaDisable"
															rendered="#{!elegibleForm.adding}"
															onkeypress="return keyIntegerCheck(event, this)"
															maxlength="8"
															value="#{elegibleForm.elegible.cedula}"
															readonly="true"
															onchange="javascript:this.value=this.value.toUpperCase()"                        								
															required="true" />
														<f:verbatim><span class="required"> *</span></f:verbatim>                        						
													<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
                        					<f:verbatim>
											<tr>
												<td colspan="2">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td width="20%" class="datatable">
																Primer Apellido
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="20"
																	id="PrimerApellido"
																	maxlength="20"
																	value="#{elegibleForm.elegible.primerApellido}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"                        								
																	required="true" />
																<f:verbatim><span class="required"> *</span></f:verbatim>
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Segundo Apellido
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="20"
																	id="SegundoApellido"
																	maxlength="20"
																	value="#{elegibleForm.elegible.segundoApellido}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"                        								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											</f:verbatim>
                        					<f:verbatim>
											<tr>
												<td colspan="2">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td width="20%" class="datatable">
																Primer Nombre
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="20"
																	id="PrimerNombre"
																	maxlength="20"
																	value="#{elegibleForm.elegible.primerNombre}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"                        								
																	required="true" />
																<f:verbatim><span class="required"> *</span></f:verbatim>
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Segundo Nombre
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="20"
																	id="SegundoNombre"
																	maxlength="20"
																	value="#{elegibleForm.elegible.segundoNombre}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"                        								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
															<td width="20%" class="datatable">
																Sexo
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.sexo}"
																	id="Sexo"
																	disabled="#{!elegibleForm.editing}"
																	immediate="false"
																	required="true"
																	title="Sexo">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listSexo}" />
																	<f:validator validatorId="eforserver.RequiredJSFValidator"/>
																</h:selectOneMenu>
																<f:verbatim><span class="required"> *</span></f:verbatim>
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Estado Civil
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.estadoCivil}"
																	id="EstadoCivil"
																	disabled="#{!elegibleForm.editing}"
																	
																	immediate="false"
																	required="true"
																	title="Estado Civil">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listEstadoCivil}" />
																	<f:validator validatorId="eforserver.RequiredJSFValidator"/>
																</h:selectOneMenu>
																<f:verbatim><span class="required"> *</span></f:verbatim>
															<f:verbatim>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
												<tr>
													<td height="5" colspan="2"></td>
												</tr>
												<tr>
													<td height="2" class="bandtable" colspan="2"></td>
												</tr>
												<tr>
													<td height="5" colspan="2"></td>
												</tr>
											</f:verbatim>
                        					<f:verbatim>
											<tr>
												<td colspan="2">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td width="20%" class="datatable">
																Fecha Nacimiento
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	id="FechaNacimiento"
																	size="10"
																	maxlength="10"
																	value="#{elegibleForm.elegible.fechaNacimiento}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"
																	onblur="javascript:check_date(this)"
																	required="true">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
																	pattern="dd-MM-yyyy" />
																	</h:inputText>
																<f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Nacionalidad
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.nacionalidad}"
																	id="Nacionalidad"
																	disabled="#{!elegibleForm.editing}"
																	immediate="false"
																	required="true"
																	title="Nacionalidad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listNacionalidad}" />
																	<f:validator validatorId="eforserver.RequiredJSFValidator"/>																	
																</h:selectOneMenu>
																<f:verbatim><span class="required"> *</span></f:verbatim>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Pais de Nacimiento
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.selectPaisForCiudadNacimiento}"
																	disabled="#{!elegibleForm.editing}"
																	onchange="saveScrollCoordinates();this.form.submit()"
																	valueChangeListener="#{elegibleForm.changePaisForCiudadNacimiento}"
																	immediate="false"
																	required="false"
																	title="Ciudad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.colPaisForCiudadNacimiento}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Estado de Nacimiento
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.selectEstadoForCiudadNacimiento}"
																	disabled="#{!elegibleForm.editing}"
																	onchange="saveScrollCoordinates();this.form.submit()"
																	valueChangeListener="#{elegibleForm.changeEstadoForCiudadNacimiento}"
																	rendered="#{elegibleForm.showEstadoForCiudadNacimiento}"
																	immediate="false"
																	required="false"
																	title="Ciudad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.colEstadoForCiudadNacimiento}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Ciudad de Nacimiento
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.selectCiudadNacimiento}"
																	id="CiudadNacimiento"
																	disabled="#{!elegibleForm.editing}"
																	rendered="#{elegibleForm.showCiudadNacimiento}"
																	immediate="false"
																	required="false"
																	title="Ciudad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.colCiudadNacimiento}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Doble Nacionalidad
															</td>
															<td width="30%" colspan="3">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.dobleNacionalidad}"
																	id="DobleNacionalidad"
																	disabled="#{!elegibleForm.editing}"
																	onchange="saveScrollCoordinates();this.form.submit()"                                                     	immediate="false"
																	required="false"
																	title="Doble Nacionalidad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listDobleNacionalidad}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Otra Nacionalidad
															</td>
															<td width="30%" colspan="3">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.selectPaisNacionalidad}"
																	id="PaisNacionalidad"
																	disabled="#{!elegibleForm.editing}"
																	 rendered="#{elegibleForm.showPaisNacionalidadAux}"                         								                                                    	immediate="false"
																	required="false"
																	title="Otra Nacionalidad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.colPaisNacionalidad}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Nacionalizado
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.nacionalizado}"
																	id="Nacionalizado"
																	disabled="#{!elegibleForm.editing}"
																	onchange="saveScrollCoordinates();this.form.submit()"                                                     	immediate="false"
																	required="false"
																	title="Nacionalizado">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listNacionalizado}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Fecha Nacionalizaci�n
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	id="FechaNacionalizacion"
																	size="10"
																	maxlength="10"
																	value="#{elegibleForm.elegible.fechaNacionalizacion}"
																	readonly="#{!elegibleForm.editing}"
																	onkeypress="return keyEnterCheck(event, this)"
																	onblur="javascript:check_date(this)"
																	 rendered="#{elegibleForm.showFechaNacionalizacionAux}"                         								onchange="javascript:this.value=this.value.toUpperCase()"
																	required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
																	pattern="dd-MM-yyyy" />
																</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Gaceta
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="10"
																	id="Gaceta"
																	maxlength="10"
																	value="#{elegibleForm.elegible.gacetaNacionalizacion}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"  
																	onkeypress="return keyEnterCheck(event, this)"                      								
																	rendered="#{elegibleForm.showGacetaNacionalizacionAux}"                         								
																	required="false" />                        						
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Otra Normativa
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="40"
																	id="OtraNormativa"
																	maxlength="40"
																	value="#{elegibleForm.elegible.otraNormativaNac}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onkeypress="return keyEnterCheck(event, this)"                       								
																	rendered="#{elegibleForm.showOtraNormativaNacAux}"
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											</f:verbatim>
											
											
											
											<f:verbatim><tr>
                        						<td>
                        							Fecha Registro
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="fechaRegistro"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{elegibleForm.elegible.fechaRegistro}"
                        								readonly="#{!elegibleForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onblur="javascript:check_date(this)"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
													<h:message for="fechaRegistro" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo que Aspira
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="50"
	                        							id="cargoAspira"
	                        							maxlength="50"
                        								value="#{elegibleForm.elegible.cargoAspira}"
                        								readonly="#{!elegibleForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="cargoAspira" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Sueldo que Aspira
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="sueldoAspira"
	                        							maxlength="10"
                        								value="#{elegibleForm.elegible.sueldoAspira}"
                        								readonly="#{!elegibleForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="sueldoAspira" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Disponibilidad
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="20"
	                        							id="disponibilidad"
	                        							maxlength="20"
                        								value="#{elegibleForm.elegible.disponibilidad}"
                        								readonly="#{!elegibleForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="disponibilidad" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							�Trabaja Actualmente?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{elegibleForm.elegible.trabajandoActualmente}"
	                                                    id="trabajandoActualmente"
                                                    	disabled="#{!elegibleForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="�Trabaja Actualmente?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{elegibleForm.listTrabajandoActualmente}" />
                                                    </h:selectOneMenu>													<h:message for="trabajandoActualmente" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Donde trabaja o trabaj� la ultima vez
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="80"
	                        							id="trabajoActual"
	                        							maxlength="80"
                        								value="#{elegibleForm.elegible.trabajoActual}"
                        								readonly="#{!elegibleForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="trabajoActual" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Que cargo tiene o tuvo la ultima vez
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="50"
	                        							id="cargoActual"
	                        							maxlength="50"
                        								value="#{elegibleForm.elegible.cargoActual}"
                        								readonly="#{!elegibleForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="cargoActual" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Que sueldo tiene o tuvo la ultima vez
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="sueldoActual"
	                        							maxlength="10"
                        								value="#{elegibleForm.elegible.sueldoActual}"
                        								readonly="#{!elegibleForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="sueldoActual" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Motivo Retiro
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="30"
	                        							id="motivoRetiro"
	                        							maxlength="30"
                        								value="#{elegibleForm.elegible.motivoRetiro}"
                        								readonly="#{!elegibleForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="motivoRetiro" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											
											
											<f:verbatim>
											<tr>
												<td height="5" colspan="2"></td>
											</tr>
											<tr>
												<td height="2" class="bandtable" colspan="2"></td>
											</tr>
											<tr>
												<td height="5" colspan="2"></td>
											</tr>
											</f:verbatim>
                        					<f:verbatim>
											<tr>
												<td colspan="2">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td width="20%" class="datatable">
																Direcci�n
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:inputText
																	size="100"
																	id="Direccion"
																	maxlength="100"
																	value="#{elegibleForm.elegible.direccionResidencia}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onkeypress="return keyEnterCheck(event, this)"                       								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Zona Postal
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:inputText
																	size="6"
																	id="ZonaPostal"
																	maxlength="6"
																	value="#{elegibleForm.elegible.zonaPostalResidencia}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onkeypress="return keyEnterCheck(event, this)"                       								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Pais de Residencia
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.selectPaisForCiudadResidencia}"
																	disabled="#{!elegibleForm.editing}"
																	onchange="saveScrollCoordinates();this.form.submit()"
																	valueChangeListener="#{elegibleForm.changePaisForCiudadResidencia}"
																	immediate="false"
																	required="false"
																	title="Ciudad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.colPaisForCiudadResidencia}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Estado de Residencia
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.selectEstadoForCiudadResidencia}"
																	disabled="#{!elegibleForm.editing}"
																	onchange="saveScrollCoordinates();this.form.submit()"
																	valueChangeListener="#{elegibleForm.changeEstadoForCiudadResidencia}"
																	rendered="#{elegibleForm.showEstadoForCiudadResidencia}"
																	immediate="false"
																	required="false"
																	title="Ciudad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.colEstadoForCiudadResidencia}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Ciudad de Residencia
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.selectCiudadResidencia}"
																	id="CiudadResidencia"
																	disabled="#{!elegibleForm.editing}"
																	rendered="#{elegibleForm.showCiudadResidencia}"
																	immediate="false"
																	required="false"
																	title="Ciudad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.colCiudadResidencia}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Municipio de Residencia
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.selectMunicipioForParroquia}"
																	disabled="#{!elegibleForm.editing}"
																	onchange="saveScrollCoordinates();this.form.submit()"
																	valueChangeListener="#{elegibleForm.changeMunicipioForParroquia}"
																	rendered="#{elegibleForm.showMunicipioForParroquia}"
																	immediate="false"
																	required="false"
																	title="Parroquia">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.colMunicipioForParroquia}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Parroquia de Residencia
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.selectParroquia}"
																	disabled="#{!elegibleForm.editing}"																																		
																	rendered="#{elegibleForm.showParroquia}"
																	immediate="false"
																	required="false"
																	title="Parroquia">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.colParroquia}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Tel�fono
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="15"
																	id="Telefono"
																	maxlength="15"
																	value="#{elegibleForm.elegible.telefonoResidencia}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onkeypress="return keyEnterCheck(event, this)"                       								
																	required="false" />
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Celular
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="15"
																	id="Celular"
																	maxlength="15"
																	value="#{elegibleForm.elegible.telefonoCelular}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"                        								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Telf Oficina
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:inputText
																	size="15"
																	id="TelfOficina"
																	maxlength="15"
																	value="#{elegibleForm.elegible.telefonoOficina}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onkeypress="return keyEnterCheck(event, this)"                       								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Email
															</td>
															<td width="80%" colspan="3">
															</f:verbatim>
																<h:inputText
																	size="60"
																	id="Email"
																	maxlength="60"
																	value="#{elegibleForm.elegible.email}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"  
																	onkeypress="return keyEnterCheck(event, this)"                      								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
												<tr>
													<td height="5" colspan="2"></td>
												</tr>
												<tr>
													<td height="2" class="bandtable" colspan="2"></td>
												</tr>
												<tr>
													<td height="5" colspan="2"></td>
												</tr>
											
												<tr>
    												<td width="20%" class="datatable">
    													A�os de Servicio en APN antes de Ingreso en Organismo
    												</td>
    												<td width="30%">
    												</f:verbatim>
    												<h:inputText size="3"
    													maxlength="2"
    													value="#{elegibleForm.elegible.aniosServicioApn}"
    													onkeypress="return keyIntegerCheck(event, this)"
    													onfocus="return deleteZero(event, this)"
    													onblur="javascript:fieldEmpty(this)"
    													onchange="javascript:this.value=this.value.toUpperCase()" />
    											<f:verbatim>
    											</tr>
											
												<tr>
													<td height="5" colspan="2"></td>
												</tr>
												<tr>
													<td height="2" class="bandtable" colspan="2"></td>
												</tr>
												<tr>
													<td height="5" colspan="2"></td>
												</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td colspan="2">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td width="20%" class="datatable">
																Tipo Vivienda
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.tipoVivienda}"
																	id="TipoVivienda"
																	disabled="#{!elegibleForm.editing}"
																																													immediate="false"
																	required="false"
																	title="Tipo Vivienda">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listTipoVivienda}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Tenencia Vivienda
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.tenenciaVivienda}"
																	id="TenenciaVivienda"
																	disabled="#{!elegibleForm.editing}"
																	immediate="false"
																	required="false"
																	title="Tenencia Vivienda">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listTenenciaVivienda}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Maneja
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.maneja}"
																	id="Maneja"
																	disabled="#{!elegibleForm.editing}"
																	immediate="false"
																	required="false"
																	title="Maneja">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listManeja}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Grado Licencia
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="10"
																	id="GradoLicencia"
																	maxlength="10"
																	value="#{elegibleForm.elegible.gradoLicencia}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onblur="javascript:fieldEmpty(this)"   
																	onkeypress="return keyIntegerCheck(event, this)"                     								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Veh�culo
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.tieneVehiculo}"
																	id="TieneVehiculo"
																	disabled="#{!elegibleForm.editing}"
																	onchange="saveScrollCoordinates();this.form.submit()"                                                     	immediate="false"
																	required="false"
																	title="Veh�culo">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listTieneVehiculo}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Marca
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="20"
																	id="Marca"
																	maxlength="20"
																	value="#{elegibleForm.elegible.marcaVehiculo}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onkeypress="return keyEnterCheck(event, this)"                       								
																	rendered="#{elegibleForm.showMarcaVehiculoAux}"
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
														<tr>
															<td width="20%" class="datatable">
																Modelo
															</td>
															<td width="30">
															</f:verbatim>
																<h:inputText
																	size="20"
																	id="Modelo"
																	maxlength="20"
																	value="#{elegibleForm.elegible.modeloVehiculo}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onkeypress="return keyEnterCheck(event, this)"                       								
																	rendered="#{elegibleForm.showModeloVehiculoAux}"
																	required="false" />
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Placa
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="10"
																	id="Placa"
																	maxlength="10"
																	value="#{elegibleForm.elegible.placaVehiculo}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onkeypress="return keyEnterCheck(event, this)"                       								
																	rendered="#{elegibleForm.showPlacaVehiculoAux}"
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
												<tr>
													<td height="5" colspan="2"></td>
												</tr>
												<tr>
													<td height="2" class="bandtable" colspan="2"></td>
												</tr>
												<tr>
													<td height="5" colspan="2"></td>
												</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td colspan="2">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td width="20%" class="datatable">
																N�SSO
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="15"
																	id="NoSSO"
																	maxlength="15"
																	value="#{elegibleForm.elegible.numeroSso}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"                        								
																	required="false" />
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																N�RIF
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="15"
																	id="NoRIF"
																	maxlength="15"
																	value="#{elegibleForm.elegible.numeroRif}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onkeypress="return keyEnterCheck(event, this)"                       								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
															<td width="20%" class="datatable">
																N�Libreta Militar
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="15"
																	id="NoLibretaMilitar"
																	maxlength="15"
																	value="#{elegibleForm.elegible.numeroLibretaMilitar}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onkeypress="return keyEnterCheck(event, this)"                       								
																	required="false" />
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Estatura
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="10"
																	id="Estatura"
																	maxlength="10"
																	value="#{elegibleForm.elegible.estatura}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onblur="javascript:fieldEmpty(this)"
																	onkeypress="return keyFloatCheck(event, this)"                       								
																	required="false" 
																	style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>																	
																</h:inputText>
															<f:verbatim>
															</td>
														</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
															<td width="20%" class="datatable">
																Peso
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="10"
																	id="Peso"
																	maxlength="10"
																	value="#{elegibleForm.elegible.peso}"
																	readonly="#{!elegibleForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()" 
																	onblur="javascript:fieldEmpty(this)"
																	onkeypress="return keyFloatCheck(event, this)"                       								
																	required="false"
																	style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
																	
																	</h:inputText>
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Diestralidad
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.diestralidad}"
																	id="Diestralidad"
																	disabled="#{!elegibleForm.editing}"
																	immediate="false"
																	required="false"
																	title="Diestralidad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listDiestralidad}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
															<td width="20%" class="datatable">
																Grupo sanguineo
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{elegibleForm.elegible.grupoSanguineo}"
																	id="GrupoSanguineo"
																	disabled="#{!elegibleForm.editing}"
																	immediate="false"
																	required="false"

																	title="Grupo sanguineo">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{elegibleForm.listGrupoSanguineo}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										</f:verbatim>
										<f:verbatim>
                        				<table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{elegibleForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!elegibleForm.editing&&!elegibleForm.deleting&&elegibleForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{elegibleForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!elegibleForm.editing&&!elegibleForm.deleting&&elegibleForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{elegibleForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{elegibleForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{elegibleForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{elegibleForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{elegibleForm.editing}" />
                            	        			<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{elegibleForm.abortUpdate}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim>
										</h:form>
                        				<f:verbatim>
										</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>
