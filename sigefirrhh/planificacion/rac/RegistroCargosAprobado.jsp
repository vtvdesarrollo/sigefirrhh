<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{registroCargosAprobadoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formRegistroCargosAprobado">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Componentes del Plan: Acciones Administrativas
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/planificacion/plan/RegistroCargosAprobado.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{registroCargosAprobadoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!registroCargosAprobadoForm.editing&&!registroCargosAprobadoForm.deleting&&registroCargosAprobadoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchRegistroCargosAprobado"
								rendered="#{!registroCargosAprobadoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Plan de Personal
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{registroCargosAprobadoForm.findSelectPlanPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosAprobadoForm.findColPlanPersonal}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{registroCargosAprobadoForm.findRegistroCargosAprobadoByPlanPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Registro
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{registroCargosAprobadoForm.findSelectRegistro}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosAprobadoForm.findColRegistro}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{registroCargosAprobadoForm.findRegistroCargosAprobadoByRegistro}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													C�digo N�mina
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="7"
														maxlength="7"
														value="#{registroCargosAprobadoForm.findCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{registroCargosAprobadoForm.findRegistroCargosAprobadoByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultRegistroCargosAprobadoByPlanPersonal"
								rendered="#{registroCargosAprobadoForm.showRegistroCargosAprobadoByPlanPersonal&&
								!registroCargosAprobadoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{registroCargosAprobadoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{registroCargosAprobadoForm.selectRegistroCargosAprobado}"
                                						styleClass="listitem">
                                						<f:param name="idRegistroCargosAprobado" value="#{result.idRegistroCargosAprobado}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultRegistroCargosAprobadoByRegistro"
								rendered="#{registroCargosAprobadoForm.showRegistroCargosAprobadoByRegistro&&
								!registroCargosAprobadoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{registroCargosAprobadoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{registroCargosAprobadoForm.selectRegistroCargosAprobado}"
                                						styleClass="listitem">
                                						<f:param name="idRegistroCargosAprobado" value="#{result.idRegistroCargosAprobado}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultRegistroCargosAprobadoByCodigoNomina"
								rendered="#{registroCargosAprobadoForm.showRegistroCargosAprobadoByCodigoNomina&&
								!registroCargosAprobadoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{registroCargosAprobadoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{registroCargosAprobadoForm.selectRegistroCargosAprobado}"
                                						styleClass="listitem">
                                						<f:param name="idRegistroCargosAprobado" value="#{result.idRegistroCargosAprobado}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataRegistroCargosAprobado"
								rendered="#{registroCargosAprobadoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!registroCargosAprobadoForm.editing&&!registroCargosAprobadoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{registroCargosAprobadoForm.editing&&!registroCargosAprobadoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{registroCargosAprobadoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{registroCargosAprobadoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="registroCargosAprobadoForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{registroCargosAprobadoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!registroCargosAprobadoForm.editing&&!registroCargosAprobadoForm.deleting&&registroCargosAprobadoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{registroCargosAprobadoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!registroCargosAprobadoForm.editing&&!registroCargosAprobadoForm.deleting&&registroCargosAprobadoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{registroCargosAprobadoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{registroCargosAprobadoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{registroCargosAprobadoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{registroCargosAprobadoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{registroCargosAprobadoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{registroCargosAprobadoForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Plan de Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosAprobadoForm.selectPlanPersonal}"
														id="planPersonal"
                                                    	disabled="#{!registroCargosAprobadoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Plan de Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosAprobadoForm.colPlanPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="planPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Registro
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosAprobadoForm.selectRegistro}"
														id="registro"
                                                    	disabled="#{!registroCargosAprobadoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Registro">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosAprobadoForm.colRegistro}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="registro" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Acci�n Administrativa
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosAprobadoForm.registroCargosAprobado.accion}"
	                                                    id="accion"
                                                    	disabled="#{!registroCargosAprobadoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Acci�n Administrativa">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosAprobadoForm.listAccion}" />
                                                    </h:selectOneMenu>													<h:message for="accion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Realizado
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosAprobadoForm.registroCargosAprobado.estatus}"
	                                                    id="estatus"
                                                    	disabled="#{!registroCargosAprobadoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Realizado">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosAprobadoForm.listEstatus}" />
                                                    </h:selectOneMenu>													<h:message for="estatus" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="fechaAccion"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{registroCargosAprobadoForm.registroCargosAprobado.fechaAccion}"
                        								readonly="#{!registroCargosAprobadoForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onblur="javascript:check_date(this)"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
													<h:message for="fechaAccion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�digo N�mina
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="7"
	                        							id="codigoNomina"
	                        							maxlength="7"
                        								value="#{registroCargosAprobadoForm.registroCargosAprobado.codigoNomina}"
                        								readonly="#{!registroCargosAprobadoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="codigoNomina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Clasificador de Cargos
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosAprobadoForm.selectManualCargoForCargo}"
                                                    	disabled="#{!registroCargosAprobadoForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="ManualCargoForCargo"
														valueChangeListener="#{registroCargosAprobadoForm.changeManualCargoForCargo}"
                        																						immediate="false"
														required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{registroCargosAprobadoForm.colManualCargoForCargo}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosAprobadoForm.selectCargo}"
	                                                    id="cargo"
                                                    	disabled="#{!registroCargosAprobadoForm.editing}"
                                                    	rendered="#{registroCargosAprobadoForm.showCargo
														 }"
                                                    	immediate="false"
                                                    	required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{registroCargosAprobadoForm.colCargo}" />
                                                    </h:selectOneMenu>													<h:message for="cargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Dependencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosAprobadoForm.selectDependencia}"
														id="dependencia"
                                                    	disabled="#{!registroCargosAprobadoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosAprobadoForm.colDependencia}" />
                                                    </h:selectOneMenu>													<h:message for="dependencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Region
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosAprobadoForm.selectRegion}"
														id="region"
                                                    	disabled="#{!registroCargosAprobadoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Region">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosAprobadoForm.colRegion}" />
                                                    </h:selectOneMenu>													<h:message for="region" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Horas
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="horas"
	                        							maxlength="4"
                        								value="#{registroCargosAprobadoForm.registroCargosAprobado.horas}"
                        								readonly="#{!registroCargosAprobadoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="horas" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{registroCargosAprobadoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!registroCargosAprobadoForm.editing&&!registroCargosAprobadoForm.deleting&&registroCargosAprobadoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{registroCargosAprobadoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!registroCargosAprobadoForm.editing&&!registroCargosAprobadoForm.deleting&&registroCargosAprobadoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{registroCargosAprobadoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{registroCargosAprobadoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{registroCargosAprobadoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{registroCargosAprobadoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{registroCargosAprobadoForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{registroCargosAprobadoForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>