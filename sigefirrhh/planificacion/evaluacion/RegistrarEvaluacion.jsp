<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{registrarEvaluacionForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			
			<td width="570" valign="top">
				<h:form id="formRegistrarEvaluacion" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Registrar Evaluaciones
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/planificacion/evaluacion/RegistrarEvaluacion.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
            					image="/images/close.gif"
            					action="go_cancelOption"
            					immediate="true"                        					
            					onclick="javascript:return clickMade()"
    	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    							<table width="100%" class="datatable">
        							<tr>
    									<td width="30%">
    										Tipo Personal
    									</td>
    									<td width="70%">
    										<h:selectOneMenu value="#{registrarEvaluacionForm.selectIdTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{registrarEvaluacionForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{registrarEvaluacionForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										A�o
    									</td>
    									<td>
    										<h:inputText
                       							size="5"
                       							id="anio"
                       							maxlength="4"
                      								value="#{registrarEvaluacionForm.anio}"   						
												onfocus="return deleteZero(event, this)"
												onblur="javascript:fieldEmpty(this)"
                      								onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyIntegerCheck(event, this)" />
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Semestre
    									</td>
    									<td>
    										<h:selectOneMenu value="#{registrarEvaluacionForm.mes}"    											
    											immediate="false">
                                            	<f:selectItem itemLabel="Primer" itemValue="1" />
                                            	<f:selectItem itemLabel="Segundo" itemValue="2" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    									<f:verbatim>
    								</tr>
    							</table>
    							</f:verbatim>
    							<f:subview id="showEmpleados" rendered="#{registrarEvaluacionForm.showEmpleados}">
    							<f:verbatim>
    							<table width="100%" class="datatable">
    								<tr>
    									<td width="20%">
    										C�dula
    									</td>
    									<td width="30%">
        								</f:verbatim>
											<h:inputText
												size="8"
												id="Cedula"
												maxlength="8"
												value="#{registrarEvaluacionForm.cedula}"   						
												onfocus="return deleteZero(event, this)"
												onblur="javascript:fieldEmpty(this)"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyIntegerCheck(event, this)" />
    									<f:verbatim>
    									</td>
    									<td width="20%">
    										Nivel
    									</td>
    									<td width="30%">
										</f:verbatim>	
											<h:selectOneMenu value="#{registrarEvaluacionForm.nivelEmpleado}"
												immediate="false">
											<f:selectItems value="#{registrarEvaluacionForm.listNivelEmpleado}" />
											</h:selectOneMenu>
										<f:verbatim>
    									</td>
    								</tr>
    								<tr>
    									<td>
        									Competencias
        								</td>
        								<td>
										</f:verbatim>	
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.competencias}"	                        								
												id="Competencias"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right" >
											<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Objetivos
										</td>
										<td>
										</f:verbatim>	
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.objetivos}"	                        								
												id="Objetivos"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right" >
											<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Cedula Supervisor
										</td>
										<td>
										</f:verbatim>	
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.cedulaSupervisor}"	                        								
												id="CedulaSupervisor"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyIntegerCheck(event, this)"
												style="text-align:right" >
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Nombre Supervisor
										</td>
										<td>
										</f:verbatim>	
											<h:inputText
												size="20"
												maxlength="25"
												value="#{registrarEvaluacionForm.nombreSupervisor}"	                        								
												id="NombreSupervisor"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()">
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Cedula Jefe
										</td>
										<td>
										</f:verbatim>	
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.cedulaJefe}"	                        								
												id="CedulaJefe"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyIntegerCheck(event, this)"
												style="text-align:right" >
											</h:inputText>
										<f:verbatim>	
										</td>
										<td>
											Nombre Jefe
										</td>
										<td>
										</f:verbatim>	
											<h:inputText
												size="20"
												maxlength="25"
												value="#{registrarEvaluacionForm.nombreJefe}"	                        								
												id="NombreJefe"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()">
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td colspan="3"></td>
										<td>
										</f:verbatim>
											<h:commandButton value="Agregar"
												image="/images/save.gif"
												action="#{registrarEvaluacionForm.actualizar}" />
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td colspan="4">
										</f:verbatim>
 											<h:outputText
												value="#{registrarEvaluacionForm.cedulaFinal}" />
												&nbsp;
											<h:outputText
												value="#{registrarEvaluacionForm.nombre}" />
												&nbsp;
											<h:outputText
												value="#{registrarEvaluacionForm.apellido}" />
												&nbsp;
											<f:verbatim>
										</td>
									</tr>
								</table>
        						</f:verbatim>
								</f:subview>
								<f:subview id="showObreros" rendered="#{registrarEvaluacionForm.showObreros}">
								<f:verbatim>
								<table width="100%" class="datatable">
									<tr>
										<td width="20%">
											C�dula
										</td>
										<td width="30%">
        								</f:verbatim>
											<h:inputText
												size="8"
												id="Cedula"
												maxlength="8"
												value="#{registrarEvaluacionForm.cedula}"   						
												onfocus="return deleteZero(event, this)"
												onblur="javascript:fieldEmpty(this)"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyIntegerCheck(event, this)" />
										<f:verbatim>
										</td>
										<td width="20%">
											Nivel
										</td>
										<td width="30%">
										</f:verbatim>	
											<h:selectOneMenu value="#{registrarEvaluacionForm.nivelObrero}"
												immediate="false">
											<f:selectItems value="#{registrarEvaluacionForm.listNivelObrero}" />
											</h:selectOneMenu>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Manejo de Bienes y Equipos
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.manejoBienes}"	                        								
												id="ManejoBienes"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right" >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											H�bitos de Seguridad
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.habitoSeguridad}"	                        								
												id="HabitoSeguridad"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right" >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Calidad de Trabajo
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.calidadTrabajo}"	                        								
												id="CalidadTrabajo"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Cumplimiento de Normas
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.cumplimientoNormas}"	                        								
												id="CumplimientoNormas"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Atenci�n al P�blico
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.atencionPublico}"	                        								
												id="AtencionPublico"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Inter�s por el Trabajo
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.interesTrabajo}"	                        								
												id="InteresTrabajo"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Cooperaci�n
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.cooperacion}"	                        								
												id="Cooperacion"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Cantidad de Trabajo
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.cantidadTrabajo}"	                        								
												id="CantidadTrabajo"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Toma de Decisiones
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.tomaDecisiones}"	                        								
												id="TomaDecisiones"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Comunicaci�n con los Trabajadores
										</td>
										<td>
										</f:verbatim>	
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.comunicacion}"	                        								
												id="Comunicacion"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Capacidad de Mando
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.capacidadMando}"	                        								
												id="CapacidadMando"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Coordinaci�n del Trabajo
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.coordinacion}"	                        								
												id="Coordinacion"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Cedula Supervisor
										</td>
										<td>
										</f:verbatim>	
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.cedulaSupervisor}"	                        								
												id="CedulaSupervisor"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyIntegerCheck(event, this)"
												style="text-align:right" >
											</h:inputText>
										<f:verbatim>	
										</td>
										<td>
											Nombre Supervisor
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="20"
												maxlength="25"
												value="#{registrarEvaluacionForm.nombreSupervisor}"	                        								
												id="NombreSupervisor"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()" >
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Cedula Jefe
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{registrarEvaluacionForm.cedulaJefe}"	                        								
												id="CedulaJefe"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyIntegerCheck(event, this)"
												style="text-align:right" >
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Nombre Jefe
										</td>
										<td>
										</f:verbatim>	
											<h:inputText
												size="20"
												maxlength="25"
												value="#{registrarEvaluacionForm.nombreJefe}"	                        								
												id="NombreJefe"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()" >
											</h:inputText>
										<f:verbatim>	
										</td>
									</tr>
									<tr>
										<td colspan="3"></td>
										<td>
										</f:verbatim>
											<h:commandButton value="Agregar"
												image="/images/add.gif"
												action="#{registrarEvaluacionForm.actualizar}" />
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td colspan="4">
										</f:verbatim>
											<h:outputText
												value="#{registrarEvaluacionForm.cedulaFinal}" />
												&nbsp;
											<h:outputText
												value="#{registrarEvaluacionForm.nombre}" />
												&nbsp;
											<h:outputText
												value="#{registrarEvaluacionForm.apellido}" />
												&nbsp;	
										<f:verbatim>
										</td>
									</tr>
								</table>
								</f:verbatim>
								</f:subview>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>
