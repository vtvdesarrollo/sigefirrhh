<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportNoEvaluaciones'].elements['formReportNoEvaluaciones:reportId'].value;
			var name = 
				document.forms['formReportNoEvaluaciones'].elements['formReportNoEvaluaciones:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportNoEvaluacionesForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			
			<td width="570" valign="top">
				<h:form id="formReportNoEvaluaciones">
				
    				<h:inputHidden id="reportId" value="#{reportNoEvaluacionesForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportNoEvaluacionesForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reportes Trabajadores No Evaluados
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success" showDetail="true" />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    		
    								<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="Anio"
    											maxlength="4"
    											value="#{reportNoEvaluacionesForm.anio}"
    											required="true" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Semestre
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:selectOneMenu value="#{reportNoEvaluacionesForm.mes}"    											
    											immediate="false">
                                            	<f:selectItem itemLabel="Primer" itemValue="1" />
                                            	<f:selectItem itemLabel="Segundo" itemValue="2" />
                                            											
                                            </h:selectOneMenu>
    									</td>
									</tr>							    																	
    								<tr>
    									<td>
    										Tipo Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNoEvaluacionesForm.idTipoPersonal}"                                                    	
                                            	immediate="false"                        								                        								                                                    	
                                            	required="false"
                                            	valueChangeListener="#{reportNoEvaluacionesForm.changeTipoPersonal}"
	    										onchange="this.form.submit()"
												title="Tipo Personal">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNoEvaluacionesForm.colTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Motivo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNoEvaluacionesForm.motivo}"
                                            	immediate="false"
                                            	onchange="this.form.submit()">                                            	
                                                <f:selectItem itemLabel="Todos" itemValue="0" />                                      
                                                <f:selectItem itemLabel="Reposo" itemValue="1" /> 
                                                <f:selectItem itemLabel="Vacaciones" itemValue="2" />  
                                                <f:selectItem itemLabel="A la Orden de RRHH" itemValue="3" />                                      
                                                <f:selectItem itemLabel="Detenido" itemValue="4" /> 
                                                <f:selectItem itemLabel="Destituido" itemValue="5" />   
                                                <f:selectItem itemLabel="Comisi�n" itemValue="6" /> 
                                                <f:selectItem itemLabel="Fallecido" itemValue="7" />  
                                                <f:selectItem itemLabel="A la Orden de Inspector�a" itemValue="8" />                                      
                                                <f:selectItem itemLabel="Renuncia" itemValue="9" /> 
                                                <f:selectItem itemLabel="Per�odo de Prueba" itemValue="A" />    
                                                <f:selectItem itemLabel="Permiso Remunerado" itemValue="B" /> 
                                                <f:selectItem itemLabel="Permiso No Remunerado" itemValue="C" /> 
                                                <f:selectItem itemLabel="Otro" itemValue="D" />                                      
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								
    									    																					
    								<tr>
    									<td>
    										Ordenado
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNoEvaluacionesForm.orden}"
                                            	immediate="false"
                                            	onchange="this.form.submit()">                                            	
                                                <f:selectItem itemLabel="Alfabetico" itemValue="1" />                                      
                                                <f:selectItem itemLabel="C�dula" itemValue="2" /> 
                                                <f:selectItem itemLabel="C�digo" itemValue="3" />                                      
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNoEvaluacionesForm.formato}"
                                            	immediate="false"
                                            	onchange="this.form.submit()">                                            	
                                                <f:selectItem itemLabel="PDF" itemValue="1" />                                      
                                                <f:selectItem itemLabel="Hoja Calculo" itemValue="2" /> 
                                                                                  
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										
    											action="#{reportNoEvaluacionesForm.runReport}"
    											onclick="windowReport()" />
    										
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>
