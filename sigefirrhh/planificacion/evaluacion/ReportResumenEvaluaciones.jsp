<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportResumenEvaluaciones'].elements['formReportResumenEvaluaciones:reportId'].value;
			var name = 
				document.forms['formReportResumenEvaluaciones'].elements['formReportResumenEvaluaciones:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportResumenEvaluacionesForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
		
			<td width="570" valign="top">
				<h:form id="formReportResumenEvaluaciones">
				
    				<h:inputHidden id="reportId" value="#{reportResumenEvaluacionesForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportResumenEvaluacionesForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reportes Resumen de Evaluaciones 
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success" showDetail="true" />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    		
    								<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="Anio"
    											maxlength="4"
    											value="#{reportResumenEvaluacionesForm.anio}"
    											required="true" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="3"
    											id="Mes"
    											maxlength="2"
    											value="#{reportResumenEvaluacionesForm.mes}"
    											required="true" >    								
                	                        </h:inputText>
    									</td>
									</tr>							    																	
    								<tr>
    									<td>
    										Tipo Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportResumenEvaluacionesForm.idTipoPersonal}"                                                    	
                                            	immediate="false"                        								                        								                                                    	
                                            	required="false"
                                            	valueChangeListener="#{reportResumenEvaluacionesForm.changeTipoPersonal}"
	    										onchange="this.form.submit()"
												title="Tipo Personal">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportResumenEvaluacionesForm.colTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Seleccione
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportResumenEvaluacionesForm.seleccionUbicacion}"
	    										valueChangeListener="#{reportResumenEvaluacionesForm.changeSeleccionUbicacion}"
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Todos" itemValue="T" /> 
                                            	<f:selectItem itemLabel="Una Region" itemValue="R" />                                             	                                   	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								
    								<tr>
    									<td>
    										Regi�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportResumenEvaluacionesForm.idRegion}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reportResumenEvaluacionesForm.showRegion}"
                                            	>
                                            	
                                                <f:selectItems value="#{reportResumenEvaluacionesForm.colRegion}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			    																					
    								<tr>
    									<td>
    										Clasificaci�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportResumenEvaluacionesForm.clasificacion}"
                                            	immediate="false"
                                            	onchange="this.form.submit()">                                            	
                                                <f:selectItem itemLabel="General" itemValue="1" />                                      
                                                <f:selectItem itemLabel="Por nivel" itemValue="2" />                                      
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										
    											action="#{reportResumenEvaluacionesForm.runReport}"
    											onclick="windowReport()" />
    										
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>
