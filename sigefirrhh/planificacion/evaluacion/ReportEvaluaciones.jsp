<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportEvaluaciones'].elements['formReportEvaluaciones:reportId'].value;
			var name = 
				document.forms['formReportEvaluaciones'].elements['formReportEvaluaciones:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportEvaluacionesForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			
			<td width="570" valign="top">
				<h:form id="formReportEvaluaciones">
				
    				<h:inputHidden id="reportId" value="#{reportEvaluacionesForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportEvaluacionesForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reportes Detalle de Evaluaciones
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success" showDetail="true" />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    		
    								<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="Anio"
    											maxlength="4"
    											value="#{reportEvaluacionesForm.anio}"
    											required="true" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Semestre
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="3"
    											id="Mes"
    											maxlength="2"
    											value="#{reportEvaluacionesForm.mes}"
    											required="true" >    								
                	                        </h:inputText>
    									</td>
									</tr>							    																	
    								<tr>
    									<td width="30%">
    										Tipo Personal
    									</td>
    									<td width="70%">
    										<h:selectOneMenu value="#{reportEvaluacionesForm.selectIdTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{reportEvaluacionesForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{reportEvaluacionesForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Seleccione
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEvaluacionesForm.seleccionUbicacion}"
	    										valueChangeListener="#{reportEvaluacionesForm.changeSeleccionUbicacion}"
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Todos" itemValue="T" /> 
                                            	<f:selectItem itemLabel="Una Region" itemValue="R" /> 
                                            	<f:selectItem itemLabel="Una Dependencia" itemValue="D" /> 
                                            	<f:selectItem itemLabel="Una Unidad Funcional" itemValue="U" />                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Ubicaci�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEvaluacionesForm.tipo}"	    										
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Dependencia Nominal" itemValue="N" />  
                                            	<f:selectItem itemLabel="Dependencia Real" itemValue="R" />  
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td>
    										Regi�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEvaluacionesForm.idRegion}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reportEvaluacionesForm.showRegion}"
                                            	>
                                            	
                                                <f:selectItems value="#{reportEvaluacionesForm.colRegion}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    								<tr>
    									<td>
    										Dependencia
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEvaluacionesForm.idDependencia}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reportEvaluacionesForm.showDependencia}"
                                            	>
                                            	
                                                <f:selectItems value="#{reportEvaluacionesForm.colDependencia}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Unidad Funcional
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEvaluacionesForm.idUnidadFuncional}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reportEvaluacionesForm.showUnidadFuncional}"
                                            	>
                                            	
                                                <f:selectItems value="#{reportEvaluacionesForm.colUnidadFuncional}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Tipo Resultado
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEvaluacionesForm.idResultadoEvaluacion}"                                                    	
                                            	immediate="false"                        								                        								                                                    	
                                            	required="false"
                                            	rendered="#{reportEvaluacionesForm.showResultadoEvaluacion}"
												title="Resultado Evaluacion">
												<f:selectItem itemLabel="Todos" itemValue="0" />
                                                <f:selectItems value="#{reportEvaluacionesForm.colResultadoEvaluacion}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Ordenado por
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEvaluacionesForm.orden}"	    										
    										    immediate="false"
	    										onchange="this.form.submit()"
                                            	rendered="#{reportEvaluacionesForm.showOrdenado}">                                            	
                                            	<f:selectItem itemLabel="Alfabetico" itemValue="A" />  
                                            	<f:selectItem itemLabel="C�dula" itemValue="C" />  
                                            	<f:selectItem itemLabel="C�digo" itemValue="N" />                                        	
                                            	<f:selectItem itemLabel="Unidad Ejecutora" itemValue="U" />                                        	                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>		    								
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEvaluacionesForm.formato}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										
    											action="#{reportEvaluacionesForm.runReport}"
    											onclick="windowReport()" />
    										
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>
