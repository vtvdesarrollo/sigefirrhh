<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{actualizarEvaluacionForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formActualizarEvaluacion">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Actualizar/Eliminar Evaluaciones
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/planificacion/evaluacion/ActualizarEvaluacion.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success"  />
										</td>
									</tr>
									
									<f:subview 
										id="searchEvaluacion"
										rendered="#{!actualizarEvaluacionForm.showData&&
										!actualizarEvaluacionForm.showAdd&&
										!actualizarEvaluacionForm.login.servicioPersonal}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{actualizarEvaluacionForm.findPersonalCedula}"
																onkeypress="javascript:return keyIntegerCheck(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{actualizarEvaluacionForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarEvaluacionForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarEvaluacionForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarEvaluacionForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarEvaluacionForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{actualizarEvaluacionForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{actualizarEvaluacionForm.showResultPersonal&&
									!actualizarEvaluacionForm.showData&&
									!actualizarEvaluacionForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <x:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{actualizarEvaluacionForm.resultPersonal}"
    	                                            preserveDataModel="false"
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{actualizarEvaluacionForm.findEvaluacionByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </x:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{actualizarEvaluacionForm.selectedPersonal&&
									!actualizarEvaluacionForm.showData&&
									!actualizarEvaluacionForm.editing&&
									!actualizarEvaluacionForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{actualizarEvaluacionForm.personal}   <-- clic aqui"
    	                            						action="#{actualizarEvaluacionForm.findEvaluacionByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{actualizarEvaluacionForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												
                                        	    			<% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{actualizarEvaluacionForm.abort}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                                                    	    <%}%>
                                                    	   <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultEvaluacion"
									rendered="#{actualizarEvaluacionForm.showResult&&
									!actualizarEvaluacionForm.showData&&
									!actualizarEvaluacionForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <x:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{actualizarEvaluacionForm.result}"
                                            	    preserveDataModel="false"
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{actualizarEvaluacionForm.selectEvaluacion}"
	                                						styleClass="listitem">
    		                            						<f:param name="idEvaluacion" value="#{result.idEvaluacion}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </x:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataEvaluacion"
								rendered="#{actualizarEvaluacionForm.showData||actualizarEvaluacionForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!actualizarEvaluacionForm.editing&&!actualizarEvaluacionForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{actualizarEvaluacionForm.editing&&!actualizarEvaluacionForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{actualizarEvaluacionForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{actualizarEvaluacionForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="evaluacionForm">
	    	        							<f:subview 
    	    	    								id="dataEvaluacion"
        	    									rendered="#{actualizarEvaluacionForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				
                	                	            	        			
                            	        										
                            	        									<h:commandButton value="Eliminar" 
			                                    					          image="/images/delete.gif"
                                    								          action="#{actualizarEvaluacionForm.delete}"
                                    								          immediate="true"
                                    								          onclick="javascript:return clickMade()"
                            	        							        />	
                            	        							        
                            	        							        
                            	        							        <h:outputText
                                                					         value="�Seguro que desea eliminar?"
                                                					         rendered="#{actualizarEvaluacionForm.deleting}" /> 
                            	        						
                            	        						
                            	        						            <h:commandButton value="Si" 
                                                					          image="/images/yes.gif"
                                                					          action="#{actualizarEvaluacionForm.deleteOk}"
                                                					          onclick="javascript:return clickMade()"
                                                					          rendered="#{actualizarEvaluacionForm.deleting}" />
                            	        							        
                            	        							 
                            	        						            <h:commandButton value="Guardar" 
			                                    					          image="/images/save.gif"
                                    								          action="#{actualizarEvaluacionForm.save}"
                                    								          immediate="true"
                                    								          onclick="javascript:return clickMade()"
                            	        							        />	
                            	        							        
                            	        							        <h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{actualizarEvaluacionForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        									/>	
                            	        										
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{actualizarEvaluacionForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>																				                    	    						
														<f:verbatim><tr>
    	                    								<td>
        	                									A�o
	            	            							</td>
		                        							<td></f:verbatim>
																<h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.anio}"/>								                        		                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									Semestre
	            	            							</td>
		                        							<td></f:verbatim>
																<h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.mes}"/>								                        									                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    						<f:subview id = "showEmpleados" rendered="#{actualizarEvaluacionForm.showEmpleados}">
															<f:verbatim><tr>
    	                    								<td>
        	                									Total Competencias
	            	            							</td>
		                        							<td></f:verbatim>
															   <h:inputText
												                 size="10"
												                 maxlength="8"
												                 value="#{actualizarEvaluacionForm.evaluacion.resultadoCompetencias}"	                        								
												                 id="Competencias"
												                 required="false"
												                 onchange="javascript:this.value=this.value.toUpperCase()"
												                 onkeypress="return keyFloatCheck(event, this)"
												                 style="text-align:right" >
											                      <f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											                   </h:inputText>
																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Total Objetivos
	            	            							</td>
		                        							<td></f:verbatim>
														     <h:inputText
											       	            size="10"
												                maxlength="8"
												                value="#{actualizarEvaluacionForm.evaluacion.resultadoObjetivos}"	                        								
												                id="Objetivos"
												                required="false"
												                onchange="javascript:this.value=this.value.toUpperCase()"
												                onkeypress="return keyFloatCheck(event, this)"
												                style="text-align:right" >
											                    <f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											                 </h:inputText>                       																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					
        	            	    					
	        	            	    					
	        	            	    					</f:subview>
	        	            	    					<f:subview id = "showObreros" rendered="#{actualizarEvaluacionForm.showObreros}">
	        	            	    					<f:verbatim><tr>
										<td>
											Manejo de Bienes y Equipos
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.manejoBienes}"	                        								
												id="ManejoBienes"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right" >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											H�bitos de Seguridad
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.habitoSeguridad}"	                        								
												id="HabitoSeguridad"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right" >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Calidad de Trabajo
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.calidadTrabajo}"	                        								
												id="CalidadTrabajo"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Cumplimiento de Normas
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.cumplimientoNormas}"	                        								
												id="CumplimientoNormas"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Atenci�n al P�blico
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.atencionPublico}"	                        								
												id="AtencionPublico"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Inter�s por el Trabajo
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.interesTrabajo}"	                        								
												id="InteresTrabajo"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Cooperaci�n
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.cooperacion}"	                        								
												id="Cooperacion"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Cantidad de Trabajo
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.cantidadTrabajo}"	                        								
												id="CantidadTrabajo"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Toma de Decisiones
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.tomaDecisiones}"	                        								
												id="TomaDecisiones"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Comunicaci�n con los Trabajadores
										</td>
										<td>
										</f:verbatim>	
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.comunicacion}"	                        								
												id="Comunicacion"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td>
											Capacidad de Mando
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.capacidadMando}"	                        								
												id="CapacidadMando"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
										<td>
											Coordinaci�n del Trabajo
										</td>
										<td>
										</f:verbatim>
											<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.coordinacion}"	                        								
												id="Coordinacion"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)"
												style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>
										<f:verbatim>
										</td>
									</tr></f:verbatim>	        	            	    					
	        	            	    					</f:subview>
															<f:verbatim><tr>
    	                    								<td>
        	                									Accion a tomar
	            	            							</td>
		                        							<td></f:verbatim>
									    		                  <h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.accionEvaluacion}"/>																									
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									C�dula Supervisor
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.cedulaSupervisor}"	                        								
												id="CedulaSupervisor"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyIntegerCheck(event, this)"
												style="text-align:right" >
											</h:inputText>
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Nombre Supervisor
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
												size="20"
												maxlength="25"
												value="#{actualizarEvaluacionForm.evaluacion.nombreSupervisor}"	                        								
												id="NombreSupervisor"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()">
											</h:inputText>																				
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									C�dula Jefe
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
												size="10"
												maxlength="8"
												value="#{actualizarEvaluacionForm.evaluacion.cedulaJefe}"	                        								
												id="CedulaJefe"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyIntegerCheck(event, this)"
												style="text-align:right" >
											</h:inputText>	
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Nombre Jefe
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
												size="20"
												maxlength="25"
												value="#{actualizarEvaluacionForm.evaluacion.nombreJefe}"	                        								
												id="NombreJefe"
												required="false"
												onchange="javascript:this.value=this.value.toUpperCase()">
											</h:inputText>	
                        																																
        	            	    							<f:verbatim></td>
        	            	    							
	        	            	    					</tr></f:verbatim>
	        	            	    						<f:verbatim><tr>
    	                    								<td>
        	                									Resultado
	            	            							</td>
		                        							<td></f:verbatim>
																<h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.resultadoEvaluacion}"  />																															                        																			
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Porcentaje de Aumento
	            	            							</td>
		                        							<td></f:verbatim>
																<h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.porcentajeAumento}"                	    		    								                    	    										                        									
			                        								style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:outputText>     
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Pasos en escala
	            	            							</td>
		                        							<td></f:verbatim>
																<h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.numeroPasos}"                	    		    								                    	    										                        									
			                        								style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:outputText>     
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Monto Aumentar
	            	            							</td>
		                        							<td></f:verbatim>
																<h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.montoAumentar}"                	    		    								                    	    										                        									
			                        								style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:outputText>     
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Monto Unico
	            	            							</td>
		                        							<td></f:verbatim>
																<h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.montoUnico}"                	    		    								                    	    										                        									
			                        								style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:outputText>     
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															 	<h:inputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.observaciones}"/>                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Tipo Personal
	            	            							</td>
		                        							<td></f:verbatim>
									    		                <h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.tipoPersonal}"/>  																								
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Cargo
	            	            							</td>
		                        							<td></f:verbatim>
									    		                 <h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.cargo}"/>  																							
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Dependencia
	            	            							</td>
		                        							<td></f:verbatim>
									    		                  <h:outputText		    		                    							
            	    		        								value="#{actualizarEvaluacionForm.evaluacion.dependencia}"/>  																								
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    					
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
													<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsEvaluacion"
            										rendered="#{actualizarEvaluacionForm.showData}">
		                        					<f:verbatim><table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
            	                            					
            	                            					
                	                    	        			<h:commandButton value="Eliminar" 
			                                    					image="/images/delete.gif"
                                    								action="#{actualizarEvaluacionForm.delete}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                            	        							 
                            	        						<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{actualizarEvaluacionForm.deleting}" /> 
                            	        						
                            	        						
                            	        						<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{actualizarEvaluacionForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{actualizarEvaluacionForm.deleting}" />
                                                					
                            	        						<h:commandButton value="Guardar" 
			                                    					image="/images/save.gif"
                                    								action="#{actualizarEvaluacionForm.save}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                            	        						
                            	        						<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{actualizarEvaluacionForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
            	                            					
                            	        							 
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>