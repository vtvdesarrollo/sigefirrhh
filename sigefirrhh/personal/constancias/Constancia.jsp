<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

	<%
	
	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{constanciaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formConstancia">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Definici�n de Constancias
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/constancias/Constancia.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{constanciaForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!constanciaForm.editing&&!constanciaForm.deleting&&constanciaForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchConstancia"
								rendered="#{!constanciaForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Tipo Personal
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{constanciaForm.findSelectTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{constanciaForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{constanciaForm.findConstanciaByTipoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConstanciaByTipoPersonal"
								rendered="#{constanciaForm.showConstanciaByTipoPersonal&&
								!constanciaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{constanciaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{constanciaForm.selectConstancia}"
                                						styleClass="listitem">
                                						<f:param name="idConstancia" value="#{result.idConstancia}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataConstancia"
								rendered="#{constanciaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!constanciaForm.editing&&!constanciaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{constanciaForm.editing&&!constanciaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{constanciaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{constanciaForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="constanciaForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{constanciaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!constanciaForm.editing&&!constanciaForm.deleting&&constanciaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{constanciaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!constanciaForm.editing&&!constanciaForm.deleting&&constanciaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{constanciaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{constanciaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{constanciaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{constanciaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{constanciaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{constanciaForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{constanciaForm.selectTipoPersonal}"
														id="tipoPersonal"
                                                    	disabled="#{!constanciaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Tipo Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{constanciaForm.colTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="tipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo de Constancia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{constanciaForm.constancia.tipo}"
	                                                    id="tipo"
                                                    	disabled="#{!constanciaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Tipo de Constancia">
														<f:selectItem itemLabel="Seleccione uno" itemValue="0" />
                                                        <f:selectItems value="#{constanciaForm.listTipo}" /> 
                                                    </h:selectOneMenu>													<h:message for="tipo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Oficina/Direcci�n
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="60"
	                        							id="oficina"
	                        							maxlength="60"
                        								value="#{constanciaForm.constancia.oficina}"
                        								readonly="#{!constanciaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="oficina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Apellidos y Nombres Firmante
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="60"
	                        							id="firmante"
	                        							maxlength="60"
                        								value="#{constanciaForm.constancia.firmante}"
                        								readonly="#{!constanciaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="firmante" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="60"
	                        							id="cargo"
	                        							maxlength="60"
                        								value="#{constanciaForm.constancia.cargo}"
                        								readonly="#{!constanciaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="cargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nombramiento
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputTextarea
	                        							cols="100"
	                        							rows="4"
	                        							id="nombramiento"
                        								value="#{constanciaForm.constancia.nombramiento}"
                        								readonly="#{!constanciaForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"                        								
                        								onkeypress="return keyEnterCheck(event, this)"                        								
                        								required="false" />													<h:message for="nombramiento" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Texto debajo de firma
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="90"
	                        							id="piePaginaUno"
	                        							maxlength="90"
                        								value="#{constanciaForm.constancia.piePaginaUno}"
                        								readonly="#{!constanciaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="piePaginaUno" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Pie de Pagina 1
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="90"
	                        							id="piePaginaDos"
	                        							maxlength="90"
                        								value="#{constanciaForm.constancia.piePaginaDos}"
                        								readonly="#{!constanciaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="piePaginaDos" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Pie de Pagina 2
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="90"
	                        							id="piePaginaTres"
	                        							maxlength="90"
                        								value="#{constanciaForm.constancia.piePaginaTres}"
                        								readonly="#{!constanciaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="piePaginaTres" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Pie de Pagina 3
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="90"
	                        							id="piePaginaCuatro"
	                        							maxlength="90"
                        								value="#{constanciaForm.constancia.piePaginaCuatro}"
                        								readonly="#{!constanciaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="piePaginaCuatro" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        							<f:verbatim><tr>
                        						<td>
                        							Iniciales
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="11"
	                        							id="iniciales"
	                        							maxlength="10"
                        								value="#{constanciaForm.constancia.iniciales}"
                        								readonly="#{!constanciaForm.editing}"
                        								                     								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="iniciales" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Observaci�n
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputTextarea
	                        							cols="100"
	                        							rows="4"
	                        							id="observacion"
                        								value="#{constanciaForm.constancia.observacion}"
                        								readonly="#{!constanciaForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"                        								
                        								onkeypress="return keyEnterCheck(event, this)"                        								
                        								required="false" />													<h:message for="observacion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{constanciaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!constanciaForm.editing&&!constanciaForm.deleting&&constanciaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{constanciaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!constanciaForm.editing&&!constanciaForm.deleting&&constanciaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{constanciaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{constanciaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{constanciaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{constanciaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{constanciaForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{constanciaForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>