<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReingresoCargoCarreraLefp'].elements['formReingresoCargoCarreraLefp:reportId'].value;
			var name = 
				document.forms['formReingresoCargoCarreraLefp'].elements['formReingresoCargoCarreraLefp:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{reingresoCargoCarreraLefpForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
			<h:form id="formReingresoCargoCarreraLefp">
			<h:inputHidden id="reportId" value="#{reingresoCargoCarreraLefpForm.reportId}" />
    		<h:inputHidden id="reportName" value="#{reingresoCargoCarreraLefpForm.reportName}" />		
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Reingreso Personal a un Cargo de Carrera
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/ReingresoCargoCarreraLefp.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
										<h:commandLink value="Ir a Datos Personales"                            						
                        						styleClass="listitem"
                        						action="#{reingresoCargoCarreraLefpForm.redirectPersonal}"
                        						rendered="#{reingresoCargoCarreraLefpForm.error}">	                            						
                        				</h:commandLink>
									</td>
								</tr>
							<f:subview 
								id="searchReingresoCargoCarrera"
								rendered="#{!reingresoCargoCarreraLefpForm.showData1}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Ingresar
												</td>
											</tr>
										</table>										
										<table width="100%" class="datatable">
											<tr>
			               						<td></f:verbatim>
													<h:outputText 
														value ="Imprimir Reporte Movimiento Realizado"
														rendered="#{reingresoCargoCarreraLefpForm.showReport}"/>										
			               						</td><f:verbatim>
			               						<td colspan="2" align="center"></f:verbatim>										
			   										<h:commandButton image="/images/report.gif"
			   											action="#{reingresoCargoCarreraLefpForm.runReport}"
			   											onclick="windowReport()"
			   											rendered="#{reingresoCargoCarreraLefpForm.showReport}" />
			   									<td><f:verbatim>
			               					</tr>	
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{reingresoCargoCarreraLefpForm.findPersonalCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													
												<f:verbatim></td>
											</tr>
											<tr>
		                						<td align="left">Fecha Vigencia</td>
		                						<td align="left">
		                						</f:verbatim>
													<h:inputText size="12"	
														maxlength="10"													
														value="#{reingresoCargoCarreraLefpForm.fechaIngreso}"														
														onkeypress="return keyEnterCheck(event, this)"
		                        						onblur="javascript:check_date(this)">																																			                         						
			                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
		                                                 pattern="dd-MM-yyyy" />
		                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim><f:verbatim><span class="required"> *</span></f:verbatim>	
		                        					<h:commandButton image="/images/find.gif" 														
														action="#{reingresoCargoCarreraLefpForm.findPersonalByCedula}"
														onclick="javascript:return clickMade()" />	                				
												<f:verbatim></td>										
											</tr></f:verbatim>
											<f:subview id="viewFechaEgreso"
														rendered="#{reingresoCargoCarreraLefpForm.showFechaEgreso}">
											<f:verbatim>
											<tr>
		                						<td align="left">Ultima Fecha de Egreso</td>
		                						<td align="left">
		                						</f:verbatim>
													<h:inputText size="12"
														maxlength="10"		
														value="#{reingresoCargoCarreraLefpForm.fechaEgreso}"
														immediate="false"
														onkeypress="return keyEnterCheck(event, this)"
		                        						onblur="javascript:check_date(this)"
		                        						>																																			                         						
			                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
		                                                 pattern="dd-MM-yyyy" />
		                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim><f:verbatim><span class="required"> *</span></f:verbatim>	
		                        					              				
												<f:verbatim></td>										
											</tr>
											<tr>
		                						<td align="left">Causa de Egreso</td>
		                						<td align="left">
		                						</f:verbatim>
					                            	<h:selectOneMenu value="#{reingresoCargoCarreraLefpForm.idCausaPersonal}"
		                                            	immediate="false">														
		                                                <f:selectItems value="#{reingresoCargoCarreraLefpForm.colCausaPersonal}" />
		                                            </h:selectOneMenu>											
		                                            <h:commandButton image="/images/next.gif" 
														action="#{reingresoCargoCarreraLefpForm.validarPersonal}"
														onclick="javascript:return clickMade()" />	  
												<f:verbatim></td>
											</tr></f:verbatim>
											</f:subview><f:verbatim>											
										</table>
									</td>
								</tr>
							</table>
							</f:verbatim>
							</f:subview>
							<f:subview
								id="viewSelectedPersonal"
								rendered="#{reingresoCargoCarreraLefpForm.showData1
								}">
								<f:verbatim>
								<table class="toptable" width="100%">
								<tr>
								<td>
								<table class="datatable" width="100%">
									<tr>
            							<td width="20%" align="left">Trabajador:&nbsp;</td>
										<td width="80%" align="left">
											</f:verbatim>
	            								<h:commandLink value="#{reingresoCargoCarreraLefpForm.personal}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
            								<f:verbatim>
            							</td>
									</tr>
									<tr>
                						<td align="left">Tipo de Personal</td>
                						<td align="left">
                    						</f:verbatim>
    			                            	<h:selectOneMenu value="#{reingresoCargoCarreraLefpForm.selectIdTipoPersonal}"
    				                            	valueChangeListener="#{reingresoCargoCarreraLefpForm.changeTipoPersonal}"
    				                            	onchange="this.form.submit()"
                                                	immediate="false">
    												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    <f:selectItems value="#{reingresoCargoCarreraLefpForm.colTipoPersonal}" />
                                                </h:selectOneMenu>
    											
    										<f:verbatim>
										</td>
									</tr></f:verbatim>
									<f:subview id="posicionesVacantes"
												rendered="#{reingresoCargoCarreraLefpForm.mostrarRegistroCargos}"> <f:verbatim>
									<tr>
                						<td align="left">Posiciones Vacantes</td>
                						<td align="left">
                    						</f:verbatim>
    			                            	<h:selectOneMenu value="#{reingresoCargoCarreraLefpForm.selectIdRegistroCargos}"
    				                            	valueChangeListener="#{reingresoCargoCarreraLefpForm.changeRegistroCargos}"
    				                            	rendered="#{reingresoCargoCarreraLefpForm.showRegistroCargos}"
    				                            	onchange="this.form.submit()"
                                                	immediate="false">
    												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    <f:selectItems value="#{reingresoCargoCarreraLefpForm.colRegistroCargos}" />
                                                </h:selectOneMenu>
    											
    										<f:verbatim>
										</td>
									</tr></f:verbatim>
									</f:subview><f:verbatim>
																	
									<tr>
                						<td align="left">Cargo</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:outputText										
												rendered="#{reingresoCargoCarreraLefpForm.showFields}"
												value="#{reingresoCargoCarreraLefpForm.registroCargos.cargo}"
												
												/>											
										<f:verbatim></td>
									</tr>							
									<tr>
                						<td align="left">Regi�n</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 										
												rendered="#{reingresoCargoCarreraLefpForm.showFields}"
												value="#{reingresoCargoCarreraLefpForm.nombreRegion}"
											 />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Sede</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 												
												rendered="#{reingresoCargoCarreraLefpForm.showFields}"
												value="#{reingresoCargoCarreraLefpForm.nombreSede}"
												
												 />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Dependencia</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 										
												rendered="#{reingresoCargoCarreraLefpForm.showFields}"
												value="#{reingresoCargoCarreraLefpForm.nombreDependencia}"
												
												/>											
										<f:verbatim></td>
									</tr>									
									<tr>
                						<td align="left">Grado</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 
												rendered="#{reingresoCargoCarreraLefpForm.showFields}"
												value="#{reingresoCargoCarreraLefpForm.grado}"
												/>											
										<f:verbatim></td>
									</tr>		
									<tr>
                						<td align="left">Sueldo B�sico</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 
												rendered="#{reingresoCargoCarreraLefpForm.showFields}"
												value="#{reingresoCargoCarreraLefpForm.sueldo}"
												>
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:outputText>	              
										<f:verbatim></td>										
									</tr>	
									
									
									<tr>	
    									<td>
    										Pagar Retroactivo?
    									</td>
    									<td width="100%">
    										</f:verbatim>
    										<h:selectOneMenu value="#{reingresoCargoCarreraLefpForm.pagarRetroactivo}"    											
    											rendered="#{reingresoCargoCarreraLefpForm.showFields}"    											
    											onchange="this.form.submit()"
    											immediate="true">
                                            	<f:selectItem itemLabel="No" itemValue="N" />  
                                            	<f:selectItem itemLabel="Si" itemValue="S" />
                                            </h:selectOneMenu>
                                            	<f:verbatim>
    									</td>
    								</tr>			
                					
                        			<tr>
                						<td align="left">Fecha Punto de Cuenta</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{reingresoCargoCarreraLefpForm.showFields}"
												value="#{reingresoCargoCarreraLefpForm.fechaPuntoCuenta}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim><f:verbatim><span class="required"> *</span></f:verbatim>	
                        						                				
										<f:verbatim></td>										
									</tr> 
                        			<tr>
                						<td align="left">
                							Punto de Cuenta
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="15"
                    							maxlength="15"
                    							rendered="#{reingresoCargoCarreraLefpForm.showFields}"
                								value="#{reingresoCargoCarreraLefpForm.puntoCuenta}"                        								
                								id="PuntoCuenta"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                					<tr>
                						<td align="left">
                							Concurso
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:outputText

                    							rendered="#{reingresoCargoCarreraLefpForm.showFields}"
                								value="#{reingresoCargoCarreraLefpForm.concursoCargo.concurso.codConcurso} - #{reingresoCargoCarreraLefpForm.concursoCargo.concurso.nombre}"                        								
                								              								      												 >
                							</h:outputText>
                						<f:verbatim></td>
                					</tr> 

                        			<tr>
                						<td>
                							Observaciones
                						</td>
                						<td></f:verbatim>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="4"                    							
                    							id="observaciones"
                    							rendered="#{reingresoCargoCarreraLefpForm.showFields}"
                								value="#{reingresoCargoCarreraLefpForm.observaciones}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />
	                					<f:verbatim>																
                						</td>
                					</tr>
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{reingresoCargoCarreraLefpForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{reingresoCargoCarreraLefp.showFields}" />
    											<h:commandButton value="Cancelar" 
                                    				image="/images/cancel.gif"
                        							action="#{reingresoCargoCarreraLefpForm.abort}"
                        							immediate="true"
                        							onclick="javascript:return clickMade()"
                	        							 />
                                    			
											<f:verbatim>
										</td>
									</tr>								
								</table>
								</td>
								</tr>
								</table>
							</f:verbatim>
							</f:subview>	
																									
						</td>
					</tr>
				</table>
				</h:form>
			</td>			
		</tr>
	</table>
</f:view>
</body>
</html>