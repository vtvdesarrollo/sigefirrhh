<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{trasladoSinRegistroForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
	<h:form id="formRegistroCargos" 
		>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											 Traslado Personal Sin Registro
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/TrasladoSinRegistro.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
                        					image="/images/close.gif"
                        					action="go_cancelOption"
                        					immediate="true"                        					
                        					onclick="javascript:return clickMade()"
                	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="Traslado"
								rendered="#{!trasladoSinRegistroForm.showTrabajador}"
								>
								<f:verbatim><tr>
									<td>
										<table width="100%" class="datatable"></f:verbatim>
                        				<f:verbatim><tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{trasladoSinRegistroForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoSinRegistroForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{trasladoSinRegistroForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{trasladoSinRegistroForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{trasladoSinRegistroForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{trasladoSinRegistroForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											</f:verbatim>
											<f:subview 
												id="viewResultTrabajador"
												rendered="#{trasladoSinRegistroForm.showResultTrabajador&&
												!trasladoSinRegistroForm.showTrabajador}">
												<f:verbatim><table class="toptable" width="100%">
													<tr>
														<td>
				    										<table width="100%" class="bandtable">
				    											<tr>
				    												<td>
				    													Resultado de Trabajador
				    												</td>
				    											</tr>
				    										</table>
														</td>
													</tr>
													<tr>
														<td></f:verbatim>
				                                            <h:dataTable id="tableTrabajador"
				                                                styleClass="datatable"
				                                                headerClass="standardTable_Header"
				                                                footerClass="standardTable_Header"
				                                                rowClasses="standardTable_Row1,standardTable_Row2"
				                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
				                                                var="resultTrabajador"
				                                				value="#{trasladoSinRegistroForm.resultTrabajador}"                                                
				                                                rows="20"
				                                                width="100%">
				                                				<h:column>
				                                					<h:commandLink value="#{resultTrabajador}"
				                                						action="#{trasladoSinRegistroForm.showTraslado}"
				                                						styleClass="listitem">
				                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
				                                					</h:commandLink>
				                                				</h:column>
				                                            </h:dataTable>
				                                            
				                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
				                                                <x:dataScroller id="scroll_1"
				                                                    for="tableTrabajador"
				                                                    fastStep="10"
				                                                    pageCountVar="pageCount"
				                                                    pageIndexVar="pageIndex">
				        	                                        <f:facet name="first" >
				    	                                            	<h:graphicImage url="/images/arrow-first.gif" />
					                                                </f:facet>
				                                                	<f:facet name="last">
				                                            	    	<h:graphicImage url="/images/arrow-last.gif" />
				                                        	        </f:facet>
				                                    	        	<f:facet name="previous">
				                                	            		<h:graphicImage url="/images/arrow-previous.gif" />
				                            	                	</f:facet>
				                        	                    	<f:facet name="next">
				                    	                        		<h:graphicImage url="/images/arrow-next.gif" />
				                	                            	</f:facet>
				            	                                	<f:facet name="fastforward">
				        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
				    	                                        	</f:facet>
					                                            	<f:facet name="fastrewind">
				                                            			<h:graphicImage url="/images/arrow-fr.gif" />
				                                            		</f:facet>
				                                            	</x:dataScroller>
					                                            <x:dataScroller id="scroll_2"
				    	                                        	for="tableTrabajador"
				        	                                    	pageCountVar="pageCount"
				            	                                	pageIndexVar="pageIndex" >
				                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
				                    	        	            	    <f:param value="#{pageIndex}" />
				                        	        	            	<f:param value="#{pageCount}" />
					                            	                </h:outputFormat>
				                                        	    </x:dataScroller>
				                                            </h:panelGrid>
				                            			<f:verbatim></td>
				                    				</tr>
				                    			</table></f:verbatim>
											</f:subview>
										</f:subview>
											<f:subview id="Trabajador" rendered="#{trasladoSinRegistroForm.showTrabajador}">
												<f:verbatim>
													<tr>
				                						<td align="left">Trabajador</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 												
																value="#{trasladoSinRegistroForm.trabajador}" />											
														<f:verbatim></td>
													</tr>
													<tr>
				                						<td align="left">Regi�n</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 												
																value="#{trasladoSinRegistroForm.trabajador.dependencia.region}" />											
														<f:verbatim></td>
													</tr>																										
													<tr>
				                						<td align="left">Dependencia</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoSinRegistroForm.trabajador.dependencia}"/>											
														<f:verbatim></td>
													</tr>
													<tr>
				                						<td align="left">Cargo</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoSinRegistroForm.trabajador.cargo}"/>											
														</td>
													</tr>
													<f:verbatim><tr>
				                						<td align="left">Grado</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoSinRegistroForm.trabajador.cargo.grado}" />											
														<f:verbatim></td>
													</tr>		
													<tr>
				                						<td align="left">Paso</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoSinRegistroForm.trabajador.paso}"/>											
														<f:verbatim></td>
													</tr>		
													<tr>
				                						<td align="left">Sueldo B�sico</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoSinRegistroForm.trabajador.sueldoBasico}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
															</h:outputText>	              
														<f:verbatim></td>										
													</tr>
													</f:verbatim>
												</f:subview>
												<f:subview id="NuevaDependencia" rendered="#{trasladoSinRegistroForm.showNuevaDependencia&&trasladoSinRegistroForm.showTrabajador}">
													<f:verbatim>								
												<tr>
			                						<td align="left">Fecha Vigencia Movimiento</td>
			                						<td align="left">
			                						</f:verbatim>
														<h:inputText size="18"
															
															value="#{trasladoSinRegistroForm.fechaMovimiento}"
															immediate="false"
															onkeypress="return keyEnterCheck(event, this)"
			                        						onblur="javascript:check_date(this)"
			                        						>																																			                         						
				                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
			                                                 pattern="dd-MM-yyyy" />
			                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
			                        						                				
													<f:verbatim></td>										
												</tr>													
												 <tr>
                        						<td>
                        							Region
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{trasladoSinRegistroForm.idRegion}"
	                                                    valueChangeListener="#{trasladoSinRegistroForm.changeRegion}"                                                    	
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"                                                    	
														title="Region">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoSinRegistroForm.colRegion}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>									
  											  
											<tr>
                        						<td>
                        							Dependencia
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{trasladoSinRegistroForm.idDependencia}"
	                                                    valueChangeListener="#{trasladoSinRegistroForm.changeDependencia}"                                                    	
	                                                    rendered="#{trasladoSinRegistroForm.showDependencia}"
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"
														title="Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoSinRegistroForm.colDependencia}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											
											
											<tr>
		                						<td>
		                							Observaciones
		                						</td>
		                						<td></f:verbatim>
					                        		<h:inputTextarea
		                    							cols="50"
		                    							rows="4"                    							
		                    							id="observaciones"
		                								value="#{trasladoSinRegistroForm.observaciones}"
		                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
		                								required="false" />													
		                						<f:verbatim></td>
		                					</tr>		
											<tr>
												<td></td>
												<td align="left">
													</f:verbatim>
		    											<h:commandButton image="/images/run.gif"
		    												action="#{trasladoSinRegistroForm.ejecutar}"
		    												onclick="javascript:return clickMade()"
		    												rendered="#{ascensoNoLefp.showData}" />
		    											
		                                    			<h:commandButton value="Cancelar" 
				                            				image="/images/cancel.gif"
				                							action="#{trasladoSinRegistroForm.abort}"
				                							immediate="true"
				                							onclick="javascript:return clickMade()"
				        	        							 />
													<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											</f:subview>
											<f:verbatim>
										</table>
									</td>
								</tr>
							</table></f:verbatim>
															

</h:form>
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>