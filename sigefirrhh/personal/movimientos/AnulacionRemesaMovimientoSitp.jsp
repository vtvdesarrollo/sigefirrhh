<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formAnulacionRemesaMovimientoSitp'].elements['formAnulacionRemesaMovimientoSitp:reportId'].value;
			var name = 
				document.forms['formAnulacionRemesaMovimientoSitp'].elements['formAnulacionRemesaMovimientoSitp:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{anulacionRemesaMovimientoSitpForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
			<h:form id="formAnulacionRemesaMovimientoSitp">
			<h:inputHidden id="reportId" value="#{anulacionRemesaMovimientoSitpForm.reportId}" />
    		<h:inputHidden id="reportName" value="#{anulacionRemesaMovimientoSitpForm.reportName}" />
    		
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Anulaci�n de Movimiento
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/AnulacionRemesaMovimientoSitp.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
										
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchCambioCargo"
								rendered="#{!anulacionRemesaMovimientoSitpForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Remesa
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
			               						<td></f:verbatim>
													<h:outputText 
														value ="Imprimir Reporte Movimiento Realizado"
														rendered="#{anulacionRemesaMovimientoSitpForm.showReport}"/>										
			               						</td><f:verbatim>
			               						<td colspan="2" align="center"></f:verbatim>										
			   										<h:commandButton image="/images/report.gif"
			   											action="#{anulacionRemesaMovimientoSitpForm.runReport}"
			   											onclick="windowReport()"
			   											rendered="#{anulacionRemesaMovimientoSitpForm.showReport}" />
			   									<f:verbatim><td>
			               					</tr>
											<tr>
                        						<td>
                        							Remesa
                        						</td>
                        						<td width="100%"></f:verbatim> 
					                               	<h:selectOneMenu value="#{anulacionRemesaMovimientoSitpForm.findSelectRemesa}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{anulacionRemesaMovimientoSitpForm.findColRemesa}" />
                                                   	</h:selectOneMenu>
													<h:commandButton image="/images/find.gif" 
														action="#{anulacionRemesaMovimientoSitpForm.findRemesaMovimientoSitpByRemesa}"
														onclick="javascript:return clickMade()" />
												 <f:verbatim></td>
											</tr>											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultRemesaMovimientoSitpByRemesa"
								rendered="#{anulacionRemesaMovimientoSitpForm.showRemesaMovimientoSitpByRemesa && 
								!anulacionRemesaMovimientoSitpForm.showData}">
								<f:verbatim>
								<table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de busqueda de movimientos en remesa
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{anulacionRemesaMovimientoSitpForm.result}"
                                                rows="10"
                                                width="100%">
                                				<h:column>
	                                                <h:outputText value="#{result}"/>
                                                </h:column>
                                                <h:column>                                                	
                                    					<h:commandLink 
                                    						action="#{anulacionRemesaMovimientoSitpForm.selectMovimiento}"
                                    						styleClass="listitem">
                                    						<h:graphicImage url="/images/b_undo-p.gif"/>
                                    						<f:param name="idMovimientoSitp" value="#{result.idMovimientoSitp}" />
                                    					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif" />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif" />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif" />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif" />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif" />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							<f:subview
								id="viewDataRemesaMovimientoSitp"
								rendered="#{anulacionRemesaMovimientoSitpForm.selectedMovimiento && anulacionRemesaMovimientoSitpForm.showData && 
										!anulacionRemesaMovimientoSitpForm.showRemesaMovimientoSitpByRemesa}">
								<f:verbatim>
								<table class="datatable" width="100%">
									<tr>
            							<td width="20%" align="left">Movimiento a Anular:&nbsp;</td>
										<td width="80%" align="left">											
											<tr>
											</f:verbatim>
	            								<h:commandLink value="#{anulacionRemesaMovimientoSitpForm.movimientoSitp}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
	                            			<f:verbatim>	
	                            			</tr>
            							</td>
									</tr>
									
									<tr>
										<td align="left">Fecha de Movimiento</td>   
                						<td align="left">             						
                						</f:verbatim>
											<h:outputText
												rendered="#{anulacionRemesaMovimientoSitpForm.showFields}"
												value="#{anulacionRemesaMovimientoSitpForm.movimientoSitp.fechaMovimiento}"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:outputText>	       				
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Causa Movimiento</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText												
												rendered="#{anulacionRemesaMovimientoSitpForm.showFields}"
												value="#{anulacionRemesaMovimientoSitpForm.movimientoSitp.causaMovimiento.descripcion}" />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">C�dula</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText												
												rendered="#{anulacionRemesaMovimientoSitpForm.showFields}"
												value="#{anulacionRemesaMovimientoSitpForm.movimientoSitp.cedula}" />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Apellidos y Nombres</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText												
												rendered="#{anulacionRemesaMovimientoSitpForm.showFields}"
												value="#{anulacionRemesaMovimientoSitpForm.movimientoSitp.apellidosNombres}" />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Descripci�n de Cargo</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText												
												rendered="#{anulacionRemesaMovimientoSitpForm.showFields}"
												value="#{anulacionRemesaMovimientoSitpForm.movimientoSitp.descripcionCargo}" />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">C�digo Nomina</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText											
												rendered="#{anulacionRemesaMovimientoSitpForm.showFields}"
												value="#{anulacionRemesaMovimientoSitpForm.movimientoSitp.codigoNomina}" />											
										<f:verbatim></td>
									</tr>												
									<tr>
                						<td align="left">Fecha Anulaci�n Movimiento</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="10"
												rendered="#{anulacionRemesaMovimientoSitpForm.showFields}"
												value="#{anulacionRemesaMovimientoSitpForm.fechaMovimiento}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim><f:verbatim><span class="required"> *</span></f:verbatim>	
                        						                				
										<f:verbatim></td>										
									</tr>									
                        			<tr>
                						<td align="left">Fecha Punto de Cuenta</td>   
                						<td align="left">             						
                						</f:verbatim>
											<h:inputText size="10"
												rendered="#{anulacionRemesaMovimientoSitpForm.showFields}"
												value="#{anulacionRemesaMovimientoSitpForm.fechaPuntoCuenta}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim><f:verbatim><span class="required"> *</span></f:verbatim>	
                        						                				
										<f:verbatim></td>										
									</tr> 
                        			<tr>
                						<td align="left">
                							Punto de Cuenta
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="15"
                    							maxlength="15"
                    							rendered="#{anulacionRemesaMovimientoSitpForm.showFields}"
                								value="#{anulacionRemesaMovimientoSitpForm.puntoCuenta}"                        								
                								id="PuntoCuenta"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                					<tr>
                						<td>
                							Observaciones
                						</td>
                						<td></f:verbatim>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="4"                    							
                    							id="observaciones"
                    							rendered="#{anulacionRemesaMovimientoSitpForm.showFields}"
                								value="#{anulacionRemesaMovimientoSitpForm.observaciones}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />													
                						<f:verbatim></td>
                					</tr>				
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{anulacionRemesaMovimientoSitpForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{anulacionRemesaMovimientoSitpForm.showData}" />
    											
                                    			<h:commandButton value="Cancelar" 
		                            				image="/images/cancel.gif"
		                							action="#{anulacionRemesaMovimientoSitpForm.abort}"
		                							immediate="true"
		                							onclick="javascript:return clickMade()"
		        	        							 />
											<f:verbatim>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview> 
							
						</td>
					</tr>
				</table>

				</h:form>

			</td>			
		</tr>
	</table>

</f:view>

</body>
</html>