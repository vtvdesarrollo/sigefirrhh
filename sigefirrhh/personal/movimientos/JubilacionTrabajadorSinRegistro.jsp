<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{jubilacionTrabajadorSinRegistroForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left"></div>
			</td>
			<td width="570" valign="top">
			<h:form id="formIngresoTrabajadorSinRegistro">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Ingreso de Personal Jubilado/Pensionado
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/JubilacionTrabajadorSinRegistro.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchJubilacionTrabajadorSinRegistro"
								rendered="#{!jubilacionTrabajadorSinRegistroForm.showData1}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Ingresar C�dula
												</td>
											</tr>
										</table>										
										<table width="100%" class="datatable"></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{jubilacionTrabajadorSinRegistroForm.findPersonalCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{jubilacionTrabajadorSinRegistroForm.findPersonalByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>												
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewSelectedPersonal"
								rendered="#{jubilacionTrabajadorSinRegistroForm.showData1
								}">
								<f:verbatim>
								<table class="toptable" width="100%">
								<tr>
								<td>
								<table class="datatable" width="100%">
									<tr>
            							<td width="25%" align="left">Trabajador:&nbsp;</td>
										<td width="75%" align="left">
											</f:verbatim>
	            								<h:commandLink value="#{jubilacionTrabajadorSinRegistroForm.personal}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
            								<f:verbatim>
            							</td>
									</tr>
									<tr>
                						<td align="left">Tipo de Personal</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{jubilacionTrabajadorSinRegistroForm.selectIdTipoPersonal}"
				                            	valueChangeListener="#{jubilacionTrabajadorSinRegistroForm.changeTipoPersonal}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{jubilacionTrabajadorSinRegistroForm.colTipoPersonal}" />
                                            </h:selectOneMenu>
											
										<f:verbatim></td>
									</tr>
									<tr>
										<td colspan="2" align="left" valign="middle">
        									</f:verbatim>
        									<f:subview
                								id="egresado"
                								rendered="#{jubilacionTrabajadorSinRegistroForm.egresado
                								}">
                								<f:verbatim>
                									El trabajador esta egresado de otro tipo de personal, �Desea continuar?
                								</f:verbatim>
                									
            											<h:commandButton image="/images/yes.gif"
            												action="#{jubilacionTrabajadorSinRegistroForm.continuarConEgresado}"
            												onclick="javascript:return clickMade()"
            												 />
                        	        		</f:subview>
                        	        		<f:verbatim>
                        	        	</td>
									</tr>
									<tr>
                						<td align="left">Movimiento</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{jubilacionTrabajadorSinRegistroForm.selectIdCausaPersonal}"
				                            	rendered="#{jubilacionTrabajadorSinRegistroForm.showCausaPersonal}"				                            	
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{jubilacionTrabajadorSinRegistroForm.colCausaPersonal}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Regi�n</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{jubilacionTrabajadorSinRegistroForm.selectIdRegion}"
				                            	rendered="#{jubilacionTrabajadorSinRegistroForm.showRegion}"
				                            	valueChangeListener="#{jubilacionTrabajadorSinRegistroForm.changeRegion}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{jubilacionTrabajadorSinRegistroForm.colRegion}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Sede</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{jubilacionTrabajadorSinRegistroForm.selectIdSede}"
				                            	rendered="#{jubilacionTrabajadorSinRegistroForm.showSede}"
				                            	valueChangeListener="#{jubilacionTrabajadorSinRegistroForm.changeSede}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{jubilacionTrabajadorSinRegistroForm.colSede}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>	
									<tr>
                						<td align="left">Dependencia</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{jubilacionTrabajadorSinRegistroForm.selectIdDependencia}"
				                            	rendered="#{jubilacionTrabajadorSinRegistroForm.showDependencia}"
				                            	
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{jubilacionTrabajadorSinRegistroForm.colDependencia}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Manual</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{jubilacionTrabajadorSinRegistroForm.selectIdManualCargo}"
				                            	rendered="#{jubilacionTrabajadorSinRegistroForm.showManualCargo}"
				                            	valueChangeListener="#{jubilacionTrabajadorSinRegistroForm.changeManualCargo}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{jubilacionTrabajadorSinRegistroForm.colManualCargo}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Cargo</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{jubilacionTrabajadorSinRegistroForm.selectIdCargo}"
				                            	rendered="#{jubilacionTrabajadorSinRegistroForm.showCargo}"
				                            	valueChangeListener="#{jubilacionTrabajadorSinRegistroForm.changeCargo}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{jubilacionTrabajadorSinRegistroForm.colCargo}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td>
                							Jubilacion/Pensi�n
                						</td>
                						<td></f:verbatim>
			                        		<h:inputText
                    							size="12"
                    							id="Sueldo"
                    							maxlength="12"
                								value="#{jubilacionTrabajadorSinRegistroForm.sueldo}"
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								onkeypress="return keyFloatCheck(event, this)"
                								 style="text-align:right"                         								required="false">
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                							</h:inputText>
                								
                        			<f:verbatim></td>
                        					</tr>
									  		<tr>
                						<td align="left">
                							C�digo N�mina
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="8"
                    							maxlength="8"
                								value="#{jubilacionTrabajadorSinRegistroForm.codigoNomina}"                        								
                								id="CodigoNomina"
                								required="false"
                								onchange="javascript:this.value=this.value.toUpperCase()"                								          												 >
                							</h:inputText>
                						<f:verbatim></td>
                        					</tr>			
										<tr>
                						<td align="left">Fecha Jubilaci�n</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												
												value="#{jubilacionTrabajadorSinRegistroForm.fechaIngreso}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr>	
									<tr>
                						<td>
                							Porcentaje Jubilacion/Pensi�n
                						</td>
                						<td></f:verbatim>
			                        		<h:inputText
                    							size="12"
                    							id="PorcentajeJubilacion"
                    							maxlength="12"
                								value="#{jubilacionTrabajadorSinRegistroForm.porcentajeJubilacion}"
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								onkeypress="return keyFloatCheck(event, this)"
                								 style="text-align:right"                         								required="false">
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                							</h:inputText>
                								
                        			<f:verbatim></td>
                        					</tr>
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{jubilacionTrabajadorSinRegistroForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{jubilacionTrabajadorSinRegistro.showFields}" />
    											<h:commandButton value="Cancelar" 
                                    				image="/images/cancel.gif"
                        							action="#{jubilacionTrabajadorSinRegistroForm.abort}"
                        							immediate="true"
                        							onclick="javascript:return clickMade()"
                	        							 />
                                    			
											<f:verbatim>
										</td>
									</tr>								
								</table>
								</td>
								</tr>
								</table>
							</f:verbatim>
							</f:subview>	
																									
						</td>
					</tr>
				</table>
				</h:form>
			</td>			
		</tr>
	</table>
</f:view>
</body>
</html>