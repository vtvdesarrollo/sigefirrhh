<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>   	
		function windowReport() {
			var number = 
				document.forms['formReportCurriculum'].elements['formReportCurriculum:reportId'].value;
			var name = 
				document.forms['formReportCurriculum'].elements['formReportCurriculum:reportName'].value;				
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}  			
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{movimientoSitpForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />
				<%
	  			}else {  enu.jsp" />
	  				
				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formMovimientoSitp">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Movimientos a registrar en SITP
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/movimientos/ReportMovimientoSitp.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success"  />
										</td>
									</tr>
									
									<f:subview 
										id="searchMovimientoSitp"
										rendered="#{!movimientoSitpForm.showData&&
										!movimientoSitpForm.showAdd&&
										!movimientoSitpForm.login.servicioPersonal}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{movimientoSitpForm.findPersonalCedula}"
																onkeypress="javascript:return keyIntegerCheck(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{movimientoSitpForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{movimientoSitpForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{movimientoSitpForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{movimientoSitpForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{movimientoSitpForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{movimientoSitpForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{movimientoSitpForm.showResultPersonal&&
									!movimientoSitpForm.showData&&
									!movimientoSitpForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <x:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{movimientoSitpForm.resultPersonal}"
    	                                            preserveDataModel="false"
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{movimientoSitpForm.findMovimientoSitpByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </x:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{movimientoSitpForm.selectedPersonal&&
									!movimientoSitpForm.showData&&
									!movimientoSitpForm.editing&&
									!movimientoSitpForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{movimientoSitpForm.personal}   <-- clic aqui"
    	                            						action="#{movimientoSitpForm.findMovimientoSitpByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{movimientoSitpForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{movimientoSitpForm.add}"
            													rendered="#{!movimientoSitpForm.editing&&!movimientoSitpForm.deleting&&movimientoSitpForm.login.agregar}" />

                                        	    			<% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{movimientoSitpForm.abort}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                                                    	    <%}%>
                                                    	   <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultMovimientoSitp"
									rendered="#{movimientoSitpForm.showResult&&
									!movimientoSitpForm.showData&&
									!movimientoSitpForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <x:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{movimientoSitpForm.result}"
                                            	    preserveDataModel="false"
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{movimientoSitpForm.selectMovimientoSitp}"
	                                						styleClass="listitem">
    		                            						<f:param name="idMovimientoSitp" value="#{result.idMovimientoSitp}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </x:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataMovimientoSitp"
								rendered="#{movimientoSitpForm.showData||movimientoSitpForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!movimientoSitpForm.editing&&!movimientoSitpForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{movimientoSitpForm.editing&&!movimientoSitpForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{movimientoSitpForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{movimientoSitpForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="movimientoSitpForm">
	    	        							<f:subview 
    	    	    								id="dataMovimientoSitp"
        	    									rendered="#{movimientoSitpForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{movimientoSitpForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!movimientoSitpForm.editing&&!movimientoSitpForm.deleting&&movimientoSitpForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{movimientoSitpForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!movimientoSitpForm.editing&&!movimientoSitpForm.deleting&&movimientoSitpForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{movimientoSitpForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{movimientoSitpForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{movimientoSitpForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{movimientoSitpForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{movimientoSitpForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{movimientoSitpForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{movimientoSitpForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
																																																								                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�dula
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="8"
	    	    		                							maxlength="8"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.cedula}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="cedula"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="cedula" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Apellidos y Nombres
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.apellidosNombres}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="apellidosNombres"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="apellidosNombres" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Personal
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{movimientoSitpForm.selectClasificacionPersonal}"
        		                                                	disabled="#{!movimientoSitpForm.editing}"
            		                                            	immediate="false"
				                        							id="clasificacionPersonal"
                		            								                    		        								                        		                                	required="false"
    																title="Personal">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{movimientoSitpForm.colClasificacionPersonal}" />
								                                    	                </h:selectOneMenu>																									<h:message for="clasificacionPersonal" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Tipo Cargo
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{movimientoSitpForm.movimientoSitp.tipoPersonal}"
        	                                        	        	disabled="#{!movimientoSitpForm.editing}"
            	                                        	    	immediate="false"
				                        							id="tipoPersonal"
                	            									                    	        										                        	                                	required="false"
    	                        	                            	title="Tipo Cargo">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{movimientoSitpForm.listTipoPersonal}" />
					    	            	                        	            </h:selectOneMenu>																				<h:message for="tipoPersonal" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Tipo Personal
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.nombreTipoPersonal}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="nombreTipoPersonal"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreTipoPersonal" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																							                        						<f:verbatim><tr>
    		                    							<td>
        		                								Movimiento Personal
	            		            						</td>
    	            		        						<td></f:verbatim>
						                	        	                            	<h:selectOneMenu value="#{movimientoSitpForm.selectMovimientoPersonalForCausaMovimiento}"
                        	    	                        		disabled="#{!movimientoSitpForm.editing}"
	                    	            	                    	onchange="this.form.submit()"
				                        							id="MovimientoPersonalForCausaMovimiento"
																	valueChangeListener="#{movimientoSitpForm.changeMovimientoPersonalForCausaMovimiento}"
        	                																											required="false"
																	immediate="false"
																	title="Causa Movimiento">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{movimientoSitpForm.colMovimientoPersonalForCausaMovimiento}" />
																							</h:selectOneMenu>																					<f:verbatim></td>
														</tr></f:verbatim>
										                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Causa Movimiento
	            	            							</td>
		                        							<td></f:verbatim>
										                        	                        	    <h:selectOneMenu value="#{movimientoSitpForm.selectCausaMovimiento}"
		                        	                            	disabled="#{!movimientoSitpForm.editing}"
    		                        	                        	rendered="#{movimientoSitpForm.showCausaMovimiento}"
				                        							id="causaMovimiento"																	
            		                        	                	immediate="false"
                		                        	            	required="false"
																	title="Causa Movimiento">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                            		                        		<f:selectItems value="#{movimientoSitpForm.colCausaMovimiento}" />
							    	                                	                </h:selectOneMenu>																									<h:message for="causaMovimiento" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Causa Movimiento
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="1"
	    	    		                							maxlength="1"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codCausaMovimiento}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codCausaMovimiento"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codCausaMovimiento" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Movimiento
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaMovimiento"
                		        									value="#{movimientoSitpForm.movimientoSitp.fechaMovimiento}"
                    		    									readonly="#{!movimientoSitpForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="true">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
																									<h:message for="fechaMovimiento" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																										                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Manual
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="5"
	    	    		                							maxlength="5"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codManualCargo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codManualCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codManualCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Cargo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codCargo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Tabulador
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codTabulador}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codTabulador"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codTabulador" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Descripci�n Cargo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.descripcionCargo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="descripcionCargo"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        			                        								<f:verbatim><span class="required"> *</span></f:verbatim>
                        																																<h:message for="descripcionCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo RAC/RPT
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codigoNomina}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codigoNomina"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codigoNomina" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Sede
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codSede}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codSede"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codSede" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Sede
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.nombreSede}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="nombreSede"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreSede" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Dependencia
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codDependencia}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codDependencia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codDependencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Dependencia
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.nombreDependencia}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="nombreDependencia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreDependencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Sueldo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.sueldo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="sueldo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="sueldo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Compensaci�n
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.compensacion}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="compensacion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="compensacion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Primas Cargo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.primasCargo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="primasCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="primasCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Primas Trabajador
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.primasTrabajador}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="primasTrabajador"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="primasTrabajador" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Grado
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.grado}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="grado"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="grado" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Paso
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.paso}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="paso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="paso" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Remesa
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{movimientoSitpForm.selectRemesa}"
        		                                                	disabled="#{!movimientoSitpForm.editing}"
            		                                            	immediate="false"
				                        							id="remesa"
                		            								                    		        								                        		                                	required="false"
    																title="Remesa">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{movimientoSitpForm.colRemesa}" />
								                                    	                </h:selectOneMenu>																									<h:message for="remesa" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									N� Movimiento
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.numeroMovimiento}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="numeroMovimiento"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="numeroMovimiento" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																										                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Organismo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="4"
	    	    		                							maxlength="4"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codOrganismo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codOrganismo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codOrganismo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Nombre Organismo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.nombreOrganismo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="nombreOrganismo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreOrganismo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Organismo MPF
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codOrganismoMpd}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codOrganismoMpd"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codOrganismoMpd" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Regi�n
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codRegion}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codRegion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codRegion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Nombre Regi�n
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.nombreRegion}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="nombreRegion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreRegion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    							                        		<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="3"
    	        		                							id="observaciones"
                    		        								                        		    								value="#{movimientoSitpForm.movimientoSitp.observaciones}"
                            										readonly="#{!movimientoSitpForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										onkeypress="return keyEnterCheck(event, this)"
                            										required="false" />
                        																																<h:message for="observaciones" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Usuario
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{movimientoSitpForm.selectUsuario}"
        		                                                	disabled="#{!movimientoSitpForm.editing}"
            		                                            	immediate="false"
				                        							id="usuario"
                		            								                    		        								                        		                                	required="false"
    																title="Usuario">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{movimientoSitpForm.colUsuario}" />
								                                    	                </h:selectOneMenu>																									<h:message for="usuario" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																			                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Punto de Cuenta
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.puntoCuenta}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="puntoCuenta"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="puntoCuenta" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Punto de Cuenta
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaPuntoCuenta"
                		        									value="#{movimientoSitpForm.movimientoSitp.fechaPuntoCuenta}"
                    		    									readonly="#{!movimientoSitpForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="fechaPuntoCuenta" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Concurso
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.codConcurso}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="codConcurso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codConcurso" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																																							                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Manual
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="5"
	    	    		                							maxlength="5"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorCodManualCargo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorCodManualCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorCodManualCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Cargo Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorCodCargo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorCodCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorCodCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Descripci�n Cargo Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorDescripcionCargo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorDescripcionCargo"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        			                        								<f:verbatim><span class="required"> *</span></f:verbatim>
                        																																<h:message for="anteriorDescripcionCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo RAC/RPT Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorCodigoNomina}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorCodigoNomina"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorCodigoNomina" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Sede anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorCodSede}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorCodSede"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorCodSede" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Sede Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorNombreSede}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorNombreSede"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorNombreSede" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Dependencia Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorCodDependencia}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorCodDependencia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorCodDependencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Dependencia Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorNombreDependencia}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorNombreDependencia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorNombreDependencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Sueldo Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorSueldo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorSueldo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="anteriorSueldo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Compensaci�n Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorCompensacion}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorCompensacion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="anteriorCompensacion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Primas Cargo Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorPrimasCargo}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorPrimasCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="anteriorPrimasCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Primas Trabajador Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorPrimasTrabajador}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorPrimasTrabajador"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="anteriorPrimasTrabajador" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Grado Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorGrado}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorGrado"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorGrado" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Paso Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorPaso}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorPaso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorPaso" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Regi�n Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorCodRegion}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorCodRegion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorCodRegion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Nombre Regi�n Anterior
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{movimientoSitpForm.movimientoSitp.anteriorNombreRegion}"
                	    		    								                    	    										readonly="#{!movimientoSitpForm.editing}"
                        											id="anteriorNombreRegion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anteriorNombreRegion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																					<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsMovimientoSitp"
            										rendered="#{movimientoSitpForm.showData}">
		                        					<f:verbatim><table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{movimientoSitpForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!movimientoSitpForm.editing&&!movimientoSitpForm.deleting&&movimientoSitpForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{movimientoSitpForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!movimientoSitpForm.editing&&!movimientoSitpForm.deleting&&movimientoSitpForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{movimientoSitpForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{movimientoSitpForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{movimientoSitpForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{movimientoSitpForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{movimientoSitpForm.editing}" />
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{movimientoSitpForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>
