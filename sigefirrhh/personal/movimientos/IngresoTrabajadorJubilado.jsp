<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{ingresoTrabajadorJubiladoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left"></div>
			</td>
			<td width="570" valign="top">
			<h:form id="formIngresoTrabajadorJubilado">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Ingreso a N�mina Jubilado/Pensionado
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/IngresoTrabajadorJubilado.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
										<h:commandLink value="Ir a Datos Personales"                            						
                        						styleClass="listitem"
                        						action="#{ingresoTrabajadorJubiladoForm.redirectPersonal}"
                        						rendered="#{ingresoTrabajadorJubiladoForm.error}">	                            						
                        				</h:commandLink>
									</td>
								</tr>
							<f:subview 
								id="searchIngresoTrabajadorJubilado"
								rendered="#{!ingresoTrabajadorJubiladoForm.showData1}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Ingresar C�dula
												</td>
											</tr>
										</table>										
										<table width="100%" class="datatable"></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{ingresoTrabajadorJubiladoForm.findPersonalCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{ingresoTrabajadorJubiladoForm.findPersonalByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>												
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewSelectedPersonal"
								rendered="#{ingresoTrabajadorJubiladoForm.showData1
								}">
								<f:verbatim>
								<table class="toptable" width="100%">
								<tr>
								<td>
								<table class="datatable" width="100%">
									<tr>
            							<td width="25%" align="left">Trabajador:&nbsp;</td>
										<td width="75%" align="left">
											</f:verbatim>
	            								<h:commandLink value="#{ingresoTrabajadorJubiladoForm.personal}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
            								<f:verbatim>
            							</td>
									</tr>
									<tr>
                						<td align="left">Tipo de Personal</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{ingresoTrabajadorJubiladoForm.selectIdTipoPersonal}"
				                            	valueChangeListener="#{ingresoTrabajadorJubiladoForm.changeTipoPersonal}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorJubiladoForm.colTipoPersonal}" />
                                            </h:selectOneMenu>
											
										<f:verbatim></td>
									</tr>
									<tr>
										<td colspan="2" align="left" valign="middle">
        									</f:verbatim>
        									<f:subview
                								id="egresado"
                								rendered="#{ingresoTrabajadorJubiladoForm.egresado
                								}">
                								<f:verbatim>
                									El trabajador esta egresado de otro tipo de personal, �Desea continuar?
                								</f:verbatim>
                									
            											<h:commandButton image="/images/yes.gif"
            												action="#{ingresoTrabajadorJubiladoForm.continuarConEgresado}"
            												onclick="javascript:return clickMade()"
            												 />
                        	        		</f:subview>
                        	        		<f:verbatim>
                        	        	</td>
									</tr>
									<tr>
                						<td align="left">Movimiento</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{ingresoTrabajadorJubiladoForm.selectIdCausaPersonal}"
				                            	rendered="#{ingresoTrabajadorJubiladoForm.showCausaPersonal}"				                            	
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorJubiladoForm.colCausaPersonal}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Regi�n</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{ingresoTrabajadorJubiladoForm.selectIdRegion}"
				                            	rendered="#{ingresoTrabajadorJubiladoForm.showRegion}"
				                            	valueChangeListener="#{ingresoTrabajadorJubiladoForm.changeRegion}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorJubiladoForm.colRegion}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Sede</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{ingresoTrabajadorJubiladoForm.selectIdSede}"
				                            	rendered="#{ingresoTrabajadorJubiladoForm.showSede}"
				                            	valueChangeListener="#{ingresoTrabajadorJubiladoForm.changeSede}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorJubiladoForm.colSede}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>	
									<tr>
                						<td align="left">Dependencia</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{ingresoTrabajadorJubiladoForm.selectIdDependencia}"
				                            	rendered="#{ingresoTrabajadorJubiladoForm.showDependencia}"
				                            	
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorJubiladoForm.colDependencia}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Clasificador</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{ingresoTrabajadorJubiladoForm.selectIdManualCargo}"
				                            	rendered="#{ingresoTrabajadorJubiladoForm.showManualCargo}"
				                            	valueChangeListener="#{ingresoTrabajadorJubiladoForm.changeManualCargo}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorJubiladoForm.colManualCargo}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Cargo</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{ingresoTrabajadorJubiladoForm.selectIdCargo}"
				                            	rendered="#{ingresoTrabajadorJubiladoForm.showCargo}"
				                            	valueChangeListener="#{ingresoTrabajadorJubiladoForm.changeCargo}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorJubiladoForm.colCargo}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td>
                							Sueldo/Salario Basico
                						</td>
                						<td></f:verbatim>
			                        		<h:inputText
                    							size="12"
                    							id="Sueldo"
                    							maxlength="12"
                    							
                								value="#{ingresoTrabajadorJubiladoForm.sueldo}"
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								onkeypress="return keyFloatCheck(event, this)"
                																						 style="text-align:right"                         								required="false">
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                							</h:inputText>
                        								
                        			<f:verbatim></td>
                        					</tr>
									
	                					
                        					<tr>
                						<td align="left">
                							C�digo N�mina
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="8"
                    							maxlength="8"
                    							
                								value="#{ingresoTrabajadorJubiladoForm.codigoNomina}"                        								
                								id="CodigoNomina"
                								readonly="true"
                								required="false"
                								onchange="javascript:this.value=this.value.toUpperCase()"                								          												 >
                							</h:inputText>
                						<f:verbatim></td>
                        					</tr> 
                        					<tr>
                						<td align="left">
                							Horas
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="3"
                    							maxlength="2"
                    							
                								value="#{ingresoTrabajadorJubiladoForm.horas}"                        								
                								id="Horas"
                								required="false"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                						        onkeypress="return keyEnterCheck(event, this)"                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                        					</tr> 
									<tr>
                						<td align="left">Fecha Ingreso</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												
												value="#{ingresoTrabajadorJubiladoForm.fechaIngreso}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr>
									<tr>
                						<td align="left">Fecha Punto de Cuenta</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"		
																						
												value="#{ingresoTrabajadorJubiladoForm.fechaPuntoCuenta}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr> 
                        			<tr>
                						<td align="left">
                							Punto de Cuenta
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
			                        			
                    							size="15"
                    							maxlength="15"                    							
                								value="#{ingresoTrabajadorJubiladoForm.puntoCuenta}"                        								
                								id="PuntoCuenta"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                					<tr>
                						<td>
                							Observaciones
                						</td>
                						<td></f:verbatim>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="4"     
                    							             							
                    							id="observaciones"
                								value="#{ingresoTrabajadorJubiladoForm.observaciones}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />													
                					<f:verbatim></td>
                					</tr>		
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{ingresoTrabajadorJubiladoForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{ingresoTrabajadorJubilado.showFields}" />
    											<h:commandButton value="Cancelar" 
                                    				image="/images/cancel.gif"
                        							action="#{ingresoTrabajadorJubiladoForm.abort}"
                        							immediate="true"
                        							onclick="javascript:return clickMade()"
                	        							 />
                                    			
											<f:verbatim>
										</td>
									</tr>								
								</table>
								</td>
								</tr>
								</table>
							</f:verbatim>
							</f:subview>	
																									
						</td>
					</tr>
				</table>
				</h:form>
			</td>			
		</tr>
	</table>
</f:view>
</body>
</html>