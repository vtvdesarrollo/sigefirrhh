<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">

	<jsp:include page="/inc/functions.jsp" />
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formTrasladoLefp'].elements['formTrasladoLefp:reportId'].value;
			var name = 
				document.forms['formTrasladoLefp'].elements['formTrasladoLefp:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{trasladoLefpForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
		
			<td width="570" valign="top">
			<h:form id="formTrasladoLefp">
			<h:inputHidden id="reportId" value="#{trasladoLefpForm.reportId}" />
    		<h:inputHidden id="reportName" value="#{trasladoLefpForm.reportName}" />
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Traslado sujeto a LEFP
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/TrasladoLefp.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
                        					image="/images/close.gif"
                        					action="go_cancelOption"
                        					immediate="true"                        					
                        					onclick="javascript:return clickMade()"
                	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview id="Traslado">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="datatable">
										  	<tr>
			               						<td></f:verbatim>
													<h:outputText 
														value ="Imprimir Reporte Movimiento Realizado"
														rendered="#{trasladoLefpForm.showReport}"/>										
			               						</td><f:verbatim>
			               						<td colspan="2" align="center"></f:verbatim>										
			   										<h:commandButton image="/images/report.gif"
			   											action="#{trasladoLefpForm.runReport}"
			   											onclick="windowReport()"
			   											rendered="#{trasladoLefpForm.showReport}" />
			   									<f:verbatim><td>
			               					</tr>
                        					<tr>
                        						<td>
                        							Registro
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{trasladoLefpForm.idRegistro}"
	                                                    valueChangeListener="#{trasladoLefpForm.changeRegistro}"                                                    		                                                    
                                                    	immediate="false"                    								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"
														title="Registro">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoLefpForm.colRegistro}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Posici�n
                        						</td>
                        						<td>
													
                                                    </f:verbatim>
                                                  		<h:inputText size="6"															
															value="#{trasladoLefpForm.codigoNomina}"															
															rendered="#{trasladoLefpForm.showPosicion}"
															immediate="false"																											
			                        						/>		
			                        					<h:commandButton value="Buscar" 
				                            				image="/images/find.gif"
				                            				rendered="#{trasladoLefpForm.showPosicion}"
				                							action="#{trasladoLefpForm.buscarRegistroCargo}"				                							
				                							onclick="javascript:return clickMade()" />								
                                                                     						
                                                   
												<f:verbatim></td>
											</tr>
											
											<tr>
                        						<!-- td>
                        							C�dula
                        						</td-->
                        						<td>
													
                                                    </f:verbatim>
                                                  		<!--h:inputText size="12"															
															value="#{trasladoLefpForm.cedula}"															
															rendered="#{trasladoLefpForm.showPosicion}"
															immediate="false"																											
			                        						/-->		
			                        					<!--h:commandButton value="Buscar" 
				                            				image="/images/find.gif"
				                            				rendered="#{trasladoLefpForm.showPosicion}"
				                							action="#{trasladoLefpForm.buscarRegistroCargo}"				                							
				                							onclick="javascript:return clickMade()"
				        	        							 /-->								
                                                                     						
                                                   
												<f:verbatim></td>
											</tr>
											
										    
											
											
											 </f:verbatim> 
											<f:subview id="Trabajador" rendered="#{trasladoLefpForm.showTrabajador}">
												<f:verbatim>
													<tr>
				                						<td align="left">Trabajador</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 												
																value="#{trasladoLefpForm.registroCargos.trabajador}" />											
														<f:verbatim></td>
													</tr>
													<tr>
				                						<td align="left">Regi�n</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 												
																value="#{trasladoLefpForm.registroCargos.trabajador.dependencia.region}" />											
														<f:verbatim></td>
													</tr>													
													
													<tr>
				                						<td align="left">Dependencia</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoLefpForm.registroCargos.trabajador.dependencia}"/>											
														<f:verbatim></td>
													</tr>
													<tr>
				                						<td align="left">Cargo</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoLefpForm.registroCargos.trabajador.cargo}"/>											
														</td>
													</tr>
													<f:verbatim><tr>
				                						<td align="left">Grado</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoLefpForm.registroCargos.trabajador.cargo.grado}" />											
														<f:verbatim></td>
													</tr>		
													<tr>
				                						<td align="left">Paso</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoLefpForm.registroCargos.trabajador.paso}"/>											
														<f:verbatim></td>
													</tr>		
													<tr>
				                						<td align="left">Sueldo B�sico</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoLefpForm.registroCargos.trabajador.sueldoBasico}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
															</h:outputText>	              
														<f:verbatim></td>										
													</tr>
													</f:verbatim>
												</f:subview>
												<f:subview id="NuevaDependencia" rendered="#{trasladoLefpForm.showNuevaDependencia}">
													<f:verbatim>								
												<tr>
			                						<td align="left">Fecha Vigencia Movimiento</td>
			                						<td align="left">
			                						</f:verbatim>
														<h:inputText size="18"
															
															value="#{trasladoLefpForm.fechaMovimiento}"
															immediate="false"
															onkeypress="return keyEnterCheck(event, this)"
			                        						onblur="javascript:check_date(this)"
			                        						>																																			                         						
				                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
			                                                 pattern="dd-MM-yyyy" />
			                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim><f:verbatim><span class="required"> *</span></f:verbatim>	
			                        						                				
													<f:verbatim></td>										
												</tr>													
												 <tr>
                        						<td>
                        							Region
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{trasladoLefpForm.idRegion}"
	                                                    valueChangeListener="#{trasladoLefpForm.changeRegion}"                                                    	
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"                                                    	
														title="Region">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoLefpForm.colRegion}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>									
  											  
											<tr>
                        						<td>
                        							Dependencia
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{trasladoLefpForm.idDependencia}"
	                                                    valueChangeListener="#{trasladoLefpForm.changeDependencia}"                                                    	
	                                                    rendered="#{trasladoLefpForm.showDependencia}"
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"
														title="Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoLefpForm.colDependencia}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											
											
											<tr>
		                						<td>
		                							Observaciones
		                						</td>
		                						<td></f:verbatim>
					                        		<h:inputTextarea
		                    							cols="50"
		                    							rows="4"                    							
		                    							id="observaciones"
		                								value="#{trasladoLefpForm.observaciones}"
		                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
		                								required="false" />													
		                						<f:verbatim></td>
		                					</tr>
		                					</f:verbatim>	
		                					<f:subview
                								id="preguntaClasificacion"
                								rendered="#{trasladoLefpForm.showPreguntaClasificacion}">
                								<f:verbatim>
                									�Desea realizar una clasificaci�n al cargo?
                								</f:verbatim>
                									
            											<h:commandButton image="/images/yes.gif"
            												action="#{trasladoLefpForm.siClasificacion}"
            												onclick="javascript:return clickMade()"
            												 />
            											<h:commandButton image="/images/cancel.gif"
            												action="#{trasladoLefpForm.noClasificacion}"
            												onclick="javascript:return clickMade()"
            												 />
                        	        		</f:subview>
                        	        		
											<f:subview id="Clasificacion" rendered="#{trasladoLefpForm.showClasificacion}">
											<f:verbatim>
											 <tr>
                        						<td>
                        							Clasificador
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{trasladoLefpForm.idManualCargo}"
	                                                    valueChangeListener="#{trasladoLefpForm.changeManualCargo}"                                                    	
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"
														title="ManualCargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoLefpForm.colManualCargo}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{trasladoLefpForm.idCargo}"
	                                                                                                       	
	                                                    rendered="#{trasladoLefpForm.showCargo}"
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoLefpForm.colCargo}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                                          						
                                                    
												</td>
											</tr>
											</f:verbatim>  
											</f:subview>	
											<f:verbatim>
											<tr>
												<td></td>
												<td align="left">
													</f:verbatim>
		    											<h:commandButton image="/images/run.gif"
		    												action="#{trasladoLefpForm.ejecutar}"
		    												onclick="javascript:return clickMade()"
		    												rendered="#{ascensoLefp.showData}" />
		    											
		                                    			<h:commandButton value="Cancelar" 
				                            				image="/images/cancel.gif"
				                							action="#{trasladoLefpForm.abort}"
				                							immediate="true"
				                							onclick="javascript:return clickMade()"
				        	        							 />
													<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											</f:subview>
											<f:verbatim>
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>						

</h:form>
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>