<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formEgresoTrabajadorLefp'].elements['formEgresoTrabajadorLefp:reportId'].value;
			var name = 
				document.forms['formEgresoTrabajadorLefp'].elements['formEgresoTrabajadorLefp:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{egresoTrabajadorLefpForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
			<h:form id="formEgresoTrabajadorLefp">
			<h:inputHidden id="reportId" value="#{egresoTrabajadorLefpForm.reportId}" />
    		<h:inputHidden id="reportName" value="#{egresoTrabajadorLefpForm.reportName}" />
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Egreso de Trabajador sujeto a LEFP
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/EgresoTrabajadorLefp.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
										
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchEgresoTrabajador"
								rendered="#{!egresoTrabajadorLefpForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
			               						<td></f:verbatim>
													<h:outputText 
														value ="Imprimir Reporte Movimiento Realizado"
														rendered="#{egresoTrabajadorLefpForm.showReport}"/>										
			               						</td><f:verbatim>
			               						<td colspan="2" align="center"></f:verbatim>										
			   										<h:commandButton image="/images/report.gif"
			   											action="#{egresoTrabajadorLefpForm.runReport}"
			   											onclick="windowReport()"
			   											rendered="#{egresoTrabajadorLefpForm.showReport}" />
			   									<f:verbatim></td>
			               					</tr>
										
											<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{egresoTrabajadorLefpForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{egresoTrabajadorLefpForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{egresoTrabajadorLefpForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{egresoTrabajadorLefpForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{egresoTrabajadorLefpForm.findTrabajadorCodigoNomina}"
														onfocus="return deleteZero(event, this)"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{egresoTrabajadorLefpForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{egresoTrabajadorLefpForm.showResultTrabajador&&
								!egresoTrabajadorLefpForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{egresoTrabajadorLefpForm.resultTrabajador}"                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{egresoTrabajadorLefpForm.showEgresoTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif" />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							<f:subview
								id="viewSelectedTrabajador"
								rendered="#{egresoTrabajadorLefpForm.selectedTrabajador&&
								egresoTrabajadorLefpForm.showData
								}">
								<f:verbatim>
								<table class="datatable" width="100%">
									<tr>
            							<td width="20%" align="left">Trabajador:&nbsp;</td>
										<td width="80%" align="left">
											</f:verbatim>
	            								<h:commandLink value="#{egresoTrabajadorLefpForm.trabajador}"                            						
	                            						styleClass="listitem">
	                            						
	                            				</h:commandLink>
            								<f:verbatim>
            							</td>
									</tr>
									<tr>
                        				<td align="left">Movimiento</td>
                						<td align="left">
                    						</f:verbatim>
    			                            	<h:selectOneMenu value="#{egresoTrabajadorLefpForm.selectCausaPersonal}"
                                                	immediate="false">
    												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    <f:selectItems value="#{egresoTrabajadorLefpForm.colCausaPersonal}" />
                                                </h:selectOneMenu>
    										<f:verbatim>
										</td>
									</tr>
									<tr>
                						<td align="left">
                							Fecha Egreso
                						</td>
                						<td align="left">
                    						</f:verbatim>
    			                        		<h:inputText
                        							size="10"
                        							maxlength="10"
                        							id="FechaEgresoReal"
                    								value="#{egresoTrabajadorLefpForm.fechaEgresoReal}"                        								
                    								onchange="javascript:this.value=this.value.toUpperCase()"
                    								onkeypress="return keyEnterCheck(event, this)"
                    								onblur="javascript:check_date(this)"
                    								required="true">
                                                  <f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                    pattern="dd-MM-yyyy" />
                    							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim><f:verbatim><span class="required"> *</span></f:verbatim>	
                    						<f:verbatim>
                						</td>                        						              
                					</tr>
                					</f:verbatim>
                					<f:verbatim>
                					
                					</f:verbatim>
                					<f:verbatim>
                					
                        			<tr>
                						<td>
                							Observaciones
                						</td>
                						<td></f:verbatim>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="4"                    							
                    							id="observaciones"
                								value="#{egresoTrabajadorLefpForm.observaciones}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />													
                						<f:verbatim></td>
                					</tr>		  		
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{egresoTrabajadorLefpForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{egresoTrabajadorLefp.showData}" />
    											
                                    			<h:commandButton value="Cancelar" 
		                            				image="/images/cancel.gif"
		                							action="#{egresoTrabajadorLefpForm.abort}"
		                							immediate="true"
		                							onclick="javascript:return clickMade()"
		        	        							 />
											<f:verbatim>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview> 
							
						</td>
					</tr>
				</table>

				</h:form>

			</td>			
		</tr>
	</table>

</f:view>

</body>
</html>