<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{reingresoTrabajadorRegistroForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left"></div>
			</td>
			<td width="570" valign="top">
			<h:form id="formIngresoTrabajadorLefp">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Reingreso no Sujeto a LEFP con Registro
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/ReingresoTrabajadorRegistro.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
										<h:commandLink value="Ir a Datos Personales"                            						
                        						styleClass="listitem"
                        						action="#{reingresoTrabajadorRegistroForm.redirectPersonal}"
                        						rendered="#{reingresoTrabajadorRegistroForm.error}">	                            						
                        				</h:commandLink>
									</td>
								</tr>
							<f:subview 
								id="searchIngresoTrabajadorRegistro"
								rendered="#{!reingresoTrabajadorRegistroForm.showData1}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Ingresar
												</td>
											</tr>
										</table>										
										<table width="100%" class="datatable">
												
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{reingresoTrabajadorRegistroForm.findPersonalCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{reingresoTrabajadorRegistroForm.findPersonalByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>												
										</table>
									</td>
								</tr>
							</table>
							</f:verbatim>
							</f:subview>
							<f:subview
								id="viewSelectedPersonal"
								rendered="#{reingresoTrabajadorRegistroForm.showData1
								}">
								<f:verbatim>
								<table class="toptable" width="100%">
								<tr>
								<td>
								<table class="datatable" width="100%">
									<tr>
            							<td width="20%" align="left">Trabajador:&nbsp;</td>
										<td width="80%" align="left">
											</f:verbatim>
	            								<h:commandLink value="#{reingresoTrabajadorRegistroForm.personal}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
            								<f:verbatim>
            							</td>
									</tr>
									<tr>
                						<td align="left">Tipo de Personal</td>
                						<td align="left">
                    						</f:verbatim>
    			                            	<h:selectOneMenu value="#{reingresoTrabajadorRegistroForm.selectIdTipoPersonal}"
    				                            	valueChangeListener="#{reingresoTrabajadorRegistroForm.changeCausaPersonalForTipoPersonal}"
    				                            	onchange="this.form.submit()"
                                                	immediate="false">
    												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    <f:selectItems value="#{reingresoTrabajadorRegistroForm.colTipoPersonal}" />
                                                </h:selectOneMenu>
    											
    										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td colspan="2" align="left" valign="middle">
        									</f:verbatim>
        									<f:subview
                								id="egresado"
                								rendered="#{reingresoTrabajadorRegistroForm.egresado
                								}">
                								<f:verbatim>
                									El trabajador esta egresado de otro tipo de personal, �Desea continuar?
                								</f:verbatim>
                									
            											<h:commandButton image="/images/yes.gif"
            												action="#{reingresoTrabajadorRegistroForm.continuarConEgresado}"
            												onclick="javascript:return clickMade()"
            												 />
                        	        		</f:subview>
                	        				<f:verbatim>
                	        			</td>
									</tr>
									<tr>
                						<td align="left">Movimiento</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{reingresoTrabajadorRegistroForm.selectIdCausaPersonal}"
				                            	rendered="#{reingresoTrabajadorRegistroForm.showCausaPersonal}"				                            	
				                            	valueChangeListener="#{reingresoTrabajadorRegistroForm.changeCausaPersonal}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reingresoTrabajadorRegistroForm.colCausaPersonal}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Posiciones Vacantes</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{reingresoTrabajadorRegistroForm.selectIdRegistroCargos}"
				                            	rendered="#{reingresoTrabajadorRegistroForm.showRegistroCargos}"
				                            	valueChangeListener="#{reingresoTrabajadorRegistroForm.changeRegistroCargos}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reingresoTrabajadorRegistroForm.colRegistroCargos}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Regi�n</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="40"												
												rendered="#{reingresoTrabajadorRegistroForm.showFields}"
												value="#{reingresoTrabajadorRegistroForm.nombreRegion}"
												immediate="false"
												readonly="true" />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Sede</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="40"												
												rendered="#{reingresoTrabajadorRegistroForm.showFields}"
												value="#{reingresoTrabajadorRegistroForm.nombreSede}"
												immediate="false"
												readonly="true" />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Dependencia</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="85"												
												rendered="#{reingresoTrabajadorRegistroForm.showFields}"
												value="#{reingresoTrabajadorRegistroForm.nombreDependencia}"
												immediate="false"
												readonly="true" />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Cargo</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="60"
												rendered="#{reingresoTrabajadorRegistroForm.showFields}"
												value="#{reingresoTrabajadorRegistroForm.descripcionCargo}"
												immediate="false"
												readonly="true"/>											
										</td>
									</tr>
									<f:verbatim><tr>
                						<td align="left">Grado</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="3"
												rendered="#{reingresoTrabajadorRegistroForm.showFields}"
												value="#{reingresoTrabajadorRegistroForm.grado}"
												immediate="false"
												readonly="true" />											
										<f:verbatim></td>
									</tr>		
									<tr>
                						<td align="left">Paso</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="3"
												value="#{reingresoTrabajadorRegistroForm.paso}"
												immediate="false"
												readonly="false" />											
										<f:verbatim></td>
									</tr>		
									<tr>
                						<td align="left">Sueldo/Salario B�sico</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{reingresoTrabajadorRegistroForm.showFields}"
												value="#{reingresoTrabajadorRegistroForm.sueldo}"
												immediate="false"
												readonly="true">
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>	              
										<f:verbatim></td>										
									</tr>	
									
									<tr>
                						<td align="left">Fecha Ingreso</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{reingresoTrabajadorRegistroForm.showFields}"
												value="#{reingresoTrabajadorRegistroForm.fechaIngreso}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr>	
									</tr>	
    									<td>
    										Pagar Retroactivo?
    									</td>
    									<td width="100%">
    										</f:verbatim>
    										<h:selectOneMenu value="#{reingresoTrabajadorRegistroForm.pagarRetroactivo}"    											
    											rendered="#{reingresoTrabajadorRegistroForm.showFields}"    											
    											onchange="this.form.submit()"
    											immediate="true">
                                            	<f:selectItem itemLabel="No" itemValue="N" />  
                                            	<f:selectItem itemLabel="Si" itemValue="S" />
                                            </h:selectOneMenu>
                                            	<f:verbatim>
    									</td>
    								</tr>									 
                        			<tr>
                						<td align="left">Fecha Punto de Cuenta</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{reingresoTrabajadorRegistroForm.showFields}"
												value="#{reingresoTrabajadorRegistroForm.fechaPuntoCuenta}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr> 
                        			<tr>
                						<td align="left">
                							Punto de Cuenta
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="15"
                    							maxlength="15"
                    							rendered="#{reingresoTrabajadorRegistroForm.showFields}"
                								value="#{reingresoTrabajadorRegistroForm.puntoCuenta}"                        								
                								id="PuntoCuenta"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                					<tr>
                						<td align="left">
                							Concurso
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="6"
                    							maxlength="6"
                    							rendered="#{reingresoTrabajadorRegistroForm.showFields}"
                								value="#{reingresoTrabajadorRegistroForm.codConcurso}"                        								
                								id="CodConcurso"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                        			<tr>
                						<td>
                							Observaciones
                						</td>
                						<td></f:verbatim>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="4"     
                    							rendered="#{reingresoTrabajadorRegistroForm.showFields}"               							
                    							id="observaciones"
                								value="#{reingresoTrabajadorRegistroForm.observaciones}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />													
                						<f:verbatim></td>
                					</tr>		
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{reingresoTrabajadorRegistroForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{reingresoTrabajadorRegistro.showFields}" />
    											<h:commandButton value="Cancelar" 
				                    				image="/images/cancel.gif"
				        							action="#{reingresoTrabajadorRegistroForm.abort}"
				        							immediate="true"
				        							onclick="javascript:return clickMade()"
					        							 />
                                    			
											<f:verbatim>
										</td>
									</tr>								
								</table>
								</td>
								</tr>
								</table>
							</f:verbatim>
							</f:subview>	
																									
						</td>
					</tr>
				</table>
				</h:form>
			</td>			
		</tr>
	</table>
</f:view>
</body>
</html>