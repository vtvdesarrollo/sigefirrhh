<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formTrasladoArt35Lefp'].elements['formTrasladoArt35Lefp:reportId'].value;
			var name = 
				document.forms['formTrasladoArt35Lefp'].elements['formTrasladoArt35Lefp:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{trasladoTransferenciaCompetenciaLefpForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
			<h:form id="formTrasladoArt35Lefp">	
			<h:inputHidden id="reportId" value="#{trasladoTransferenciaCompetenciaLefpForm.reportId}" />
    		<h:inputHidden id="reportName" value="#{trasladoTransferenciaCompetenciaLefpForm.reportName}" />				
    				
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Traslado por Transferencia de Competencia
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/TrasladoTransferenciaCompetenciaLefp.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
										
										<h:commandLink value="Ir a Datos Personales"                            						
                        						styleClass="listitem"
                        						action="#{trasladoTransferenciaCompetenciaLefpForm.redirectPersonal}"
                        						rendered="#{trasladoTransferenciaCompetenciaLefpForm.error}">	                            						
                        				</h:commandLink>
									</td>
								</tr>
							<f:subview 
								id="searchIngresoTrabajadorRegistro"
								rendered="#{!trasladoTransferenciaCompetenciaLefpForm.showData1}">
								<f:verbatim>
								<tr>
									<td>
										<table width="100%" class="bandtable">
										  <tr>
			               						<td></f:verbatim>
													<h:outputText 
														value ="Imprimir Reporte Movimiento Realizado"
														rendered="#{trasladoTransferenciaCompetenciaLefpForm.showReport}"/>										
			               						</td><f:verbatim>
			               						<td colspan="2" align="center"></f:verbatim>										
			   										<h:commandButton image="/images/report.gif"
			   											action="#{trasladoTransferenciaCompetenciaLefpForm.runReport}"
			   											onclick="windowReport()"
			   											rendered="#{trasladoTransferenciaCompetenciaLefpForm.showReport}" />
			   									<f:verbatim><td>
			               					</tr>
											<tr>
												<td>
													Ingresar
												</td>
											</tr>
										</table>										
										<table width="100%" class="datatable">
												
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{trasladoTransferenciaCompetenciaLefpForm.findPersonalCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{trasladoTransferenciaCompetenciaLefpForm.findPersonalByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>												
										</table>
									</td>
								</tr>
							</table>
							</f:verbatim>
							</f:subview>
							<f:subview
								id="viewSelectedPersonal"
								rendered="#{trasladoTransferenciaCompetenciaLefpForm.showData1
								}">
								<f:verbatim>
								<table class="toptable" width="100%">
								<tr>
								<td>
								<table class="datatable" width="100%">
									<tr>
            							<td width="20%" align="left">Trabajador:&nbsp;</td>
										<td width="80%" align="left">
											</f:verbatim>
	            								<h:commandLink value="#{trasladoTransferenciaCompetenciaLefpForm.personal}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
            								<f:verbatim>
            							</td>
									</tr>
									<tr>
                						<td align="left">Tipo de Personal</td>
                						<td align="left">
                    						</f:verbatim>
    			                            	<h:selectOneMenu value="#{trasladoTransferenciaCompetenciaLefpForm.selectIdTipoPersonal}"
    				                            	valueChangeListener="#{trasladoTransferenciaCompetenciaLefpForm.changeCausaPersonalForTipoPersonal}"
    				                            	onchange="this.form.submit()"
                                                	immediate="false">
    												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    <f:selectItems value="#{trasladoTransferenciaCompetenciaLefpForm.colTipoPersonal}" />
                                                </h:selectOneMenu>
    											
    										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td colspan="2" align="left" valign="middle">
        									</f:verbatim>
        									<f:subview
                								id="egresado"
                								rendered="#{trasladoTransferenciaCompetenciaLefpForm.egresado
                								}">
                								<f:verbatim>
                									El trabajador esta egresado de otro tipo de personal, �Desea continuar?
                								</f:verbatim>
                									
            											<h:commandButton image="/images/yes.gif"
            												action="#{trasladoTransferenciaCompetenciaLefpForm.continuarConEgresado}"
            												onclick="javascript:return clickMade()"
            												 />
                        	        		</f:subview>
                	        				<f:verbatim>
                	        			</td>
									</tr>
									
									<tr>
                						<td align="left">Posiciones Vacantes</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{trasladoTransferenciaCompetenciaLefpForm.selectIdRegistroCargos}"
				                            	rendered="#{trasladoTransferenciaCompetenciaLefpForm.showRegistroCargos}"
				                            	valueChangeListener="#{trasladoTransferenciaCompetenciaLefpForm.changeRegistroCargos}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{trasladoTransferenciaCompetenciaLefpForm.colRegistroCargos}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Regi�n</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="40"												
												rendered="#{trasladoTransferenciaCompetenciaLefpForm.showFields}"
												value="#{trasladoTransferenciaCompetenciaLefpForm.nombreRegion}"
												immediate="false"
												readonly="true" />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Sede</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="40"												
												rendered="#{trasladoTransferenciaCompetenciaLefpForm.showFields}"
												value="#{trasladoTransferenciaCompetenciaLefpForm.nombreSede}"
												immediate="false"
												readonly="true" />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Dependencia</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="85"												
												rendered="#{trasladoTransferenciaCompetenciaLefpForm.showFields}"
												value="#{trasladoTransferenciaCompetenciaLefpForm.nombreDependencia}"
												immediate="false"
												readonly="true" />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Cargo</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="60"
												rendered="#{trasladoTransferenciaCompetenciaLefpForm.showFields}"
												value="#{trasladoTransferenciaCompetenciaLefpForm.descripcionCargo}"
												immediate="false"
												readonly="true"/>											
										</td>
									</tr>
									<f:verbatim><tr>
                						<td align="left">Grado</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="3"
												rendered="#{trasladoTransferenciaCompetenciaLefpForm.showFields}"
												value="#{trasladoTransferenciaCompetenciaLefpForm.grado}"
												immediate="false"
												readonly="true" />											
										<f:verbatim></td>
									</tr>		
									<tr>
                						<td align="left">Sueldo B�sico</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{trasladoTransferenciaCompetenciaLefpForm.showFields}"
												value="#{trasladoTransferenciaCompetenciaLefpForm.sueldo}"
												immediate="false"
												readonly="true">
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:inputText>	              
										<f:verbatim></td>										
									</tr>												
									<tr>
                						<td align="left">Fecha Vigencia Movimiento</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{trasladoTransferenciaCompetenciaLefpForm.showFields}"
												value="#{trasladoTransferenciaCompetenciaLefpForm.fechaIngreso}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim><f:verbatim><span class="required"> *</span></f:verbatim>	
                        						                				
										<f:verbatim></td>										
									</tr>																	
                						                    				
                        			<tr>
                						<td align="left">Fecha Punto de Cuenta</td>   
                						<td align="left">             						
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{trasladoTransferenciaCompetenciaLefpForm.showFields}"
												value="#{trasladoTransferenciaCompetenciaLefpForm.fechaPuntoCuenta}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim><f:verbatim><span class="required"> *</span></f:verbatim>	
                        						                				
										<f:verbatim></td>										
									</tr> 
                        			<tr>
                						<td align="left">
                							Punto de Cuenta
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="15"
                    							maxlength="15"
                    							rendered="#{trasladoTransferenciaCompetenciaLefpForm.showFields}"
                								value="#{trasladoTransferenciaCompetenciaLefpForm.puntoCuenta}"                        								
                								id="PuntoCuenta"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                					
                					<tr>
                						<td>
                							Observaciones
                						</td>
                						<td></f:verbatim>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="4"     
                    							rendered="#{trasladoTransferenciaCompetenciaLefpForm.showFields}"               							
                    							id="observaciones"
                								value="#{trasladoTransferenciaCompetenciaLefpForm.observaciones}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />													
                						<f:verbatim></td>
                					</tr>		   
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{trasladoTransferenciaCompetenciaLefpForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{ingresoTrabajadorLefp.showFields}" />
    											<h:commandButton value="Cancelar" 
		                            				image="/images/cancel.gif"
		                							action="#{trasladoTransferenciaCompetenciaLefpForm.abort}"
		                							immediate="true"
		                							onclick="javascript:return clickMade()"
		        	        					 />		        	        					
											<f:verbatim>
										</td>
									</tr>								
								</table>
								</td>
								</tr>
								</table>
							</f:verbatim>
							</f:subview>	
																									
						</td>
					</tr>
				</table>
				</h:form>
			</td>			
		</tr>
	</table>
</f:view>
</body>
</html>