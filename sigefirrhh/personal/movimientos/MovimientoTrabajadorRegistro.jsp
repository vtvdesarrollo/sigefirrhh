<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{movimientoTrabajadorRegistroForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left"></div>
			</td>
			<td width="570" valign="top">
			<h:form id="formIngresoTrabajadorLefp">
			<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Cambio Tipo Personal con Registro
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/EgresoTrabajadorRegistro.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchMovimientoTrabajador"
								rendered="#{!movimientoTrabajadorRegistroForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
											<f:verbatim><tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{movimientoTrabajadorRegistroForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{movimientoTrabajadorRegistroForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{movimientoTrabajadorRegistroForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{movimientoTrabajadorRegistroForm.findTrabajadorCodigoNomina}"
														onfocus="return deleteZero(event, this)"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{movimientoTrabajadorRegistroForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{movimientoTrabajadorRegistroForm.showResultTrabajador&&
								!movimientoTrabajadorRegistroForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{movimientoTrabajadorRegistroForm.resultTrabajador}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{movimientoTrabajadorRegistroForm.showEgresoTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>0
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif" />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif" />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif" />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif" />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif" />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
						</td>
					</tr>
				</table>
			
			
			
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							
							
							<f:subview
								id="viewSelectedTrabajador"
								rendered="#{movimientoTrabajadorRegistroForm.selectedTrabajador&&
								movimientoTrabajadorRegistroForm.showData
								}">
								<f:verbatim>
								<table class="toptable" width="100%">
								<tr>
								<td>
								<table class="datatable" width="100%">
									<tr>
            							<td width="20%" align="left">Trabajador:&nbsp;</td>
										<td width="80%" align="left">
											</f:verbatim>
	            								<h:commandLink value="#{movimientoTrabajadorRegistroForm.trabajador}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
            								<f:verbatim>
            							</td>
									</tr>
									<tr>
                						<td align="left">Tipo de Personal</td>
                						<td align="left">
                    						</f:verbatim>
    			                            	<h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.selectIdTipoPersonal}"
    				                            	valueChangeListener="#{movimientoTrabajadorRegistroForm.changeCausaPersonalForTipoPersonal}"
    				                            	onchange="this.form.submit()"
                                                	immediate="false">
    												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    <f:selectItems value="#{movimientoTrabajadorRegistroForm.colTipoPersonal}" />
                                                </h:selectOneMenu>
    											
    										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td colspan="2" align="left" valign="middle">
        									</f:verbatim>
        									<f:subview
                								id="egresado"
                								rendered="#{movimientoTrabajadorRegistroForm.egresado
                								}">
                								<f:verbatim>
                									El trabajador esta egresado de otro tipo de personal, �Desea continuar?
                								</f:verbatim>
                									
            											<h:commandButton image="/images/yes.gif"
            												action="#{movimientoTrabajadorRegistroForm.continuarConEgresado}"
            												onclick="javascript:return clickMade()"
            												 />
                        	        		</f:subview>
                	        				<f:verbatim>
                	        			</td>
									</tr>
									<tr>
                						<td align="left">Movimiento</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.selectIdCausaPersonal}"
				                            				                            	
				                            	valueChangeListener="#{movimientoTrabajadorRegistroForm.changeCausaPersonal}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorRegistroForm.colCausaPersonal}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Indique Paso</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="3"
												value="#{movimientoTrabajadorRegistroForm.paso}"
												immediate="false"
												readonly="false" />											
										<f:verbatim></td>
									</tr>	
									</f:verbatim>	
									<f:subview
            							id="pregunta"
        								rendered="#{movimientoTrabajadorRegistroForm.selectedTrabajador}">
        								<f:verbatim>
        								<td align="left">Mostrar Cargos Vacantes</td>
                						<td align="left">
        									
        								</f:verbatim>
        									
											<h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.seleccion}"
					                            	rendered="#{movimientoTrabajadorRegistroForm.selectedTrabajador}"
					                            	valueChangeListener="#{movimientoTrabajadorRegistroForm.changeSeleccion}"
					                            	onchange="this.form.submit()"
	                                            	immediate="false">
	                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
													<f:selectItem itemLabel="Todos" itemValue="T" />
													<f:selectItem itemLabel="Por Region" itemValue="R" />	                                               
	                                            </h:selectOneMenu>		
											<f:verbatim>	</td></f:verbatim>
                    	        	</f:subview>
                    	        	<f:subview
        								id="busquedaTodos"
        								rendered="#{movimientoTrabajadorRegistroForm.showBusquedaTodos}">     
        								<f:verbatim>       						
										<tr>
	                						<td align="left">Posiciones Vacantes</td>
	                						<td align="left">
	                						</f:verbatim>
				                            	<h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.selectIdRegistroCargos}"					                            	
					                            	valueChangeListener="#{movimientoTrabajadorRegistroForm.changeRegistroCargos}"
					                            	rendered="#{movimientoTrabajadorRegistroForm.showRegistroCargos}"
					                            	onchange="this.form.submit()"
	                                            	immediate="false">
													<f:selectItem itemLabel="Seleccione" itemValue="0" />
	                                                <f:selectItems value="#{movimientoTrabajadorRegistroForm.colRegistroCargos}" />
	                                            </h:selectOneMenu>											
											<f:verbatim></td>
										</tr>									
									<tr>
                						<td align="left">Regi�n</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText												
												rendered="#{movimientoTrabajadorRegistroForm.showFields}"
												value="#{movimientoTrabajadorRegistroForm.nombreRegion}"
												
												/>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Sede</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 										
												rendered="#{movimientoTrabajadorRegistroForm.showFields}"
												value="#{movimientoTrabajadorRegistroForm.nombreSede}"
												
												 />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Dependencia</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 									
												rendered="#{movimientoTrabajadorRegistroForm.showFields}"
												value="#{movimientoTrabajadorRegistroForm.nombreDependencia}"
												
												/>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Cargo</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 
												rendered="#{movimientoTrabajadorRegistroForm.showFields}"
												value="#{movimientoTrabajadorRegistroForm.descripcionCargo}"
												
												/>											
										</td>
									</tr>
									<f:verbatim><tr>
                						<td align="left">Grado</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText
												rendered="#{movimientoTrabajadorRegistroForm.showFields}"
												value="#{movimientoTrabajadorRegistroForm.grado}"
												
												 />											
										<f:verbatim></td>
									</tr>		
									
									<tr>
                						<td align="left">Sueldo/Salario B�sico</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 
												rendered="#{movimientoTrabajadorRegistroForm.showFields}"
												value="#{movimientoTrabajadorRegistroForm.sueldo}"
												
												>
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:outputText>	              
										<f:verbatim></td>										
									</tr>	
									</f:verbatim>
								</f:subview>
								<f:subview
									id="busquedaRegion"
        							rendered="#{movimientoTrabajadorRegistroForm.showBusquedaRegion}">  								
									<f:verbatim>
						 			<tr>
                						<td>
                							Region
                						</td>
                						<td></f:verbatim>											
                                            <h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.idRegion}"
                                                valueChangeListener="#{movimientoTrabajadorRegistroForm.changeRegion}"  
                                                 rendered="#{movimientoTrabajadorRegistroForm.showRegion}"                                                 	
                                            	immediate="false"                        								                        								                                                    	                                                    	
                                            	onchange="this.form.submit()"                                                    	
												title="Region">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorRegistroForm.colRegion}" />
												
                                            </h:selectOneMenu><f:verbatim>
                                            
                                            </f:verbatim>                        						
                                            
										<f:verbatim></td>
									</tr>									
									  <tr>
                						<td>
                							Sede
                						</td>
                						<td></f:verbatim>
											
                                            <h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.idSede}"
                                                valueChangeListener="#{movimientoTrabajadorRegistroForm.changeSede}"                                                    	
                                            	immediate="false"                        								                        								                                                    	                                                    	
                                            	onchange="this.form.submit()"
                                            	rendered="#{movimientoTrabajadorRegistroForm.showSede}" 
												title="Sede">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorRegistroForm.colSede}" />
												
                                            </h:selectOneMenu><f:verbatim>
                                            
                                            </f:verbatim>                        						
                                            
										<f:verbatim></td>
									</tr>
									<tr>
                						<td>
                							Dependencia
                						</td>
                						<td></f:verbatim>
											
                                            <h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.idDependencia}"
                                                valueChangeListener="#{movimientoTrabajadorRegistroForm.changeDependencia}"                                                    	
                                                rendered="#{movimientoTrabajadorRegistroForm.showDependencia}"
                                            	immediate="false"                        								                        								                                                    	                                                    	
                                            	onchange="this.form.submit()"
												title="Dependencia">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorRegistroForm.colDependencia}" />
												
                                            </h:selectOneMenu><f:verbatim>                                                                                                           						
                                            
										</td>
									</tr>		
									<tr>
                						<td align="left">Posiciones Vacantes</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.selectIdRegistroCargos}"
				                            	rendered="#{movimientoTrabajadorRegistroForm.showRegistroCargos}"
				                            	valueChangeListener="#{movimientoTrabajadorRegistroForm.changeRegistroCargos}"
				                            	
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorRegistroForm.colRegistroCargos}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									</f:verbatim>							
								</f:subview>
								<f:verbatim>
									<tr>
                						<td align="left">Fecha Vigencia</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												
												value="#{movimientoTrabajadorRegistroForm.fechaIngreso}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr>	
									</tr>	
    									<td>
    										Pagar Retroactivo?
    									</td>
    									<td width="100%">
    										</f:verbatim>
    										<h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.pagarRetroactivo}"    											
    											rendered="#{movimientoTrabajadorRegistroForm.showFields}"    											
    											onchange="this.form.submit()"
    											immediate="true">
                                            	<f:selectItem itemLabel="No" itemValue="N" />  
                                            	<f:selectItem itemLabel="Si" itemValue="S" />
                                            </h:selectOneMenu>
                                            	<f:verbatim>
    									</td>
    								</tr>		
    								</tr>	
    									<td>
    										Tiene Continiudad?
    									</td>
    									<td width="100%">
    										</f:verbatim>
    										<h:selectOneMenu value="#{movimientoTrabajadorRegistroForm.tieneContinuidad}"    											
    											rendered="#{movimientoTrabajadorRegistroForm.showFields}"   											
    											onchange="this.form.submit()"
    											immediate="true">
                                            	<f:selectItem itemLabel="No" itemValue="N" />  
                                            	<f:selectItem itemLabel="Si" itemValue="S" />
                                            </h:selectOneMenu>
                                            	<f:verbatim>
    									</td>
    								</tr>									 
                        			<tr>
                						<td align="left">Fecha Punto de Cuenta</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{movimientoTrabajadorRegistroForm.showFields}"
												value="#{movimientoTrabajadorRegistroForm.fechaPuntoCuenta}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr> 
                        			<tr>
                						<td align="left">
                							Punto de Cuenta
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="15"
                    							maxlength="15"
                    							rendered="#{movimientoTrabajadorRegistroForm.showFields}"
                								value="#{movimientoTrabajadorRegistroForm.puntoCuenta}"                        								
                								id="PuntoCuenta"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                					<tr>
                						<td align="left">
                							Concurso
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="6"
                    							maxlength="6"
                    							rendered="#{movimientoTrabajadorRegistroForm.showFields}"
                								value="#{movimientoTrabajadorRegistroForm.codConcurso}"                        								
                								id="CodConcurso"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                        			<tr>
                						<td>
                							Observaciones
                						</td>
                						<td></f:verbatim>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="4"     
                    							rendered="#{movimientoTrabajadorRegistroForm.showFields}"               							
                    							id="observaciones"
                								value="#{movimientoTrabajadorRegistroForm.observaciones}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />													
                						<f:verbatim></td>
                					</tr>		
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{movimientoTrabajadorRegistroForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{movimientoTrabajadorRegistro.showFields}" />
    											<h:commandButton value="Cancelar" 
				                    				image="/images/cancel.gif"
				        							action="#{movimientoTrabajadorRegistroForm.abort}"
				        							immediate="true"
				        							onclick="javascript:return clickMade()"
					        							 />
                                    			
											<f:verbatim>
										</td>
									</tr>								
								</table>
								</td>
								</tr>
								</table>
							</f:verbatim>
							</f:subview>	
																									
						</td>
					</tr>
				</table>
				</h:form>
			</td>			
		</tr>
	</table>
</f:view>
</body>
</html>