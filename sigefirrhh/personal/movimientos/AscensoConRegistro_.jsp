<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
        
    	function keyFloatCheck(eventObj, obj)
		{
			var keyCode
		
			// Check For Browser Type
			if (document.all){ 
				keyCode=eventObj.keyCode
			}
			else{
				keyCode=eventObj.which
			}
		
			var str=obj.value
		
			if(keyCode==13){ 
				return false
			}
			
			if(keyCode==44){ 
				if (str.indexOf(",")>0){
					return false
				}
			}
		
			if((keyCode<48 || keyCode >58)   &&   (keyCode != 44)){ 
				return false
			}
		
			return true
		}
		function keyIntegerCheck(eventObj, obj)
		{
			var keyCode
		
			if (document.all){ 
				keyCode=eventObj.keyCode
			}
			else{
				keyCode=eventObj.which
			}
		
			if(keyCode==13){ 
				return false
			}
			if((keyCode<48 || keyCode >58)){
				return false
			}
		
			return true
		}
		function keyEnterCheck(eventObj, obj)
		{
			var keyCode
		
			if (document.all){ 
				keyCode=eventObj.keyCode
			}
			else{
				keyCode=eventObj.which
			}
		
			if(keyCode==13){ 
				return false
			}		
			return true
		}
		
		var double_delay = null;
		var delay_value = 1000; // 1 second.
		var click_count=0;
		
		function clickMade() {
		
		if (click_count>0) {
		    // it is a double click
			clearTimeout(double_delay);
			double_delay=null;
			return false
		  } else {
		    // it is a single click
			click_count++;
			double_delay = setTimeout("click_count=0",delay_value);
			return true

		  }

		}
		
		function firstFocus(){
			var paso = "false";			
			for (i = 0; i < document.forms.length; i++){
				for (j = 0; j < document.forms[i].elements.length; j++){					
					if (document.forms[i].elements[j].type =="text" || document.forms[i].elements[j].type =="select-one"){					
						paso = "true";
						document.forms[i].elements[j].focus();					
						break;
					}
				}				
				if (paso =="true"){
					break;
				}
			}
			
		}
		
		function fieldEmpty(field){
			var varField = field;
			var varValue = "";
			varValue = varField.value;
			if (varValue.length == 0) {	
				varField.value = 0;
			}
			
		}
		
		function deleteZero(eventObj, obj)
		{
		
			var keyCode
		
			// Check For Browser Type
			if (document.all){ 
				keyCode=eventObj.keyCode
			}
			else{
				keyCode=eventObj.which
			}					
		
			var varField = obj;
			var varValue = "";
			varValue = varField.value;
			if (varValue.length == 1 && keyCode == 0) {	
				varField.value = "";
			}
			
			return true
		}
		
		
		function check_date(field){
		var checkstr = "0123456789";
		var DateField = field;
		var Datevalue = "";
		var DateTemp = "";
		var seperator = "-";
		var day;
		var month;
		var year;
		var leap = 0;
		var err = 0;
		var i;
		   err = 0;
		   DateValue = DateField.value;
		   /* Delete all chars except 0..9 */
		   for (i = 0; i < DateValue.length; i++) {
			  if (checkstr.indexOf(DateValue.substr(i,1)) >= 0) {
			     DateTemp = DateTemp + DateValue.substr(i,1);
			  }
		   }
		   DateValue = DateTemp;
		   /* Always change date to 8 digits - string*/
		   /* if year is entered as 2-digit / always assume 20xx */
		   if (DateValue.length == 6) {
		      DateValue = DateValue.substr(0,4) + '20' + DateValue.substr(4,2); }
		   if (DateValue.length != 8) {
		      err = 19;}
		   /* year is wrong if year = 0000 */
		   year = DateValue.substr(4,4);
		   if (year == 0) {
		      err = 20;
		   }
		   /* Validation of month*/
		   month = DateValue.substr(2,2);
		   if ((month < 1) || (month > 12)) {
		      err = 21;
		   }
		   /* Validation of day*/
		   day = DateValue.substr(0,2);
		   if (day < 1) {
		     err = 22;
		   }
		   /* Validation leap-year / february / day */
		   if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
		      leap = 1;
		   }
		   if ((month == 2) && (leap == 1) && (day > 29)) {
		      err = 23;
		   }
		   if ((month == 2) && (leap != 1) && (day > 28)) {
		      err = 24;
		   }
		   /* Validation of other months */
		   if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
		      err = 25;
		   }
		   if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
		      err = 26;
		   }
		   /* if 00 ist entered, no error, deleting the entry */
		   if ((day == 0) && (month == 0) && (year == 00)) {
		      err = 0; day = ""; month = ""; year = ""; seperator = "";
		   }
		   /* if no error, write the completed date to Input-Field (e.g. 13.12.2001) */
		   if (err == 0) {
		      DateField.value = day + seperator + month + seperator + year;
		   }
		   /* Error-message if err != 0 */
		   else {
		      alert("Fecha Inv�lida!");
		      DateField.select();
			  DateField.focus();
		   }
		}
    </script>
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{ascensoConRegistroForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
			<h:form id="formAscensoConRegistro">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Ascenso Personal con Registro
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/trabajador/Ascenso.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
										
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchAscenso"
								rendered="#{!ascensoConRegistroForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
											<f:verbatim><tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{ascensoConRegistroForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{ascensoConRegistroForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{ascensoConRegistroForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{ascensoConRegistroForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{ascensoConRegistroForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{ascensoConRegistroForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{ascensoConRegistroForm.showResultTrabajador&&
								!ascensoConRegistroForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{ascensoConRegistroForm.resultTrabajador}"                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{ascensoConRegistroForm.showAscenso}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif" />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif" />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							<f:subview
								id="viewSelectedTrabajador"
								rendered="#{ascensoConRegistroForm.selectedTrabajador&&
								ascensoConRegistroForm.showData
								}">
								<f:verbatim>
								<table class="datatable" width="100%">
									<tr>
            							<td width="20%" align="left">Trabajador:&nbsp;</td>
										<td width="80%" align="left">											
											<tr>
											</f:verbatim>
	            								<h:commandLink value="#{ascensoConRegistroForm.trabajador}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
	                            			<f:verbatim>	
	                            			</tr>
	                            			<tr>
	                            			</f:verbatim>
	                            				<h:commandLink value="#{ascensoConRegistroForm.trabajador.cargo}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
	                            			<f:verbatim>
	                            			</tr>
            								
            							</td>
									</tr>
									
									<f:subview
            							id="pregunta"
        								rendered="#{ascensoConRegistroForm.showData1}">
        								<f:verbatim>
        								<td align="left">Mostrar Cargos Vacantes</td>
                						<td align="left">
        									
        								</f:verbatim>
        									
											<h:selectOneMenu value="#{ascensoConRegistroForm.seleccion}"
					                            	rendered="#{ascensoConRegistroForm.showData1}"
					                            	valueChangeListener="#{ascensoConRegistroForm.changeSeleccion}"
					                            	onchange="this.form.submit()"
	                                            	immediate="false">
	                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
													<f:selectItem itemLabel="Todos" itemValue="T" />
													<f:selectItem itemLabel="Por Region" itemValue="R" />	                                               
	                                            </h:selectOneMenu>		
											<f:verbatim>	</td></f:verbatim>
                    	        	</f:subview>
                    	        	<f:subview
        								id="busquedaTodos"
        								rendered="#{ascensoConRegistroForm.showBusquedaTodos}">     
        								<f:verbatim>       						
										<tr>
	                						<td align="left">Posiciones Vacantes</td>
	                						<td align="left">
	                						</f:verbatim>
				                            	<h:selectOneMenu value="#{ascensoConRegistroForm.selectIdRegistroCargos}"					                            	
					                            	valueChangeListener="#{ascensoConRegistroForm.changeRegistroCargos}"
					                            	rendered="#{ascensoConRegistroForm.showRegistroCargos}"
					                            	onchange="this.form.submit()"
	                                            	immediate="false">
													<f:selectItem itemLabel="Seleccione" itemValue="0" />
	                                                <f:selectItems value="#{ascensoConRegistroForm.colRegistroCargos}" />
	                                            </h:selectOneMenu>											
											<f:verbatim></td>
										</tr>									
									<tr>
                						<td align="left">Regi�n</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText												
												rendered="#{ascensoConRegistroForm.showFields}"
												value="#{ascensoConRegistroForm.nombreRegion}"
												
												/>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Sede</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 										
												rendered="#{ascensoConRegistroForm.showFields}"
												value="#{ascensoConRegistroForm.nombreSede}"
												
												 />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Dependencia</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 									
												rendered="#{ascensoConRegistroForm.showFields}"
												value="#{ascensoConRegistroForm.nombreDependencia}"
												
												/>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Cargo</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 
												rendered="#{ascensoConRegistroForm.showFields}"
												value="#{ascensoConRegistroForm.descripcionCargo}"
												
												/>											
										</td>
									</tr>
									<f:verbatim><tr>
                						<td align="left">Grado</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText
												rendered="#{ascensoConRegistroForm.showFields}"
												value="#{ascensoConRegistroForm.grado}"
												
												 />											
										<f:verbatim></td>
									</tr>		
									
									<tr>
                						<td align="left">Sueldo/Salario B�sico</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 
												rendered="#{ascensoConRegistroForm.showFields}"
												value="#{ascensoConRegistroForm.sueldo}"
												
												>
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:outputText>	              
										<f:verbatim></td>										
									</tr>	
									</f:verbatim>
								</f:subview>
								<f:subview
									id="busquedaRegion"
        							rendered="#{ascensoConRegistroForm.showBusquedaRegion}">  								
									<f:verbatim>
						 			<tr>
                						<td>
                							Region
                						</td>
                						<td></f:verbatim>											
                                            <h:selectOneMenu value="#{ascensoConRegistroForm.idRegion}"
                                                valueChangeListener="#{ascensoConRegistroForm.changeRegion}"  
                                                 rendered="#{ascensoConRegistroForm.showRegion}"                                                 	
                                            	immediate="false"                        								                        								                                                    	                                                    	
                                            	onchange="this.form.submit()"                                                    	
												title="Region">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ascensoConRegistroForm.colRegion}" />
												
                                            </h:selectOneMenu><f:verbatim>
                                            
                                            </f:verbatim>                        						
                                            
										<f:verbatim></td>
									</tr>									
									  <tr>
                						<td>
                							Sede
                						</td>
                						<td></f:verbatim>
											
                                            <h:selectOneMenu value="#{ascensoConRegistroForm.idSede}"
                                                valueChangeListener="#{ascensoConRegistroForm.changeSede}"                                                    	
                                            	immediate="false"                        								                        								                                                    	                                                    	
                                            	onchange="this.form.submit()"
                                            	rendered="#{ascensoConRegistroForm.showSede}" 
												title="Sede">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ascensoConRegistroForm.colSede}" />
												
                                            </h:selectOneMenu><f:verbatim>
                                            
                                            </f:verbatim>                        						
                                            
										<f:verbatim></td>
									</tr>
									<tr>
                						<td>
                							Dependencia
                						</td>
                						<td></f:verbatim>
											
                                            <h:selectOneMenu value="#{ascensoConRegistroForm.idDependencia}"
                                                valueChangeListener="#{ascensoConRegistroForm.changeDependencia}"                                                    	
                                                rendered="#{ascensoConRegistroForm.showDependencia}"
                                            	immediate="false"                        								                        								                                                    	                                                    	
                                            	onchange="this.form.submit()"
												title="Dependencia">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ascensoConRegistroForm.colDependencia}" />
												
                                            </h:selectOneMenu><f:verbatim>                                                                                                           						
                                            
										</td>
									</tr>		
									<tr>
                						<td align="left">Posiciones Vacantes</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{ascensoConRegistroForm.selectIdRegistroCargos}"
				                            	rendered="#{ascensoConRegistroForm.showRegistroCargos}"
				                            	valueChangeListener="#{ascensoConRegistroForm.changeRegistroCargos}"
				                            	
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ascensoConRegistroForm.colRegistroCargos}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									</f:verbatim>							
								</f:subview>
			
									<tr>
                						<td align="left">Fecha Vigencia Movimiento</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{ascensoConRegistroForm.showFields}"
												value="#{ascensoConRegistroForm.fechaIngreso}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr>																		
									</tr>	
    									<td>
    										Pagar Retroactivo?
    									</td>
    									<td width="100%">
    										</f:verbatim>
    										<h:selectOneMenu value="#{ascensoConRegistroForm.pagarRetroactivo}"    											
    											rendered="#{ascensoConRegistroForm.showFields}"    											
    											onchange="this.form.submit()"
    											immediate="true">
                                            	<f:selectItem itemLabel="No" itemValue="N" />  
                                            	<f:selectItem itemLabel="Si" itemValue="S" />
                                            </h:selectOneMenu>
                                            	<f:verbatim>
    									</td>
    								</tr>										
                						                    				
                        			<tr>
                						<td align="left">Fecha Punto de Cuenta</td>   
                						<td align="left">             						
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{ascensoConRegistroForm.showFields}"
												value="#{ascensoConRegistroForm.fechaPuntoCuenta}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr> 
                        			<tr>
                						<td align="left">
                							Punto de Cuenta
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="15"
                    							maxlength="15"
                    							rendered="#{ascensoConRegistroForm.showFields}"
                								value="#{ascensoConRegistroForm.puntoCuenta}"                        								
                								id="PuntoCuenta"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                					<tr>
                						<td align="left">
                							Codigo Concurso
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="6"
                    							maxlength="6"
                    							rendered="#{ascensoConRegistroForm.showFields}"
                								value="#{ascensoConRegistroForm.codConcurso}"                        								
                								id="CodConcurso"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"                								      												 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr> 
                        			<tr>
                						<td>
                							Observaciones
                						</td>
                						<td></f:verbatim>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="4"       
                    							rendered="#{ascensoConRegistroForm.showFields}"             							
                    							id="observaciones"
                								value="#{ascensoConRegistroForm.observaciones}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />													
                						<f:verbatim></td>
                					</tr>				
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{ascensoConRegistroForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{ascensoConRegistro.showData}" />
    											
                                    			<h:commandButton value="Cancelar" 
		                            				image="/images/cancel.gif"
		                							action="#{ascensoConRegistroForm.abort}"
		                							immediate="true"
		                							onclick="javascript:return clickMade()"
		        	        							 />
											<f:verbatim>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview> 
							
						</td>
					</tr>
				</table>

				</h:form>

			</td>			
		</tr>
	</table>

</f:view>

</body>
</html>