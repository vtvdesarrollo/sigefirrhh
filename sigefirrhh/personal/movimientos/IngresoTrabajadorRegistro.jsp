<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{ingresoTrabajadorRegistroForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left"></div>
			</td>
			<td width="570" valign="top">
			<h:form id="formIngresoTrabajadorLefp">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Ingreso no Sujeto a LEFP con Registro
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/IngresoTrabajadorRegistro.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
										<h:commandLink value="Ir a Datos Personales"                            						
                        						styleClass="listitem"
                        						action="#{ingresoTrabajadorRegistroForm.redirectPersonal}"
                        						rendered="#{ingresoTrabajadorRegistroForm.error}">	                            						
                        				</h:commandLink>
									</td>
								</tr>
							<f:subview 
								id="searchIngresoTrabajadorRegistro"
								rendered="#{!ingresoTrabajadorRegistroForm.showData1}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Ingresar
												</td>
											</tr>
										</table>										
										<table width="100%" class="datatable">
												
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{ingresoTrabajadorRegistroForm.findPersonalCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{ingresoTrabajadorRegistroForm.findPersonalByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>												
										</table>
									</td>
								</tr>
							</table>
							</f:verbatim>
							</f:subview>
							<f:subview
								id="viewSelectedPersonal"
								rendered="#{ingresoTrabajadorRegistroForm.showData1
								}">
								<f:verbatim>
								<table class="toptable" width="100%">
								<tr>
								<td>
								<table class="datatable" width="100%">
									<tr>
            							<td width="20%" align="left">Trabajador:&nbsp;</td>
										<td width="80%" align="left">
											</f:verbatim>
	            								<h:commandLink value="#{ingresoTrabajadorRegistroForm.personal}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
            								<f:verbatim>
            							</td>
									</tr>
									<tr>
                						<td align="left">Tipo de Personal</td>
                						<td align="left">
                    						</f:verbatim>
    			                            	<h:selectOneMenu value="#{ingresoTrabajadorRegistroForm.selectIdTipoPersonal}"
    				                            	valueChangeListener="#{ingresoTrabajadorRegistroForm.changeCausaPersonalForTipoPersonal}"
    				                            	onchange="this.form.submit()"
                                                	immediate="false">
    												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    <f:selectItems value="#{ingresoTrabajadorRegistroForm.colTipoPersonal}" />
                                                </h:selectOneMenu>
    											
    										<f:verbatim>
										</td>
									</tr>
									<tr>
										<td colspan="2" align="left" valign="middle">
        									</f:verbatim>
        									<f:subview
                								id="egresado"
                								rendered="#{ingresoTrabajadorRegistroForm.egresado
                								}">
                								<f:verbatim>
                									El trabajador esta egresado de otro tipo de personal, �Desea continuar?
                								</f:verbatim>
                									
            											<h:commandButton image="/images/yes.gif"
            												action="#{ingresoTrabajadorRegistroForm.continuarConEgresado}"
            												onclick="javascript:return clickMade()"
            												 />
                        	        		</f:subview>
                	        				<f:verbatim>
                	        			</td>
									</tr>
									<tr>
                						<td align="left">Movimiento</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{ingresoTrabajadorRegistroForm.selectIdCausaPersonal}"
				                            	rendered="#{ingresoTrabajadorRegistroForm.showCausaPersonal}"				                            	
				                            	valueChangeListener="#{ingresoTrabajadorRegistroForm.changeCausaPersonal}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorRegistroForm.colCausaPersonal}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Indique Paso</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="3"
												value="#{ingresoTrabajadorRegistroForm.paso}"
												immediate="false"
												readonly="false" />											
										<f:verbatim></td>
									</tr>	
									</f:verbatim>	
									<f:subview
            							id="pregunta"
        								rendered="#{ingresoTrabajadorRegistroForm.showData1}">
        								<f:verbatim>
        								<td align="left">Mostrar Cargos Vacantes</td>
                						<td align="left">
        									
        								</f:verbatim>
        									
											<h:selectOneMenu value="#{ingresoTrabajadorRegistroForm.seleccion}"
					                            	rendered="#{ingresoTrabajadorRegistroForm.showData1}"
					                            	valueChangeListener="#{ingresoTrabajadorRegistroForm.changeSeleccion}"
					                            	onchange="this.form.submit()"
	                                            	immediate="false">
	                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
													<f:selectItem itemLabel="Todos" itemValue="T" />
													<f:selectItem itemLabel="Por Region" itemValue="R" />	                                               
	                                            </h:selectOneMenu>		
											<f:verbatim>	</td></f:verbatim>
                    	        	</f:subview>
                    	        	<f:subview
        								id="busquedaTodos"
        								rendered="#{ingresoTrabajadorRegistroForm.showBusquedaTodos}">     
        								<f:verbatim>       						
										<tr>
	                						<td align="left">Posiciones Vacantes</td>
	                						<td align="left">
	                						</f:verbatim>
				                            	<h:selectOneMenu value="#{ingresoTrabajadorRegistroForm.selectIdRegistroCargos}"					                            	
					                            	valueChangeListener="#{ingresoTrabajadorRegistroForm.changeRegistroCargos}"
					                            	rendered="#{ingresoTrabajadorRegistroForm.showRegistroCargos}"
					                            	onchange="this.form.submit()"
	                                            	immediate="false">
													<f:selectItem itemLabel="Seleccione" itemValue="0" />
	                                                <f:selectItems value="#{ingresoTrabajadorRegistroForm.colRegistroCargos}" />
	                                            </h:selectOneMenu>											
											<f:verbatim></td>
										</tr>									
									<tr>
                						<td align="left">Regi�n</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText												
												rendered="#{ingresoTrabajadorRegistroForm.showFields}"
												value="#{ingresoTrabajadorRegistroForm.nombreRegion}"
												
												/>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Sede</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 										
												rendered="#{ingresoTrabajadorRegistroForm.showFields}"
												value="#{ingresoTrabajadorRegistroForm.nombreSede}"
												
												 />											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Dependencia</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 									
												rendered="#{ingresoTrabajadorRegistroForm.showFields}"
												value="#{ingresoTrabajadorRegistroForm.nombreDependencia}"
												
												/>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Cargo</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 
												rendered="#{ingresoTrabajadorRegistroForm.showFields}"
												value="#{ingresoTrabajadorRegistroForm.descripcionCargo}"
												
												/>											
										</td>
									</tr>
									<f:verbatim><tr>
                						<td align="left">Grado</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText
												rendered="#{ingresoTrabajadorRegistroForm.showFields}"
												value="#{ingresoTrabajadorRegistroForm.grado}"
												
												 />											
										<f:verbatim></td>
									</tr>		
									
									<tr>
                						<td align="left">Sueldo/Salario B�sico</td>
                						<td align="left">
                						</f:verbatim>
											<h:outputText 
												rendered="#{ingresoTrabajadorRegistroForm.showFields}"
												value="#{ingresoTrabajadorRegistroForm.sueldo}"
												
												>
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
											</h:outputText>	              
										<f:verbatim></td>										
									</tr>	
									</f:verbatim>
								</f:subview>
								<f:subview
									id="busquedaRegion"
        							rendered="#{ingresoTrabajadorRegistroForm.showBusquedaRegion}">  								
									<f:verbatim>
						 			<tr>
                						<td>
                							Region
                						</td>
                						<td></f:verbatim>											
                                            <h:selectOneMenu value="#{ingresoTrabajadorRegistroForm.idRegion}"
                                                valueChangeListener="#{ingresoTrabajadorRegistroForm.changeRegion}"  
                                                 rendered="#{ingresoTrabajadorRegistroForm.showRegion}"                                                 	
                                            	immediate="false"                        								                        								                                                    	                                                    	
                                            	onchange="this.form.submit()"                                                    	
												title="Region">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorRegistroForm.colRegion}" />
												
                                            </h:selectOneMenu><f:verbatim>
                                            
                                            </f:verbatim>                        						
                                            
										<f:verbatim></td>
									</tr>									
									  <tr>
                						<td>
                							Sede
                						</td>
                						<td></f:verbatim>
											
                                            <h:selectOneMenu value="#{ingresoTrabajadorRegistroForm.idSede}"
                                                valueChangeListener="#{ingresoTrabajadorRegistroForm.changeSede}"                                                    	
                                            	immediate="false"                        								                        								                                                    	                                                    	
                                            	onchange="this.form.submit()"
                                            	rendered="#{ingresoTrabajadorRegistroForm.showSede}" 
												title="Sede">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorRegistroForm.colSede}" />
												
                                            </h:selectOneMenu><f:verbatim>
                                            
                                            </f:verbatim>                        						
                                            
										<f:verbatim></td>
									</tr>
									<tr>
                						<td>
                							Dependencia
                						</td>
                						<td></f:verbatim>
											
                                            <h:selectOneMenu value="#{ingresoTrabajadorRegistroForm.idDependencia}"
                                                valueChangeListener="#{ingresoTrabajadorRegistroForm.changeDependencia}"                                                    	
                                                rendered="#{ingresoTrabajadorRegistroForm.showDependencia}"
                                            	immediate="false"                        								                        								                                                    	                                                    	
                                            	onchange="this.form.submit()"
												title="Dependencia">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorRegistroForm.colDependencia}" />
												
                                            </h:selectOneMenu><f:verbatim>                                                                                                           						
                                            
										</td>
									</tr>		
									<tr>
                						<td align="left">Posiciones Vacantes</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{ingresoTrabajadorRegistroForm.selectIdRegistroCargos}"
				                            	rendered="#{ingresoTrabajadorRegistroForm.showRegistroCargos}"
				                            	valueChangeListener="#{ingresoTrabajadorRegistroForm.changeRegistroCargos}"
				                            	
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ingresoTrabajadorRegistroForm.colRegistroCargos}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									</f:verbatim>							
								</f:subview>
								<f:verbatim>
									<tr>
                						<td align="left">Fecha Ingreso</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{ingresoTrabajadorRegistroForm.showFields}"
												value="#{ingresoTrabajadorRegistroForm.fechaIngreso}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr>	
									</tr>	
    									<td>
    										Pagar Retroactivo?
    									</td>
    									<td width="100%">
    										</f:verbatim>
    										<h:selectOneMenu value="#{ingresoTrabajadorRegistroForm.pagarRetroactivo}"    											
    											rendered="#{ingresoTrabajadorRegistroForm.showFields}"    											
    											onchange="this.form.submit()"
    											immediate="true">
                                            	<f:selectItem itemLabel="No" itemValue="N" />  
                                            	<f:selectItem itemLabel="Si" itemValue="S" />
                                            </h:selectOneMenu>
                                            	<f:verbatim>
    									</td>
    								</tr>									 
                        			<tr>
                						<td align="left">Fecha Punto de Cuenta</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												rendered="#{ingresoTrabajadorRegistroForm.showFields}"
												value="#{ingresoTrabajadorRegistroForm.fechaPuntoCuenta}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr> 
                        			<tr>
                						<td align="left">
                							Punto de Cuenta
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="15"
                    							maxlength="15"
                    							rendered="#{ingresoTrabajadorRegistroForm.showFields}"
                								value="#{ingresoTrabajadorRegistroForm.puntoCuenta}"                        								
                								id="PuntoCuenta"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                					<tr>
                						<td align="left">
                							Concurso
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="6"
                    							maxlength="6"
                    							rendered="#{ingresoTrabajadorRegistroForm.showFields}"
                								value="#{ingresoTrabajadorRegistroForm.codConcurso}"                        								
                								id="CodConcurso"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                        			<tr>
                						<td>
                							Observaciones
                						</td>
                						<td></f:verbatim>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="4"     
                    							rendered="#{ingresoTrabajadorRegistroForm.showFields}"               							
                    							id="observaciones"
                								value="#{ingresoTrabajadorRegistroForm.observaciones}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />													
                						<f:verbatim></td>
                					</tr>		
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{ingresoTrabajadorRegistroForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{ingresoTrabajadorRegistro.showFields}" />
    											<h:commandButton value="Cancelar" 
				                    				image="/images/cancel.gif"
				        							action="#{ingresoTrabajadorRegistroForm.abort}"
				        							immediate="true"
				        							onclick="javascript:return clickMade()"
					        							 />
                                    			
											<f:verbatim>
										</td>
									</tr>								
								</table>
								</td>
								</tr>
								</table>
							</f:verbatim>
							</f:subview>	
																									
						</td>
					</tr>
				</table>
				</h:form>
			</td>			
		</tr>
	</table>
</f:view>
</body>
</html>