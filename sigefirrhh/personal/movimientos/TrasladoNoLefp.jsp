<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{trasladoNoLefpForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			
			<td width="570" valign="top">
<h:form id="formRegistroCargos">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Traslado No Sujeto a LEFP
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/TrasladoNoLefp.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
                        					image="/images/close.gif"
                        					action="go_cancelOption"
                        					immediate="true"                        					
                        					onclick="javascript:return clickMade()"
                	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="Traslado"
								>
								<f:verbatim><tr>
									<td>
										<table width="100%" class="datatable"></f:verbatim>
                        					<f:verbatim>
                        					<tr>
                        						<td>
                        							Registro
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{trasladoNoLefpForm.idRegistro}"
	                                                    valueChangeListener="#{trasladoNoLefpForm.changeRegistro}"                                                    		                                                    
                                                    	immediate="false"                    								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"
														title="Registro">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoNoLefpForm.colRegistro}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Posici�n
                        						</td>
                        						<td>
													
                                                    </f:verbatim>
                                                  		<h:inputText size="6"															
															value="#{trasladoNoLefpForm.codigoNomina}"															
															rendered="#{trasladoNoLefpForm.showPosicion}"
															immediate="false"																											
			                        						/>		
			                        					<h:commandButton value="Buscar" 
				                            				image="/images/find.gif"
				                            				rendered="#{trasladoNoLefpForm.showPosicion}"
				                							action="#{trasladoNoLefpForm.buscarRegistroCargo}"				                							
				                							onclick="javascript:return clickMade()" />								
                                                                     						
                                                   
												<f:verbatim></td>
											</tr>
											<tr>
                        						<!--td>
                        							C�dula
                        						</td-->
                        						<td>
													
                                                    </f:verbatim>
                                                  		<!--h:inputText size="12"															
															value="#{trasladoNoLefpForm.cedula}"															
															rendered="#{trasladoNoLefpForm.showPosicion}"
															immediate="false"																											
			                        						/-->		
			                        					<!--h:commandButton value="Buscar" 
				                            				image="/images/find.gif"
				                            				rendered="#{trasladoNoLefpForm.showPosicion}"
				                							action="#{trasladoNoLefpForm.buscarRegistroCargo}"				                							
				                							onclick="javascript:return clickMade()"
				        	        							 /-->								
                                                                     						
                                                   
												<f:verbatim></td>
											</tr>
											
											 </f:verbatim> 
											<f:subview id="Trabajador" rendered="#{trasladoNoLefpForm.showTrabajador}">
												<f:verbatim>
													<tr>
				                						<td align="left">Trabajador</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 												
																value="#{trasladoNoLefpForm.registroCargos.trabajador}" />											
														<f:verbatim></td>
													</tr>
													<tr>
				                						<td align="left">Regi�n</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 												
																value="#{trasladoNoLefpForm.registroCargos.trabajador.dependencia.region}" />											
														<f:verbatim></td>
													</tr>													
													
													<tr>
				                						<td align="left">Dependencia</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoNoLefpForm.registroCargos.trabajador.dependencia}"/>											
														<f:verbatim></td>
													</tr>
													<tr>
				                						<td align="left">Cargo</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoNoLefpForm.registroCargos.trabajador.cargo}"/>											
														</td>
													</tr>
													<f:verbatim><tr>
				                						<td align="left">Grado</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoNoLefpForm.registroCargos.trabajador.cargo.grado}" />											
														<f:verbatim></td>
													</tr>		
													<tr>
				                						<td align="left">Paso</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoNoLefpForm.registroCargos.trabajador.paso}"/>											
														<f:verbatim></td>
													</tr>		
													<tr>
				                						<td align="left">Sueldo B�sico</td>
				                						<td align="left">
				                						</f:verbatim>
															<h:outputText 
																value="#{trasladoNoLefpForm.registroCargos.trabajador.sueldoBasico}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
															</h:outputText>	              
														<f:verbatim></td>										
													</tr>
													</f:verbatim>
												</f:subview>
												<f:subview id="NuevaDependencia" rendered="#{trasladoNoLefpForm.showNuevaDependencia}">
													<f:verbatim>								
												<tr>
			                						<td align="left">Fecha Vigencia Movimiento</td>
			                						<td align="left">
			                						</f:verbatim>
														<h:inputText size="18"
															
															value="#{trasladoNoLefpForm.fechaMovimiento}"
															immediate="false"
															onkeypress="return keyEnterCheck(event, this)"
			                        						onblur="javascript:check_date(this)"
			                        						>																																			                         						
				                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
			                                                 pattern="dd-MM-yyyy" />
			                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
			                        						                				
													<f:verbatim></td>										
												</tr>													
												 <tr>
                        						<td>
                        							Region
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{trasladoNoLefpForm.idRegion}"
	                                                    valueChangeListener="#{trasladoNoLefpForm.changeRegion}"                                                    	
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"                                                    	
														title="Region">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoNoLefpForm.colRegion}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>									
  											  
											<tr>
                        						<td>
                        							Dependencia
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{trasladoNoLefpForm.idDependencia}"
	                                                    valueChangeListener="#{trasladoNoLefpForm.changeDependencia}"                                                    	
	                                                    rendered="#{trasladoNoLefpForm.showDependencia}"
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"
														title="Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trasladoNoLefpForm.colDependencia}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											
											
											<tr>
		                						<td>
		                							Observaciones
		                						</td>
		                						<td></f:verbatim>
					                        		<h:inputTextarea
		                    							cols="50"
		                    							rows="4"                    							
		                    							id="observaciones"
		                								value="#{trasladoNoLefpForm.observaciones}"
		                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
		                								required="false" />													
		                						<f:verbatim></td>
		                					</tr>		
											<tr>
												<td></td>
												<td align="left">
													</f:verbatim>
		    											<h:commandButton image="/images/run.gif"
		    												action="#{trasladoNoLefpForm.ejecutar}"
		    												onclick="javascript:return clickMade()"
		    												rendered="#{ascensoNoLefp.showData}" />
		    											
		                                    			<h:commandButton value="Cancelar" 
				                            				image="/images/cancel.gif"
				                							action="#{trasladoNoLefpForm.abort}"
				                							immediate="true"
				                							onclick="javascript:return clickMade()"
				        	        							 />
													<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											</f:subview>
											<f:verbatim>
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>									

</h:form>
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>