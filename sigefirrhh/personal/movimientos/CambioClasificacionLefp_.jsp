<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
        function scrollToCoordinates() {
        	if (document.forms['viewDataRegistroCargos:cambioClasificacionLefpForm']) {
        		window.scrollTo(
        			<%= request.getParameter("viewDataRegistroCargos:cambioClasificacionLefpForm:scrollx") %>,
        			<%= request.getParameter("viewDataRegistroCargos:cambioClasificacionLefpForm:scrolly") %>);
        		document.forms['viewDataRegistroCargos:cambioClasificacionLefpForm'].elements['viewDataRegistroCargos:cambioClasificacionLefpForm:scrollx'].value = 0;
        		document.forms['viewDataRegistroCargos:cambioClasificacionLefpForm'].elements['viewDataRegistroCargos:cambioClasificacionLefpForm:scrolly'].value = 0;
        	}
    	}
    	function saveScrollCoordinates() {
        	if (document.forms['viewDataRegistroCargos:cambioClasificacionLefpForm']) {
        		document.forms['viewDataRegistroCargos:cambioClasificacionLefpForm'].elements['viewDataRegistroCargos:cambioClasificacionLefpForm:scrollx'].value = (document.all)?document.documentElement.scrollLeft:window.pageXOffset;
        		document.forms['viewDataRegistroCargos:cambioClasificacionLefpForm'].elements['viewDataRegistroCargos:cambioClasificacionLefpForm:scrolly'].value = (document.all)?document.documentElement.scrollTop:window.pageYOffset;
        	}
    	}
    	
    	function keyFloatCheck(eventObj, obj)
		{
			var keyCode
		
			// Check For Browser Type
			if (document.all){ 
				keyCode=eventObj.keyCode
			}
			else{
				keyCode=eventObj.which
			}
		
			var str=obj.value
		
			if(keyCode==13){ 
				return false
			}
			
			if(keyCode==44){ 
				if (str.indexOf(",")>0){
					return false
				}
			}
		
			if((keyCode<48 || keyCode >58)   &&   (keyCode != 44)){ 
				return false
			}
		
			return true
		}
		function keyIntegerCheck(eventObj, obj)
		{
			var keyCode
		
			if (document.all){ 
				keyCode=eventObj.keyCode
			}
			else{
				keyCode=eventObj.which
			}
		
			if(keyCode==13){ 
				return false
			}
			if((keyCode<48 || keyCode >58)){
				return false
			}
		
			return true
		}
		function keyEnterCheck(eventObj, obj)
		{
			var keyCode
		
			if (document.all){ 
				keyCode=eventObj.keyCode
			}
			else{
				keyCode=eventObj.which
			}
		
			if(keyCode==13){ 
				return false
			}		
			return true
		}
		
		var double_delay = null;
		var delay_value = 1000; // 1 second.
		var click_count=0;
		
		function clickMade() {
		
		if (click_count>0) {
		    // it is a double click
			clearTimeout(double_delay);
			double_delay=null;
			return false
		  } else {
		    // it is a single click
			click_count++;
			double_delay = setTimeout("click_count=0",delay_value);
			return true

		  }

		}
		
		function firstFocus(){
			var paso = "false";			
			for (i = 0; i < document.forms.length; i++){
				for (j = 0; j < document.forms[i].elements.length; j++){					
					if (document.forms[i].elements[j].type =="text" || document.forms[i].elements[j].type =="select-one"){					
						paso = "true";
						document.forms[i].elements[j].focus();					
						break;
					}
				}				
				if (paso =="true"){
					break;
				}
			}
			
		}
		
		function deleteZero(eventObj, obj)
		{
		
			var keyCode
		
			// Check For Browser Type
			if (document.all){ 
				keyCode=eventObj.keyCode
			}
			else{
				keyCode=eventObj.which
			}					
		
			var varField = obj;
			var varValue = "";
			varValue = varField.value;
			if (varValue.length == 1 && keyCode == 0) {	
				varField.value = "";
			}
			
			return true
		}
		
    </script>
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{cambioClasificacionLefpForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formRegistroCargos">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Cambio Clasificacion bajo Lefp
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/registroCargos/RegistroCargos.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
                        					image="/images/close.gif"
                        					action="go_cancelOption"
                        					immediate="true"                        					
                        					onclick="javascript:return clickMade()"
                	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="Clasificacion"
								>
								<f:verbatim><tr>
									<td>
										<table width="100%" class="datatable"></f:verbatim>
                        					<f:verbatim>
                        					<tr>
                        						<td>
                        							Registro
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{cambioClasificacionLefpForm.idRegistro}"
	                                                    valueChangeListener="#{cambioClasificacionLefpForm.changeRegistro}"                                                    		                                                    
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"
														title="Registro">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cambioClasificacionLefpForm.colRegistro}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Posici�n
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{cambioClasificacionLefpForm.idRegistroCargos}"
	                                                    valueChangeListener="#{cambioClasificacionLefpForm.changeRegistroCargos}"                                                    	
	                                                    rendered="#{cambioClasificacionLefpForm.showRegistroCargos}"
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"
														title="Posicion">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cambioClasificacionLefpForm.colRegistroCargos}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Manual Cargo
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{cambioClasificacionLefpForm.idManualCargo}"
	                                                    valueChangeListener="#{cambioClasificacionLefpForm.changeManualCargo}"                                                    	
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"
														title="ManualCargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cambioClasificacionLefpForm.colManualCargo}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{cambioClasificacionLefpForm.idCargo}"
	                                                    valueChangeListener="#{cambioClasificacionLefpForm.changeCargo}"                                                    	
	                                                    rendered="#{cambioClasificacionLefpForm.showCargo}"
                                                    	immediate="false"                        								                        								                                                    	                                                    	
                                                    	onchange="this.form.submit()"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cambioClasificacionLefpForm.colCargo}" />
														
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											<tr>
		                						<td>
		                							Observaciones
		                						</td>
		                						<td></f:verbatim>
					                        		<h:inputTextarea
		                    							cols="50"
		                    							rows="4"                    							
		                    							id="observaciones"
		                								value="#{cambioClasificacionLefpForm.observaciones}"
		                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
		                								required="false" />													
		                						<f:verbatim></td>
                							</tr>		
											<tr>
												<td></td>
												<td align="left">
													</f:verbatim>
		    											<h:commandButton image="/images/run.gif"
		    												action="#{cambioClasificacionLefpForm.ejecutar}"
		    												onclick="javascript:return clickMade()"
		    												rendered="#{ascensoLefp.showData}" />
		    											
		                                    			<h:commandButton value="Cancelar" 
				                            				image="/images/cancel.gif"
				                							action="#{cambioClasificacionLefpForm.abort}"
				                							immediate="true"
				                							onclick="javascript:return clickMade()"
				        	        							 />
													<f:verbatim>
												</td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>									

</h:form>
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>