<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{movimientoTrabajadorSinRegistroForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left"></div>
			</td>
			<td width="570" valign="top">
			<h:form id="formMovimientoTrabajadorSinRegistro">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Cambio Tipo Personal Sin Registro
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/MovimientoTrabajadorSinRegistro.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
										<h:commandLink value="Ir a Datos Personales"                            						
                        						styleClass="listitem"
                        						action="#{movimientoTrabajadorSinRegistroForm.redirectPersonal}"
                        						rendered="#{movimientoTrabajadorSinRegistroForm.error}">	                            						
                        				</h:commandLink>
									</td>
								</tr>
							<f:subview 
								id="searchMovimientoTrabajadorSinRegistro" rendered="#{!movimientoTrabajadorSinRegistroForm.showData1}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
											<f:verbatim><tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						
                        						
                        						
                						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{movimientoTrabajadorSinRegistroForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{movimientoTrabajadorSinRegistroForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{movimientoTrabajadorSinRegistroForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{movimientoTrabajadorSinRegistroForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{movimientoTrabajadorSinRegistroForm.findTrabajadorCodigoNomina}"
														onfocus="return deleteZero(event, this)"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{movimientoTrabajadorSinRegistroForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							
							
							<f:subview
								id="viewSelectedPersonal"
								rendered="#{movimientoTrabajadorSinRegistroForm.showData1}">
								<f:verbatim>
								<table class="toptable" width="100%">
								<tr>
								<td>
								<table class="datatable" width="100%">
									<tr>
            							<td width="25%" align="left">Trabajador:&nbsp;</td>
										<td width="75%" align="left">
											</f:verbatim>
	            								<h:commandLink value="#{movimientoTrabajadorSinRegistroForm.trabajador}"                            						
	                            						styleClass="listitem">	                            						
	                            				</h:commandLink>
            								<f:verbatim>
            							</td>
									</tr>
									<tr>
                						<td align="left">Tipo de Personal</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{movimientoTrabajadorSinRegistroForm.selectIdTipoPersonal}"
				                            	valueChangeListener="#{movimientoTrabajadorSinRegistroForm.changeTipoPersonal}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorSinRegistroForm.colTipoPersonal}" />
                                            </h:selectOneMenu>
											
										<f:verbatim></td>
									</tr>
									<tr>
										<td colspan="2" align="left" valign="middle">
        									</f:verbatim>
        									<f:subview
                								id="egresado"
                								rendered="#{movimientoTrabajadorSinRegistroForm.egresado
                								}">
                								<f:verbatim>
                									El trabajador esta egresado de otro tipo de personal, �Desea continuar?
                								</f:verbatim>
                									
            											<h:commandButton image="/images/yes.gif"
            												action="#{movimientoTrabajadorSinRegistroForm.continuarConEgresado}"
            												onclick="javascript:return clickMade()"
            												 />
                        	        		</f:subview>
                        	        		<f:verbatim>
                        	        	</td>
									</tr>
									        
									<tr>
                        				<td align="left">Movimiento</td>
                						<td align="left">
                    						</f:verbatim>
    			                            	<h:selectOneMenu value="#{movimientoTrabajadorSinRegistroForm.selectCausaPersonalMovimiento}"
                                                	immediate="false"
                                                	rendered="#{movimientoTrabajadorSinRegistroForm.showCausaPersonalMovimiento}"	
                                                	>
    												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    <f:selectItems value="#{movimientoTrabajadorSinRegistroForm.colCausaPersonalMovimiento}" />
                                                </h:selectOneMenu>
    										<f:verbatim>
										</td>
									</tr>
									
									<tr>
                						<td align="left">Regi�n</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{movimientoTrabajadorSinRegistroForm.selectIdRegion}"
				                            	rendered="#{movimientoTrabajadorSinRegistroForm.showRegion}"
				                            	valueChangeListener="#{movimientoTrabajadorSinRegistroForm.changeRegion}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorSinRegistroForm.colRegion}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Sede</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{movimientoTrabajadorSinRegistroForm.selectIdSede}"
				                            	rendered="#{movimientoTrabajadorSinRegistroForm.showSede}"
				                            	valueChangeListener="#{movimientoTrabajadorSinRegistroForm.changeSede}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorSinRegistroForm.colSede}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>	
									<tr>
                						<td align="left">Dependencia</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{movimientoTrabajadorSinRegistroForm.selectIdDependencia}"
				                            	rendered="#{movimientoTrabajadorSinRegistroForm.showDependencia}"
				                            	
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorSinRegistroForm.colDependencia}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Clasificador</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{movimientoTrabajadorSinRegistroForm.selectIdManualCargo}"
				                            	rendered="#{movimientoTrabajadorSinRegistroForm.showManualCargo}"
				                            	valueChangeListener="#{movimientoTrabajadorSinRegistroForm.changeManualCargo}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorSinRegistroForm.colManualCargo}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td align="left">Cargo</td>
                						<td align="left">
                						</f:verbatim>
			                            	<h:selectOneMenu value="#{movimientoTrabajadorSinRegistroForm.selectIdCargo}"
				                            	rendered="#{movimientoTrabajadorSinRegistroForm.showCargo}"
				                            	valueChangeListener="#{movimientoTrabajadorSinRegistroForm.changeCargo}"
				                            	onchange="this.form.submit()"
                                            	immediate="false">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{movimientoTrabajadorSinRegistroForm.colCargo}" />
                                            </h:selectOneMenu>											
										<f:verbatim></td>
									</tr>
									<tr>
                						<td>
                							Sueldo/Salario Basico
                						</td>
                						<td></f:verbatim>
			                        		<h:inputText
                    							size="12"
                    							id="Sueldo"
                    							maxlength="12"
                    							
                								value="#{movimientoTrabajadorSinRegistroForm.sueldo}"
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								onkeypress="return keyFloatCheck(event, this)"
                																						 style="text-align:right"                         								 >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                							</h:inputText>
                        								
                        			<f:verbatim></td>
                        					</tr>
									
	                					</tr>	
	    									<td>
	    										Pagar Retroactivo?
	    									</td>
	    									<td width="100%">
	    										</f:verbatim>
	    										<h:selectOneMenu value="#{movimientoTrabajadorSinRegistroForm.pagarRetroactivo}"    											
	    											   											
	    											onchange="this.form.submit()"
	    											immediate="true">
	                                            	<f:selectItem itemLabel="No" itemValue="N" />  
	                                            	<f:selectItem itemLabel="Si" itemValue="S" />
	                                            </h:selectOneMenu>
	                                            	<f:verbatim>
	    									</td>
	    								</tr>	 
	    								</tr>	
	    									<td>
	    										Tiene Continuidad?
	    									</td>
	    									<td width="100%">
	    										</f:verbatim>
	    										<h:selectOneMenu value="#{movimientoTrabajadorSinRegistroForm.tieneContinuidad}"    											
	    											
	    											onchange="this.form.submit()"
	    											immediate="true">
	                                            	<f:selectItem itemLabel="No" itemValue="N" />  
	                                            	<f:selectItem itemLabel="Si" itemValue="S" />
	                                            </h:selectOneMenu>
	                                            	<f:verbatim>
	    									</td>
	    								</tr>	 
                        					<tr>
                						<td align="left">
                							C�digo N�mina
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="8"
                    							maxlength="8"
                    							
                								value="#{movimientoTrabajadorSinRegistroForm.codigoNomina}"                        								
                								id="CodigoNomina"
                								 
                								readonly="true"
                								onchange="javascript:this.value=this.value.toUpperCase()"                								          												 >
                							</h:inputText>
                						<f:verbatim></td>
                        					</tr> 
                        					<tr>
                						<td align="left">
                							Horas
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
                    							size="3"
                    							maxlength="2"
                    							
                								value="#{movimientoTrabajadorSinRegistroForm.horas}"                        								
                								id="Horas"
                								             								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                						        onkeypress="return keyEnterCheck(event, this)"                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                        					</tr> 
									<tr>
                						<td align="left">Fecha Vigencia</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"
												
												value="#{movimientoTrabajadorSinRegistroForm.fechaIngreso}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr>
									<tr>
                						<td align="left">Fecha Punto de Cuenta</td>
                						<td align="left">
                						</f:verbatim>
											<h:inputText size="18"		
																						
												value="#{movimientoTrabajadorSinRegistroForm.fechaPuntoCuenta}"
												immediate="false"
												onkeypress="return keyEnterCheck(event, this)"
                        						onblur="javascript:check_date(this)"
                        						>																																			                         						
	                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                                                 pattern="dd-MM-yyyy" />
                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						                				
										<f:verbatim></td>										
									</tr> 
                        			<tr>
                						<td align="left">
                							Punto de Cuenta
                						</td>
                						<td align="left"></f:verbatim>
			                        		<h:inputText
			                        			
                    							size="15"
                    							maxlength="15"                    							
                								value="#{movimientoTrabajadorSinRegistroForm.puntoCuenta}"                        								
                								id="PuntoCuenta"                								
                								onchange="javascript:this.value=this.value.toUpperCase()"
                								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                																						 >
                							</h:inputText>
                						<f:verbatim></td>
                					</tr>  
                					<tr>
                						<td>
                							Observaciones
                						</td>
                						<td></f:verbatim>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="4"     
                    							             							
                    							id="observaciones"
                								value="#{movimientoTrabajadorSinRegistroForm.observaciones}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								/>													
                					<f:verbatim></td>
                					</tr>		
									<tr>
										<td></td>
										<td align="left">
											</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{movimientoTrabajadorSinRegistroForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{movimientoTrabajadorSinRegistro.showFields}" />
    											<h:commandButton value="Cancelar" 
                                    				image="/images/cancel.gif"
                        							action="#{movimientoTrabajadorSinRegistroForm.abort}"
                        							immediate="true"
                        							onclick="javascript:return clickMade()"
                	        							 />
                                    			
											<f:verbatim>
										</td>
									</tr>								
								</table>
								</td>
								</tr>
								</table>
							</f:verbatim>
							</f:subview>	
																									
						</td>
					</tr>
				</table>
				</h:form>
			</td>			
		</tr>
	</table>
</f:view>
</body>
</html>