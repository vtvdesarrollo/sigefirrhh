<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{remesaMovimientoSitpForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formRemesaMovimientoSitp">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Asociar Movimientos
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/movimientos/RemesaMovimientoSitp.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{remesaMovimientoSitpForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!remesaMovimientoSitpForm.editing&&!remesaMovimientoSitpForm.deleting&&remesaMovimientoSitpForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchRemesaMovimientoSitp"
								rendered="#{!remesaMovimientoSitpForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Remesa
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{remesaMovimientoSitpForm.findSelectRemesa}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{remesaMovimientoSitpForm.findColRemesa}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{remesaMovimientoSitpForm.findRemesaMovimientoSitpByRemesa}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultRemesaMovimientoSitpByRemesa"
								rendered="#{remesaMovimientoSitpForm.showRemesaMovimientoSitpByRemesa&&
								!remesaMovimientoSitpForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{remesaMovimientoSitpForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                                <h:column>
	                                                <h:outputText value="#{result}"/>
                                                </h:column>
                                                <h:column>                                                	
                                    					<h:commandLink 
                                    						action="#{remesaMovimientoSitpForm.deleteOk}"
                                    						styleClass="listitem">
                                    						<h:graphicImage url="/images/delete.gif"/>
                                    						<f:param name="idMovimientoSitp" value="#{result.idMovimientoSitp}" />
                                    					</h:commandLink>
                                    				
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif" />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif" />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif" />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif" />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif" />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataRemesaMovimientoSitp"
								rendered="#{remesaMovimientoSitpForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!remesaMovimientoSitpForm.editing&&!remesaMovimientoSitpForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{remesaMovimientoSitpForm.editing&&!remesaMovimientoSitpForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{remesaMovimientoSitpForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{remesaMovimientoSitpForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="remesaMovimientoSitpForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{remesaMovimientoSitpForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!remesaMovimientoSitpForm.editing&&!remesaMovimientoSitpForm.deleting&&remesaMovimientoSitpForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{remesaMovimientoSitpForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!remesaMovimientoSitpForm.editing&&!remesaMovimientoSitpForm.deleting&&remesaMovimientoSitpForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{remesaMovimientoSitpForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{remesaMovimientoSitpForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{remesaMovimientoSitpForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{remesaMovimientoSitpForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{remesaMovimientoSitpForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{remesaMovimientoSitpForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Remesa
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{remesaMovimientoSitpForm.selectRemesa}"
														id="remesa"
                                                    	disabled="#{!remesaMovimientoSitpForm.editing}"
                        								onchange="this.form.submit()"                        		
                        								immediate="false"                                                    	
														title="Remesa">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{remesaMovimientoSitpForm.colRemesa}" />
														
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													
                                                    <h:message for="remesa" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Movimiento
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{remesaMovimientoSitpForm.selectMovimientoSitp}"
														id="movimientoSitp"
														onchange="this.form.submit()" 
                                                    	disabled="#{!remesaMovimientoSitpForm.editing}"
                        								immediate="false"
                                                    	required="true"
														title="Movimiento">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{remesaMovimientoSitpForm.colMovimientoSitp}" />
														
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="movimientoSitp" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{remesaMovimientoSitpForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!remesaMovimientoSitpForm.editing&&!remesaMovimientoSitpForm.deleting&&remesaMovimientoSitpForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{remesaMovimientoSitpForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!remesaMovimientoSitpForm.editing&&!remesaMovimientoSitpForm.deleting&&remesaMovimientoSitpForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{remesaMovimientoSitpForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{remesaMovimientoSitpForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{remesaMovimientoSitpForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{remesaMovimientoSitpForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{remesaMovimientoSitpForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{remesaMovimientoSitpForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>