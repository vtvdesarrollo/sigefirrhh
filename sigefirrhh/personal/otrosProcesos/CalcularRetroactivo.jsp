<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formCalcularRetroactivo'].elements['formCalcularRetroactivo:reportId'].value;
			var name = 
				document.forms['formCalcularRetroactivo'].elements['formCalcularRetroactivo:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}    	
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{calcularRetroactivoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formCalcularRetroactivo">
				     <h:inputHidden id="reportId" value="#{calcularRetroactivoForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{calcularRetroactivoForm.reportName}" />
				    
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Calcular Retroactivos
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/otrosProcesos/CalcularRetroactivo.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{calcularRetroactivoForm.selectTipoPersonal}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{calcularRetroactivoForm.changeTipoPersonal}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{calcularRetroactivoForm.listTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{calcularRetroactivoForm.selectConceptoTipoPersonal}"
	    										rendered="#{calcularRetroactivoForm.showConcepto}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{calcularRetroactivoForm.changeConceptoTipoPersonal}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{calcularRetroactivoForm.listConceptoTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    								<tr>
    									<td>
    										Frecuencia
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{calcularRetroactivoForm.selectFrecuenciaTipoPersonal}"
	    										rendered="#{calcularRetroactivoForm.showConcepto}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{calcularRetroactivoForm.changeFrecuenciaTipoPersonal}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{calcularRetroactivoForm.listFrecuenciaTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>								
    								<tr>
    									<td width="20%">
    										Fecha Vigencia
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{calcularRetroactivoForm.show}"
    											size="10"
    											id="Desde"
    											maxlength="10"
    											onblur="javascript:check_date(this)"
    											value="#{calcularRetroactivoForm.fechaVigencia}"
    											required="false" > 
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                            	pattern="dd-MM-yyyy" />   										    											
                	                        </h:inputText>
    									</td>
									</tr>									
																						
									<tr>
    									<td>
    										D�as a calcular retroactivo
    									</td>
    									<td width="100%">
    									<h:inputText
											size="6"
											id="DiasRetroactivo"
											maxlength="4"
											value="#{calcularRetroactivoForm.dias}"
											rendered="#{calcularRetroactivoForm.show}"
											onkeypress="return keyIntegerCheck(event, this)"
											required="false" />   
    									</td>
    								</tr>	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										rendered="#{calcularRetroactivoForm.show}"
    											action="#{calcularRetroactivoForm.generate}"
    											/>
    									<td>
    								</tr>    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>