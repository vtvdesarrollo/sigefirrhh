<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
		<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formProcesoAumentoPorTabulador'].elements['formProcesoAumentoPorTabulador:reportId'].value;
			var name = 
				document.forms['formProcesoAumentoPorTabulador'].elements['formProcesoAumentoPorTabulador:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    	</script>
    	
    	<jsp:include page="/inc/functions.jsp" />
    	
</head>

<body>
<f:view>
	<x:saveState value="#{procesoAumentoPorTabuladorForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formProcesoAumentoPorTabulador">
				
				    <h:inputHidden id="reportId" value="#{procesoAumentoPorTabuladorForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{procesoAumentoPorTabuladorForm.reportName}" />
				    			
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Aplicar Nuevo Tabulador
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/otrosProcesos/ProcesoAumentoPorTabulador.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorTabuladorForm.selectTipoPersonal}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{procesoAumentoPorTabuladorForm.changeTipoPersonal}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{procesoAumentoPorTabuladorForm.listTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Escala y Tabulador Actual
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorTabuladorForm.selectTabuladorActual}"

	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{procesoAumentoPorTabuladorForm.changeTabuladorActual}"
                                            	
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{procesoAumentoPorTabuladorForm.listTabuladorActual}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    								<tr>
    									<td>
    										Escala y Tabulador Nuevo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorTabuladorForm.selectTabuladorNuevo}"

	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{procesoAumentoPorTabuladorForm.changeTabuladorNuevo}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{procesoAumentoPorTabuladorForm.listTabuladorNuevo}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>						
    								<tr>
    									<td>
    										Condici�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorTabuladorForm.mantenerPaso}"
	    										rendered="#{procesoAumentoPorTabuladorForm.showCondicionQuincenal}"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Se Ajusta Compensaci�n" itemValue="S" />
                                            	<f:selectItem itemLabel="Se Mantiene el Paso" itemValue="P" />
                                            	<f:selectItem itemLabel="Se Ajusta Diferencia" itemValue="N" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Se mantiene el paso?
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorTabuladorForm.mantenerPaso}"
	    										rendered="#{procesoAumentoPorTabuladorForm.showCondicionSemanal}"
                                            	immediate="false">                           
                                            	<f:selectItem itemLabel="No" itemValue="N" />                 	
                                            	<f:selectItem itemLabel="Si" itemValue="S" />                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
									<tr>
    									<td>
    										Proceso
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorTabuladorForm.selectProceso}"
	    										rendered="#{procesoAumentoPorTabuladorForm.show}"
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Prueba" itemValue="1" />
                                            	<f:selectItem itemLabel="Definitivo" itemValue="2" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	    									    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										rendered="#{procesoAumentoPorTabuladorForm.show}"
    											action="#{procesoAumentoPorTabuladorForm.preGenerate}"
    											/>
    									<td>    									
    								</tr>  
    								<tr>
	    								<td colspan="2" align="center">	
		    															
											<h:outputText
	                        					value="�Esta seguro que desea realizar este proceso?"
	                        					rendered="#{procesoAumentoPorTabuladorForm.show2}" />                							                        				                				
	                        				<h:commandButton value="Si" 
	                        					image="/images/yes.gif"
	                        					action="#{procesoAumentoPorTabuladorForm.generate}"
	                        					onclick="javascript:return clickMade()"
	                        					rendered="#{procesoAumentoPorTabuladorForm.show2}" />                        					                        			                        				
	                        				<h:commandButton value="Cancelar" 
	                        					image="/images/cancel.gif"
	                        					action="#{procesoAumentoPorTabuladorForm.abort}"
	                        					immediate="true"
	                        					rendered="#{procesoAumentoPorTabuladorForm.show2}"
	                        					onclick="javascript:return clickMade()"
	                	        				 />
	                	        			
	    								<td>
    								</tr>  	
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorTabuladorForm.formato}"
	    										onchange="this.form.submit()"
	    										rendered="#{procesoAumentoPorTabuladorForm.show}"   	
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										rendered="#{procesoAumentoPorTabuladorForm.show}"
    											action="#{procesoAumentoPorTabuladorForm.runReport}"
    											onclick="windowReport()" />
    										
    									<td>
    								</tr>							
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>
