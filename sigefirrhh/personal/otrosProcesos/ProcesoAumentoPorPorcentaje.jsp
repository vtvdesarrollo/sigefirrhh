<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">

	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{procesoAumentoPorPorcentajeForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formAumentoPorPorcentaje" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Aumento por Porcentaje
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/otrosProcesos/ProcesoAumentoPorPorcentaje.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorPorcentajeForm.idTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{procesoAumentoPorPorcentajeForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{procesoAumentoPorPorcentajeForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Criterio
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorPorcentajeForm.criterio}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{procesoAumentoPorPorcentajeForm.changeCriterio}">
												<f:selectItem itemLabel="Todos" itemValue="0" />
                                                <f:selectItem itemLabel="Por Regi�n" itemValue="1" />
                                                <f:selectItem itemLabel="Por Rango de Grado" itemValue="2" />
                                                <f:selectItem itemLabel="Por Per�odo de Ingreso" itemValue="3" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>    									
    										Regi�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorPorcentajeForm.idRegion}"
    											rendered="#{procesoAumentoPorPorcentajeForm.showRegion}"
                                            	immediate="false">
                                                <f:selectItems value="#{procesoAumentoPorPorcentajeForm.listRegion}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td colspan="2">
    									<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
    									<tr>
    									<td width="20%">
    										Desde Grado
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{procesoAumentoPorPorcentajeForm.showGrado}"
    											size="5"
    											id="DesdeGrado"
    											maxlength="3"
    											value="#{procesoAumentoPorPorcentajeForm.desdeGrado}"
    											required="false" />    										
    									</td>
    									<td width="20%">
    										Hasta Grado
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{procesoAumentoPorPorcentajeForm.showGrado}"
    											size="5"
    											id="HastaGrado"
    											maxlength="3"
    											value="#{procesoAumentoPorPorcentajeForm.hastaGrado}"
    											required="false" />    								
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Desde Fecha Ingreso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{procesoAumentoPorPorcentajeForm.showFecha}"
    											size="15"
    											id="DesdeFechaIngreso"
    											maxlength="12"
    											value="#{procesoAumentoPorPorcentajeForm.desdeFechaIngreso}"
    											required="false" />    										
    									</td>
    									<td width="20%">
    										Hasta Fecha Ingreso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{procesoAumentoPorPorcentajeForm.showFecha}"
    											size="15"
    											id="HastaFechaIngreso"
    											maxlength="12"
    											value="#{procesoAumentoPorPorcentajeForm.hastaFechaIngreso}"
    											required="false" />    								
    									</td>
									</tr>
									<tr>
    									<td>
    										Aumento se refleja en
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorPorcentajeForm.tipoAumento}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{procesoAumentoPorPorcentajeForm.changeTipoAumento}">
                                                <f:selectItem itemLabel="Sueldo/Salario Basico" itemValue="1" />
                                                <f:selectItem itemLabel="Ajustes" itemValue="2" />
                                                <f:selectItem itemLabel="Compensaciones" itemValue="3" />
                                                <f:selectItem itemLabel="Primas Trabajador" itemValue="4" />
                                                <f:selectItem itemLabel="Primas por Cargo" itemValue="5" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>    									
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorPorcentajeForm.idConcepto}"
	    										onchange="this.form.submit()"
	    										valueChangeListener="#{procesoAumentoPorPorcentajeForm.changeConcepto}"
    											rendered="#{procesoAumentoPorPorcentajeForm.showConcepto}"
                                            	immediate="false">
                                                <f:selectItems value="#{procesoAumentoPorPorcentajeForm.listConcepto}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    								
    								<tr>
    									<td>
    										Tiene retroactivo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorPorcentajeForm.retroactivo}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{procesoAumentoPorPorcentajeForm.changeRetroactivo}">
                                                <f:selectItem itemLabel="No" itemValue="N" />												
                                                <f:selectItem itemLabel="Si" itemValue="S" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
									<tr>
    									<td>
    										Concepto retroactivo
    									</td>
    									<td width="100%">
    										<h:inputText
    											readonly="true"
    											size="50"
    											id="ConceptoRetroactivo"
    											value="#{procesoAumentoPorPorcentajeForm.descripcionRetroactivo}"
    											rendered="#{procesoAumentoPorPorcentajeForm.showConceptoRetroactivo}"
    											required="false" />   
    									</td>
    									</td>
    								</tr>   
    								<tr>
    									<td>
    										D�as a calcular retroactivo
    									</td>
    									<td width="100%">
    									<h:inputText
											size="6"
											id="DiasRetroactivo"
											maxlength="4"
											value="#{procesoAumentoPorPorcentajeForm.diasRetroactivo}"
											rendered="#{procesoAumentoPorPorcentajeForm.showConceptoRetroactivo}"
											required="false" />   
    									</td>
    								</tr>    	
    								<tr>
    									<td>
    										Porcentaje de Aumento
    									</td>
    									<td width="100%">
    									<h:inputText
											size="15"
											id="PorcentajeAumento"
											maxlength="12"
											value="#{procesoAumentoPorPorcentajeForm.porcentajeAumento}"
											required="false" />   
    									</td>
    								</tr>    								    								
									</table>
									</td>
    								<tr>
    								<td colspan="2" align="center">										
										<h:commandButton image="/images/run.gif"
    										rendered="#{procesoAumentoPorPorcentajeForm.show1}"
											action="#{procesoAumentoPorPorcentajeForm.generar}"
											/>
    								<td>
    								</tr>
    								
    								<tr>
    								<td colspan="2" align="center">	
	    															
										<h:outputText
                        					value="�Esta seguro que desea actualizar estos "
                        					rendered="#{procesoAumentoPorPorcentajeForm.show2}" />	
                        				<h:outputText
                        					value=" registros?"
                        					rendered="#{procesoAumentoPorPorcentajeForm.show2}" />                        				
                        				<h:commandButton value="Si" 
                        					image="/images/yes.gif"
                        					action="#{procesoAumentoPorPorcentajeForm.actualizar}"
                        					onclick="javascript:return clickMade()"
                        					rendered="#{procesoAumentoPorPorcentajeForm.show2}" />
                        					                        			                        				
                        				<h:commandButton value="Cancelar" 
                        					image="/images/cancel.gif"
                        					action="#{procesoAumentoPorPorcentajeForm.abort}"
                        					immediate="true"
                        					rendered="#{procesoAumentoPorPorcentajeForm.show2}"
                        					onclick="javascript:return clickMade()"
                	        				 />
                	        			
    								<td>
    								</tr>
    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>