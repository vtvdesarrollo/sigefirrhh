<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
		<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formProcesoAumentoPorEvaluacion'].elements['formProcesoAumentoPorEvaluacion:reportId'].value;
			var name = 
				document.forms['formProcesoAumentoPorEvaluacion'].elements['formProcesoAumentoPorEvaluacion:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    	</script>
    	
    	<jsp:include page="/inc/functions.jsp" />
    	
</head>

<body>
<f:view>
	<x:saveState value="#{procesoAumentoPorEvaluacionForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			
			<td width="570" valign="top">
				<h:form id="formProcesoAumentoPorEvaluacion">
				
				    <h:inputHidden id="reportId" value="#{procesoAumentoPorEvaluacionForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{procesoAumentoPorEvaluacionForm.reportName}" />
				    			
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Aumento Por Evaluaci�n
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/otrosProcesos/ProcesoAumentoPorEvaluacion.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorEvaluacionForm.selectTipoPersonal}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{procesoAumentoPorEvaluacionForm.changeTipoPersonal}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{procesoAumentoPorEvaluacionForm.listTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    														
    								<tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">   
    									<h:selectOneMenu value="#{procesoAumentoPorEvaluacionForm.mes}"    											
    											immediate="false">
                                            	<f:selectItem itemLabel="Primer" itemValue="1" />
                                            	<f:selectItem itemLabel="Segundo" itemValue="2" />
                                            											
                                            </h:selectOneMenu> 									
    										  										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{procesoAumentoPorEvaluacionForm.show}"
    											
    											size="5"
    											id="Anio"
    											maxlength="4"
    											value="#{procesoAumentoPorEvaluacionForm.anio}"    											
    											required="false" />    								
    									</td>
									</tr>
									<tr>
    									<td>
    										Criterio Aplicar
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorEvaluacionForm.metodo}"
	    										rendered="#{procesoAumentoPorEvaluacionForm.show}"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Aumentar por Porcentaje" itemValue="P" />
                                            	<f:selectItem itemLabel="Aumentar por Monto" itemValue="M" />
                                            	<f:selectItem itemLabel="Incrementar Pasos" itemValue="N" />                                            	
                                            	<f:selectItem itemLabel="Asignar Monto Unico" itemValue="U" />                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
									<tr>
    									<td>
    										Reflejar Aumento en
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorEvaluacionForm.registro}"
	    										rendered="#{procesoAumentoPorEvaluacionForm.show}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{procesoAumentoPorEvaluacionForm.changeRegistro}"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Sueldo Basico" itemValue="S" />
                                            	<f:selectItem itemLabel="Compensacion sin escala" itemValue="C" />
                                            	<f:selectItem itemLabel="Compensacion base escala" itemValue="A" />                                            	
                                            	<f:selectItem itemLabel="Otro Concepto" itemValue="O" />                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								    								
									<tr>
    									<td>
    										Proceso
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorEvaluacionForm.selectProceso}"
	    										rendered="#{procesoAumentoPorEvaluacionForm.show}"
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Prueba" itemValue="1" />
                                            	<f:selectItem itemLabel="Definitivo" itemValue="2" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Baso conceptos a utilizar
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorEvaluacionForm.origen}"
	    										rendered="#{procesoAumentoPorEvaluacionForm.show}"
	    										onchange="this.form.submit()" 
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Asignaciones actuales" itemValue="F" />
                                            	<f:selectItem itemLabel="Asignaciones de historico" itemValue="H" />
                                            	                                         	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td width="20%">
    										Del Mes
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											
    											rendered="#{procesoAumentoPorEvaluacionForm.showOrigen}"
    											size="3"
    											id="MesHistorico"
    											maxlength="2"
    											value="#{procesoAumentoPorEvaluacionForm.mesHistorico}"    											
    											required="false" />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Del A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{procesoAumentoPorEvaluacionForm.showOrigen}"
    											
    											size="5"
    											id="AnioHistorico"
    											maxlength="4"
    											value="#{procesoAumentoPorEvaluacionForm.anioHistorico}"    											
    											required="false" />    								
    									</td>
									</tr>	
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorEvaluacionForm.selectConceptoTipoPersonal}"
	    										rendered="#{procesoAumentoPorEvaluacionForm.showConcepto}"
	    										onchange="this.form.submit()"                                              	
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{procesoAumentoPorEvaluacionForm.listConceptoTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>    								
    								<tr>
    									<td>
    										Registrar en
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorEvaluacionForm.grabar}"
	    										rendered="#{procesoAumentoPorEvaluacionForm.showConcepto}"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Fijo" itemValue="F" />
                                            	<f:selectItem itemLabel="Variable" itemValue="V" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										rendered="#{procesoAumentoPorEvaluacionForm.show}"
    											action="#{procesoAumentoPorEvaluacionForm.preGenerate}"
    											/>
    									<td>    									
    								</tr>   
    								<tr>
	    								<td colspan="2" align="center">	
		    															
											<h:outputText
	                        					value="�Esta seguro que desea realizar este proceso?"
	                        					rendered="#{procesoAumentoPorEvaluacionForm.show2}" />                							                        				                				
	                        				<h:commandButton value="Si" 
	                        					image="/images/yes.gif"
	                        					action="#{procesoAumentoPorEvaluacionForm.generate}"
	                        					onclick="javascript:return clickMade()"
	                        					rendered="#{procesoAumentoPorEvaluacionForm.show2}" />                        					                        			                        				
	                        				<h:commandButton value="Cancelar" 
	                        					image="/images/cancel.gif"
	                        					action="#{procesoAumentoPorEvaluacionForm.abort}"
	                        					immediate="true"
	                        					rendered="#{procesoAumentoPorEvaluacionForm.show2}"
	                        					onclick="javascript:return clickMade()"
	                	        				 />
	                	        			
	    								<td>
    								</tr>  	
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{procesoAumentoPorEvaluacionForm.formato}"
	    										onchange="this.form.submit()"
	    										rendered="#{procesoAumentoPorEvaluacionForm.show}"   	
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	 	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										rendered="#{procesoAumentoPorEvaluacionForm.show}"
    											action="#{procesoAumentoPorEvaluacionForm.runReport}"
    											onclick="windowReport()" />
    										
    									<td>
    								</tr>							
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>