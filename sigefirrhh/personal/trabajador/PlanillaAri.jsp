<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{planillaAriForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formPlanillaAri">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Planilla ARI
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/trabajador/PlanillaAri.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchPlanillaAri"
								rendered="#{!planillaAriForm.showData&&!planillaAriForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
										<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{planillaAriForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planillaAriForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{planillaAriForm.findTrabajadorCedula}"
														onkeypress="javascript:return keyCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{planillaAriForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{planillaAriForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onblur="javascript:fieldEmpty(this)"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{planillaAriForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{planillaAriForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{planillaAriForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{planillaAriForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{planillaAriForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{planillaAriForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{planillaAriForm.showResultTrabajador&&
								!planillaAriForm.showData&&
								!planillaAriForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{planillaAriForm.resultTrabajador}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{planillaAriForm.findPlanillaAriByTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{planillaAriForm.selectedTrabajador&&
								!planillaAriForm.showData&&
								!planillaAriForm.editing&&
								!planillaAriForm.deleting}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{planillaAriForm.trabajador}"
                            						action="#{planillaAriForm.findPlanillaAriByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{planillaAriForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
									<tr>
										<td>
											<table width="100%" class="bandtable">
    											<tr>
    												<td style="text-align: right"></f:verbatim>
            											<h:commandButton image="/images/add.gif"
            												action="#{planillaAriForm.add}"
            												onclick="javascript:return clickMade()"
            												rendered="#{!planillaAriForm.editing&&!planillaAriForm.deleting&&planillaAriForm.login.agregar}" />
            											<h:commandButton value="Cancelar" 
			                                    			image="/images/cancel.gif"
                                    						action="#{planillaAriForm.abort}"
                                    						immediate="true"
                                    						onclick="javascript:return clickMade()"
                            	        					/>	
                            	        							
                                            			<f:verbatim>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultPlanillaAri"
								rendered="#{planillaAriForm.showResult&&
								!planillaAriForm.showData&&
								!planillaAriForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{planillaAriForm.result}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{planillaAriForm.selectPlanillaAri}"
                                						styleClass="listitem">
                                						<f:param name="idPlanillaAri" value="#{result.idPlanillaAri}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataPlanillaAri"
								rendered="#{planillaAriForm.showData||planillaAriForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!planillaAriForm.editing&&!planillaAriForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{planillaAriForm.editing&&!planillaAriForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{planillaAriForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{planillaAriForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
<h:form id="planillaAriForm">
            							<f:subview 
            								id="dataPlanillaAri"
            								rendered="#{planillaAriForm.showData}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{planillaAriForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!planillaAriForm.editing&&!planillaAriForm.deleting&&planillaAriForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{planillaAriForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!planillaAriForm.editing&&!planillaAriForm.deleting&&planillaAriForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{planillaAriForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{planillaAriForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{planillaAriForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{planillaAriForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{planillaAriForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{planillaAriForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							/>	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim><tr>
                        						<td>
            										Trabajador
                        						</td>
                        						<td></f:verbatim>
                        							<h:outputText value="#{planillaAriForm.trabajador}" />
                        						<f:verbatim></td>
            								</tr></f:verbatim>
            								
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Remuneraciones
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="14"
	                        							maxlength="16"
                        								value="#{planillaAriForm.planillaAri.remuneraciones}"
                        								                        								readonly="#{!planillaAriForm.editing}"
                        								id="remuneraciones"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        																				<h:message for="remuneraciones" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Desgravamenes
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="14"
	                        							maxlength="16"
                        								value="#{planillaAriForm.planillaAri.desgravamenes}"
                        								                        								readonly="#{!planillaAriForm.editing}"
                        								id="desgravamenes"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        																				<h:message for="desgravamenes" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Total Cargas
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							maxlength="3"
                        								value="#{planillaAriForm.planillaAri.totalCargas}"
                        								                        								readonly="#{!planillaAriForm.editing}"
                        								id="totalCargas"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        																						 >
                        							</h:inputText>
                        																				<h:message for="totalCargas" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Porcentaje %
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaAriForm.planillaAri.porcentaje}"
                        								readonly="true"
                        								id="porcentaje"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        																				<h:message for="porcentaje" styleClass="error"/>
                        						<f:verbatim></td>
                        						<td align="left">
													</f:verbatim>				
														<h:commandButton value="Calcular" 
 														    action="#{planillaAriForm.calcular}"
		                                					image="/images/calculate.gif"		                                					
		                                					onclick="javascript:return clickMade()"/>
		                        	        		<f:verbatim>
													</td>
                        					</tr>                            </f:verbatim>
									</f:subview>
            							<f:subview 
            								id="commandsPlanillaAri"
            								rendered="#{planillaAriForm.showData}">
                        				<f:verbatim><table>
                        				<table class="datatable" align="center">
                        					<tr>
                        					
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{planillaAriForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!planillaAriForm.editing&&!planillaAriForm.deleting&&planillaAriForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{planillaAriForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!planillaAriForm.editing&&!planillaAriForm.deleting&&planillaAriForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{planillaAriForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{planillaAriForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{planillaAriForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{planillaAriForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{planillaAriForm.editing}" />	
                            	        			<h:commandButton value="Cancelar" 
			                                    		image="/images/cancel.gif"
                                    					action="#{planillaAriForm.abortUpdate}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />	
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>