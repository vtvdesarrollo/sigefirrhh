<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{prestamoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formPrestamo">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Pr�stamos
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/trabajador/Prestamo.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchPrestamo"
								rendered="#{!prestamoForm.showData&&
									!prestamoForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{prestamoForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{prestamoForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												</td>
											</tr>
                        					<f:verbatim><tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{prestamoForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{prestamoForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{prestamoForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{prestamoForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{prestamoForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{prestamoForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{prestamoForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()"
														 />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{prestamoForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{prestamoForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{prestamoForm.showResultTrabajador&&
								!prestamoForm.showData&&
								!prestamoForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{prestamoForm.resultTrabajador}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{prestamoForm.findPrestamoByTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{prestamoForm.selectedTrabajador&&
								!prestamoForm.showData&&
								!prestamoForm.editing&&
								!prestamoForm.deleting}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{prestamoForm.trabajador}"
                            						action="#{prestamoForm.findPrestamoByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{prestamoForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
									<tr>
										<td>
											<table width="100%" class="bandtable">
    											<tr>
    												<td style="text-align: right"></f:verbatim>
            											<h:commandButton image="/images/add.gif"
            												action="#{prestamoForm.add}"
            												onclick="javascript:return clickMade()"
            												rendered="#{!prestamoForm.editing&&!prestamoForm.deleting&&prestamoForm.login.agregar&&!prestamoForm.trabajadorEgresado}" />
            											<h:commandButton value="Cancelar" 
	                                    					image="/images/cancel.gif"
	                        								action="#{prestamoForm.abort}"
	                        								immediate="true"
	                        								onclick="javascript:return clickMade()"
	                	        							/>
                                            			<f:verbatim>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultPrestamo"
								rendered="#{prestamoForm.showResult&&
								!prestamoForm.showData&&
								!prestamoForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{prestamoForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{prestamoForm.selectPrestamo}"
                                						styleClass="listitem">
                                						<f:param name="idPrestamo" value="#{result.idPrestamo}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataPrestamo"
								rendered="#{prestamoForm.showData||prestamoForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!prestamoForm.editing&&!prestamoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{prestamoForm.editing&&!prestamoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{prestamoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{prestamoForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
<h:form id="prestamoForm">
<h:inputHidden id="scrollx" value="#{prestamoForm.scrollx}" />
<h:inputHidden id="scrolly" value="#{prestamoForm.scrolly}" />
            							<f:subview 
            								id="dataPrestamo"
            								rendered="#{prestamoForm.showData}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{prestamoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!prestamoForm.editing&&!prestamoForm.deleting&&prestamoForm.login.modificar&&!prestamoForm.trabajadorEgresado}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{prestamoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!prestamoForm.editing&&!prestamoForm.deleting&&prestamoForm.login.eliminar&&!prestamoForm.trabajadorEgresado}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{prestamoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{prestamoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{prestamoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{prestamoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{prestamoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{prestamoForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim><tr>
                        						<td>
            										Trabajador
                        						</td>
                        						<td></f:verbatim>
                        							<h:outputText value="#{prestamoForm.trabajador}" />
                        						<f:verbatim></td>
            								</tr></f:verbatim>
            								
            								<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{prestamoForm.selectConceptoTipoPersonal}"
                                                    	disabled="#{!prestamoForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="false"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{prestamoForm.colConceptoTipoPersonal}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Frecuencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{prestamoForm.selectFrecuenciaTipoPersonal}"
                                                    	disabled="#{!prestamoForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="false"
														title="Frecuencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{prestamoForm.colFrecuenciaTipoPersonal}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
            								
                        					<f:verbatim><tr>
                        						<td>
                        							Monto Prestamo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							maxlength="15"
                        								value="#{prestamoForm.prestamo.montoPrestamo}"                        								
                        								readonly="#{!prestamoForm.editing}"
                        								id="MontoPrestamo"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Numero Cuotas
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="5"
	                        							maxlength="4"
                        								value="#{prestamoForm.prestamo.numeroCuotas}"
                        								                        								readonly="#{!prestamoForm.editing}"
                        								id="NumeroCuotas"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
														 >
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cuotas Pagadas
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="5"
	                        							maxlength="4"
                        								value="#{prestamoForm.prestamo.cuotasPagadas}"
                        								                        								readonly="#{!prestamoForm.editing}"
                        								id="CuotasPagadas"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
														 >
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Monto Cuota
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							maxlength="15"
                        								value="#{prestamoForm.prestamo.montoCuota}"
                        								                        								readonly="#{!prestamoForm.editing}"
                        								id="MontoCuota"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>   </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Monto Pagado
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							maxlength="15"
                        								value="#{prestamoForm.prestamo.montoPagado}"
                        								readonly="#{!prestamoForm.editing}"
                        								id="MontoPagado"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        					<f:verbatim></td>
                        					</tr>                            
                        					</f:verbatim>
                        					<f:verbatim>
											<tr>
                        						<td>
                        							Saldo
                        						</td>
                        						<td>
												</f:verbatim>
													<f:verbatim>
													<table border="0" width="100%">
														<tr>
															<td width="25%">
															</f:verbatim>
																<h:inputText
																	size="15"
																	maxlength="15"
																	value="#{prestamoForm.saldo}"
																	readonly="true"                          								
																	id="MontoSaldo"
																	required="false"
																	title="Saldo"
																	alt="Saldo"                        								
																	 style="text-align:right"
																	 onkeypress="return keyEnterCheck(event, this)"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
																</h:inputText>
															<f:verbatim>
															</td>
															<td width="75%" align="left">
															</f:verbatim>
																<h:commandButton value="Calcular" 
																	image="/images/calculate.gif"
																	action="this.form.submit()"
																	onclick="javascript:return clickMade()"/>
															<f:verbatim>
															</td>
														</tr>
													</table>
													</f:verbatim>
                        					<f:verbatim>
											</td>
                        					</tr>                            
                        					</f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha Otorgo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
	                        							id="FechaOtorgo"
                        								value="#{prestamoForm.prestamo.fechaOtorgo}"
                        								readonly="#{!prestamoForm.editing}"                        		
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								onblur="javascript:check_date(this)"
                        								                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha Comienzo Pago
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
	                        							id="FechaComienzoPago"
                        								value="#{prestamoForm.prestamo.fechaComienzoPago}"
                        								readonly="#{!prestamoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								onblur="javascript:check_date(this)"
                        								                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha Registro
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
	                        							id="FechaRegistro"
                        								value="#{prestamoForm.prestamo.fechaRegistro}"
                        								readonly="true"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								onblur="javascript:check_date(this)"
                        								                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Estatus
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{prestamoForm.prestamo.estatus}"
                                                    	disabled="#{!prestamoForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="false"
                                                    	title="Estatus">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{prestamoForm.listEstatus}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					
                        					
                        					
									</f:subview>
            							<f:subview 
            								id="commandsPrestamo"
            								rendered="#{prestamoForm.showData}">
                        				<f:verbatim><table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{prestamoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!prestamoForm.editing&&!prestamoForm.deleting&&prestamoForm.login.modificar&&!prestamoForm.trabajadorEgresado}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{prestamoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!prestamoForm.editing&&!prestamoForm.deleting&&prestamoForm.login.eliminar&&!prestamoForm.trabajadorEgresado}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{prestamoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{prestamoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{prestamoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{prestamoForm.save}"
                            	        				rendered="#{prestamoForm.editing}" />	
                            	        			<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{prestamoForm.abortUpdate}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>