<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{trabajadorAsignaturaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formTrabajadorAsignatura">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Asignaturas/Actividades
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/trabajador/TrabajadorAsignatura.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchTrabajadorAsignatura"
								rendered="#{!trabajadorAsignaturaForm.showData&&
									!trabajadorAsignaturaForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
										<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{trabajadorAsignaturaForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trabajadorAsignaturaForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{trabajadorAsignaturaForm.findTrabajadorCedula}"
														onkeypress="javascript:return keyCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{trabajadorAsignaturaForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{trabajadorAsignaturaForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onblur="javascript:fieldEmpty(this)"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{trabajadorAsignaturaForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{trabajadorAsignaturaForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{trabajadorAsignaturaForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{trabajadorAsignaturaForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{trabajadorAsignaturaForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{trabajadorAsignaturaForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{trabajadorAsignaturaForm.showResultTrabajador&&
								!trabajadorAsignaturaForm.showData&&
								!trabajadorAsignaturaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{trabajadorAsignaturaForm.resultTrabajador}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{trabajadorAsignaturaForm.findTrabajadorAsignaturaByTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif" />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif" />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{trabajadorAsignaturaForm.selectedTrabajador&&
								!trabajadorAsignaturaForm.showData&&
								!trabajadorAsignaturaForm.editing&&
								!trabajadorAsignaturaForm.deleting}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{trabajadorAsignaturaForm.trabajador}"
                            						action="#{trabajadorAsignaturaForm.findTrabajadorAsignaturaByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{trabajadorAsignaturaForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
									<tr>
										<td>
											<table width="100%" class="bandtable">
    											<tr>
    												<td style="text-align: right"></f:verbatim>
            											<h:commandButton image="/images/add.gif"
            												action="#{trabajadorAsignaturaForm.add}"
            												onclick="javascript:return clickMade()"
            												rendered="#{!trabajadorAsignaturaForm.editing&&!trabajadorAsignaturaForm.deleting&&trabajadorAsignaturaForm.login.agregar}" />
            											<h:commandButton value="Cancelar" 
			                                    			image="/images/cancel.gif"
                                    						action="#{trabajadorAsignaturaForm.abort}"
                                    						immediate="true"
                                    						onclick="javascript:return clickMade()"
                            	        					/>	
                            	        							
                                            			<f:verbatim>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultTrabajadorAsignatura"
								rendered="#{trabajadorAsignaturaForm.showResult&&
								!trabajadorAsignaturaForm.showData&&
								!trabajadorAsignaturaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{trabajadorAsignaturaForm.result}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{trabajadorAsignaturaForm.selectTrabajadorAsignatura}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajadorAsignatura" value="#{result.idTrabajadorAsignatura}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataTrabajadorAsignatura"
								rendered="#{trabajadorAsignaturaForm.showData||trabajadorAsignaturaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!trabajadorAsignaturaForm.editing&&!trabajadorAsignaturaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{trabajadorAsignaturaForm.editing&&!trabajadorAsignaturaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{trabajadorAsignaturaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{trabajadorAsignaturaForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
<h:form id="trabajadorAsignaturaForm">
            							<f:subview 
            								id="dataTrabajadorAsignatura"
            								rendered="#{trabajadorAsignaturaForm.showData}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{trabajadorAsignaturaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!trabajadorAsignaturaForm.editing&&!trabajadorAsignaturaForm.deleting&&trabajadorAsignaturaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{trabajadorAsignaturaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!trabajadorAsignaturaForm.editing&&!trabajadorAsignaturaForm.deleting&&trabajadorAsignaturaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{trabajadorAsignaturaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{trabajadorAsignaturaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{trabajadorAsignaturaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{trabajadorAsignaturaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{trabajadorAsignaturaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{trabajadorAsignaturaForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							/>	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim><tr>
                        						<td>
            										Trabajador
                        						</td>
                        						<td></f:verbatim>
                        							<h:outputText value="#{trabajadorAsignaturaForm.trabajador}" />
                        						<f:verbatim></td>
            								</tr></f:verbatim>
            								
                        					<f:verbatim><tr>
                        						<td>
                        							Asignatura/Actividad
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{trabajadorAsignaturaForm.selectAsignatura}"
                                                    	disabled="#{!trabajadorAsignaturaForm.editing}"
                                                    	immediate="false"
	                        					
	                        							
                        								                        								                                                    	required="false"
														title="Asignatura/Actividad">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{trabajadorAsignaturaForm.colAsignatura}" />
                                                    </h:selectOneMenu>												<h:message for="asignatura" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Horas
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="8"
	                        							maxlength="8"
                        								value="#{trabajadorAsignaturaForm.trabajadorAsignatura.horas}"
                        								                        								readonly="#{!trabajadorAsignaturaForm.editing}"
                        								id="horas"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        																				<h:message for="horas" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
									</f:subview>
            							<f:subview 
            								id="commandsTrabajadorAsignatura"
            								rendered="#{trabajadorAsignaturaForm.showData}">
                        				<f:verbatim><table>
                        				<table class="datatable" align="center">
                        					<tr>
                        					
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{trabajadorAsignaturaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!trabajadorAsignaturaForm.editing&&!trabajadorAsignaturaForm.deleting&&trabajadorAsignaturaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{trabajadorAsignaturaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!trabajadorAsignaturaForm.editing&&!trabajadorAsignaturaForm.deleting&&trabajadorAsignaturaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{trabajadorAsignaturaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{trabajadorAsignaturaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{trabajadorAsignaturaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{trabajadorAsignaturaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{trabajadorAsignaturaForm.editing}" />	
                            	        			<h:commandButton value="Cancelar" 
			                                    		image="/images/cancel.gif"
                                    					action="#{trabajadorAsignaturaForm.abortUpdate}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />	
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>