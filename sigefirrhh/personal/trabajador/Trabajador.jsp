<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">

<f:view>
	<x:saveState value="#{trabajadorForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />
				<%
	  			}else { %>
	  				 
	  				
				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formTrabajador">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>Consultar Datos N�mina</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/trabajador/Trabajador.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									
									<f:subview 
										id="searchTrabajador"
										rendered="#{!trabajadorForm.showData}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable">
													<tr>
		                        						<td>
		                        							Tipo de Personal
		                        						</td>
		                        						<td></f:verbatim>
							                            	<h:selectOneMenu value="#{trabajadorForm.findSelectTrabajadorIdTipoPersonal}"
							                            		
		                                                    	immediate="false">
		                                                    	<f:selectItem itemLabel="Seleccione" itemValue="0" />																
		                                                        <f:selectItems value="#{trabajadorForm.findColTipoPersonal}" />
		                                                    </h:selectOneMenu>
															
														<f:verbatim></td>
													</tr>
        		                					<tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{trabajadorForm.findTrabajadorCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
																onfocus="return deleteZero(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{trabajadorForm.findTrabajadorByCedula}" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{trabajadorForm.findTrabajadorPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{trabajadorForm.findTrabajadorSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{trabajadorForm.findTrabajadorPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{trabajadorForm.findTrabajadorSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{trabajadorForm.findTrabajadorByNombresApellidos}" />
														<f:verbatim></td>
													</tr>
													<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{trabajadorForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
																action="#{trabajadorForm.findTrabajadorByCodigoNomina}"
																onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{trabajadorForm.showResultTrabajador&&
									!trabajadorForm.showData}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Trabajador
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
										<td></f:verbatim>
                                            <h:dataTable id="resultTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{trabajadorForm.resultTrabajador}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{trabajadorForm.selectTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="resultTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="resultTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    				</table></f:verbatim>
								</f:subview>
								
								
							</h:form>
							<f:subview 
								id="viewDataTrabajador"
								rendered="#{trabajadorForm.showData}">
								<f:verbatim>
								<table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td>
													</verbatim>
														<h:outputText value="Consultando"/>
															
													<verbatim>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
										</f:verbatim>
                                            <h:form id="trabajadorForm">
	                                            <h:inputHidden id="scrollx" value="#{trabajadorForm.scrollx}" />
    	                                        <h:inputHidden id="scrolly" value="#{trabajadorForm.scrolly}" />
	    	        							<f:subview 
    	    	    								id="dataTrabajador"
        	    									rendered="#{trabajadorForm.showData}">
	        	    								<f:verbatim>
													<table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>    	                        	                        				
                                    	                					<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
						                        								action="#{trabajadorForm.abortUpdate}"
						                        								immediate="true"
						                        								onclick="javascript:return clickMade()"
						                	        							/>
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
                    	    								<td>Trabajador</td>
	                        								<td>
															</f:verbatim>
	    	                    								<h:outputText value="#{trabajadorForm.trabajador.personal}" />
    	    	                							<f:verbatim>
															</td>
	            										</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Tipo de Personal</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{trabajadorForm.trabajador.tipoPersonal.nombre}"
																	readonly="true"
                        											id="NombreTipoPersonal"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Estatus</td>
		                        							<td>
															</f:verbatim>
																<h:selectOneMenu value="#{trabajadorForm.trabajador.estatus}"
																	disabled="#{true}"
																	immediate="false"
																	required="false"
																	title="Estatus">
																<f:selectItem itemLabel="Seleccione" itemValue="0" />
																<f:selectItems value="#{trabajadorForm.listEstatus}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Registro Cargos/RPT</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{trabajadorForm.trabajador.registroCargos.registro.nombre}"
																	readonly="true"
                        											id="RegistroCargos"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														
														<f:verbatim>
														<tr>
    	                    								<td>Cargo</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="66"
	    	    		                							maxlength="66"
            	    		        								value="#{trabajadorForm.trabajador.cargo.descripcionCargo} #{trabajadorForm.trabajador.cargo.codCargo}"
																	readonly="true"
                        											id="Cargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Grado</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="4"
	    	    		                							maxlength="4"
            	    		        								value="#{trabajadorForm.trabajador.cargo.grado}"
																	readonly="true"
                        											id="Grado"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Dependencia</td>
    	                    								<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="100"
	    	    		                							maxlength="100"
            	    		        								value="#{trabajadorForm.trabajador.dependencia.nombre} #{trabajadorForm.trabajador.dependencia.codDependencia}"
																	readonly="true"
                        											id="Dependencia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Lugar de Pago</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="100"
	    	    		                							maxlength="100"
            	    		        								value="#{trabajadorForm.trabajador.lugarPago.nombre} #{trabajadorForm.trabajador.lugarPago.codLugarPago} #{trabajadorForm.trabajador.lugarPago.sede.nombre}"
																	readonly="true"
                        											id="LugarPago"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>C�digo N�mina</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="7"
	    	    		                							maxlength="7"
            	    		        								value="#{trabajadorForm.trabajador.codigoNomina}"
																	readonly="true"
                        											id="CodigoNomina"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Sueldo B�sico</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{trabajadorForm.trabajador.sueldoBasico}"
																	readonly="true"
                        											id="SueldoBasico"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
																	style="text-align:right">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Paso</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{trabajadorForm.trabajador.paso}"
																	readonly="true"
                        											id="Paso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Turno</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="30"
	    	    		                							maxlength="30"
            	    		        								value="#{trabajadorForm.trabajador.turno.nombre} #{trabajadorForm.trabajador.turno.codTurno}"
																	readonly="true"
                        											id="Turno"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>										        	            	    							<f:verbatim></td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Riesgo SSO</td>
		                        							<td>
															</f:verbatim>
																 <h:selectOneMenu value="#{trabajadorForm.trabajador.riesgo}"
        	                                        	        	disabled="true"
            	                                        	    	immediate="false"
																	required="false"
    	                        	                            	title="Riesgo SSO">
    															<f:selectItem itemLabel="Seleccione" itemValue="0" />
																<f:selectItems value="#{trabajadorForm.listRiesgo}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Regimen</td>
		                        							<td>
															</f:verbatim>
																<h:selectOneMenu value="#{trabajadorForm.trabajador.regimen}"
        	                                        	        	disabled="true"
            	                                        	    	immediate="false"
																	required="false"
    	                        	                            	title="Regimen">
    															<f:selectItem itemLabel="Seleccione" itemValue="0" />
																<f:selectItems value="#{trabajadorForm.listRegimen}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Fecha Ingreso Organismo</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaIngresoOrganismo"
                		        									value="#{trabajadorForm.trabajador.fechaIngreso}"
                    		    									readonly="true"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
																	required="false">
																<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                pattern="dd-MM-yyyy" />
                    		    								</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Fecha Ingreso APN</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaIngresoAPN"
                		        									value="#{trabajadorForm.trabajador.fechaIngresoApn}"
                    		    									readonly="true"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
    	                    										required="false">
																<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                pattern="dd-MM-yyyy" />
                    		    								</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														
														
														<f:verbatim>
														<tr>
    	                    								<td>Fecha C�lculo Vacaciones</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaCalculoVacaciones"
                		        									value="#{trabajadorForm.trabajador.fechaVacaciones}"
                    		    									readonly="true"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
																	required="false">
																<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                pattern="dd-MM-yyyy" />
                    		    								</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														
														<f:verbatim>
														<tr>
    	                    								<td>Fecha C�culo Prima Antiguedad</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaCalculoPrimaAntiguedad"
                		        									value="#{trabajadorForm.trabajador.fechaAntiguedad}"
                    		    									readonly="true"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
																	required="false">
																<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                pattern="dd-MM-yyyy" />
                    		    								</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														
														<f:verbatim>
														<tr>
    	                    								<td>Fecha C�lculo Prestaciones</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaCalculoPrestaciones"
                		        									value="#{trabajadorForm.trabajador.fechaPrestaciones}"
                    		    									readonly="true"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
																	required="false">
																<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                pattern="dd-MM-yyyy" />
                    		    								</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														
														<f:verbatim>
														<tr>
    	                    								<td>Forma de Pago</td>
		                        							<td>
															</f:verbatim>
																<h:selectOneMenu value="#{trabajadorForm.trabajador.formaPago}"
        	                                        	        	disabled="true"
            	                                        	    	immediate="false"
																	required="false"
    	                        	                            	title="Forma de Pago">
    															<f:selectItem itemLabel="Seleccione" itemValue="0" />
																<f:selectItems value="#{trabajadorForm.listFormaPago}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Banco N�mina</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{trabajadorForm.trabajador.bancoNomina.nombre} #{trabajadorForm.trabajador.bancoNomina.codBanco}"
																	readonly="true"
                        											id="BancoNomina"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Tipo de Cuenta</td>
		                        							<td>
															</f:verbatim>
																<h:selectOneMenu value="#{trabajadorForm.trabajador.tipoCtaNomina}"
        	                                        	        	disabled="true"
            	                                        	    	immediate="false"
                	            									 rendered="#{trabajadorForm.showTipoCtaNominaAux}"                     	        										                        	                                	required="false"
    	                        	                            	title="Tipo de Cuenta">
    															<f:selectItem itemLabel="Seleccione" itemValue="0" />
																<f:selectItems value="#{trabajadorForm.listTipoCtaNomina}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>N� Cuenta</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="25"
	    	    		                							maxlength="20"
            	    		        								value="#{trabajadorForm.trabajador.cuentaNomina}"
																	rendered="#{trabajadorForm.showCuentaNominaAux}"
																	readonly="true"
                        											id="NoCuenta"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Banco FAOV</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{trabajadorForm.trabajador.bancoLph.nombre} #{trabajadorForm.trabajador.bancoLph.codBanco}"
																	readonly="true"
                        											id="BancoLPH"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Banco Fideicomiso</td>
		                        							<td></f:verbatim>
																<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{trabajadorForm.trabajador.bancoFid.nombre} #{trabajadorForm.trabajador.bancoFid.codBanco}"
																	readonly="true"
                        											id="BancoFideicomiso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>% I.S.L.R.</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{trabajadorForm.trabajador.porcentajeIslr}"
																	readonly="true"
                        											id="ISLR"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	style="text-align:right">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Cotiza SSO</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="3"
	    	    		                							maxlength="3"
            	    		        								value="#{trabajadorForm.trabajador.cotizaSso}"
																	readonly="true"
                        											id="CotizaSSO"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Cotiza SPF</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="3"
	    	    		                							maxlength="3"
            	    		        								value="#{trabajadorForm.trabajador.cotizaSpf}"
																	readonly="true"
                        											id="CotizaSPF"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Cotiza FAOV</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="3"
	    	    		                							maxlength="3"
            	    		        								value="#{trabajadorForm.trabajador.cotizaLph}"
                	    		    								readonly="true"
                        											id="CotizaLPH"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Cotiza FJU</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="3"
	    	    		                							maxlength="3"
            	    		        								value="#{trabajadorForm.trabajador.cotizaFju}"
																	readonly="true"
                        											id="CotizaFJU"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Fecha Egreso</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaEgreso"
                		        									value="#{trabajadorForm.trabajador.fechaEgreso}"
                    		    									readonly="true"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
																	required="false">
																<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                pattern="dd-MM-yyyy" />
                    		    								</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>F� de Vida</td>
		                        							<td>
															</f:verbatim>
																 <h:selectOneMenu value="#{trabajadorForm.trabajador.feVida}"
        	                                        	        	disabled="true"
            	                                        	    	immediate="false"
																	required="false"
    	                        	                            	title="F� de Vida">
    															<f:selectItem itemLabel="Seleccione" itemValue="0" />
																<f:selectItems value="#{trabajadorForm.listFeVida}" />
																</h:selectOneMenu>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Ultimo Movimiento</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{trabajadorForm.trabajador.causaMovimiento.descripcion}"
																	readonly="true"
                        											id="CausaEgreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
																</h:inputText>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>Fecha Ultimo Movimiento</td>
		                        							<td>
															</f:verbatim>
																<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaUltimoMovimiento"
                		        									value="#{trabajadorForm.trabajador.fechaUltimoMovimiento}"
                    		    									readonly="true"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
    	                    										required="false">
																<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                pattern="dd-MM-yyyy" />
                    		    								</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
															<f:verbatim>
															</td>
	        	            	    					</tr>
														</f:verbatim>
													<f:verbatim>
													</table>
													</f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsTrabajador"
            										rendered="#{trabajadorForm.showData}">
		                        					<f:verbatim><table>
    		                    						<tr>
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{trabajadorForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>
