<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportEgreso'].elements['formReportEgreso:reportId'].value;
			var name = 
				document.forms['formReportEgreso'].elements['formReportEgreso:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}    	
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportEgresoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportEgreso">
				
    				<h:inputHidden id="reportId" value="#{reportEgresoForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportEgresoForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reporte Egresos
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success" />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEgresoForm.idTipoPersonal}"
                                            	immediate="false">
                                                <f:selectItems value="#{reportEgresoForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    								
    								<tr>
    									<td>
    										Agrupado por ?
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEgresoForm.agrupado}"
    											
    											onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Regi�n" itemValue="R" />  
                                            	<f:selectItem itemLabel="Sede" itemValue="S" />
                                            	<f:selectItem itemLabel="Dependencia" itemValue="D" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportEgresoForm.formato}"
    											onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                                <f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />   
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td width="20%">
    										Desde
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText    											
    											size="10"
    											id="Desde"
    											maxlength="10"
    											onblur="javascript:check_date(this)"
    											value="#{reportEgresoForm.desde}"
    											required="false" >    										
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		pattern="dd-MM-yyyy" />
                	                        </h:inputText>
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Hasta
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="10"
    											id="Hasta"
    											maxlength="10"
    											onblur="javascript:check_date(this)"
    											value="#{reportEgresoForm.hasta}"
    											required="false" >    								
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		pattern="dd-MM-yyyy" />
                	                        </h:inputText>
    									</td>
									</tr>	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
    											action="#{reportEgresoForm.runReport}"
    											onclick="windowReport()" />
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>