<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formFechasNacimientoServicioUnidadEjecutora'].elements['formFechasNacimientoServicioUnidadEjecutora:reportId'].value;
			var name = 
				document.forms['formFechasNacimientoServicioUnidadEjecutora'].elements['formFechasNacimientoServicioUnidadEjecutora:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}    	
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportFechasNacimientoServicioUnidadEjecutoraForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formFechasNacimientoServicioUnidadEjecutora">
				
    				<h:inputHidden id="reportId" value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								 Reporte Edad y Tiempo Servicio/UEL
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.idTipoPersonal}"
                                            	immediate="false">
                                                <f:selectItems value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Unidades Ejecutoras
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.idUnidadEjecutora}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
    											<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.listUnidadEjecutora}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Ordenado por 
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.orden}"
    											
    											onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Alfab�tico" itemValue="A" />  
                                            	<f:selectItem itemLabel="C�dula" itemValue="C" />
                                            	<f:selectItem itemLabel="C�digo de N�mina" itemValue="N" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td width="20%">
    										Fecha Tope
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText    											
    											size="10"
    											id="FechaTope"
    											maxlength="10"
    											onblur="javascript:check_date(this)"
    											value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.fechaTope}"
    											required="false" >    										
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		pattern="dd-MM-yyyy" />
                	                        </h:inputText>
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Edad M�nima
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="EdadMinima"
    											maxlength="3"
    											value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.edadMinima}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Edad M�xima
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="EdadMaxima"
    											maxlength="3"
    											value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.edadMaxima}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Tiempo Servicio M�nimo
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="ServicioMinimo"
    											maxlength="3"
    											value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.servicioMinimo}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Tiempo de Servicio M�ximo
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="ServicioMaximo"
    											maxlength="3"
    											value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.servicioMaximo}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td>
    										Sexo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.sexo}"
    											
    											onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Todos" itemValue="T" />  
                                            	<f:selectItem itemLabel="Femenino" itemValue="F" />
                                            	<f:selectItem itemLabel="Masculino" itemValue="M" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFechasNacimientoServicioUnidadEjecutoraForm.formato}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
    											action="#{reportFechasNacimientoServicioUnidadEjecutoraForm.runReport}"
    											onclick="windowReport()" />
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>