<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{actualizarDatosTrabajadorForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="center">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />
				<%
	  			}else { %>
	  				 
	  				
				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formActualizarDatosTrabajador">
								<table width="100%" class="toptable">
									<tr>
										<td align="center">
											<b>
												Actualizar Datos del Trabajador
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/trabajador/ActualizarDatosTrabajador.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
											
											<h:commandButton value="Cerrar" 
                        					image="/images/close.gif"
                        					action="go_cancelOption"
                        					immediate="true"                        					
                        					onclick="javascript:return clickMade()"
                	        				 />
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="center">
										<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									
									<f:subview 
										id="searchTrabajador"
										rendered="#{!actualizarDatosTrabajadorForm.showData&&
										!actualizarDatosTrabajadorForm.showAdd}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable">
        		                						<tr>
		                        						<td>
		                        							Tipo de Personal
		                        						</td>
		                        						<td></f:verbatim>
							                            	<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.findSelectTrabajadorIdTipoPersonal}"
							                            		id="colTipoPersonal"
		                                                    	immediate="false">
		                                                    	<f:selectItem itemLabel="Seleccione" itemValue="0" />																
		                                                        <f:selectItems value="#{actualizarDatosTrabajadorForm.findColTipoPersonal}" />
		                                                    </h:selectOneMenu>
															
														<f:verbatim></td>
													</tr>
        		                					<tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{actualizarDatosTrabajadorForm.findTrabajadorCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
																onfocus="return deleteZero(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{actualizarDatosTrabajadorForm.findTrabajadorByCedula}" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarDatosTrabajadorForm.findTrabajadorPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarDatosTrabajadorForm.findTrabajadorSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarDatosTrabajadorForm.findTrabajadorPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarDatosTrabajadorForm.findTrabajadorSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{actualizarDatosTrabajadorForm.findTrabajadorByNombresApellidos}" />
														<f:verbatim></td>
													</tr>
													<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{actualizarDatosTrabajadorForm.findTrabajadorCodigoNomina}"
														onfocus="return deleteZero(event, this)"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
																action="#{actualizarDatosTrabajadorForm.findTrabajadorByCodigoNomina}"
																onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultTrabajador"
									rendered="#{actualizarDatosTrabajadorForm.showResultTrabajador&&
									!actualizarDatosTrabajadorForm.showData&&
									!actualizarDatosTrabajadorForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <h:dataTable id="resultTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{actualizarDatosTrabajadorForm.resultTrabajador}"
                                               
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{actualizarDatosTrabajadorForm.selectTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="resultTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="resultTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataTrabajador"
								rendered="#{actualizarDatosTrabajadorForm.showData||actualizarDatosTrabajadorForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!actualizarDatosTrabajadorForm.editing&&!actualizarDatosTrabajadorForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{actualizarDatosTrabajadorForm.editing&&!actualizarDatosTrabajadorForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{actualizarDatosTrabajadorForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{actualizarDatosTrabajadorForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="actualizarDatosTrabajadorForm">
	    	        							<f:subview 
    	    	    								id="dataTrabajador"
        	    									rendered="#{actualizarDatosTrabajadorForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{actualizarDatosTrabajadorForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!actualizarDatosTrabajadorForm.editing&&!actualizarDatosTrabajadorForm.deleting&&actualizarDatosTrabajadorForm.login.modificar&&!actualizarDatosTrabajadorForm.trabajadorEgresado}" />
                    	                        	        				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{actualizarDatosTrabajadorForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{actualizarDatosTrabajadorForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{actualizarDatosTrabajadorForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{actualizarDatosTrabajadorForm.trabajador.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Lugar de Pago
	            	            							</td>
		                        							<td></f:verbatim>
									    		                    <h:selectOneMenu value="#{actualizarDatosTrabajadorForm.selectLugarPago}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="LugarPago"
                		            								required="false"
    																title="Lugar de Pago">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.colLugarPago}" />
								                                    </h:selectOneMenu><h:message for="LugarPago" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						
														
														<f:verbatim><tr>
    	                    								<td>
        	                									Turno
	            	            							</td>
		                        							<td></f:verbatim>
									    		                    <h:selectOneMenu value="#{actualizarDatosTrabajadorForm.selectTurno}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="Turno"
                		            								                    		        								                        		                                	required="false"
    																title="Turno">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.colTurno}" />
								                                    </h:selectOneMenu>																									<h:message for="Turno" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									Riesgo SSO
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.riesgo}"
        	                                        	        	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            	                                        	    	immediate="false"
				                        							id="Riesgo"
                	            									                    	        										                        	                                	required="false"
    	                        	                            	title="Riesgo SSO">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{actualizarDatosTrabajadorForm.listRiesgo}" />
					    	            	                        </h:selectOneMenu>																				<h:message for="Riesgo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									Regimen
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.regimen}"
        	                                        	        	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            	                                        	    	immediate="false"
				                        							id="Regimen"
                	            									                    	        										                        	                                	required="false"
    	                        	                            	title="Regimen">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{actualizarDatosTrabajadorForm.listRegimen}" />
					    	            	                        </h:selectOneMenu>																				<h:message for="Regimen" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														
														<f:verbatim><tr>
    	                    								<td>
        	                									Forma de Pago
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.formaPago}"
        	                                        	        	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            	                                        	    	immediate="false"
				                        							id="FormaPago"
                	            									onchange="submit()"                    	        										                        	                                	required="false"
    	                        	                            	title="Forma de Pago">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{actualizarDatosTrabajadorForm.listFormaPago}" />
					    	            	                        </h:selectOneMenu>																				<h:message for="FormaPago" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									Banco N�mina
	            	            							</td>
		                        							<td></f:verbatim>
									    		                    <h:selectOneMenu value="#{actualizarDatosTrabajadorForm.selectBancoNomina}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="BancoNomina"
                		            								 rendered="#{actualizarDatosTrabajadorForm.showBancoNominaAux}"                     		        								                        		                                	required="false"
    																title="Banco N�mina">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.colBancoNomina}" />
								                                    </h:selectOneMenu>																									<h:message for="BancoNomina" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									Tipo de Cuenta
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.tipoCtaNomina}"
        	                                        	        	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            	                                        	    	immediate="false"
				                        							id="TipoCtaNomina"
                	            									 rendered="#{actualizarDatosTrabajadorForm.showTipoCtaNominaAux}"         required="false"
    	                        	                            	title="Tipo de Cuenta">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{actualizarDatosTrabajadorForm.listTipoCtaNomina}" />
					    	            	                        </h:selectOneMenu>	<h:message for="TipoCtaNomina" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									N� Cuenta
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="24"
	    	    		                							maxlength="20"
            	    		        								value="#{actualizarDatosTrabajadorForm.trabajador.cuentaNomina}"
                	    		    								 rendered="#{actualizarDatosTrabajadorForm.showCuentaNominaAux}"                     	    										readonly="#{!actualizarDatosTrabajadorForm.editing}"
                        											id="CuentaNomina"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="CuentaNomina" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Banco FAOV
	            	            							</td>
		                        							<td></f:verbatim>
									    		                 <h:selectOneMenu value="#{actualizarDatosTrabajadorForm.selectBancoLph}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="BancoLph"
                		            								                    		        								                        		                                	required="false"
    																title="Banco LPH">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.colBancoLph}" />
								                                 </h:selectOneMenu>																									<h:message for="BancoLph" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																										                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Banco Fideicomiso
	            	            							</td>
		                        							<td></f:verbatim>
									    		                <h:selectOneMenu value="#{actualizarDatosTrabajadorForm.selectBancoFid}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="BancoFid"
                		            								                    		        								                        		                                	required="false"
    																title="Banco Fideicomiso">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.colBancoFid}" />
								                                 </h:selectOneMenu>																									<h:message for="BancoFid" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    						<f:verbatim><tr>
    	                    								<td>
        	                									N� Cuenta Fideicomiso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="24"
	    	    		                							maxlength="20"
            	    		        								value="#{actualizarDatosTrabajadorForm.trabajador.cuentaFid}"
                	    		    								readonly="#{!actualizarDatosTrabajadorForm.editing}"
                        											id="CuentaFid"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="CuentaNomina" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																										                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									% I.S.L.R.
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{actualizarDatosTrabajadorForm.trabajador.porcentajeIslr}"
                	    		    								                    	    										readonly="#{!actualizarDatosTrabajadorForm.editing}"
                        											id="PorcentajeIslr"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="PorcentajeIslr" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Cotiza SSO
	            	            							</td>
		                        							<td></f:verbatim>
																<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.cotizaSso}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="cotizaSso"                		            								                   		        								                        		                                	
                		            								required="false"
    																title="Cotiza Sso">
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.listCotizaSso}" />				                        																									                        		
                        										</h:selectOneMenu >	
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Cotiza SPF
	            	            							</td>
		                        							<td></f:verbatim>
																<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.cotizaSpf}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="cotizaSpf"                		            								                   		        								                        		                                	
                		            								required="false"
    																title="Cotiza Spf">
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.listCotizaSpf}" />				                        																									                        		
                        										</h:selectOneMenu >	
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Cotiza FAOV
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.cotizaLph}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="cotizaLph"                		            								                   		        								                        		                                	
                		            								required="false"
    																title="Cotiza Lph">
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.listCotizaLph}" />				                        																									                        		
                        										</h:selectOneMenu >	
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Cotiza FJU
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.cotizaFju}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="cotizaFju"                		            								                   		        								                        		                                	
                		            								required="false"
    																title="Cotiza Fju">
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.listCotizaFju}" />				                        																									                        		
                        										</h:selectOneMenu >	
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Codigo Patronal
	            	            							</td>
		                        							<td></f:verbatim>
															<h:inputText
		    		                    							size="11"
	    	    		                							maxlength="10"
            	    		        								value="#{actualizarDatosTrabajadorForm.trabajador.codigoPatronal}"
                	    		    								readonly="#{!actualizarDatosTrabajadorForm.editing}"
                        											id="CodigoPatronal"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"                        											
			                        								onblur="javascript:fieldEmpty(this)">
								                        		</h:inputText>
                        											<h:message for="CodigoPatronal" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					
	        	            	    					
	        	            	    					<f:verbatim><tr>
	        	            	    						<td>
        	                									Tiene Retroactivo SSO(En pr�xima n�mina a cerrar)
	            	            							</td>
		                        							<td></f:verbatim>
																<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.hayRetroactivo}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="hayRetroactivo"                		            								                   		        								                        		                                	
                		            								required="false"
    																title="Tiene Retroactivo Aportes Legales ? ">
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.listHayRetroactivo}" />				                        																									                        		
                        										</h:selectOneMenu >	
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					
	        	            	    					
	        	            	                         <f:verbatim><tr>
    	                    								<td>
        	                									Lunes Retroactivo SSO(En pr�xima n�mina a cerrar)
	            	            							</td>
		                        							<td></f:verbatim>
															<h:inputText
		    		                    							size="3"
	    	    		                							maxlength="2"
            	    		        								value="#{actualizarDatosTrabajadorForm.trabajador.lunesRetroactivo}"
                	    		    								readonly="#{!actualizarDatosTrabajadorForm.editing}"
                        											id="lunesRetroactivo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)">
								                        		</h:inputText>
                        											<h:message for="LunesRetroactivo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					
	        	            	    					
	        	            	    					 <f:verbatim><tr>
    	                    								<td>
        	                									Lunes 1ra Quinc SSO(En el mes de ingreso)
	            	            							</td>
		                        							<td></f:verbatim>
															<h:inputText
		    		                    							size="3"
	    	    		                							maxlength="2"
            	    		        								value="#{actualizarDatosTrabajadorForm.trabajador.lunesPrimera}"
                	    		    								readonly="#{!actualizarDatosTrabajadorForm.editing}"
                        											id="lunesPrimera"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)">
								                        		</h:inputText>
                        											<h:message for="LunesPrimera" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					
	        	            	    					 <f:verbatim><tr>
    	                    								<td>
        	                									Lunes 2da Quinc SSO(En el mes de ingreso)
	            	            							</td>
		                        							<td></f:verbatim>
															<h:inputText
		    		                    							size="3"
	    	    		                							maxlength="2"
            	    		        								value="#{actualizarDatosTrabajadorForm.trabajador.lunesSegunda}"
                	    		    								readonly="#{!actualizarDatosTrabajadorForm.editing}"
                        											id="lunesSegunda"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)">
								                        		</h:inputText>
                        											<h:message for="LunesSegunda" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					
	        	            	    					
	        	            	    					
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Situacion Actual
	            	            							</td>
		                        							<td></f:verbatim>
																<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.situacion}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="situacion"                		            								                   		        								                        		                                	
                		            								required="false"
    																title="Situacion Actual">
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.listSituacion}" />				                        																									                        		
                        										</h:selectOneMenu >	
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Comisi�n Servicio
		                        							<td></f:verbatim>
															<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaComisionServicio"
                		        									value="#{actualizarDatosTrabajadorForm.trabajador.fechaComisionServicio}"
                    		    									readonly="#{!actualizarDatosTrabajadorForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="FechaComisionServicio" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Organismo Comisi�n Servicio
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="50"
	    	    		                							maxlength="60"
            	    		        								value="#{actualizarDatosTrabajadorForm.trabajador.organismoComisionServicio}"
                	    		    								readonly="#{!actualizarDatosTrabajadorForm.editing}"
                        											id="OrganismoComisionServicio"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																				<h:message for="OrganismoComisionServicio" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Encargaduria
		                        							<td></f:verbatim>
															<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaEncargaduria"
                		        									value="#{actualizarDatosTrabajadorForm.trabajador.fechaEncargaduria}"
                    		    									readonly="#{!actualizarDatosTrabajadorForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="FechaEncargaduria" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									C�digo N�mina (Encargado)
	            	            							</td>
		                        							<td></f:verbatim>
															<h:inputText
		    		                    							size="7"
	    	    		                							maxlength="7"
            	    		        								value="#{actualizarDatosTrabajadorForm.trabajador.codigoNominaReal}"
                	    		    								                    	    										readonly="#{!actualizarDatosTrabajadorForm.editing}"
                        											id="CodigoNominaReal"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)">
								                        		</h:inputText>
                        											<h:message for="CodigoNominaReal" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Dependencia (Real/Encargado)
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{actualizarDatosTrabajadorForm.selectDependenciaReal}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="DependenciaReal"
                		            								                    		        								                        		                                	required="false"
    																title="Dependencia Real">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.colDependenciaReal}" />
								                                    	                </h:selectOneMenu>																									<h:message for="DependenciaReal" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																							                        						<f:verbatim><tr>
    		                    							<td>
        		                								Manual Cargo (Encargado)
	            		            						</td>
    	            		        						<td></f:verbatim>
						                	        	                            	<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.selectManualCargoForCargoReal}"
                        	    	                        		disabled="#{!actualizarDatosTrabajadorForm.editing}"
	                    	            	                    	onchange="this.form.submit()"
				                        							id="ManualCargoForCargoReal"
																	valueChangeListener="#{actualizarDatosTrabajadorForm.changeManualCargoForCargoReal}"
        	                																											required="false"
																	immediate="false"
																	title="Cargo Real">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{actualizarDatosTrabajadorForm.colManualCargoForCargoReal}" />
																							</h:selectOneMenu>																					<f:verbatim></td>
														</tr></f:verbatim>
										                   <f:verbatim><tr>
    	                    								<td>
        	                									Cargo (Encargado)
	            	            							</td>
		                        							<td></f:verbatim>
										                        	                        	    <h:selectOneMenu value="#{actualizarDatosTrabajadorForm.selectCargoReal}"
		                        	                            	disabled="#{!actualizarDatosTrabajadorForm.editing}"
    		                        	                        	rendered="#{actualizarDatosTrabajadorForm.showCargoReal}"
				                        							id="CargoReal"
            		                        	                	immediate="false"
                		                        	            	required="false"
																	title="Cargo Real">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                            		                        		<f:selectItems value="#{actualizarDatosTrabajadorForm.colCargoReal}" />
							    	                                	                </h:selectOneMenu>																									<h:message for="CargoReal" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
	        	            	    					
	        	            	    					
	        	            	    						<td>
        	                									Tiene Jubilaci�n Planificada
	            	            							</td>
		                        							<td></f:verbatim>
																<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.jubilacionPlanificada}"
        		                                                	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="jubilacionPlanificada"                		            								                   		        								                        		                                	
                		            								required="false"
    																title="Tiene Jubilaci�n PLanificada ? ">
                                    		                    	<f:selectItems value="#{actualizarDatosTrabajadorForm.listJubilacionPlanificada}" />				                        																									                        		
                        										</h:selectOneMenu >	
                        																																
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					
	        	            	    					
	        	            	    					
	        	            	    					
														<f:verbatim><tr>
	        	            	    					
	        	            	    					
    	                    								<td>
        	                									F� de Vida
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	<h:selectOneMenu value="#{actualizarDatosTrabajadorForm.trabajador.feVida}"
        	                                        	        	disabled="#{!actualizarDatosTrabajadorForm.editing}"
            	                                        	    	immediate="false"
				                        							id="FeVida"
                	            									                    	        										                        	                                	required="false"
    	                        	                            	title="F� de Vida">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{actualizarDatosTrabajadorForm.listFeVida}" />
					    	            	                        </h:selectOneMenu>																				<h:message for="Regimen" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Fecha F� de Vida
	            	            							</td>
		                        							<td></f:verbatim>
															<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaFeVida"
                		        									value="#{actualizarDatosTrabajadorForm.trabajador.fechaFeVida}"
                    		    									readonly="#{!actualizarDatosTrabajadorForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="FechaFeVida" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																																																																																																																																																																																																						<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsTrabajador"
            										rendered="#{actualizarDatosTrabajadorForm.showData}">
		                        					<f:verbatim><table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{actualizarDatosTrabajadorForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!actualizarDatosTrabajadorForm.editing&&!actualizarDatosTrabajadorForm.deleting&&actualizarDatosTrabajadorForm.login.modificar&&!actualizarDatosTrabajadorForm.trabajadorEgresado}" />
	                        	                				
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{actualizarDatosTrabajadorForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{actualizarDatosTrabajadorForm.editing}" />
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{actualizarDatosTrabajadorForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>
