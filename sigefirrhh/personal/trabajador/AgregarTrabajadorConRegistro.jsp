<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{agregarTrabajadorConRegistroForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formConceptoFijo">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Agregar Trabajador con Registro
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/trabajador/AgregarTrabajadorConRegistro.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
	                        					image="/images/close.gif"
	                        					action="go_cancelOption"
	                        					immediate="true"                        					
	                        					onclick="javascript:return clickMade()"
	                	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchConceptoFijo"
								rendered="#{!agregarTrabajadorConRegistroForm.showData&&
									!agregarTrabajadorConRegistroForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{agregarTrabajadorConRegistroForm.findTrabajadorCedula}"
														onkeypress="javascript:return keyCheck(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{agregarTrabajadorConRegistroForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{agregarTrabajadorConRegistroForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{agregarTrabajadorConRegistroForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{agregarTrabajadorConRegistroForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{agregarTrabajadorConRegistroForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{agregarTrabajadorConRegistroForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{agregarTrabajadorConRegistroForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{agregarTrabajadorConRegistroForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{agregarTrabajadorConRegistroForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													<h:commandButton image="/images/find.gif" 
														action="#{agregarTrabajadorConRegistroForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{agregarTrabajadorConRegistroForm.showResultTrabajador&&
								!agregarTrabajadorConRegistroForm.showData&&
								!agregarTrabajadorConRegistroForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{agregarTrabajadorConRegistroForm.resultTrabajador}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{agregarTrabajadorConRegistroForm.findConceptoFijoByTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{agregarTrabajadorConRegistroForm.selectedTrabajador&&
								!agregarTrabajadorConRegistroForm.showData&&
								!agregarTrabajadorConRegistroForm.editing&&
								!agregarTrabajadorConRegistroForm.deleting}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{agregarTrabajadorConRegistroForm.trabajador}"
                            						action="#{agregarTrabajadorConRegistroForm.findConceptoFijoByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{agregarTrabajadorConRegistroForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
									<tr>
										<td>
											<table width="100%" class="bandtable">
    											<tr>
    												<td style="text-align: right"></f:verbatim>
            											<h:commandButton image="/images/add.gif"
            												action="#{agregarTrabajadorConRegistroForm.add}"
            												onclick="javascript:return clickMade()"
            												rendered="#{!agregarTrabajadorConRegistroForm.editing&&!agregarTrabajadorConRegistroForm.deleting&&agregarTrabajadorConRegistroForm.login.agregar}" />
                                            			<f:verbatim><img
                                                        	onclick="document.forms['formCancel'].elements['formCancel:abort'].click()"
                                                     		style="cursor:hand"
                                                            src="/images/cancel.gif" />
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultConceptoFijo"
								rendered="#{agregarTrabajadorConRegistroForm.showResult&&
								!agregarTrabajadorConRegistroForm.showData&&
								!agregarTrabajadorConRegistroForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{agregarTrabajadorConRegistroForm.result}"
                                                preserveDataModel="true"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{agregarTrabajadorConRegistroForm.selectConceptoFijo}"
                                						styleClass="listitem">
                                						<f:param name="idConceptoFijo" value="#{result.idConceptoFijo}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataConceptoFijo"
								rendered="#{agregarTrabajadorConRegistroForm.showData||agregarTrabajadorConRegistroForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!agregarTrabajadorConRegistroForm.editing&&!agregarTrabajadorConRegistroForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{agregarTrabajadorConRegistroForm.editing&&!agregarTrabajadorConRegistroForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{agregarTrabajadorConRegistroForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{agregarTrabajadorConRegistroForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
<h:form id="agregarTrabajadorConRegistroForm">
<h:inputHidden id="scrollx" value="#{agregarTrabajadorConRegistroForm.scrollx}" />
<h:inputHidden id="scrolly" value="#{agregarTrabajadorConRegistroForm.scrolly}" />
            							<f:subview 
            								id="dataConceptoFijo"
            								rendered="#{agregarTrabajadorConRegistroForm.showData}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{agregarTrabajadorConRegistroForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!agregarTrabajadorConRegistroForm.editing&&!agregarTrabajadorConRegistroForm.deleting&&agregarTrabajadorConRegistroForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{agregarTrabajadorConRegistroForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!agregarTrabajadorConRegistroForm.editing&&!agregarTrabajadorConRegistroForm.deleting&&agregarTrabajadorConRegistroForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{agregarTrabajadorConRegistroForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{agregarTrabajadorConRegistroForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{agregarTrabajadorConRegistroForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{agregarTrabajadorConRegistroForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{agregarTrabajadorConRegistroForm.editing}" />
                                        	        				
                                                                <f:verbatim><img
			                                                    	onclick="document.forms['formCancel'].elements['formCancel:abortUpdate'].click()"
			                                                    	style="cursor:hand"
            	                                                    src="/images/cancel.gif" />
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim><tr>
                        						<td>
            										Trabajador
                        						</td>
                        						<td></f:verbatim>
                        							<h:outputText value="#{agregarTrabajadorConRegistroForm.trabajador}" />
                        						<f:verbatim></td>
            								</tr></f:verbatim>
            								
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{agregarTrabajadorConRegistroForm.selectConceptoTipoPersonal}"
                                                    	disabled="#{!agregarTrabajadorConRegistroForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="false"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{agregarTrabajadorConRegistroForm.colConceptoTipoPersonal}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Horas/Dias/Cantidad
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="8"
	                        							maxlength="8"
                        								value="#{agregarTrabajadorConRegistroForm.conceptoFijo.unidades}"
                        								                        								readonly="#{!agregarTrabajadorConRegistroForm.editing}"
                        								id="Horas/Dias/Cantidad"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Monto
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="18"
	                        							maxlength="18"
                        								value="#{agregarTrabajadorConRegistroForm.conceptoFijo.monto}"
                        								                        								readonly="#{!agregarTrabajadorConRegistroForm.editing}"
                        								id="Monto"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                           
                        					 </f:verbatim>
                        					 <f:verbatim>
											<tr>
                        						<td colspan="4" align="right">
												</f:verbatim>				
													<h:commandButton value="Calcular" 
                                    					image="/images/calculate.gif"                                    					
                                    					action="#{agregarTrabajadorConRegistroForm.calcular}"
                                    					disabled="#{!agregarTrabajadorConRegistroForm.editing}"
                                    					onclick="javascript:return clickMade()"/>
                            	        		<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Estatus
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{agregarTrabajadorConRegistroForm.conceptoFijo.estatus}"
                                                    	disabled="#{!agregarTrabajadorConRegistroForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="false"
                                                    	title="Estatus">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{agregarTrabajadorConRegistroForm.listEstatus}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Frecuencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{agregarTrabajadorConRegistroForm.selectFrecuenciaTipoPersonal}"
                                                    	disabled="#{!agregarTrabajadorConRegistroForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="false"
														title="Frecuencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{agregarTrabajadorConRegistroForm.colFrecuenciaTipoPersonal}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
									</f:subview>
            							<f:subview 
            								id="commandsConceptoFijo"
            								rendered="#{agregarTrabajadorConRegistroForm.showData}">
                        				<f:verbatim><table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{agregarTrabajadorConRegistroForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!agregarTrabajadorConRegistroForm.editing&&!agregarTrabajadorConRegistroForm.deleting&&agregarTrabajadorConRegistroForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{agregarTrabajadorConRegistroForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!agregarTrabajadorConRegistroForm.editing&&!agregarTrabajadorConRegistroForm.deleting&&agregarTrabajadorConRegistroForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{agregarTrabajadorConRegistroForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{agregarTrabajadorConRegistroForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{agregarTrabajadorConRegistroForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{agregarTrabajadorConRegistroForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{agregarTrabajadorConRegistroForm.editing}" />	
	                                    			<f:verbatim><img
                                                    	onclick="document.forms['formCancel'].elements['formCancel:abortUpdate'].click()"
                                                 		style="cursor:hand"
	                                                    src="/images/cancel.gif" />
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<h:form id="formCancel">
    <h:commandButton
    	id="abort"
    	action="#{agregarTrabajadorConRegistroForm.abort}"
    	style="visibility:hidden"
    	type="submit" />
    <h:commandButton
    	id="abortUpdate"
    	action="#{agregarTrabajadorConRegistroForm.abortUpdate}"
    	style="visibility:hidden"
    	type="submit" />
</h:form>
</f:view>
</body>
</html>