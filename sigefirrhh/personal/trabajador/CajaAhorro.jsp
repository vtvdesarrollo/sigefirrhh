<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{cajaAhorroForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			
			<td width="570" valign="top">
<h:form id="formCajaAhorro">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Caja de Ahorros
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/trabajador/CajaAhorro.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchCajaAhorro"
								rendered="#{!cajaAhorroForm.showData&&
									!cajaAhorroForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{cajaAhorroForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cajaAhorroForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												</td>
											</tr>
                        					<f:verbatim><tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{cajaAhorroForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{cajaAhorroForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{cajaAhorroForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{cajaAhorroForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{cajaAhorroForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{cajaAhorroForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{cajaAhorroForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()"
														 />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{cajaAhorroForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{cajaAhorroForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{cajaAhorroForm.showResultTrabajador&&
								!cajaAhorroForm.showData&&
								!cajaAhorroForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{cajaAhorroForm.resultTrabajador}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{cajaAhorroForm.findCajaAhorroByTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{cajaAhorroForm.selectedTrabajador&&
								!cajaAhorroForm.showData&&
								!cajaAhorroForm.editing&&
								!cajaAhorroForm.deleting}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{cajaAhorroForm.trabajador}"
                            						action="#{cajaAhorroForm.findCajaAhorroByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{cajaAhorroForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
									<tr>
										<td>
											<table width="100%" class="bandtable">
    											<tr>
    												<td style="text-align: right"></f:verbatim>
            											<h:commandButton image="/images/add.gif"
            												action="#{cajaAhorroForm.add}"
            												onclick="javascript:return clickMade()"
            												rendered="#{!cajaAhorroForm.editing&&!cajaAhorroForm.deleting&&cajaAhorroForm.login.agregar&&!cajaAhorroForm.trabajadorEgresado}" />
            											<h:commandButton value="Cancelar" 
	                                    					image="/images/cancel.gif"
	                        								action="#{cajaAhorroForm.abort}"
	                        								immediate="true"
	                        								onclick="javascript:return clickMade()"
	                	        							/>
                                            			<f:verbatim>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultCajaAhorro"
								rendered="#{cajaAhorroForm.showResult&&
								!cajaAhorroForm.showData&&
								!cajaAhorroForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{cajaAhorroForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{cajaAhorroForm.selectCajaAhorro}"
                                						styleClass="listitem">
                                						<f:param name="idCajaAhorro" value="#{result.idCajaAhorro}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataCajaAhorro"
								rendered="#{cajaAhorroForm.showData||cajaAhorroForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!cajaAhorroForm.editing&&!cajaAhorroForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{cajaAhorroForm.editing&&!cajaAhorroForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{cajaAhorroForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{cajaAhorroForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
<h:form id="cajaAhorroForm">
<h:inputHidden id="scrollx" value="#{cajaAhorroForm.scrollx}" />
<h:inputHidden id="scrolly" value="#{cajaAhorroForm.scrolly}" />
            							<f:subview 
            								id="dataCajaAhorro"
            								rendered="#{cajaAhorroForm.showData}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{cajaAhorroForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!cajaAhorroForm.editing&&!cajaAhorroForm.deleting&&cajaAhorroForm.login.modificar&&!cajaAhorroForm.trabajadorEgresado}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{cajaAhorroForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!cajaAhorroForm.editing&&!cajaAhorroForm.deleting&&cajaAhorroForm.login.eliminar&&!cajaAhorroForm.trabajadorEgresado}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{cajaAhorroForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{cajaAhorroForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{cajaAhorroForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{cajaAhorroForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{cajaAhorroForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{cajaAhorroForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim><tr>
                        						<td>
            										Trabajador
                        						</td>
                        						<td></f:verbatim>
                        							<h:outputText value="#{cajaAhorroForm.trabajador}" />
                        						<f:verbatim></td>
            								</tr></f:verbatim>
            								<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{cajaAhorroForm.selectConceptoTipoPersonal}"
                                                    	disabled="#{!cajaAhorroForm.editing}"
                                                    	immediate="false"
                        								required="false"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cajaAhorroForm.colConceptoTipoPersonal}" />
                                                    </h:selectOneMenu> 
                                                   <f:verbatim></td>
                        					</tr>  </f:verbatim>
            								
            								<f:verbatim><tr>
                        						<td>
                        							% Trabajador
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="8"
	                        							maxlength="6"
                        								value="#{cajaAhorroForm.cajaAhorro.porcentajeTrabajador}"                        								
                        								readonly="#{!cajaAhorroForm.editing}"
                        								id="PorcentajeTrabajador"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim></td>
                        					</tr> </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							% Patron
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="8"
	                        							maxlength="6"
                        								value="#{cajaAhorroForm.cajaAhorro.porcentajePatron}"                        								
                        								readonly="#{!cajaAhorroForm.editing}"
                        								id="PorcentajePatron"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim></td>
                        					</tr> </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Aporte Trabajador
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="12"
	                        							maxlength="10"
                        								value="#{cajaAhorroForm.cajaAhorro.aporteTrabajador}"                        								
                        								readonly="#{!cajaAhorroForm.editing}"
                        								id="AporteTrabajador"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim></td>
                        					</tr> </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Aporte Patron
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="12"
	                        							maxlength="10"
                        								value="#{cajaAhorroForm.cajaAhorro.aportePatron}"                        								
                        								readonly="#{!cajaAhorroForm.editing}"
                        								id="AportePatron"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim></td>
                        					</tr> </f:verbatim>
            								
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Acumulado Trabajador
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							maxlength="15"
                        								value="#{cajaAhorroForm.cajaAhorro.acumuladoTrabajador}"                        								
                        								readonly="#{!cajaAhorroForm.editing}"
                        								id="AcumuladoTrabajador"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim></td>
                        					</tr> </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Acumulado Patron
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							maxlength="15"
                        								value="#{cajaAhorroForm.cajaAhorro.acumuladoPatron}"                        								
                        								readonly="#{!cajaAhorroForm.editing}"
                        								id="AcumuladoPatron"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim></td>
                        					</tr> </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Acumulado Retiros
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							maxlength="15"
                        								value="#{cajaAhorroForm.cajaAhorro.acumuladoRetiros}"                        								
                        								readonly="#{!cajaAhorroForm.editing}"
                        								id="AcumuladoRetiros"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim></td>
                        					</tr> </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Acumulado Haberes
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							maxlength="15"
                        								value="#{cajaAhorroForm.cajaAhorro.acumuladoHaberes}"                        								
                        								readonly="#{!cajaAhorroForm.editing}"
                        								id="AcumuladoHaberes"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim></td>
                        					</tr> </f:verbatim>
                        				                        					                       					
                        					
									</f:subview>
            							<f:subview 
            								id="commandsCajaAhorro"
            								rendered="#{cajaAhorroForm.showData}">
                        				<f:verbatim><table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{cajaAhorroForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!cajaAhorroForm.editing&&!cajaAhorroForm.deleting&&cajaAhorroForm.login.modificar&&!cajaAhorroForm.trabajadorEgresado}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{cajaAhorroForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!cajaAhorroForm.editing&&!cajaAhorroForm.deleting&&cajaAhorroForm.login.eliminar&&!cajaAhorroForm.trabajadorEgresado}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{cajaAhorroForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{cajaAhorroForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{cajaAhorroForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{cajaAhorroForm.save}"
                            	        				rendered="#{cajaAhorroForm.editing}" />	
                            	        			<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{cajaAhorroForm.abortUpdate}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>