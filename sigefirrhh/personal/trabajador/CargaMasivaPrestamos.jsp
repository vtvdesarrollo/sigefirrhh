<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">

	<jsp:include page="/inc/functions.jsp" />
			
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{cargaMasivaPrestamosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formCargaMasivaPrestamos" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Carga Masiva de Prestamos
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/conceptos/CargaMasivaPrestamos.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
            					image="/images/close.gif"
            					action="go_cancelOption"
            					immediate="true"                        					
            					onclick="javascript:return clickMade()"
    	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">   
        														    																	
        								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td>
    										<h:selectOneMenu value="#{cargaMasivaPrestamosForm.idTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{cargaMasivaPrestamosForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{cargaMasivaPrestamosForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td>
    										<h:selectOneMenu
    											value="#{cargaMasivaPrestamosForm.idConcepto}"
                                            	immediate="true"
												rendered="#{cargaMasivaPrestamosForm.showConcepto}">
                                                <f:selectItems value="#{cargaMasivaPrestamosForm.listConcepto}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Frecuencia
    									</td>
    									<td>
    										<h:selectOneMenu
    											value="#{cargaMasivaPrestamosForm.idFrecuenciaTipoPersonal}"
                                            	immediate="true"
												rendered="#{cargaMasivaPrestamosForm.showFrecuenciaTipoPersonal}">
                                                <f:selectItems value="#{cargaMasivaPrestamosForm.listFrecuenciaTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								    								
    								<f:subview id="agregar" rendered="#{!cargaMasivaPrestamosForm.showEliminar}">
        								<f:verbatim>
        								<tr>
        									<td colspan="2">
        										<table width="100%" class="datatable">
        											<tr>
        												<td>
        													C�dula
        												</td>
        												<td>
        													Monto Prestamo
        												</td>
    													<td>
    														Monto Cuota
    													</td>
    													<td>
    														Fecha Otorgo
    													</td>    													    													<td>
    														Fecha Comienzo
    													</td>
    													
        											</tr>
        											<tr>
        												<td></f:verbatim>
    						                        		<h:inputText
    		                        							size="8"
    		                        							id="Cedula"
    		                        							maxlength="8"
    	                        								value="#{cargaMasivaPrestamosForm.cedula}"   						
    															onfocus="return deleteZero(event, this)"
    															onblur="javascript:fieldEmpty(this)"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyIntegerCheck(event, this)" />
    													<f:verbatim>		
        												</td>
        												<td>
        												</f:verbatim>
    						                        		<h:inputText
    		                        							size="6"
    		                        							id="MontoPrestamo"
    		                        							maxlength="6"
    	                        								value="#{cargaMasivaPrestamosForm.montoPrestamo}"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyFloatCheck(event, this)" >
    															<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
    															</h:inputText>
    													<f:verbatim>
        												</td>
    													<td>
    													</f:verbatim>
    						                        		<h:inputText
    		                        							size="12"
    		                        							id="MontoCuota"
    		                        							maxlength="12"
    	                        								value="#{cargaMasivaPrestamosForm.montoCuota}"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyFloatCheck(event, this)" >
    															<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
    														</h:inputText>
    			    									<f:verbatim>	
    													</td>
    													<td>
    													</f:verbatim>
    						                        		<h:inputText
																id="FechaOtorgo"
																size="10"
																maxlength="10"
																value="#{cargaMasivaPrestamosForm.fechaOtorgo}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)"
																onblur="javascript:check_date(this)"
																required="true">
																<f:convertDateTime timeZone="#{dateTime.timeZone}" 
																pattern="dd-MM-yyyy" />
																</h:inputText>
    			    									<f:verbatim>	
    													</td>	
    													<td>
    													</f:verbatim>
    						                        		<h:inputText
																id="FechaComienzo"
																size="10"
																maxlength="10"
																value="#{cargaMasivaPrestamosForm.fechaComienzo}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)"
																onblur="javascript:check_date(this)"
																required="true">
																<f:convertDateTime timeZone="#{dateTime.timeZone}" 
																pattern="dd-MM-yyyy" />
																</h:inputText>
    			    									<f:verbatim>	
    													</td>	
    													<td>
    													</f:verbatim>
    														<h:commandButton value="Agregar"
    			    											image="/images/add.gif"
        														action="#{cargaMasivaPrestamosForm.agregar}" />
        												<f:verbatim>	
    													</td>
    													
        											</tr>
        											<tr>
        												<td>
        												</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaMasivaPrestamosForm.cedulaFinal}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{cargaMasivaPrestamosForm.nombre}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{cargaMasivaPrestamosForm.apellido}" />
    	                        						<f:verbatim>
        												</td>
        												<td>
        												</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaMasivaPrestamosForm.montoPrestamoFinal}"
    	                        								 />
    	                        						<f:verbatim>
        												</td>
    													<td>
    													</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaMasivaPrestamosForm.montoCuotaFinal}"
    	                        								 />
    													<f:verbatim>
    													</td>    													    													<td>
    													</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaMasivaPrestamosForm.numeroCuotas}"
    	                        								 />
    													<f:verbatim>
    													</td>    													
    													
        											</tr>
        										</table>
        									</td>
        									</f:verbatim>
    									</f:subview>
    									
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>