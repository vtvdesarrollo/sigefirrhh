<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{actualizarEstatusFeVidaTrabajadorForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />
	  				
				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top" width="100%">
							<h:form id="formActualizarEstatusFeVidaTrabajador">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Actualizar Estatus y F� de Vida
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/jubilacion/ActualizarEstatusFeVidaTrabajador.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
											
											<h:commandButton value="Cerrar" 
                        					image="/images/close.gif"
                        					action="go_cancelOption"
                        					immediate="true"                        					
                        					onclick="javascript:return clickMade()"
                	        				 />
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									
									<f:subview 
										id="searchTrabajador"
										rendered="#{!actualizarEstatusFeVidaTrabajadorForm.showData&&
										!actualizarEstatusFeVidaTrabajadorForm.showAdd}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable">
        		                						<tr>
		                        						<td>
		                        							Tipo de Personal
		                        						</td>
		                        						<td></f:verbatim>
							                            	<h:selectOneMenu value="#{actualizarEstatusFeVidaTrabajadorForm.findSelectTrabajadorIdTipoPersonal}"
							                            		id="colTipoPersonal"
		                                                    	immediate="false">
		                                                    	<f:selectItem itemLabel="Seleccione" itemValue="0" />																
		                                                        <f:selectItems value="#{actualizarEstatusFeVidaTrabajadorForm.findColTipoPersonal}" />
		                                                    </h:selectOneMenu>
															
														<f:verbatim></td>
													</tr>
        		                					<tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{actualizarEstatusFeVidaTrabajadorForm.findTrabajadorCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
																onfocus="return deleteZero(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{actualizarEstatusFeVidaTrabajadorForm.findTrabajadorByCedula}" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarEstatusFeVidaTrabajadorForm.findTrabajadorPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarEstatusFeVidaTrabajadorForm.findTrabajadorSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarEstatusFeVidaTrabajadorForm.findTrabajadorPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarEstatusFeVidaTrabajadorForm.findTrabajadorSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{actualizarEstatusFeVidaTrabajadorForm.findTrabajadorByNombresApellidos}" />
														<f:verbatim></td>
													</tr>
													<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{actualizarEstatusFeVidaTrabajadorForm.findTrabajadorCodigoNomina}"
														onfocus="return deleteZero(event, this)"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
																action="#{actualizarEstatusFeVidaTrabajadorForm.findTrabajadorByCodigoNomina}"
																onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultTrabajador"
									rendered="#{actualizarEstatusFeVidaTrabajadorForm.showResultTrabajador&&
									!actualizarEstatusFeVidaTrabajadorForm.showData&&
									!actualizarEstatusFeVidaTrabajadorForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <h:dataTable id="resultTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{actualizarEstatusFeVidaTrabajadorForm.resultTrabajador}"
                                               
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{actualizarEstatusFeVidaTrabajadorForm.selectTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="resultTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="resultTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataTrabajador"
								rendered="#{actualizarEstatusFeVidaTrabajadorForm.showData||actualizarEstatusFeVidaTrabajadorForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!actualizarEstatusFeVidaTrabajadorForm.editing&&!actualizarEstatusFeVidaTrabajadorForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{actualizarEstatusFeVidaTrabajadorForm.editing&&!actualizarEstatusFeVidaTrabajadorForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{actualizarEstatusFeVidaTrabajadorForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{actualizarEstatusFeVidaTrabajadorForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="actualizarEstatusFeVidaTrabajadorForm">
	    	        							<f:subview 
    	    	    								id="dataTrabajador"
        	    									rendered="#{actualizarEstatusFeVidaTrabajadorForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="3">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{actualizarEstatusFeVidaTrabajadorForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!actualizarEstatusFeVidaTrabajadorForm.editing&&!actualizarEstatusFeVidaTrabajadorForm.deleting&&actualizarEstatusFeVidaTrabajadorForm.login.modificar&&!actualizarEstatusFeVidaTrabajadorForm.trabajadorEgresado}" />
                    	                        	        				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{actualizarEstatusFeVidaTrabajadorForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{actualizarEstatusFeVidaTrabajadorForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{actualizarEstatusFeVidaTrabajadorForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td width="30%">
            													Trabajador
                        									</td colspan="2">
	                        								<td width="70%"></f:verbatim>
	    	                    								<h:outputText value="#{actualizarEstatusFeVidaTrabajadorForm.trabajador.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
														
													
														<f:verbatim><tr>
    	                    								<td>
        	                									Estatus
	            	            							</td>
		                        							<td colspan="2"></f:verbatim>
																<h:selectOneMenu value="#{actualizarEstatusFeVidaTrabajadorForm.trabajador.estatus}"
        		                                                	disabled="#{!actualizarEstatusFeVidaTrabajadorForm.editing}"
        		                                                	onchange="this.form.submit()"  
                                            						valueChangeListener="#{actualizarEstatusFeVidaTrabajadorForm.changeEstatus}"
            		                                            	immediate="false"
				                        							id="estatus"                		            								                   		        								                        		                                	                		            								
    																title="Estatus">
                                    		                    	<f:selectItems value="#{actualizarEstatusFeVidaTrabajadorForm.listEstatus}" />				                        																									                        		
                        										</h:selectOneMenu >	
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>																	                    	    		
																	                    	    					
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									F� de Vida
	            	            							</td>
		                        							<td colspan="2"></f:verbatim>
				    	                                        	<h:selectOneMenu value="#{actualizarEstatusFeVidaTrabajadorForm.trabajador.feVida}"
        	                                        	        	disabled="#{!actualizarEstatusFeVidaTrabajadorForm.editing}"
            	                                        	    	immediate="false"
				                        							id="FeVida"
                	            									                    	        										                        	                                	required="false"
    	                        	                            	title="F� de Vida">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{actualizarEstatusFeVidaTrabajadorForm.listFeVida}" />
					    	            	                        </h:selectOneMenu>																				<h:message for="FeVida" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Fecha F� de Vida
	            	            							</td>
		                        							<td colspan="2"></f:verbatim>
															<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaFeVida"
                		        									value="#{actualizarEstatusFeVidaTrabajadorForm.trabajador.fechaFeVida}"
                    		    									readonly="#{!actualizarEstatusFeVidaTrabajadorForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="FechaFeVida" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:subview 
		    	    	    								id="dataRetroactivo"
		        	    									rendered="#{actualizarEstatusFeVidaTrabajadorForm.showRetroactivo}">
		        	    								<f:verbatim><tr>
    	                    								<td>
        	                									�Pagar Retroactivo?
	            	            							</td>
		                        							<td colspan="2"></f:verbatim>
				    	                                        	<h:selectOneMenu value="#{actualizarEstatusFeVidaTrabajadorForm.pagarRetroactivo}"
        	                                        	        	disabled="#{!actualizarEstatusFeVidaTrabajadorForm.editing}"
            	                                        	    	immediate="false"
				                        							id="PagarRetroactivo"
                	            									onchange="this.form.submit()"  
                                            						valueChangeListener="#{actualizarEstatusFeVidaTrabajadorForm.changePagarRetroactivo}"                   	        										                        	                                	required="false"
    	                        	                            	title="Pagar Retroactivo">
    	                        	                            	<f:selectItem itemLabel="No" itemValue="N" />
    																<f:selectItem itemLabel="Si" itemValue="S" />
    																

					    	            	                        </h:selectOneMenu>																				
        	            	    							<f:verbatim></td>
	        	            	    					</tr>
	        	            	    					<tr>
					    									<td>
					    										Concepto
					    									</td></f:verbatim>
					    									<td colspan="2">
					    										<h:selectOneMenu value="#{actualizarEstatusFeVidaTrabajadorForm.idConceptoTipoPersonal}"
					    											rendered="#{actualizarEstatusFeVidaTrabajadorForm.showConceptoFrecuencia}"
					                                            	immediate="false">
					                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
					                                                <f:selectItems value="#{actualizarEstatusFeVidaTrabajadorForm.listConceptoTipoPersonal}" />
					                                            </h:selectOneMenu>
					    									<f:verbatim></td>
					    								</tr>
					    								<tr>
					    									<td>
					    										Frecuencia
					    									</td></f:verbatim>
					    									<td colspan="2">
					    										<h:selectOneMenu value="#{actualizarEstatusFeVidaTrabajadorForm.idFrecuenciaTipoPersonal}"
					    											rendered="#{actualizarEstatusFeVidaTrabajadorForm.showConceptoFrecuencia}"
					                                            	immediate="false">
					                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
					                                                <f:selectItems value="#{actualizarEstatusFeVidaTrabajadorForm.listFrecuenciaTipoPersonal}" />
					                                            </h:selectOneMenu>
					    									<f:verbatim></td>
					    								</tr>
					    								<tr>
					    									<td>
					    										D�as a Pagar
					    									</td></f:verbatim>
					    									<td>    									
					    										<h:inputText				    										
					    											size="20"
					    											id="Dias"
					    											maxlength="20"
					    											rendered="#{actualizarEstatusFeVidaTrabajadorForm.showConceptoFrecuencia}"
					    											value="#{actualizarEstatusFeVidaTrabajadorForm.dias}"
					    											required="false" >
					    											<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>  										
																	</h:inputText>    										
					    									<f:verbatim></td>
					    									<td align="right">
																</f:verbatim>				
																	<h:commandButton value="Calcular" 
				                                    					image="/images/calculate.gif"                                    					
				                                    					action="#{actualizarEstatusFeVidaTrabajadorForm.calcular}"
				                                    					disabled="#{!actualizarEstatusFeVidaTrabajadorForm.editing}"
				                                    					onclick="javascript:return clickMade()"/>
				                            	        		<f:verbatim>
															</td>
					    								</tr>
					    								<tr>
					    									<td width="100%">
					    										Monto Retroactivo
					    									</td></f:verbatim>
					    									<td colspan="2">    									
					    										<h:outputText				    										
					    											
					    											id="MontoConcepto"
					    											
					    											rendered="#{actualizarEstatusFeVidaTrabajadorForm.showConceptoFrecuencia}"
					    											value="#{actualizarEstatusFeVidaTrabajadorForm.montoConcepto}"
					    											>
					    											<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>  										
																	</h:outputText>    										
					    									<f:verbatim></td>
					    								</tr></f:verbatim>
														</f:subview>																																																																																																																																																																																																																								<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsTrabajador"
            										rendered="#{actualizarEstatusFeVidaTrabajadorForm.showData}">
		                        					<f:verbatim></table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{actualizarEstatusFeVidaTrabajadorForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!actualizarEstatusFeVidaTrabajadorForm.editing&&!actualizarEstatusFeVidaTrabajadorForm.deleting&&actualizarEstatusFeVidaTrabajadorForm.login.modificar&&!actualizarEstatusFeVidaTrabajadorForm.trabajadorEgresado}" />
	                        	                				
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{actualizarEstatusFeVidaTrabajadorForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{actualizarEstatusFeVidaTrabajadorForm.editing}" />
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{actualizarEstatusFeVidaTrabajadorForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>