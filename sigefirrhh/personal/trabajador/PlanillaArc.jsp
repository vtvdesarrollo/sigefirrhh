<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{planillaArcForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formPlanillaArc">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Planilla ARC
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/trabajador/PlanillaArc.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchPlanillaArc"
								rendered="#{!planillaArcForm.showData&&
									!planillaArcForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{planillaArcForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{planillaArcForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												</td>
											</tr>
                        					<f:verbatim><tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{planillaArcForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{planillaArcForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{planillaArcForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{planillaArcForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{planillaArcForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{planillaArcForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{planillaArcForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{planillaArcForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{planillaArcForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{planillaArcForm.showResultTrabajador&&
								!planillaArcForm.showData&&
								!planillaArcForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{planillaArcForm.resultTrabajador}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{planillaArcForm.findPlanillaArcByTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{planillaArcForm.selectedTrabajador&&
								!planillaArcForm.showData&&
								!planillaArcForm.editing&&
								!planillaArcForm.deleting}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{planillaArcForm.trabajador}"
                            						action="#{planillaArcForm.findPlanillaArcByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{planillaArcForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultPlanillaArc"
								rendered="#{planillaArcForm.showResult&&
								!planillaArcForm.showData&&
								!planillaArcForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{planillaArcForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{planillaArcForm.selectPlanillaArc}"
                                						styleClass="listitem">
                                						<f:param name="idPlanillaArc" value="#{result.idPlanillaArc}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataPlanillaArc"
								rendered="#{planillaArcForm.showData||planillaArcForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!planillaArcForm.editing&&!planillaArcForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{planillaArcForm.editing&&!planillaArcForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{planillaArcForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{planillaArcForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
<h:form id="planillaArcForm">
<h:inputHidden id="scrollx" value="#{planillaArcForm.scrollx}" />
<h:inputHidden id="scrolly" value="#{planillaArcForm.scrolly}" />
            							<f:subview 
            								id="dataPlanillaArc"
            								rendered="#{planillaArcForm.showData}">
											<f:verbatim>
											<table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="4">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{planillaArcForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!planillaArcForm.editing&&!planillaArcForm.deleting&&planillaArcForm.login.modificar&&!planillaArcForm.trabajadorEgresado}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{planillaArcForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{planillaArcForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{planillaArcForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{planillaArcForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{planillaArcForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{planillaArcForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim>
											<tr>
                        						<td>
            										Trabajador
                        						</td>
                        						<td colspan="3">
													</f:verbatim>
                        								<h:outputText value="#{planillaArcForm.trabajador}" />
                        							<f:verbatim>
												</td>
            								</tr>
											</f:verbatim>
            								
                        					<f:verbatim>
											<tr>
                        						<td>
                        							A�o
                        						</td>
                        						<td colspan="3">
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.anio}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="Anio"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
														 >
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td width="25%"></td>
                        						<td width="25%">Devengado</td>
												<td width="25%">Porcentaje</td>
												<td width="25%">Retencion</td>
                        					</tr>
											</f:verbatim>
                        					<f:verbatim>
											<tr>
                        						<td>
                        							Enero
                        						</td>
                        						<td>
													</f:verbatim>
														<h:inputText
															size="10"
															maxlength="10"
															value="#{planillaArcForm.planillaArc.devengadoEnero}"
															readonly="#{!planillaArcForm.editing}"
															id="DevEnero"
															required="false"
															title="CampoX"
															alt="CampoX"
															onchange="javascript:this.value=this.value.toUpperCase()"
															onkeypress="return keyFloatCheck(event, this)"
															 style="text-align:right"  >
															<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
														</h:inputText>
													<f:verbatim>
												</td>
												<td>
													</f:verbatim>
														<h:inputText
															size="10"
															maxlength="10"
															value="#{planillaArcForm.planillaArc.porcentajeEnero}"
															readonly="#{!planillaArcForm.editing}"
															id="PorcentajeenEnero"
															required="false"
															title="CampoX"
															alt="CampoX"
															onchange="javascript:this.value=this.value.toUpperCase()"
															onkeypress="return keyFloatCheck(event, this)"
															 style="text-align:right"  >
															<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
														</h:inputText>
													<f:verbatim>
												</td>
												<td>
													</f:verbatim>
														<h:inputText
															size="10"
															maxlength="10"
															value="#{planillaArcForm.planillaArc.retencionEnero}"
															readonly="#{!planillaArcForm.editing}"
															id="RetencionEnero"
															required="false"
															title="CampoX"
															alt="CampoX"
															onchange="javascript:this.value=this.value.toUpperCase()"
															onkeypress="return keyFloatCheck(event, this)"
															 style="text-align:right"  >
															<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
														</h:inputText>
													<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
                        					<f:verbatim>
											<tr>
                        						<td>Febrero</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoFebrero}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevFebrero"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeFebrero}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenFebrero"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionFebrero}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionFebrero"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
                        					<f:verbatim>
											<tr>
                        						<td>Marzo</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoMarzo}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevMarzo"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeMarzo}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenMarzo"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionMarzo}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionMarzo"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>Abril</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoAbril}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevAbril"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeAbril}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenAbril"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionAbril}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionAbril"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>Mayo</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoMayo}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevMayo"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeMayo}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenMayo"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionMayo}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionMayo"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>Junio</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoJunio}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevJunio"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeJunio}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenJunio"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionJunio}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionJunio"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>Julio</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoJulio}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevJulio"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeJulio}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenJulio"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionJulio}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionJulio"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>Agosto</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoAgosto}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevAgosto"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeAgosto}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenAgosto"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionAgosto}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionAgosto"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>Septiembre</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoSeptiembre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevSeptiembre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeSeptiembre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenSeptiembre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionSeptiembre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionSeptiembre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>Octubre</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoOctubre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevOctubre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeOctubre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenOctubre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionOctubre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionOctubre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>Noviembre</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoNoviembre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevNoviembre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeNoviembre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenNoviembre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionNoviembre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionNoviembre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>Diciembre</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.devengadoDiciembre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="DevDiciembre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.porcentajeDiciembre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="PorcentajeenDiciembre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        						<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.planillaArc.retencionDiciembre}"
                        								readonly="#{!planillaArcForm.editing}"
                        								id="RetencionDiciembre"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
														 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td colspan="4" height="10"></td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>TOTAL</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.totalDevengado}"
                        								readonly="true"
                        								id="TotalDevengado"
                        								required="false"
                        								title="Total Devengado"
                        								alt="Total Devengado"                        								
														 style="text-align:right"
														 onkeypress="return keyEnterCheck(event, this)"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td></td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{planillaArcForm.totalRetencion}"
                        								readonly="true"
                        								id="TotalRetencion"
                        								required="false"
                        								title="Total Retencion"
                        								alt="Total Retencion"                        								
														 style="text-align:right"
														 onkeypress="return keyEnterCheck(event, this)"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td colspan="4" align="right">
												</f:verbatim>				
													<h:commandButton value="Calcular" 
                                    					image="/images/calculate.gif"                                    					
                                    					action="this.form.submit()"
                                    					onclick="javascript:return clickMade()"/>
                            	        		<f:verbatim>
												</td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td height="2" colspan="4" class="bandtable"></td>
                        					</tr>
											</f:verbatim>
											
                        					
									</f:subview>
            							<f:subview 
            								id="commandsPlanillaArc"
            								rendered="#{planillaArcForm.showData}">
                        				<f:verbatim><table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{planillaArcForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!planillaArcForm.editing&&!planillaArcForm.deleting&&!planillaArcForm.login.modificar&&!planillaArcForm.trabajadorEgresado}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{planillaArcForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{planillaArcForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{planillaArcForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{planillaArcForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{planillaArcForm.editing}" />	
                            	        			<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{planillaArcForm.abortUpdate}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>