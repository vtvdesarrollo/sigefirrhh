<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{embargoConceptoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />
				<%
	  			}else { %>
	  				 
	  				
				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formEmbargoConcepto">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Detalle de Embargos al Trabajador
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/trabajador/EmbargoConcepto.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success"  />
										</td>
									</tr>
									
									<f:subview 
										id="searchEmbargoConcepto"
										rendered="#{!embargoConceptoForm.showData&&
										!embargoConceptoForm.showAdd&&
										!embargoConceptoForm.login.servicioPersonal}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{embargoConceptoForm.findPersonalCedula}"
																onkeypress="javascript:return keyIntegerCheck(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{embargoConceptoForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{embargoConceptoForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{embargoConceptoForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{embargoConceptoForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{embargoConceptoForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{embargoConceptoForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{embargoConceptoForm.showResultPersonal&&
									!embargoConceptoForm.showData&&
									!embargoConceptoForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <x:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{embargoConceptoForm.resultPersonal}"
    	                                            preserveDataModel="false"
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{embargoConceptoForm.findEmbargoConceptoByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </x:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{embargoConceptoForm.selectedPersonal&&
									!embargoConceptoForm.showData&&
									!embargoConceptoForm.editing&&
									!embargoConceptoForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{embargoConceptoForm.personal}   <-- clic aqui"
    	                            						action="#{embargoConceptoForm.findEmbargoConceptoByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{embargoConceptoForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{embargoConceptoForm.add}"
            													rendered="#{!embargoConceptoForm.editing&&!embargoConceptoForm.deleting&&embargoConceptoForm.login.agregar}" />

                                        	    			<% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{embargoConceptoForm.abort}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                                                    	    <%}%>
                                                    	   <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultEmbargoConcepto"
									rendered="#{embargoConceptoForm.showResult&&
									!embargoConceptoForm.showData&&
									!embargoConceptoForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <x:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{embargoConceptoForm.result}"
                                            	    preserveDataModel="false"
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{embargoConceptoForm.selectEmbargoConcepto}"
	                                						styleClass="listitem">
    		                            						<f:param name="idEmbargoConcepto" value="#{result.idEmbargoConcepto}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </x:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataEmbargoConcepto"
								rendered="#{embargoConceptoForm.showData||embargoConceptoForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!embargoConceptoForm.editing&&!embargoConceptoForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{embargoConceptoForm.editing&&!embargoConceptoForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{embargoConceptoForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{embargoConceptoForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="embargoConceptoForm">
	    	        							<f:subview 
    	    	    								id="dataEmbargoConcepto"
        	    									rendered="#{embargoConceptoForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{embargoConceptoForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!embargoConceptoForm.editing&&!embargoConceptoForm.deleting&&embargoConceptoForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{embargoConceptoForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!embargoConceptoForm.editing&&!embargoConceptoForm.deleting&&embargoConceptoForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{embargoConceptoForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{embargoConceptoForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{embargoConceptoForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{embargoConceptoForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{embargoConceptoForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{embargoConceptoForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{embargoConceptoForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
																				                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Embargo
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{embargoConceptoForm.selectEmbargo}"
        		                                                	disabled="#{!embargoConceptoForm.editing}"
            		                                            	immediate="false"
				                        							id="embargo"
                		            								                    		        								                        		                                	required="false"
    																title="Embargo">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{embargoConceptoForm.colEmbargo}" />
								                                    	                </h:selectOneMenu>																									<h:message for="embargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Concepto
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{embargoConceptoForm.selectConcepto}"
        		                                                	disabled="#{!embargoConceptoForm.editing}"
            		                                            	immediate="false"
				                        							id="concepto"
                		            								                    		        								                        		                                	required="false"
    																title="Concepto">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{embargoConceptoForm.colConcepto}" />
								                                    	                </h:selectOneMenu>																									<h:message for="concepto" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Apellidos y Nombes del Beneficiario
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="50"
	    	    		                							maxlength="50"
            	    		        								value="#{embargoConceptoForm.embargoConcepto.nombre}"
                	    		    								                    	    										readonly="#{!embargoConceptoForm.editing}"
                        											id="nombre"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        			                        								<f:verbatim><span class="required"> *</span></f:verbatim>
                        																																<h:message for="nombre" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�dula Beneficiario
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{embargoConceptoForm.embargoConcepto.cedulaBeneficiario}"
                	    		    								                    	    										readonly="#{!embargoConceptoForm.editing}"
                        											id="cedulaBeneficiario"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="cedulaBeneficiario" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									RIF Beneficiario
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="15"
	    	    		                							maxlength="15"
            	    		        								value="#{embargoConceptoForm.embargoConcepto.rifBeneficiario}"
                	    		    								                    	    										readonly="#{!embargoConceptoForm.editing}"
                        											id="rifBeneficiario"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="rifBeneficiario" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Porcentaje a embargar
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{embargoConceptoForm.embargoConcepto.porcentaje}"
                	    		    								                    	    										readonly="#{!embargoConceptoForm.editing}"
                        											id="porcentaje"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="porcentaje" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Monto a embargar
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="15"
	    	    		                							maxlength="15"
            	    		        								value="#{embargoConceptoForm.embargoConcepto.monto}"
                	    		    								                    	    										readonly="#{!embargoConceptoForm.editing}"
                        											id="monto"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="monto" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Cuenta Beneficiario
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="20"
	    	    		                							maxlength="20"
            	    		        								value="#{embargoConceptoForm.embargoConcepto.cuenta}"
                	    		    								                    	    										readonly="#{!embargoConceptoForm.editing}"
                        											id="cuenta"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="cuenta" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Banco
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="20"
	    	    		                							maxlength="20"
            	    		        								value="#{embargoConceptoForm.embargoConcepto.banco}"
                	    		    								                    	    										readonly="#{!embargoConceptoForm.editing}"
                        											id="banco"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="banco" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																				<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsEmbargoConcepto"
            										rendered="#{embargoConceptoForm.showData}">
		                        					<f:verbatim><table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{embargoConceptoForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!embargoConceptoForm.editing&&!embargoConceptoForm.deleting&&embargoConceptoForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{embargoConceptoForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!embargoConceptoForm.editing&&!embargoConceptoForm.deleting&&embargoConceptoForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{embargoConceptoForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{embargoConceptoForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{embargoConceptoForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{embargoConceptoForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{embargoConceptoForm.editing}" />
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{embargoConceptoForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>