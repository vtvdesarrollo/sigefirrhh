<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{conceptoVariableForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formConceptoVariable">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Conceptos Variables
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/trabajador/ConceptoVariable.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchConceptoVariable"
								rendered="#{!conceptoVariableForm.showData&&
									!conceptoVariableForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{conceptoVariableForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVariableForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												</td>
											</tr>
                        					<f:verbatim><tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{conceptoVariableForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{conceptoVariableForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{conceptoVariableForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{conceptoVariableForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{conceptoVariableForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{conceptoVariableForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{conceptoVariableForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{conceptoVariableForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{conceptoVariableForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{conceptoVariableForm.showResultTrabajador&&
								!conceptoVariableForm.showData&&
								!conceptoVariableForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{conceptoVariableForm.resultTrabajador}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{conceptoVariableForm.findConceptoVariableByTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{conceptoVariableForm.selectedTrabajador&&
								!conceptoVariableForm.showData&&
								!conceptoVariableForm.editing&&
								!conceptoVariableForm.deleting}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{conceptoVariableForm.trabajador}"
                            						action="#{conceptoVariableForm.findConceptoVariableByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{conceptoVariableForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
									<tr>
										<td>
											<table width="100%" class="bandtable">
    											<tr>
    												<td style="text-align: right"></f:verbatim>
            											<h:commandButton image="/images/add.gif"
            												action="#{conceptoVariableForm.add}"
            												onclick="javascript:return clickMade()"
            												rendered="#{!conceptoVariableForm.editing&&!conceptoVariableForm.deleting&&conceptoVariableForm.login.agregar}" />
            											<h:commandButton value="Cancelar" 
		                                    				image="/images/cancel.gif"
		                        							action="#{conceptoVariableForm.abort}"
		                        							immediate="true"
		                        							onclick="javascript:return clickMade()"
		                	        							 />
                                            			<f:verbatim>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultConceptoVariable"
								rendered="#{conceptoVariableForm.showResult&&
								!conceptoVariableForm.showData&&
								!conceptoVariableForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoVariableForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoVariableForm.selectConceptoVariable}"
                                						styleClass="listitem">
                                						<f:param name="idConceptoVariable" value="#{result.idConceptoVariable}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataConceptoVariable"
								rendered="#{conceptoVariableForm.showData||conceptoVariableForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!conceptoVariableForm.editing&&!conceptoVariableForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{conceptoVariableForm.editing&&!conceptoVariableForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{conceptoVariableForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{conceptoVariableForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
<h:form id="conceptoVariableForm">
<h:inputHidden id="scrollx" value="#{conceptoVariableForm.scrollx}" />
<h:inputHidden id="scrolly" value="#{conceptoVariableForm.scrolly}" />
            							<f:subview 
            								id="dataConceptoVariable"
            								rendered="#{conceptoVariableForm.showData}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{conceptoVariableForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoVariableForm.editing&&!conceptoVariableForm.deleting&&conceptoVariableForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{conceptoVariableForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoVariableForm.editing&&!conceptoVariableForm.deleting&&conceptoVariableForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{conceptoVariableForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{conceptoVariableForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{conceptoVariableForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{conceptoVariableForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{conceptoVariableForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{conceptoVariableForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim><tr>
                        						<td>
            										Trabajador
                        						</td>
                        						<td></f:verbatim>
                        							<h:outputText value="#{conceptoVariableForm.trabajador}" />
                        						<f:verbatim></td>
            								</tr></f:verbatim>
            								<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoVariableForm.selectConceptoTipoPersonal}"
                                                    	disabled="#{!conceptoVariableForm.editing}"
                                                    	immediate="false"
     													required="true"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVariableForm.colConceptoTipoPersonal}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Frecuencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoVariableForm.selectFrecuenciaTipoPersonal}"
                                                    	disabled="#{!conceptoVariableForm.editing}"
                                                    	immediate="false"
                        								required="true"
														title="Frecuencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVariableForm.colFrecuenciaTipoPersonal}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Horas/D�as/Cantidad
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{conceptoVariableForm.conceptoVariable.unidades}"
                        								                        								readonly="#{!conceptoVariableForm.editing}"
                        								id="Unidades"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim>
                        					<tr>
                        						<td>
                        							Monto
                        						</td>
                        						<td>
                        						<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
                        						<tr>
                        						<td>
                        						</f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							maxlength="15"
                        								value="#{conceptoVariableForm.conceptoVariable.monto}"
                        								                        								readonly="#{!conceptoVariableForm.editing}"
                        								id="Monto"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        							<f:verbatim></td>
                        						<td align="right">
												</f:verbatim>				
													<h:commandButton value="Calcular" 
                                    					image="/images/calculate.gif"                                    					
                                    					action="#{conceptoVariableForm.calcular}"
                                    					disabled="#{!conceptoVariableForm.editing}"
                                    					onclick="javascript:return clickMade()"/>
                            	        		<f:verbatim>
												</td>
												</tr>
												</table>
												</td>
                        					</tr>                            
                        					</f:verbatim>
                        					
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Estatus
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoVariableForm.conceptoVariable.estatus}"
                                                    	disabled="#{!conceptoVariableForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="false"
                                                    	title="Estatus">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVariableForm.listEstatus}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					
                        					
                        					
                        					
                        					
                        					
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Documento Soporte
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
                        								value="#{conceptoVariableForm.conceptoVariable.documentoSoporte}"
                        								                        								readonly="#{!conceptoVariableForm.editing}"
                        								id="DocumentoSoporte"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						 >
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha Registro
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							maxlength="10"
	                        							id="FechaRegistro"
                        								value="#{conceptoVariableForm.conceptoVariable.fechaRegistro}"
                        								readonly="true"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								onblur="javascript:check_date(this)"
                        								                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
									</f:subview>
            							<f:subview 
            								id="commandsConceptoVariable"
            								rendered="#{conceptoVariableForm.showData}">
                        				<f:verbatim><table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{conceptoVariableForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoVariableForm.editing&&!conceptoVariableForm.deleting&&conceptoVariableForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{conceptoVariableForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoVariableForm.editing&&!conceptoVariableForm.deleting&&conceptoVariableForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{conceptoVariableForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{conceptoVariableForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{conceptoVariableForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{conceptoVariableForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{conceptoVariableForm.editing}" />	
                            	        			<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{conceptoVariableForm.abortUpdate}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>