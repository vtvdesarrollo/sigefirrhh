<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{registrarAnticiposForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formRegistrarAnticipos">
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Registrar Anticipos
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/pasivoLaboral/RegistrarAnticipos.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{registrarAnticiposForm.selectIdTipoPersonal}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{registrarAnticiposForm.changeTipoPersonal}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{registrarAnticiposForm.listTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText  
	    										size="3"
	    										maxlength="2"  											
    											id="Mes"  
    											onkeypress="return keyIntegerCheck(event, this)"  											
    											value="#{registrarAnticiposForm.mes}"    											
    											 />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										size="5"
	    										maxlength="4"
    											id="Anio"
    											onkeypress="return keyIntegerCheck(event, this)"  
    											value="#{registrarAnticiposForm.anio}"    											
    											 />    								
    									</td>
									</tr>			
									<tr>
    									<td width="20%">
    										Fecha Anticipo
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="10"
    											id="FechaAnticipo"
    											maxlength="10"
    											value="#{registrarAnticiposForm.fechaAnticipo}"
    											onblur="javascript:check_date(this)"
    											readonly="false"
    											required="false" > 
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                            	pattern="dd-MM-yyyy" />   										    											
                	                        </h:inputText>
    									</td>
									</tr>
									<tr>
                      						<td>
                      							Monto Anticipo
                      						</td>
                      						<td>
			                        		<h:inputText
                       							size="12"
                       							maxlength="15"
                      								value="#{registrarAnticiposForm.montoAnticipo}"
                      								id="MontoAnticipo"
                      								required="false"
                      								onchange="javascript:this.value=this.value.toUpperCase()"
                      								onkeypress="return keyFloatCheck(event, this)"
												 style="text-align:right"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                      							</h:inputText>
                      					</td>
                        			</tr>                        
    								<tr>
    									<td>
    										Tipo 
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{registrarAnticiposForm.tipoAnticipo}"	
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Nuevo Regimen" itemValue="N" />
                                            	<f:selectItem itemLabel="Regimen Derogado" itemValue="V" />
                                            	<f:selectItem itemLabel="Intereses Nuevo Regimen" itemValue="I" />
                                            	<f:selectItem itemLabel="Intereses Regimen Derogado" itemValue="F" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Forma de Pago
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{registrarAnticiposForm.formaAnticipo}"	
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Pago Directo" itemValue="P" />
                                            	<f:selectItem itemLabel="Bono Deuda Publica" itemValue="B" />
                                            	<f:selectItem itemLabel="Otro Instrumento" itemValue="O" />                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"	    										
    											action="#{registrarAnticiposForm.ejecutar}"
    											/>
    									<td>
    								</tr>   
    								 								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>