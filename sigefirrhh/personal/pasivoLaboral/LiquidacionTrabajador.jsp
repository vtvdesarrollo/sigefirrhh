<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{liquidacionTrabajadorForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />
				<%
	  			}else { %>
	  				 
	  				
				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<h:form id="formLiquidacionTrabajador">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Liquidaci�n de Prestaciones Sociales
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/pasivoLaboral/LiquidacionTrabajador.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
											
											<h:commandButton value="Cerrar" 
                        					image="/images/close.gif"
                        					action="go_cancelOption"
                        					immediate="true"                        					
                        					onclick="javascript:return clickMade()"
                	        				 />
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									
									<f:subview 
										id="searchTrabajador"
										rendered="#{!liquidacionTrabajadorForm.showData}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable">
        		                						<tr>
		                        						<td>
		                        							Tipo de Personal
		                        						</td>
		                        						<td></f:verbatim>
							                            	<h:selectOneMenu value="#{liquidacionTrabajadorForm.findSelectTrabajadorIdTipoPersonal}"
							                            		id="colTipoPersonal"
		                                                    	immediate="false">
		                                                    	<f:selectItem itemLabel="Seleccione" itemValue="0" />																
		                                                        <f:selectItems value="#{liquidacionTrabajadorForm.findColTipoPersonal}" />
		                                                    </h:selectOneMenu>
															
														<f:verbatim></td>
													</tr>
        		                					<tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{liquidacionTrabajadorForm.findTrabajadorCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
																onfocus="return deleteZero(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{liquidacionTrabajadorForm.findTrabajadorByCedula}" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{liquidacionTrabajadorForm.findTrabajadorPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{liquidacionTrabajadorForm.findTrabajadorSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{liquidacionTrabajadorForm.findTrabajadorPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{liquidacionTrabajadorForm.findTrabajadorSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{liquidacionTrabajadorForm.findTrabajadorByNombresApellidos}" />
														<f:verbatim></td>
													</tr>
													<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{liquidacionTrabajadorForm.findTrabajadorCodigoNomina}"
														onfocus="return deleteZero(event, this)"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
																action="#{liquidacionTrabajadorForm.findTrabajadorByCodigoNomina}"
																onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultTrabajador"
									rendered="#{liquidacionTrabajadorForm.showResultTrabajador&&
									!liquidacionTrabajadorForm.showData}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <h:dataTable id="resultTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{liquidacionTrabajadorForm.resultTrabajador}"
                                               
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{liquidacionTrabajadorForm.selectTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="resultTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="resultTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataTrabajador"
								rendered="#{liquidacionTrabajadorForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td>
													</f:verbatim>
														<h:outputText value="Consultando"
															rendered="#{!liquidacionTrabajadorForm.procesing}" />
														<h:outputText value="Procesando"
															rendered="#{liquidacionTrabajadorForm.procesing}" />
													<f:verbatim>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="liquidacionTrabajadorForm">
	    	        							<f:subview 
    	    	    								id="dataTrabajador"
        	    									rendered="#{liquidacionTrabajadorForm.showData}">
	        	    								<f:verbatim>
	        	    								<table class="datatable" width="100%">
    	        	    	        				</f:verbatim>
													<f:verbatim>
														<tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td>
	                        						</f:verbatim>
	    	                    								<h:outputText value="#{liquidacionTrabajadorForm.trabajador.personal}" />
    	    	                					<f:verbatim>
    	    	                							</td>
	            										</tr>
	            									</f:verbatim>
													
	        	            	    				<f:verbatim>
	        	            	    					<tr>
    	                    								<td>
        	                									Fecha de Ingreso
		                        							</td>
		                        							<td>
		                        					</f:verbatim>
															<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaIngreso"
                		        									value="#{liquidacionTrabajadorForm.trabajador.fechaIngreso}"
                    		    									readonly="true"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		                	pattern="dd-MM-yyyy" />
                    		    								</h:inputText>
                    		    								<f:verbatim>(dd-mm-aaaa)</f:verbatim>
        	            	    					<f:verbatim>
        	            	    						</td>
	        	            	    				  </tr>
	        	            	    				 </f:verbatim>
	        	            	    				<f:verbatim>
	        	            	    					<tr>
    	                    								<td>
        	                									Fecha de Ingreso APN
		                        							<td>
		                        					</f:verbatim>
															<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaIngresoAPN"
                		        									value="#{liquidacionTrabajadorForm.trabajador.fechaIngresoApn}"
                    		    									readonly="true"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
        	            	    					<f:verbatim></td>
	        	            	    					</tr>
	        	            	    				</f:verbatim>
        	            	    					<f:verbatim>
	        	            	    					<tr>
    	                    								<td>
        	                									Fecha de Egreso
	            	            							</td>
		                        							<td>
		                        					</f:verbatim>
																<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaEgreso"
                		        									value="#{liquidacionTrabajadorForm.trabajador.fechaEgreso}"
                    		    									readonly="true"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}" pattern="dd-MM-yyyy" />
                    		    								</h:inputText>
                    		    							<f:verbatim>(dd-mm-aaaa)</f:verbatim>
        	            	    							</td>
        	            	    					<f:verbatim>
	        	            	    					 </tr>
	        	            	    				  </f:verbatim>
													</table>																																																																																																																																																																																																																									<f:verbatim></table></f:verbatim>
												</f:subview>
            						<f:subview 
            							id="commandsTrabajador"
            							rendered="#{liquidacionTrabajadorForm.showData}">
		                        		<f:verbatim>
		                        		<table class="datatable" align="center">
    		                    		<tr>
											<td align="left">
												</f:verbatim>
    											<h:commandButton image="/images/run.gif"
    												action="#{liquidacionTrabajadorForm.ejecutar}"
    												onclick="javascript:return clickMade()"
    												rendered="#{liquidacionTrabajadorForm.showData && liquidacionTrabajadorForm.trabajadorEgresado}" />
    											
                                    			<h:commandButton value="Cancelar" 
		                            				image="/images/cancel.gif"
		                							action="#{liquidacionTrabajadorForm.abort}"
		                							immediate="true"
		                							onclick="javascript:return clickMade()"
		        	        							 />
												<f:verbatim>
											</td>
										</tr>
    								</table></f:verbatim>
	    							</f:subview>
								</h:form>
        	                     <f:verbatim></td>
                        	</tr>
                        </table></f:verbatim>
                       	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>