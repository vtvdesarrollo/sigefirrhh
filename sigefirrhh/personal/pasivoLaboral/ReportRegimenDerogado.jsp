<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportRegimenDerogado'].elements['formReportRegimenDerogado:reportId'].value;
			var name = 
				document.forms['formReportRegimenDerogado'].elements['formReportRegimenDerogado:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportRegimenDerogadoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportRegimenDerogado">
				
    				<h:inputHidden id="reportId" value="#{reportRegimenDerogadoForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportRegimenDerogadoForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Deuda e Intereses por Tipo Personal
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success" />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegimenDerogadoForm.idTipoPersonal}"
	    										onchange="this.form.submit()"    										
                                            	immediate="false">
                                                <f:selectItems value="#{reportRegimenDerogadoForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Regiones
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegimenDerogadoForm.idRegion}"
	    										onchange="this.form.submit()"
                                            	immediate="false">  
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />    											
                                                <f:selectItems value="#{reportRegimenDerogadoForm.listRegion}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	    								
    									<tr>
    									<td>
    										Tipo Reporte
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegimenDerogadoForm.tipo}"    											
    											onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Deuda Regimen Derogado hasta 18-06-97" itemValue="1" />  
                                            	<f:selectItem itemLabel="Deuda e Intereses Adicionales a partir 19-06-97 " itemValue="2" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    									<tr>
    									<td>
    										Ordenado por 
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegimenDerogadoForm.orden}"    											    										
    											onchange="this.form.submit()"    											
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Alfab�tico" itemValue="alf" />  
                                            	<f:selectItem itemLabel="C�dula" itemValue="ced" />
                                            	<f:selectItem itemLabel="C�digo de N�mina" itemValue="cod" />                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportTrabajadorDependenciaForm.formato}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    									
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
    											action="#{reportRegimenDerogadoForm.runReport}"
    											onclick="windowReport()" />
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>