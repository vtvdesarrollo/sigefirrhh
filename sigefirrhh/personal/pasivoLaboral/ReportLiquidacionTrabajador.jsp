<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportLiquidacionTrabajador'].elements['formReportLiquidacionTrabajador:reportId'].value;
			var name = 
				document.forms['formReportLiquidacionTrabajador'].elements['formReportLiquidacionTrabajador:reportName'].value;				
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}    	
    </script>
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>

<f:view>
	<x:saveState value="#{reportLiquidacionTrabajadorForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	               
    	  	 />
    	  				
    				
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>

						<td valign="top">
							<h:form id="formReportLiquidacionTrabajador">
							<h:inputHidden id="reportId" value="#{reportLiquidacionTrabajadorForm.reportId}" />
							<h:inputHidden id="reportName" value="#{reportLiquidacionTrabajadorForm.reportName}" />
    				
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Formatos de Liquidaci�n de Prestaciones Sociales
											</b>
										</td>
										<td align="right">
											
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									<f:subview 
										id="searchTrabajador"
										rendered="#{!reportLiquidacionTrabajadorForm.selectedTrabajador}">
										<f:verbatim><tr>
											<td>
											<tr>
												<td>
													<table width="100%" class="bandtable">
														<tr>
															<td>
																Buscar Trabajador
															</td>
														</tr>
													</table>
												<table width="100%" class="datatable">
													<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{reportLiquidacionTrabajadorForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{reportLiquidacionTrabajadorForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{reportLiquidacionTrabajadorForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{reportLiquidacionTrabajadorForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{reportLiquidacionTrabajadorForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{reportLiquidacionTrabajadorForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{reportLiquidacionTrabajadorForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{reportLiquidacionTrabajadorForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{reportLiquidacionTrabajadorForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{reportLiquidacionTrabajadorForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{reportLiquidacionTrabajadorForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{reportLiquidacionTrabajadorForm.showResultTrabajador&&
									!reportLiquidacionTrabajadorForm.selectedTrabajador}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Trabajador
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <h:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultTrabajador"
	                                				value="#{reportLiquidacionTrabajadorForm.resultTrabajador}"
    	                                            
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultTrabajador}"
    	                            						action="#{reportLiquidacionTrabajadorForm.selectTrabajador}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </h:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{reportLiquidacionTrabajadorForm.selectedTrabajador}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>
            									<h:outputText value="#{reportLiquidacionTrabajadorForm.trabajador}" />
            										
            										
	            							<f:verbatim></td>
										</tr>
										
										
										
	        	            	    				  
	        	            	    			
										
										
										<tr>
											<td>
												<table width="100%" class="bandtable">
												
		                        					<tr>
				    									<td>
				    										Tipo Reporte
				    									</td>
				    									<td width="100%"></f:verbatim>
				    										<h:selectOneMenu value="#{reportLiquidacionTrabajadorForm.tipo}"
				    											
				    											onchange="this.form.submit()"
				                                            	immediate="false">
				                                                       <f:selectItem itemLabel="Liquidaci�n Regimen Derogado" itemValue="1" /> 
				                                           	           <f:selectItem itemLabel="Liquidaci�n Intereses Adicionales" itemValue="2" />  
				                                                	   <f:selectItem itemLabel="Liquidaci�n Regimen Vigente" itemValue="3" /> 
				                                                	   <f:selectItem itemLabel="Otros Pagos Liquidacion" itemValue="4" />  
				                                                	   <f:selectItem itemLabel="Devengado Integral R�gimen Derogado" itemValue="5" />  
				                                                	   <f:selectItem itemLabel="Devengado Integral Nuevo R�gimen" itemValue="6" /> 
				                                                </h:selectOneMenu>
				    									<f:verbatim></td>
				    								</tr>	
				    								
				    								
														 <tr>
    	                    								<td>
        	                									Formato
	            	            							</td>
		                        							<td>
		                        					           </f:verbatim>
		                        					   	            <h:selectOneMenu value="#{reportLiquidacionTrabajadorForm.formato}"				    															    											
					    								       	       onchange="this.form.submit()"
				                                                 	   immediate="false">
				                                           	            <f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	                        <f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />       
				                                                   </h:selectOneMenu>			   																						
        	            	    					           <f:verbatim>
        	            	    							</td>
	        	            	    					  </tr>
	        	            	    				
				    								
				    								<tr>
    													<td style="text-align: right"></f:verbatim>
    														<h:commandButton image="/images/report.gif"
    														rendered="#{reportLiquidacionTrabajadorForm.showBoton}"
			    											action="#{reportLiquidacionTrabajadorForm.runReport}"
    														onclick="windowReport()" />            												
                                        	    			                                        	    			
                                        	    			<f:verbatim>
																<img
                                            	            	onclick="document.forms['formCancel'].elements['formCancel:abort'].click()"
                                                	     		style="cursor:hand"
                                                    	        src="/sigefirrhh/images/cancel.gif" />
                                                    	    </f:verbatim>
										    				
                                        	    			<f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								
							</h:form>	
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<h:form id="formCancel">
    	<h:commandButton
    		id="abort"
	    	action="#{reportLiquidacionTrabajadorForm.abort}"
    		style="visibility:hidden"
    		type="submit" />
    	<h:commandButton
    		id="abortUpdate"
	    	action="#{reportLiquidacionTrabajadorForm.abortUpdate}"
    		style="visibility:hidden"
    		type="submit" />
	</h:form>
</f:view>

</body>
</html>