<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{disqueteNominaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formDisqueteTicket">
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Generar Archivos/Disquete de N�mina
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/disquete/DisqueteNomina.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Disquete
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Grupo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteNominaForm.selectGrupoNomina}"
                                            	immediate="true"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{disqueteNominaForm.changeGrupoNomina}">
                                            	<f:selectItem itemLabel="Todos" itemValue="0" />
                                                <f:selectItems value="#{disqueteNominaForm.listGrupoNomina}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Tipo de N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteNominaForm.tipoNomina}"	    										
	    										onchange="this.form.submit()"
                                            	immediate="true"
                                            	valueChangeListener="#{disqueteNominaForm.changeTipoNomina}">
                                            	<f:selectItem itemLabel="Ordinaria" itemValue="O" />  
                                            	<f:selectItem itemLabel="Especial" itemValue="E" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										N�mina Especial
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteNominaForm.selectNominaEspecial}"
                                            	immediate="true"
                                            	onchange="this.form.submit()" 
                                            	rendered="#{disqueteNominaForm.showNominaEspecial}" 
                                            	valueChangeListener="#{disqueteNominaForm.changeNominaEspecial}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{disqueteNominaForm.listNominaEspecial}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td width="20%">
    										Desde
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{disqueteNominaForm.show}"
    											size="10"
    											id="Desde"
    											maxlength="10"
    											value="#{disqueteNominaForm.inicio}"
    											readonly="true"
    											required="false" />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Hasta
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{disqueteNominaForm.show}"
    											size="10"
    											id="Hasta"
    											maxlength="10"
    											value="#{disqueteNominaForm.fin}"
    											readonly="true"
    											required="false" />    								
    									</td>
									</tr>
									<tr>
    									<td>
    										Disquete
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteNominaForm.selectDisquete}"
                                            	immediate="false"
                                            	onchange="this.form.submit()">
                                                <f:selectItems value="#{disqueteNominaForm.listDisquete}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Regi�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteNominaForm.selectRegion}"
                                            	immediate="false"
                                            	onchange="this.form.submit()">
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{disqueteNominaForm.listRegion}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								    								<tr>
    									<td>
    										Unidad Administradora
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteNominaForm.selectUnidadAdministradora}"
                                            	immediate="false"
                                            	onchange="this.form.submit()">
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{disqueteNominaForm.listUnidadAdministradora}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td width="20%">
    										Numero Orden de Pago
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="25"
    											id="numeroOrdenPAgo"
    											maxlength="25"
    											value="#{disqueteNominaForm.numeroOrdenPago}"    											
    											readonly="false"
    											required="false" > 
    											 										    											
                	                        </h:inputText>
    									</td>
									</tr>									
    								<tr>
    									<td width="20%">
    										Fecha Abono
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="10"
    											id="FechaAbono"
    											maxlength="10"
    											value="#{disqueteNominaForm.fechaAbono}"
    											onblur="javascript:check_date(this)"
    											readonly="false"
    											required="false" > 
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                            	pattern="dd-MM-yyyy" />   										    											
                	                        </h:inputText>
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Fecha Proceso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText    											
    											size="10"
    											id="FechaProceso"
    											maxlength="10"
    											value="#{disqueteNominaForm.fechaProceso}"
    											readonly="true"
    											onblur="javascript:check_date(this)"
    											required="false" >    
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                            	pattern="dd-MM-yyyy" />											
    										</h:inputText> 								
    									</td>
									</tr>											    								    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										rendered="#{disqueteNominaForm.show}"
    											action="#{disqueteNominaForm.generar}"/>
    								
    									<td>
    								</tr>
    								<tr>
                						<td align="center">                							
                							<h:outputLink value="#{disqueteNominaForm.urlArchivo}" rendered="#{disqueteNominaForm.generado}">
                							<f:verbatim>Archivo Generado  (Abrir nueva ventana con el bot�n derecho del mouse)</f:verbatim></h:outputLink>
                						</td>
                					</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>