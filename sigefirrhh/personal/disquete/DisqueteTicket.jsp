<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{disqueteTicketForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formDisqueteTicket">
				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Disquete de Cesta Tickets
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/disquete/DisqueteTicket.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Disquete
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteTicketForm.selectGrupoTicket}"
	    										onchange="this.form.submit()"  
	    										valueChangeListener="#{disqueteTicketForm.changeGrupoTicket}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{disqueteTicketForm.listGrupoTicket}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Tipo Proceso
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteTicketForm.tipoProceso}"	    										
	    										onchange="this.form.submit()"
                                            	immediate="true"
                                            	valueChangeListener="#{disqueteTicketForm.changeTipoProceso}">
                                            	<f:selectItem itemLabel="Ordinario" itemValue="N" />  
                                            	<f:selectItem itemLabel="Especial" itemValue="E" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText    	
	    										rendered="#{disqueteTicketForm.showEspecial}"   										
    											size="3"
    											id="Mes"
    											maxlength="2"
    											value="#{disqueteTicketForm.mes}"
    											required="true" />    
    										<h:outputText    	
	    										rendered="#{!disqueteTicketForm.showEspecial}"   										
    											value="#{disqueteTicketForm.mes}"/>    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{disqueteTicketForm.showEspecial}"     											
    											size="5"
    											id="Anio"
    											maxlength="5"
    											value="#{disqueteTicketForm.anio}"
    											readonly="false"
    											required="true" />    
    										<h:outputText
	    										rendered="#{!disqueteTicketForm.showEspecial}"     											
    											value="#{disqueteTicketForm.anio}"/>    								
    									</td>
									</tr>
									<tr>
    									<td>
    										Disquete
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteTicketForm.selectDisquete}"
                                            	immediate="false"
                                            	onchange="this.form.submit()">
                                                <f:selectItems value="#{disqueteTicketForm.listDisquete}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	    								
									<tr>
    									<td width="20%">
    										Fecha Proceso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText    											
    											size="10"
    											id="FechaProceso"
    											maxlength="10"
    											value="#{disqueteTicketForm.fechaProceso}"
    											readonly="true"
    											onblur="javascript:check_date(this)"
    											required="false" >    
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                            	pattern="dd-MM-yyyy" />											
    										</h:inputText> 								
    									</td>
									</tr>											    								    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										rendered="#{disqueteTicketForm.show}"
    											action="#{disqueteTicketForm.generar}"/>
    								
    									<td>
    								</tr>
    								<tr>
                						<td align="center">                							
                							<h:outputLink value="#{disqueteTicketForm.urlArchivo}" rendered="#{disqueteTicketForm.generado}">
                							<f:verbatim>Archivo Generado  (Abrir nueva ventana con el bot�n derecho del mouse)</f:verbatim></h:outputLink>
                						</td>
                					</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>