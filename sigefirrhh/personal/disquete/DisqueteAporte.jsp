<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{disqueteAporteForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formDisqueteAporte">
				
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Generar Archivos/Disquete Retenciones y Aportes(Asociado a Banco)
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/disquete/DisqueteAporte.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Disquete
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteAporteForm.selectTipoPersonal}"
    										valueChangeListener="#{disqueteAporteForm.changeTipoPersonal}"                    
                                            	immediate="true"
                                            	onchange="this.form.submit()"  
                                            	>
                                            	<f:selectItem itemLabel="Todos" itemValue="0" />
                                                <f:selectItems value="#{disqueteAporteForm.listTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td width="20%">
    										Mes Proceso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											
    											size="3"
    											id="Mes"
    											maxlength="2"
    											value="#{disqueteAporteForm.mes}"
    											required="true" />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										A�o Proceso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											
    											size="5"
    											id="Anio"
    											maxlength="5"
    											value="#{disqueteAporteForm.anio}"
    											readonly="false"
    											required="true" />    								
    									</td>
									</tr>
									
									
									<tr>
    									<td>
    										Unidad Administradora
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteAporteForm.idUnidadAdministradora}"
                                            	immediate="false"

                                            	onchange="this.form.submit()"  >
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{disqueteAporteForm.listUnidadAdministradora}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>
									
									
									<tr>
    									<td>
    										Disquete
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteAporteForm.selectDisquete}"
                                            	immediate="false"
                                            	onchange="this.form.submit()">
                                                <f:selectItems value="#{disqueteAporteForm.listDisquete}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Regi�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{disqueteAporteForm.selectRegion}"
                                            	immediate="false"
                                            	onchange="this.form.submit()">
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{disqueteAporteForm.listRegion}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								    								<tr>
    														
    								<tr>
    									<td width="20%">
    										Fecha Abono
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="10"
    											id="FechaAbono"
    											maxlength="10"
    											value="#{disqueteAporteForm.fechaAbono}"
    											onblur="javascript:check_date(this)"
    											readonly="false"
    											required="false" > 
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                            	pattern="dd-MM-yyyy" />   										    											
                	                        </h:inputText>
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Fecha Ejecucion
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText    											
    											size="10"
    											id="FechaProceso"
    											maxlength="10"
    											value="#{disqueteAporteForm.fechaProceso}"
    											readonly="true"
    											onblur="javascript:check_date(this)"
    											required="false" >    
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                            	pattern="dd-MM-yyyy" />											
    										</h:inputText> 								
    									</td>
									</tr>											    								    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    									
    											action="#{disqueteAporteForm.generar}"/>
    								
    									<td>
    								</tr>
    								<tr>
                						<td align="center">                							
                							<h:outputLink value="#{disqueteAporteForm.urlArchivo}" rendered="#{disqueteAporteForm.generado}">
                							<f:verbatim>Archivo Generado  (Abrir nueva ventana con el bot�n derecho del mouse)</f:verbatim></h:outputLink>
                						</td>
                					</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>