<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{cambioEstatusForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formCargaMasivaConceptos" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Cambio de Estatus
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/conceptos/CambioEstatus.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />

    							</a>
    							<h:commandButton value="Cerrar"                     					
    							image="/images/close.gif"
            					action="go_cancelOption"
            					immediate="true"                        					
            					onclick="javascript:return clickMade()"
    	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{cambioEstatusForm.idTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{cambioEstatusForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{cambioEstatusForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu
    											value="#{cambioEstatusForm.idConcepto}"
                                            	immediate="false"
												rendered="#{cambioEstatusForm.showConcepto}">
                                                <f:selectItems value="#{cambioEstatusForm.listConcepto}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Frecuencia
    									</td>
    									<td width="100%">
    										<h:selectOneMenu
    											value="#{cambioEstatusForm.idFrecuenciaTipoPersonal}"
                                            	immediate="false"
												rendered="#{cambioEstatusForm.showFrecuenciaTipoPersonal}">
                                                <f:selectItems value="#{cambioEstatusForm.listFrecuenciaTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Estatus Actual
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{cambioEstatusForm.estatusActual}"	    											    										
	    										
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Activo" itemValue="A" />  
                                            	<f:selectItem itemLabel="Suspendido" itemValue="S" />
                                            	<f:selectItem itemLabel="Proyectado" itemValue="Y" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Estatus Nuevo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{cambioEstatusForm.estatusNuevo}"	    											    										
	    										
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Activo" itemValue="A" />  
                                            	<f:selectItem itemLabel="Suspendido" itemValue="S" />
                                            	<f:selectItem itemLabel="Proyectado" itemValue="Y" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Actualizar en
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{cambioEstatusForm.tipoConcepto}"
                                            	immediate="false">
                                                <f:selectItem itemLabel="Fijo" itemValue="F" />
                                                <f:selectItem itemLabel="Variable" itemValue="V" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    								<td colspan="2" align="center">										
										<h:commandButton image="/images/run.gif"
    										rendered="#{cambioEstatusForm.show1}"
											action="#{cambioEstatusForm.generar}"
											/>
    								<td>
    								</tr>
    								
    								<tr>
    								<td colspan="2" align="center">	
	    															
										<h:outputText
                        					value="�Esta seguro que desea actualizar"
                        					rendered="#{cambioEstatusForm.show2}" />
                						<h:outputText
                        					value="#{cambioEstatusForm.registros}"
                        					rendered="#{cambioEstatusForm.show2}" />		
                        				<h:outputText
                        					value=" registros?"
                        					rendered="#{cambioEstatusForm.show2}" />                        				
                        				<h:commandButton value="Si" 
                        					image="/images/yes.gif"
                        					action="#{cambioEstatusForm.actualizar}"
                        					onclick="javascript:return clickMade()"
                        					rendered="#{cambioEstatusForm.show2}" />
                        					                        			                        				
                        				<h:commandButton value="Cancelar" 
                        					image="/images/cancel.gif"
                        					action="#{cambioEstatusForm.abort}"
                        					immediate="true"
                        					rendered="#{cambioEstatusForm.show2}"
                        					onclick="javascript:return clickMade()"
                	        				 />
                	        			
    								<td>
    								</tr>
    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>