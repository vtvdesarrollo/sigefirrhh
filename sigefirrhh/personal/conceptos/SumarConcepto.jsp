<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
 
</head>

<body>
<f:view>
	<x:saveState value="#{sumarConceptoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formCargaMasivaConceptos" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Sumar un Concepto a otro Concepto
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/conceptos/SumarConcepto.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{sumarConceptoForm.idTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{sumarConceptoForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{sumarConceptoForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu
    											value="#{sumarConceptoForm.idConceptoActualizar}"
                                            	immediate="false"
												rendered="#{sumarConceptoForm.showConcepto}">
                                                <f:selectItems value="#{sumarConceptoForm.listConceptoActualizar}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>  
    								<tr>
    									<td>
    										Desde
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{sumarConceptoForm.tipoConceptoActualizar}"
                                            	immediate="false">
                                                <f:selectItem itemLabel="Fijo" itemValue="F" />
                                                <f:selectItem itemLabel="Variable" itemValue="V" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Sumarle el  Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu
    											value="#{sumarConceptoForm.idConceptoSumar}"
                                            	immediate="false"
												rendered="#{sumarConceptoForm.showConcepto}">
                                                <f:selectItems value="#{sumarConceptoForm.listConceptoSumar}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    
    																						
    								<tr>
    									<td>
    										Desde
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{sumarConceptoForm.tipoConceptoSumar}"
                                            	immediate="false">
                                                <f:selectItem itemLabel="Fijo" itemValue="F" />
                                                <f:selectItem itemLabel="Variable" itemValue="V" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    								<td colspan="2" align="center">										
										<h:commandButton image="/images/run.gif"
    										rendered="#{sumarConceptoForm.show1}"
											action="#{sumarConceptoForm.generar}"
											/>
    								<td>
    								</tr>    								
    								<tr>
    								<td colspan="2" align="center">	
	    															
										<h:outputText
                        					value="�Esta seguro que desea actualizar"
                        					rendered="#{sumarConceptoForm.show2}" />                							
                        				<h:outputText
                        					value=" los registros?"
                        					rendered="#{sumarConceptoForm.show2}" />                        				
                        				<h:commandButton value="Si" 
                        					image="/images/yes.gif"
                        					action="#{sumarConceptoForm.actualizar}"
                        					onclick="javascript:return clickMade()"
                        					rendered="#{sumarConceptoForm.show2}" />
                        					                        			                        				
                        				<h:commandButton value="Cancelar" 
                        					image="/images/cancel.gif"
                        					action="#{sumarConceptoForm.abort}"
                        					immediate="true"
                        					rendered="#{sumarConceptoForm.show2}"
                        					onclick="javascript:return clickMade()"
                	        				 />
                	        			
    								<td>
    								</tr>
    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>