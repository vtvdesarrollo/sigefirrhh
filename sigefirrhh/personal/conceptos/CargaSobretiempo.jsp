<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{cargaSobretiempoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formCargaSobretiempo" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Carga Sobretiempo
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/conceptos/CargaSobretiempo.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
            					image="/images/close.gif"
            					action="go_cancelOption"
            					immediate="true"                        					
            					onclick="javascript:return clickMade()"
    	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">   
        														    																	
        							<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td>
    										<h:selectOneMenu value="#{cargaSobretiempoForm.idTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{cargaSobretiempoForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{cargaSobretiempoForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td>
    										<h:selectOneMenu
    											value="#{cargaSobretiempoForm.idConcepto}"
                                            	immediate="false"
												rendered="#{cargaSobretiempoForm.showConcepto}"
												>
                                                <f:selectItems value="#{cargaSobretiempoForm.listConcepto}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								
    								
    								<f:subview id="agregar" rendered="#{!cargaSobretiempoForm.showEliminar}">
        								<f:verbatim>
        								<tr>
        									<td colspan="2">
        										<table width="100%" class="datatable">
        											<tr>
        												<td>
        													C�dula
        												</td>
        												<td>
        													Unidades
        												</td>    													
    													<td>
    														Soporte
    													</td>
    													<td>
    														Mes
    													</td>
    													<td>
    														A�o
    													</td>
        											</tr>
        											<tr>
        												<td></f:verbatim>
    						                        		<h:inputText
    		                        							size="8"
    		                        							id="Cedula"
    		                        							maxlength="8"
    	                        								value="#{cargaSobretiempoForm.cedula}"   						
    															onfocus="return deleteZero(event, this)"
    															onblur="javascript:fieldEmpty(this)"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyIntegerCheck(event, this)" />
    													<f:verbatim>		
        												</td>
        												<td>
        												</f:verbatim>
    						                        		<h:inputText
    		                        							size="6"
    		                        							id="HorasDiasCantidad"
    		                        							maxlength="6"
    	                        								value="#{cargaSobretiempoForm.horasDiasCantidad}"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyIntegerCheck(event, this)" >
    															<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
    															</h:inputText>
    													<f:verbatim>
        												</td>    													
    													<td>
    													</f:verbatim>
    						                        		<h:inputText
    		                        							size="15"
    		                        							id="Soporte"
    		                        							maxlength="15"
    	                        								value="#{cargaSobretiempoForm.documentoSoporte}"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															/>
    			    									<f:verbatim>	
    													</td>	
    													<td>
    														<h:selectOneMenu value="#{cargaSobretiempoForm.mes}"
				                                            	immediate="false"
																onchange="this.form.submit()"
																valueChangeListener="#{cargaSobretiempoForm.changeMes}">
				                                                <f:selectItem itemLabel="Enero" itemValue="1" />
				                                                <f:selectItem itemLabel="Febrero" itemValue="2" />
				                                                <f:selectItem itemLabel="Marzo" itemValue="3" />
				                                                <f:selectItem itemLabel="Abril" itemValue="4" />
				                                                <f:selectItem itemLabel="Mayo" itemValue="5" />
				                                                <f:selectItem itemLabel="Junio" itemValue="6" />
				                                                <f:selectItem itemLabel="Julio" itemValue="7" />
				                                                <f:selectItem itemLabel="Agosto" itemValue="8" />
				                                                <f:selectItem itemLabel="Septiembre" itemValue="9" />
				                                                <f:selectItem itemLabel="Octubre" itemValue="10" />
				                                                <f:selectItem itemLabel="Noviembre" itemValue="11" />
				                                                <f:selectItem itemLabel="Diciembre" itemValue="12" />
				                                            </h:selectOneMenu>
    													</td>
    													<td>
        												</f:verbatim>
    						                        		<h:inputText
    		                        							size="5"
    		                        							id="anio"
    		                        							maxlength="4"
    	                        								value="#{cargaSobretiempoForm.anio}"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyIntegerCheck(event, this)" >
    															
    															</h:inputText>
    													<f:verbatim>
        												</td>    		
    													<td>
    													</f:verbatim>
    														<h:commandButton value="Agregar"
    			    											image="/images/add.gif"
        														action="#{cargaSobretiempoForm.agregar}" />
        												<f:verbatim>	
    													</td>
    													
        											</tr>
        											<tr>
        												<td>
        												</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaSobretiempoForm.cedulaFinal}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{cargaSobretiempoForm.nombre}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{cargaSobretiempoForm.apellido}" />
    	                        						<f:verbatim>
        												</td>
        												<td>
        												</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaSobretiempoForm.horasDiasCantidadFinal}"
    	                        								 />
    	                        						<f:verbatim>
        												</td>
    													<td>
    													</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaSobretiempoForm.montoFinal}"
    	                        								 />
    													<f:verbatim>
    													</td>
    													<td>
    													</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaSobretiempoForm.documentoSoporteFinal}"
    	                        								 />
    													<f:verbatim>
    													</td>
        											</tr>
        										</table>
        									</td>
        									</f:verbatim>
    									</f:subview>
    									<f:subview id="eliminar" rendered="#{cargaSobretiempoForm.showEliminar}">
        								<f:verbatim>
        								<tr>
        									<td colspan="2">
        										<table width="100%" class="datatable">
        											<tr>
        												<td>
        													C�dula
        												</td>
        												
        											</tr>
        											<tr>
        												<td></f:verbatim>
    						                        		<h:inputText
    		                        							size="8"
    		                        							id="Cedula"
    		                        							maxlength="8"
    	                        								value="#{cargaSobretiempoForm.cedula}"   						
    															onfocus="return deleteZero(event, this)"
    															onblur="javascript:fieldEmpty(this)"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyIntegerCheck(event, this)" />
    													<f:verbatim>		
        												</td>
        												
    													<td>
    													</f:verbatim>
    														<h:commandButton value="eliminar"
    			    											image="/images/delete.gif"
        														action="#{cargaSobretiempoForm.eliminar}" />
        												<f:verbatim>	
    													</td>
    													
        											</tr>
        											<tr>
        												<td>
        												</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaSobretiempoForm.cedulaFinal}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{cargaSobretiempoForm.nombre}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{cargaSobretiempoForm.apellido}" />
    	                        						<f:verbatim>
        												</td>
        												        											</tr>
        										</table>
        									</td>
        									</f:verbatim>
    									</f:subview>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>