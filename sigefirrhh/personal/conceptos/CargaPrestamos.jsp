<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
		<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{cargaPrestamosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formCargaPrestamos" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Carga Externa de Prestamos(Archivo)
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/conceptos/CargaPrestamos.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    							<h:outputLink value="#{cargaPrestamosForm.urlError}" rendered="#{cargaPrestamosForm.hasError}">
    							<f:verbatim>Archivo de errores  (Use el bot�n derecho del mouse)</f:verbatim></h:outputLink>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{cargaPrestamosForm.idTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{cargaPrestamosForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{cargaPrestamosForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Frecuencia Pago
    									</td>
    									<td width="100%">
    										<h:selectOneMenu
    											value="#{cargaPrestamosForm.idFrecuenciaPago}"
                                            	immediate="false"
												rendered="#{cargaPrestamosForm.showFrecuenciaPago}">
                                                <f:selectItems value="#{cargaPrestamosForm.listFrecuenciaPago}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td>    									
    										Archivo
    									</td>
    
    									<td width="50%">
                                            <x:inputFileUpload id="fileupload"
                                                accept="*.txt"
                                                value="#{cargaPrestamosForm.archivo}"
                                                storage="file"/>
    										<h:commandButton value="Cargar Archivo"
    											action="#{cargaPrestamosForm.cargar}" />
    									</td>
    									
    								</tr>
    								<tr>
    									
    								</tr>
    								<tr>
    									<td colspan="2">
                                            El archivo para importar, debe ser un archivo de texto tipo 
                                            con la extensi�n .TXT. Los campos debe estar distribuidos
                                            en columnas y separados por un tabulador (tecla TAB) en el siguiente orden:
                                            <br>
                                          	<br>
                                            ejemplo para suspender un pr�stamo:
                                            <br>
                                            concepto cedula montoCuota 
                                            <br>
                                            <br>                                            
                                            <br>
                                            <br>
                                            <font style="font-family: courier; size=15">
                                            0001   13878909   0<br>                                           
                                            <br>
                                            <br>
                                            ejemplo para ingresar un pr�stamo:
                                            <br>
                                            concepto cedula montoCuota numeroCuotas montoPrestamo fechaInicioDescuento	Referencia
                                            <br>
                                            <br>                                            
                                            <br>
                                            <br>
                                            <font style="font-family: courier; size=15">
                                            0001   13878909   25000.24	10	250002.4	30/11/2005	REFERENCIA<br>
                                            0104   12545433   50000	10	500000	30/11/2005	REFERENCIA<br>
                                            
                                            </font>
                                            <br>
                                            <br>
    									</td>
    								</tr>
    								<tr>
										<td></td>
										<td align="left">
											
											<h:commandButton image="/images/run.gif"    												
                    							action="#{cargaPrestamosForm.procesar}"
												onclick="javascript:return clickMade()"
												 />
											
    											
											
										</td>
									</tr>				
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>