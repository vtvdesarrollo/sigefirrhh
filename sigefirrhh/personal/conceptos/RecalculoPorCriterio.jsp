<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{recalculoPorCriterioForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formRecalculoPorCriterio" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Carga por Criterios
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/conceptos/RecalculoPorCriterio.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{recalculoPorCriterioForm.idTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{recalculoPorCriterioForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{recalculoPorCriterioForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu
    											value="#{recalculoPorCriterioForm.idConcepto}"
                                            	immediate="false"
												rendered="#{recalculoPorCriterioForm.showConcepto}">
                                                <f:selectItems value="#{recalculoPorCriterioForm.listConcepto}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Frecuencia
    									</td>
    									<td width="100%">
    										<h:selectOneMenu
    											value="#{recalculoPorCriterioForm.idFrecuenciaTipoPersonal}"
                                            	immediate="false"
												rendered="#{recalculoPorCriterioForm.showFrecuenciaTipoPersonal}">
                                                <f:selectItems value="#{recalculoPorCriterioForm.listFrecuenciaTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    	
    								<tr>
    									<td>
    										Unidades 
    									</td>
    									<td>
			                        		<h:inputText
                    							size="12"
                    							id="Unidades"
                    							maxlength="12"
                								value="#{recalculoPorCriterioForm.unidades}"
                								onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyFloatCheck(event, this)" />
    										
										</td>
    								</tr>								
    								<tr>
    									<td>
    										Actualizar en
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{recalculoPorCriterioForm.tipoConcepto}"
                                            	immediate="false">
                                                <f:selectItem itemLabel="Fijo" itemValue="F" />
                                                <f:selectItem itemLabel="Variable" itemValue="V" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    								
    								<tr>
    									<td>    									
    										Regi�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{recalculoPorCriterioForm.idRegion}"
    											rendered="#{recalculoPorCriterioForm.show}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{recalculoPorCriterioForm.listRegion}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Manual
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{recalculoPorCriterioForm.idManualCargo}"
                                            	immediate="false"
												onchange="this.form.submit()"
												rendered="#{recalculoPorCriterioForm.showManual}"
												valueChangeListener="#{recalculoPorCriterioForm.changeManualCargo}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{recalculoPorCriterioForm.listManualCargo}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>    									
    										Cargo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{recalculoPorCriterioForm.idCargo}"
    											rendered="#{recalculoPorCriterioForm.showCargo}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Todos" itemValue="0" />
                                                <f:selectItems value="#{recalculoPorCriterioForm.listCargo}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td colspan="2">
    									<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
    									<tr>
    									<td width="20%">
    										Desde Grado
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{recalculoPorCriterioForm.show}"
    											size="5"
    											id="DesdeGrado"
    											maxlength="3"
    											value="#{recalculoPorCriterioForm.desdeGrado}"
    											required="false" />    										
    									</td>
    									<td width="20%">
    										Hasta Grado
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{recalculoPorCriterioForm.show}"
    											size="5"
    											id="HastaGrado"
    											maxlength="3"
    											value="#{recalculoPorCriterioForm.hastaGrado}"
    											required="false" />    								
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Desde Fecha Ingreso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{recalculoPorCriterioForm.show}"
    											size="15"
    											id="DesdeFechaIngreso"
    											maxlength="12"
    											value="#{recalculoPorCriterioForm.desdeFechaIngreso}"
    											onblur="javascript:check_date(this)"
	                        					onkeypress="return keyEnterCheck(event, this)"
    											required="false" >
    											<f:convertDateTime timeZone="#{dateTime.timeZone}"                 	                        		                	
                	                        		    pattern="dd-MM-yyyy" />   
                	                       </h:inputText> 										
    									</td>
    									<td width="20%">
    										Hasta Fecha Ingreso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{recalculoPorCriterioForm.show}"
    											size="15"
    											id="HastaFechaIngreso"
    											maxlength="12"
    											value="#{recalculoPorCriterioForm.hastaFechaIngreso}"
    											onblur="javascript:check_date(this)"
	                        					onkeypress="return keyEnterCheck(event, this)"
    											required="false" >    	
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		    pattern="dd-MM-yyyy" />	
                	                        </h:inputText> 							
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Desde Sueldo Basico
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{recalculoPorCriterioForm.show}"
    											size="20"
    											id="DesdeSueldoBasico"
    											maxlength="20"
    											value="#{recalculoPorCriterioForm.desdeSueldoBasico}"
    											required="false" >
    											<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>  										
												</h:inputText>    										
    									</td>
    									<td width="20%">
    										Hasta Sueldo Basico
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{recalculoPorCriterioForm.show}"
    											size="20"
    											id="HastaSueldoBasico"
    											maxlength="20"
    											value="#{recalculoPorCriterioForm.hastaSueldoBasico}"
    											required="false" > 
    											<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>  										
												</h:inputText>   								
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Desde Sueldo Integral
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{recalculoPorCriterioForm.show}"
    											size="20"
    											id="DesdeSueldoBasIntegral"
    											maxlength="20"
    											value="#{recalculoPorCriterioForm.desdeSueldoIntegral}"
    											required="false"  >
												<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>  										
												</h:inputText>
    									</td>
    									<td width="20%">
    										Hasta Sueldo Integral
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{recalculoPorCriterioForm.show}"
    											size="20"
    											id="HastaSueldoIntegral"
    											maxlength="20"
    											value="#{recalculoPorCriterioForm.hastaSueldoIntegral}"
    											required="false" >
    											<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>   								
    											</h:inputText>
    									</td>
									</tr>
									</table>
									</td>
    								<tr>
    								<td colspan="2" align="center">										
										<h:commandButton image="/images/run.gif"
    										rendered="#{recalculoPorCriterioForm.show1}"
											action="#{recalculoPorCriterioForm.generar}"
											/>
    								<td>
    								</tr>
    								
    								<tr>
    								<td colspan="2" align="center">	
	    															
										<h:outputText
                        					value="�Esta seguro que desea actualizar estos "
                        					rendered="#{recalculoPorCriterioForm.show2}" />	
                        				<h:outputText
                        					value=" registros?"
                        					rendered="#{recalculoPorCriterioForm.show2}" />                        				
                        				<h:commandButton value="Si" 
                        					image="/images/yes.gif"
                        					action="#{recalculoPorCriterioForm.actualizar}"
                        					onclick="javascript:return clickMade()"
                        					rendered="#{recalculoPorCriterioForm.show2}" />
                        					                        			                        				
                        				<h:commandButton value="Cancelar" 
                        					image="/images/cancel.gif"
                        					action="#{recalculoPorCriterioForm.abort}"
                        					immediate="true"
                        					rendered="#{recalculoPorCriterioForm.show2}"
                        					onclick="javascript:return clickMade()"
                	        				 />
                	        			
    								<td>
    								</tr>
    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>