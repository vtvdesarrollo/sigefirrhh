<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
	<script language=javascript> 
		function firstFocus2(){
				var paso = "false";			
				for (i = 0; i < document.forms.length; i++){
					for (j = 0; j < document.forms[i].elements.length; j++){					
						if (document.forms[i].elements[j].type =="text"){																				
							document.forms[i].elements[j].focus();					
							break;
						}
					}				
					if (paso =="true"){
						break;
					}
				}
				
			}	
	</script>
</head>

<body onload="firstFocus2();">
<f:view>
	<x:saveState value="#{cargaMasivaConceptosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" v 
	                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formCargaMasivaConceptos" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Actualizacion Masiva de Conceptos
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/conceptos/CargaMasivaConceptos.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
            					image="/images/close.gif"
            					action="go_cancelOption"
            					immediate="true"                        					
            					onclick="javascript:return clickMade()"
    	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">   
        							<tr>
        									<td width="25%">
        										Acci�n
        									</td>
        									<td width="75%">
        										<h:selectOneMenu value="#{cargaMasivaConceptosForm.accion}"
	        										onchange="this.form.submit()"
	        										
                                                	immediate="false">
                                                    <f:selectItem itemLabel="INGRESAR CONCEPTOS" itemValue="A" />
                                                    <f:selectItem itemLabel="SUSTITUIR CONCEPTOS" itemValue="M" />
                                                    <f:selectItem itemLabel="SUMAR CONCEPTOS" itemValue="S" />
                                                    <f:selectItem itemLabel="ELIMINAR CONCEPTOS" itemValue="E" />
                                                </h:selectOneMenu>
        									</td>
        								</tr> 								    																	
        								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td>
    										<h:selectOneMenu value="#{cargaMasivaConceptosForm.idTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{cargaMasivaConceptosForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{cargaMasivaConceptosForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td>
    										<h:selectOneMenu
    											value="#{cargaMasivaConceptosForm.idConcepto}"
                                            	immediate="true"
												rendered="#{cargaMasivaConceptosForm.showConcepto}">
                                                <f:selectItems value="#{cargaMasivaConceptosForm.listConcepto}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Frecuencia
    									</td>
    									<td>
    										<h:selectOneMenu
    											value="#{cargaMasivaConceptosForm.idFrecuenciaTipoPersonal}"
                                            	immediate="true"
												rendered="#{cargaMasivaConceptosForm.showFrecuenciaTipoPersonal}">
                                                <f:selectItems value="#{cargaMasivaConceptosForm.listFrecuenciaTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td>
    										Grabar o eliminar en
    									</td>
    									<td>
    										<table width="100%">
    											<tr>
    												<td width="20%">
			    										<h:selectOneMenu value="#{cargaMasivaConceptosForm.tipoConcepto}"
			                                            	immediate="false">
			                                                <f:selectItem itemLabel="Fijo" itemValue="F" />
			                                                <f:selectItem itemLabel="Variable" itemValue="V" />
			                                            </h:selectOneMenu>
			                                        </td>
			                                        <td width="80%">
			                                        	si selecciona conceptos variables, la acci�n siempre ser� ingresar
			                                       	</td>
			                                   </tr>
			                               </table>
    									</td>
    									<td>
    										
    									</td>
    									
    								</tr>
    								<f:subview id="agregar" rendered="#{!cargaMasivaConceptosForm.showEliminar}">
        								<f:verbatim>
        								<tr>
        									<td colspan="2">
        										<table width="100%" class="datatable">
        											<tr>
        												<td>
        													C�dula
        												</td>
        												<td>
        													Unidades
        												</td>
    													<td>
    														Monto
    													</td>
    													<td>
    														Soporte
    													</td>
        											</tr>
        											<tr>
        												<td></f:verbatim>
    						                        		<h:inputText
    		                        							size="8"
    		                        							id="Cedula"
    		                        							maxlength="8"
    	                        								value="#{cargaMasivaConceptosForm.cedula}"   						
    															onfocus="return deleteZero(event, this)"
    															onblur="javascript:fieldEmpty(this)"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyIntegerCheck(event, this)" />
    													<f:verbatim>		
        												</td>
        												<td>
        												</f:verbatim>
    						                        		<h:inputText
    		                        							size="6"
    		                        							id="HorasDiasCantidad"
    		                        							maxlength="6"
    	                        								value="#{cargaMasivaConceptosForm.horasDiasCantidad}"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyIntegerCheck(event, this)" >
    															<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
    															</h:inputText>
    													<f:verbatim>
        												</td>
    													<td>
    													</f:verbatim>
    						                        		<h:inputText
    		                        							size="12"
    		                        							id="Monto"
    		                        							maxlength="12"
    	                        								value="#{cargaMasivaConceptosForm.monto}"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyFloatCheck(event, this)" >
    															<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
    														</h:inputText>
    			    									<f:verbatim>	
    													</td>
    													<td>
    													</f:verbatim>
    						                        		<h:inputText
    		                        							size="15"
    		                        							id="Soporte"
    		                        							maxlength="15"
    	                        								value="#{cargaMasivaConceptosForm.documentoSoporte}"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															/>
    			    									<f:verbatim>	
    													</td>	
    													<td>
    													</f:verbatim>
    														<h:commandButton value="Agregar"
    			    											image="/images/add.gif"
        														action="#{cargaMasivaConceptosForm.agregar}" />
        												<f:verbatim>	
    													</td>
    													
        											</tr>
        											<tr>
        												<td>
        												</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaMasivaConceptosForm.cedulaFinal}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{cargaMasivaConceptosForm.nombre}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{cargaMasivaConceptosForm.apellido}" />
    	                        						<f:verbatim>
        												</td>
        												<td>
        												</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaMasivaConceptosForm.horasDiasCantidadFinal}"
    	                        								 />
    	                        						<f:verbatim>
        												</td>
    													<td>
    													</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaMasivaConceptosForm.montoFinal}"
    	                        								 />
    													<f:verbatim>
    													</td>
    													<td>
    													</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaMasivaConceptosForm.documentoSoporteFinal}"
    	                        								 />
    													<f:verbatim>
    													</td>
        											</tr>
        										</table>
        									</td>
        									</f:verbatim>
    									</f:subview>
    									<f:subview id="eliminar" rendered="#{cargaMasivaConceptosForm.showEliminar}">
        								<f:verbatim>
        								<tr>
        									<td colspan="2">
        										<table width="100%" class="datatable">
        											<tr>
        												<td>
        													C�dula
        												</td>
        												
        											</tr>
        											<tr>
        												<td></f:verbatim>
    						                        		<h:inputText
    		                        							size="8"
    		                        							id="Cedula"
    		                        							maxlength="8"
    	                        								value="#{cargaMasivaConceptosForm.cedula}"   						
    															onfocus="return deleteZero(event, this)"
    															onblur="javascript:fieldEmpty(this)"
    	                        								onchange="javascript:this.value=this.value.toUpperCase()"
    															onkeypress="return keyIntegerCheck(event, this)" />
    													<f:verbatim>		
        												</td>
        												
    													<td>
    													</f:verbatim>
    														<h:commandButton value="eliminar"
    			    											image="/images/delete.gif"
        														action="#{cargaMasivaConceptosForm.eliminar}" />
        												<f:verbatim>	
    													</td>
    													
        											</tr>
        											<tr>
        												<td>
        												</f:verbatim>
    						                        		<h:outputText
    	                        								value="#{cargaMasivaConceptosForm.cedulaFinal}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{cargaMasivaConceptosForm.nombre}" />
    	                        							&nbsp;
    						                        		<h:outputText
    	                        								value="#{cargaMasivaConceptosForm.apellido}" />
    	                        						<f:verbatim>
        												</td>
        												        											</tr>
        										</table>
        									</td>
        									</f:verbatim>
    									</f:subview>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>