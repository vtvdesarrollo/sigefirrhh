<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{nominaDiplomaticoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formNominaDiplomatico">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Nomina del Servicio Exterior
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/mre/NominaDiplomatico.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchNominaDiplomatico"
								rendered="#{!nominaDiplomaticoForm.showData&&
									!nominaDiplomaticoForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
                        						<td>
                        							A�o
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="5"
														maxlength="4"
														value="#{nominaDiplomaticoForm.findAnio}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{nominaDiplomaticoForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{nominaDiplomaticoForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{nominaDiplomaticoForm.findTrabajadorCedula}"
														onkeypress="javascript:return keyCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{nominaDiplomaticoForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{nominaDiplomaticoForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onblur="javascript:fieldEmpty(this)"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{nominaDiplomaticoForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{nominaDiplomaticoForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{nominaDiplomaticoForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{nominaDiplomaticoForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{nominaDiplomaticoForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{nominaDiplomaticoForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{nominaDiplomaticoForm.showResultTrabajador&&
								!nominaDiplomaticoForm.showData&&
								!nominaDiplomaticoForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{nominaDiplomaticoForm.resultTrabajador}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{nominaDiplomaticoForm.findNominaDiplomaticoByTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif" />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif" />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{nominaDiplomaticoForm.selectedTrabajador&&
								!nominaDiplomaticoForm.showData&&
								!nominaDiplomaticoForm.editing&&
								!nominaDiplomaticoForm.deleting}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{nominaDiplomaticoForm.trabajador}"
                            						action="#{nominaDiplomaticoForm.findNominaDiplomaticoByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{nominaDiplomaticoForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultNominaDiplomatico"
								rendered="#{nominaDiplomaticoForm.showResult&&
								!nominaDiplomaticoForm.showData&&
								!nominaDiplomaticoForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{nominaDiplomaticoForm.result}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{nominaDiplomaticoForm.selectNominaDiplomatico}"
                                						styleClass="listitem">
                                						<f:param name="idNominaDiplomatico" value="#{result.idNominaDiplomatico}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataNominaDiplomatico"
								rendered="#{nominaDiplomaticoForm.showData||nominaDiplomaticoForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!nominaDiplomaticoForm.editing&&!nominaDiplomaticoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{nominaDiplomaticoForm.editing&&!nominaDiplomaticoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{nominaDiplomaticoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{nominaDiplomaticoForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
<h:form id="nominaDiplomaticoForm">
            							<f:subview 
            								id="dataNominaDiplomatico"
            								rendered="#{nominaDiplomaticoForm.showData}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                			
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{nominaDiplomaticoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{nominaDiplomaticoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{nominaDiplomaticoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{nominaDiplomaticoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{nominaDiplomaticoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{nominaDiplomaticoForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							/>	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim><tr>
                        						<td width="40%">
            										Trabajador
                        						</td>
                        						<td></f:verbatim>
                        							<h:outputText value="#{nominaDiplomaticoForm.trabajador}" />
                        						<f:verbatim></td>
            								</tr></f:verbatim>
            								
                        					<f:verbatim><tr>
                        						<td>
                        							A�o
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.anio}"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Mes
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.mes}">
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Asignaciones Bs.
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.asignacionBs}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Asignaciones $
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.asignacionOtraMoneda}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Deducciones Legales Bs.
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.deduccionLeyBs}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Deducciones Legales $
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.deduccionLeyOtraMoneda}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Deducciones Personales Bs.
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.deduccionPersonalBs}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Deducciones Personales $
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.deduccionPersonalOtraMoneda}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Asignacion MRE
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.asignacionMre}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Otras Asignaciones
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.otrasAsignaciones}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fluctuacion
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.fluctuacionMre}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Asignacion Anual ONU
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.asignacionAnualOnu}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Asignacion Mensual ONU
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.asignacionMensualOnu}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Ajuste Anual ONU
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.ajusteAnualOnu}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Prima Destino MRE
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.primaDestinoMre}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Prima Destino ONU
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.primaDestinoOnu}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Multiplicador
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
                        								value="#{nominaDiplomaticoForm.nominaDiplomatico.multiplicador}">
                        								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>	
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Dependencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:outputText value="#{nominaDiplomaticoForm.nominaDiplomatico.dependencia}"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:outputText value="#{nominaDiplomaticoForm.nominaDiplomatico.cargo}"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
									</f:subview>
            							<f:subview 
            								id="commandsNominaDiplomatico"
            								rendered="#{nominaDiplomaticoForm.showData}">
                        				<f:verbatim><table>
                        				<table class="datatable" align="center">
                        					<tr>
                        					
                        						<td></f:verbatim>
                                    				
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{nominaDiplomaticoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{nominaDiplomaticoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{nominaDiplomaticoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{nominaDiplomaticoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{nominaDiplomaticoForm.editing}" />	
                            	        			<h:commandButton value="Cancelar" 
			                                    		image="/images/cancel.gif"
                                    					action="#{nominaDiplomaticoForm.abortUpdate}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />	
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>