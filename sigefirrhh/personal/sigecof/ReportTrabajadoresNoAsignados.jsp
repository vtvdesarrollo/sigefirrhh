<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formTrabajadoresNoAsignados'].elements['formTrabajadoresNoAsignados:reportId'].value;
			var name = 
				document.forms['formTrabajadoresNoAsignados'].elements['formTrabajadoresNoAsignados:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportTrabajadoresNoAsignadosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formTrabajadoresNoAsignados">
				
    				<h:inputHidden id="reportId" value="#{reportTrabajadoresNoAsignadosForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportTrabajadoresNoAsignadosForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Trabajadores - Conceptos no Asignados
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">  
    															    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportTrabajadoresNoAsignadosForm.idTipoPersonal}"
    											onchange="this.form.submit()"
    											
                                            	immediate="false">
                                            	
                                                <f:selectItems value="#{reportTrabajadoresNoAsignadosForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td>
    										Mostrar
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportTrabajadoresNoAsignadosForm.mostrar}"
    											
    											onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Trabajadores no Asignados" itemValue="1" />  
                                            	<f:selectItem itemLabel="Trabajadores con Asignaci�n Errada" itemValue="2" />
                                            	<f:selectItem itemLabel="Conceptos no Distribuidos" itemValue="3" />
                                            	<f:selectItem itemLabel="Conceptos no Asociados a Partidas" itemValue="4" />
                                            	
                                            	
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="Anio"
    											maxlength="4"
    											value="#{reportTrabajadoresNoAsignadosForm.anio}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
    											action="#{reportTrabajadoresNoAsignadosForm.runReport}"
    											onclick="windowReport()" />
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>