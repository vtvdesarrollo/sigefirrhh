<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{generarTrabajadorCargoEspecificaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formGenerarCompromiso">
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Asignaci�n Masiva Trabajadores/Cargos a Proyecto/Acci�n
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/sigecof/GenerarTrabajadorCargoEspecifica.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    							<tr>
                						<td>
                							Tipo Personal
                						</td>
                						<td>
                                            <h:selectOneMenu value="#{generarTrabajadorCargoEspecificaForm.selectTipoPersonal}"
												id="tipoPersonal"
                                            	       								                                                    
                								immediate="false"
                                            	required="false"
												title="Tipo Personal">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarTrabajadorCargoEspecificaForm.listTipoPersonal}" />
                                            </h:selectOneMenu>													<h:message for="tipoPersonal" styleClass="error"/>
                						</td>
                					</tr>	    	
    							<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										
    											
    											size="10"
    											id="Anio"
    											maxlength="4"
    											value="#{generarTrabajadorCargoEspecificaForm.anio}"    											
    											required="false" />    								
    									</td>
									</tr>
    								<tr>
                						<td>
                							Unidad Ejecutora
                						</td>
                						<td>
                                            <h:selectOneMenu value="#{generarTrabajadorCargoEspecificaForm.selectUnidadEjecutora}"
												id="unidadEjecutora"
                                            	onchange="this.form.submit()"     
                                            	valueChangeListener="#{generarTrabajadorCargoEspecificaForm.changeUnidadEjecutora}"
                								                        								                                                    
                								immediate="false"
                                            	required="false"
												title="Unidad Ejecutora">
												<f:selectItem itemLabel="Todas las Unidades Ejecutoras" itemValue="0" />
                                                <f:selectItems value="#{generarTrabajadorCargoEspecificaForm.colUnidadEjecutora}" />
                                            </h:selectOneMenu>													<h:message for="unidadEjecutora" styleClass="error"/>
                						<f:verbatim></td>
                					</tr>	    									
                					<tr>
                						<td>
                							Asignar a
                						</td>
                						<td></f:verbatim>
                                            <h:selectOneMenu value="#{generarTrabajadorCargoEspecificaForm.tipo}"
                                                id="tipo"
                                            	rendered="#{generarTrabajadorCargoEspecificaForm.showTipo}" 
                								onchange="this.form.submit()"                                                     	
                								immediate="false"
                                            	required="false"
                                            	valueChangeListener="#{generarTrabajadorCargoEspecificaForm.changeTipo}"
												title="Relacionada a">
												<f:selectItem itemLabel="Proyecto" itemValue="P" />
												<f:selectItem itemLabel="Accion Centralizada" itemValue="A" />
                                            </h:selectOneMenu>													
                						<f:verbatim></td>
                					</tr></f:verbatim>                					
                				 
                					<f:verbatim><tr>
                						<td>
                							Accion Especifica
                						</td>
                						<td></f:verbatim>
                                            <h:selectOneMenu value="#{generarTrabajadorCargoEspecificaForm.selectUelEspecifica}"
												id="accionEspecifica"
                                            	onchange="this.form.submit()"     
                                            	valueChangeListener="#{generarTrabajadorCargoEspecificaForm.changeUelEspecifica}"
                								rendered="#{generarTrabajadorCargoEspecificaForm.showAccionEspecifica}"                       								                                                    	
                								immediate="false"
                                            	required="false"
												title="Accion Especifica">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarTrabajadorCargoEspecificaForm.colUelEspecifica}" />
                                            </h:selectOneMenu>													
                                            
                						<f:verbatim></td>
                					</tr></f:verbatim>
									
									<tr>
    									<td width="20%">
    										Porcentaje
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	                        							size="15"
	                        							maxlength="15"
                        								value="#{generarTrabajadorCargoEspecificaForm.porcentaje}"                        								            
                        								id="Porcentaje"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>							
											</h:inputText>
    									</td>
									</tr>
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										
    											action="#{generarTrabajadorCargoEspecificaForm.generate}"
    											/>
    									<td>
    								</tr>    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>