<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{uelEspecificaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formUelEspecifica">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Acciones Espec�ficas por Unidades Ejecutoras
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/sigecof/UelEspecifica.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{uelEspecificaForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!uelEspecificaForm.editing&&!uelEspecificaForm.deleting&&uelEspecificaForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchUelEspecifica"
								rendered="#{!uelEspecificaForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim><tr>
												<td>
													A�o
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="4"
														maxlength="4"
														value="#{uelEspecificaForm.findAnio}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						
														 />

															
												<f:verbatim></td>
											</tr></f:verbatim>
														<f:verbatim>
														
															<tr>
												<td>
													Unidad Ejecutora
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{uelEspecificaForm.findSelectUnidadEjecutora}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{uelEspecificaForm.findColUnidadEjecutora}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{uelEspecificaForm.findUelEspecificaByUnidadEjecutora}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Categoria Presupuesto
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="15"
														maxlength="15"
														value="#{uelEspecificaForm.findCategoriaPresupuesto}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{uelEspecificaForm.findUelEspecificaByCategoriaPresupuesto}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
						
							<f:subview
								id="viewResultUelEspecificaByUnidadEjecutora"
								rendered="#{uelEspecificaForm.showUelEspecificaByUnidadEjecutora&&
								!uelEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{uelEspecificaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{uelEspecificaForm.selectUelEspecifica}"
                                						styleClass="listitem">
                                						<f:param name="idUelEspecifica" value="#{result.idUelEspecifica}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultUelEspecificaByCategoriaPresupuesto"
								rendered="#{uelEspecificaForm.showUelEspecificaByCategoriaPresupuesto&&
								!uelEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{uelEspecificaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{uelEspecificaForm.selectUelEspecifica}"
                                						styleClass="listitem">
                                						<f:param name="idUelEspecifica" value="#{result.idUelEspecifica}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
</h:form>
							<f:subview 
								id="viewDataUelEspecifica"
								rendered="#{uelEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!uelEspecificaForm.editing&&!uelEspecificaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{uelEspecificaForm.editing&&!uelEspecificaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{uelEspecificaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{uelEspecificaForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="uelEspecificaForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{uelEspecificaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!uelEspecificaForm.editing&&!uelEspecificaForm.deleting&&uelEspecificaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{uelEspecificaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!uelEspecificaForm.editing&&!uelEspecificaForm.deleting&&uelEspecificaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{uelEspecificaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{uelEspecificaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{uelEspecificaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{uelEspecificaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{uelEspecificaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{uelEspecificaForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
											<f:verbatim><tr>
                        						<td>
                        							Unidad Ejecutora
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{uelEspecificaForm.selectUnidadEjecutora}"
														id="unidadEjecutora"
                                                    	disabled="#{!uelEspecificaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Unidad Ejecutora">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{uelEspecificaForm.colUnidadEjecutora}" />
                                                    </h:selectOneMenu>													<h:message for="unidadEjecutora" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:subview id="viewAnio"
												rendered="#{uelEspecificaForm.adding}">
                        						<f:verbatim><tr>
												<td>
													A�o
												</td>
												<td width="100%"></f:verbatim>
													<h:inputText size="4"
														disabled="#{!uelEspecificaForm.editing}"
														maxlength="4"
														value="#{uelEspecificaForm.findAnio}"
														onchange="this.submit()"		
														onkeypress="return keyIntegerCheck(event, this)"                        								                        																						
														 />
													<h:commandButton image="/images/find.gif" 
														action="#{uelEspecificaForm.findProyectoAccionByAnio}"
														onclick="javascript:return clickMade()" 
														disabled="#{!uelEspecificaForm.editing}"/>
														
												<f:verbatim></td>
											</tr></f:verbatim>
											
											<f:verbatim><tr>
                        						<td>
                        							Pertenece a
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{uelEspecificaForm.tipo}"
	                                                    id="tipo"
                                                    	disabled="#{!uelEspecificaForm.editing}"
                        								onchange="this.form.submit()"                                                     	
                        								immediate="false"
                                                    	required="false"
                                                    	valueChangeListener="#{uelEspecificaForm.changeTipo}"
														title="Relacionada a">
														<f:selectItem itemLabel="Proyecto" itemValue="P" />
														<f:selectItem itemLabel="Accion Centralizada" itemValue="A" />
                                                    </h:selectOneMenu>													
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Proyecto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{uelEspecificaForm.selectProyecto}"
														id="proyecto"
                                                    	disabled="#{!uelEspecificaForm.editing}"
                        								rendered="#{uelEspecificaForm.showProyecto}"                       								                                                    	
                        								immediate="false"
                                                    	required="false"
                                                    	onchange="this.form.submit()"  
                                                    	valueChangeListener="#{uelEspecificaForm.changeProyecto}"         
														title="Proyecto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{uelEspecificaForm.colProyecto}" />
                                                    </h:selectOneMenu>													
                                                   
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Accion Centralizada
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{uelEspecificaForm.selectAccionCentralizada}"
														id="accionCentralizada"
                                                    	disabled="#{!uelEspecificaForm.editing}"
                        								rendered="#{uelEspecificaForm.showAccionCentralizada}"                       								                                                    	
                        								immediate="false"
                                                    	required="false"
                                                    	onchange="this.form.submit()"     
                                                    	valueChangeListener="#{uelEspecificaForm.changeAccionCentralizada}"         
														title="Accion Centralizada">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{uelEspecificaForm.colAccionCentralizada}" />
                                                    </h:selectOneMenu>													
                                                    
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				 </f:subview>
                        					<f:verbatim><tr>
                        						<td>
                        							Accion Especifica
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{uelEspecificaForm.selectAccionEspecifica}"
														id="accionEspecifica"
                                                    	disabled="#{!uelEspecificaForm.editing}"
                        								rendered="#{uelEspecificaForm.showAccionEspecifica}"                       								                                                    	
                        								immediate="false"
                                                    	required="false"
														title="Accion Especifica">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{uelEspecificaForm.colAccionEspecifica}" />
                                                    </h:selectOneMenu>													<h:message for="accionEspecifica" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Categoria Presupuesto
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
	                        							
                        								value="#{uelEspecificaForm.uelEspecifica.categoriaPresupuesto}"
                        								
                        								                        								                        								                        								>
                        							</h:outputText>
                        								
                        																					
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							A�o
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
	                        							
                        								value="#{uelEspecificaForm.uelEspecifica.anio}"
                        								             								                        																						                        								>
                        							</h:outputText>
                        								
                        																				
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{uelEspecificaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!uelEspecificaForm.editing&&!uelEspecificaForm.deleting&&uelEspecificaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{uelEspecificaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!uelEspecificaForm.editing&&!uelEspecificaForm.deleting&&uelEspecificaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{uelEspecificaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{uelEspecificaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{uelEspecificaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{uelEspecificaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{uelEspecificaForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{uelEspecificaForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>