<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{cargoEspecificaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formCargoEspecifica">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Asignar Cargo a Proyecto/Acci�n
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/sigecof/CargoEspecifica.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{cargoEspecificaForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!cargoEspecificaForm.editing&&!cargoEspecificaForm.deleting&&cargoEspecificaForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchCargoEspecifica"
								rendered="#{!cargoEspecificaForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														
															<f:verbatim><tr>
												<td>
													A�o
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="4"
														maxlength="4"
														value="#{cargoEspecificaForm.findAnio}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						
														 />

															
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim><tr>
                        						<td>
                        							Registro
                        						</td>
                        						<td></f:verbatim>
				                                                    <h:selectOneMenu value="#{cargoEspecificaForm.findSelectRegistroForRegistroCargos}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{cargoEspecificaForm.findChangeRegistroForRegistroCargos}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{cargoEspecificaForm.findColRegistroForRegistroCargos}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
																<f:verbatim><tr>
												<td>
													Cargo
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{cargoEspecificaForm.findSelectRegistroCargos}"
                                                    	rendered="#{cargoEspecificaForm.findShowRegistroCargos}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{cargoEspecificaForm.findColRegistroCargos}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{cargoEspecificaForm.findCargoEspecificaByRegistroCargos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="showCargoEspecificaByRegistroCargos"
								rendered="#{cargoEspecificaForm.showCargoEspecificaByRegistroCargos&&
								!cargoEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{cargoEspecificaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{cargoEspecificaForm.selectCargoEspecifica}"
                                						styleClass="listitem">
                                						<f:param name="idCargoEspecifica" value="#{result.idCargoEspecifica}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultCargoEspecificaByAnio"
								rendered="#{cargoEspecificaForm.showCargoEspecificaByAnio&&
								!cargoEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{cargoEspecificaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{cargoEspecificaForm.selectCargoEspecifica}"
                                						styleClass="listitem">
                                						<f:param name="idCargoEspecifica" value="#{result.idCargoEspecifica}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataCargoEspecifica"
								rendered="#{cargoEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!cargoEspecificaForm.editing&&!cargoEspecificaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{cargoEspecificaForm.editing&&!cargoEspecificaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{cargoEspecificaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{cargoEspecificaForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="cargoEspecificaForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{cargoEspecificaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!cargoEspecificaForm.editing&&!cargoEspecificaForm.deleting&&cargoEspecificaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{cargoEspecificaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!cargoEspecificaForm.editing&&!cargoEspecificaForm.deleting&&cargoEspecificaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{cargoEspecificaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{cargoEspecificaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{cargoEspecificaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{cargoEspecificaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{cargoEspecificaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{cargoEspecificaForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
												 </f:verbatim>
											<f:subview 
            								id="ProyectoAccion"
            								rendered="#{cargoEspecificaForm.adding}">
            								<f:verbatim>
											<tr>
                        						<td>
                        							Registro
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{cargoEspecificaForm.selectRegistroForRegistroCargos}"
                                                    	disabled="#{!cargoEspecificaForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="RegistroForRegistroCargos"
														valueChangeListener="#{cargoEspecificaForm.changeRegistroForRegistroCargos}"
                        																						immediate="false"
														required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{cargoEspecificaForm.colRegistroForRegistroCargos}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{cargoEspecificaForm.selectRegistroCargos}"
	                                                    id="registroCargos"
                                                    	disabled="#{!cargoEspecificaForm.editing}"
                                                    	rendered="#{cargoEspecificaForm.showRegistroCargos}"
                                                    	onchange="this.form.submit()"
                                                    	valueChangeListener="#{cargoEspecificaForm.changeRegistroCargos}"
														
                                                    	immediate="false"
                                                    	required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{cargoEspecificaForm.colRegistroCargos}" />
                                                    </h:selectOneMenu>													
                                                    <h:message for="registroCargos" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
            								<f:verbatim><tr>
                        						<td>
                        							Pertenece a
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{cargoEspecificaForm.tipo}"
	                                                    id="tipo"                                                    	
                        								onchange="this.form.submit()"                                                     	
                        								immediate="false"
                                                    	required="false"
                                                    	valueChangeListener="#{cargoEspecificaForm.changeTipo}"
														title="Relacionada a">
														<f:selectItem itemLabel="Proyecto" itemValue="P" />
														<f:selectItem itemLabel="Accion Centralizada" itemValue="A" />
                                                    </h:selectOneMenu>													
                        						<f:verbatim></td>
                        					</tr></f:verbatim>                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Categor�a Presupuestaria
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{cargoEspecificaForm.selectUelEspecifica}"
                                                    	disabled="#{!cargoEspecificaForm.editing}"
                                                    	rendered="#{cargoEspecificaForm.showUelEspecifica}"   
                                                    	immediate="false"
	                        							id="uelEspecifica"
                        								                        								                                                    	required="false"
														title="Categor�a Presupuestaria">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cargoEspecificaForm.colUelEspecifica}" />
                                                    </h:selectOneMenu>												<h:message for="uelEspecifica" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            
                        					</f:verbatim>
                        					 </f:subview>
                        					 <f:subview 
            								id="ProyectoAccionNoAdd"
            								rendered="#{!cargoEspecificaForm.adding}">
            								
            								                					
                        					<f:verbatim><tr>
                        						<td>
                        							Categor�a Presupuestaria
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
	                        							
                        								value="#{cargoEspecificaForm.cargoEspecifica.uelEspecifica}"
                        								                        								
                        								id="uelEspecifica2"
                        								
                        								                        																						 >
                        							</h:outputText>
                        																			
                        						<f:verbatim></td>
                        					</tr>                            
                        					</f:verbatim>
                        					 </f:subview>
											
                        					<f:verbatim><tr>
                        						<td>
                        							% de Asignaci�n
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="5"
	                        							id="porcentaje"
	                        							maxlength="3"
                        								value="#{cargoEspecificaForm.cargoEspecifica.porcentaje}"
                        								readonly="#{!cargoEspecificaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="porcentaje" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					                        					<f:verbatim><tr>
                        						<td>
                        							A�o
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
	                        							
                        								value="#{cargoEspecificaForm.cargoEspecifica.anio}"
                        								                        								                        																						                        								>
                        							</h:outputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="anio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{cargoEspecificaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!cargoEspecificaForm.editing&&!cargoEspecificaForm.deleting&&cargoEspecificaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{cargoEspecificaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!cargoEspecificaForm.editing&&!cargoEspecificaForm.deleting&&cargoEspecificaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{cargoEspecificaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{cargoEspecificaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{cargoEspecificaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{cargoEspecificaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{cargoEspecificaForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{cargoEspecificaForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>