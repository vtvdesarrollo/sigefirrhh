<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{partidaUelEspecificaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formPartidaUelEspecifica">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Fuentes de Financiamiento por Partidas
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/sigecof/PartidaUelEspecifica.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{partidaUelEspecificaForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!partidaUelEspecificaForm.editing&&!partidaUelEspecificaForm.deleting&&partidaUelEspecificaForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchPartidaUelEspecifica"
								rendered="#{!partidaUelEspecificaForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Uel Especifica
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{partidaUelEspecificaForm.findSelectUelEspecifica}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{partidaUelEspecificaForm.findColUelEspecifica}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{partidaUelEspecificaForm.findPartidaUelEspecificaByUelEspecifica}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Cuenta Presupuesto
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{partidaUelEspecificaForm.findSelectCuentaPresupuesto}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{partidaUelEspecificaForm.findColCuentaPresupuesto}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{partidaUelEspecificaForm.findPartidaUelEspecificaByCuentaPresupuesto}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Fuente Financiamiento
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{partidaUelEspecificaForm.findSelectFuenteFinanciamiento}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{partidaUelEspecificaForm.findColFuenteFinanciamiento}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{partidaUelEspecificaForm.findPartidaUelEspecificaByFuenteFinanciamiento}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													A�o
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="4"
														maxlength="4"
														value="#{partidaUelEspecificaForm.findAnio}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{partidaUelEspecificaForm.findPartidaUelEspecificaByAnio}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultPartidaUelEspecificaByUelEspecifica"
								rendered="#{partidaUelEspecificaForm.showPartidaUelEspecificaByUelEspecifica&&
								!partidaUelEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{partidaUelEspecificaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{partidaUelEspecificaForm.selectPartidaUelEspecifica}"
                                						styleClass="listitem">
                                						<f:param name="idPartidaUelEspecifica" value="#{result.idPartidaUelEspecifica}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultPartidaUelEspecificaByCuentaPresupuesto"
								rendered="#{partidaUelEspecificaForm.showPartidaUelEspecificaByCuentaPresupuesto&&
								!partidaUelEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{partidaUelEspecificaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{partidaUelEspecificaForm.selectPartidaUelEspecifica}"
                                						styleClass="listitem">
                                						<f:param name="idPartidaUelEspecifica" value="#{result.idPartidaUelEspecifica}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultPartidaUelEspecificaByFuenteFinanciamiento"
								rendered="#{partidaUelEspecificaForm.showPartidaUelEspecificaByFuenteFinanciamiento&&
								!partidaUelEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{partidaUelEspecificaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{partidaUelEspecificaForm.selectPartidaUelEspecifica}"
                                						styleClass="listitem">
                                						<f:param name="idPartidaUelEspecifica" value="#{result.idPartidaUelEspecifica}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultPartidaUelEspecificaByAnio"
								rendered="#{partidaUelEspecificaForm.showPartidaUelEspecificaByAnio&&
								!partidaUelEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{partidaUelEspecificaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{partidaUelEspecificaForm.selectPartidaUelEspecifica}"
                                						styleClass="listitem">
                                						<f:param name="idPartidaUelEspecifica" value="#{result.idPartidaUelEspecifica}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataPartidaUelEspecifica"
								rendered="#{partidaUelEspecificaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!partidaUelEspecificaForm.editing&&!partidaUelEspecificaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{partidaUelEspecificaForm.editing&&!partidaUelEspecificaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{partidaUelEspecificaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{partidaUelEspecificaForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="partidaUelEspecificaForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{partidaUelEspecificaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!partidaUelEspecificaForm.editing&&!partidaUelEspecificaForm.deleting&&partidaUelEspecificaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{partidaUelEspecificaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!partidaUelEspecificaForm.editing&&!partidaUelEspecificaForm.deleting&&partidaUelEspecificaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{partidaUelEspecificaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{partidaUelEspecificaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{partidaUelEspecificaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{partidaUelEspecificaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{partidaUelEspecificaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{partidaUelEspecificaForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Uel Especifica
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{partidaUelEspecificaForm.selectUelEspecifica}"
														id="uelEspecifica"
                                                    	disabled="#{!partidaUelEspecificaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Uel Especifica">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{partidaUelEspecificaForm.colUelEspecifica}" />
                                                    </h:selectOneMenu>													<h:message for="uelEspecifica" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cuenta Presupuesto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{partidaUelEspecificaForm.selectCuentaPresupuesto}"
														id="cuentaPresupuesto"
                                                    	disabled="#{!partidaUelEspecificaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Cuenta Presupuesto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{partidaUelEspecificaForm.colCuentaPresupuesto}" />
                                                    </h:selectOneMenu>													<h:message for="cuentaPresupuesto" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fuente Financiamiento
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{partidaUelEspecificaForm.selectFuenteFinanciamiento}"
														id="fuenteFinanciamiento"
                                                    	disabled="#{!partidaUelEspecificaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Fuente Financiamiento">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{partidaUelEspecificaForm.colFuenteFinanciamiento}" />
                                                    </h:selectOneMenu>													<h:message for="fuenteFinanciamiento" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							A�o
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="anio"
	                        							maxlength="4"
                        								value="#{partidaUelEspecificaForm.partidaUelEspecifica.anio}"
                        								readonly="#{!partidaUelEspecificaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="anio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{partidaUelEspecificaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!partidaUelEspecificaForm.editing&&!partidaUelEspecificaForm.deleting&&partidaUelEspecificaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{partidaUelEspecificaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!partidaUelEspecificaForm.editing&&!partidaUelEspecificaForm.deleting&&partidaUelEspecificaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{partidaUelEspecificaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{partidaUelEspecificaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{partidaUelEspecificaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{partidaUelEspecificaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{partidaUelEspecificaForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{partidaUelEspecificaForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>