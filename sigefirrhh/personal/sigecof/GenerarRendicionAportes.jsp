<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{generarRendicionAportesForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">

				<h:form id="formGenerarRendicionAportes"> 
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Generar Rendici�n Aportes
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/sigecof/GenerarRendicionAportes.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    							<tr>
    									<td>
    										Unidad Administradora
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarRendicionAportesForm.selectUnidadAdministradora}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarRendicionAportesForm.changeUnidadAdministradora}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{generarRendicionAportesForm.listUnidadAdministradora}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
	    							<tr>
    									<td>
    										Categoria de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarRendicionAportesForm.selectCategoriaPresupuesto}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarRendicionAportesForm.changeCategoriaPresupuesto}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarRendicionAportesForm.listCategoriaPresupuesto}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarRendicionAportesForm.selectTipoPersonal}"
	    										rendered="#{generarRendicionAportesForm.showTipoPersonal}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarRendicionAportesForm.changeTipoPersonal}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarRendicionAportesForm.listTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Aporte
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarRendicionAportesForm.selectConceptoAporte}"
	    										rendered="#{generarRendicionAportesForm.showAporte}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarRendicionAportesForm.changeConceptoAporte}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarRendicionAportesForm.listConceptoAporte}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    									
    									
    								<tr>
    									<td>
    										Periodo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarRendicionAportesForm.periodo}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="1ra. Semana" itemValue="4" /> 
                                            	<f:selectItem itemLabel="2da. Semana" itemValue="5" /> 
                                            	<f:selectItem itemLabel="3ra. Semana" itemValue="6" /> 
                                            	<f:selectItem itemLabel="4ta. Semana" itemValue="7" /> 
                                            	<f:selectItem itemLabel="5ta. Semana" itemValue="8" /> 
                                            	<f:selectItem itemLabel="1ra. Quincena(Semanas Comprendidas)" itemValue="1" /> 
                                            	<f:selectItem itemLabel="2da. Quincena(Semanas Comprendidas)" itemValue="2" /> 
                                            	<f:selectItem itemLabel="Todo el Mes" itemValue="3" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>				
    								
    									
    													
    								<tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
												size="3"
												maxlength="2"
    											rendered="#{generarRendicionAportesForm.show}"

    											id="Mes"

    											value="#{generarRendicionAportesForm.mes}"    											
											/>    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{generarRendicionAportesForm.show}"
    											maxlength="4"
    											size="5"
    											id="Anio"
    											
    											value="#{generarRendicionAportesForm.anio}"    											
    											 />    								
    									</td>
									</tr>								
																
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										rendered="#{generarRendicionAportesForm.show}"
    											action="#{generarRendicionAportesForm.generate}"
    											/>
    									<td>
    								</tr>    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>