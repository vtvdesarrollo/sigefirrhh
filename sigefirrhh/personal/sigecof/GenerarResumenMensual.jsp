<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>
 
<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{generarResumenMensualForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	               />
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formGenerarResumenMensual">
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Generar Resumen Mensual
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/sigecof/GenerarResumenMensual.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    							<tr>
    									<td>
    										Unidad Admninistradora
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarResumenMensualForm.selectUnidadAdministradora}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarResumenMensualForm.changeUnidadAdministradora}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{generarResumenMensualForm.listUnidadAdministradora}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
	    							<tr>
    									<td>
    										Categoria de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarResumenMensualForm.selectCategoriaPresupuesto}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarResumenMensualForm.changeCategoriaPresupuesto}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarResumenMensualForm.listCategoriaPresupuesto}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarResumenMensualForm.selectTipoPersonal}"
	    										rendered="#{generarResumenMensualForm.showTipoPersonal}"
	    										onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarResumenMensualForm.changeTipoPersonal}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Todos" itemValue="0" />
                                                <f:selectItems value="#{generarResumenMensualForm.listTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    								<tr>
    									<td>
    										Tipo Nomina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarResumenMensualForm.tipoNomina}"	    										
    											rendered="#{generarResumenMensualForm.show}"
	    										onchange="this.form.submit()"
                                            	immediate="true"
                                            	valueChangeListener="#{generarResumenMensualForm.changeTipoNomina}">
                                            	<f:selectItem itemLabel="Ordinaria" itemValue="O" />  
                                            	<f:selectItem itemLabel="Especial" itemValue="E" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td>
    										N�mina Especial
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarResumenMensualForm.selectNominaEspecial}"
                                            	immediate="true"
                                            	onchange="this.form.submit()" 
                                            	rendered="#{generarResumenMensualForm.showNominaEspecial}" 
                                            	valueChangeListener="#{generarResumenMensualForm.changeNominaEspecial}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarResumenMensualForm.listNominaEspecial}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>								
    								<tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="3"
    											maxlength="2"
    											rendered="#{generarResumenMensualForm.show}"
    											
    											id="Mes"
    											
    											value="#{generarResumenMensualForm.mes}"    											
    											 />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{generarResumenMensualForm.show}"
												size="5"
												maxlength="4"
    											id="Anio"

    											value="#{generarResumenMensualForm.anio}"    											
    											 />    								
    									</td>
									</tr>								
																
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										rendered="#{generarResumenMensualForm.show}"
    											action="#{generarResumenMensualForm.generate}"
    											/>
    									<td>
    								</tr>    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>