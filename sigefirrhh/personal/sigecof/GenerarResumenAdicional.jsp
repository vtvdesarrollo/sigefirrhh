<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{generarResumenAdicionalForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formGenerarResumenAdicional">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Generar Resumen Complementario
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/sigecof/GenerarResumenAdicional.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchResumenMensual">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
										<tr>
                        					<td>
                        							A�o
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{generarResumenAdicionalForm.findAnio}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Mes
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="5"
														maxlength="2"
														value="#{generarResumenAdicionalForm.findMes}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
												<td>
													Tipo Personal
												</td>
												<td width="100%"></f:verbatim>
					                                <h:selectOneMenu value="#{generarResumenAdicionalForm.findSelectTipoPersonal}"
					                                	valueChangeListener="#{generarResumenAdicionalForm.changeFindTipoPersonal}"
					                                	onchange="this.form.submit()"  
					                                	id="findTipoPersonal" 
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{generarResumenAdicionalForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>																		
												<f:verbatim></td>
											</tr>			
											<tr>
												<td>
													Concepto
												</td>
												<td width="100%"></f:verbatim>
					                                <h:selectOneMenu value="#{generarResumenAdicionalForm.findSelectConceptoTipoPersonal}"
					                                	id="findConceptoTipoPersonal" 
					                                	rendered="#{generarResumenAdicionalForm.showConcepto}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0"/>
                                                        <f:selectItems value="#{generarResumenAdicionalForm.findColConceptoTipoPersonal}"/>
                                                    </h:selectOneMenu>																		
												<f:verbatim></td>
											</tr>						
											<tr>
												<td>
													Unidad Administradora
												</td>
												<td width="100%"></f:verbatim>
					                                <h:selectOneMenu value="#{generarResumenAdicionalForm.findSelectUnidadAdministradora}"
					                                	valueChangeListener="#{generarResumenAdicionalForm.changeFindUnidadAdministradora}"
					                                	onchange="this.form.submit()"  
					                                	id="findUnidadAdministradora" 
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{generarResumenAdicionalForm.findColUnidadAdministradora}" />
                                                    </h:selectOneMenu>
																		
												<f:verbatim></td>
											</tr>											
											<tr>
												<td>
													Unidad Ejecutora
												</td>
												<td width="100%"></f:verbatim>
					                                 <h:selectOneMenu value="#{generarResumenAdicionalForm.findSelectAdministradoraUel}"
					                                 rendered="#{generarResumenAdicionalForm.showFindAdministradoraUel}"
					                                 valueChangeListener="#{generarResumenAdicionalForm.changeFindAdministradoraUel}"
					                                 onchange="this.form.submit()"   
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{generarResumenAdicionalForm.findColAdministradoraUel}" />
                                                    </h:selectOneMenu>
													
												<f:verbatim></td>
											</tr>
											<tr>
												<td>
													Accion Especifica
												</td>
												<td width="100%"></f:verbatim>
					                                <h:selectOneMenu value="#{generarResumenAdicionalForm.findSelectUelEspecifica}"
					                                	rendered="#{generarResumenAdicionalForm.showFindUelEspecifica}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{generarResumenAdicionalForm.findColUelEspecifica}" />
                                                    </h:selectOneMenu>																
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Monto
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="19"
														maxlength="18"
														value="#{generarResumenAdicionalForm.monto}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyFloatCheck(event, this)" >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
													</h:inputText>	
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Quincena
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="5"
														maxlength="2"
														value="#{generarResumenAdicionalForm.quincena}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyIntegerCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Numero 
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="5"
														maxlength="2"
														value="#{generarResumenAdicionalForm.numero}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyIntegerCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
		    									<td colspan="2" align="center">	</f:verbatim>									
		    										<h:commandButton image="/images/run.gif"			    										
		    											action="#{generarResumenAdicionalForm.generar}"
		    											/>
		    									<f:verbatim><td>
		    								</tr>    	
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</h:form>
</f:view>
</body>
</html>