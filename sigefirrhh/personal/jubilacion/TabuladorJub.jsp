<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{tabuladorJubForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formTabuladorJub">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Parametros Jubilaci�n
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/jubilacion/TabuladorJub.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{tabuladorJubForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!tabuladorJubForm.editing&&!tabuladorJubForm.deleting&&tabuladorJubForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchTabuladorJub"
								rendered="#{!tabuladorJubForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Tipo de Personal
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{tabuladorJubForm.findSelectTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tabuladorJubForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{tabuladorJubForm.findTabuladorJubByTipoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultTabuladorJubByTipoPersonal"
								rendered="#{tabuladorJubForm.showTabuladorJubByTipoPersonal&&
								!tabuladorJubForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{tabuladorJubForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{tabuladorJubForm.selectTabuladorJub}"
                                						styleClass="listitem">
                                						<f:param name="idTabuladorJub" value="#{result.idTabuladorJub}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataTabuladorJub"
								rendered="#{tabuladorJubForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!tabuladorJubForm.editing&&!tabuladorJubForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{tabuladorJubForm.editing&&!tabuladorJubForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{tabuladorJubForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{tabuladorJubForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="tabuladorJubForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{tabuladorJubForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!tabuladorJubForm.editing&&!tabuladorJubForm.deleting&&tabuladorJubForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{tabuladorJubForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!tabuladorJubForm.editing&&!tabuladorJubForm.deleting&&tabuladorJubForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{tabuladorJubForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{tabuladorJubForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{tabuladorJubForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{tabuladorJubForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{tabuladorJubForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{tabuladorJubForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tabuladorJubForm.selectTipoPersonal}"
														id="tipoPersonal"
                                                    	disabled="#{!tabuladorJubForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Tipo de Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tabuladorJubForm.colTipoPersonal}" />
                                                    </h:selectOneMenu>													<h:message for="tipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Sexo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tabuladorJubForm.tabuladorJub.sexo}"
	                                                    id="sexo"
                                                    	disabled="#{!tabuladorJubForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Sexo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tabuladorJubForm.listSexo}" />
                                                    </h:selectOneMenu>													<h:message for="sexo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Edad Minima
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="aniosEdad"
	                        							maxlength="2"
                        								value="#{tabuladorJubForm.tabuladorJub.aniosEdad}"
                        								readonly="#{!tabuladorJubForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="aniosEdad" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Minimo A�os de Servicio
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="aniosServicio"
	                        							maxlength="2"
                        								value="#{tabuladorJubForm.tabuladorJub.aniosServicio}"
                        								readonly="#{!tabuladorJubForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="aniosServicio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							A�os de Servicio (excepcion)
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="aniosExcepcion"
	                        							maxlength="2"
                        								value="#{tabuladorJubForm.tabuladorJub.aniosExcepcion}"
                        								readonly="#{!tabuladorJubForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="aniosExcepcion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se aplica Factor?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tabuladorJubForm.tabuladorJub.aplicaFactor}"
	                                                    id="aplicaFactor"
                                                    	disabled="#{!tabuladorJubForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Se aplica Factor?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tabuladorJubForm.listAplicaFactor}" />
                                                    </h:selectOneMenu>													<h:message for="aplicaFactor" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Factor por a�o
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="factor"
	                        							maxlength="6"
                        								value="#{tabuladorJubForm.tabuladorJub.factor}"
                        								readonly="#{!tabuladorJubForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        								 rendered="#{tabuladorJubForm.showFactorAux}" 														 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="factor" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Porcentaje M�ximo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="porcentajeMaximoJubilacion"
	                        							maxlength="6"
                        								value="#{tabuladorJubForm.tabuladorJub.porcentajeMaximoJubilacion}"
                        								readonly="#{!tabuladorJubForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="porcentajeMaximoJubilacion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Base
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tabuladorJubForm.tabuladorJub.base}"
	                                                    id="base"
                                                    	disabled="#{!tabuladorJubForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Base">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tabuladorJubForm.listBase}" />
                                                    </h:selectOneMenu>													<h:message for="base" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Meses a Promediar
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="numeroMeses"
	                        							maxlength="6"
                        								value="#{tabuladorJubForm.tabuladorJub.numeroMeses}"
                        								readonly="#{!tabuladorJubForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        								 rendered="#{tabuladorJubForm.showNumeroMesesAux}" 														 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="numeroMeses" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{tabuladorJubForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!tabuladorJubForm.editing&&!tabuladorJubForm.deleting&&tabuladorJubForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{tabuladorJubForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!tabuladorJubForm.editing&&!tabuladorJubForm.deleting&&tabuladorJubForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{tabuladorJubForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{tabuladorJubForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{tabuladorJubForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{tabuladorJubForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{tabuladorJubForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{tabuladorJubForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>