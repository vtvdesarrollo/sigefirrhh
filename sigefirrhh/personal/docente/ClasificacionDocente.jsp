<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
		<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{clasificacionDocenteForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top"          
				</div>
			</td>
			<td width="570" valign="top">
			<h:form id="formClasificacionDocente">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Clasificacion (Personal Docente)
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/docente/ClasificacionDocente.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="show1"
								rendered="#{clasificacionDocenteForm.show1}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos del Movimiento
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>
											<tr>
                        						<td width="40%" align="left">
                        							Region
                        						</td>
                        						<td width="60%" align="left"></f:verbatim>
					                            	<h:selectOneMenu value="#{clasificacionDocenteForm.selectRegion}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{clasificacionDocenteForm.colRegion}" />
                                                    </h:selectOneMenu>
												<f:verbatim>
												</td>
											</tr>
											<tr>
                        						<td align="left">
                        							Tipo de Personal
                        						</td>
                        						<td align="left"></f:verbatim>
					                            	<h:selectOneMenu value="#{clasificacionDocenteForm.selectTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{clasificacionDocenteForm.colTipoPersonal}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
		                						<td align="left">Fecha Movimiento</td>
		                						<td align="left">
		                						</f:verbatim>
													<h:inputText size="16"
														value="#{clasificacionDocenteForm.fechaMovimiento}"
														immediate="false"
														onkeypress="return keyEnterCheck(event, this)"
		                        						onblur="javascript:check_date(this)"
		                        						>																																			                         						
			                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
		                                                 pattern="dd-MM-yyyy" />
		                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
		                        						                				
												<f:verbatim></td>										
											</tr>
											<tr>
                        						<td width="20%">
                        							C�dula
                        						</td>
                        						<td widht="80%"></f:verbatim>
													<h:inputText size="10"
														maxlength="8"
														value="#{clasificacionDocenteForm.cedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														required="true"
														onchange="javascript:this.value=this.value.toUpperCase()" />													
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td align="left">
                        							Tipo Movimiento
                        						</td>
                        						<td align="left"></f:verbatim>
					                            	<h:selectOneMenu value="#{clasificacionDocenteForm.tipoMovimiento}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Clasificacion - Categoria" itemValue="1" />
														<f:selectItem itemLabel="Clasificacion - Tipo Profesional" itemValue="2" />
														<f:selectItem itemLabel="Clasificacion - Asignacion Prima Postgrado" itemValue="3" />														
														<f:selectItem itemLabel="Clasificacion - Asignacion Prima Doctorado" itemValue="4" />																												
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
		                						<td align="left">Mes Ingreso Nomina</td>
		                						<td align="left">
		                						</f:verbatim>
													<h:outputText 
														value="#{clasificacionDocenteForm.mesNomina}"
		                        						>																																			                         						
		                        					</h:outputText>
		                        						                				
												<f:verbatim>
												</td>										
											</tr>
                        					<tr>
                        						<td align="left">
                        							
                        						</td>
                        						<td align="right">
                        						</f:verbatim>
													<h:commandButton image="/images/next.gif" 
														action="#{clasificacionDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>																					
							<f:subview 
								id="showDatosPersonales"
								rendered="#{clasificacionDocenteForm.showDatosPersonales}">
								<f:verbatim><tr>
									<td>										
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											
                        					<tr>
                        						<td width="15%" class="subTitle">
                        							</f:verbatim>
													<h:outputText 
														value="#{clasificacionDocenteForm.cedula} - " />													
												<f:verbatim>
                        						</td>
                        						<td width="85%" class="subTitle">
                        						</f:verbatim>
													<h:outputText																	
														value="#{clasificacionDocenteForm.personal.primerApellido}, " />	
													<h:outputText
														value="#{clasificacionDocenteForm.personal.primerNombre}"/>															
												<f:verbatim>
                        						</td>
											</tr>
																																		
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>	
							<f:subview 
								id="show2"
								rendered="#{clasificacionDocenteForm.show2}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos Cargos Actuales
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											                        					
													<tr>
		                        						<td width="20%">
		                        							Tipo Profesional
		                        						</td>
		                        						<td widht="80%"></f:verbatim>
															<h:outputText 
																value="#{clasificacionDocenteForm.gradoDocente}" />													
														<f:verbatim></td>
													</tr>
													<tr>
		                        						<td width="20%">
		                        							Jerarquia
		                        						</td>
		                        						<td widht="80%"></f:verbatim>
															<h:outputText 
																value="#{clasificacionDocenteForm.jerarquiaDocente}" />													
														<f:verbatim></td>
													</tr>
													<tr>
		                        						<td width="20%">
		                        							Categoria
		                        						</td>
		                        						<td widht="80%"></f:verbatim>
															<h:outputText 
																value="#{clasificacionDocenteForm.categoriaDocente}" />													
														<f:verbatim></td>
													</tr>
													
													<tr>
														<td colspan="2">
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
														<td colspan="2" class="sub-bandtable">
															Cargos
														</td>
													</tr>
													<tr>
		                        						<td colspan="2" class="listData">
		                        							 <h:dataTable id="tableCargosActuales"
				                                                styleClass="datatable"
				                                                headerClass="standardTable_Header"
				                                                footerClass="standardTable_Header"
				                                                rowClasses="standardTable_Row1,standardTable_Row2"
				                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
				                                                var="resultCargosActuales"
				                                				value="#{clasificacionDocenteForm.resultCargosActuales}"		                                                
				                                                width="100%">
				                                                <h:column>
					                                                <f:facet name="header">
																		<h:outputText value="Codigo" />
																	</f:facet>
				                                					<h:outputText 		
																		value="#{resultCargosActuales.cargo.codCargo}"/>
				                                				</h:column>
				                                				<h:column>
					                                				 <f:facet name="header">
																		<h:outputText value="Nombre" />
																	</f:facet>
				                                					<h:outputText 		
																		value="#{resultCargosActuales.cargo.descripcionCargo}"/>
				                                				</h:column>
				                                				<h:column>
					                                				 <f:facet name="header">
																		<h:outputText value="Horas" />
																	</f:facet>
				                                					<h:outputText 		
																		value="#{resultCargosActuales.totalHoras}"/>
				                                				</h:column>
				                                            </h:dataTable>
		                        						</td>
		                        						
													</tr>						
													</f:verbatim>
													<f:verbatim>
													<tr>
		                        						<td colspan="2" align="left">
		                        							
		                        						</td>
		                        						<td colspan="2" align="right"></f:verbatim>
															<h:commandButton image="/images/next.gif" 
																action="#{clasificacionDocenteForm.next}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													</table>
												</td>
											</tr>											
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>	
							
							<f:subview 
								id="show3"
								rendered="#{clasificacionDocenteForm.show3}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos de Clasificacion
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>
											<tr>
												<td width="40%">
													A�os de Servicio
												</td>
												<td>
													<h:outputText value="#{clasificacionDocenteForm.anios}" />	
												</td>
											</tr>
											<tr>
                        						<td>
												Nueva Categoria
                        						</td>
                        						<td width="60%"></f:verbatim>
					                            	<h:outputText value="#{clasificacionDocenteForm.nuevaCategoriaDocente}"
                                                    	/>														
												<f:verbatim>
												</td>
											</tr>
											
											<tr>
                        						<td colspan="2" align="left">
                        							
                        						</td>
                        						<td colspan="2" align="right"></f:verbatim>
													<h:commandButton image="/images/next.gif" 
														action="#{clasificacionDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>		
						
						
						<f:subview 
								id="show4"
								rendered="#{clasificacionDocenteForm.show4}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos de Clasificacion
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>
											
											<tr>
                        						<td>
													Nuevo Tipo Profesional
                        						</td>
                        						<td width="60%"></f:verbatim>
					                            	<h:selectOneMenu value="#{clasificacionDocenteForm.gradoDocenteSeleccionado}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{clasificacionDocenteForm.listGradoDocente}" />
                                                    </h:selectOneMenu>													
												<f:verbatim>
												</td>
											</tr>
											
											<tr>
                        						<td colspan="2" align="left">
                        							
                        						</td>
                        						<td colspan="2" align="right"></f:verbatim>
													<h:commandButton image="/images/next.gif" 
														action="#{clasificacionDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>	
							
								
							<f:subview 
								id="show8"
								rendered="#{clasificacionDocenteForm.showFinal}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Informaci�n final del movimiento
												</td>
											</tr>
										</table>			
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											
											<tr>
												<td>
													<b>Cedula</b>
												</td>
												<td>
													<h:outputText value="#{clasificacionDocenteForm.cedula}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Apellidos</b>
												</td>
												<td>
													<h:outputText value="#{clasificacionDocenteForm.personal.primerApellido} #{clasificacionDocenteForm.personal.segundoApellido}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Nombres</b>
												</td>
												<td>
													<h:outputText value="#{clasificacionDocenteForm.personal.primerNombre} #{clasificacionDocenteForm.personal.segundoNombre}" />	
												</td>
											</tr>											
											<tr>
												<td>
													<b>Tipo de Profesional</b>
												</td>
												<td>
													<h:outputText value="#{clasificacionDocenteForm.gradoDocente}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Jerarquia</b>
												</td>
												<td>
													<h:outputText value="#{clasificacionDocenteForm.jerarquiaDocente}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Categoria</b>
												</td>
												<td>
													<h:outputText value="#{clasificacionDocenteForm.categoriaDocente}" />	
												</td>
											</tr>						
											<tr>
												<td>
													<b>A�os de Servicio</b>
												</td>
												<td>
													<h:outputText value="#{clasificacionDocenteForm.anios}" />	
												</td>
											</tr>
											
											<tr>
												<td colspan="2" class="sub-bandtable">
													Cargos y Conceptos Actuales
												</td>
											</tr>											
													
                        						<td colspan="2"></f:verbatim>
                        							 <h:dataTable id="tableCargosActuales"
		                                                styleClass="datatable"
		                                                headerClass="standardTable_Header"
		                                                footerClass="standardTable_Header"
		                                                rowClasses="standardTable_Row1,standardTable_Row2"
		                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
		                                                var="resultCargoActual"
		                                				value="#{clasificacionDocenteForm.colCargos}"		                                                
		                                                width="100%">
		                                				<h:column>
		                                					<f:facet name="header">
																<h:outputText value="Cargo" />
															</f:facet>
		                                				
		                                					<h:outputText 		
																value="#{resultCargoActual.cargoActual.codCargo}"/>
														</h:column>
														<h:column>
															<h:dataTable id="tableConceptosActuales"
				                                                styleClass="datatable"
				                                                headerClass="standardTable_Header"
				                                                footerClass="standardTable_Header"
				                                                rowClasses="standardTable_Row1,standardTable_Row2"
				                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
				                                                var="resultConceptoActual"
				                                				value="#{resultCargoActual.conceptosActuales}"		                                                
				                                                width="100%">
				                                				
																<h:column>
																	<f:facet name="header">
																		<h:outputText value="Descripcion" />
																	</f:facet>
																	<h:outputText 		
																		value="#{resultConceptoActual}"/>
																</h:column>																
				                                         	</h:dataTable>
			                                           </h:column>	
			                                    	</h:dataTable><f:verbatim>													
                        						</td>                        					
											</tr>
											<tr>
												<td colspan="2" class="sub-bandtable">
													Cargos y Conceptos Nuevos
												</td>
											</tr>											
													
                        						<td colspan="2"></f:verbatim>
                        							 <h:dataTable id="tableCargosNuevos"
		                                                styleClass="datatable"
		                                                headerClass="standardTable_Header"
		                                                footerClass="standardTable_Header"
		                                                rowClasses="standardTable_Row1,standardTable_Row2"
		                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
		                                                var="resultCargoNuevo"
		                                				value="#{clasificacionDocenteForm.colCargos}"		                                                
		                                                width="100%">
		                                				<h:column>
		                                					<f:facet name="header">
																<h:outputText value="Cargo" />
															</f:facet>
		                                				
		                                					<h:outputText 		
																value="#{resultCargoNuevo.cargoNuevo.codCargo}"/>
														</h:column>
														<h:column>
															<h:dataTable id="tableConceptosNuevos"
				                                                styleClass="datatable"
				                                                headerClass="standardTable_Header"
				                                                footerClass="standardTable_Header"
				                                                rowClasses="standardTable_Row1,standardTable_Row2"
				                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
				                                                var="resultConceptoNuevo"
				                                				value="#{resultCargoNuevo.conceptosNuevos}"		                                                
				                                                width="100%">
				                                				
																<h:column>
																	<f:facet name="header">
																		<h:outputText value="Descripcion" />
																	</f:facet>
																	<h:outputText 		
																		value="#{resultConceptoNuevo}"/>
																</h:column>																
				                                         	</h:dataTable>
			                                           </h:column>	
			                                    	</h:dataTable><f:verbatim>													
                        						</td>                        					
											</tr>								
											<tr>
                        						<td colspan="2" align="right"></f:verbatim>
													
													<h:commandButton image="/images/run.gif" 	
														rendered="#{!clasificacionDocenteForm.showConfirmacion}"													
														action="#{clasificacionDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												
												<h:outputText
			                        					value="�Esta seguro que desea realizar este proceso?"
			                        					rendered="#{clasificacionDocenteForm.showConfirmacion}" />                							                        				                				
			                        				<h:commandButton value="Si" 
			                        					image="/images/yes.gif"
			                        					action="#{clasificacionDocenteForm.next}"
			                        					onclick="javascript:return clickMade()"
			                        					rendered="#{clasificacionDocenteForm.showConfirmacion}" />                        					                        			                        				
			                        				<h:commandButton value="Cancelar" 
			                        					image="/images/cancel.gif"
			                        					action="#{clasificacionDocenteForm.abort}"
			                        					immediate="true"
			                        					rendered="#{clasificacionDocenteForm.showConfirmacion}"
			                        					onclick="javascript:return clickMade()"
			                	        				 />
			                	        		<f:verbatim>				    								
			    								</td>
											</tr>
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>																																					
						</td>
					</tr>
				</table>

				</h:form>

			</td>			
		</tr>
	</table>

</f:view>

</body>
</html>