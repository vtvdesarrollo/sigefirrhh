<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
		<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{ingresoDocenteForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top"          
				</div>
			</td>
			<td width="570" valign="top">
			<h:form id="formIngresoDocente">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Ingreso (Personal Docente)
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/docente/IngresoDocente.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="show1"
								rendered="#{ingresoDocenteForm.show1}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos del Movimiento
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>
											<tr>
                        						<td width="40%" align="left">
                        							Region
                        						</td>
                        						<td width="60%" align="left"></f:verbatim>
					                            	<h:selectOneMenu value="#{ingresoDocenteForm.selectRegion}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{ingresoDocenteForm.colRegion}" />
                                                    </h:selectOneMenu>
												<f:verbatim>
												</td>
											</tr>
											<tr>
                        						<td align="left">
                        							Tipo de Personal
                        						</td>
                        						<td align="left"></f:verbatim>
					                            	<h:selectOneMenu value="#{ingresoDocenteForm.selectTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{ingresoDocenteForm.colTipoPersonal}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
		                						<td align="left">Fecha Movimiento</td>
		                						<td align="left">
		                						</f:verbatim>
													<h:inputText size="16"
														value="#{ingresoDocenteForm.fechaMovimiento}"
														immediate="false"
														onkeypress="return keyEnterCheck(event, this)"
		                        						onblur="javascript:check_date(this)"
		                        						>																																			                         						
			                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
		                                                 pattern="dd-MM-yyyy" />
		                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
		                        						                				
												<f:verbatim></td>										
											</tr>
											<tr>
		                						<td align="left">Mes Ingreso Nomina</td>
		                						<td align="left">
		                						</f:verbatim>
													<h:outputText 
														value="#{ingresoDocenteForm.mesNomina}"
		                        						>																																			                         						
		                        					</h:outputText>
		                        						                				
												<f:verbatim>
												</td>										
											</tr>
                        					<tr>
                        						<td align="left">
                        							
                        						</td>
                        						<td align="right">
                        						</f:verbatim>
													<h:commandButton image="/images/next.gif" 
														action="#{ingresoDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>																					
							
							<f:subview 
								id="show2"
								rendered="#{ingresoDocenteForm.show2}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos Personales
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											
                        					<tr>
                        						<td width="20%">
                        							C�dula
                        						</td>
                        						<td widht="80%"></f:verbatim>
													<h:inputText size="10"
														maxlength="8"
														value="#{ingresoDocenteForm.cedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														required="true"
														onchange="javascript:this.value=this.value.toUpperCase()" />													
												<f:verbatim></td>
											</tr>
											
											<tr>
												<td colspan="2">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td width="20%" class="datatable">
																Primer Apellido
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="20"
																	id="PrimerApellido"
																	maxlength="20"
																	value="#{ingresoDocenteForm.primerApellido}"																	
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"                        								
																	required="true" />
															<f:verbatim>
															</td>
															<td width="20%" class="datatable">
																Segundo Apellido
															</td>
															<td width="30%">
															</f:verbatim>
																<h:inputText
																	size="20"
																	id="SegundoApellido"
																	maxlength="20"
																	value="#{ingresoDocenteForm.segundoApellido}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"                        								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
															<td  class="datatable">
																Primer Nombre
															</td>
															<td >
															</f:verbatim>
																<h:inputText
																	size="20"
																	id="PrimerNombre"
																	maxlength="20"
																	value="#{ingresoDocenteForm.primerNombre}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"                        								
																	required="true" />																
															<f:verbatim>
															</td>
															<td class="datatable">
																Segundo Nombre
															</td>
															<td >
															</f:verbatim>
																<h:inputText
																	size="20"
																	id="SegundoNombre"
																	maxlength="20"
																	value="#{ingresoDocenteForm.segundoNombre}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"                        								
																	required="false" />
															<f:verbatim>
															</td>
														</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
															<td class="datatable">
																Sexo
															</td>
															<td>
															</f:verbatim>
																<h:selectOneMenu value="#{ingresoDocenteForm.sexo}"
																	id="Sexo"
																	immediate="false"
																	required="true"
																	title="Sexo">																	
																	<f:selectItems value="#{ingresoDocenteForm.listSexo}" />
																	<f:validator validatorId="eforserver.RequiredJSFValidator"/>
																</h:selectOneMenu>																
															<f:verbatim>
															</td>
															<td  class="datatable">
																Estado Civil
															</td>
															<td >
															</f:verbatim>
																<h:selectOneMenu value="#{ingresoDocenteForm.estadoCivil}"
																	id="EstadoCivil"																	
																	immediate="false"
																	required="true"
																	title="Estado Civil">																	
																	<f:selectItems value="#{ingresoDocenteForm.listEstadoCivil}" />
																	<f:validator validatorId="eforserver.RequiredJSFValidator"/>
																</h:selectOneMenu>																														
															<f:verbatim></td>
														</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
														<td class="datatable">
																Nacionalidad
															</td>
															<td width="30%">
															</f:verbatim>
																<h:selectOneMenu value="#{ingresoDocenteForm.nacionalidad}"
																	id="Nacionalidad"																	
																	immediate="false"
																	required="true"
																	title="Nacionalidad">			
																	<f:selectItems value="#{ingresoDocenteForm.listNacionalidad}" />
																	<f:validator validatorId="eforserver.RequiredJSFValidator"/>
																</h:selectOneMenu>																														
															<f:verbatim></td>
															<td  class="datatable">
																Fecha Nacimiento
															</td>
															<td >
															</f:verbatim>
																<h:inputText size="12"
																	value="#{ingresoDocenteForm.fechaNacimiento}"
																	immediate="false"
																	onkeypress="return keyEnterCheck(event, this)"
					                        						onblur="javascript:check_date(this)"
					                        						>																																			                         						
						                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
					                                                 pattern="dd-MM-yyyy" />
					                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
															<f:verbatim>
															</td>
														</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
			                        						<td colspan="2" align="left">
			                        							
			                        						</td>
			                        						<td colspan="2" align="right"></f:verbatim>
																<h:commandButton image="/images/next.gif" 
																	action="#{ingresoDocenteForm.next}"
																	onclick="javascript:return clickMade()" />
															<f:verbatim></td>
														</tr>
													</table>
												</td>
											</tr>											
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>	
							<f:subview 
								id="showDatosPersonales"
								rendered="#{ingresoDocenteForm.showDatosPersonales}">
								<f:verbatim><tr>
									<td>										
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											
                        					<tr>
                        						<td width="15%" class="subTitle">
                        							</f:verbatim>
													<h:outputText 
														value="#{ingresoDocenteForm.cedula} - " />													
												<f:verbatim>
                        						</td>
                        						<td width="85%" class="subTitle">
                        						</f:verbatim>
													<h:outputText																	
														value="#{ingresoDocenteForm.primerApellido}, " />	
													<h:outputText
														value="#{ingresoDocenteForm.primerNombre}"/>															
												<f:verbatim>
                        						</td>
											</tr>
																																		
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>	
							<f:subview 
								id="show3"
								rendered="#{ingresoDocenteForm.show3}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos del Cargo (Secci�n I)
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim><tr>
                        						<td width="40%">
                        							Tipo de Profesional
                        						</td>
                        						<td width="60%"></f:verbatim>
					                            	<h:selectOneMenu value="#{ingresoDocenteForm.selectGradoDocente}"
                                                    	immediate="false">														
                                                        <f:selectItems value="#{ingresoDocenteForm.colGradoDocente}" />
                                                    </h:selectOneMenu>
												<f:verbatim>
												</td>
											</tr>
											<tr>
                        						<td>
                        							Nivel o Modalidad
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{ingresoDocenteForm.selectNivelDocente}"
                                                    	immediate="false">														
                                                        <f:selectItems value="#{ingresoDocenteForm.colNivelDocente}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Materia
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{ingresoDocenteForm.selectAsignatura}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{ingresoDocenteForm.colAsignatura}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
												<td>
													C�digo Entidad
												</td>
												<td width="100%"></f:verbatim>
													<h:inputText size="12"													
														maxlength="12"
														value="#{ingresoDocenteForm.findCodDependencia}"
														onchange="javascript:this.value=this.value.toUpperCase()"														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"                        																						
													/>
													<h:commandButton image="/images/find2.gif" 
														action="#{ingresoDocenteForm.findDependenciaByCodigoAndRegion}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											</f:verbatim>
											<f:verbatim>
												<tr>
													<td colspan="2">&nbsp;</td>
												</tr>
											</f:verbatim>
											<f:verbatim>
												<tr>
													<td>
													</td>
													<td>
												</f:verbatim>
													<h:outputText 		
														rendered="#{ingresoDocenteForm.showFindDependencia}"												
														value="#{ingresoDocenteForm.dependencia}"/>
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							
                        						</td>
                        						<td align="right"></f:verbatim>
													<h:commandButton image="/images/next.gif" 														
														action="#{ingresoDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>		
							<f:subview 
								id="show4"
								rendered="#{ingresoDocenteForm.show4}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos del Cargo (Secci�n II)
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim><tr>
                        						<td width="40%">
                        							Jerarquia
                        						</td>
                        						<td width="60%"></f:verbatim>
					                            	<h:outputText value="#{ingresoDocenteForm.jerarquiaDocente}"
                                                    	/>														
												<f:verbatim>
												</td>
											</tr>											
											<tr>
                        						<td>
                        							Turno
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{ingresoDocenteForm.turno}"
                                                    	immediate="false">
                                                        <f:selectItems value="#{ingresoDocenteForm.listTurno}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Condicion
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{ingresoDocenteForm.condicion}"
                                                    	immediate="false">
                                                        <f:selectItems value="#{ingresoDocenteForm.listCondicion}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							
                        						</td>
                        						<td align="right"></f:verbatim>
													
													<h:commandButton image="/images/next.gif" 														
														action="#{ingresoDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>	
							<f:subview 
								id="show5"
								rendered="#{ingresoDocenteForm.show5}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos del Cargo (Secci�n III)
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>
										<tr>
                        						<td width="40%">
                        							Categor�a
                        						</td>
                        						<td width="60%"></f:verbatim>
					                            	<h:outputText                         								
														value="#{ingresoDocenteForm.categoriaDocente}" >	
													</h:outputText>
											
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Turno
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{ingresoDocenteForm.selectTurnoDocente}"
                                                    	immediate="false">														
                                                        <f:selectItems value="#{ingresoDocenteForm.colTurnoDocente}" />
                                                    </h:selectOneMenu>
												<f:verbatim>
												</td>
											</tr>
											<tr>
                        						<td>
                        							Dedicacion
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{ingresoDocenteForm.selectDedicacionDocente}"
                                                    	immediate="false">														
                                                        <f:selectItems value="#{ingresoDocenteForm.colDedicacionDocente}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>											
											<tr>
                        						<td>
                        							
                        						</td>
                        						<td align="right"></f:verbatim>
													
													<h:commandButton image="/images/next.gif" 														
														action="#{ingresoDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>		
							<f:subview 
								id="show6"
								rendered="#{ingresoDocenteForm.show6}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos del Cargo (Secci�n IV)
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>
											<tr>
                        						<td width="40%">
                        							Cargo
                        						</td>
                        						<td width="60"></f:verbatim>
					                            	<h:outputText 
														value="#{ingresoDocenteForm.cargo}" />	
											
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Horas Docente
                        						</td>
                        						<td></f:verbatim>
                        						<h:outputText 
                        								rendered="#{ingresoDocenteForm.showCargaHoraria}"	
														value="#{ingresoDocenteForm.detalleTabulador.cargaHoraria}" >	
												</h:outputText>
												<h:inputText
	                        							size="6"
	                        							maxlength="7"
                        								value="#{ingresoDocenteForm.horasDocente}"
                        								rendered="#{!ingresoDocenteForm.showCargaHoraria}"	
                        								id="horas"
                        								title="Horas"
                        								alt="Horas"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>           							
												<f:verbatim>
												</td>
											</tr>																					
											<tr>
                        						<td>
                        							
                        						</td>
                        						<td align="right"></f:verbatim>
													
													<h:commandButton image="/images/next.gif" 														
														action="#{ingresoDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>	
							<f:subview 
								id="show7"
								rendered="#{ingresoDocenteForm.show7}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos del Cargo (Secci�n V)
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>
											<tr>
                        						<td width="40%">
                        							Sueldo
                        						</td>
                        						<td width="60%"></f:verbatim>
					                            	<h:outputText 
														value="#{ingresoDocenteForm.sueldo}">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
													</h:outputText>	
											
												<f:verbatim></td>
											</tr>
											<tr>
												<td>
													C�digo Banco
												</td>
												<td width="100%"></f:verbatim>
													<h:inputText size="12"													
														maxlength="12"
														value="#{ingresoDocenteForm.findCodBanco}"
														onchange="javascript:this.value=this.value.toUpperCase()"														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"                        																						
													/>
													<h:commandButton image="/images/find2.gif" 
														action="#{ingresoDocenteForm.findBancoByCodigo}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											</f:verbatim>
											<f:verbatim>
												<tr>
													<td colspan="2">&nbsp;</td>
												</tr>
											</f:verbatim>
											<f:verbatim>
												<tr>
													<td>
													</td>
													<td>
												</f:verbatim>
													<h:outputText 		
														rendered="#{ingresoDocenteForm.showFindBanco}"												
														value="#{ingresoDocenteForm.banco}"/>
												<f:verbatim></td>
											</tr>											
											<tr>
                        						<td>
                        							N� Cuenta
                        						</td>
                        						<td></f:verbatim>
					                            	<h:inputText
														size="25"
														id="CuentaNomina"
														maxlength="20"
														value="#{ingresoDocenteForm.cuentaNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)"                        								
														 />	
											
												<f:verbatim></td>
											</tr>	
											<tr>
                        						<td>
                        							Confirmar N� Cuenta
                        						</td>
                        						<td></f:verbatim>
					                            	<h:inputText
														size="25"
														id="RepetirCuentaNomina"
														maxlength="20"
														value="#{ingresoDocenteForm.repetirCuentaNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)"                        								
														 />	
											
												<f:verbatim></td>
											</tr>	
											<tr>
		    									<td>
		    										Procedencia de las horas
		    									</td></f:verbatim>
		    									<td width="100%">
		    										<h:selectOneMenu value="#{ingresoDocenteForm.procedenciaHoras}"
			    										onchange="this.form.submit()"
			    										immediate="false">
		                                            	<f:selectItem itemLabel="Apertura Escolar" itemValue="1" /> 
		                                            	<f:selectItem itemLabel="Sustitucion" itemValue="2" />   
		                                            	<f:selectItem itemLabel="Transferencia sustitucion" itemValue="3" />   
		                                            	<f:selectItem itemLabel="Transferencia apertura" itemValue="3" />                                              	
		                                            </h:selectOneMenu><f:verbatim>
		    									</td>
		    								</tr>													
											<tr>
                        						<td>
                        							
                        						</td>
                        						<td align="right"></f:verbatim>
													
													<h:commandButton image="/images/next.gif" 	
														immediate="false"													
														action="#{ingresoDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>
							<f:subview 
								id="showSustitucion"
								rendered="#{ingresoDocenteForm.showSustitucion}">	
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos del Cargo (Secci�n VI)
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim><tr>
                     						<td width="20%">
                     							C�dula Docente
                     						</td>
                     						<td widht="80%"></f:verbatim>
										<h:inputText size="10"
											maxlength="8"
											value="#{ingresoDocenteForm.cedulaSustitucion}"
											onkeypress="return keyIntegerCheck(event, this)"
											onfocus="return deleteZero(event, this)"
											onblur="javascript:fieldEmpty(this)"
											required="true"
											onchange="javascript:this.value=this.value.toUpperCase()" />													
											
											<h:commandButton image="/images/find2.gif" 
												action="#{ingresoDocenteForm.findRegistroDocente}"
												onclick="javascript:return clickMade()" />
											<f:verbatim>
											</td>
										</tr>
										<tr>
                     						<td colspan="2" width="100%">
	                     						<h:outputText 													
													value="#{ingresoDocenteForm.nombreSustitucion}, #{ingresoDocenteForm.apellidoSustitucion}"/>
                     						</td>
                    					</tr>
										<tr>
                        					<td colspan="2"></f:verbatim>
                        							<h:dataTable id="tableSustitucion"
		                                                styleClass="datatable"
		                                                headerClass="standardTable_Header"
		                                                footerClass="standardTable_Header"
		                                                rowClasses="standardTable_Row1,standardTable_Row2"
		                                                columnClasses="standardTable_Column,standardTable_Column, standardTable_Column, standardTable_Column"
		                                                var="result"
		                                				value="#{ingresoDocenteForm.listSustitucion}"		                                                
		                                                width="100%">
		                                				<h:column>
		                                					<h:outputText 		
																value="#{result.registroDocente.cargo.codCargo}"/>																																																																																											
		                                				</h:column>
		                                				<h:column>
		                                					<h:outputText 		
																value="#{result.registroDocente.cargo.descripcionCargo}"/>			
		                                				</h:column>
		                                				<h:column>
		                                				<h:outputText 		
																value="#{result.registroDocente.horasRestante}"/>
		                                				</h:column>
		                                				<h:column>		                                				
															<h:inputText size="3"		
																value="#{result.cantidad}"/>		
		                                				</h:column>
		                                				
		                                            </h:dataTable>
                        					<f:verbatim></td>
                        						
										</tr>																																				
										<tr>
                        					<td>
                        							
                       						</td>
                       						<td align="right"></f:verbatim>
												
												<h:commandButton image="/images/next.gif" 	
													immediate="false"													
													action="#{ingresoDocenteForm.next}"
													onclick="javascript:return clickMade()" />
											<f:verbatim>
											</td>
										</tr>
										</table>
									</td>
								</tr>
							</f:verbatim>				
							</f:subview>							
							<f:subview 
								id="show8"
								rendered="#{ingresoDocenteForm.showFinal}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Informaci�n final del movimiento
												</td>
											</tr>
										</table>			
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>
											<tr>
												<td width="40%">
													<b>Fecha Ingreso</b>
												</td>
												<td width="60%">
													<h:outputText value="#{ingresoDocenteForm.fechaMovimiento}" />	
												</td>
											</tr>
											<tr>
												<td width="40%">
													<b>Dependencia</b>
												</td>
												<td width="60%">
													<h:outputText value="#{ingresoDocenteForm.dependencia}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Cedula</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.cedula}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Apellidos</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.primerApellido} #{ingresoDocenteForm.segundoApellido}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Nombres</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.primerNombre} #{ingresoDocenteForm.segundoNombre}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Sexo</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.stringSexo}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Estado Civil</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.stringEstadoCivil}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Nacionalidad</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.stringNacionalidad}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Banco</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.banco}" />	
												</td>
											</tr>											
											<tr>
												<td>
													<b>N� Cuenta</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.cuentaNomina}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Tipo de Profesional</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.gradoDocente}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Jerarquia</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.jerarquiaDocente}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Nivel o Modalidad</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.nivelDocente}" />	
												</td>
											</tr>											
											<tr>
												<td>
													<b>Categoria</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.categoriaDocente}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Turno</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.turnoDocente}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Dedicacion</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.dedicacionDocente}" />	
												</td>
											</tr>
											<tr>
												<td>
													<b>Cargo</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.cargo}" />	
												</td>
											</tr>											
											<tr>
												<td>
													<b>Total Horas</b>
												</td>
												<td>
													<h:outputText value="#{ingresoDocenteForm.totalHoras}" />	
												</td>
											</tr>
											<tr>
												<td colspan="2" class="sub-bandtable">
													Conceptos Fijos
												</td>
											</tr>
											
											<tr>
                        						<td colspan="2">
                        							 <h:dataTable id="tableConceptosFijos"
		                                                styleClass="datatable"
		                                                headerClass="standardTable_Header,standardTable_Header,standardTable_Header,standardTable_Header,standardTable_Header"
		                                                footerClass="standardTable_Header"
		                                                rowClasses="standardTable_Row1,standardTable_Row2"
		                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
		                                                var="resultConceptoFijo"
		                                				value="#{ingresoDocenteForm.resultConceptoFijo}"		                                                
		                                                width="100%">
		                                				<h:column>
		                                					<f:facet name="header">
																<h:outputText value="Codigo" />
															</f:facet>
		                                				
		                                					<h:outputText 		
																value="#{resultConceptoFijo.conceptoTipoPersonal.concepto.codConcepto}"/>
														</h:column>
														<h:column>
															<f:facet name="header">
																<h:outputText value="Descripcion" />
															</f:facet>
															<h:outputText 		
																value="#{resultConceptoFijo.conceptoTipoPersonal.concepto.descripcion}"/>
														</h:column>
														<h:column>
															<f:facet name="header">
																<h:outputText value="Frec." />
															</f:facet>
															<h:outputText 		
																value="#{resultConceptoFijo.frecuenciaTipoPersonal.frecuenciaPago.codFrecuenciaPago}"/>	
														</h:column>
														<h:column >
															<f:facet name="header">
																<h:outputText value="Monto" />
															</f:facet>
															<h:outputText 		
																value="#{resultConceptoFijo.monto}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>	
															</h:outputText>	
														</h:column>
														<h:column>
														
														</h:column>
		                                            </h:dataTable>
                        						</td>
                        						
											</tr>
											<tr>
												<td colspan="2" class="sub-bandtable">
													Conceptos Retroactivos
												</td>
											</tr>
											<tr>
                        						<td colspan="2" class="listData"></f:verbatim>
                        							 <h:dataTable id="tableConceptosRetroactivos"
		                                                styleClass="datatable"
		                                                headerClass="standardTable_Header"
		                                                footerClass="standardTable_Header"
		                                                rowClasses="standardTable_Row1,standardTable_Row2"
		                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
		                                                var="resultConceptoRetroactivo"		                                                
		                                                binding="#{ingresoDocenteForm.tableConceptoRetroactivo}"
		                                				value="#{ingresoDocenteForm.resultConceptoRetroactivo}"		  		                                				                                              
		                                                width="100%">
		                                				<h:column>
			                                				<f:facet name="header">
																<h:outputText value="Codigo" />
															</f:facet>
		                                					<h:outputText 		
																value="#{resultConceptoRetroactivo.conceptoTipoPersonal.concepto.codConcepto}"/>
														</h:column>
														<h:column>
															<f:facet name="header">
																<h:outputText value="Descripcion" />
															</f:facet>
															<h:outputText 		
																value="#{resultConceptoRetroactivo.conceptoTipoPersonal.concepto.descripcion}"/>
														</h:column>
														<h:column>
															<f:facet name="header">
																<h:outputText value="Frec." />
															</f:facet>
															<h:outputText 		
																value="#{resultConceptoRetroactivo.frecuenciaTipoPersonal.frecuenciaPago.codFrecuenciaPago}"/>	
														</h:column>
														<h:column rendered="#{ingresoDocenteForm.idModificar!=resultConceptoRetroactivo.idConceptoDocente}">
															<f:facet name="header">
																<h:outputText value="Monto" />
															</f:facet>
															<h:outputText 		
																value="#{resultConceptoRetroactivo.monto}">
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>	
															</h:outputText>	
														</h:column>
														<h:column rendered="#{ingresoDocenteForm.idModificar==resultConceptoRetroactivo.idConceptoDocente}">
															<f:facet name="header">
																<h:outputText value="Monto" />
															</f:facet>
															<h:inputText size="10"																	
																value="#{ingresoDocenteForm.conceptoDocenteModificar.monto}">																	
															</h:inputText>	
														</h:column>
														<h:column rendered="#{ingresoDocenteForm.idModificar!=resultConceptoRetroactivo.idConceptoDocente}">
															<h:commandButton value="editItem" 
																image="/images/b_modify-p.gif" 																												
																action="#{ingresoDocenteForm.editItem}"																
																onclick="javascript:return clickMade()" />
														</h:column>
														<h:column rendered="#{ingresoDocenteForm.idModificar==resultConceptoRetroactivo.idConceptoDocente}">
															<h:commandButton value="saveItem"
																image="/images/b_save-p.gif" 																												
																action="#{ingresoDocenteForm.saveItem}"																
																onclick="javascript:return clickMade()" />
														</h:column>
														<h:column rendered="#{ingresoDocenteForm.idModificar==resultConceptoRetroactivo.idConceptoDocente}">
															<h:commandButton value="undoItem"
																image="/images/b_undo-p.gif" 																												
																action="#{ingresoDocenteForm.undoItem}"																
																onclick="javascript:return clickMade()" />
														</h:column>
		                                            </h:dataTable><f:verbatim>
                        						</td>
                        						
											</tr>												
											<tr>
                        						<td colspan="2" align="right"></f:verbatim>
													
													<h:commandButton image="/images/run.gif" 	
														rendered="#{!ingresoDocenteForm.showConfirmacion}"													
														action="#{ingresoDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												
												<h:outputText
			                        					value="�Esta seguro que desea realizar este proceso?"
			                        					rendered="#{ingresoDocenteForm.showConfirmacion}" />                							                        				                				
			                        				<h:commandButton value="Si" 
			                        					image="/images/yes.gif"
			                        					action="#{ingresoDocenteForm.next}"
			                        					onclick="javascript:return clickMade()"
			                        					rendered="#{ingresoDocenteForm.showConfirmacion}" />                        					                        			                        				
			                        				<h:commandButton value="Cancelar" 
			                        					image="/images/cancel.gif"
			                        					action="#{ingresoDocenteForm.abort}"
			                        					immediate="true"
			                        					rendered="#{ingresoDocenteForm.showConfirmacion}"
			                        					onclick="javascript:return clickMade()"
			                	        				 />
			                	        		<f:verbatim>				    								
			    								</td>
											</tr>
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>																																					
						</td>
					</tr>
				</table>

				</h:form>

			</td>			
		</tr>
	</table>

</f:view>

</body>
</html>