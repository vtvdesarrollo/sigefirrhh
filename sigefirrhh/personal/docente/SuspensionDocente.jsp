<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
		<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{suspensionDocenteForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top"          
				</div>
			</td>
			<td width="570" valign="top">
			<h:form id="formSuspensionDocente">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Suspension (Personal Docente)
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/docente/SuspensionDocente.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="show1"
								rendered="#{suspensionDocenteForm.show1}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos del Movimiento
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											
											<tr>
                        						<td align="left">
                        							Tipo de Personal
                        						</td>
                        						<td align="left"></f:verbatim>
					                            	<h:selectOneMenu value="#{suspensionDocenteForm.selectTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{suspensionDocenteForm.colTipoPersonal}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td align="left">
                        							Tipo Movimiento
                        						</td>
                        						<td align="left"></f:verbatim>
					                            	<h:selectOneMenu value="#{suspensionDocenteForm.tipoMovimiento}"
                                                    	immediate="false">														                                            
														<f:selectItem itemLabel="Suspender Cargo" itemValue="1" />                                                       
														<f:selectItem itemLabel="Suspender Pago" itemValue="2" />           
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
		                						<td align="left">Fecha Movimiento</td>
		                						<td align="left">
		                						</f:verbatim>
													<h:inputText size="16"
														value="#{suspensionDocenteForm.fechaMovimiento}"
														immediate="false"
														onkeypress="return keyEnterCheck(event, this)"
		                        						onblur="javascript:check_date(this)"
		                        						>																																			                         						
			                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
		                                                 pattern="dd-MM-yyyy" />
		                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
		                        						                				
												<f:verbatim></td>										
											</tr>
											<tr>
                        						<td width="20%">
                        							C�dula
                        						</td>
                        						<td widht="80%"></f:verbatim>
													<h:inputText size="10"
														maxlength="8"
														value="#{suspensionDocenteForm.cedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														required="true"
														onchange="javascript:this.value=this.value.toUpperCase()" />													
												<f:verbatim></td>
											</tr>
											
                        					<tr>
                        						<td align="left">
                        							
                        						</td>
                        						<td align="right">
                        						</f:verbatim>
													<h:commandButton image="/images/next.gif" 
														action="#{suspensionDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>																					
							<f:subview 
								id="showDatosPersonales"
								rendered="#{!suspensionDocenteForm.show1}">
								<f:verbatim><tr>
									<td>										
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											
                        					<tr>
                        						<td width="15%" class="subTitle">
                        							</f:verbatim>
													<h:outputText 
														value="#{suspensionDocenteForm.cedula} - " />													
												<f:verbatim>
                        						</td>
                        						<td width="85%" class="subTitle">
                        						</f:verbatim>
													<h:outputText																	
														value="#{suspensionDocenteForm.personal.primerApellido}, " />	
													<h:outputText
														value="#{suspensionDocenteForm.personal.primerNombre}"/>															
												<f:verbatim>
                        						</td>
											</tr>
																																		
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>	
							<f:subview 
								id="show2"
								rendered="#{suspensionDocenteForm.show2}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Cargos Actuales
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											                        					
											
											<tr>
												<td class="sub-bandtable">
													Cargos
												</td>
											</tr>
											<tr>
	                       						<td class="listData"></f:verbatim>
	                       							 <h:dataTable id="tableCargosActuales"
		                                                styleClass="datatable"
		                                                headerClass="standardTable_Header"
		                                                footerClass="standardTable_Header"
		                                                rowClasses="standardTable_Row1,standardTable_Row2"
		                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
		                                                var="resultCargosActuales"
		                                                binding="#{suspensionDocenteForm.tableCargosActuales}"
		                                				value="#{suspensionDocenteForm.resultCargosActuales}"		                                                
		                                                width="100%">
		                                                <h:column>
			                                                <f:facet name="header">
																<h:outputText value="Codigo" />
															</f:facet>
		                                					<h:outputText 		
																value="#{resultCargosActuales.cargo.codCargo}"/>
		                                				</h:column>
		                                				<h:column>
			                                				 <f:facet name="header">
																<h:outputText value="Nombre Cargo" />
															</f:facet>
		                                					<h:outputText 		
																value="#{resultCargosActuales.cargo.descripcionCargo}"/>
		                                				</h:column>
		                                				<h:column>
			                                				 <f:facet name="header">
																<h:outputText value="Dep" />
															</f:facet>
		                                					<h:outputText 		
																value="#{resultCargosActuales.dependencia.codDependencia}"/>
		                                				</h:column>
		                                				<h:column>
			                                				 <f:facet name="header">
																<h:outputText value="Horas" />
															</f:facet>
		                                					<h:outputText 		
																value="#{resultCargosActuales.totalHoras}"/>
		                                				</h:column>
		                                				<h:column>					                                			
		                                					<h:commandButton 
	                                    						action="#{suspensionDocenteForm.seleccionar}"			                                    						
	                                    						image="/images/delete.gif">			                                    						
	                                    					</h:commandButton>
		                                				</h:column>
		                                            </h:dataTable><f:verbatim>
	                       						</td>
											</tr>		
											<tr>
												<td class="sub-bandtable">
													Cargo a Suspender
												</td>
											</tr>				
											<tr>
                        						<td align="left"></f:verbatim>
													<h:outputText 														
														value="#{suspensionDocenteForm.cargoSuspender.cargo.codCargo}  #{suspensionDocenteForm.cargoSuspender.cargo.descripcionCargo}  - #{suspensionDocenteForm.cargoSuspender.dependencia.codDependencia} #{suspensionDocenteForm.cargoSuspender.dependencia.nombre} - #{suspensionDocenteForm.cargoSuspender.totalHoras}"
														/>
												<f:verbatim></td>
											</tr>													
											<tr>
                        						<td align="right"></f:verbatim>
													<h:commandButton image="/images/run.gif" 
														rendered="#{!suspensionDocenteForm.showConfirmacion}"
														action="#{suspensionDocenteForm.preGenerate}"
														onclick="javascript:return clickMade()" />
														
													<h:outputText
			                        					value="�Esta seguro que desea realizar este proceso?"
			                        					rendered="#{suspensionDocenteForm.showConfirmacion}" />                							                        				                				
			                        				<h:commandButton value="Si" 
			                        					image="/images/yes.gif"
			                        					action="#{suspensionDocenteForm.generate}"
			                        					onclick="javascript:return clickMade()"
			                        					rendered="#{suspensionDocenteForm.showConfirmacion}" />                        					                        			                        				
			                        				<h:commandButton value="Cancelar" 
			                        					image="/images/cancel.gif"
			                        					action="#{suspensionDocenteForm.abort}"
			                        					immediate="true"
			                        					rendered="#{suspensionDocenteForm.showConfirmacion}"
			                        					onclick="javascript:return clickMade()"
			                	        				 />	
												<f:verbatim></td>
											</tr>										
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>																						
							<f:subview 
								id="show3"
								rendered="#{suspensionDocenteForm.show3}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Cargos Actuales
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											                        					
											
											<tr>
												<td class="sub-bandtable">
													Cargos
												</td>
											</tr>
											<tr>
	                       						<td class="listData"></f:verbatim>
	                       							 <h:dataTable id="tableCargosActuales2"
		                                                styleClass="datatable"
		                                                headerClass="standardTable_Header"
		                                                footerClass="standardTable_Header"
		                                                rowClasses="standardTable_Row1,standardTable_Row2"
		                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
		                                                var="resultCargosActuales"		                                               
		                                				value="#{suspensionDocenteForm.resultCargosActuales}"		                                                
		                                                width="100%">
		                                                <h:column>
			                                                <f:facet name="header">
																<h:outputText value="Codigo" />
															</f:facet>
		                                					<h:outputText 		
																value="#{resultCargosActuales.cargo.codCargo}"/>
		                                				</h:column>
		                                				<h:column>
			                                				 <f:facet name="header">
																<h:outputText value="Nombre Cargo" />
															</f:facet>
		                                					<h:outputText 		
																value="#{resultCargosActuales.cargo.descripcionCargo}"/>
		                                				</h:column>
		                                				<h:column>
			                                				 <f:facet name="header">
																<h:outputText value="Dep" />
															</f:facet>
		                                					<h:outputText 		
																value="#{resultCargosActuales.dependencia.codDependencia}"/>
		                                				</h:column>
		                                				<h:column>
			                                				 <f:facet name="header">
																<h:outputText value="Horas" />
															</f:facet>
		                                					<h:outputText 		
																value="#{resultCargosActuales.totalHoras}"/>
		                                				</h:column>
		                                				
		                                            </h:dataTable><f:verbatim>
	                       						</td>
											</tr>		
																					
											<tr>
                        						<td align="right"></f:verbatim>
													<h:commandButton image="/images/run.gif" 
														rendered="#{!suspensionDocenteForm.showConfirmacion}"
														action="#{suspensionDocenteForm.preGenerate}"
														onclick="javascript:return clickMade()" />
														
													<h:outputText
			                        					value="�Esta seguro que desea realizar este proceso?"
			                        					rendered="#{suspensionDocenteForm.showConfirmacion}" />                							                        				                				
			                        				<h:commandButton value="Si" 
			                        					image="/images/yes.gif"
			                        					action="#{suspensionDocenteForm.generate}"
			                        					onclick="javascript:return clickMade()"
			                        					rendered="#{suspensionDocenteForm.showConfirmacion}" />                        					                        			                        				
			                        				<h:commandButton value="Cancelar" 
			                        					image="/images/cancel.gif"
			                        					action="#{suspensionDocenteForm.abort}"
			                        					immediate="true"
			                        					rendered="#{suspensionDocenteForm.showConfirmacion}"
			                        					onclick="javascript:return clickMade()"
			                	        				 />	
												<f:verbatim></td>
											</tr>										
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>																			
							
						</td>
					</tr>
				</table>

				</h:form>

			</td>			
		</tr>
	</table>

</f:view>

</body>
</html>