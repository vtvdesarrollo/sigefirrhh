<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
		<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{modificacionDatosDocenteForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top"          
				</div>
			</td>
			<td width="570" valign="top">
			<h:form id="formModificacionDatosDocente">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>						
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Modificacion Datos Personales (Personal Docente)
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/docente/ModificacionDatosDocente.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
		                					image="/images/close.gif"
		                					action="go_cancelOption"
		                					immediate="true"                        					
		                					onclick="javascript:return clickMade()"
		        	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="show1"
								rendered="#{modificacionDatosDocenteForm.show1}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Datos del Movimiento
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											
											<tr>
                        						<td align="left">
                        							Tipo de Personal
                        						</td>
                        						<td align="left"></f:verbatim>
					                            	<h:selectOneMenu value="#{modificacionDatosDocenteForm.selectTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{modificacionDatosDocenteForm.colTipoPersonal}" />
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td align="left">
                        							Tipo Modificacion
                        						</td>
                        						<td align="left"></f:verbatim>
					                            	<h:selectOneMenu value="#{modificacionDatosDocenteForm.tipoModificacion}"
                                                    	immediate="false">														                                            
														<f:selectItem itemLabel="Cedula de Identidad" itemValue="1" />                                                       
														<f:selectItem itemLabel="Apellidos y Nombres" itemValue="2" />
														<f:selectItem itemLabel="Estado Civil" itemValue="3" />           
														<f:selectItem itemLabel="Fecha Nacimiento" itemValue="4" />           
                                                    </h:selectOneMenu>
											
												<f:verbatim></td>
											</tr>
											<tr>
		                						<td align="left">Fecha Movimiento</td>
		                						<td align="left">
		                						</f:verbatim>
													<h:inputText size="16"
														value="#{modificacionDatosDocenteForm.fechaMovimiento}"
														immediate="false"
														onkeypress="return keyEnterCheck(event, this)"
		                        						onblur="javascript:check_date(this)"
		                        						>																																			                         						
			                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
		                                                 pattern="dd-MM-yyyy" />
		                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
		                        						                				
												<f:verbatim></td>										
											</tr>
											<tr>
                        						<td width="20%">
                        							C�dula
                        						</td>
                        						<td widht="80%"></f:verbatim>
													<h:inputText size="10"
														maxlength="8"
														value="#{modificacionDatosDocenteForm.cedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														required="true"
														onchange="javascript:this.value=this.value.toUpperCase()" />													
												<f:verbatim></td>
											</tr>
											
                        					<tr>
                        						<td align="left">
                        							
                        						</td>
                        						<td align="right">
                        						</f:verbatim>
													<h:commandButton image="/images/next.gif" 
														action="#{modificacionDatosDocenteForm.next}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>																					
							<f:subview 
								id="showDatosPersonales"
								rendered="#{!modificacionDatosDocenteForm.show1}">
								<f:verbatim><tr>
									<td>										
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											
                        					<tr>
                        						<td width="15%" class="subTitle">
                        							</f:verbatim>
													<h:outputText 
														value="#{modificacionDatosDocenteForm.cedula} - " />													
												<f:verbatim>
                        						</td>
                        						<td width="85%" class="subTitle">
                        						</f:verbatim>
													<h:outputText																	
														value="#{modificacionDatosDocenteForm.personal.primerApellido}, " />	
													<h:outputText
														value="#{modificacionDatosDocenteForm.personal.primerNombre}"/>															
												<f:verbatim>
                        						</td>
											</tr>
																																		
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>	
							<f:subview 
								id="showCedula"
								rendered="#{modificacionDatosDocenteForm.showCedula}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Modificac�n de C�dula de Identidad
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											                        																
											<tr>
                        						<td width="20%">
                        							C�dula
                        						</td>
                        					</tr>
                        						<td widht="80%"></f:verbatim>
													<h:inputText size="10"
														maxlength="8"
														value="#{modificacionDatosDocenteForm.nuevaCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														required="true"
														onchange="javascript:this.value=this.value.toUpperCase()" />													
												<f:verbatim></td>
											</tr>							
											<tr>
                        						<td align="right"></f:verbatim>
													<h:commandButton image="/images/run.gif" 
														rendered="#{!modificacionDatosDocenteForm.showConfirmacion}"
														action="#{modificacionDatosDocenteForm.preGenerate}"
														onclick="javascript:return clickMade()" />
														
													<h:outputText
			                        					value="�Esta seguro que desea realizar este proceso?"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}" />                							                        				                				
			                        				<h:commandButton value="Si" 
			                        					image="/images/yes.gif"
			                        					action="#{modificacionDatosDocenteForm.generate}"
			                        					onclick="javascript:return clickMade()"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}" />                        					                        			                        				
			                        				<h:commandButton value="Cancelar" 
			                        					image="/images/cancel.gif"
			                        					action="#{modificacionDatosDocenteForm.abort}"
			                        					immediate="true"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}"
			                        					onclick="javascript:return clickMade()"
			                	        				 />	
												<f:verbatim></td>
											</tr>										
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>																						
							<f:subview 
								id="showNombre"
								rendered="#{modificacionDatosDocenteForm.showNombre}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Modificaci�n de Apellidos y Nombres
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											                        																
											<tr>
                        						<td width="20%">
                        							Primer Apellido
                        						</td>
                        					</tr>
                        						<td widht="80%"></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														required="true" 
														value="#{modificacionDatosDocenteForm.nuevoPrimerApellido}"	
														onkeypress="return keyEnterCheck(event, this)"           													
														onchange="javascript:this.value=this.value.toUpperCase()" />													
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td width="20%">
                        							Segundo Apellido
                        						</td>
                        					</tr>
                        						<td widht="80%"></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{modificacionDatosDocenteForm.nuevoSegundoApellido}"	
														onkeypress="return keyEnterCheck(event, this)"           																																																				
														onchange="javascript:this.value=this.value.toUpperCase()" />													
												<f:verbatim></td>
											</tr>		
											<tr>
                        						<td width="20%">
                        							Primer Nombre
                        						</td>
                        					</tr>
                        						<td widht="80%"></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														required="true" 
														value="#{modificacionDatosDocenteForm.nuevoPrimerNombre}"	
														onkeypress="return keyEnterCheck(event, this)"           													
														onchange="javascript:this.value=this.value.toUpperCase()" />													
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td width="20%">
                        							Segundo Nombre
                        						</td>
                        					</tr>
                        						<td widht="80%"></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{modificacionDatosDocenteForm.nuevoSegundoNombre}"	
														onkeypress="return keyEnterCheck(event, this)"           										
														onchange="javascript:this.value=this.value.toUpperCase()" />													
												<f:verbatim></td>
											</tr>							
											<tr>
                        						<td align="right"></f:verbatim>
													<h:commandButton image="/images/run.gif" 
														rendered="#{!modificacionDatosDocenteForm.showConfirmacion}"
														action="#{modificacionDatosDocenteForm.preGenerate}"
														onclick="javascript:return clickMade()" />
														
													<h:outputText
			                        					value="�Esta seguro que desea realizar este proceso?"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}" />                							                        				                				
			                        				<h:commandButton value="Si" 
			                        					image="/images/yes.gif"
			                        					action="#{modificacionDatosDocenteForm.generate}"
			                        					onclick="javascript:return clickMade()"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}" />                        					                        			                        				
			                        				<h:commandButton value="Cancelar" 
			                        					image="/images/cancel.gif"
			                        					action="#{modificacionDatosDocenteForm.abort}"
			                        					immediate="true"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}"
			                        					onclick="javascript:return clickMade()"
			                	        				 />	
												<f:verbatim></td>
											</tr>										
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>																			
							<f:subview 
								id="showEstadoCivil"
								rendered="#{modificacionDatosDocenteForm.showEstadoCivil}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Modificac�n de Estado Civil
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											                        																
											<tr>
                        						<td width="20%">
                        							Estado Civil
                        						</td>
                        					</tr>
                        						<td widht="80%"></f:verbatim>
													<h:selectOneMenu value="#{modificacionDatosDocenteForm.nuevoEstadoCivil}"
														id="EstadoCivil"																	
														immediate="false"
														required="true"
														title="Estado Civil">																	
														<f:selectItems value="#{modificacionDatosDocenteForm.listEstadoCivil}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu>																
												<f:verbatim></td>
											</tr>							
											<tr>
                        						<td align="right"></f:verbatim>
													<h:commandButton image="/images/run.gif" 
														rendered="#{!modificacionDatosDocenteForm.showConfirmacion}"
														action="#{modificacionDatosDocenteForm.preGenerate}"
														onclick="javascript:return clickMade()" />
														
													<h:outputText
			                        					value="�Esta seguro que desea realizar este proceso?"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}" />                							                        				                				
			                        				<h:commandButton value="Si" 
			                        					image="/images/yes.gif"
			                        					action="#{modificacionDatosDocenteForm.generate}"
			                        					onclick="javascript:return clickMade()"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}" />                        					                        			                        				
			                        				<h:commandButton value="Cancelar" 
			                        					image="/images/cancel.gif"
			                        					action="#{modificacionDatosDocenteForm.abort}"
			                        					immediate="true"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}"
			                        					onclick="javascript:return clickMade()"
			                	        				 />	
												<f:verbatim></td>
											</tr>										
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>	
							<f:subview 
								id="showFechaNacimiento"
								rendered="#{modificacionDatosDocenteForm.showFechaNacimiento}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Modificac�n de Estado Civil
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim>											                        																
											<tr>
                        						<td width="20%">
                        							Estado Civil
                        						</td>
                        					</tr>
                        						<td widht="80%"></f:verbatim>
													<h:inputText size="12"
														value="#{modificacionDatosDocenteForm.nuevaFechaNacimiento}"
														immediate="false"
														onkeypress="return keyEnterCheck(event, this)"
		                        						onblur="javascript:check_date(this)"
		                        						>																																			                         						
			                            			<f:convertDateTime timeZone="#{dateTime.timeZone}" 
		                                                 pattern="dd-MM-yyyy" />
		                        					</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>														
												<f:verbatim></td>
											</tr>							
											<tr>
                        						<td align="right"></f:verbatim>
													<h:commandButton image="/images/run.gif" 
														rendered="#{!modificacionDatosDocenteForm.showConfirmacion}"
														action="#{modificacionDatosDocenteForm.preGenerate}"
														onclick="javascript:return clickMade()" />
														
													<h:outputText
			                        					value="�Esta seguro que desea realizar este proceso?"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}" />                							                        				                				
			                        				<h:commandButton value="Si" 
			                        					image="/images/yes.gif"
			                        					action="#{modificacionDatosDocenteForm.generate}"
			                        					onclick="javascript:return clickMade()"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}" />                        					                        			                        				
			                        				<h:commandButton value="Cancelar" 
			                        					image="/images/cancel.gif"
			                        					action="#{modificacionDatosDocenteForm.abort}"
			                        					immediate="true"
			                        					rendered="#{modificacionDatosDocenteForm.showConfirmacion}"
			                        					onclick="javascript:return clickMade()"
			                	        				 />	
												<f:verbatim></td>
											</tr>										
										</table>
									</td>
								</tr>
							</f:verbatim>
							</f:subview>			
						</td>
					</tr>
				</table>

				</h:form>

			</td>			
		</tr>
	</table>

</f:view>

</body>
</html>