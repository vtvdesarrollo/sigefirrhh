<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportRegistroCargos'].elements['formReportRegistroCargos:reportId'].value;
			var name = 
				document.forms['formReportRegistroCargos'].elements['formReportRegistroCargos:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportRegistroCargosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportRegistroCargos">
				
    				<h:inputHidden id="reportId" value="#{reportRegistroCargosForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportRegistroCargosForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Registros Cargos/Puestos
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success" showDetail="true" />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Registro
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegistroCargosForm.findRegistro}"                                                    	
                                            	immediate="false"                        								                        								                                                    	
                                            	required="false"
                                            	valueChangeListener="#{reportRegistroCargosForm.changeRegistro}"
	    										onchange="this.form.submit()"
												title="Registro">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportRegistroCargosForm.colFindRegistro}" />
												<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Seleccione
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegistroCargosForm.seleccionUbicacion}"
	    										valueChangeListener="#{reportRegistroCargosForm.changeSeleccionUbicacion}"
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Todos" itemValue="T" /> 
                                            	<f:selectItem itemLabel="Una Region" itemValue="R" /> 
                                            	<f:selectItem itemLabel="Una Dependencia" itemValue="D" />                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Regi�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegistroCargosForm.idRegion}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reportRegistroCargosForm.showRegion}"
                                            	valueChangeListener="#{reportRegistroCargosForm.changeRegion}"
                                            	>
                                            	
                                                <f:selectItems value="#{reportRegistroCargosForm.colRegion}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    								<tr>
    									<td>
    										Dependencia
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegistroCargosForm.idDependencia}"
	    										valueChangeListener="#{reportRegistroCargosForm.changeDependencia}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reportRegistroCargosForm.showDependencia}"
                                            	>
                                            	
                                                <f:selectItems value="#{reportRegistroCargosForm.colDependencia}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Seleccione
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegistroCargosForm.seleccionCargo}"
	    										valueChangeListener="#{reportRegistroCargosForm.changeSeleccionCargo}"
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Todos" itemValue="T" />                                          
                                            	<f:selectItem itemLabel="Un Cargo" itemValue="C" />                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td>
    										Clasificador
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegistroCargosForm.idManualCargo}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reportRegistroCargosForm.showManualCargo}"
                                            	valueChangeListener="#{reportRegistroCargosForm.changeManualCargo}"
                                            	>
                                            	<f:selectItem itemLabel="Todos" itemValue="0" /> 
                                                <f:selectItems value="#{reportRegistroCargosForm.colManualCargo}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    								<tr>
    									<td>
    										Cargo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegistroCargosForm.idCargo}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reportRegistroCargosForm.showCargo}"
                                            	>
                                            	<f:selectItem itemLabel="Todos" itemValue="0" /> 
                                                <f:selectItems value="#{reportRegistroCargosForm.colCargo}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>				
    														
    								<tr>
    									<td>
    										Desde Grado
    									</td>
    									<td width="100%">
    										<h:inputText
    											
    											size="2"
    											id="GradoDesde"
    											maxlength="2"
    											value="#{reportRegistroCargosForm.gradoDesde}"
    											readonly="false"
    											required="false" />    
    									</td>
    								</tr>	
									<tr>
    									<td>
    										Hasta Grado
    									</td>
    									<td width="100%">
    										<h:inputText
    											
    											size="2"
    											id="GradoHasta"
    											maxlength="2"
    											value="#{reportRegistroCargosForm.gradoHasta}"
    											readonly="false"
    											required="false" />    
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Situaci�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegistroCargosForm.situacion}"
	    										
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Todos" itemValue="T" />  
                                            	<f:selectItem itemLabel="Ocupados" itemValue="O" />  
                                            	<f:selectItem itemLabel="Vacantes" itemValue="V" />                                        	
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td>
    										Estatus
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegistroCargosForm.estatus}"
	    										
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Todos" itemValue="T" />  
                                            	<f:selectItem itemLabel="Aprobados" itemValue="A" />  
                                            	<f:selectItem itemLabel="En Tr�mite" itemValue="E" />                                        	
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRegistroCargosForm.formato}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										
    											action="#{reportRegistroCargosForm.runReport}"
    											onclick="windowReport()" />
    										
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>