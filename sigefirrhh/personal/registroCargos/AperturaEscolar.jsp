<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{aperturaEscolarForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formAperturaEscolar">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Apertura Escolar
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/registroCargos/AperturaEscolar.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchAperturaEscolar"
								rendered="#{!aperturaEscolarForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														
											<f:verbatim><tr>
												<td>
													Region
												</td>
												<td width="100%"></f:verbatim>
					                                <h:selectOneMenu value="#{aperturaEscolarForm.findIdRegion}"
                                                    	immediate="false">
                                                 
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{aperturaEscolarForm.findColRegion}" />
                                                    </h:selectOneMenu>
                                                    
													
												<f:verbatim></td>
											</tr></f:verbatim>
											
											<f:verbatim><tr>
												<td>
													C�digo Entidad
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="12"
														maxlength="12"
														value="#{aperturaEscolarForm.findCodDependencia}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"                        																						
														 />

													<h:commandButton image="/images/find2.gif" 
														action="#{aperturaEscolarForm.findDependenciaByCodigoAndRegion}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>		
											<f:verbatim>
												<tr>
												<td>
													
												</td>
													<td ></f:verbatim>
														<h:outputText 		
														rendered="#{aperturaEscolarForm.showFindDependencia}"												
														value="#{aperturaEscolarForm.dependencia}"																												                         																						
													/>

															
												<f:verbatim></td>
											</tr></f:verbatim>	
											
											<f:verbatim><tr>
											<td>
													
												</td>
												<td></f:verbatim>
													<h:commandButton image="/images/find.gif" 
														rendered="#{aperturaEscolarForm.showFindDependencia}"
														action="#{aperturaEscolarForm.findAperturaEscolarByDependencia}"
														onclick="javascript:return clickMade()" />
												</td><f:verbatim>
												
											</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultAperturaEscolarByDependencia"
								rendered="#{aperturaEscolarForm.showAperturaEscolarByDependencia&&
								!aperturaEscolarForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{aperturaEscolarForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{aperturaEscolarForm.selectAperturaEscolar}"
                                						styleClass="listitem">
                                						<f:param name="idAperturaEscolar" value="#{result.idAperturaEscolar}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultAperturaEscolarByCodCargo"
								rendered="#{aperturaEscolarForm.showAperturaEscolarByCodCargo&&
								!aperturaEscolarForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{aperturaEscolarForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{aperturaEscolarForm.selectAperturaEscolar}"
                                						styleClass="listitem">
                                						<f:param name="idAperturaEscolar" value="#{result.idAperturaEscolar}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataAperturaEscolar"
								rendered="#{aperturaEscolarForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!aperturaEscolarForm.editing&&!aperturaEscolarForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{aperturaEscolarForm.editing&&!aperturaEscolarForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{aperturaEscolarForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{aperturaEscolarForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="aperturaEscolarForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{aperturaEscolarForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!aperturaEscolarForm.editing&&!aperturaEscolarForm.deleting&&aperturaEscolarForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{aperturaEscolarForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!aperturaEscolarForm.editing&&!aperturaEscolarForm.deleting&&aperturaEscolarForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{aperturaEscolarForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{aperturaEscolarForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{aperturaEscolarForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{aperturaEscolarForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{aperturaEscolarForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{aperturaEscolarForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>											
											<f:subview 
												id="viewDataClasificacionDependencia"
												rendered="#{aperturaEscolarForm.adding}">
											<f:verbatim><tr>
													<td>
														Region
													</td>
													<td width="100%"></f:verbatim>
						                                <h:selectOneMenu value="#{aperturaEscolarForm.findIdRegion}"
	                                                    	immediate="false">
															<f:selectItem itemLabel="Seleccione" itemValue="0" />
	                                                        <f:selectItems value="#{aperturaEscolarForm.findColRegion}" />
	                                                    </h:selectOneMenu>
	                                                    
														
													<f:verbatim></td>
												</tr></f:verbatim>
												
												<f:verbatim><tr>
													<td>
														C�digo Entidad
													</td>
													<td width="100%"></f:verbatim>
																<h:inputText size="12"
															maxlength="12"
															value="#{aperturaEscolarForm.findCodDependencia}"
															onchange="javascript:this.value=this.value.toUpperCase()"
															                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"                        																						
															 />
	
														<h:commandButton image="/images/find2.gif" 
															action="#{aperturaEscolarForm.findDependenciaByCodigoAndRegion}"
															onclick="javascript:return clickMade()" />
													<f:verbatim></td>
												</tr></f:verbatim>		
											</f:subview>
											<f:verbatim>
												<tr>
												<td>
													
												</td>
												<td ></f:verbatim>
													<h:outputText 		
													rendered="#{aperturaEscolarForm.showFindDependencia}"												
													value="#{aperturaEscolarForm.dependencia}"																												                         																						
												/>

															
												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Codigo Cargo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="codCargo"
	                        							maxlength="4"
                        								value="#{aperturaEscolarForm.aperturaEscolar.codCargo}"
                        								readonly="#{!aperturaEscolarForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="codCargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Cantidad Cargos Apertura
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="cargosApertura"
	                        							maxlength="2"
                        								value="#{aperturaEscolarForm.aperturaEscolar.cargosApertura}"
                        								readonly="#{!aperturaEscolarForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="cargosApertura" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cantidad Cargos Restante
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
	                        							
	                        							id="cargosRestante"	                        							
                        								value="#{aperturaEscolarForm.aperturaEscolar.cargosRestante}" >
                        								
                        								                        								                        																						            
                        							</h:outputText>
                        								
                        																					
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Total Horas Apertura
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="horasApertura"
	                        							maxlength="6"
                        								value="#{aperturaEscolarForm.aperturaEscolar.horasApertura}"
                        								readonly="#{!aperturaEscolarForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="horasApertura" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Total Horas Restante
                        						</td>
                        						<td></f:verbatim>
					                        		<h:outputText
	                        							
	                        							id="horasRestante"
	                        							
                        								value="#{aperturaEscolarForm.aperturaEscolar.horasRestante}"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								>
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:outputText>
                        								
                        																					<h:message for="horasRestante" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{aperturaEscolarForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!aperturaEscolarForm.editing&&!aperturaEscolarForm.deleting&&aperturaEscolarForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{aperturaEscolarForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!aperturaEscolarForm.editing&&!aperturaEscolarForm.deleting&&aperturaEscolarForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{aperturaEscolarForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{aperturaEscolarForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{aperturaEscolarForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{aperturaEscolarForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{aperturaEscolarForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{aperturaEscolarForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>