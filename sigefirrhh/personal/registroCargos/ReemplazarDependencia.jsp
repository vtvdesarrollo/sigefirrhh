<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
   
   </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reemplazarDependenciaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	               
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReemplazarDependencia">
				
    				<h:inputHidden id="reportId" value="#{reemplazarDependenciaForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reemplazarDependenciaForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reemplazar Dependencia
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success" showDetail="true" />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros para Reemplazar Dependencia
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Registro
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reemplazarDependenciaForm.findRegistro}"                                                    	
                                            	immediate="false"                        								                        								                                                    	
                                            	required="false"
                                            	valueChangeListener="#{reemplazarDependenciaForm.changeRegistro}"
	    										onchange="this.form.submit()"
												title="Registro">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reemplazarDependenciaForm.colFindRegistro}" />
												<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    									
    								<tr>
    									<td>
    										De la Dependencia
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reemplazarDependenciaForm.idDependencia}"
	    										valueChangeListener="#{reemplazarDependenciaForm.changeDependencia}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	>
                                            	
                                                <f:selectItems value="#{reemplazarDependenciaForm.colDependencia}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								
    								<tr>
    									<td>
    										A la Dependencia
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reemplazarDependenciaForm.idDependencia2}"
	    										valueChangeListener="#{reemplazarDependenciaForm.changeDependencia2}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	>
                                            	
                                                <f:selectItems value="#{reemplazarDependenciaForm.colDependencia2}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Seleccione
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reemplazarDependenciaForm.seleccionCargo}"
	    										valueChangeListener="#{reemplazarDependenciaForm.changeSeleccionCargo}"
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Todos" itemValue="T" />                                          
                                            	<f:selectItem itemLabel="Un Cargo" itemValue="C" />                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td>
    										Clasificador
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reemplazarDependenciaForm.idManualCargo}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reemplazarDependenciaForm.showManualCargo}"
                                            	valueChangeListener="#{reemplazarDependenciaForm.changeManualCargo}"
                                            	>
                                            	<f:selectItem itemLabel="Todos" itemValue="0" /> 
                                                <f:selectItems value="#{reemplazarDependenciaForm.colManualCargo}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    								<tr>
    									<td>
    										Cargo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reemplazarDependenciaForm.idCargo}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reemplazarDependenciaForm.showCargo}"
                                            	>
                                            	<f:selectItem itemLabel="Todos" itemValue="0" /> 
                                                <f:selectItems value="#{reemplazarDependenciaForm.colCargo}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>				
    														
    								<tr>
    									<td>
    										Desde Grado
    									</td>
    									<td width="100%">
    										<h:inputText
    											
    											size="2"
    											id="GradoDesde"
    											maxlength="2"
    											value="#{reemplazarDependenciaForm.gradoDesde}"
    											readonly="false"
    											required="false" />    
    									</td>
    								</tr>	
									<tr>
    									<td>
    										Hasta Grado
    									</td>
    									<td width="100%">
    										<h:inputText
    											
    											size="2"
    											id="GradoHasta"
    											maxlength="2"
    											value="#{reemplazarDependenciaForm.gradoHasta}"
    											readonly="false"
    											required="false" />    
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Situaci�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reemplazarDependenciaForm.situacion}"
	    										
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Todos" itemValue="T" />  
                                            	<f:selectItem itemLabel="Ocupados" itemValue="O" />  
                                            	<f:selectItem itemLabel="Vacantes" itemValue="V" />                                        	
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td>
    										Estatus
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reemplazarDependenciaForm.estatus}"
	    										
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Todos" itemValue="T" />  
                                            	<f:selectItem itemLabel="Aprobados" itemValue="A" />  
                                            	<f:selectItem itemLabel="En Tr�mite" itemValue="E" />                                        	
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								
    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										
    											action="#{reemplazarDependenciaForm.procesar}"
    											 />
    										
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>