<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
		<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{cargaNuevoRegistroCargosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formCargaNuevoRegistroCargos" enctype="multipart/form-data">    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Carga Nuevo Registro de Cargos
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/registrocargos/CargaNuevoRegistroCargos.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    							<h:outputLink value="#{cargaNuevoRegistroCargosForm.urlError}" rendered="#{cargaNuevoRegistroCargosForm.hasError}">
    							<f:verbatim>Archivo de errores  (Use el bot�n derecho del mouse)</f:verbatim></h:outputLink>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{cargaNuevoRegistroCargosForm.idTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{cargaNuevoRegistroCargosForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{cargaNuevoRegistroCargosForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td>    									
    										Archivo
    									</td>
    
    									<td width="50%">
                                            <x:inputFileUpload id="fileupload"
                                                accept="*.txt"
                                                value="#{cargaNuevoRegistroCargosForm.archivo}"
                                                storage="file"/>
    										<h:commandButton value="Cargar Archivo"
    											action="#{cargaNuevoRegistroCargosForm.cargar}" />
    									</td>
    									
    								</tr>
    								<tr>
    									
    								</tr>
    								<tr>
    									<td colspan="2">
                                            El archivo para importar, debe ser un archivo de texto tipo 
                                            con la extensi�n .TXT. Los campos debe estar distribuidos
                                            en columnas y separados por un tabulador (tecla TAB) en el siguiente orden:
                                            <br>
                                          	<br>
                                            <br>
                                            numeroRegistro codigoNomina codigoManualCargo codigoCargo codigoSede CodigoDependencia aprobadoMpf Situacion Cedula fechaCreacion horas
                                            <br>
                                            <br>  
                                            Ejemplo:
                                            <br>                                            
                                            <font style="font-family: courier; size=15">
                                            1   	240   	193		21111		01		101010201010 	S 	O 	 19189905 	01/08/2006 8.0<br>
                                            1  	 	241   	193		21112		01		101010201020 	N 	V        	0 	02/09/2006 8.0<br>
                                            </font>
                                            <br>
                                            <br>
    									</td>
    								</tr>
    								<tr>
										<td></td>
										<td align="left">
											
											<h:commandButton image="/images/run.gif"    												
                    							action="#{cargaNuevoRegistroCargosForm.procesar}"
												onclick="javascript:return clickMade()"
												 />
											
    											
											
										</td>
									</tr>				
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>
