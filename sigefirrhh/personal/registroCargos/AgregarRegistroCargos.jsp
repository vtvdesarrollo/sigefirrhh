<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{agregarRegistroCargosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			
			<td width="570" valign="top">
<h:form id="formRegistroCargos">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Agregar Cargos/Puestos
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/registro/AgregarRegistroCargos.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
										<h:commandButton value="Cerrar" 
                        					image="/images/close.gif"
                        					action="go_cancelOption"
                        					immediate="true"                        					
                        					onclick="javascript:return clickMade()"
                	        				 />
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchRegistroCargos"
								>
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Validar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
                        					<f:verbatim>
                        					<tr>
                        						<td>
                        							Registro
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{agregarRegistroCargosForm.findRegistro}"
	                                                    valueChangeListener="#{agregarRegistroCargosForm.changeRegistro}"                                                    	
                                                    	immediate="false"                        								                        								                                                    	
                                                    	required="false"
														title="Registro">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{agregarRegistroCargosForm.colFindRegistro}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{agregarRegistroCargosForm.findRegistroCargosCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{agregarRegistroCargosForm.findRegistroCargosByCodigoNominaNumeroRegistro}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>									

</h:form>
							<f:subview 
								id="AgregarRegistroCargos"
								rendered="#{agregarRegistroCargosForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Agregando"
														rendered="#{agregarRegistroCargosForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
											<h:form id="agregarRegistroCargosForm">
            							<f:subview 
            								id="dataRegistroCargos"
            								rendered="#{agregarRegistroCargosForm.showAdd}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				
                                    	        				
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>            								
                        					<f:verbatim><tr>
                        						<td>
                        							Clasificador
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{agregarRegistroCargosForm.selectManualCargoForCargo}"
                                                    	
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{agregarRegistroCargosForm.changeManualCargoForCargo}"
                        								required="false"
														immediate="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{agregarRegistroCargosForm.colManualCargoForCargo}" />
													</h:selectOneMenu>
													<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{agregarRegistroCargosForm.selectCargo}"
                                                    	rendered="#{agregarRegistroCargosForm.showCargo}"
                                                    	onchange="this.form.submit()"
                                                    	immediate="false"
                                                    	required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{agregarRegistroCargosForm.colCargo}" />
                                                    </h:selectOneMenu> <f:verbatim></td>
                        					</tr>  </f:verbatim>
                        					
											<f:verbatim><tr>
                        						<td>
                        							Sede
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{agregarRegistroCargosForm.selectSede}"
                                                    	onchange="this.form.submit()"
                                                    	valueChangeListener="#{agregarRegistroCargosForm.changeSede}"
                        								immediate="false"
                        								required="false"
														title="sede">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{agregarRegistroCargosForm.colSede}" />
                                                    </h:selectOneMenu> <f:verbatim></td>
                        					</tr> </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Dependencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{agregarRegistroCargosForm.selectDependencia}"
                                                    	rendered="#{agregarRegistroCargosForm.showDependencia}"
                                                    	onchange="this.form.submit()"
                                                    	immediate="false"
                        								required="false"
														title="Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{agregarRegistroCargosForm.colDependencia}" />
                                                    </h:selectOneMenu> <f:verbatim></td>
                        					</tr>  </f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Horas
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							maxlength="4"
                        								value="#{agregarRegistroCargosForm.registroCargos.horas}"
                        								
                        								id="Horas"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Reporta a
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="7"
	                        							maxlength="7"
                        								value="#{agregarRegistroCargosForm.registroCargos.reportaRac}"
                        								
                        								id="Reportaa"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						 >
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						
                        							<td align="left">
													
													
																	
                        						</td>
                        						<td></f:verbatim>
                                                                           						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
									</f:subview>
            							<f:subview 
            								id="commandsRegistroCargos"
            								rendered="#{agregarRegistroCargosForm.showAdd}">
                        				<f:verbatim><table>
                        					<tr>
                        						<td></f:verbatim>
                        						<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{agregarRegistroCargosForm.save}"
                                    					onclick="javascript:return clickMade()"
    	                    	        				/>
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{agregarRegistroCargosForm.abortUpdate}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>