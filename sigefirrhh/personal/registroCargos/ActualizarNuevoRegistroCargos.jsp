<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{actualizarNuevoRegistroCargosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formActualizarNuevoRegistroCargos">
				
    				<h:inputHidden id="reportId" value="#{actualizarNuevoRegistroCargosForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{actualizarNuevoRegistroCargosForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Actulizar Nuevo Registro Cargos
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success" showDetail="true" />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Proceso
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    							
    							
    							     <tr>
    									<td>
    										Tipo Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{actualizarNuevoRegistroCargosForm.idTipoPersonal}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{actualizarNuevoRegistroCargosForm.changeTipoPersonal}"
                                            	>
                                            	
                                                <f:selectItems value="#{actualizarNuevoRegistroCargosForm.colTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    							    								    																	
    								<tr>
    									<td>
    										Registro Anterior
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{actualizarNuevoRegistroCargosForm.findRegistro}"                                                    	
                                            	immediate="false"                        								                        								                                                    	
                                            	required="false"
                                            	valueChangeListener="#{actualizarNuevoRegistroCargosForm.changeRegistro}"
	    										onchange="this.form.submit()"
												title="Registro Viejo">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{actualizarNuevoRegistroCargosForm.colFindRegistro}" />
												<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								
    								
    								<tr>
    									<td>
    										Registro Nuevo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{actualizarNuevoRegistroCargosForm.findRegistro2}"                                                    	
                                            	immediate="false"                        								                        								                                                    	
                                            	required="false"
                                            	valueChangeListener="#{actualizarNuevoRegistroCargosForm.changeRegistro2}"
	    										onchange="this.form.submit()"
												title="Registro Nuevo">
												<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{actualizarNuevoRegistroCargosForm.colFindRegistro2}" />
												<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    									   action="#{actualizarNuevoRegistroCargosForm.procesar}"
    										/>
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>