<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{registroCargosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	               
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formRegistroCargos">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Consultar/Eliminar Posici�n Registro
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/registro/RegistroCargos.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchRegistroCargos"
								rendered="#{!registroCargosForm.showData&&
									!registroCargosForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
                        					<f:verbatim>
                        					<tr>
                        						<td>
                        							Registro
                        						</td>
                        						<td></f:verbatim>
													
                                                    <h:selectOneMenu value="#{registroCargosForm.findRegistro}"                                                    	
                                                    	immediate="false"                        								                        								                                                    	
                                                    	required="false"
														title="Registro">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosForm.colFindRegistro}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim>
                                                    
                                                    </f:verbatim>                        						
                                                    
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{registroCargosForm.findRegistroCargosCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{registroCargosForm.findRegistroCargosByCodigoNominaNumeroRegistro}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>									
							<f:subview 
								id="viewResultRegistroCargos"
								rendered="#{registroCargosForm.showResult&&
								!registroCargosForm.showData&&
								!registroCargosForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{registroCargosForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{registroCargosForm.selectRegistroCargos}"
                                						styleClass="listitem">
                                						<f:param name="idRegistroCargos" value="#{result.idRegistroCargos}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataRegistroCargos"
								rendered="#{registroCargosForm.showData||registroCargosForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!registroCargosForm.editing&&!registroCargosForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{registroCargosForm.editing&&!registroCargosForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{registroCargosForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{registroCargosForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
<h:form id="registroCargosForm">
<h:inputHidden id="scrollx" value="#{registroCargosForm.scrollx}" />
<h:inputHidden id="scrolly" value="#{registroCargosForm.scrolly}" />
            							<f:subview 
            								id="dataRegistroCargos"
            								rendered="#{registroCargosForm.showData}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				
                                                				    <h:commandButton value="Eliminar" 
				                                    					image="/images/delete.gif"
				                        								action="#{registroCargosForm.delete}"
				                        								immediate="true"
				                        								onclick="javascript:return clickMade()"
				                        								rendered="#{!registroCargosForm.editing&&!registroCargosForm.deleting&&registroCargosForm.login.eliminar&&!registroCargosForm.racOcupado}" />
                                                				  
                                                				  	<h:outputText
                                                					   value="�Seguro que desea eliminar?"
                                                					   rendered="#{registroCargosForm.deleting}" />
                                        				
                                                				    <h:commandButton value="Si" 
                                                					   image="/images/yes.gif"
                                                					   immediate="true"
                                                					   action="#{registroCargosForm.deleteOk}"
                                                					   onclick="javascript:return clickMade()"
                                                					   rendered="#{registroCargosForm.deleting}" />
                                                				  
                                                				  
				                	        						<h:commandButton value="Cancelar" 
				                                    					image="/images/cancel.gif"
				                        								action="#{registroCargosForm.abortUpdate}"
				                        								immediate="true"
				                        								onclick="javascript:return clickMade()"
				                	        							 />
				                	        				    <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim><tr>
                        						<td>
            										Trabajador
                        						</td>
                        						<td></f:verbatim>
                        							<h:outputText value="#{registroCargosForm.registroCargos.trabajador.personal}" />
                        						<f:verbatim></td>
            								</tr></f:verbatim>
            								
                        					<f:verbatim><tr>
                        						<td>
                        							Registro
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosForm.selectRegistro}"
                                                    	disabled="#{!registroCargosForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="true"
														title="Registro">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosForm.colRegistro}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�digo N�mina
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="7"
	                        							maxlength="7"
                        								value="#{registroCargosForm.registroCargos.codigoNomina}"
                        								                        								readonly="#{!registroCargosForm.editing}"
                        								id="CodigoNomina"
                        								required="true"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						 >
                        							</h:inputText>
                        								<f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Situaci�n
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosForm.registroCargos.situacion}"
                                                    	disabled="#{!registroCargosForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="false"
                                                    	title="Situaci�n">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosForm.listSituacion}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>

                        					<f:verbatim><tr>
                        						<td>
                        							Manual
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosForm.selectManualCargoForCargo}"
                                                    	disabled="#{!registroCargosForm.editing}"
                                                    	onchange="saveScrollCoordinates();this.form.submit()"
														valueChangeListener="#{registroCargosForm.changeManualCargoForCargo}"
                        																						required="false"
														immediate="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{registroCargosForm.colManualCargoForCargo}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosForm.selectCargo}"
                                                    	disabled="#{!registroCargosForm.editing}"
                                                    	rendered="#{registroCargosForm.showCargo
														 }"
                                                    	immediate="false"
                                                    	required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{registroCargosForm.colCargo}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim>
											<tr>
                								<td>Grado</td>
                    							<td>
												</f:verbatim>
													<h:inputText
		                    							size="2"
    		                							maxlength="2"
	    		        								value="#{registroCargosForm.registroCargos.cargo.grado}"
														readonly="true"
            											id="Grado"
            											required="false"
            											title="CampoX"
            											alt="CampoX"
            											onchange="javascript:this.value=this.value.toUpperCase()"
            											onkeypress="return keyEnterCheck(event, this)">
													</h:inputText>
												<f:verbatim>
												</td>
	            	    					</tr>
											</f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Dependencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosForm.selectDependencia}"
                                                    	disabled="#{!registroCargosForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="false"
														title="Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosForm.colDependencia}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							sede
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{registroCargosForm.selectSede}"
                                                    	disabled="#{!registroCargosForm.editing}"
                                                    	immediate="false"
                        								                        								                                                    	required="false"
														title="sede">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{registroCargosForm.colSede}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Horas
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							maxlength="4"
                        								value="#{registroCargosForm.registroCargos.horas}"
                        								                        								readonly="#{!registroCargosForm.editing}"
                        								id="Horas"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								                        								                        																						 style="text-align:right"  >
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Reporta a
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="7"
	                        							maxlength="7"
                        								value="#{registroCargosForm.registroCargos.reportaRac}"
                        								                        								readonly="#{!registroCargosForm.editing}"
                        								id="Reportaa"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						 >
                        							</h:inputText>
                        								                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						
                        							<td align="left">
													
													
																	
                        						</td>
                        						<td></f:verbatim>
                                                                           						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
									</f:subview>
            							<f:subview 
            								id="commandsRegistroCargos"
            								rendered="#{registroCargosForm.showData}">
                        				<f:verbatim><table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{registroCargosForm.abortUpdate}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>