<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{historicoCargosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formHistoricoCargos">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Trayectoria Posici�n Registro
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/registro/HistoricoCargos.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchHistoricoCargos"
								rendered="#{!historicoCargosForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Registro
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{historicoCargosForm.findSelectRegistro}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoCargosForm.findColRegistro}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{historicoCargosForm.findHistoricoCargosByRegistro}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													C�digo N�mina
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="7"
														maxlength="7"
														value="#{historicoCargosForm.findCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{historicoCargosForm.findHistoricoCargosByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultHistoricoCargosByRegistro"
								rendered="#{historicoCargosForm.showHistoricoCargosByRegistro&&
								!historicoCargosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{historicoCargosForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{historicoCargosForm.selectHistoricoCargos}"
                                						styleClass="listitem">
                                						<f:param name="idHistoricoCargos" value="#{result.idHistoricoCargos}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultHistoricoCargosByCodigoNomina"
								rendered="#{historicoCargosForm.showHistoricoCargosByCodigoNomina&&
								!historicoCargosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{historicoCargosForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{historicoCargosForm.selectHistoricoCargos}"
                                						styleClass="listitem">
                                						<f:param name="idHistoricoCargos" value="#{result.idHistoricoCargos}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataHistoricoCargos"
								rendered="#{historicoCargosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!historicoCargosForm.editing&&!historicoCargosForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{historicoCargosForm.editing&&!historicoCargosForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{historicoCargosForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{historicoCargosForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="historicoCargosForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{historicoCargosForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{historicoCargosForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{historicoCargosForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{historicoCargosForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{historicoCargosForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{historicoCargosForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Registro
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoCargosForm.selectRegistro}"
														id="registro"
                                                    	disabled="#{!historicoCargosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Registro">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoCargosForm.colRegistro}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="registro" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�digo N�mina
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="7"
	                        							id="codigoNomina"
	                        							maxlength="7"
                        								value="#{historicoCargosForm.historicoCargos.codigoNomina}"
                        								readonly="#{!historicoCargosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="codigoNomina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Situaci�n
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoCargosForm.historicoCargos.situacion}"
	                                                    id="situacion"
                                                    	disabled="#{!historicoCargosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Situaci�n">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoCargosForm.listSituacion}" />
                                                    </h:selectOneMenu>													<h:message for="situacion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Movimiento
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoCargosForm.historicoCargos.movimiento}"
	                                                    id="movimiento"
                                                    	disabled="#{!historicoCargosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Movimiento">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoCargosForm.listMovimiento}" />
                                                    </h:selectOneMenu>													<h:message for="movimiento" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha del Movimiento
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="fechaMovimiento"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{historicoCargosForm.historicoCargos.fechaMovimiento}"
                        								readonly="#{!historicoCargosForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onblur="javascript:check_date(this)"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
													<h:message for="fechaMovimiento" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Causa Movimiento
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoCargosForm.selectCausaMovimiento}"
														id="causaMovimiento"
                                                    	disabled="#{!historicoCargosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Causa Movimiento">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoCargosForm.colCausaMovimiento}" />
                                                    </h:selectOneMenu>													<h:message for="causaMovimiento" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Manual Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoCargosForm.selectManualCargoForCargo}"
                                                    	disabled="#{!historicoCargosForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="ManualCargoForCargo"
														valueChangeListener="#{historicoCargosForm.changeManualCargoForCargo}"
                        																						immediate="false"
														required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{historicoCargosForm.colManualCargoForCargo}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoCargosForm.selectCargo}"
	                                                    id="cargo"
                                                    	disabled="#{!historicoCargosForm.editing}"
                                                    	rendered="#{historicoCargosForm.showCargo
														 }"
                                                    	immediate="false"
                                                    	required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{historicoCargosForm.colCargo}" />
                                                    </h:selectOneMenu>													<h:message for="cargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Dependencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoCargosForm.selectDependencia}"
														id="dependencia"
                                                    	disabled="#{!historicoCargosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoCargosForm.colDependencia}" />
                                                    </h:selectOneMenu>													<h:message for="dependencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="8"
	                        							id="cedula"
	                        							maxlength="8"
                        								value="#{historicoCargosForm.historicoCargos.cedula}"
                        								readonly="#{!historicoCargosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="cedula" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Primer Apellido
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="20"
	                        							id="primerApellido"
	                        							maxlength="20"
                        								value="#{historicoCargosForm.historicoCargos.primerApellido}"
                        								readonly="#{!historicoCargosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="primerApellido" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Segundo Apellido
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="20"
	                        							id="segundoApellido"
	                        							maxlength="20"
                        								value="#{historicoCargosForm.historicoCargos.segundoApellido}"
                        								readonly="#{!historicoCargosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="segundoApellido" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Primer Nombre
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="20"
	                        							id="primerNombre"
	                        							maxlength="20"
                        								value="#{historicoCargosForm.historicoCargos.primerNombre}"
                        								readonly="#{!historicoCargosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="primerNombre" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Segundo Nombre
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="20"
	                        							id="segundoNombre"
	                        							maxlength="20"
                        								value="#{historicoCargosForm.historicoCargos.segundoNombre}"
                        								readonly="#{!historicoCargosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="segundoNombre" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Horas
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="horas"
	                        							maxlength="4"
                        								value="#{historicoCargosForm.historicoCargos.horas}"
                        								readonly="#{!historicoCargosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="horas" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{historicoCargosForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{historicoCargosForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{historicoCargosForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{historicoCargosForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{historicoCargosForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{historicoCargosForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>