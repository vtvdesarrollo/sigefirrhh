<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">

<f:view>
	<x:saveState value="#{encargaduriaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formEncargaduria">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Encargadur�as
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/expediente/Encargaduria.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									<f:subview 
										id="searchEncargaduria"
										rendered="#{!encargaduriaForm.showData&&
										!encargaduriaForm.showAdd}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{encargaduriaForm.findPersonalCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
        														onfocus="return deleteZero(event, this)"
        														onblur="javascript:fieldEmpty(this)"
        														onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{encargaduriaForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{encargaduriaForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{encargaduriaForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{encargaduriaForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{encargaduriaForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{encargaduriaForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{encargaduriaForm.showResultPersonal&&
									!encargaduriaForm.showData&&
									!encargaduriaForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <h:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{encargaduriaForm.resultPersonal}"
    	                                            
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{encargaduriaForm.findEncargaduriaByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </h:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{encargaduriaForm.selectedPersonal&&
									!encargaduriaForm.showData&&
									!encargaduriaForm.editing&&
									!encargaduriaForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>
            									<h:commandLink value="#{encargaduriaForm.personal}  <-- clic aqui"
    	                            						action="#{encargaduriaForm.findEncargaduriaByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{encargaduriaForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{encargaduriaForm.add}"
            													onclick="javascript:return clickMade()"
            													rendered="#{!encargaduriaForm.editing&&!encargaduriaForm.deleting&&encargaduriaForm.login.agregar}" />
            												<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{encargaduriaForm.abort}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />
                                        	    			<f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultEncargaduria"
									rendered="#{encargaduriaForm.showResult&&
									!encargaduriaForm.showData&&
									!encargaduriaForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <h:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{encargaduriaForm.result}"
                                            	    
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{encargaduriaForm.selectEncargaduria}"
	                                						styleClass="listitem">
    		                            						<f:param name="idEncargaduria" value="#{result.idEncargaduria}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </h:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataEncargaduria"
								rendered="#{encargaduriaForm.showData||encargaduriaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!encargaduriaForm.editing&&!encargaduriaForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{encargaduriaForm.editing&&!encargaduriaForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{encargaduriaForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{encargaduriaForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="encargaduriaForm">
	                                            <h:inputHidden id="scrollx" value="#{encargaduriaForm.scrollx}" />
    	                                        <h:inputHidden id="scrolly" value="#{encargaduriaForm.scrolly}" />
	    	        							<f:subview 
    	    	    								id="dataEncargaduria"
        	    									rendered="#{encargaduriaForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{encargaduriaForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!encargaduriaForm.editing&&!encargaduriaForm.deleting&&encargaduriaForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{encargaduriaForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!encargaduriaForm.editing&&!encargaduriaForm.deleting&&encargaduriaForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{encargaduriaForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{encargaduriaForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{encargaduriaForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{encargaduriaForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{encargaduriaForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
						                        								action="#{encargaduriaForm.abortUpdate}"
						                        								immediate="true"
						                        								onclick="javascript:return clickMade()"
						                	        							 />
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{encargaduriaForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
	            											<f:verbatim><tr>
    	                    								<td>
        	                									Cargo
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{encargaduriaForm.encargaduria.cargo}"
                	    		    								                    	    										readonly="#{!encargaduriaForm.editing}"
                        											id="Cargo"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	 >
								                        		</h:inputText>
                        			                        								<f:verbatim><span class="required"> *</span></f:verbatim>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Inicio
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaInicio"
                		        									value="#{encargaduriaForm.encargaduria.fechaInicio}"
                    		    									readonly="#{!encargaduriaForm.editing}"
                    		    									onblur="javascript:check_date(this)"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="true">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Fin
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaFin"
                		        									value="#{encargaduriaForm.encargaduria.fechaFin}"
                    		    									readonly="#{!encargaduriaForm.editing}"
                    		    									onblur="javascript:check_date(this)"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
									        	            	    							<f:verbatim></td>
	        	            	    					</tr>
														</f:verbatim>
	        	            	    					<f:verbatim>
														<tr>
    	                    								<td>Tiempo</td>
		                        							<td>
		                        							<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                        								<tr>
		                        									<td>
		                        									</f:verbatim>
																		<h:inputText
																			size="25"
																			maxlength="25"
																			id="DiasExperiencia"
																			value="#{encargaduriaForm.diferencia}"
																			readonly="true"
																			onchange="javascript:this.value=this.value.toUpperCase()"
																			onkeypress="return keyEnterCheck(event, this)">																	
																		</h:inputText>
																		<f:verbatim><span class="required"></span></f:verbatim>
																	<f:verbatim>
																	</td>
														        	<td align="left">
																	</f:verbatim>				
																		<h:commandButton value="Calcular" 
						                                					image="/images/calculate.gif"
						                                					action="this.form.submit()"
						                                					onclick="javascript:return clickMade()"/>
						                        	        		<f:verbatim>
																	</td>
																</tr>
															</table>
															</td>
	        	            	    					</tr>
														</f:verbatim>
	        	            	    					<f:verbatim>
														<tr>
    	                    								<td>
        	                									Dependencia Administrativa
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{encargaduriaForm.encargaduria.dependencia}"
                	    		    								                    	    										readonly="#{!encargaduriaForm.editing}"
                        											id="DependenciaAdministrativa"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    							                        		<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="4"
    	        		                							id="Observaciones"
                    		        								                        		    								value="#{encargaduriaForm.encargaduria.observaciones}"
                            										readonly="#{!encargaduriaForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										onkeypress="return keyEnterCheck(event, this)"
                            										required="false" />
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																										<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsEncargaduria"
            										rendered="#{encargaduriaForm.showData}">
		                        					<f:verbatim><table>
    		                    						<tr>
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{encargaduriaForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!encargaduriaForm.editing&&!encargaduriaForm.deleting&&encargaduriaForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{encargaduriaForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!encargaduriaForm.editing&&!encargaduriaForm.deleting&&encargaduriaForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{encargaduriaForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{encargaduriaForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{encargaduriaForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{encargaduriaForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{encargaduriaForm.editing}" />	
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{encargaduriaForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>