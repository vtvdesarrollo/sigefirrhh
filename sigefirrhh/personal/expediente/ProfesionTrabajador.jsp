<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{profesionTrabajadorForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />
				<%
	  			}else { %>
	  				 
	  				
				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formProfesionTrabajador">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Profesiones y/o Oficios
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/expediente/ProfesionTrabajador.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success"  />
										</td>
									</tr>
									
									<f:subview 
										id="searchProfesionTrabajador"
										rendered="#{!profesionTrabajadorForm.showData&&
										!profesionTrabajadorForm.showAdd&&
										!profesionTrabajadorForm.login.servicioPersonal}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{profesionTrabajadorForm.findPersonalCedula}"
																onkeypress="javascript:return keyIntegerCheck(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{profesionTrabajadorForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{profesionTrabajadorForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{profesionTrabajadorForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{profesionTrabajadorForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{profesionTrabajadorForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{profesionTrabajadorForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{profesionTrabajadorForm.showResultPersonal&&
									!profesionTrabajadorForm.showData&&
									!profesionTrabajadorForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <x:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{profesionTrabajadorForm.resultPersonal}"
    	                                            preserveDataModel="false"
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{profesionTrabajadorForm.findProfesionTrabajadorByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </x:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{profesionTrabajadorForm.selectedPersonal&&
									!profesionTrabajadorForm.showData&&
									!profesionTrabajadorForm.editing&&
									!profesionTrabajadorForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{profesionTrabajadorForm.personal}   <-- clic aqui"
    	                            						action="#{profesionTrabajadorForm.findProfesionTrabajadorByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{profesionTrabajadorForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{profesionTrabajadorForm.add}"
            													rendered="#{!profesionTrabajadorForm.editing&&!profesionTrabajadorForm.deleting&&profesionTrabajadorForm.login.agregar}" />

                                        	    			<% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{profesionTrabajadorForm.abort}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                                                    	    <%}%>
                                                    	   <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultProfesionTrabajador"
									rendered="#{profesionTrabajadorForm.showResult&&
									!profesionTrabajadorForm.showData&&
									!profesionTrabajadorForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <x:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{profesionTrabajadorForm.result}"
                                            	    preserveDataModel="false"
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{profesionTrabajadorForm.selectProfesionTrabajador}"
	                                						styleClass="listitem">
    		                            						<f:param name="idProfesionTrabajador" value="#{result.idProfesionTrabajador}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </x:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataProfesionTrabajador"
								rendered="#{profesionTrabajadorForm.showData||profesionTrabajadorForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!profesionTrabajadorForm.editing&&!profesionTrabajadorForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{profesionTrabajadorForm.editing&&!profesionTrabajadorForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{profesionTrabajadorForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{profesionTrabajadorForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="profesionTrabajadorForm">
	    	        							<f:subview 
    	    	    								id="dataProfesionTrabajador"
        	    									rendered="#{profesionTrabajadorForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{profesionTrabajadorForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!profesionTrabajadorForm.editing&&!profesionTrabajadorForm.deleting&&profesionTrabajadorForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{profesionTrabajadorForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!profesionTrabajadorForm.editing&&!profesionTrabajadorForm.deleting&&profesionTrabajadorForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{profesionTrabajadorForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{profesionTrabajadorForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{profesionTrabajadorForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{profesionTrabajadorForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{profesionTrabajadorForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{profesionTrabajadorForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{profesionTrabajadorForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
																													                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Profesi�n/Oficio
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{profesionTrabajadorForm.selectProfesion}"
        		                                                	disabled="#{!profesionTrabajadorForm.editing}"
            		                                            	immediate="false"
				                        							id="profesion"
                		            								                    		        								                        		                                	required="true"
    																title="Profesi�n/Oficio">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{profesionTrabajadorForm.colProfesion}" />
																							<f:validator validatorId="eforserver.RequiredJSFValidator"/>
								                                    	                </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>																									<h:message for="profesion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																		                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Desempe�a actualmente
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{profesionTrabajadorForm.profesionTrabajador.actualmente}"
        	                                        	        	disabled="#{!profesionTrabajadorForm.editing}"
            	                                        	    	immediate="false"
				                        							id="actualmente"
                	            									                    	        										                        	                                	required="false"
    	                        	                            	title="Desempe�a actualmente">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{profesionTrabajadorForm.listActualmente}" />
					    	            	                        	            </h:selectOneMenu>																				<h:message for="actualmente" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																					<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsProfesionTrabajador"
            										rendered="#{profesionTrabajadorForm.showData}">
		                        					<f:verbatim><table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{profesionTrabajadorForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!profesionTrabajadorForm.editing&&!profesionTrabajadorForm.deleting&&profesionTrabajadorForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{profesionTrabajadorForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!profesionTrabajadorForm.editing&&!profesionTrabajadorForm.deleting&&profesionTrabajadorForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{profesionTrabajadorForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{profesionTrabajadorForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{profesionTrabajadorForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{profesionTrabajadorForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{profesionTrabajadorForm.editing}" />
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{profesionTrabajadorForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>