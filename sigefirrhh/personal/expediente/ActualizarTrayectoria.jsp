<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{actualizarTrayectoriaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />  	  				 
	  				
				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formActualizarTrayectoria">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Actualizar Trayectoria en Organismo
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/expediente/ActualizarTrayectoria.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success"  />
										</td>
									</tr>
									
									<f:subview 
										id="searchActualizarTrayectoria"
										rendered="#{!actualizarTrayectoriaForm.showData&&
										!actualizarTrayectoriaForm.showAdd&&
										!actualizarTrayectoriaForm.login.servicioPersonal}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{actualizarTrayectoriaForm.findPersonalCedula}"
																onkeypress="javascript:return keyIntegerCheck(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{actualizarTrayectoriaForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarTrayectoriaForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarTrayectoriaForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarTrayectoriaForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{actualizarTrayectoriaForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{actualizarTrayectoriaForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{actualizarTrayectoriaForm.showResultPersonal&&
									!actualizarTrayectoriaForm.showData&&
									!actualizarTrayectoriaForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <x:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{actualizarTrayectoriaForm.resultPersonal}"
    	                                            preserveDataModel="false"
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{actualizarTrayectoriaForm.findTrayectoriaByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </x:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{actualizarTrayectoriaForm.selectedPersonal&&
									!actualizarTrayectoriaForm.showData&&
									!actualizarTrayectoriaForm.editing&&
									!actualizarTrayectoriaForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{actualizarTrayectoriaForm.personal}   <-- clic aqui"
    	                            						action="#{actualizarTrayectoriaForm.findTrayectoriaByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{actualizarTrayectoriaForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{actualizarTrayectoriaForm.add}"
            													rendered="#{!actualizarTrayectoriaForm.editing&&!actualizarTrayectoriaForm.deleting&&actualizarTrayectoriaForm.login.agregar}" />

                                        	    			<% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{actualizarTrayectoriaForm.abort}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                                                    	    <%}%>
                                                    	   <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultActualizarTrayectoria"
									rendered="#{actualizarTrayectoriaForm.showResult&&
									!actualizarTrayectoriaForm.showData&&
									!actualizarTrayectoriaForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <x:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{actualizarTrayectoriaForm.result}"
                                            	    preserveDataModel="false"
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{actualizarTrayectoriaForm.selectTrayectoria}"
	                                						styleClass="listitem">
    		                            						<f:param name="idTrayectoria" value="#{result.idTrayectoria}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </x:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataActualizarTrayectoria"
								rendered="#{actualizarTrayectoriaForm.showData||actualizarTrayectoriaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!actualizarTrayectoriaForm.editing&&!actualizarTrayectoriaForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{actualizarTrayectoriaForm.editing&&!actualizarTrayectoriaForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{actualizarTrayectoriaForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{actualizarTrayectoriaForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="actualizarTrayectoriaForm">
	    	        							<f:subview 
    	    	    								id="dataActualizarTrayectoria"
        	    									rendered="#{actualizarTrayectoriaForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{actualizarTrayectoriaForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!actualizarTrayectoriaForm.editing&&!actualizarTrayectoriaForm.deleting&&actualizarTrayectoriaForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{actualizarTrayectoriaForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!actualizarTrayectoriaForm.editing&&!actualizarTrayectoriaForm.deleting&&actualizarTrayectoriaForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{actualizarTrayectoriaForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{actualizarTrayectoriaForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{actualizarTrayectoriaForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{actualizarTrayectoriaForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{actualizarTrayectoriaForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{actualizarTrayectoriaForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{actualizarTrayectoriaForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>																				                    	    																																					                    	    																							                    	    						
															<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Vigencia
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaVigencia"
                		        									value="#{actualizarTrayectoriaForm.trayectoria.fechaVigencia}"
                    		    									readonly="#{!actualizarTrayectoriaForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="fechaVigencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																					                    	    						<f:verbatim><tr>
    	                    								<td>
	    	                    								<h:outputText 
    	                    										rendered="#{actualizarTrayectoriaForm.adding}"
    	                    										value="Regi�n" />
    	                    									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="C�digo Regi�n" />
	            	            							</td>
		                        							<td></f:verbatim>
			                        							<h:selectOneMenu value="#{actualizarTrayectoriaForm.selectRegion}"
				                        							rendered="#{actualizarTrayectoriaForm.adding}"
				                        							valueChangeListener="#{actualizarTrayectoriaForm.changeRegion}"
						    										onchange="this.form.submit()"
					                                            	immediate="false">
					                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  
					                                                <f:selectItems value="#{actualizarTrayectoriaForm.listRegion}" />
					                                            </h:selectOneMenu>
																<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
	    	    		                							rendered="#{!actualizarTrayectoriaForm.adding}"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.codRegion}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="codRegion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codRegion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Nombre Regi�n" />
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.nombreRegion}"
            	    		        								rendered="#{!actualizarTrayectoriaForm.adding}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="nombreRegion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreRegion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
	    	                    								<h:outputText 
    	                    										rendered="#{actualizarTrayectoriaForm.adding}"
    	                    										value="Dependencia" />
        	                									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Codigo Dependencia" />
	            	            							</td>
		                        							<td></f:verbatim>
			                        							<h:selectOneMenu value="#{actualizarTrayectoriaForm.idDependencia}"
					                        							rendered="#{actualizarTrayectoriaForm.adding&&actualizarTrayectoriaForm.showDependencia}"					                        							
							    										
						                                            	immediate="false">	
						                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  					                                            	
						                                                <f:selectItems value="#{actualizarTrayectoriaForm.listDependencia}" />
						                                        </h:selectOneMenu>
																<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.codDependencia}"
            	    		        								rendered="#{!actualizarTrayectoriaForm.adding}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="codDependencia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        		</h:inputText>
                        																																<h:message for="codDependencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Nombre Dependencia" />
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="100"
	    	    		                							maxlength="100"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.nombreDependencia}"
            	    		        								rendered="#{!actualizarTrayectoriaForm.adding}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="nombreDependencia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreDependencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText 
    	                    										rendered="#{actualizarTrayectoriaForm.adding}"
    	                    										value="Tipo Movimiento" />
        	                									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Codigo Causa Movimiento" />
	            	            							</td>
		                        							<td></f:verbatim>
			                        								<h:selectOneMenu value="#{actualizarTrayectoriaForm.selectMovimientoPersonal}"
					                        							rendered="#{actualizarTrayectoriaForm.adding}"
					                        							valueChangeListener="#{actualizarTrayectoriaForm.changeMovimientoPersonal}"
							    										onchange="this.form.submit()"
						                                            	immediate="false">
						                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  
						                                                <f:selectItems value="#{actualizarTrayectoriaForm.listMovimientoPersonal}" />
						                                            </h:selectOneMenu>
																	<h:inputText
		    		                    							size="3"
	    	    		                							maxlength="3"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.codCausaMovimiento}"
            	    		        								rendered="#{!actualizarTrayectoriaForm.adding}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="codCausaMovimiento"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)">
								                        									</h:inputText>
                        																																<h:message for="codCausaMovimiento" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText 
    	                    										rendered="#{actualizarTrayectoriaForm.adding}"
    	                    										value="Causa Movimiento" />
        	                									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Causa Movimiento" />
	            	            							</td>
		                        							<td></f:verbatim>
				                        							<h:selectOneMenu value="#{actualizarTrayectoriaForm.idCausaMovimiento}"
							    										
							    										rendered="#{actualizarTrayectoriaForm.showCausaMovimiento&&actualizarTrayectoriaForm.adding}"
						                                            	immediate="false">		
						                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  				                                            	
						                                                <f:selectItems value="#{actualizarTrayectoriaForm.listCausaMovimiento}" />
							                                        </h:selectOneMenu>
																	<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
	    	    		                							
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.descripcionMovimiento}"
            	    		        								rendered="#{!actualizarTrayectoriaForm.adding}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="descripcionMovimiento"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="descripcionMovimiento" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																					                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText 
    	                    										rendered="#{actualizarTrayectoriaForm.adding}"
    	                    										value="Manual Cargo" />
        	                									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Codigo Manual Cargo" />
	            	            							</td>
		                        							<td></f:verbatim>
			                        							<h:selectOneMenu value="#{actualizarTrayectoriaForm.selectManualCargo}"
					                        							rendered="#{actualizarTrayectoriaForm.adding}"
					                        							valueChangeListener="#{actualizarTrayectoriaForm.changeManualCargo}"
							    										onchange="this.form.submit()"
						                                            	immediate="false">
						                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  
						                                                <f:selectItems value="#{actualizarTrayectoriaForm.listManualCargo}" />
						                                            </h:selectOneMenu>
																<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="8"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.codManualCargo}"
            	    		        								rendered="#{!actualizarTrayectoriaForm.adding}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="codManualCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                       			</h:inputText>
                        																																<h:message for="codManualCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText 
    	                    										rendered="#{actualizarTrayectoriaForm.adding}"
    	                    										value="Cargo" />
        	                									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Codigo Cargo" />
	            	            							</td>
		                        							<td></f:verbatim>
				                        							<h:selectOneMenu value="#{actualizarTrayectoriaForm.idCargo}"
							    										
							    										rendered="#{actualizarTrayectoriaForm.showCargo&&actualizarTrayectoriaForm.adding}"
						                                            	immediate="false">		
						                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  				                                            	
						                                                <f:selectItems value="#{actualizarTrayectoriaForm.listCargo}" />
							                                        </h:selectOneMenu>
																	<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.codCargo}"
            	    		        								rendered="#{!actualizarTrayectoriaForm.adding}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="codCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Nombre Cargo" />
	            	            							</td>
		                        							<td></f:verbatim>
																							                        	<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.descripcionCargo}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                	    		    								rendered="#{!actualizarTrayectoriaForm.adding}"
                        											id="descripcionCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="descripcionCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                								<h:outputText 
    	                    										rendered="#{actualizarTrayectoriaForm.adding}"
    	                    										value="Clasificacion Personal" />
        	                									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Codigo Relacion Personal" />
	            	            							</td>
		                        							<td></f:verbatim>
			                        							<h:selectOneMenu value="#{actualizarTrayectoriaForm.idClasificacionPersonal}"
					                        							rendered="#{actualizarTrayectoriaForm.adding}"					                        													    										
						                                            	immediate="false">
						                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  
						                                                <f:selectItems value="#{actualizarTrayectoriaForm.listClasificacionPersonal}" />
						                                        </h:selectOneMenu>
																<h:inputText
		    		                    							size="1"
	    	    		                							maxlength="1"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.codRelacion}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                	    		    								rendered="#{!actualizarTrayectoriaForm.adding}"
                        											id="codRelacion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codRelacion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
    	                    									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Descripcion Relacion Personal" />
        	                									
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="20"
	    	    		                							maxlength="20"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.descRelacion}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                	    		    								rendered="#{!actualizarTrayectoriaForm.adding}"
                        											id="descRelacion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="descRelacion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
    	                    									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="C�digo Categoria Personal" />
        	                									
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="1"
	    	    		                							maxlength="1"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.codCategoria}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                	    		    								rendered="#{!actualizarTrayectoriaForm.adding}"
                        											id="codCategoria"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codCategoria" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
    	                    									<h:outputText 
    	                    										rendered="#{!actualizarTrayectoriaForm.adding}"
    	                    										value="Categor�a Personal" />
        	                									
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="20"
	    	    		                							maxlength="20"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.descCategoria}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                	    		    								rendered="#{!actualizarTrayectoriaForm.adding}"
                        											id="descCategoria"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="descCategoria" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																										                    	    						
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Nomina
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.codigoNomina}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="codigoNomina"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codigoNomina" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																																																																		                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Sueldo Basico
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.sueldoBasico}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="sueldoBasico"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"			                        											                        											                        																									
			                        							    style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:inputText>
                        										<h:message for="sueldoBasico" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Compensacion
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.compensacion}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="compensacion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"			               
			                        								style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:inputText>
                        											<h:message for="compensacion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Prima Jerarquia
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.primaJerarquia}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="primaJerarquia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"			               
			                        								style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:inputText>
                        											<h:message for="primaJerarquia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									Prima Servicio
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.primaServicio}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="primaServicio"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"			               
			                        								style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:inputText>
                        											<h:message for="primaServicio" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>		
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Otras Primas del Cargo
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.primasCargo}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="primasCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"			               
			                        								style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:inputText>
                        											<h:message for="primasCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>		
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Otras Primas del Trabajador
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.primasTrabajador}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="primasTrabajador"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"			               
			                        								style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:inputText>
                        											<h:message for="primasTrabajador" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>		
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Ajustes Sueldo
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.ajusteSueldo}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="ajusteSueldo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"			               
			                        								style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:inputText>
                        											<h:message for="ajusteSueldo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>			  
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Otros Pagos
	            	            							</td>
		                        							<td></f:verbatim>
																<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{actualizarTrayectoriaForm.trayectoria.otrosPagos}"
                	    		    								readonly="#{!actualizarTrayectoriaForm.editing}"
                        											id="otrosPagos"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"			               
			                        								style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        		</h:inputText>
                        											<h:message for="otrosPagos" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>			  	                    	    						
														<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    							                        		<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="3"
    	        		                							id="observaciones"
                    		        								                        		    								value="#{actualizarTrayectoriaForm.trayectoria.observaciones}"
                            										readonly="#{!actualizarTrayectoriaForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										onkeypress="return keyEnterCheck(event, this)"
                            										required="false" />
                        																																<h:message for="observaciones" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	
												<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsActualizarTrayectoria"
            										rendered="#{actualizarTrayectoriaForm.showData}">
		                        					<f:verbatim><table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
   	                        	                    					image="/images/modify.gif"
       	                        	                					action="#{actualizarTrayectoriaForm.edit}"
       	                        	                					onclick="javascript:return clickMade()"
           	                        	            					rendered="#{!actualizarTrayectoriaForm.editing&&!actualizarTrayectoriaForm.deleting&&actualizarTrayectoriaForm.login.modificar}" />
               	                        	        				<h:commandButton value="Eliminar"
                   	                        	    					image="/images/delete.gif"
                       	                        						action="#{actualizarTrayectoriaForm.delete}"
                       	                        						onclick="javascript:return clickMade()"
                           	                    						rendered="#{!actualizarTrayectoriaForm.editing&&!actualizarTrayectoriaForm.deleting&&actualizarTrayectoriaForm.login.eliminar}" />
                               	                					<h:outputText
                                   	            						value="�Seguro que desea eliminar?"
                                       	        						rendered="#{actualizarTrayectoriaForm.deleting}" />
	                                                				<h:commandButton value="Si" 
   		                                            					image="/images/yes.gif"
       		                                        					action="#{actualizarTrayectoriaForm.deleteOk}"
       		                                        					onclick="javascript:return clickMade()"
           		                                    					rendered="#{actualizarTrayectoriaForm.deleting}" />
                	                                				<h:commandButton value="Guardar" 
   	                	                            					image="/images/save.gif"
       	                	                        					action="#{actualizarTrayectoriaForm.save}"
       	                	                        					onclick="javascript:return clickMade()"
           	                	            	        				rendered="#{actualizarTrayectoriaForm.editing}" />
           	                	            	        			<h:commandButton value="Cancelar" 
				                                    					image="/images/cancel.gif"
                               											action="#{actualizarTrayectoriaForm.abortUpdate}"
                               											immediate="true"
                               											onclick="javascript:return clickMade()"
                       	        										/>	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>