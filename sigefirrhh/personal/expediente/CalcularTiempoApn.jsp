<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{calcularTiempoApnForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div 				 
	  				
			
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formCalcularTiempoApn">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Calcular el Tiempo de Servicio en APN
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/jubilacion/CalcularTiempoApn.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success"  />
										</td>
									</tr>
									
									<f:subview 
										id="searchCalcularTiempoApn"
										rendered="#{!calcularTiempoApnForm.showData}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{calcularTiempoApnForm.findPersonalCedula}"
																onkeypress="javascript:return keyIntegerCheck(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{calcularTiempoApnForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{calcularTiempoApnForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{calcularTiempoApnForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{calcularTiempoApnForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{calcularTiempoApnForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{calcularTiempoApnForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{calcularTiempoApnForm.showResultPersonal&&
									!calcularTiempoApnForm.showData}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <x:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{calcularTiempoApnForm.resultPersonal}"
    	                                            preserveDataModel="false"    	                                           
        	                                        rows="10"
	                                                width="100%">
		                                                <h:column>
		                                					<h:commandLink value="#{resultPersonal}"																	                                					
	    	                            						action="#{calcularTiempoApnForm.selectPersonal}"
	        	                        						styleClass="listitem">
	            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
	                	                					</h:commandLink>
	                    	            				</h:column>
	                    	            				 
                        	                    </x:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{calcularTiempoApnForm.selectedPersonal&&
									calcularTiempoApnForm.showData}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{calcularTiempoApnForm.personal}   <-- clic aqui"
    	                            						action="#{calcularTiempoApnForm.findAntecedentesApn}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{calcularTiempoApnForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>            												

                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{calcularTiempoApnForm.abort}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                                                    	   <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								
							</h:form>
							<f:subview 
								id="viewDataCalcularTiempoApn"
								rendered="#{calcularTiempoApnForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
												<td>
													Antecedentes
												</td>

												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="antecedentesApnform">
	    	        							
	            									<f:subview 
	            										id="antecedentes"
	            										rendered="#{calcularTiempoApnForm.showResultAntecedentes}">			                        					
			                        					<x:dataTable id="result"
		                	                                styleClass="datatable"
		                    	                            headerClass="standardTable_Header"
		                        	                        footerClass="standardTable_Header"
		                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
		                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
		                                    	            var="result"
		                                					value="#{calcularTiempoApnForm.resultAntecedentes}"
		                                            	    preserveDataModel="false"
		                                            	    binding="#{calcularTiempoApnForm.tableAntecedentes}"
		                                                	rows="10"
			                                                width="100%">
		    	                            				<h:column>
		    	                            						<f:facet name="header">
																		<h:outputText value="Organismo" />
																	</f:facet>
		                                						<h:outputText value="#{result.organismo}">		    		                            						
		            	                    					</h:outputText>
		                	                				</h:column>
		                	                				<h:column>
				                	                				<f:facet name="header">
																		<h:outputText value="Desde" />
																	</f:facet>
		                                						<h:outputText value="#{result.fechaDesde}">		    		                            						
		            	                    					</h:outputText>
		                	                				</h:column>
		                	                				<h:column>
			                	                				<f:facet name="header">
																		<h:outputText value="Hasta" />
																	</f:facet>
		                                						<h:outputText value="#{result.fechaHasta}">		    		                            						
		            	                    					</h:outputText>
		                	                				</h:column>
		                	                				 <h:column>                                                	
			                                    					<h:commandButton 
			                                    						action="#{calcularTiempoApnForm.delete}"			                                    						
			                                    						image="/images/delete.gif">
			                                    						
			                                    					</h:commandButton>
			                                    				
			                                				</h:column>
		                    	                        </x:dataTable>
		    										</f:subview>
											
        	                            <f:verbatim></td>
                        			</tr>
	                        			<tr>
										<td>
											<table width="100%" class="bandtable">
											<tr>
											<td>
	                        					A�os
	                        				<td>
	                        				<td style="text-align: right"></f:verbatim>            												
	
	                                        	<h:outputText value="#{calcularTiempoApnForm.totalAnios}" 
	                            	       		/>	
	                                         <f:verbatim>
											</td>
	                        			</tr>
	                        			<tr>
		                        			<td>
	                        					Meses
	                        				<td>
	                        				<td style="text-align: right"></f:verbatim>            												
	
	                                        	<h:outputText value="#{calcularTiempoApnForm.totalMeses}" 
	                            	       		/>	
	                                         <f:verbatim>
											</td>
	                        			</tr>
	                        			<tr>
		                        			<td>
	                        					Dias
	                        				<td>
	                        				<td style="text-align: right"></f:verbatim>            												
	
	                                        	<h:outputText value="#{calcularTiempoApnForm.totalDias}" 
	                            	       		/>	
	                                         <f:verbatim>
											</td>
	                        			</tr>
	                        			<tr>
	                        				<td></td>
	                        				<td style="text-align: right"></f:verbatim>            												
	                                        	<h:commandButton value="Cancelar" 
				                                	image="/images/calculate.gif"
		                                    		action="#{calcularTiempoApnForm.calcular}"
	    	                                		immediate="true"
	        	                            		onclick="javascript:return clickMade()"
	                            	       		/>	
	                                         <f:verbatim>
											</td>
	                        			</tr>
	                        			<tr>
	                        				<td></td>
	                        				<td style="text-align: right"></f:verbatim>            												
	                                        	<h:commandButton value="Guardar" 
				                                	image="/images/save.gif"
		                                    		action="#{calcularTiempoApnForm.save}"
	    	                                		immediate="true"
	        	                            		onclick="javascript:return clickMade()"
	                            	       		/>	
	                                         <f:verbatim>
											</td>
	                        			</tr>
	                        			</table>
	                        			</td>
	                        			</tr>
	                        			
                        		</table></f:verbatim>
                        		</h:form>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>