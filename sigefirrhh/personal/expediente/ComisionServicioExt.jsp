<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">

<f:view>
	<x:saveState value="#{comisionServicioExtForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formComisionServicioExt">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Comisiones de Servicio - Personal Externo
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/expediente/ComisionServicioExt.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									<f:subview 
										id="searchComisionServicioExt"
										rendered="#{!comisionServicioExtForm.showData&&
										!comisionServicioExtForm.showAdd}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{comisionServicioExtForm.findPersonalCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{comisionServicioExtForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{comisionServicioExtForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{comisionServicioExtForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{comisionServicioExtForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{comisionServicioExtForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{comisionServicioExtForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()"
																 />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{comisionServicioExtForm.showResultPersonal&&
									!comisionServicioExtForm.showData&&
									!comisionServicioExtForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <h:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{comisionServicioExtForm.resultPersonal}"
    	                                            
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{comisionServicioExtForm.findComisionServicioExtByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </h:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{comisionServicioExtForm.selectedPersonal&&
									!comisionServicioExtForm.showData&&
									!comisionServicioExtForm.editing&&
									!comisionServicioExtForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>
            									<h:commandLink value="#{comisionServicioExtForm.personal}"
    	                            						action="#{comisionServicioExtForm.findComisionServicioExtByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{comisionServicioExtForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{comisionServicioExtForm.add}"
            													onclick="javascript:return clickMade()"
            													rendered="#{!comisionServicioExtForm.editing&&!comisionServicioExtForm.deleting&&comisionServicioExtForm.login.agregar}" />
            												<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{comisionServicioExtForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							/>		
                                        	    			<f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultComisionServicioExt"
									rendered="#{comisionServicioExtForm.showResult&&
									!comisionServicioExtForm.showData&&
									!comisionServicioExtForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <h:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{comisionServicioExtForm.result}"
                                            	    
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{comisionServicioExtForm.selectComisionServicioExt}"
	                                						styleClass="listitem">
    		                            						<f:param name="idComisionServicioExt" value="#{result.idComisionServicioExt}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </h:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataComisionServicioExt"
								rendered="#{comisionServicioExtForm.showData||comisionServicioExtForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!comisionServicioExtForm.editing&&!comisionServicioExtForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{comisionServicioExtForm.editing&&!comisionServicioExtForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{comisionServicioExtForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{comisionServicioExtForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="comisionServicioExtForm">
	                                            <h:inputHidden id="scrollx" value="#{comisionServicioExtForm.scrollx}" />
    	                                        <h:inputHidden id="scrolly" value="#{comisionServicioExtForm.scrolly}" />
	    	        							<f:subview 
    	    	    								id="dataComisionServicioExt"
        	    									rendered="#{comisionServicioExtForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{comisionServicioExtForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!comisionServicioExtForm.editing&&!comisionServicioExtForm.deleting&&comisionServicioExtForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{comisionServicioExtForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!comisionServicioExtForm.editing&&!comisionServicioExtForm.deleting&&comisionServicioExtForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{comisionServicioExtForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{comisionServicioExtForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{comisionServicioExtForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{comisionServicioExtForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{comisionServicioExtForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
						                        								action="#{comisionServicioExtForm.abortUpdate}"
						                        								immediate="true"
						                        								onclick="javascript:return clickMade()"
						                	        							/>		
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{comisionServicioExtForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
																				                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Institucion/Organismo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{comisionServicioExtForm.comisionServicioExt.nombreInstitucion}"
                	    		    								                    	    										readonly="#{!comisionServicioExtForm.editing}"
                        											id="InstitucionOrganismo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Inicio
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaInicio"
                		        									value="#{comisionServicioExtForm.comisionServicioExt.fechaInicio}"
                    		    									readonly="#{!comisionServicioExtForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
    	                    										        	                										required="true">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Fin
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaFin"
                		        									value="#{comisionServicioExtForm.comisionServicioExt.fechaFin}"
                    		    									readonly="#{!comisionServicioExtForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
									        	            	    							<f:verbatim></td>
	        	            	    					</tr>
														</f:verbatim>
	        	            	    					<f:verbatim>
														<tr>
    	                    								<td>Tiempo</td>
		                        							<td>
		                        							<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                        								<tr>
		                        									<td>
		                        									</f:verbatim>
																		<h:inputText
																			size="25"
																			maxlength="25"
																			id="DiasExperiencia"
																			value="#{comisionServicioExtForm.diferencia}"
																			readonly="true"
																			onchange="javascript:this.value=this.value.toUpperCase()"
																			onkeypress="return keyEnterCheck(event, this)"																	
                    		    										</h:inputText>
																		<f:verbatim><span class="required"></span></f:verbatim>
																	<f:verbatim>
																	</td>
														        	<td align="left">
																	</f:verbatim>				
																		<h:commandButton value="Calcular" 
						                                					image="/images/calculate.gif"
						                                					action="this.form.submit()"
						                                					onclick="javascript:return clickMade()"/>
						                        	        		<f:verbatim>
																	</td>
																</tr>
															</table>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    							                        		<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="4"
    	        		                							id="Observaciones"
                    		        								                        		    								value="#{comisionServicioExtForm.comisionServicioExt.observaciones}"
                            										readonly="#{!comisionServicioExtForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										onkeypress="return keyEnterCheck(event, this)"
                            										required="false" />
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Sede Diplom�tica
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{comisionServicioExtForm.selectSedeDiplomatica}"
        		                                                	disabled="#{!comisionServicioExtForm.editing}"
            		                                            	immediate="false"
                		            								                    		        								                        		                                	required="false"
    																title="Sede Diplom�tica">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{comisionServicioExtForm.colSedeDiplomatica}" />
								                                    	                </h:selectOneMenu>									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																						<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsComisionServicioExt"
            										rendered="#{comisionServicioExtForm.showData}">
		                        					<f:verbatim><table>
    		                    						<tr>
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{comisionServicioExtForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!comisionServicioExtForm.editing&&!comisionServicioExtForm.deleting&&comisionServicioExtForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{comisionServicioExtForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!comisionServicioExtForm.editing&&!comisionServicioExtForm.deleting&&comisionServicioExtForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{comisionServicioExtForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{comisionServicioExtForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{comisionServicioExtForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{comisionServicioExtForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{comisionServicioExtForm.editing}" />
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{comisionServicioExtForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							rendered="#{comisionServicioExtForm.editing}" />		
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>