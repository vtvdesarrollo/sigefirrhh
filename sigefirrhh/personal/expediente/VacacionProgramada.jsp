<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{vacacionProgramadaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />
				<%
	  			}else { %>
	  				 
	  				
				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formVacacionProgramada">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Vacaciones Programadas
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/expediente/VacacionProgramada.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success"  />
										</td>
									</tr>
									
									<f:subview 
										id="searchVacacionProgramada"
										rendered="#{!vacacionProgramadaForm.showData&&
										!vacacionProgramadaForm.showAdd&&
										!vacacionProgramadaForm.login.servicioPersonal}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{vacacionProgramadaForm.findPersonalCedula}"
																onkeypress="javascript:return keyIntegerCheck(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{vacacionProgramadaForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{vacacionProgramadaForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{vacacionProgramadaForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{vacacionProgramadaForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{vacacionProgramadaForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{vacacionProgramadaForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{vacacionProgramadaForm.showResultPersonal&&
									!vacacionProgramadaForm.showData&&
									!vacacionProgramadaForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <x:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{vacacionProgramadaForm.resultPersonal}"
    	                                            preserveDataModel="false"
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{vacacionProgramadaForm.findVacacionProgramadaByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </x:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{vacacionProgramadaForm.selectedPersonal&&
									!vacacionProgramadaForm.showData&&
									!vacacionProgramadaForm.editing&&
									!vacacionProgramadaForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{vacacionProgramadaForm.personal}   <-- clic aqui"
    	                            						action="#{vacacionProgramadaForm.findVacacionProgramadaByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{vacacionProgramadaForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{vacacionProgramadaForm.add}"
            													rendered="#{!vacacionProgramadaForm.editing&&!vacacionProgramadaForm.deleting&&vacacionProgramadaForm.login.agregar}" />

                                        	    			<% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{vacacionProgramadaForm.abort}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
                                                    	    <%}%>
                                                    	   <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultVacacionProgramada"
									rendered="#{vacacionProgramadaForm.showResult&&
									!vacacionProgramadaForm.showData&&
									!vacacionProgramadaForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <x:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{vacacionProgramadaForm.result}"
                                            	    preserveDataModel="false"
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{vacacionProgramadaForm.selectVacacionProgramada}"
	                                						styleClass="listitem">
    		                            						<f:param name="idVacacionProgramada" value="#{result.idVacacionProgramada}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </x:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataVacacionProgramada"
								rendered="#{vacacionProgramadaForm.showData||vacacionProgramadaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></f:verbatim>
														<h:outputText value="Consultando"
															rendered="#{!vacacionProgramadaForm.editing&&!vacacionProgramadaForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{vacacionProgramadaForm.editing&&!vacacionProgramadaForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{vacacionProgramadaForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{vacacionProgramadaForm.adding}" />
													<f:verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="vacacionProgramadaForm">
	    	        							<f:subview 
    	    	    								id="dataVacacionProgramada"
        	    									rendered="#{vacacionProgramadaForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{vacacionProgramadaForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!vacacionProgramadaForm.editing&&!vacacionProgramadaForm.deleting&&vacacionProgramadaForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{vacacionProgramadaForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!vacacionProgramadaForm.editing&&!vacacionProgramadaForm.deleting&&vacacionProgramadaForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{vacacionProgramadaForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{vacacionProgramadaForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{vacacionProgramadaForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{vacacionProgramadaForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{vacacionProgramadaForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{vacacionProgramadaForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{vacacionProgramadaForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
																													                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Tipo Personal
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{vacacionProgramadaForm.selectTipoPersonal}"
        		                                                	disabled="#{!vacacionProgramadaForm.editing}"
            		                                            	immediate="false"
				                        							id="tipoPersonal"
                		            								                    		        								                        		                                	required="false"
    																title="Tipo Personal">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{vacacionProgramadaForm.colTipoPersonal}" />
								                                    	                </h:selectOneMenu>																									<h:message for="tipoPersonal" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									A�o
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="4"
	    	    		                							maxlength="4"
            	    		        								value="#{vacacionProgramadaForm.vacacionProgramada.anio}"
                	    		    								                    	    										readonly="#{!vacacionProgramadaForm.editing}"
                        											id="anio"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        			                        								<f:verbatim><span class="required"> *</span></f:verbatim>
                        																																<h:message for="anio" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Inicio
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaInicio"
                		        									value="#{vacacionProgramadaForm.vacacionProgramada.fechaInicio}"
                    		    									readonly="#{!vacacionProgramadaForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="true">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
																									<h:message for="fechaInicio" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Fin
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaFin"
                		        									value="#{vacacionProgramadaForm.vacacionProgramada.fechaFin}"
                    		    									readonly="#{!vacacionProgramadaForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="true">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
																									<h:message for="fechaFin" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Reintegro
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaReintegro"
                		        									value="#{vacacionProgramadaForm.vacacionProgramada.fechaReintegro}"
                    		    									readonly="#{!vacacionProgramadaForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="fechaReintegro" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									A partir de semana
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{vacacionProgramadaForm.vacacionProgramada.semanaAnio}"
                	    		    								                    	    										readonly="#{!vacacionProgramadaForm.editing}"
                        											id="semanaAnio"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="semanaAnio" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									D�as Disfrute
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="3"
	    	    		                							maxlength="3"
            	    		        								value="#{vacacionProgramadaForm.vacacionProgramada.diasDisfrute}"
                	    		    								                    	    										readonly="#{!vacacionProgramadaForm.editing}"
                        											id="diasDisfrute"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="diasDisfrute" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    							                        		<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="4"
    	        		                							id="observaciones"
                    		        								                        		    								value="#{vacacionProgramadaForm.vacacionProgramada.observaciones}"
                            										readonly="#{!vacacionProgramadaForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										onkeypress="return keyEnterCheck(event, this)"
                            										required="false" />
                        																																<h:message for="observaciones" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																				<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsVacacionProgramada"
            										rendered="#{vacacionProgramadaForm.showData}">
		                        					<f:verbatim><table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{vacacionProgramadaForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!vacacionProgramadaForm.editing&&!vacacionProgramadaForm.deleting&&vacacionProgramadaForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{vacacionProgramadaForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!vacacionProgramadaForm.editing&&!vacacionProgramadaForm.deleting&&vacacionProgramadaForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{vacacionProgramadaForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{vacacionProgramadaForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{vacacionProgramadaForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{vacacionProgramadaForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{vacacionProgramadaForm.editing}" />
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{vacacionProgramadaForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>