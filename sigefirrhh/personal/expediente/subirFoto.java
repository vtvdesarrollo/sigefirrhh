package sigefirrhh.common;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class subirFoto {
	static Logger log = Logger.getLogger(subirFoto.class.getName());

	private static final int heightDefault = 140;
	private static final int widthDefault = 140;
	private UploadedFile myFile;
	private String myResult;

	public UploadedFile getMyFile() {
		return myFile;
	}

	public void setMyFile(UploadedFile myFile) {
		this.myFile = myFile;
	}

	public String getMyResult() {
		return myResult;
	}

	public void setMyResult(String myResult) {
		this.myResult = myResult;
	}

	public String processMyFile() {
		try {
			String archivoTemporal = "/tmp/" + myFile.getName();
			String archivoTemporalResizedB64 = "/tmp/resizedB64" + myFile.getName();
			String fotoresized = "/tmp/resized" + myFile.getName();
			InputStream in = new BufferedInputStream(myFile.getInputStream());
			BufferedOutputStream bos = new BufferedOutputStream(
					new FileOutputStream(archivoTemporal));
			int BUFFER_SIZE = 1024;
			byte[] buffer = new byte[1024];
			int n = in.read(buffer, 0, BUFFER_SIZE);
			while (n >= 0) {
				bos.write(buffer, 0, n);
				n = in.read(buffer, 0, BUFFER_SIZE);
			}
			in.close();
			bos.close();
			resizeImageWithNoBox(archivoTemporal, myFile.getName(), ".PNG",
					fotoresized, heightDefault, widthDefault, new Color(255,
							2, 255));
			InputStream in2 = new BufferedInputStream(new FileInputStream(fotoresized));
			OutputStream output = new Base64OutputStream(new FileOutputStream(archivoTemporalResizedB64));
			buffer = new byte[1024];
			n=0;
			n = in2.read(buffer, 0, BUFFER_SIZE);
			while (n >= 0) {
				output.write(buffer, 0, n);
				n = in2.read(buffer, 0, BUFFER_SIZE);
			}
			in2.close();
			output.close();
			FileInputStream fis = null;
			BufferedInputStream bis = null;
			DataInputStream dis = null;
			StringBuffer fotoB64 = new StringBuffer();
			fis = new FileInputStream(archivoTemporalResizedB64);
			//Here BufferedInputStream is added for fast reading.
			bis = new BufferedInputStream(fis);
			dis = new DataInputStream(bis);
			// dis.available() returns 0 if the file does not have more lines.
			while (dis.available() != 0) {
				// this statement reads the line from the file and print it to
				// the console.
				String leido = dis.readLine();
				fotoB64.append(leido);
			}
			myResult = "data:image/png;base64," + fotoB64.toString();
			File f=new File(archivoTemporal);
			f.delete();
			f=new File(archivoTemporalResizedB64);
			f.delete();
			f=new File(fotoresized);
			f.delete();
			return "success";
		} catch (Exception x) {
			log					.error(
							"Ha ocurrido un error subiendo la foto del trabajador",
							x);
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_FATAL, x.getClass().getName(),
					"Ha ocurrido un error subiendo el archivo");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
	}

	private void resizeImageWithNoBox(String imagePath, String imageName,
			String fileExtension, String fileOutPutName, int h, int w,
			Color bgColor) {
		int height = 150;
		int width = 150;
		try {
			h = h > 0 ? h : height;
			w = w > 0 ? w : width;
			// load image from INFILE
			String fullImagePath = imagePath;
			Image image = Toolkit.getDefaultToolkit()
					.createImage(fullImagePath);
			log.debug("Thumbnail=" + imagePath);
			MediaTracker mediaTracker = new MediaTracker(new Container());
			mediaTracker.addImage(image, 0);
			mediaTracker.waitForID(0);
			// delete the old thumbnail
			// new File(imagePath + fileOutPutName + ".png").delete();
			// determine thumbnail size from WIDTH and HEIGHT
			// draw original image to thumbnail image object and
			// scale it to the new size on-the-fly
			BufferedImage bgImage = new BufferedImage(w, h,
					BufferedImage.TYPE_INT_RGB);
			Graphics2D g = bgImage.createGraphics();
			g.setColor(bgColor);
			g.fillRect(0, 0, w, h);

			BufferedImage thumbImage = new BufferedImage(w, h,
					BufferedImage.TYPE_INT_RGB);

			Graphics2D graphics2D = thumbImage.createGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_QUALITY);
			graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			graphics2D.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
					RenderingHints.VALUE_COLOR_RENDER_QUALITY);
			graphics2D.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,
					RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
			graphics2D.drawImage(image, 0, 0, w, h, bgColor, null);
			graphics2D.dispose();

			// compute offsets to center image in its space
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			g.setRenderingHint(RenderingHints.KEY_RENDERING,
					RenderingHints.VALUE_RENDER_QUALITY);
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
					RenderingHints.VALUE_COLOR_RENDER_QUALITY);
			g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,
					RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
			g.drawImage(thumbImage, null, 0, 0);
			g.dispose();

			// save thumbnail image to OUTFILE
			BufferedOutputStream out = new BufferedOutputStream(
					new FileOutputStream(fileOutPutName));
			log.debug("Output image=" + fileOutPutName);
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
			JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(bgImage);
			int quality = Integer.parseInt("100");
			quality = Math.max(0, Math.min(quality, 100));
			param.setQuality((float) quality / 100.0f, false);
			encoder.setJPEGEncodeParam(param);
			encoder.encode(bgImage);
			out.close();
			log.debug("Done.");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

}
