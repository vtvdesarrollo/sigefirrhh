<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">

<f:view>
	<x:saveState value="#{experienciaNoEstForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formExperienciaNoEst">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Antecedentes de Servicio no sujeto a LEFP
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/expediente/ExperienciaNoEst.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									<f:subview 
										id="searchExperienciaNoEst"
										rendered="#{!experienciaNoEstForm.showData&&
										!experienciaNoEstForm.showAdd}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{experienciaNoEstForm.findPersonalCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
        														onfocus="return deleteZero(event, this)"
        														onblur="javascript:fieldEmpty(this)"
        														onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{experienciaNoEstForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{experienciaNoEstForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{experienciaNoEstForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{experienciaNoEstForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{experienciaNoEstForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{experienciaNoEstForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{experienciaNoEstForm.showResultPersonal&&
									!experienciaNoEstForm.showData&&
									!experienciaNoEstForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <h:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{experienciaNoEstForm.resultPersonal}"
    	                                            
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{experienciaNoEstForm.findExperienciaNoEstByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </h:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{experienciaNoEstForm.selectedPersonal&&
									!experienciaNoEstForm.showData&&
									!experienciaNoEstForm.editing&&
									!experienciaNoEstForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>
            									<h:commandLink value="#{experienciaNoEstForm.personal}  <-- clic aqui"
    	                            						action="#{experienciaNoEstForm.findExperienciaNoEstByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{experienciaNoEstForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{experienciaNoEstForm.add}"
            													onclick="javascript:return clickMade()"
            													rendered="#{!experienciaNoEstForm.editing&&!experienciaNoEstForm.deleting&&experienciaNoEstForm.login.agregar}" />
            												<h:commandButton value="Cancelar" 
		                                    					image="/images/cancel.gif"
		                        								action="#{experienciaNoEstForm.abort}"
		                        								immediate="true"
		                        								onclick="javascript:return clickMade()"
		                	        							 />
                                        	    			<f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultExperienciaNoEst"
									rendered="#{experienciaNoEstForm.showResult&&
									!experienciaNoEstForm.showData&&
									!experienciaNoEstForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <h:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{experienciaNoEstForm.result}"
                                            	    
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{experienciaNoEstForm.selectExperienciaNoEst}"
	                                						styleClass="listitem">
    		                            						<f:param name="idExperienciaNoEst" value="#{result.idExperienciaNoEst}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </h:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataExperienciaNoEst"
								rendered="#{experienciaNoEstForm.showData||experienciaNoEstForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!experienciaNoEstForm.editing&&!experienciaNoEstForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{experienciaNoEstForm.editing&&!experienciaNoEstForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{experienciaNoEstForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{experienciaNoEstForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="experienciaNoEstForm">
	                                            <h:inputHidden id="scrollx" value="#{experienciaNoEstForm.scrollx}" />
    	                                        <h:inputHidden id="scrolly" value="#{experienciaNoEstForm.scrolly}" />
	    	        							<f:subview 
    	    	    								id="dataExperienciaNoEst"
        	    									rendered="#{experienciaNoEstForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{experienciaNoEstForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!experienciaNoEstForm.editing&&!experienciaNoEstForm.deleting&&experienciaNoEstForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{experienciaNoEstForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!experienciaNoEstForm.editing&&!experienciaNoEstForm.deleting&&experienciaNoEstForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{experienciaNoEstForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{experienciaNoEstForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{experienciaNoEstForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{experienciaNoEstForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{experienciaNoEstForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
						                        								action="#{experienciaNoEstForm.abortUpdate}"
						                        								immediate="true"
						                        								onclick="javascript:return clickMade()"
						                	        							/>
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{experienciaNoEstForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
																				                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Organismo/Instituci�n
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{experienciaNoEstForm.experienciaNoEst.nombreInstitucion}"
                	    		    								                    	    										readonly="#{!experienciaNoEstForm.editing}"
                        											id="OrganismoInstitucion"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	 >
								                        									</h:inputText>
								                        									<f:verbatim><span class="required"> *</span></f:verbatim>
                        									<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
													<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Ingreso
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaIngreso"
                		        									value="#{experienciaNoEstForm.experienciaNoEst.fechaIngreso}"
                    		    									readonly="#{!experienciaNoEstForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
    	                    										        	                										required="true">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Egreso
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaEgreso"
                		        									value="#{experienciaNoEstForm.experienciaNoEst.fechaEgreso}"
                    		    									readonly="#{!experienciaNoEstForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
    	                    										        	                										required="true">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim>
														
														
														<tr>
    	                    								<td>Tiempo Servicio</td>
		                        							<td>
		                        							<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                        								<tr>
		                        									<td>
		                        									</f:verbatim>
																		<h:inputText
																			size="25"
																			maxlength="25"
																			id="DiasExperiencia"
																			value="#{experienciaNoEstForm.diferencia}"
																			readonly="true"
																			onchange="javascript:this.value=this.value.toUpperCase()"
																			onkeypress="return keyEnterCheck(event, this)">																	
																		</h:inputText>
																		<f:verbatim><span class="required"></span></f:verbatim>
											        	            <f:verbatim>
																	</td>
														        	<td align="left">
																	</f:verbatim>				
																		<h:commandButton value="Calcular" 
						                                					image="/images/calculate.gif"
						                                					action="this.form.submit()"
						                                					onclick="javascript:return clickMade()"/>
						                        	        		<f:verbatim>
																	</td>
																</tr>
															</table>
															</td>
	        	            	    					</tr>
														</f:verbatim>
	        	            	    					<f:verbatim>
														<tr>
    	                    								<td>
        	                									Cargo/Oficio al ingreso
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="50"
	    	    		                							maxlength="50"
            	    		        								value="#{experienciaNoEstForm.experienciaNoEst.cargoIngreso}"
                	    		    								                    	    										readonly="#{!experienciaNoEstForm.editing}"
                        											id="CargoOficioalingreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Cargo/Oficio al egreso
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="50"
	    	    		                							maxlength="50"
            	    		        								value="#{experienciaNoEstForm.experienciaNoEst.cargoEgreso}"
                	    		    								                    	    										readonly="#{!experienciaNoEstForm.editing}"
                        											id="CargoOficioalegreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Nombre Jefe
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="50"
	    	    		                							maxlength="50"
            	    		        								value="#{experienciaNoEstForm.experienciaNoEst.jefe}"
                	    		    								                    	    										readonly="#{!experienciaNoEstForm.editing}"
                        											id="NombreJefe"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Tel�fono
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="15"
	    	    		                							maxlength="15"
            	    		        								value="#{experienciaNoEstForm.experienciaNoEst.telefono}"
                	    		    								                    	    										readonly="#{!experienciaNoEstForm.editing}"
                        											id="Telefono"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Causa Retiro
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="20"
	    	    		                							maxlength="20"
            	    		        								value="#{experienciaNoEstForm.experienciaNoEst.causaRetiro}"
                	    		    								                    	    										readonly="#{!experienciaNoEstForm.editing}"
                        											id="CausaRetiro"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Ultimo Sueldo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{experienciaNoEstForm.experienciaNoEst.ultimoSueldo}"
                	    		    								                    	    										readonly="#{!experienciaNoEstForm.editing}"
                        											id="UltimoSueldo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
																	 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    							                        		<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="3"
    	        		                							id="Observaciones"
                    		        								                        		    								value="#{experienciaNoEstForm.experienciaNoEst.observaciones}"
                            										readonly="#{!experienciaNoEstForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										onkeypress="return keyEnterCheck(event, this)"
                            										required="false" />
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																						<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsExperienciaNoEst"
            										rendered="#{experienciaNoEstForm.showData}">
		                        					<f:verbatim><table>
    		                    						<tr>
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{experienciaNoEstForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!experienciaNoEstForm.editing&&!experienciaNoEstForm.deleting&&experienciaNoEstForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{experienciaNoEstForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!experienciaNoEstForm.editing&&!experienciaNoEstForm.deleting&&experienciaNoEstForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{experienciaNoEstForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{experienciaNoEstForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{experienciaNoEstForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{experienciaNoEstForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{experienciaNoEstForm.editing}" />	
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{experienciaNoEstForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>