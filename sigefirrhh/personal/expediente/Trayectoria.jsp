<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">

<f:view>
	<x:saveState value="#{trayectoriaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />
	  				
				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formTrayectoria">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Trayectoria en APN
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/expediente/Trayectoria.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
										<h:messages errorClass="error" styleClass="success"  />
										</td>
									</tr>
									
									<f:subview 
										id="searchTrayectoria"
										rendered="#{!trayectoriaForm.showData&&
										!trayectoriaForm.showAdd&&
										!trayectoriaForm.login.servicioPersonal}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{trayectoriaForm.findPersonalCedula}"
																onkeypress="javascript:return keyIntegerCheck(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onfocus="return deleteZero(event, this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{trayectoriaForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{trayectoriaForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{trayectoriaForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{trayectoriaForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{trayectoriaForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{trayectoriaForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
														<td>
															Movimientos
														</td>
														<td></f:verbatim>
															<h:selectOneMenu value="#{trayectoriaForm.findEstatus}"
				                                            	immediate="false">
				                                            	<f:selectItem itemLabel="Aprobados" itemValue="A" />  
				                                                <f:selectItem itemLabel="En Tramite" itemValue="T" />
				                                                <f:selectItem itemLabel="Devueltos" itemValue="D" />
				                                            </h:selectOneMenu>
														</td><f:verbatim>
													
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{trayectoriaForm.showResultPersonal&&
									!trayectoriaForm.showData&&
									!trayectoriaForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <x:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{trayectoriaForm.resultPersonal}"
    	                                            preserveDataModel="false"
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{trayectoriaForm.findTrayectoriaByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </x:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{trayectoriaForm.selectedPersonal&&
									!trayectoriaForm.showData&&
									!trayectoriaForm.editing&&
									!trayectoriaForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{trayectoriaForm.personal}   <-- clic aqui"
    	                            						action="#{trayectoriaForm.findTrayectoriaByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{trayectoriaForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultTrayectoria"
									rendered="#{trayectoriaForm.showResult&&
									!trayectoriaForm.showData&&
									!trayectoriaForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <x:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{trayectoriaForm.result}"
                                            	    preserveDataModel="false"
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{trayectoriaForm.selectTrayectoria}"
	                                						styleClass="listitem">
    		                            						<f:param name="idTrayectoria" value="#{result.idTrayectoria}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </x:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataTrayectoria"
								rendered="#{trayectoriaForm.showData||trayectoriaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!trayectoriaForm.editing&&!trayectoriaForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{trayectoriaForm.editing&&!trayectoriaForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{trayectoriaForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{trayectoriaForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="trayectoriaForm">
	    	        							<f:subview 
    	    	    								id="dataTrayectoria"
        	    									rendered="#{trayectoriaForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{trayectoriaForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{trayectoriaForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{trayectoriaForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{trayectoriaForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{trayectoriaForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
                                    											action="#{trayectoriaForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>	
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{trayectoriaForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
																				                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�dula
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="8"
	    	    		                							maxlength="8"
            	    		        								value="#{trayectoriaForm.trayectoria.cedula}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="cedula"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="cedula" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Primer Apellido
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="20"
	    	    		                							maxlength="20"
            	    		        								value="#{trayectoriaForm.trayectoria.primerApellido}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="primerApellido"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="primerApellido" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Segundo Apellido
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="20"
	    	    		                							maxlength="20"
            	    		        								value="#{trayectoriaForm.trayectoria.segundoApellido}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="segundoApellido"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="segundoApellido" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Primer Nombre
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="20"
	    	    		                							maxlength="20"
            	    		        								value="#{trayectoriaForm.trayectoria.primerNombre}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="primerNombre"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="primerNombre" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Segundo Nombre
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="20"
	    	    		                							maxlength="20"
            	    		        								value="#{trayectoriaForm.trayectoria.segundoNombre}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="segundoNombre"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="segundoNombre" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									A�o Preparaci�n
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="4"
	    	    		                							maxlength="4"
            	    		        								value="#{trayectoriaForm.trayectoria.anioPreparacion}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="anioPreparacion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="anioPreparacion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Preparaci�n
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaPreparacion"
                		        									value="#{trayectoriaForm.trayectoria.fechaPreparacion}"
                    		    									readonly="#{!trayectoriaForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="fechaPreparacion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																										                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Nro. Remesa
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{trayectoriaForm.trayectoria.numeroRemesa}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="numeroRemesa"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="numeroRemesa" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Nro. Movimiento
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{trayectoriaForm.trayectoria.numeroMovimiento}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="numeroMovimiento"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="numeroMovimiento" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																			                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Organismo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{trayectoriaForm.trayectoria.nombreOrganismo}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="nombreOrganismo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreOrganismo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Estatus
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="1"
	    	    		                							maxlength="1"
            	    		        								value="#{trayectoriaForm.trayectoria.estatus}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="estatus"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="estatus" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Estatus
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaEstatus"
                		        									value="#{trayectoriaForm.trayectoria.fechaEstatus}"
                    		    									readonly="#{!trayectoriaForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="fechaEstatus" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Vigencia
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaVigencia"
                		        									value="#{trayectoriaForm.trayectoria.fechaVigencia}"
                    		    									readonly="#{!trayectoriaForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="fechaVigencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																					                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Regi�n
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{trayectoriaForm.trayectoria.codRegion}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="codRegion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codRegion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Nombre Regi�n
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{trayectoriaForm.trayectoria.nombreRegion}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="nombreRegion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreRegion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Dependencia
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="12"
	    	    		                							maxlength="12"
            	    		        								value="#{trayectoriaForm.trayectoria.codDependencia}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="codDependencia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codDependencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Dependencia
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="100"
	    	    		                							maxlength="100"
            	    		        								value="#{trayectoriaForm.trayectoria.nombreDependencia}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="nombreDependencia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="nombreDependencia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																										                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Movimiento
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{trayectoriaForm.trayectoria.descripcionMovimiento}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="descripcionMovimiento"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="descripcionMovimiento" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																														                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Codigo Cargo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{trayectoriaForm.trayectoria.codCargo}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="codCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Descripcion Cargo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{trayectoriaForm.trayectoria.descripcionCargo}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="descripcionCargo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="descripcionCargo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																														                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Grado
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{trayectoriaForm.trayectoria.grado}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="grado"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="grado" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Paso
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="2"
	    	    		                							maxlength="2"
            	    		        								value="#{trayectoriaForm.trayectoria.paso}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="paso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="paso" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Nomina
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{trayectoriaForm.trayectoria.codigoNomina}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="codigoNomina"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        								onkeypress="return keyIntegerCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codigoNomina" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																																																																																		                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Sueldo Basico
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{trayectoriaForm.trayectoria.sueldoBasico}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="sueldoBasico"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="sueldoBasico" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Compensacion
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{trayectoriaForm.trayectoria.compensacion}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="compensacion"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="compensacion" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Prima Jerarquia
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{trayectoriaForm.trayectoria.primaJerarquia}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="primaJerarquia"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="primaJerarquia" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Prima Servicio
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{trayectoriaForm.trayectoria.primaServicio}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="primaServicio"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="primaServicio" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Ajuste Sueldo
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{trayectoriaForm.trayectoria.ajusteSueldo}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="ajusteSueldo"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="ajusteSueldo" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Otras Remuneraciones
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{trayectoriaForm.trayectoria.otrosPagos}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="otrosPagos"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="otrosPagos" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Otros No Aprobados Vicepladin
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{trayectoriaForm.trayectoria.otrosNoVicepladin}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="otrosNoVicepladin"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="otrosNoVicepladin" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																			                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Punto de Cuenta
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{trayectoriaForm.trayectoria.puntoCuenta}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="puntoCuenta"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="puntoCuenta" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Punto de Cuenta
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="fechaPuntoCuenta"
                		        									value="#{trayectoriaForm.trayectoria.fechaPuntoCuenta}"
                    		    									readonly="#{!trayectoriaForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onblur="javascript:check_date(this)"
	                        										onkeypress="return keyEnterCheck(event, this)"
    	                    										        	                										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
																									<h:message for="fechaPuntoCuenta" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Concurso
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{trayectoriaForm.trayectoria.codConcurso}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="codConcurso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        											                        											                        								onkeypress="return keyEnterCheck(event, this)"
			                        																									 >
								                        									</h:inputText>
                        																																<h:message for="codConcurso" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    							                        		<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="3"
    	        		                							id="observaciones"
                    		        								                        		    								value="#{trayectoriaForm.trayectoria.observaciones}"
                            										readonly="#{!trayectoriaForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										onkeypress="return keyEnterCheck(event, this)"
                            										required="false" />
                        																																<h:message for="observaciones" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																													                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									horas
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{trayectoriaForm.trayectoria.horas}"
                	    		    								                    	    										readonly="#{!trayectoriaForm.editing}"
                        											id="horas"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        														                        								onkeypress="return keyFloatCheck(event, this)"
			                        								onblur="javascript:fieldEmpty(this)"
			                        											                        											                        																									 style="text-align:right"  >
																								<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        									</h:inputText>
                        																																<h:message for="horas" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																			<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsTrayectoria"
            										rendered="#{trayectoriaForm.showData}">
		                        					<f:verbatim><table>
		                        					<table class="datatable" align="center">
    		                    						<tr>
    		                    						
        		                							<td></f:verbatim>
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{trayectoriaForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{trayectoriaForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{trayectoriaForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{trayectoriaForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{trayectoriaForm.editing}" />
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{trayectoriaForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>