<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">

<f:view>
	<x:saveState value="#{antecedenteForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formAntecedente">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Antecedentes de Servicio sujeto a LEFP
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/expediente/Antecedente.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									<f:subview 
										id="searchAntecedente"
										rendered="#{!antecedenteForm.showData&&
										!antecedenteForm.showAdd}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{antecedenteForm.findPersonalCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
																onfocus="return deleteZero(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{antecedenteForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{antecedenteForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:inputText size="20"
																maxlength="20"
																value="#{antecedenteForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{antecedenteForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:inputText size="20"
																maxlength="20"
																value="#{antecedenteForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{antecedenteForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{antecedenteForm.showResultPersonal&&
									!antecedenteForm.showData&&
									!antecedenteForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <h:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{antecedenteForm.resultPersonal}"
    	                                            
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{antecedenteForm.findAntecedenteByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </h:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif" />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif" />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif" />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{antecedenteForm.selectedPersonal&&
									!antecedenteForm.showData&&
									!antecedenteForm.editing&&
									!antecedenteForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>
            									<h:commandLink value="#{antecedenteForm.personal}"
    	                            						action="#{antecedenteForm.findAntecedenteByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{antecedenteForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{antecedenteForm.add}"
            													onclick="javascript:return clickMade()"
            													rendered="#{!antecedenteForm.editing&&!antecedenteForm.deleting&&antecedenteForm.login.agregar}" />
            													<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{antecedenteForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />
                                        	    			<f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultAntecedente"
									rendered="#{antecedenteForm.showResult&&
									!antecedenteForm.showData&&
									!antecedenteForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <h:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{antecedenteForm.result}"
                                            	   
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{antecedenteForm.selectAntecedente}"
	                                						styleClass="listitem">
    		                            						<f:param name="idAntecedente" value="#{result.idAntecedente}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </h:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif" />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataAntecedente"
								rendered="#{antecedenteForm.showData||antecedenteForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!antecedenteForm.editing&&!antecedenteForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{antecedenteForm.editing&&!antecedenteForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{antecedenteForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{antecedenteForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="antecedenteForm">
	                                            <h:inputHidden id="scrollx" value="#{antecedenteForm.scrollx}" />
    	                                        <h:inputHidden id="scrolly" value="#{antecedenteForm.scrolly}" />
	    	        							<f:subview 
    	    	    								id="dataAntecedente"
        	    									rendered="#{antecedenteForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{antecedenteForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!antecedenteForm.editing&&!antecedenteForm.deleting&&antecedenteForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{antecedenteForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!antecedenteForm.editing&&!antecedenteForm.deleting&&antecedenteForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{antecedenteForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{antecedenteForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{antecedenteForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{antecedenteForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{antecedenteForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
			                                    								action="#{antecedenteForm.abortUpdate}"
			                                    								immediate="true"
			                                    								onclick="javascript:return clickMade()"
			                            	        							 />
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{antecedenteForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									Instituci�n
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{antecedenteForm.antecedente.nombreInstitucion}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="Institucion"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        			</h:inputText>
                        			                        		<f:verbatim><span class="required"> *</span></f:verbatim>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	<f:verbatim><tr>
    	                    								<td>
        	                									Tipo de Personal al ingresar
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{antecedenteForm.antecedente.personalIngreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="TipodePersonalalingresar"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									Fecha de Ingreso
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechadeIngreso"
                		        									value="#{antecedenteForm.antecedente.fechaIngreso}"
                    		    									readonly="#{!antecedenteForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
    	                    										onblur="check_date(this)"       
    	                    										required="true">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									C�digo Cargo al Ingreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{antecedenteForm.antecedente.cargoIngreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="C�digoCargoalIngreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									Cargo al Ingreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{antecedenteForm.antecedente.descargoIngreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="CargoalIngreso"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        			</h:inputText>
                        			                        		<f:verbatim><span class="required"> *</span></f:verbatim>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									C�digo RAC al Ingreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{antecedenteForm.antecedente.codracIngreso}"
                	    		    								 readonly="#{!antecedenteForm.editing}"
                        											id="CodigoRACalIngreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyIntegerCheck(event, this)"
																	 >
								                        	</h:inputText>
                        									<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									Sueldo al Ingreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{antecedenteForm.antecedente.sueldoIngreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="SueldoalIngreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
																	 style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									Compensaci�n al Ingreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{antecedenteForm.antecedente.compensacionIngreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="CompensacionalIngreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
																	 style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									Primas al Ingreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{antecedenteForm.antecedente.primasIngreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="PrimasalIngreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
																	 style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									Tipo de Personal al egresar
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{antecedenteForm.antecedente.personalEgreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="TipodePersonalalegresar"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        			</h:inputText>
                        												<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									Fecha de Egreso
	            	            							</td>
		                        							<td></f:verbatim>
																                	        		<h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechadeEgreso"
                		        									value="#{antecedenteForm.antecedente.fechaEgreso}"
                    		    									readonly="#{!antecedenteForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
    	                    										 onblur="check_date(this)"     
    	                    										 required="true">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
									        	            	    							<f:verbatim></td>
	        	            	    					</tr>
														</f:verbatim>
	        	            	    					<f:verbatim>
														<tr>
    	                    								<td>Tiempo Servicio</td>
		                        							<td>
		                        							<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                        								<tr>
		                        									<td>
		                        									</f:verbatim>
																		<h:inputText
																			size="25"
																			maxlength="25"
																			id="Tiempo"
																			value="#{antecedenteForm.diferencia}"
																			readonly="true"
																			onchange="javascript:this.value=this.value.toUpperCase()">																	
																		</h:inputText>
																		<f:verbatim><span class="required"></span></f:verbatim>
									        	            	    <f:verbatim>
									        	            	    </td>
														        	<td align="left">
																	</f:verbatim>				
																		<h:commandButton value="Calcular" 
						                                					image="/images/calculate.gif"
						                                					action="this.form.submit()"
						                                					onclick="javascript:return clickMade()"/>
						                        	        		<f:verbatim>
																	</td>
																</tr>
															</table>
															</td>
	        	            	    					</tr>
														</f:verbatim>
														<f:verbatim>
														<tr>
    	                    								<td>
        	                									C�digo Cargo al Egreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="6"
	    	    		                							maxlength="6"
            	    		        								value="#{antecedenteForm.antecedente.cargoEgreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="CodigoCargoalEgreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	<f:verbatim><tr>
    	                    								<td>
        	                									Cargo al Egreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{antecedenteForm.antecedente.descargoEgreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="CargoalEgreso"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        			</h:inputText>
                        			                        		<f:verbatim><span class="required"> *</span></f:verbatim>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									Causa de Egreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{antecedenteForm.antecedente.causaEgreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="CausadeEgreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									C�digo RAC al Egreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{antecedenteForm.antecedente.codracEgreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="CodigoRACalEgreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyIntegerCheck(event, this)"
																	 >
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									Sueldo al Egreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{antecedenteForm.antecedente.sueldoEgreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="SueldoalEgreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
																	 style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									Compensaci�n al Egreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{antecedenteForm.antecedente.compensacionEgreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="CompensacionalEgreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
																	 style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	<f:verbatim><tr>
    	                    								<td>
        	                									Primas al Egreso
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{antecedenteForm.antecedente.primasEgreso}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="PrimasalEgreso"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
																	 style="text-align:right"  >
																	<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									Prestaciones Pendientes
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	 <h:selectOneMenu value="#{antecedenteForm.antecedente.prestacionesPendientes}"
        	                                        	        	disabled="#{!antecedenteForm.editing}"
            	                                        	    	immediate="false"
                	            									required="false"
    	                        	                            	title="Prestaciones Pendientes">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{antecedenteForm.listPrestacionesPendientes}" />
					    	            	                        	            </h:selectOneMenu>	<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	<f:verbatim><tr>
    	                    								<td>
        	                									Vacaciones Pendientes
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	<h:selectOneMenu value="#{antecedenteForm.antecedente.vacacionesPendientes}"
        	                                        	        	disabled="#{!antecedenteForm.editing}"
            	                                        	    	immediate="false"
                	            									required="false"
    	                        	                            	title="Vacaciones Pendientes">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{antecedenteForm.listVacacionesPendientes}" />
					    	            	                        	            </h:selectOneMenu>	<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim><tr>
    	                    								<td>
        	                									Dias Vacaciones
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="10"
	    	    		                							maxlength="10"
            	    		        								value="#{antecedenteForm.antecedente.diasVacaciones}"
                	    		    								readonly="#{!antecedenteForm.editing}"
                        											id="DiasVacaciones"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyIntegerCheck(event, this)"
																	 >
								                        			</h:inputText>
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    	<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="4"
    	        		                							id="Observaciones"
                    		        								value="#{antecedenteForm.antecedente.observaciones}"
                            										readonly="#{!antecedenteForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										required="false" />
                        											<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
															<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsAntecedente"
            										rendered="#{antecedenteForm.showData}">
		                        					<f:verbatim><table>
    		                    						<tr>
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{antecedenteForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!antecedenteForm.editing&&!antecedenteForm.deleting&&antecedenteForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{antecedenteForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!antecedenteForm.editing&&!antecedenteForm.deleting&&antecedenteForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{antecedenteForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{antecedenteForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{antecedenteForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{antecedenteForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{antecedenteForm.editing}" />	
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{antecedenteForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>