<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">

	<jsp:include page="/inc/functions.jsp" />

</head>

<body onload="scrollToCoordinates(); firstFocus();">

<f:view>
	<x:saveState value="#{credencialForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formCredencial">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Documentos de Identificaci�n
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/expediente/Credencial.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									
									<f:subview 
										id="searchCredencial"
										rendered="#{!credencialForm.showData&&
										!credencialForm.showAdd&&
										!credencialForm.login.servicioPersonal}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{credencialForm.findPersonalCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
																onfocus="return deleteZero(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{credencialForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{credencialForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{credencialForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{credencialForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{credencialForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:commandButton image="/images/find.gif" 
																action="#{credencialForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{credencialForm.showResultPersonal&&
									!credencialForm.showData&&
									!credencialForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <h:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{credencialForm.resultPersonal}"
    	                                            
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{credencialForm.findCredencialByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </h:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif"  />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif"  />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{credencialForm.selectedPersonal&&
									!credencialForm.showData&&
									!credencialForm.editing&&
									!credencialForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{credencialForm.personal}   <-- clic aqui"
    	                            						action="#{credencialForm.findCredencialByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{credencialForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{credencialForm.add}"
            													onclick="javascript:return clickMade()"
            													rendered="#{!credencialForm.editing&&!credencialForm.deleting&&credencialForm.login.agregar}" />
                                        	    			
                                        	    			<% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{credencialForm.abort}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />
                                                    	    <%}%>
                                                    	    <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultCredencial"
									rendered="#{credencialForm.showResult&&
									!credencialForm.showData&&
									!credencialForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <h:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{credencialForm.result}"
                                            	    
                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{credencialForm.selectCredencial}"
	                                						styleClass="listitem">
    		                            						<f:param name="idCredencial" value="#{result.idCredencial}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </h:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif"  />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif"  />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataCredencial"
								rendered="#{credencialForm.showData||credencialForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!credencialForm.editing&&!credencialForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{credencialForm.editing&&!credencialForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{credencialForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{credencialForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="credencialForm">
	                                            <h:inputHidden id="scrollx" value="#{credencialForm.scrollx}" />
    	                                        <h:inputHidden id="scrolly" value="#{credencialForm.scrolly}" />
	    	        							<f:subview 
    	    	    								id="dataCredencial"
        	    									rendered="#{credencialForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{credencialForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!credencialForm.editing&&!credencialForm.deleting&&credencialForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{credencialForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!credencialForm.editing&&!credencialForm.deleting&&credencialForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{credencialForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{credencialForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{credencialForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{credencialForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{credencialForm.editing}" />
                	                	            	        			<h:commandButton value="Cancelar" 
						                                    					image="/images/cancel.gif"
						                        								action="#{credencialForm.abortUpdate}"
						                        								immediate="true"
						                        								onclick="javascript:return clickMade()"
						                	        							 />
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{credencialForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
														<f:verbatim><tr>
                        						<td>
                        							Tipo Documento
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{credencialForm.selectTipoCredencialForSubTipoCredencial}"
                                                    	disabled="#{!credencialForm.editing}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{credencialForm.changeTipoCredencialForSubTipoCredencial}"
                        								required="false"
                        								readonly="true"
														immediate="false"
														title="Credencial">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{credencialForm.colTipoCredencialForSubTipoCredencial}" />
													</h:selectOneMenu>
												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Clase Documento
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{credencialForm.selectSubTipoCredencial}"
                                                    	disabled="#{!credencialForm.editing}"
                                                    	rendered="#{credencialForm.showSubTipoCredencial}"
                                                    	immediate="false"
	                        							id="subTipoCredencial"
                                                    	required="false"
                                                    	readonly="true"
														title="Credencial">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{credencialForm.colSubTipoCredencial}" />
                                                    </h:selectOneMenu><h:message for="subTipoCredencial" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr> </f:verbatim>
														<f:verbatim><tr>
    	                    								<td>
        	                									N�mero
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="15"
	    	    		                							maxlength="15"
            	    		        								value="#{credencialForm.credencial.numero}"
                	    		    								readonly="#{!credencialForm.editing}"
                        											id="numero"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        			</h:inputText>
                        									<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Color
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="22"
	    	    		                							maxlength="20"
            	    		        								value="#{credencialForm.credencial.color}"
                	    		    								readonly="#{!credencialForm.editing}"
                        											id="color"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        			</h:inputText>
                        									<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														
														<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Entrega
	            	            							</td>
		                        							<td></f:verbatim>
																   <h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaEntrega"
                		        									value="#{credencialForm.credencial.fechaEntrega}"
                    		    									readonly="#{!credencialForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
    	                    										required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
									        	            <f:verbatim></td>
	        	            	    					</tr></f:verbatim>
														
														<f:verbatim><tr>
    	                    								<td>
        	                									Fecha Retiro
	            	            							</td>
		                        							<td></f:verbatim>
																    <h:inputText
			                        								size="10"
	    		                    								maxlength="10"
	        		                								id="FechaRetiro"
                		        									value="#{credencialForm.credencial.fechaRetiro}"
                    		    									readonly="#{!credencialForm.editing}"
	                        										onchange="javascript:this.value=this.value.toUpperCase()"
	                        										onkeypress="return keyEnterCheck(event, this)"
	                        										onblur="javascript:check_date(this)"
    	                    									    required="false">
																	<f:convertDateTime timeZone="#{dateTime.timeZone}"
                	                        		                	pattern="dd-MM-yyyy" />
                    		    									</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
									        	            <f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Motivo
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	<h:selectOneMenu value="#{credencialForm.credencial.motivo}"
        	                                        	        	disabled="#{!credencialForm.editing}"
            	                                        	    	immediate="false"
                	            									required="false"
    	                        	                            	title="Motivo">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{credencialForm.listMotivo}" />
					    	            	                       </h:selectOneMenu>
					    	            	                 <f:verbatim></td>
	        	            	    					</tr></f:verbatim>
	        	            	    					<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    	<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="4"
    	        		                							id="observaciones"
                    		        								value="#{credencialForm.credencial.observaciones}"
                            										readonly="#{!credencialForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										onkeypress="return keyEnterCheck(event, this)"
                            										required="false" />
                        											<h:message for="observaciones" styleClass="error"/>
        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
													<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsCredencial"
            										rendered="#{credencialForm.showData}">
		                        					<f:verbatim><table>
    		                    						<tr>
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{credencialForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!credencialForm.editing&&!credencialForm.deleting&&credencialForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{credencialForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!credencialForm.editing&&!credencialForm.deleting&&credencialForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{credencialForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{credencialForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{credencialForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{credencialForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{credencialForm.editing}" />	
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{credencialForm.abortUpdate}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>