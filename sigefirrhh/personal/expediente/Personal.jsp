<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags"%>
<%@ page import="sigefirrhh.login.LoginSession"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x"%>

<tags:seguridad />
<%
	if (!((LoginSession) session.getAttribute("loginSession"))
			.isValid()) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<title>Sigefirrhh</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/sigefirrhh/style/webstyle.css" rel="stylesheet"
	type="text/css">
<jsp:include page="/inc/functions.jsp" />
</head>
<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{personalForm}" />
	<f:loadBundle basename="sigefirrhh.custMessages" var="custMessages" />
	<jsp:include page="/inc/top.jsp" />

	<table width="770" border="0" cellspacing="0" cellpadding="5"
		align="center">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />
	
    				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formPersonal">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
						<table width="100%" class="toptable">
							<tr>
								<td align="left"><b> Datos Personales </b></td>
								<td align="right"><a href="#"
									onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/expediente/Personal.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
								<h:graphicImage url="/images/help/help.gif" /> </a></td>
							</tr>
						</table>
						<table width="100%" class="toptable">
							<tr>
								<td align="left">
								<table width="100%" class="bandtable">
									<tr>
										<td style="text-align: right"><h:commandButton
											image="/images/add.gif" action="#{personalForm.add}"
											onclick="javascript:return clickMade()"
											rendered="#{!personalForm.editing&&!personalForm.deleting&&personalForm.login.agregar&&!personalForm.login.servicioPersonal}" />
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td align="left"><h:messages errorClass="error"
									styleClass="success" /></td>
							</tr>
							<f:subview id="search1"
								rendered="#{!personalForm.showData&&
								!personalForm.login.servicioPersonal}">
								<f:verbatim>
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>Buscar</td>
											</tr>
										</table>

										<table width="100%" class="datatable">
											</f:verbatim>
											<f:verbatim>
												<tr>
													<td>C�dula</td>
													<td>
											</f:verbatim>
											<h:inputText size="10" maxlength="10"
												value="#{personalForm.findPersonalCedula}"
												onkeypress="return keyIntegerCheck(event, this)"
												onfocus="return deleteZero(event, this)"
												onblur="javascript:fieldEmpty(this)"
												onchange="javascript:this.value=this.value.toUpperCase()" />
											<h:commandButton image="/images/find.gif"
												action="#{personalForm.findPersonalByCedula}"
												onclick="javascript:return clickMade()" />
											<f:verbatim>
												</td>
												</tr>
												<tr>
													<td class="datatable">Nombres</td>
													<td>
											</f:verbatim>
											<h:inputText size="20" maxlength="20"
												value="#{personalForm.findPersonalPrimerNombre}"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyEnterCheck(event, this)" />
											<h:inputText size="20" maxlength="20"
												value="#{personalForm.findPersonalSegundoNombre}"
												onchange="javascript:this.value=this.value.toUpperCase()"
												onkeypress="return keyEnterCheck(event, this)" />
											<f:verbatim>
												</td>
												</tr>
												<tr>
													<td class="datatable">Apellidos</td>
													<td>
											</f:verbatim>
											<h:inputText size="20" maxlength="20"
												value="#{personalForm.findPersonalPrimerApellido}"
												onchange="javascript:this.value=this.value.toUpperCase()" />
											<h:inputText size="20" maxlength="20"
												value="#{personalForm.findPersonalSegundoApellido}"
												onchange="javascript:this.value=this.value.toUpperCase()" />
											<h:commandButton image="/images/find.gif"
												action="#{personalForm.findPersonalByNombresApellidos}"
												onclick="javascript:return clickMade()" />
											<f:verbatim>
												</td>
												</tr>
										</table>
										</td>
									</tr>
						</table>
						</f:verbatim> </f:subview> <f:subview id="viewResultPersonal"
							rendered="#{personalForm.showPersonalByCedula&&
								!personalForm.showData}">
							<f:verbatim>
								<table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>Resultado de B�squeda</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim> <h:dataTable id="result" styleClass="datatable"
											headerClass="standardTable_Header"
											footerClass="standardTable_Header"
											rowClasses="standardTable_Row1,standardTable_Row2"
											columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
											var="result" value="#{personalForm.result}" rows="10"
											width="100%">
											<h:column>
												<h:commandLink value="#{result}"
													action="#{personalForm.selectPersonal}"
													styleClass="listitem">
													<f:param name="idPersonal" value="#{result.idPersonal}" />
												</h:commandLink>
											</h:column>
										</h:dataTable> <h:panelGrid columns="1" styleClass="scrollerTable2"
											columnClasses="standardTable_ColumnCentered">
											<x:dataScroller id="scroll_3" for="result" fastStep="10"
												pageCountVar="pageCount" pageIndexVar="pageIndex">
												<f:facet name="first">
													<h:graphicImage url="/images/arrow-first.gif" />
												</f:facet>
												<f:facet name="last">
													<h:graphicImage url="/images/arrow-last.gif" />
												</f:facet>
												<f:facet name="previous">
													<h:graphicImage url="/images/arrow-previous.gif" />
												</f:facet>
												<f:facet name="next">
													<h:graphicImage url="/images/arrow-next.gif" />
												</f:facet>
												<f:facet name="fastforward">
													<h:graphicImage url="/images/arrow-ff.gif" />
												</f:facet>
												<f:facet name="fastrewind">
													<h:graphicImage url="/images/arrow-fr.gif" />
												</f:facet>
											</x:dataScroller>
											<x:dataScroller id="scroll_4" for="result"
												pageCountVar="pageCount" pageIndexVar="pageIndex">
												<h:outputFormat
													value="#{custMessages['data_scroller_pages']}">
													<f:param value="#{pageIndex}" />
													<f:param value="#{pageCount}" />
												</h:outputFormat>
											</x:dataScroller>
										</h:panelGrid> <f:verbatim></td>
									</tr>
								</table>
							</f:verbatim>
						</f:subview> </h:form> <f:subview id="viewDataPersonal"
							rendered="#{personalForm.showData}">
							<f:verbatim>
								<table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim> <h:outputText value="Consultando"
													rendered="#{!personalForm.editing&&!personalForm.deleting}" />
												<h:outputText value="Modificando"
													rendered="#{personalForm.editing&&!personalForm.adding}" />
												<h:outputText value="Eliminando"
													rendered="#{personalForm.deleting}" /> <h:outputText
													value="Agregando" rendered="#{personalForm.adding}" /> <f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim> <h:form id="personalForm"
											enctype="multipart/form-data">
											<h:inputHidden id="scrollx" value="#{personalForm.scrollx}" />
											<h:inputHidden id="scrolly" value="#{personalForm.scrolly}" />
											<f:verbatim>
												<table class="datatable" width="100%">
													<tr>
														<td colspan="2">
														<table align="center">
															<tr>
																<td></f:verbatim> <h:commandButton value="Modificar"
																	image="/images/modify.gif"
																	action="#{personalForm.edit}"
																	onclick="javascript:return clickMade()"
																	rendered="#{!personalForm.editing&&!personalForm.deleting&&personalForm.login.modificar}" />
																<h:commandButton value="Eliminar"
																	image="/images/delete.gif"
																	action="#{personalForm.delete}"
																	onclick="javascript:return clickMade()"
																	rendered="#{!personalForm.editing&&!personalForm.deleting&&personalForm.login.eliminar&&!personalForm.login.servicioPersonal}" />
																<h:outputText value="�Seguro que desea eliminar?"
																	rendered="#{personalForm.deleting}" /> <h:commandButton
																	value="Si" image="/images/yes.gif"
																	action="#{personalForm.deleteOk}"
																	onclick="javascript:return clickMade()"
																	rendered="#{personalForm.deleting}" /> <h:commandButton
																	value="Guardar" image="/images/save.gif"
																	action="#{personalForm.save}"
																	onclick="javascript:return clickMade()"
																	rendered="#{personalForm.editing}" /> <h:commandButton
																	value="Cancelar" image="/images/cancel.gif"
																	action="#{personalForm.abortUpdate}" immediate="true"
																	onclick="javascript:return clickMade()" /> <f:verbatim></td>
															</tr>
														</table>
														</td>
													</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td width="20%" class="datatable">C�dula</td>
															<td width="80%">
													</f:verbatim>
													<h:inputText size="8" id="Cedula"
														rendered="#{personalForm.adding}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)" maxlength="8"
														value="#{personalForm.personal.cedula}"
														readonly="#{!personalForm.editing}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														required="true" />
													<h:inputText size="8" id="CedulaDisable"
														rendered="#{!personalForm.adding}"
														onkeypress="return keyIntegerCheck(event, this)"
														maxlength="8" value="#{personalForm.personal.cedula}"
														readonly="true"
														onchange="javascript:this.value=this.value.toUpperCase()"
														required="true" />
													<f:verbatim>
														<span class="required"> *</span>
													</f:verbatim>
													<f:verbatim>
														</td>
														</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td colspan="2">
															<table border="0" cellpadding="0" cellspacing="0"
																width="100%">
																<tr>
																	<td width="20%" class="datatable">Primer Apellido
																	</td>
																	<td width="30%"></f:verbatim> <h:inputText size="20"
																		id="PrimerApellido" maxlength="20"
																		value="#{personalForm.personal.primerApellido}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="true" /> <f:verbatim>
																		<span class="required"> *</span>
																	</f:verbatim> <f:verbatim></td>
																	<td width="20%" class="datatable">Segundo Apellido
																	</td>
																	<td width="30%"></f:verbatim> <h:inputText size="20"
																		id="SegundoApellido" maxlength="20"
																		value="#{personalForm.personal.segundoApellido}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																</tr>
															</table>
															</td>
														</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td colspan="2">
															<table border="0" cellpadding="0" cellspacing="0"
																width="100%">
																<tr>
																	<td width="20%" class="datatable">Primer Nombre</td>
																	<td width="30%"></f:verbatim> <h:inputText size="20"
																		id="PrimerNombre" maxlength="20"
																		value="#{personalForm.personal.primerNombre}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="true" /> <f:verbatim>
																		<span class="required"> *</span>
																	</f:verbatim> <f:verbatim></td>
																	<td width="20%" class="datatable">Segundo Nombre</td>
																	<td width="30%"></f:verbatim> <h:inputText size="20"
																		id="SegundoNombre" maxlength="20"
																		value="#{personalForm.personal.segundoNombre}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																</tr>
																</f:verbatim>
																<f:verbatim>
																	<tr>
																		<td width="20%" class="datatable">Sexo</td>
																		<td width="30%">
																</f:verbatim>
																<h:selectOneMenu value="#{personalForm.personal.sexo}"
																	id="Sexo" disabled="#{!personalForm.editing}"
																	immediate="false" required="true" title="Sexo">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{personalForm.listSexo}" />
																	<f:validator
																		validatorId="eforserver.RequiredJSFValidator" />
																</h:selectOneMenu>
																<f:verbatim>
																	<span class="required"> *</span>
																</f:verbatim>
																<f:verbatim>
																	</td>
																	<td width="20%" class="datatable">Estado Civil</td>
																	<td width="30%">
																</f:verbatim>
																<h:selectOneMenu
																	value="#{personalForm.personal.estadoCivil}"
																	id="EstadoCivil" disabled="#{!personalForm.editing}"
																	immediate="false" required="true" title="Estado Civil">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{personalForm.listEstadoCivil}" />
																	<f:validator
																		validatorId="eforserver.RequiredJSFValidator" />
																</h:selectOneMenu>
																<f:verbatim>
																	<span class="required"> *</span>
																</f:verbatim>
																<f:verbatim>
																	</td>
																	</tr>
																	<tr>
																		<td class="datatable">Es Madre o Padre?</td>
																		<td colspan="3">
																</f:verbatim>

																<h:selectOneMenu
																	value="#{personalForm.personal.madrePadre}"
																	id="MadrePadre" disabled="#{!personalForm.editing}"
																	immediate="false" required="true"
																	title="Es Madre o Padre">
																	<f:selectItems value="#{personalForm.listMadrePadre}" />
																	<f:validator
																		validatorId="eforserver.RequiredJSFValidator" />
																</h:selectOneMenu>
																<f:verbatim>
																	</td>
																	</tr>
																	<!-- agregado por pedro  al 06072009 -->
																	<tr>
																		<td colspan="4">
																		<table width="689" height="50">
																			<tr>
																				<td class="datatable" width="20%">Posee alguna
																				Discapacidad</td>
																				<td width="20%" colspan="3"></f:verbatim> <h:selectOneMenu
																					value="#{personalForm.personal.discapacidad}"
																					id="discapacidad"
																					disabled="#{!personalForm.editing}"
																					onchange="saveScrollCoordinates();this.form.submit()"
																					immediate="false" required="false"
																					title="discapacidad">
																					<f:selectItem itemLabel="Seleccione" itemValue="0" />
																					<f:selectItems
																						value="#{personalForm.listdiscapacidad}" />
																				</h:selectOneMenu> <f:verbatim></td>
																				<td class="datatable" width="20%">Tipo
																				Discapacidad</td>
																				<td width="20%" colspan="3"></f:verbatim> <h:selectOneMenu
																					value="#{personalForm.personal.tipodiscapacidad}"
																					id="tipodiscapacidad"
																					disabled="#{!personalForm.editing}"
																					onchange="saveScrollCoordinates();this.form.submit()"
																					immediate="false" required="false"
																					title="tipodiscapacidad">
																					<f:selectItem itemLabel="Seleccione" itemValue="0" />
																					<f:selectItems
																						value="#{personalForm.listtipodiscapacidad}" />
																				</h:selectOneMenu> <f:verbatim></td>
																			</tr>
																			<tr>
																				<!-- agregado pedro -->
																				<td width="20%" class="datatable">Pueblo
																				Indigena</td>

																				<td width="20%" class="datatable"></f:verbatim> <h:selectOneMenu
																					value="#{personalForm.personal.puebloindigena}"
																					id="puebloindigena"
																					disabled="#{!personalForm.editing}"
																					onchange="saveScrollCoordinates();this.form.submit()"
																					immediate="false" required="false"
																					title="puebloindigena">
																					<f:selectItem itemLabel="Seleccione" itemValue="0" />
																					<f:selectItems
																						value="#{personalForm.listpuebloindigena}" />
																				</h:selectOneMenu><f:verbatim></td>
																				<!-- agregado pedro -->
																			</tr>
																			<!-- agregado pedro fin -->
																		</table>
																		</td>
																	</tr>
																	<!-- agregado por pedro  al 06072009 fin -->
															</table>
															</td>
														</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>
														<tr>
															<td height="2" class="bandtable" colspan="2"></td>
														</tr>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td colspan="2">
															<table border="0" cellpadding="0" cellspacing="0"
																width="100%">
																<tr>
																	<td width="20%" class="datatable">Fecha Nacimiento
																	</td>
																	<td width="60%"></f:verbatim> <h:inputText id="FechaNacimiento"
																		size="10" maxlength="10"
																		value="#{personalForm.personal.fechaNacimiento}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		onblur="javascript:check_date(this)" required="true">
																		<f:convertDateTime timeZone="#{dateTime.timeZone}"
																			pattern="dd-MM-yyyy" />
																	</h:inputText> <f:verbatim>(dd-mm-aaaa)<span
																			class="required"> *</span>
																	</f:verbatim> <f:verbatim></td>
																	<td width="30%" class="datatable">Nacionalidad</td>
																	<td width="30%"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.personal.nacionalidad}"
																		id="Nacionalidad" disabled="#{!personalForm.editing}"
																		immediate="false" required="true" title="Nacionalidad">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.listNacionalidad}" />
																		<f:validator
																			validatorId="eforserver.RequiredJSFValidator" />
																	</h:selectOneMenu> <f:verbatim>
																		<span class="required">
																	</f:verbatim> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Pais de
																	Nacimiento</td>
																	<td width="10%" colspan="3"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.selectPaisForCiudadNacimiento}"
																		disabled="#{!personalForm.editing}"
																		onchange="saveScrollCoordinates();this.form.submit()"
																		valueChangeListener="#{personalForm.changePaisForCiudadNacimiento}"
																		immediate="false" required="false" title="Ciudad">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.colPaisForCiudadNacimiento}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Estado de
																	Nacimiento</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.selectEstadoForCiudadNacimiento}"
																		disabled="#{!personalForm.editing}"
																		onchange="saveScrollCoordinates();this.form.submit()"
																		valueChangeListener="#{personalForm.changeEstadoForCiudadNacimiento}"
																		rendered="#{personalForm.showEstadoForCiudadNacimiento}"
																		immediate="false" required="false" title="Ciudad">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.colEstadoForCiudadNacimiento}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Ciudad de
																	Nacimiento</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.selectCiudadNacimiento}"
																		id="CiudadNacimiento"
																		disabled="#{!personalForm.editing}"
																		rendered="#{personalForm.showCiudadNacimiento}"
																		immediate="false" required="false" title="Ciudad">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.colCiudadNacimiento}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td class="datatable">Fecha Fallecimiento</td>
																	<td colspan="3"></f:verbatim> <h:inputText
																		id="FechaFallecimiento" size="10" maxlength="10"
																		value="#{personalForm.personal.fechaFallecimiento}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		onblur="javascript:check_date(this)">
																		<f:convertDateTime timeZone="#{dateTime.timeZone}"
																			pattern="dd-MM-yyyy" />
																	</h:inputText> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Doble
																	Nacionalidad</td>
																	<td width="30%" colspan="3"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.personal.dobleNacionalidad}"
																		id="DobleNacionalidad"
																		disabled="#{!personalForm.editing}"
																		onchange="saveScrollCoordinates();this.form.submit()"
																		immediate="false" required="false"
																		title="Doble Nacionalidad">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.listDobleNacionalidad}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Otra
																	Nacionalidad</td>
																	<td width="30%" colspan="3"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.selectPaisNacionalidad}"
																		id="PaisNacionalidad"
																		disabled="#{!personalForm.editing}"
																		rendered="#{personalForm.showPaisNacionalidadAux}"
																		immediate="false" required="false"
																		title="Otra Nacionalidad">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.colPaisNacionalidad}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Nacionalizado</td>
																	<td width="30%"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.personal.nacionalizado}"
																		id="Nacionalizado" disabled="#{!personalForm.editing}"
																		onchange="saveScrollCoordinates();this.form.submit()"
																		immediate="false" required="false"
																		title="Nacionalizado">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.listNacionalizado}" />
																	</h:selectOneMenu> <f:verbatim></td>
																	<td width="20%" class="datatable">Fecha
																	Nacionalizaci�n</td>
																	<td width="30%"></f:verbatim> <h:inputText
																		id="FechaNacionalizacion" size="10" maxlength="10"
																		value="#{personalForm.personal.fechaNacionalizacion}"
																		readonly="#{!personalForm.editing}"
																		onkeypress="return keyEnterCheck(event, this)"
																		onblur="javascript:check_date(this)"
																		rendered="#{personalForm.showFechaNacionalizacionAux}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		required="false">
																		<f:convertDateTime timeZone="#{dateTime.timeZone}"
																			pattern="dd-MM-yyyy" />
																	</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Gaceta</td>
																	<td width="30%"></f:verbatim> <h:inputText size="10"
																		id="Gaceta" maxlength="10"
																		value="#{personalForm.personal.gacetaNacionalizacion}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		rendered="#{personalForm.showGacetaNacionalizacionAux}"
																		required="false" /> <f:verbatim></td>
																	<td width="20%" class="datatable">Otra Normativa</td>
																	<td width="30%"></f:verbatim> <h:inputText size="40"
																		id="OtraNormativa" maxlength="40"
																		value="#{personalForm.personal.otraNormativaNac}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		rendered="#{personalForm.showOtraNormativaNacAux}"
																		required="false" /> <f:verbatim></td>
																</tr>
															</table>
															</td>
														</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>
														<tr>
															<td height="2" class="bandtable" colspan="2"></td>
														</tr>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td colspan="2">
															<table border="0" cellpadding="0" cellspacing="0"
																width="100%">
																<tr>
																	<td width="20%" class="datatable">Direcci�n</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:inputText
																		size="100" id="Direccion" maxlength="100"
																		value="#{personalForm.personal.direccionResidencia}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Zona Postal</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:inputText size="6"
																		id="ZonaPostal" maxlength="6"
																		value="#{personalForm.personal.zonaPostalResidencia}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Pais de
																	Residencia</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.selectPaisForCiudadResidencia}"
																		disabled="#{!personalForm.editing}"
																		onchange="saveScrollCoordinates();this.form.submit()"
																		valueChangeListener="#{personalForm.changePaisForCiudadResidencia}"
																		immediate="false" required="false" title="Ciudad">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.colPaisForCiudadResidencia}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Estado de
																	Residencia</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.selectEstadoForCiudadResidencia}"
																		disabled="#{!personalForm.editing}"
																		onchange="saveScrollCoordinates();this.form.submit()"
																		valueChangeListener="#{personalForm.changeEstadoForCiudadResidencia}"
																		rendered="#{personalForm.showEstadoForCiudadResidencia}"
																		immediate="false" required="false" title="Ciudad">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.colEstadoForCiudadResidencia}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Ciudad de
																	Residencia</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.selectCiudadResidencia}"
																		id="CiudadResidencia"
																		disabled="#{!personalForm.editing}"
																		rendered="#{personalForm.showCiudadResidencia}"
																		immediate="false" required="false" title="Ciudad">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.colCiudadResidencia}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Municipio de
																	Residencia</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.selectMunicipioForParroquia}"
																		disabled="#{!personalForm.editing}"
																		onchange="saveScrollCoordinates();this.form.submit()"
																		valueChangeListener="#{personalForm.changeMunicipioForParroquia}"
																		rendered="#{personalForm.showMunicipioForParroquia}"
																		immediate="false" required="false" title="Parroquia">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.colMunicipioForParroquia}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Parroquia de
																	Residencia</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.selectParroquia}"
																		disabled="#{!personalForm.editing}"
																		rendered="#{personalForm.showParroquia}"
																		immediate="false" required="false" title="Parroquia">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems value="#{personalForm.colParroquia}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Tel�fono</td>
																	<td width="30%"></f:verbatim> <h:inputText size="15"
																		id="Telefono" maxlength="15"
																		value="#{personalForm.personal.telefonoResidencia}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																	<td width="20%" class="datatable">Celular</td>
																	<td width="30%"></f:verbatim> <h:inputText size="15"
																		id="Celular" maxlength="15"
																		value="#{personalForm.personal.telefonoCelular}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Telf Oficina</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:inputText
																		size="15" id="TelfOficina" maxlength="15"
																		value="#{personalForm.personal.telefonoOficina}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Email</td>
																	<td width="80%" colspan="3"></f:verbatim> <h:inputText
																		size="60" id="Email" maxlength="60"
																		value="#{personalForm.personal.email}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																</tr>
															</table>
															</td>
														</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>
														<tr>
															<td height="2" class="bandtable" colspan="2"></td>
														</tr>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>

														<tr>
															<td width="20%" class="datatable">A�os de Servicio
															en APN antes de Ingreso en Organismo</td>
															<td width="30%">
													</f:verbatim>
													<h:inputText size="3" maxlength="2"
														value="#{personalForm.personal.aniosServicioApn}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<f:verbatim>
														</tr>

														<tr>
															<td height="5" colspan="2"></td>
														</tr>
														<tr>
															<td height="2" class="bandtable" colspan="2"></td>
														</tr>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td colspan="2">
															<table border="0" cellpadding="0" cellspacing="0"
																width="100%">
																<tr>
																	<td width="20%" class="datatable">Tipo Vivienda</td>
																	<td width="30%"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.personal.tipoVivienda}"
																		id="TipoVivienda" disabled="#{!personalForm.editing}"
																		immediate="false" required="false"
																		title="Tipo Vivienda">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.listTipoVivienda}" />
																	</h:selectOneMenu> <f:verbatim></td>
																	<td width="20%" class="datatable">Tenencia
																	Vivienda</td>
																	<td width="30%"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.personal.tenenciaVivienda}"
																		id="TenenciaVivienda"
																		disabled="#{!personalForm.editing}" immediate="false"
																		required="false" title="Tenencia Vivienda">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.listTenenciaVivienda}" />
																	</h:selectOneMenu> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Maneja</td>
																	<td width="30%"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.personal.maneja}" id="Maneja"
																		disabled="#{!personalForm.editing}" immediate="false"
																		required="false" title="Maneja">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems value="#{personalForm.listManeja}" />
																	</h:selectOneMenu> <f:verbatim></td>
																	<td width="20%" class="datatable">Grado Licencia</td>
																	<td width="30%"></f:verbatim> <h:inputText size="10"
																		id="GradoLicencia" maxlength="10"
																		value="#{personalForm.personal.gradoLicencia}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onblur="javascript:fieldEmpty(this)"
																		onkeypress="return keyIntegerCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Veh�culo</td>
																	<td width="30%"></f:verbatim> <h:selectOneMenu
																		value="#{personalForm.personal.tieneVehiculo}"
																		id="TieneVehiculo" disabled="#{!personalForm.editing}"
																		onchange="saveScrollCoordinates();this.form.submit()"
																		immediate="false" required="false" title="Veh�culo">
																		<f:selectItem itemLabel="Seleccione" itemValue="0" />
																		<f:selectItems
																			value="#{personalForm.listTieneVehiculo}" />
																	</h:selectOneMenu> <f:verbatim></td>
																	<td width="20%" class="datatable">Marca</td>
																	<td width="30%"></f:verbatim> <h:inputText size="20" id="Marca"
																		maxlength="20"
																		value="#{personalForm.personal.marcaVehiculo}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		rendered="#{personalForm.showMarcaVehiculoAux}"
																		required="false" /> <f:verbatim></td>
																</tr>
																<tr>
																	<td width="20%" class="datatable">Modelo</td>
																	<td width="30"></f:verbatim> <h:inputText size="20" id="Modelo"
																		maxlength="20"
																		value="#{personalForm.personal.modeloVehiculo}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		rendered="#{personalForm.showModeloVehiculoAux}"
																		required="false" /> <f:verbatim></td>
																	<td width="20%" class="datatable">Placa</td>
																	<td width="30%"></f:verbatim> <h:inputText size="10" id="Placa"
																		maxlength="10"
																		value="#{personalForm.personal.placaVehiculo}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		rendered="#{personalForm.showPlacaVehiculoAux}"
																		required="false" /> <f:verbatim></td>
																</tr>
															</table>
															</td>
														</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>
														<tr>
															<td height="2" class="bandtable" colspan="2"></td>
														</tr>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>
													</f:verbatim>
													<f:verbatim>
														<tr>
															<td colspan="2">
															<table border="0" cellpadding="0" cellspacing="0"
																width="100%">
																<tr>
																	<td width="20%" class="datatable">N�SSO</td>
																	<td width="30%"></f:verbatim> <h:inputText size="15" id="NoSSO"
																		maxlength="15"
																		value="#{personalForm.personal.numeroSso}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																	<td width="20%" class="datatable">N�RIF</td>
																	<td width="30%"></f:verbatim> <h:inputText size="15" id="NoRIF"
																		maxlength="15"
																		value="#{personalForm.personal.numeroRif}"
																		readonly="#{!personalForm.editing}"
																		onchange="javascript:this.value=this.value.toUpperCase()"
																		onkeypress="return keyEnterCheck(event, this)"
																		required="false" /> <f:verbatim></td>
																</tr>
																</f:verbatim>
																<f:verbatim>
																	<tr>
																		<td width="20%" class="datatable">N�Libreta
																		Militar</td>
																		<td width="30%">
																</f:verbatim>
																<h:inputText size="15" id="NoLibretaMilitar"
																	maxlength="15"
																	value="#{personalForm.personal.numeroLibretaMilitar}"
																	readonly="#{!personalForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onkeypress="return keyEnterCheck(event, this)"
																	required="false" />
																<f:verbatim>
																	</td>
																	<td width="20%" class="datatable">Estatura</td>
																	<td width="30%">
																</f:verbatim>
																<h:inputText size="10" id="Estatura" maxlength="10"
																	value="#{personalForm.personal.estatura}"
																	readonly="#{!personalForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onblur="javascript:fieldEmpty(this)"
																	onkeypress="return keyFloatCheck(event, this)"
																	required="false" style="text-align:right">
																	<f:convertNumber minFractionDigits="2"
																		maxFractionDigits="2" groupingUsed="true" />
																</h:inputText>
																<f:verbatim>
																	</td>
																	</tr>
																</f:verbatim>
																<f:verbatim>
																	<tr>
																		<td width="20%" class="datatable">Peso</td>
																		<td width="30%">
																</f:verbatim>
																<h:inputText size="10" id="Peso" maxlength="10"
																	value="#{personalForm.personal.peso}"
																	readonly="#{!personalForm.editing}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onblur="javascript:fieldEmpty(this)"
																	onkeypress="return keyFloatCheck(event, this)"
																	required="false" style="text-align:right">
																	<f:convertNumber minFractionDigits="2"
																		maxFractionDigits="2" groupingUsed="true" />

																</h:inputText>
																<f:verbatim>
																	</td>
																	<td width="20%" class="datatable">Diestralidad</td>
																	<td width="30%">
																</f:verbatim>
																<h:selectOneMenu
																	value="#{personalForm.personal.diestralidad}"
																	id="Diestralidad" disabled="#{!personalForm.editing}"
																	immediate="false" required="false" title="Diestralidad">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems value="#{personalForm.listDiestralidad}" />
																</h:selectOneMenu>
																<f:verbatim>
																	</td>
																	</tr>
																</f:verbatim>
																<f:verbatim>
																	<tr>
																		<td width="20%" class="datatable">Grupo sanguineo
																		</td>
																		<td width="30%">
																</f:verbatim>
																<h:selectOneMenu
																	value="#{personalForm.personal.grupoSanguineo}"
																	id="GrupoSanguineo" disabled="#{!personalForm.editing}"
																	immediate="false" required="false"
																	title="Grupo sanguineo">
																	<f:selectItem itemLabel="Seleccione" itemValue="0" />
																	<f:selectItems
																		value="#{personalForm.listGrupoSanguineo}" />
																</h:selectOneMenu>
																<f:verbatim>
																	</td>
																	</tr>
															</table>
															</td>
														</tr>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>
														<tr>
															<td height="2" class="bandtable" colspan="2"></td>
														</tr>
														<tr>
															<td height="5" colspan="2"></td>
														</tr>
														<!-- WANG-RADARTECH -->
														<tr>
															<td colspan="3" align="center"><!-- SUBIR ARCHIVO FOTO -->
													</f:verbatim>
													<x:div id="divArchivoFoto"
														rendered="#{personalForm.editing}">
														<h:panelGroup rendered="#{personalForm.editing&&!personalForm.showFotoTrabajador}">
															<f:verbatim>
																<iframe id="iFoto" src="./FormularioFoto.jsf"
																	frameborder="no" scrolling="no" width="100%"
																	height="28px" class="archivo"> </iframe>
															</f:verbatim>
														</h:panelGroup>
													</x:div>
													<h:inputHidden id="fotoTrabajador"
														value="#{personalForm.personal.fotoTrabajador}" />
													<h:graphicImage id="imagenTrabajador"
														value="#{personalForm.personal.fotoTrabajador}"
														rendered="#{personalForm.showFotoTrabajador}"></h:graphicImage>
													<h:graphicImage alt="Eliminar Foto" title="Eliminar Foto"
														value="/images/close.gif" id="eliminarFoto"
														style="width:15px;height:15px;vertical-align:top;cursor:pointer;"
														onclick="javascript:dF('viewDataPersonal:personalForm:divArchivoFoto');"
														onmouseover=""
														rendered="#{personalForm.showFotoTrabajador&&personalForm.editing}" />
													<f:verbatim>
													</td>
													</tr>
												</table>
												<table>
													<tr>
														<td></f:verbatim> <h:commandButton value="Modificar"
															image="/images/modify.gif" action="#{personalForm.edit}"
															onclick="javascript:return clickMade()"
															rendered="#{!personalForm.editing&&!personalForm.deleting&&personalForm.login.modificar}" />
														<h:commandButton value="Eliminar"
															image="/images/delete.gif"
															action="#{personalForm.delete}"
															onclick="javascript:return clickMade()"
															rendered="#{!personalForm.editing&&!personalForm.deleting&&personalForm.login.eliminar&&!personalForm.login.servicioPersonal}" />
														<h:outputText value="�Seguro que desea eliminar?"
															rendered="#{personalForm.deleting}" /> <h:commandButton
															value="Si" image="/images/yes.gif"
															action="#{personalForm.deleteOk}"
															onclick="javascript:return clickMade()"
															rendered="#{personalForm.deleting}" /> <h:commandButton
															value="Guardar" image="/images/save.gif"
															action="#{personalForm.save}"
															onclick="javascript:return clickMade()"
															rendered="#{personalForm.editing}" /> <h:commandButton
															value="Cancelar" image="/images/cancel.gif"
															action="#{personalForm.abortUpdate}" immediate="true"
															onclick="javascript:return clickMade()" /> <f:verbatim></td>
													</tr>
												</table>
											</f:verbatim>
										</h:form> <f:verbatim></td>
									</tr>
								</table>
							</f:verbatim>
						</f:subview></td>
					</tr>
				</table></td>
		</tr>
	</table>
</f:view>
</body>
</html>