<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportFamiliares'].elements['formReportFamiliares:reportId'].value;
			var name = 
				document.forms['formReportFamiliares'].elements['formReportFamiliares:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}    	    	
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportFamiliaresForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportFamiliares">
				
    				<h:inputHidden id="reportId" value="#{reportFamiliaresForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportFamiliaresForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reporte Familiares
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    	
    							<tr>
    									<td>
    										Tipo Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFamiliaresForm.idTipoPersonal}"
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                                <f:selectItems value="#{reportFamiliaresForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    										    																	
    								<tr>
    									<td>
    										Regi�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFamiliaresForm.idRegion}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />  
                                                <f:selectItems value="#{reportFamiliaresForm.listRegion}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    								
    								<tr>
    									<td>
    										Forma de Ordenar
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFamiliaresForm.orden}"
    											
    											onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Alfab�tico" itemValue="A" />  
                                            	<f:selectItem itemLabel="C�dula" itemValue="C" />
                                            	<f:selectItem itemLabel="C�digo" itemValue="O" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Agrupado por
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFamiliaresForm.agrupado}"    											
    											onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="General" itemValue="G" />  
                                            	<f:selectItem itemLabel="Dependencia" itemValue="D" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Parentesco
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFamiliaresForm.parentesco}"    											
    											onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Todos" itemValue="0" />                                              	
                                            	<f:selectItem itemLabel="Conyuge" itemValue="C" />  
                                            	<f:selectItem itemLabel="Madre" itemValue="M" />
                                            	<f:selectItem itemLabel="Padre" itemValue="P" />
                                 				<f:selectItem itemLabel="Hijo(a)" itemValue="H" />
                                 				<f:selectItem itemLabel="Hermano(a)" itemValue="E" />
                                            	<f:selectItem itemLabel="Suegro(a)" itemValue="S" />                                 		
                                            	<f:selectItem itemLabel="Abuelo(a)" itemValue="A" />                                            	
                                            	<f:selectItem itemLabel="Sobrino(a)" itemValue="S" />                                            	
                                            	<f:selectItem itemLabel="Tio(a)" itemValue="I" />                                            	
                                            	<f:selectItem itemLabel="Tutelado" itemValue="U" />                                            	
                                            	<f:selectItem itemLabel="Otro" itemValue="O" />                                            	
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td width="20%">
    										Fecha Tope
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText    											
    											size="10"
    											id="FechaTope"
    											maxlength="10"
    											onblur="javascript:check_date(this)"
    											value="#{reportFamiliaresForm.fechaTope}"
    											required="false" >    										
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		pattern="dd-MM-yyyy" />
                	                        </h:inputText>
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Edad M�nima
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="EdadMinima"
    											maxlength="3"
    											value="#{reportFamiliaresForm.edadMinima}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Edad M�xima
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="EdadMaxima"
    											maxlength="3"
    											value="#{reportFamiliaresForm.edadMaxima}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportFamiliaresForm.formato}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
    											action="#{reportFamiliaresForm.runReport}"
    											onclick="windowReport()" />
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>