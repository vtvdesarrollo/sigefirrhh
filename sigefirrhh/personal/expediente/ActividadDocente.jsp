<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">

<f:view>
	<x:saveState value="#{actividadDocenteForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                <% if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
					<jsp:include page="/inc/menuServicioPersonal.jsp" />

				<%}%>
				</div>
			</td>
			<td width="570" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<h:form id="formActividadDocente">
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<b>
												Actividades Docentes
											</b>
										</td>
										<td align="right">
											<a href="#" onclick="window.open
												('/sigefirrhh//help/sigefirrhh/personal/expediente/ActividadDocente.jsp',
												'helpwindow',
												'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
												<h:graphicImage  url="/images/help/help.gif" />
											</a>
										</td>
									</tr>
								</table>
								<table width="100%" class="toptable">
									<tr>
										<td align="left">
											<h:messages errorClass="error" styleClass="success" />
										</td>
									</tr>
									
									<f:subview 
										id="searchActividadDocente"
										rendered="#{!actividadDocenteForm.showData&&
										!actividadDocenteForm.showAdd&&
										!actividadDocenteForm.login.servicioPersonal}">
										<f:verbatim><tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Buscar Trabajador
														</td>
													</tr>
												</table>
												<table width="100%" class="datatable"></f:verbatim>
        		                					<f:verbatim><tr>
                		        						<td>
                        									C�dula
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="10"
																maxlength="10"
																value="#{actividadDocenteForm.findPersonalCedula}"
																onkeypress="return keyIntegerCheck(event, this)"
																onfocus="return deleteZero(event, this)"
																onblur="javascript:fieldEmpty(this)"
																onchange="javascript:this.value=this.value.toUpperCase()" />
															<h:commandButton image="/images/find.gif" 
																action="#{actividadDocenteForm.findPersonalByCedula}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
													<tr>
                        								<td>
                        									Nombres
		                        						</td>
        		                						<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{actividadDocenteForm.findPersonalPrimerNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()"
																onkeypress="return keyEnterCheck(event, this)" />
															<h:inputText size="20"
																maxlength="20"
																value="#{actividadDocenteForm.findPersonalSegundoNombre}"
																onchange="javascript:this.value=this.value.toUpperCase()" 
																onkeypress="return keyEnterCheck(event, this)"/>
														<f:verbatim></td>
													</tr>
													<tr>
        		                						<td>
                		        							Apellidos
                        								</td>
                        								<td></f:verbatim>
															<h:inputText size="20"
																maxlength="20"
																value="#{actividadDocenteForm.findPersonalPrimerApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()" 
																onkeypress="return keyEnterCheck(event, this)"/>
															<h:inputText size="20"
																maxlength="20"
																value="#{actividadDocenteForm.findPersonalSegundoApellido}"
																onchange="javascript:this.value=this.value.toUpperCase()" 
																onkeypress="return keyEnterCheck(event, this)"/>
															<h:commandButton image="/images/find.gif" 
																action="#{actividadDocenteForm.findPersonalByNombresApellidos}"
																onclick="javascript:return clickMade()" />
														<f:verbatim></td>
													</tr>
												</table>
											</td>
										</tr></f:verbatim>
									</f:subview>
								<f:verbatim></table></f:verbatim>

								<f:subview 
									id="viewResultPersonal"
									rendered="#{actividadDocenteForm.showResultPersonal&&
									!actividadDocenteForm.showData&&
									!actividadDocenteForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
    											<table width="100%" class="bandtable">
    												<tr>
    													<td>
    														Resultado de Personal
    													</td>
	    											</tr>
    											</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
                        	                    <h:dataTable id="tablePersonal"
                            	                    styleClass="datatable"
                                	                headerClass="standardTable_Header"
                                    	            footerClass="standardTable_Header"
                                        	        rowClasses="standardTable_Row1,standardTable_Row2"
                                            	    columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                	var="resultPersonal"
	                                				value="#{actividadDocenteForm.resultPersonal}"
    	                                            
        	                                        rows="10"
	                                                width="100%">
    	                            				<h:column>
	                                					<h:commandLink value="#{resultPersonal}"
    	                            						action="#{actividadDocenteForm.findActividadDocenteByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{resultPersonal.idPersonal}" />
                	                					</h:commandLink>
                    	            				</h:column>
                        	                    </h:dataTable>
	                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
	                                                <x:dataScroller id="scroll_1"
    	                                                for="tablePersonal"
        	                                            fastStep="10"
            	                                        pageCountVar="pageCount"
                	                                    pageIndexVar="pageIndex">
        	        	                                <f:facet name="first" >
    	                	                            	<h:graphicImage url="/images/arrow-first.gif" />
	                        	                        </f:facet>
                                	                	<f:facet name="last">
                                    	        	    	<h:graphicImage url="/images/arrow-last.gif"/>
                                        		        </f:facet>
                                    	    	    	<f:facet name="previous">
                                	            			<h:graphicImage url="/images/arrow-previous.gif" />
	                            	                	</f:facet>
    	                    	                    	<f:facet name="next">
        	            	                        		<h:graphicImage url="/images/arrow-next.gif" />
            	    	                            	</f:facet>
            		                                	<f:facet name="fastforward">
        	        	                            		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                	                        	</f:facet>
	                        	                    	<f:facet name="fastrewind">
                                	            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                    	        		</f:facet>
                                        	    	</x:dataScroller>
	                                            	<x:dataScroller id="scroll_2"
	    	                                        	for="tablePersonal"
    	    	                                    	pageCountVar="pageCount"
        	    	                                	pageIndexVar="pageIndex" >
                	                        	    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
															<f:param value="#{pageIndex}" />
                        	        	            		<f:param value="#{pageCount}" />
	                            	                	</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewSelectedPersonal"
									rendered="#{actividadDocenteForm.selectedPersonal&&
									!actividadDocenteForm.showData&&
									!actividadDocenteForm.editing&&
									!actividadDocenteForm.deleting}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
            								<td>
												Trabajador:&nbsp;</f:verbatim>

            									<h:commandLink value="#{actividadDocenteForm.personal}   <-- clic aqui"
    	                            						action="#{actividadDocenteForm.findActividadDocenteByPersonal}"
        	                        						styleClass="listitem">
            	                    						<f:param name="idPersonal" value="#{actividadDocenteForm.personal.idPersonal}" />
                	                					</h:commandLink>
	            							<f:verbatim></td>
										</tr>
										<tr>
											<td>
												<table width="100%" class="bandtable">
    												<tr>
    													<td style="text-align: right"></f:verbatim>
            												<h:commandButton image="/images/add.gif"
            													action="#{actividadDocenteForm.add}"
            													onclick="javascript:return clickMade()"
            													rendered="#{!actividadDocenteForm.editing&&!actividadDocenteForm.deleting&&actividadDocenteForm.login.agregar}" />

                                        	    			<% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
                                        	    			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{actividadDocenteForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />
                            	        					 
                                                    	    <%}%>
                                                    	    <f:verbatim>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></f:verbatim>
								</f:subview>
								<f:subview 
									id="viewResultActividadDocente"
									rendered="#{actividadDocenteForm.showResult&&
									!actividadDocenteForm.showData&&
									!actividadDocenteForm.showAdd}">
									<f:verbatim><table class="toptable" width="100%">
										<tr>
											<td>
												<table width="100%" class="bandtable">
													<tr>
														<td>
															Resultado de B�squeda
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></f:verbatim>
            	                                <h:dataTable id="result"
                	                                styleClass="datatable"
                    	                            headerClass="standardTable_Header"
                        	                        footerClass="standardTable_Header"
                            	                    rowClasses="standardTable_Row1,standardTable_Row2"
                                	                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                    	            var="result"
                                					value="#{actividadDocenteForm.result}"

                                                	rows="10"
	                                                width="100%">
    	                            				<h:column>
                                						<h:commandLink value="#{result}"
                                							action="#{actividadDocenteForm.selectActividadDocente}"
	                                						styleClass="listitem">
    		                            						<f:param name="idActividadDocente" value="#{result.idActividadDocente}" />
            	                    					</h:commandLink>
                	                				</h:column>
                    	                        </h:dataTable>
                        	                    <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                            	                    <x:dataScroller id="scroll_3"
                                	                    for="result"
                                    	                fastStep="10"
                                        	            pageCountVar="pageCount"
                                            	        pageIndexVar="pageIndex">
        	                                    	    <f:facet name="first" >
    	                                            		<h:graphicImage url="/images/arrow-first.gif" />
		                                                </f:facet>
    	                                            	<f:facet name="last">
        	                                    	    	<h:graphicImage url="/images/arrow-last.gif"  />
            	                            	        </f:facet>
                	                    	        	<f:facet name="previous">
                    	            	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                        	    	                	</f:facet>
                        		                    	<f:facet name="next">
                    	        	                		<h:graphicImage url="/images/arrow-next.gif" />
                	                	            	</f:facet>
            	                        	        	<f:facet name="fastforward">
        	                                	    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        		</f:facet>
	                                            		<f:facet name="fastrewind">
                                            				<h:graphicImage url="/images/arrow-fr.gif"  />
	                                            		</f:facet>
    	                                        	</x:dataScroller>
	    	                                        <x:dataScroller id="scroll_4"
    	    	                                    	for="result"
        	    	                                	pageCountVar="pageCount"
            	    	                            	pageIndexVar="pageIndex" >
                	    		                    	<h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        			            	<f:param value="#{pageIndex}" />
                        	        			            <f:param value="#{pageCount}" />
	                            	            		</h:outputFormat>
                                        	    	</x:dataScroller>
                                            	</h:panelGrid>
                            				<f:verbatim></td>
                    					</tr>
                    				</table></f:verbatim>
								</f:subview>
							</h:form>
							<f:subview 
								id="viewDataActividadDocente"
								rendered="#{actividadDocenteForm.showData||actividadDocenteForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
											<table width="100%" class="bandtable">
												<tr>
													<td></verbatim>
														<h:outputText value="Consultando"
															rendered="#{!actividadDocenteForm.editing&&!actividadDocenteForm.deleting}" />
														<h:outputText value="Modificando"
															rendered="#{actividadDocenteForm.editing&&!actividadDocenteForm.adding}" />
														<h:outputText value="Eliminando"
															rendered="#{actividadDocenteForm.deleting}" />
														<h:outputText value="Agregando"
															rendered="#{actividadDocenteForm.adding}" />
													<verbatim></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:form id="actividadDocenteForm">
	                                            <h:inputHidden id="scrollx" value="#{actividadDocenteForm.scrollx}" />
    	                                        <h:inputHidden id="scrolly" value="#{actividadDocenteForm.scrolly}" />
	    	        							<f:subview 
    	    	    								id="dataActividadDocente"
        	    									rendered="#{actividadDocenteForm.showData}">
	        	    								<f:verbatim><table class="datatable" width="100%">
    	        	    	        					<tr>
	    	        	    	        					<td colspan="2">
            	        	    	            				<table align="center">
                	        	    	        					<tr>
                    	        	    	    						<td></f:verbatim>
    	                        	                        				<h:commandButton value="Modificar" 
        	                        	                    					image="/images/modify.gif"
            	                        	                					action="#{actividadDocenteForm.edit}"
            	                        	                					onclick="javascript:return clickMade()"
                	                        	            					rendered="#{!actividadDocenteForm.editing&&!actividadDocenteForm.deleting&&actividadDocenteForm.login.modificar}" />
                    	                        	        				<h:commandButton value="Eliminar"
                        	                        	    					image="/images/delete.gif"
                            	                        						action="#{actividadDocenteForm.delete}"
                            	                        						onclick="javascript:return clickMade()"
                                	                    						rendered="#{!actividadDocenteForm.editing&&!actividadDocenteForm.deleting&&actividadDocenteForm.login.eliminar}" />
                                    	                					<h:outputText
                                        	            						value="�Seguro que desea eliminar?"
                                            	        						rendered="#{actividadDocenteForm.deleting}" />
    		                                                				<h:commandButton value="Si" 
        		                                            					image="/images/yes.gif"
            		                                        					action="#{actividadDocenteForm.deleteOk}"
            		                                        					onclick="javascript:return clickMade()"
                		                                    					rendered="#{actividadDocenteForm.deleting}" />
    	                	                                				<h:commandButton value="Guardar" 
        	                	                            					image="/images/save.gif"
            	                	                        					action="#{actividadDocenteForm.save}"
            	                	                        					onclick="javascript:return clickMade()"
                	                	            	        				rendered="#{actividadDocenteForm.editing}" />
																			<h:commandButton value="Cancelar" 
			                                    								image="/images/cancel.gif"
                                    											action="#{actividadDocenteForm.abortUpdate}"
                                    											immediate="true"
                                    											onclick="javascript:return clickMade()"
                            	        										/>
    	                                	                                <f:verbatim>
	    	        	                                    			</td>
	        	        	                                		</tr>
    	        												</table>
															</td>
														</tr></f:verbatim>
														<f:verbatim><tr>
                    	    								<td>
            													Trabajador
                        									</td>
	                        								<td></f:verbatim>
	    	                    								<h:outputText value="#{actividadDocenteForm.personal}" />
    	    	                							<f:verbatim></td>
	            										</tr></f:verbatim>
																																															                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Nivel Educativo
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{actividadDocenteForm.actividadDocente.nivelEducativo}"
        	                                        	        	disabled="#{!actividadDocenteForm.editing}"
            	                                        	    	immediate="false"
                	            									onchange="saveScrollCoordinates();this.form.submit()"                    	        										                        	                                	required="true"
    	                        	                            	title="Nivel Educativo">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{actividadDocenteForm.listNivelEducativo}" />
																						<f:validator validatorId="eforserver.RequiredJSFValidator"/>
					    	            	                        	            </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>				        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									A�o Inicio
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="4"
	    	    		                							maxlength="4"
            	    		        								value="#{actividadDocenteForm.actividadDocente.anioInicio}"
                	    		    								                    	    										readonly="#{!actividadDocenteForm.editing}"
                        											id="AnioInicio"
                        											required="true"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyIntegerCheck(event, this)"
                        											onblur="javascript:fieldEmpty(this)"
																	 >
								                        									</h:inputText>
                        			                        								<f:verbatim>(aaaa)<span class="required"> *</span></f:verbatim>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									A�o Final
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="4"
	    	    		                							maxlength="4"
            	    		        								value="#{actividadDocenteForm.actividadDocente.anioFin}"
                	    		    								                    	    										readonly="#{!actividadDocenteForm.editing}"
                        											id="AniooFinal"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyFloatCheck(event, this)"
                        											onblur="javascript:fieldEmpty(this)"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Actualmente
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{actividadDocenteForm.actividadDocente.estatus}"
        	                                        	        	disabled="#{!actividadDocenteForm.editing}"
            	                                        	    	immediate="false"
                	            									                    	        										                        	                                	required="true"
    	                        	                            	title="Actualmente">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{actividadDocenteForm.listEstatus}" />
																						<f:validator validatorId="eforserver.RequiredJSFValidator"/>
					    	            	                        	            </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>				        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Sector
	            	            							</td>
		                        							<td></f:verbatim>
				    	                                        	            <h:selectOneMenu value="#{actividadDocenteForm.actividadDocente.sector}"
        	                                        	        	disabled="#{!actividadDocenteForm.editing}"
            	                                        	    	immediate="false"
                	            									                    	        										                        	                                	required="false"
    	                        	                            	title="Sector">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
            	                        	                        <f:selectItems value="#{actividadDocenteForm.listSector}" />
					    	            	                        	            </h:selectOneMenu>				        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Entidad Educativa
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="90"
	    	    		                							maxlength="90"
            	    		        								value="#{actividadDocenteForm.actividadDocente.nombreEntidad}"
                	    		    								required="true"                  	    										readonly="#{!actividadDocenteForm.editing}"
                        											id="EntidadEducativa"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Asignatura
	            	            							</td>
		                        							<td></f:verbatim>
																	<h:inputText
		    		                    							size="60"
	    	    		                							maxlength="60"
            	    		        								value="#{actividadDocenteForm.actividadDocente.asignatura}"
                	    		    								                    	    										readonly="#{!actividadDocenteForm.editing}"
                        											id="Asignatura"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Relaci�n Laboral
	            	            							</td>
		                        							<td></f:verbatim>
																							                        		<h:inputText
		    		                    							size="100"
	    	    		                							maxlength="100"
            	    		        								value="#{actividadDocenteForm.actividadDocente.relacionLaboral}"
                	    		    								                    	    										readonly="#{!actividadDocenteForm.editing}"
                        											id="RelacionLaboral"
                        											required="false"
                        											title="CampoX"
                        											alt="CampoX"
                        											onchange="javascript:this.value=this.value.toUpperCase()"
                        											onkeypress="return keyEnterCheck(event, this)"
																	 >
								                        									</h:inputText>
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Carrera
	            	            							</td>
		                        							<td></f:verbatim>
									    		                                                    <h:selectOneMenu value="#{actividadDocenteForm.selectCarrera}"
        		                                                	disabled="#{!actividadDocenteForm.editing}"
            		                                            	immediate="false"
																	rendered="#{actividadDocenteForm.showCarreraAux}"                  		            								                    		        								                        		                                	
                		            								required="false"
    																title="Carrera">
    																<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                    		                    	<f:selectItems value="#{actividadDocenteForm.colCarrera}" />
								                                    	                </h:selectOneMenu>									        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																	                    	    						<f:verbatim><tr>
    	                    								<td>
        	                									Observaciones
	            	            							</td>
		                        							<td></f:verbatim>
															    							                        		<h:inputTextarea
    			                        							cols="100"
    	    		                    							rows="4"
    	        		                							id="Observaciones"
                    		        								                        		    								value="#{actividadDocenteForm.actividadDocente.observaciones}"
                            										readonly="#{!actividadDocenteForm.editing}"
                            										onchange="javascript:this.value=this.value.toUpperCase()"
                            										onkeypress="return keyEnterCheck(event, this)"
                            										required="false" />
                        																        	            	    							<f:verbatim></td>
	        	            	    					</tr></f:verbatim>
																																						<f:verbatim></table></f:verbatim>
												</f:subview>
            									<f:subview 
            										id="commandsActividadDocente"
            										rendered="#{actividadDocenteForm.showData}">
		                        					<f:verbatim><table>
    		                    						<tr>
        		                							<td></f:verbatim>
            	                            					<h:commandButton value="Modificar" 
                	                        						image="/images/modify.gif"
                    	                    						action="#{actividadDocenteForm.edit}"
                    	                    						onclick="javascript:return clickMade()"
                        	                						rendered="#{!actividadDocenteForm.editing&&!actividadDocenteForm.deleting&&actividadDocenteForm.login.modificar}" />
	                        	                				<h:commandButton value="Eliminar"
    	                        	            					image="/images/delete.gif"
        	                        	        					action="#{actividadDocenteForm.delete}"
        	                        	        					onclick="javascript:return clickMade()"
            	                        	    					rendered="#{!actividadDocenteForm.editing&&!actividadDocenteForm.deleting&&actividadDocenteForm.login.eliminar}" />
                                            					<h:outputText
                                            						value="�Seguro que desea eliminar?"
                                            						rendered="#{actividadDocenteForm.deleting}" />
                                    				
                                            					<h:commandButton value="Si" 
                                            						image="/images/yes.gif"
                                            						action="#{actividadDocenteForm.deleteOk}"
                                            						onclick="javascript:return clickMade()"
	                                            					rendered="#{actividadDocenteForm.deleting}" />
                                            			
    	                                        				<h:commandButton value="Guardar" 
        	                                    					image="/images/save.gif"
            	                                					action="#{actividadDocenteForm.save}"
            	                                					onclick="javascript:return clickMade()"
                	                    	        				rendered="#{actividadDocenteForm.editing}" />	
                	                    	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{actividadDocenteForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							 />	
        	        	                            			<f:verbatim>
                                    	        			</td>
	                                    	    		</tr>
    												</table></f:verbatim>
	    										</f:subview>
											</h:form>
        	                            <f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</f:view>

</body>
</html>