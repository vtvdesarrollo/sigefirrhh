<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags"%>
<%@ page import="sigefirrhh.login.LoginSession"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x"%>

<tags:seguridad />
<%
	if (!((LoginSession) session.getAttribute("loginSession"))
			.isValid()) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="/sigefirrhh/style/webstyle.css"
	type="text/css" />
<script type="text/javascript">
function o(f){
    var v=window.document.getElementById(f);
    if (v) return v;
    else return null;
}
function vf(f,fm){
    var ft=document.getElementById(f);
    if(ft.value.search(/\S/g) == -1){
        alert('No ha seleccionado ningun archivo para enviar.');
    }
    var fl = ft.value.match(/[^\/\\]+\.(?:png)$/i)
    if(fl != null){
        fl = fl.toString().replace(/\.png$/i,"")
        if((/["']|\-{4,}|\.{1,}|\s/i).test(fl)){
            alert("El nombre del archivo no debe tener comillas simples o doble, puntos, espacios"+
                "\no m�s de tres guiones en el nombre del archivo.\t\n")
        }
        var f=document.getElementById(fm);
        f.submit();
    }
    else{
        alert("Formato de imagen no permitido, solo se permiten imagenes del tipo PNG.");
    }
}
</script>
</head>
<body>
<f:view>
	<x:saveState value="#{subirFoto}" />
	<h:form id="MyForm" enctype="multipart/form-data" style="background-color: #F7F7FF;">
		<h:messages errorClass="error" styleClass="success" />
		<h:panelGrid columns="3" border="0" cellspacing="0"
			styleClass="datatable">
			<h:outputLabel for="archivo" value="Foto del Trabajador: "
				styleClass="datatable" />
			<x:inputFileUpload id="myFileId" value="#{subirFoto.myFile}"
				storage="file" required="true" styleClass="datatable" />
			<h:commandButton value="Enviar" action="#{subirFoto.processMyFile}" />
		</h:panelGrid>
	</h:form>
</f:view>
</body>
</html>
