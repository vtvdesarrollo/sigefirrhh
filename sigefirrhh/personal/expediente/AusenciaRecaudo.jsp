<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{ausenciaRecaudoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formAusenciaRecaudo">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Recaudos de Ausencias
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/expediente/AusenciaRecaudo.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{ausenciaRecaudoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!ausenciaRecaudoForm.editing&&!ausenciaRecaudoForm.deleting&&ausenciaRecaudoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchAusenciaRecaudo"
								rendered="#{!ausenciaRecaudoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
										<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataAusenciaRecaudo"
								rendered="#{ausenciaRecaudoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!ausenciaRecaudoForm.editing&&!ausenciaRecaudoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{ausenciaRecaudoForm.editing&&!ausenciaRecaudoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{ausenciaRecaudoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{ausenciaRecaudoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="ausenciaRecaudoForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{ausenciaRecaudoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!ausenciaRecaudoForm.editing&&!ausenciaRecaudoForm.deleting&&ausenciaRecaudoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{ausenciaRecaudoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!ausenciaRecaudoForm.editing&&!ausenciaRecaudoForm.deleting&&ausenciaRecaudoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{ausenciaRecaudoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{ausenciaRecaudoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{ausenciaRecaudoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{ausenciaRecaudoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{ausenciaRecaudoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{ausenciaRecaudoForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha Recaudo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="fechaRecaudo"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{ausenciaRecaudoForm.ausenciaRecaudo.fechaRecaudo}"
                        								readonly="#{!ausenciaRecaudoForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onblur="javascript:check_date(this)"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
													<h:message for="fechaRecaudo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Relacion Recaudo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{ausenciaRecaudoForm.selectRelacionRecaudo}"
														id="relacionRecaudo"
                                                    	disabled="#{!ausenciaRecaudoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Relacion Recaudo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{ausenciaRecaudoForm.colRelacionRecaudo}" />
                                                    </h:selectOneMenu>													<h:message for="relacionRecaudo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Ausencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{ausenciaRecaudoForm.selectAusencia}"
														id="ausencia"
                                                    	disabled="#{!ausenciaRecaudoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Ausencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{ausenciaRecaudoForm.colAusencia}" />
                                                    </h:selectOneMenu>													<h:message for="ausencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{ausenciaRecaudoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!ausenciaRecaudoForm.editing&&!ausenciaRecaudoForm.deleting&&ausenciaRecaudoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{ausenciaRecaudoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!ausenciaRecaudoForm.editing&&!ausenciaRecaudoForm.deleting&&ausenciaRecaudoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{ausenciaRecaudoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{ausenciaRecaudoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{ausenciaRecaudoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{ausenciaRecaudoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{ausenciaRecaudoForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{ausenciaRecaudoForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>