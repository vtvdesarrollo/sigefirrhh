<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportNominaHistorico'].elements['formReportNominaHistorico:reportId'].value;
			var name = 
				document.forms['formReportNominaHistorico'].elements['formReportNominaHistorico:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportNominaHistoricoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">               
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportNominaHistorico">
				
    				<h:inputHidden id="reportId" value="#{reportNominaHistoricoForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportNominaHistoricoForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reportes de Hist�rico de Nominas
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Grupo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaHistoricoForm.selectGrupoNomina}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{reportNominaHistoricoForm.changeGrupoNomina}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaHistoricoForm.listGrupoNomina}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>										
    								<tr>
    									<td>
    										Tipo de N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaHistoricoForm.tipoNomina}"	    										
	    										onchange="this.form.submit()"
                                            	immediate="true"
                                            	valueChangeListener="#{reportNominaHistoricoForm.changeTipoNomina}">
                                            	<f:selectItem itemLabel="Ordinaria" itemValue="O" />  
                                            	<f:selectItem itemLabel="Especial" itemValue="E" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										N�mina Especial
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaHistoricoForm.selectNominaEspecial}"
                                            	immediate="true"
                                            	onchange="this.form.submit()" 
                                            	rendered="#{reportNominaHistoricoForm.showGeneral&&reportNominaHistoricoForm.showNominaEspecial}" 
                                            	valueChangeListener="#{reportNominaHistoricoForm.changeNominaEspecial}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaHistoricoForm.listNominaEspecial}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											maxlength="4"
    											rendered="#{reportNominaHistoricoForm.showPeriodo}"    											
    											id="Anio"
    											value="#{reportNominaHistoricoForm.anio}"
    										/>    								
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="3"
    											maxlength="2"
    											rendered="#{reportNominaHistoricoForm.showPeriodo}"    											
    											id="Mes"
    											value="#{reportNominaHistoricoForm.mes}"
    										/>    								
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Quincena
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="2"
    											maxlength="1"
    											rendered="#{reportNominaHistoricoForm.showQuincena}"    											
    											id="Quincena"
    											value="#{reportNominaHistoricoForm.quincena}"
    										/>    								
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Semana del A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="3"
    											maxlength="2"
    											rendered="#{reportNominaHistoricoForm.showSemana}"    											
    											id="Semana"
    											value="#{reportNominaHistoricoForm.semana}"
    										/>    								
    									</td>
									</tr>
									<tr>
    									<td>
    										Unidad Administradora
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaHistoricoForm.idUnidadAdministradora}"
	    										rendered="#{reportNominaHistoricoForm.showGeneral}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{reportNominaHistoricoForm.listUnidadAdministradora}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
									
    								<tr>
    									<td>
    										Criterios
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaHistoricoForm.tipoReporte}"
	    										rendered="#{reportNominaHistoricoForm.showGeneral}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  
                                            	<f:selectItem itemLabel="Nomina por Codigo" itemValue="1" />
                                            	<f:selectItem itemLabel="Nomina Alfabetico" itemValue="2" />
                                            	<f:selectItem itemLabel="Nomina por Programa" itemValue="24" /> 
                                            	<f:selectItem itemLabel="Nomina por UEL" itemValue="3" /> 
                                            	<f:selectItem itemLabel="Nomina por Region" itemValue="22" />
                                            	<f:selectItem itemLabel="Nomina por Dependencia" itemValue="23" />                                            	
                                            	<f:selectItem itemLabel="Detalle de Conceptos Alfab�tico" itemValue="4" />
                                            	<f:selectItem itemLabel="Detalle de Conceptos por C�digo" itemValue="28" />
                                            	<f:selectItem itemLabel="Detalle de Conceptos por C�dula" itemValue="29" />
                                            	<f:selectItem itemLabel="Resumen de Conceptos General" itemValue="5" />
                                            	<f:selectItem itemLabel="Resumen de Conceptos por UEL" itemValue="6" />                                              	
                                            	<f:selectItem itemLabel="Resumen de Conceptos con aportes" itemValue="15" />
                                            	<f:selectItem itemLabel="Resumen de Conceptos con aportes por UEL" itemValue="16" />                                              	                                              	
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Unidad Administradora" itemValue="20" />                                   				
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Categoria Presupuestaria" itemValue="30" />                                   				                                            	
    											<f:selectItem itemLabel="Dep�sitos Bancarios Alfab�tico" itemValue="7" /> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por C�digo" itemValue="25" /> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por C�dula" itemValue="26" />  
    											<f:selectItem itemLabel="Dep�sitos Bancarios por Unidad Administradora" itemValue="27" /> 
    											<f:selectItem itemLabel="Listado de Cheques" itemValue="8" />
    											<f:selectItem itemLabel="Netos a Pagar" itemValue="32" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Unidad Administradora" itemValue="9" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Lugar de Pago" itemValue="10" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Dependencia" itemValue="21" />
    											<f:selectItem itemLabel="Recibos de Pago Alfab�tico" itemValue="100" />
    											<f:selectItem itemLabel="Recibos de Pago 2pp Carta Alfab�tico" itemValue="101" />
    											<f:selectItem itemLabel="Recibos de Pago 3pp Carta Alfab�tico" itemValue="102" />
    											<f:selectItem itemLabel="Recibos de Pago 4pp Carta Alfab�tico" itemValue="103" />
    											<f:selectItem itemLabel="Recibos de Pago por C�digo" itemValue="104" />
    											<f:selectItem itemLabel="Recibos de Pago 2pp Carta por C�digo" itemValue="105" />
    											<f:selectItem itemLabel="Recibos de Pago 3pp Carta por C�digo" itemValue="106" />
    											<f:selectItem itemLabel="Recibos de Pago 4pp Carta por C�digo" itemValue="107" />
    											<f:selectItem itemLabel="Recibos de Pago por C�dula" itemValue="108" />
    											<f:selectItem itemLabel="Recibos de Pago 2pp Carta por C�dula" itemValue="109" />
    											<f:selectItem itemLabel="Recibos de Pago 3pp Carta por C�dula" itemValue="110" />
    											<f:selectItem itemLabel="Recibos de Pago 4pp Carta por C�dula" itemValue="111" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	    
    														
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										rendered="#{reportNominaHistoricoForm.showGeneral}"
    											action="#{reportNominaHistoricoForm.runReport}"
    											onclick="windowReport()" />
    								
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>