<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{historicoConceptoTrabajadorForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formHistoricoConcepto">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Hist�rico Conceptos por Trabajador
										</b>
									</td>
									
								</tr>
								
								
								<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						    </td>
								
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchHistorico"
								rendered="#{!historicoConceptoTrabajadorForm.showTrabajador}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
											<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{historicoConceptoTrabajadorForm.findSelectTrabajadorIdTipoPersonal}"
					                            		onchange="this.form.submit()"
					                            		valueChangeListener="#{historicoConceptoTrabajadorForm.changeTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoConceptoTrabajadorForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{historicoConceptoTrabajadorForm.findTrabajadorCedula}"
														onkeypress="return keyIntegerCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{historicoConceptoTrabajadorForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{historicoConceptoTrabajadorForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{historicoConceptoTrabajadorForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{historicoConceptoTrabajadorForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{historicoConceptoTrabajadorForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{historicoConceptoTrabajadorForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{historicoConceptoTrabajadorForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{historicoConceptoTrabajadorForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{historicoConceptoTrabajadorForm.showResultTrabajador&&!historicoConceptoTrabajadorForm.selectedTrabajador&&
								!historicoConceptoTrabajadorForm.showData&&
								!historicoConceptoTrabajadorForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{historicoConceptoTrabajadorForm.resultTrabajador}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{historicoConceptoTrabajadorForm.selectTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif" />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif" />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{historicoConceptoTrabajadorForm.selectedTrabajador&&historicoConceptoTrabajadorForm.showTrabajador}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{historicoConceptoTrabajadorForm.trabajador}"
                            						action="#{historicoConceptoTrabajadorForm.findConceptoFijoByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{historicoConceptoTrabajadorForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
									<tr>
										<td>
											<table width="100%" class="bandtable">
    											<tr>
    												<td style="text-align: right"></f:verbatim>            											
            											<h:commandButton value="Cancelar" 
		                                    				image="/images/cancel.gif"
		                        							action="#{historicoConceptoTrabajadorForm.abort}"
		                        							immediate="true"
		                        							onclick="javascript:return clickMade()"
		                	        							 />
                                            			<f:verbatim>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview id="busquedaConcepto" rendered="#{historicoConceptoTrabajadorForm.selectedTrabajador}">
							<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
								<tr>
									<!-- B�squeda Concepto -->
									<td valign="top">							
										<table width="100%" class="toptable">															
											<f:verbatim><tr>
												<td>
													<table width="100%" class="bandtable">
														<tr>
															<td>
																B�squeda de Conceptos
															</td>
														</tr>
													</table>
													
													<table width="100%" class="datatable">
														<tr>
			                        						<td>
			                        							A�o
			                        						</td>
			                        						<td></f:verbatim>
																<h:inputText size="4"
																	maxlength="5"
																	onchange="this.form.submit()"
																	value="#{historicoConceptoTrabajadorForm.anio}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onfocus="return deleteZero(event, this)"
																	onkeypress="return keyEnterCheck(event, this)" />
																	
															<f:verbatim></td>
														</tr>															
														<tr>
			                        						<td>
			                        							Concepto
			                        						</td>
			                        						<td></f:verbatim>
								                            	<h:selectOneMenu value="#{historicoConceptoTrabajadorForm.idConceptoTipoPersonal}"
								                            	onchange="this.form.submit()"								                   
					                            				valueChangeListener="#{historicoConceptoTrabajadorForm.changeConceptoTipoPersonal}"
			                                                    	immediate="false">
																	<f:selectItem itemLabel="Todos" itemValue="0" />
			                                                        <f:selectItems value="#{historicoConceptoTrabajadorForm.colConceptoTipoPersonal}" />
			                                                    </h:selectOneMenu>
																
															<f:verbatim></td>
														</tr>																												
														<tr>
			                        						<td>
			                        							Mes
			                        						</td>
			                        						<td></f:verbatim>
																<h:inputText size="2"
																	maxlength="3"																	
																	value="#{historicoConceptoTrabajadorForm.mes}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onfocus="return deleteZero(event, this)"
																	onkeypress="return keyEnterCheck(event, this)" />																
															<f:verbatim></td>
														</tr>													
														
														<tr>
			                        						<td>
			                        							Semana/Quincena
			                        						</td>
			                        						<td></f:verbatim>
																<h:inputText size="2"
																	maxlength="3"																	
																	value="#{historicoConceptoTrabajadorForm.semanaQuincena}"
																	onchange="javascript:this.value=this.value.toUpperCase()"
																	onfocus="return deleteZero(event, this)"
																	onkeypress="return keyEnterCheck(event, this)" />																
															<f:verbatim></td>
														</tr>	
																									
					    								<tr>
			                        						
			                        						<td></f:verbatim>																
																	<h:commandButton image="/images/find.gif" 
																	action="#{historicoConceptoTrabajadorForm.findHistorico}"
																	onclick="javascript:return clickMade()" />
															<f:verbatim></td>
														
													</table>
												</td>
											</tr>
							</table></f:verbatim>
							
							</f:subview>
							<f:subview 
								id="viewResultConceptoFijo"
								rendered="#{historicoConceptoTrabajadorForm.showResult&&
								!historicoConceptoTrabajadorForm.showData&&
								!historicoConceptoTrabajadorForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda 
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{historicoConceptoTrabajadorForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:outputText value="#{result}" />
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif" />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif" />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif" />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
						</h:form>							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>