<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<script language=javascript>  

		function windowReport() {
			var number = 
				document.forms['formReportHistoricoAdministradora'].elements['formReportHistoricoAdministradora:reportId'].value;
			var name = 
				document.forms['formReportHistoricoAdministradora'].elements['formReportHistoricoAdministradora:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
	</script>
	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{reportHistoricoAdministradoraForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valig                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportHistoricoAdministradora" enctype="multipart/form-data">    				
				<h:inputHidden id="reportId" value="#{reportHistoricoAdministradoraForm.reportId}" />
    			<h:inputHidden id="reportName" value="#{reportHistoricoAdministradoraForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Hist�rico por Unidad Administradora
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoAdministradoraForm.idTipoPersonal}"
                                            	immediate="false"
												onchange="this.form.submit()"
												valueChangeListener="#{reportHistoricoAdministradoraForm.changeTipoPersonal}">
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{reportHistoricoAdministradoraForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    
    								<tr>
    									<td>
    										Unidad Administradora
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoAdministradoraForm.idUnidadAdministradora}"
                                            	immediate="false">
                                                <f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{reportHistoricoAdministradoraForm.listUnidadAdministradora}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Por Categoria Presupuestaria
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoAdministradoraForm.categoria}"    											    											
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Si" itemValue="S" />  
                                            	<f:selectItem itemLabel="No" itemValue="N" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>    	
    								<tr>
    									<td>    									
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoAdministradoraForm.idConceptoTipoPersonal}"
	    										onchange="this.form.submit()"
	    										valueChangeListener="#{reportHistoricoAdministradoraForm.changeConceptoTipoPersonal}"
    											rendered="#{reportHistoricoAdministradoraForm.showConcepto}"
                                            	immediate="false">
                                                <f:selectItems value="#{reportHistoricoAdministradoraForm.colConceptoTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>        														
    								<tr>
    									<td>
    										A�o
    									</td>
    									<td width="100%" >    									
    										<h:inputText
    											size="5"
    											id="Anio"
    											maxlength="4"
    											value="#{reportHistoricoAdministradoraForm.anio}"
    											required="false" />    										
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Mes
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoAdministradoraForm.mes}"    											    											
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Enero" itemValue="1" />
                                            	<f:selectItem itemLabel="Febrero" itemValue="2" />
                                            	<f:selectItem itemLabel="Marzo" itemValue="3" />
                                            	<f:selectItem itemLabel="Abril" itemValue="4" />
                                            	<f:selectItem itemLabel="Mayo" itemValue="5" />
                                            	<f:selectItem itemLabel="Junio" itemValue="6" />
                                            	<f:selectItem itemLabel="Julio" itemValue="7" />
                                            	<f:selectItem itemLabel="Agosto" itemValue="8" />
                                            	<f:selectItem itemLabel="Septiembre" itemValue="9" />
                                            	<f:selectItem itemLabel="Octubre" itemValue="10" />
                                            	<f:selectItem itemLabel="Noviembre" itemValue="11" />
                                            	<f:selectItem itemLabel="Diciembre" itemValue="12" />                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td>
    										Orden
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoAdministradoraForm.orden}"    											    											
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Alfab�tico" itemValue="A" />  
                                            	<f:selectItem itemLabel="C�dula" itemValue="C" />
                                            	<f:selectItem itemLabel="C�digo" itemValue="O" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoAdministradoraForm.formato}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
												
    								
									
    																    								
									</table>
									</td>
    								<tr>
    								<td colspan="2" align="center">										
										<h:commandButton image="/images/report.gif"
											action="#{reportHistoricoAdministradoraForm.runReport}"
											onclick="windowReport()" /> 
    								<td>
    								</tr>
    								    								
    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>