<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<script language=javascript>  
		function windowReport() {
			var number = 
				document.forms['formReportHistoricoGrupoNomina'].elements['formReportHistoricoGrupoNomina:reportId'].value;
			var name = 
				document.forms['formReportHistoricoGrupoNomina'].elements['formReportHistoricoGrupoNomina:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
	</script>
	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{reportHistoricoGrupoNominaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign                
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportHistoricoGrupoNomina" enctype="multipart/form-data">    				
				<h:inputHidden id="reportId" value="#{reportHistoricoGrupoNominaForm.reportId}" />
    			<h:inputHidden id="reportName" value="#{reportHistoricoGrupoNominaForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Hist�rico por Grupo Nomina
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Grupo de N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoGrupoNominaForm.idGrupoNomina}"
                                            	immediate="false"		
                                            	onchange="this.form.submit()"										
												valueChangeListener="#{reportHistoricoGrupoNominaForm.changeGrupoNomina}"> 
												<f:selectItem itemLabel="Seleccione" itemValue="0" />                                              
                                                <f:selectItems value="#{reportHistoricoGrupoNominaForm.listGrupoNomina}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    
    								<tr>
    									<td>
    										Regi�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoGrupoNominaForm.idRegion}"
                                            	immediate="false">
                                                <f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{reportHistoricoGrupoNominaForm.listRegion}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    	
    								<tr>
    									<td>    									
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoGrupoNominaForm.idConcepto}"
    										
	    										onchange="this.form.submit()"	    										
                                            	immediate="false">
                                            	 <f:selectItem itemLabel="Todos" itemValue="0" />
                                                <f:selectItems value="#{reportHistoricoGrupoNominaForm.colConcepto}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>        														
    								<tr>
    									<td>
    										A�o
    									</td>
    									<td width="100%" >    									
    										<h:inputText
    											size="5"
    											id="Anio"
    											maxlength="4"
    											value="#{reportHistoricoGrupoNominaForm.anio}"
    											required="false" />    										
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Mes
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoGrupoNominaForm.mes}"    											    											
	    										onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Enero" itemValue="1" />
                                            	<f:selectItem itemLabel="Febrero" itemValue="2" />
                                            	<f:selectItem itemLabel="Marzo" itemValue="3" />
                                            	<f:selectItem itemLabel="Abril" itemValue="4" />
                                            	<f:selectItem itemLabel="Mayo" itemValue="5" />
                                            	<f:selectItem itemLabel="Junio" itemValue="6" />
                                            	<f:selectItem itemLabel="Julio" itemValue="7" />
                                            	<f:selectItem itemLabel="Agosto" itemValue="8" />
                                            	<f:selectItem itemLabel="Septiembre" itemValue="9" />
                                            	<f:selectItem itemLabel="Octubre" itemValue="10" />
                                            	<f:selectItem itemLabel="Noviembre" itemValue="11" />
                                            	<f:selectItem itemLabel="Diciembre" itemValue="12" />                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Quincena/Semana
    									</td>
    									<td width="100%" >    									
    										<h:inputText
    											size="3"
    											id="quincenaSemana"
    											maxlength="2"
    											onblur="this.form.submit()"
    											value="#{reportHistoricoGrupoNominaForm.quincenaSemana}"
    											required="false" />    										
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Orden
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoGrupoNominaForm.orden}"    											    											
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Alfab�tico" itemValue="A" />  
                                            	<f:selectItem itemLabel="C�dula" itemValue="C" />
                                            	<f:selectItem itemLabel="C�digo" itemValue="O" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoGrupoNominaForm.formato}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
												
    								
									
    																    								
									</table>
									</td>
    								<tr>
    								<td colspan="2" align="center">										
										<h:commandButton image="/images/report.gif"
											action="#{reportHistoricoGrupoNominaForm.runReport}"
											onclick="windowReport()" /> 
    								<td>
    								</tr>
    								    								
    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>