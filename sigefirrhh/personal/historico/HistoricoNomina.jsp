<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{historicoNominaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formHistoricoNomina">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Hist�rico Datos N�mina del Trabajador
										</b>
									</td>
									
								</tr>
								
								<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						    </td>
								
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchHistoricoNomina"
								rendered="#{!historicoNominaForm.showData&&
									!historicoNominaForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable">
										<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{historicoNominaForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{historicoNominaForm.findTrabajadorCedula}"
														onkeypress="javascript:return keyCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{historicoNominaForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{historicoNominaForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onblur="javascript:fieldEmpty(this)"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{historicoNominaForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{historicoNominaForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{historicoNominaForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{historicoNominaForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{historicoNominaForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{historicoNominaForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{historicoNominaForm.showResultTrabajador&&
								!historicoNominaForm.showData&&
								!historicoNominaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{historicoNominaForm.resultTrabajador}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{historicoNominaForm.findHistoricoNominaByTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif" />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif" />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{historicoNominaForm.selectedTrabajador&&
								!historicoNominaForm.showData&&
								!historicoNominaForm.editing&&
								!historicoNominaForm.deleting}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{historicoNominaForm.trabajador}"
                            						action="#{historicoNominaForm.findHistoricoNominaByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{historicoNominaForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultHistoricoNomina"
								rendered="#{historicoNominaForm.showResult&&
								!historicoNominaForm.showData&&
								!historicoNominaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{historicoNominaForm.result}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{historicoNominaForm.selectHistoricoNomina}"
                                						styleClass="listitem">
                                						<f:param name="idHistoricoNomina" value="#{result.idHistoricoNomina}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>

</h:form>
							<f:subview 
								id="viewDataHistoricoNomina"
								rendered="#{historicoNominaForm.showData||historicoNominaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!historicoNominaForm.editing&&!historicoNominaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{historicoNominaForm.editing&&!historicoNominaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{historicoNominaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{historicoNominaForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
<h:form id="historicoNominaForm">
            							<f:subview 
            								id="dataHistoricoNomina"
            								rendered="#{historicoNominaForm.showData}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{historicoNominaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{historicoNominaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{historicoNominaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{historicoNominaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{historicoNominaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{historicoNominaForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							/>	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim><tr>
                        						<td>
            										Trabajador
                        						</td>
                        						<td></f:verbatim>
                        							<h:outputText value="#{historicoNominaForm.trabajador}" />
                        						<f:verbatim></td>
            								</tr></f:verbatim>
            								
                        					<f:verbatim><tr>
                        						<td>
                        							A�o
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							maxlength="4"
                        								value="#{historicoNominaForm.historicoNomina.anio}"
                        								                        								readonly="#{!historicoNominaForm.editing}"
                        								id="anio"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        																						 >
                        							</h:inputText>
                        																				<h:message for="anio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Mes
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							maxlength="2"
                        								value="#{historicoNominaForm.historicoNomina.mes}"
                        								                        								readonly="#{!historicoNominaForm.editing}"
                        								id="mes"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        																						 >
                        							</h:inputText>
                        																				<h:message for="mes" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Quincena/Semana
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="1"
	                        							maxlength="1"
                        								value="#{historicoNominaForm.historicoNomina.semanaQuincena}"
                        								                        								readonly="#{!historicoNominaForm.editing}"
                        								id="semanaQuincena"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        																						 >
                        							</h:inputText>
                        																				<h:message for="semanaQuincena" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Estatus
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.historicoNomina.estatus}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="Estatus"
                        								                        								                                                    	required="false"
                                                    	title="Estatus">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.listEstatus}" />
                                                    </h:selectOneMenu>												<h:message for="estatus" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Dependencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectDependencia}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colDependencia}" />
                                                    </h:selectOneMenu>												<h:message for="dependencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectCargo}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colCargo}" />
                                                    </h:selectOneMenu>												<h:message for="cargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�digo N�mina
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							maxlength="6"
                        								value="#{historicoNominaForm.historicoNomina.codigoNomina}"
                        								                        								readonly="#{!historicoNominaForm.editing}"
                        								id="codigoNomina"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        																						 >
                        							</h:inputText>
                        																				<h:message for="codigoNomina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Forma de Pago
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.historicoNomina.formaPago}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="FormaPago"
                        								                        								 onchange="this.form.submit()"                                                     	required="false"
                                                    	title="Forma de Pago">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.listFormaPago}" />
                                                    </h:selectOneMenu>												<h:message for="formaPago" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Banco N�mina
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectBanco}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								 rendered="#{historicoNominaForm.showBancoAux}"                         								                                                    	required="false"
														title="Banco N�mina">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colBanco}" />
                                                    </h:selectOneMenu>												<h:message for="banco" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo de Cuenta
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.historicoNomina.tipoCtaNomina}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="TipoCtaNomina"
                        								 rendered="#{historicoNominaForm.showTipoCtaNominaAux}"                         								                                                    	required="false"
                                                    	title="Tipo de Cuenta">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.listTipoCtaNomina}" />
                                                    </h:selectOneMenu>												<h:message for="tipoCtaNomina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							N� Cuenta N�mina
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="20"
	                        							maxlength="20"
                        								value="#{historicoNominaForm.historicoNomina.cuentaNomina}"
                        								 rendered="#{historicoNominaForm.showCuentaNominaAux}"                         								readonly="#{!historicoNominaForm.editing}"
                        								id="cuentaNomina"
                        								required="false"
                        								title="CampoX"
                        								alt="CampoX"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						 >
                        							</h:inputText>
                        																				<h:message for="cuentaNomina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectTipoPersonal}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Tipo Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colTipoPersonal}" />
                                                    </h:selectOneMenu>												<h:message for="tipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Grupo Nomina
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectGrupoNomina}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Grupo Nomina">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colGrupoNomina}" />
                                                    </h:selectOneMenu>												<h:message for="grupoNomina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nomina Especial
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectNominaEspecial}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Nomina Especial">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colNominaEspecial}" />
                                                    </h:selectOneMenu>												<h:message for="nominaEspecial" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Lugar de Pago
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectLugarPago}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Lugar de Pago">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colLugarPago}" />
                                                    </h:selectOneMenu>												<h:message for="lugarPago" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Trabajador
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectTrabajador}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Trabajador">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colTrabajador}" />
                                                    </h:selectOneMenu>												<h:message for="trabajador" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Unidad Ejecutora
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectUnidadEjecutora}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Unidad Ejecutora">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colUnidadEjecutora}" />
                                                    </h:selectOneMenu>												<h:message for="unidadEjecutora" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Unidad Administradora
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectUnidadAdministradora}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Unidad Administradora">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colUnidadAdministradora}" />
                                                    </h:selectOneMenu>												<h:message for="unidadAdministradora" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Sede
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectSede}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Sede">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colSede}" />
                                                    </h:selectOneMenu>												<h:message for="sede" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Region
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{historicoNominaForm.selectRegion}"
                                                    	disabled="#{!historicoNominaForm.editing}"
                                                    	immediate="false"
	                        							id="$relation.getName()"
                        								                        								                                                    	required="false"
														title="Region">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{historicoNominaForm.colRegion}" />
                                                    </h:selectOneMenu>												<h:message for="region" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr>                            </f:verbatim>
									</f:subview>
            							<f:subview 
            								id="commandsHistoricoNomina"
            								rendered="#{historicoNominaForm.showData}">
                        				<f:verbatim><table>
                        				<table class="datatable" align="center">
                        					<tr>
                        					
                        						<td></f:verbatim>
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{historicoNominaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{historicoNominaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{historicoNominaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{historicoNominaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{historicoNominaForm.editing}" />	
                            	        			<h:commandButton value="Cancelar" 
			                                    		image="/images/cancel.gif"
                                    					action="#{historicoNominaForm.abortUpdate}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />	
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>