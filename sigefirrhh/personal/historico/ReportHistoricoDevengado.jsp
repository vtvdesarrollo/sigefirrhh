<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<script language=javascript>  

		function windowReport() {
			var number = 
				document.forms['formReportHistoricoDevengado'].elements['formReportHistoricoDevengado:reportId'].value;
			var name = 
				document.forms['formReportHistoricoDevengado'].elements['formReportHistoricoDevengado:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
	</script>
	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body>
<f:view>
	<x:saveState value="#{reportHistoricoDevengadoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valig                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportHistoricoDevengado" enctype="multipart/form-data">    				
				<h:inputHidden id="reportId" value="#{reportHistoricoDevengadoForm.reportId}" />
    			<h:inputHidden id="reportName" value="#{reportHistoricoDevengadoForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Hist�rico por Devengados/Incidencias
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  /><br>
    						</td>
    					</tr>
    					<tr>		
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoDevengadoForm.idTipoPersonal}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"
                                            	valueChangeListener="#{reportHistoricoDevengadoForm.changeTipoPersonal}"
												>
                                                <f:selectItem itemLabel="Seleccionar" itemValue="0" />
                                                <f:selectItems value="#{reportHistoricoDevengadoForm.listTipoPersonal}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    
    								<tr>
    									<td>
    										Regi�n
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoDevengadoForm.idRegion}"
                                            	immediate="false">
                                                <f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{reportHistoricoDevengadoForm.listRegion}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    	
    																						
    								<tr>
    									<td>
    										Incidencia en
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoDevengadoForm.incidencia}"    											    											
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="ISLR" itemValue="1" />  
                                            	<f:selectItem itemLabel="Bono Fin A�o" itemValue="2" />
                                            	<f:selectItem itemLabel="Prestaciones Mensuales" itemValue="3" />
                                            	<f:selectItem itemLabel="Prestaciones Mensuales (Columna Otros)" itemValue="4" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											maxlength="4"    											  											
    											id="Anio"
    											value="#{reportHistoricoDevengadoForm.anio}"
    										/>    								
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="3"
    											maxlength="2"    											  											
    											id="Mes"
    											value="#{reportHistoricoDevengadoForm.mes}"
    										/>    								
    									</td>
									</tr>
    								<tr>
    									<td>
    										Orden
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoDevengadoForm.orden}"    											    											
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Alfab�tico" itemValue="A" />  
                                            	<f:selectItem itemLabel="C�dula" itemValue="C" />
                                            	<f:selectItem itemLabel="C�digo" itemValue="O" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Formato
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportHistoricoDevengadoForm.formato}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="PDF" itemValue="1" /> 
                                            	<f:selectItem itemLabel="Hoja de Calculo" itemValue="2" />                                              	
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
												
    								
									
    																    								
									</table>
									</td>
    								<tr>
    								<td colspan="2" align="center">										
										<h:commandButton image="/images/report.gif"
											action="#{reportHistoricoDevengadoForm.runReport}"
											onclick="windowReport()" /> 
    								<td>
    								</tr>
    								    								
    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>