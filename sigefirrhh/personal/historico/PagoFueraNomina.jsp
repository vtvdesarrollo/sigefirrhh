<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{pagoFueraNominaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formPagoFueraNomina">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Pagos Fuera del Sistema
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/personal/otrosProcesos/PagoFueraNomina.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchHistoricoQuincena"
								rendered="#{!pagoFueraNominaForm.showData&&
									!pagoFueraNominaForm.showAdd}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar Trabajador
												</td>
											</tr>
										</table>										
										<table width="100%" class="datatable">
										<tr>
                        						<td>
                        							Tipo de Personal
                        						</td>
                        						<td></f:verbatim>
					                            	<h:selectOneMenu value="#{pagoFueraNominaForm.findSelectTrabajadorIdTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{pagoFueraNominaForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
													
												<f:verbatim></td>
											</tr>
                        					<tr>
                        						<td>
                        							C�dula
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="10"
														maxlength="10"
														value="#{pagoFueraNominaForm.findTrabajadorCedula}"
														onkeypress="javascript:return keyCheck(event, this)"
														onfocus="return deleteZero(event, this)"
														onblur="javascript:fieldEmpty(this)"
														onchange="javascript:this.value=this.value.toUpperCase()" />
													<h:commandButton image="/images/find.gif" 
														action="#{pagoFueraNominaForm.findTrabajadorByCedula}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Nombres
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{pagoFueraNominaForm.findTrabajadorPrimerNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onblur="javascript:fieldEmpty(this)"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{pagoFueraNominaForm.findTrabajadorSegundoNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							Apellidos
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="20"
														maxlength="20"
														value="#{pagoFueraNominaForm.findTrabajadorPrimerApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:inputText size="20"
														maxlength="20"
														value="#{pagoFueraNominaForm.findTrabajadorSegundoApellido}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />
													<h:commandButton image="/images/find.gif" 
														action="#{pagoFueraNominaForm.findTrabajadorByNombresApellidos}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											<tr>
                        						<td>
                        							C�digo de N�mina
                        						</td>
                        						<td></f:verbatim>
													<h:inputText size="7"
														maxlength="7"
														value="#{pagoFueraNominaForm.findTrabajadorCodigoNomina}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onfocus="return deleteZero(event, this)"
														onkeypress="return keyEnterCheck(event, this)" />
														<h:commandButton image="/images/find.gif" 
														action="#{pagoFueraNominaForm.findTrabajadorByCodigoNomina}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr>
											
										</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>

							<f:subview 
								id="viewResultTrabajador"
								rendered="#{pagoFueraNominaForm.showResultTrabajador&&
								!pagoFueraNominaForm.showData&&
								!pagoFueraNominaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
    										<table width="100%" class="bandtable">
    											<tr>
    												<td>
    													Resultado de Trabajador
    												</td>
    											</tr>
    										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="tableTrabajador"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="resultTrabajador"
                                				value="#{pagoFueraNominaForm.resultTrabajador}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{resultTrabajador}"
                                						action="#{pagoFueraNominaForm.findHistoricoByTrabajador}"
                                						styleClass="listitem">
                                						<f:param name="idTrabajador" value="#{resultTrabajador.idTrabajador}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_1"
                                                    for="tableTrabajador"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif" />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif" />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_2"
    	                                        	for="tableTrabajador"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							
							
							
							<f:subview 
								id="viewSelectedTrabajador"
								rendered="#{pagoFueraNominaForm.selectedTrabajador&&
								!pagoFueraNominaForm.showData&&
								!pagoFueraNominaForm.editing&&
								!pagoFueraNominaForm.deleting}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
            							<td>
											Trabajador:&nbsp;</f:verbatim>
            								<h:commandLink value="#{pagoFueraNominaForm.trabajador}"
                            						action="#{pagoFueraNominaForm.findHistoricoByTrabajador}"
                            						styleClass="listitem">
                            						<f:param name="idTrabajador" value="#{pagoFueraNominaForm.trabajador.idTrabajador}" />
                            					</h:commandLink>
            							<f:verbatim></td>
									</tr>
									<tr>
										<td>
											<table width="100%" class="bandtable">
    											<tr>
    												<td style="text-align: right"></f:verbatim>
            											<h:commandButton image="/images/add.gif"
            												action="#{pagoFueraNominaForm.add}"
            												onclick="javascript:return clickMade()"
            												rendered="#{!pagoFueraNominaForm.editing&&!pagoFueraNominaForm.deleting&&pagoFueraNominaForm.login.agregar}" />
            											<h:commandButton value="Cancelar" 
			                                    			image="/images/cancel.gif"
                                    						action="#{pagoFueraNominaForm.abort}"
                                    						immediate="true"
                                    						onclick="javascript:return clickMade()"
                            	        					/>	
                            	        							
                                            			<f:verbatim>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultHistoricoQuincena"
								rendered="#{pagoFueraNominaForm.showResult&&
								!pagoFueraNominaForm.showData&&
								!pagoFueraNominaForm.showAdd&&
								pagoFueraNominaForm.showResultQuincena}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda 
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{pagoFueraNominaForm.result}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{pagoFueraNominaForm.selectHistoricoQuincena}"
                                						styleClass="listitem">
                                						<f:param name="idHistoricoQuincena" value="#{result.idHistoricoQuincena}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview 
								id="viewResultHistoricoSemana"
								rendered="#{pagoFueraNominaForm.showResult&&
								!pagoFueraNominaForm.showData&&
								!pagoFueraNominaForm.showAdd&&
								pagoFueraNominaForm.showResultSemana}">
								<f:verbatim><table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <x:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{pagoFueraNominaForm.result}"
                                                preserveDataModel="false"
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{pagoFueraNominaForm.selectHistoricoSemana}"
                                						styleClass="listitem">
                                						<f:param name="idHistoricoSemana" value="#{result.idHistoricoSemana}" />
                                					</h:commandLink>
                                				</h:column>
                                            </x:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                            			<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataHistoricoQuincena"
								rendered="#{pagoFueraNominaForm.showData||pagoFueraNominaForm.showAdd}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></verbatim>
													<h:outputText value="Consultando"
														rendered="#{!pagoFueraNominaForm.editing&&!pagoFueraNominaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{pagoFueraNominaForm.editing&&!pagoFueraNominaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{pagoFueraNominaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{pagoFueraNominaForm.adding}" />
												<verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
										<h:form id="pagoFueraNominaForm">
            							<f:subview 
            								id="dataHistoricoQuincena"
            								rendered="#{pagoFueraNominaForm.showData}">
											<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{pagoFueraNominaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!pagoFueraNominaForm.editing&&!pagoFueraNominaForm.deleting&&pagoFueraNominaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{pagoFueraNominaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!pagoFueraNominaForm.editing&&!pagoFueraNominaForm.deleting&&pagoFueraNominaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{pagoFueraNominaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{pagoFueraNominaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{pagoFueraNominaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{pagoFueraNominaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{pagoFueraNominaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
                                    								action="#{pagoFueraNominaForm.abortUpdate}"
                                    								immediate="true"
                                    								onclick="javascript:return clickMade()"
                            	        							/>	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
            								
            								
											<f:verbatim><tr>
                        						<td>
            										Trabajador
                        						</td>
                        						<td></f:verbatim>
                        							<h:outputText value="#{pagoFueraNominaForm.trabajador}" />
                        						<f:verbatim></td>
            								</tr></f:verbatim>
            								<f:subview 
			            								id="dataQuincena"
			            								rendered="#{pagoFueraNominaForm.showDataQuincena}">
		                        					<f:verbatim><tr>
		                        						<td>
		                        							N�mina
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:outputText	                        							
		                        								value="#{pagoFueraNominaForm.historicoQuincena.numeroNomina}"                        								                        								
		                        								id="numeroNomina"                        								
		                        								title="CampoX"
		                        								 >
		                        							</h:outputText>
		                        																				
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Tipo Personal
		                        						</td>
		                        						<td></f:verbatim>
		                                                   <h:outputText	                        							
		                        								value="#{pagoFueraNominaForm.historicoQuincena.tipoPersonal}"                        								                        								
		                        								id="tipoPersonal"                        								
		                        								title="CampoX"
		                        								 >
		                        							</h:outputText>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Grupo de N�mina
		                        						</td>
		                        						<td></f:verbatim>
		                                                   <h:outputText	                        							
		                        								value="#{pagoFueraNominaForm.historicoQuincena.grupoNomina}"                        								                        								
		                        								id="grupoNomina"                        								
		                        								title="CampoX"
		                        								>
		                        							</h:outputText>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Concepto
		                        						</td>
		                        						<td></f:verbatim>
		                                                    <h:selectOneMenu value="#{pagoFueraNominaForm.selectConceptoTipoPersonal}"
		                                                    	disabled="#{!pagoFueraNominaForm.editing}"
		                                                    	immediate="false"
			                        							id="concepto"                        								
				                                            	onchange="this.form.submit()"  
				                                            	valueChangeListener="#{pagoFueraNominaForm.changeConcepto}"                        								                                                    	required="false"
																title="Concepto">
																<f:selectItem itemLabel="Seleccione" itemValue="0" />
		                                                        <f:selectItems value="#{pagoFueraNominaForm.colConceptoTipoPersonal}" />
		                                                    </h:selectOneMenu>												<h:message for="conceptoTipoPersonal" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Frecuencia
		                        						</td>
		                        						<td></f:verbatim>
		                                                   <h:outputText	                        							
		                        								value="#{pagoFueraNominaForm.historicoQuincena.frecuenciaTipoPersonal}"                        								                        								
		                        								id="frecuencia"                        								
		                        								title="CampoX"
		                        								>
		                        							</h:outputText>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							A�o
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="4"
			                        							maxlength="4"
		                        								value="#{pagoFueraNominaForm.historicoQuincena.anio}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								id="anio"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        																						 >
		                        							</h:inputText>
		                        																				<h:message for="anio" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Mes
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="2"
			                        							maxlength="2"
		                        								value="#{pagoFueraNominaForm.historicoQuincena.mes}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								id="mes"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        																						 >
		                        							</h:inputText>
		                        																				<h:message for="mes" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Quincena
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="1"
			                        							maxlength="1"
		                        								value="#{pagoFueraNominaForm.historicoQuincena.semanaQuincena}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								id="semanaQuincena"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        																						 >
		                        							</h:inputText>
		                        																				<h:message for="semanaQuincena" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Fecha
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="10"
			                        							maxlength="10"
			                        							id="fecha"
		                        								value="#{pagoFueraNominaForm.historicoQuincena.fecha}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								onkeypress="return keyEnterCheck(event, this)"
		                        								onblur="javascript:check_date(this)"
		                        								                        								required="false">
		                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
		                                                        pattern="dd-MM-yyyy" />
		                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
														<h:message for="fecha" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Unidades
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="6"
			                        							maxlength="6"
		                        								value="#{pagoFueraNominaForm.historicoQuincena.unidades}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								id="unidades"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								onkeypress="return keyFloatCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        								                        																						 style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
		                        							</h:inputText>
		                        																				<h:message for="unidades" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Asignaci�n
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="16"
			                        							maxlength="16"
			                        							immediate="false"
		                        								value="#{pagoFueraNominaForm.historicoQuincena.montoAsigna}"
		                        								rendered="#{pagoFueraNominaForm.showAsignacion}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								id="montoAsigna"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								onkeypress="return keyFloatCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        								                        																						 style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
		                        							</h:inputText>
		                        																				<h:message for="montoAsigna" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Deducci�n
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="16"
			                        							maxlength="16"
			                        							immediate="false"
		                        								value="#{pagoFueraNominaForm.historicoQuincena.montoDeduce}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								rendered="#{pagoFueraNominaForm.showDeduccion}"
		                        								id="montoDeduce"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								onkeypress="return keyFloatCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        								                        																						 style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
		                        							</h:inputText>
		                        																				<h:message for="montoDeduce" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Origen
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:outputText	                        							
		                        								value="#{pagoFueraNominaForm.historicoQuincena.origen}"                        								                        								
		                        								id="origen"                        								
		                        								title="CampoX"
		                        								 >
		                        							</h:outputText>
		                        																				
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					
		                        					
                        				</f:subview>
                        				<f:subview 
			            					id="dataSemana"
			            					rendered="#{pagoFueraNominaForm.showDataSemana}">
		                        					<f:verbatim><tr>
		                        						<td>
		                        							N�mina
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:outputText	                        							
		                        								value="#{pagoFueraNominaForm.historicoSemana.numeroNomina}"                        								                        								
		                        								id="numeroNomina"                        								
		                        								title="CampoX"
		                        								 >
		                        							</h:outputText>
		                        																				
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Tipo Personal
		                        						</td>
		                        						<td></f:verbatim>
		                                                   <h:outputText	                        							
		                        								value="#{pagoFueraNominaForm.historicoSemana.tipoPersonal}"                        								                        								
		                        								id="tipoPersonal"                        								
		                        								title="CampoX"
		                        								 >
		                        							</h:outputText>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Grupo de N�mina
		                        						</td>
		                        						<td></f:verbatim>
		                                                   <h:outputText	                        							
		                        								value="#{pagoFueraNominaForm.historicoSemana.grupoNomina}"                        								                        								
		                        								id="grupoNomina"                        								
		                        								title="CampoX"
		                        								>
		                        							</h:outputText>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Concepto
		                        						</td>
		                        						<td></f:verbatim>
		                                                    <h:selectOneMenu value="#{pagoFueraNominaForm.selectConceptoTipoPersonal}"
		                                                    	disabled="#{!pagoFueraNominaForm.editing}"
		                                                    	immediate="false"
			                        							id="concepto"                        								
				                                            	onchange="this.form.submit()"  
				                                            	valueChangeListener="#{pagoFueraNominaForm.changeConcepto}"                        								                                                    	required="false"
																title="Concepto">
																<f:selectItem itemLabel="Seleccione" itemValue="0" />
		                                                        <f:selectItems value="#{pagoFueraNominaForm.colConceptoTipoPersonal}" />
		                                                    </h:selectOneMenu>												<h:message for="conceptoTipoPersonal" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Frecuencia
		                        						</td>
		                        						<td></f:verbatim>
		                                                   <h:outputText	                        							
		                        								value="#{pagoFueraNominaForm.historicoSemana.frecuenciaTipoPersonal}"                        								                        								
		                        								id="frecuencia"                        								
		                        								title="CampoX"
		                        								>
		                        							</h:outputText>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							A�o
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="4"
			                        							maxlength="4"
		                        								value="#{pagoFueraNominaForm.historicoSemana.anio}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								id="anio"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        																						 >
		                        							</h:inputText>
		                        																				<h:message for="anio" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Mes
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="2"
			                        							maxlength="2"
		                        								value="#{pagoFueraNominaForm.historicoSemana.mes}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								id="mes"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        																						 >
		                        							</h:inputText>
		                        																				<h:message for="mes" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Quincena
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="1"
			                        							maxlength="1"
		                        								value="#{pagoFueraNominaForm.historicoSemana.semanaQuincena}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								id="semanaQuincena"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        																						 >
		                        							</h:inputText>
		                        																				<h:message for="semanaQuincena" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Fecha
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="10"
			                        							maxlength="10"
			                        							id="fecha"
		                        								value="#{pagoFueraNominaForm.historicoSemana.fecha}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								onkeypress="return keyEnterCheck(event, this)"
		                        								onblur="javascript:check_date(this)"
		                        								                        								required="false">
		                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
		                                                        pattern="dd-MM-yyyy" />
		                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
														<h:message for="fecha" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Unidades
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="6"
			                        							maxlength="6"
		                        								value="#{pagoFueraNominaForm.historicoSemana.unidades}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								id="unidades"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								onkeypress="return keyFloatCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        								                        																						 style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
		                        							</h:inputText>
		                        																				<h:message for="unidades" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Asignaci�n
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="16"
			                        							maxlength="16"
			                        							immediate="false"
		                        								value="#{pagoFueraNominaForm.historicoSemana.montoAsigna}"
		                        								rendered="#{pagoFueraNominaForm.showAsignacion}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								id="montoAsigna"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								onkeypress="return keyFloatCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        								                        																						 style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
		                        							</h:inputText>
		                        																				<h:message for="montoAsigna" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Deducci�n
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:inputText
			                        							size="16"
			                        							maxlength="16"
			                        							immediate="false"
		                        								value="#{pagoFueraNominaForm.historicoSemana.montoDeduce}"
		                        								readonly="#{!pagoFueraNominaForm.editing}"
		                        								rendered="#{pagoFueraNominaForm.showDeduccion}"
		                        								id="montoDeduce"
		                        								required="false"
		                        								title="CampoX"
		                        								alt="CampoX"
		                        								onchange="javascript:this.value=this.value.toUpperCase()"
		                        								                        								onkeypress="return keyFloatCheck(event, this)"
		                        								onblur="javascript:fieldEmpty(this)"
		                        								                        								                        																						 style="text-align:right"  >
																<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
		                        							</h:inputText>
		                        																				<h:message for="montoDeduce" styleClass="error"/>
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					<f:verbatim><tr>
		                        						<td>
		                        							Origen
		                        						</td>
		                        						<td></f:verbatim>
							                        		<h:outputText	                        							
		                        								value="#{pagoFueraNominaForm.historicoSemana.origen}"                        								                        								
		                        								id="origen"                        								
		                        								title="CampoX"
		                        								 >
		                        							</h:outputText>
		                        																				
		                        						<f:verbatim></td>
		                        					</tr>                            </f:verbatim>
		                        					
		                        					
                        				</f:subview>
									</f:subview>
            							<f:subview 
            								id="commandsHistoricoQuincena"
            								rendered="#{pagoFueraNominaForm.showData}">
                        				<f:verbatim><table>
                        				<table class="datatable" align="center">
                        					<tr>
                        					
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{pagoFueraNominaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!pagoFueraNominaForm.editing&&!pagoFueraNominaForm.deleting&&pagoFueraNominaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{pagoFueraNominaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!pagoFueraNominaForm.editing&&!pagoFueraNominaForm.deleting&&pagoFueraNominaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{pagoFueraNominaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{pagoFueraNominaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{pagoFueraNominaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{pagoFueraNominaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{pagoFueraNominaForm.editing}" />	
                            	        			<h:commandButton value="Cancelar" 
			                                    		image="/images/cancel.gif"
                                    					action="#{pagoFueraNominaForm.abortUpdate}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />	
	                                    			<f:verbatim>
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim></f:subview>
</h:form>
                                    	<f:verbatim>
                        				</td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>