<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportRetencionISLR'].elements['formReportRetencionISLR:reportId'].value;
			var name = 
				document.forms['formReportRetencionISLR'].elements['formReportRetencionISLR:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>

	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{reportRetencionISLRForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportRetencionISLR">
    				<h:inputHidden id="reportId" value="#{reportRetencionISLRForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportRetencionISLRForm.reportName}" />
				
				
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reporte de Retenciones de ISLR
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/disquete/DisqueteAporte.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Criterios del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Grupo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRetencionISLRForm.selectGrupoNomina}"
    										valueChangeListener="#{reportRetencionISLRForm.changeGrupoNomina}"                    
                                            	immediate="true"
                                            	onchange="this.form.submit()"  
                                            	>
                                                <f:selectItems value="#{reportRetencionISLRForm.listGrupoNomina}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td width="20%">
    										Mes Proceso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="3"
    											id="Mes"
    											maxlength="2"
    											value="#{reportRetencionISLRForm.mes}"
    											required="true" />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										A�o Proceso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="Anio"
    											maxlength="5"
    											value="#{reportRetencionISLRForm.anio}"
    											readonly="false"
    											required="true" />    								
    									</td>
									</tr>
									
									
									<tr>
    									<td>
    										Tipo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportRetencionISLRForm.tipoNomina}"
    											valueChangeListener="#{reportRetencionISLRForm.changeTipoNomina}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  >
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                            	<f:selectItem itemLabel="Ordinarias" itemValue="1" />
                                            	<f:selectItem itemLabel="Especiales" itemValue="2" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
								

    								<tr>
    									<td width="20%">
    										<h:outputLabel value="Semana/Quincena" rendered="#{reportRetencionISLRForm.showOrdinaria}"/> 
    										<h:outputLabel value="N�mina Especial" rendered="#{reportRetencionISLRForm.showEspecial}"/> 
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="10"
    											id="SemanaQuincena"
    											maxlength="10"
    											value="#{reportRetencionISLRForm.semanaQuincena}"
    											readonly="false"
    											required="false" 
    											rendered="#{reportRetencionISLRForm.showOrdinaria}" /> 
    										<h:selectOneMenu value="#{reportRetencionISLRForm.selectNominaEspecial}"
    											valueChangeListener="#{reportRetencionISLRForm.changeNominaEspecial}"
                                            	immediate="true"
                                            	onchange="this.form.submit()"
                                            	rendered="#{reportRetencionISLRForm.showEspecial}">
                                                <f:selectItems value="#{reportRetencionISLRForm.listNominaEspecial}" />                                                
                                            </h:selectOneMenu>                                            
    											
    									</td>
									</tr>
										    								    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										action="#{reportRetencionISLRForm.generar}"
    											onclick="windowReport()" title="Ejecutar Reporte"/>
    									<td>
    								</tr>

    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>