<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportAportesPatronales'].elements['formReportAportesPatronales:reportId'].value;
			var name = 
				document.forms['formReportAportesPatronales'].elements['formReportAportesPatronales:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportAportesPatronalesForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	               
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportAportesPatronales">
				
    				<h:inputHidden id="reportId" value="#{reportAportesPatronalesForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportAportesPatronalesForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reportes de Retenciones y Aportes Patronales
    							</b>
    						</td>
    						<td align="right">
    						
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">   
	    							<tr>
    									<td>
    										Tipo de Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportAportesPatronalesForm.idTipoPersonal}"
	    										onchange="this.form.submit()"
	    										valueChangeListener="#{reportAportesPatronalesForm.changeTipoPersonal}"
                                            	immediate="false">                                         	
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportAportesPatronalesForm.listTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		 								    																	
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportAportesPatronalesForm.idConcepto}"
	    										rendered="#{reportAportesPatronalesForm.showConcepto}"	    										
    											onchange="this.form.submit()"
    											valueChangeListener="#{reportAportesPatronalesForm.changeConcepto}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  
                                                <f:selectItems value="#{reportAportesPatronalesForm.listConcepto}" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>    								    								
    								<tr>
    									<td>
    										Por Banco
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportAportesPatronalesForm.banco}"    											    											
	    										rendered="#{reportAportesPatronalesForm.showBanco}"	 
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="No" itemValue="N" />  
                                            	<f:selectItem itemLabel="Si" itemValue="S" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Por C�digo Patronal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportAportesPatronalesForm.codigoPatronal}"    											    											
	    										rendered="#{reportAportesPatronalesForm.showCodigoPatronal}"	 
	    										onchange="this.form.submit()"
	    										valueChangeListener="#{reportAportesPatronalesForm.changeCodigoPatronal}"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="No" itemValue="N" />  
                                            	<f:selectItem itemLabel="Si" itemValue="S" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Agrupacion Presupuestaria
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportAportesPatronalesForm.categoriaPresupuesto}"    											    											
	    										rendered="#{reportAportesPatronalesForm.showAgrupacion}"	 
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Categoria Presupuestaria/UEL" itemValue="U" />  
                                            	<f:selectItem itemLabel="UEL/Categoria Presupuestaria" itemValue="C" />
                                            	<f:selectItem itemLabel="Unidad Ejecutora Local" itemValue="E" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>    								
									<tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{reportAportesPatronalesForm.showReport}"	 
    											size="5"
    											id="Anio"
    											maxlength="4"
    											value="#{reportAportesPatronalesForm.anio}"
    											required="true" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{reportAportesPatronalesForm.showReport}"	 
    											size="3"
    											id="mes"
    											maxlength="2"
    											value="#{reportAportesPatronalesForm.mes}"
    											required="true" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td>
    										Primera Firma
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportAportesPatronalesForm.firma1}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportAportesPatronalesForm.listFirma1}" />                                                
                                            </h:selectOneMenu>
    									</td>    									
    								</tr>	
    								<tr>
    									<td>
    										Segunda Firma
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportAportesPatronalesForm.firma2}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportAportesPatronalesForm.listFirma2}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Tercera Firma
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportAportesPatronalesForm.firma3}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportAportesPatronalesForm.listFirma3}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										rendered="#{reportAportesPatronalesForm.showReport}"	 
    											action="#{reportAportesPatronalesForm.runReport}"
    											onclick="windowReport()" title="Ejecutar Reporte"/>
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>