<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportPrenominaEspecial'].elements['formReportPrenominaEspecial:reportId'].value;
			var name = 
				document.forms['formReportPrenominaEspecial'].elements['formReportPrenominaEspecial:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}    	
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportPrenominaEspecialForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	               
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportPrenominaEspecial">
				
    				<h:inputHidden id="reportId" value="#{reportPrenominaEspecialForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportPrenominaEspecialForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reportes: Prenomina Especial
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/procesoNomina/ReportPrenominaEspecial.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Grupo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPrenominaEspecialForm.selectGrupoNomina}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{reportPrenominaEspecialForm.changeGrupoNomina}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportPrenominaEspecialForm.listGrupoNomina}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>			
    								<tr>
    									<td>
    										Nomina Especial
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPrenominaEspecialForm.selectNominaEspecial}"
	    										required="true"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reportPrenominaEspecialForm.show}"
                                            	valueChangeListener="#{reportPrenominaEspecialForm.changeNominaEspecial}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportPrenominaEspecialForm.listNominaEspecial}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>									
    								<tr>
    									<td width="20%">
    										Desde
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{reportPrenominaEspecialForm.show2}"
    											size="10"
    											id="Desde"
    											readonly="true"
    											value="#{reportPrenominaEspecialForm.inicio}">    										
    											
                	                           </h:inputText>										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Hasta
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{reportPrenominaEspecialForm.show2}"
    											size="10"
    											id="Hasta"
    											readonly="true"
    											value="#{reportPrenominaEspecialForm.fin}">    								
    											
                	                           </h:inputText>										
    									</td>
									</tr>
									<tr>
    									<td>
    										Unidad Administradora
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPrenominaEspecialForm.idUnidadAdministradora}"
	    										rendered="#{reportPrenominaEspecialForm.show2}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	rendered="#{reportPrenominaEspecialForm.show}">
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{reportPrenominaEspecialForm.listUnidadAdministradora}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Criterios
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPrenominaEspecialForm.tipoReporte}"
	    										rendered="#{reportPrenominaEspecialForm.show2}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  
                                            	<f:selectItem itemLabel="Prenomina Especial por Codigo" itemValue="1" />
                                            	<f:selectItem itemLabel="Prenomina Especial Alfabetico" itemValue="2" />
                                            	<f:selectItem itemLabel="Prenomina Especial por Programa" itemValue="24" /> 
                                            	<f:selectItem itemLabel="Prenomina Especial por UEL" itemValue="3" /> 
                                            	<f:selectItem itemLabel="Prenomina Especial por Region" itemValue="22" />
                                            	<f:selectItem itemLabel="Prenomina Especial por Dependencia" itemValue="23" />                                            	
                                            	<f:selectItem itemLabel="Detalle de Conceptos Alfab�tico" itemValue="4" />
                                            	<f:selectItem itemLabel="Detalle de Conceptos por C�digo" itemValue="28" />
                                            	<f:selectItem itemLabel="Detalle de Conceptos por C�dula" itemValue="29" />
                                            	<f:selectItem itemLabel="Resumen de Conceptos General" itemValue="5" />
                                            	<f:selectItem itemLabel="Resumen de Conceptos por UEL" itemValue="6" />  
												<f:selectItem itemLabel="Resumen de Conceptos por Categoria Presupuestaria" itemValue="70" />  
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Categoria Presupuestaria/UEL" itemValue="71" />                                                 	
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Unidad Administradora" itemValue="20" />                                   				
    											<f:selectItem itemLabel="Dep�sitos Bancarios Alfab�tico" itemValue="7" /> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por C�digo" itemValue="25" /> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por C�dula" itemValue="26" />  
    											<f:selectItem itemLabel="Dep�sitos Bancarios por Unidad Administradora" itemValue="27" />
    											<f:selectItem itemLabel="Dep�sitos Bancarios por Tipo de Cuenta" itemValue="31" />  			  						    											    											
    											<f:selectItem itemLabel="Listado de Cheques" itemValue="8" />    											
    											<f:selectItem itemLabel="Sobregirados" itemValue="30" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										rendered="#{reportPrenominaEspecialForm.show2}"
    											action="#{reportPrenominaEspecialForm.runReport}"
    											onclick="windowReport()" />
    										
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>