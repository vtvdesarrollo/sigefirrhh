<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{generarNominaEspecialForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formGenerarNominaEspecial">
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Proceso: Generar Nomina Especial
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/procesoNomina/GenerarNominaEspecial.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Grupo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarNominaEspecialForm.selectGrupoNomina}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarNominaEspecialForm.changeGrupoNomina}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarNominaEspecialForm.listGrupoNomina}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td>
    										Nomina Especial
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarNominaEspecialForm.selectNominaEspecial}"
    											required="true"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarNominaEspecialForm.changeNominaEspecial}"
                                            	rendered="#{generarNominaEspecialForm.show3}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarNominaEspecialForm.listNominaEspecial}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>									
    								<tr>
    									<td width="20%">
    										Desde
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{generarNominaEspecialForm.show2}"
    											size="10"
    											id="Desde"
												readonly="true"
    											value="#{generarNominaEspecialForm.inicio}" >    										
    										
                	                           </h:inputText>										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Hasta
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{generarNominaEspecialForm.show2}"
    											size="10"
    											id="Hasta"
    											readonly="true"    											
    											value="#{generarNominaEspecialForm.fin}" >    								
    											
                	                           </h:inputText>										
    									</td>
									</tr>
					<!--					<tr>
    									<td width="20%">
    										Mes Contable
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											readonly="false"
    											size="3"
    											id="Mes"
    											maxlength="2"
    											value="#{generarNominaEspecialForm.mes}"    											
    											required="false" />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										A�o Contable
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											readonly="false"
    											size="5"
    											id="Anio"
    											maxlength="4"
    											value="#{generarNominaEspecialForm.anio}"    											
    											required="false" />    								
    									</td>
									</tr>		-->
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										rendered="#{generarNominaEspecialForm.show2}"
    											action="#{generarNominaEspecialForm.preGenerate}"
    											/>
    										
    									<td>
    								</tr>
    								<tr>
    								<td colspan="2" align="center">	
	    															
										<h:outputText
                        					value="Se grabar� en el a�o y mes indicado. �Esta seguro que desea realizar este proceso?"
                        					rendered="#{generarNominaEspecialForm.show}" />                							                        				                				
                        				<h:commandButton value="Si" 
                        					image="/images/yes.gif"
                        					action="#{generarNominaEspecialForm.generate}"
                        					onclick="javascript:return clickMade()"
                        					rendered="#{generarNominaEspecialForm.show}" />                        					                        			                        				
                        				<h:commandButton value="Cancelar" 
                        					image="/images/cancel.gif"
                        					action="#{generarNominaEspecialForm.abort}"
                        					immediate="true"
                        					rendered="#{generarNominaEspecialForm.show}"
                        					onclick="javascript:return clickMade()"
                	        				 />
                	        			
    								<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>