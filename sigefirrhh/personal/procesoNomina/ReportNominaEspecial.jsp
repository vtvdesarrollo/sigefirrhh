<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportNominaEspecial'].elements['formReportNominaEspecial:reportId'].value;
			var name = 
				document.forms['formReportNominaEspecial'].elements['formReportNominaEspecial:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}	
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportNominaEspecialForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportNominaEspecial">
				
    				<h:inputHidden id="reportId" value="#{reportNominaEspecialForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportNominaEspecialForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reportes: Nomina Especial
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/procesoNomina/ReportNominaEspecial.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
	            					image="/images/close.gif"
	            					action="go_cancelOption"
	            					immediate="true"                        					
	            					onclick="javascript:return clickMade()"
	    	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Grupo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaEspecialForm.selectGrupoNomina}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{reportNominaEspecialForm.changeGrupoNomina}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaEspecialForm.listGrupoNomina}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td>
    										Nomina Especial
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaEspecialForm.selectNominaEspecial}"
    											required="true"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{reportNominaEspecialForm.changeNominaEspecial}"
                                            	rendered="#{reportNominaEspecialForm.show}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaEspecialForm.listNominaEspecial}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>											
    								<tr>
    									<td width="20%">
    										Desde
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										onblur="javascript:check_date(this)"
    											rendered="#{reportNominaEspecialForm.show2}"
    											size="10"
    											id="Desde"
    											readonly="true"
    											value="#{reportNominaEspecialForm.inicio}">    										
    											
                	                           </h:inputText>										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Hasta
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											onblur="javascript:check_date(this)"
    											rendered="#{reportNominaEspecialForm.show2}"
    											size="10"
    											id="Hasta"
    											readonly="true"
    											value="#{reportNominaEspecialForm.fin}">    								
    											
                	                           </h:inputText>										
    									</td>
									</tr>
									<tr>
    									<td>
    										Unidad Administradora
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaEspecialForm.idUnidadAdministradora}"
	    										rendered="#{reportNominaEspecialForm.show}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{reportNominaEspecialForm.listUnidadAdministradora}" />                                                
                                            </h:selectOneMenu>
    									</td>    									
    								</tr>	
									<tr>
    									<td>
    										Primera Firma
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaEspecialForm.firma1}"
	    										rendered="#{reportNominaEspecialForm.show}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaEspecialForm.listFirma1}" />                                                
                                            </h:selectOneMenu>
    									</td>    									
    								</tr>	
    								<tr>
    									<td>
    										Segunda Firma
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaEspecialForm.firma2}"
	    										rendered="#{reportNominaEspecialForm.show}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaEspecialForm.listFirma2}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Tercera Firma
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaEspecialForm.firma3}"
	    										rendered="#{reportNominaEspecialForm.show}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaEspecialForm.listFirma3}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Criterios
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaEspecialForm.tipoReporte}"
	    										rendered="#{reportNominaEspecialForm.show2}"
	    										onchange="this.form.submit()">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  
                                            	<f:selectItem itemLabel="Nomina por Codigo" itemValue="1" />
                                            	<f:selectItem itemLabel="Nomina Alfabetico" itemValue="2" />
                                            	<f:selectItem itemLabel="Nomina por Programa" itemValue="24" /> 
                                            	<f:selectItem itemLabel="Nomina por UEL" itemValue="3" /> 
                                            	<f:selectItem itemLabel="Nomina por Region" itemValue="22" />
                                            	<f:selectItem itemLabel="Nomina por Dependencia" itemValue="23" />                                            	
                                            	<f:selectItem itemLabel="Detalle de Conceptos Alfab�tico" itemValue="4" />
                                            	<f:selectItem itemLabel="Detalle de Conceptos por C�digo" itemValue="28" />
                                            	<f:selectItem itemLabel="Detalle de Conceptos por C�dula" itemValue="29" />
                                            	<f:selectItem itemLabel="Resumen de Conceptos General" itemValue="5" />
                                            	<f:selectItem itemLabel="Resumen de Conceptos por UEL" itemValue="6" />             
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Categoria Presupuestaria" itemValue="70" />  
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Categoria Presupuestaria/UEL" itemValue="71" />                                       	
                                            	<f:selectItem itemLabel="Resumen de Conceptos con aportes" itemValue="15" />
                                            	<f:selectItem itemLabel="Resumen de Conceptos con aportes por UEL" itemValue="16" />                                             	                                              	
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Unidad Administradora" itemValue="20" />                                   				
    											<f:selectItem itemLabel="Dep�sitos Bancarios Alfab�tico" itemValue="7" /> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por C�digo" itemValue="25" /> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por C�dula" itemValue="26" />  
    											<f:selectItem itemLabel="Dep�sitos Bancarios por Unidad Administradora" itemValue="27" /> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por Tipo de Cuenta" itemValue="31" />  						    											    											
    											<f:selectItem itemLabel="Listado de Cheques" itemValue="8" />
    											<f:selectItem itemLabel="Netos a Pagar Con Cuenta Nomina" itemValue="400" />
    											<f:selectItem itemLabel="Netos a Pagar Sin Cuenta Nomina" itemValue="405" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Unidad Administradora" itemValue="300" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Lugar de Pago" itemValue="305" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Dependencia" itemValue="310" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Dependencia Sin Monto" itemValue="315" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Uel Sin Monto" itemValue="320" />
    											<f:selectItem itemLabel="Recibos de Pago Alfab�tico" itemValue="100" />
    											<f:selectItem itemLabel="Recibos de Pago 2pp Carta Alfab�tico" itemValue="101" />
    											<f:selectItem itemLabel="Recibos de Pago 3pp Carta Alfab�tico" itemValue="102" />
    											<f:selectItem itemLabel="Recibos de Pago 4pp Carta Alfab�tico" itemValue="103" />
    											<f:selectItem itemLabel="Recibos de Pago por C�digo" itemValue="104" />
    											<f:selectItem itemLabel="Recibos de Pago 2pp Carta por C�digo" itemValue="105" />
    											<f:selectItem itemLabel="Recibos de Pago 3pp Carta por C�digo" itemValue="106" />
    											<f:selectItem itemLabel="Recibos de Pago 4pp Carta por C�digo" itemValue="107" />
    											<f:selectItem itemLabel="Recibos de Pago por C�dula" itemValue="108" />
    											<f:selectItem itemLabel="Recibos de Pago 2pp Carta por C�dula" itemValue="109" />
    											<f:selectItem itemLabel="Recibos de Pago 3pp Carta por C�dula" itemValue="110" />
    											<f:selectItem itemLabel="Recibos de Pago 4pp Carta por C�dula" itemValue="111" />
    											<f:selectItem itemLabel="Recibos de Pago 3pp Carta por Dependecia" itemValue="112" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
                						<td>
                							Mensaje
                						</td>
                						<td>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="2"
                    							
                    							id="mensaje"
                								value="#{reportNominaEspecialForm.mensaje}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />													
                						</td>
                					</tr>			
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										rendered="#{reportNominaEspecialForm.show2}"
    											action="#{reportNominaEspecialForm.runReport}"
    											onclick="windowReport()" />
    										
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>