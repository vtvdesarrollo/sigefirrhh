<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportNomina'].elements['formReportNomina:reportId'].value;
			var name = 
				document.forms['formReportNomina'].elements['formReportNomina:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportNominaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign=               
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportNomina">
				
    				<h:inputHidden id="reportId" value="#{reportNominaForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportNominaForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reportes: Nomina
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/procesoNomina/ReportNomina.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Grupo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaForm.selectGrupoNomina}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{reportNominaForm.changeGrupoNomina}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaForm.listGrupoNomina}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>										
    								<tr>
    									<td width="20%">
    										Desde
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    											rendered="#{reportNominaForm.show}"    											
    											id="Desde"    											
    											value="#{reportNominaForm.inicio}"
    											 />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Hasta
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    											rendered="#{reportNominaForm.show}"    											
    											id="Hasta"    					
    											value="#{reportNominaForm.fin}"
    											 />    								
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Semana del A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    											rendered="#{reportNominaForm.showSemana}"    											
    											id="Semana"
    											value="#{reportNominaForm.semanaAnio}"
    										/>    								
    									</td>
									</tr>
									<tr>
    									<td>
    										Unidad Administradora
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaForm.idUnidadAdministradora}"
	    										rendered="#{reportNominaForm.show}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{reportNominaForm.listUnidadAdministradora}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
									<tr>
    									<td>
    										Primera Firma
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaForm.firma1}"
	    										rendered="#{reportNominaForm.show}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaForm.listFirma1}" />                                                
                                            </h:selectOneMenu>
    									</td>    									
    								</tr>	
    								<tr>
    									<td>
    										Segunda Firma
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaForm.firma2}"
	    										rendered="#{reportNominaForm.show}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaForm.listFirma2}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Tercera Firma
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaForm.firma3}"
	    										rendered="#{reportNominaForm.show}"
                                            	immediate="false"
                                            	onchange="this.form.submit()" >
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportNominaForm.listFirma3}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Criterios
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaForm.tipoReporte}"
	    										rendered="#{reportNominaForm.show}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  
                                            	<f:selectItem itemLabel="Nomina por Codigo" itemValue="10" />
                                            	<f:selectItem itemLabel="Nomina Alfabetico" itemValue="15"/>
                                            	<f:selectItem itemLabel="Nomina por Categoria Pesupuestaria" itemValue="20"/> 
                                            	<f:selectItem itemLabel="Nomina por UEL" itemValue="25"/> 
                                            	<f:selectItem itemLabel="Nomina por Region" itemValue="30"/>
                                            	<f:selectItem itemLabel="Nomina por Dependencia" itemValue="35"/>                                            	
                                            	<f:selectItem itemLabel="Detalle de Conceptos Alfab�tico" itemValue="50"/>
                                            	<f:selectItem itemLabel="Detalle de Conceptos por C�digo" itemValue="55"/>
                                            	<f:selectItem itemLabel="Detalle de Conceptos por C�dula" itemValue="60"/>
                                            	<f:selectItem itemLabel="Detalle de Conceptos por Dependencia con salto " itemValue="61"/>
                                            	<f:selectItem itemLabel="Resumen de Conceptos General" itemValue="100"/>
                                            	<f:selectItem itemLabel="Resumen de Conceptos por UEL" itemValue="105"/>  
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Categoria Presupuestaria" itemValue="110"/>  
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Categoria Presupuestaria/UEL" itemValue="115"/>                                            	
                                            	<f:selectItem itemLabel="Resumen de Conceptos con aportes" itemValue="120"/>
                                            	<f:selectItem itemLabel="Resumen de Conceptos con aportes por UEL" itemValue="125"/>                                              	                                              	
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Unidad Administradora" itemValue="130"/>                                   				
    											<f:selectItem itemLabel="Dep�sitos Bancarios Alfab�tico" itemValue="150"/> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por C�digo" itemValue="155"/> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por C�dula" itemValue="160"/>  
    											<f:selectItem itemLabel="Dep�sitos Bancarios por Unidad Administradora" itemValue="165"/> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por Tipo de Cuenta" itemValue="170"/>  	    													 						    											    											
    											<f:selectItem itemLabel="Listado de Cheques" itemValue="200"/>
    											<f:selectItem itemLabel="Listado de Cheques por Unidad Administradora" itemValue="205"/>
    											<f:selectItem itemLabel="Listado de Cheques por Dependencia" itemValue="210"/>
    											<f:selectItem itemLabel="Lista de Pagos a Beneficiarios" itemValue="250"/>
    											<f:selectItem itemLabel="Netos a Pagar Con Cuenta Nomina"  itemValue="400"/>
    											<f:selectItem itemLabel="Netos a Pagar Sin Cuenta Nomina"  itemValue="405"/>    											
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Unidad Administradora" itemValue="300" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Lugar de Pago" itemValue="305" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Dependencia con salto" itemValue="310" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Dependencia sin salto" itemValue="311" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Dependencia Sin Monto" itemValue="315" />
    											<f:selectItem itemLabel="Aceptaci�n de Pagos por Uel Sin Monto" itemValue="320" />
    											<f:selectItem itemLabel="Recibos de Pago Alfab�tico" itemValue="500" />    											
    											<f:selectItem itemLabel="Recibos de Pago por C�digo" itemValue="505" />
    											<f:selectItem itemLabel="Recibos de Pago por C�dula" itemValue="510" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Recibos (Distribuci�n) 
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaForm.distribucionRecibos}"
	    										rendered="#{reportNominaForm.showRecibo}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seguidos" itemValue="0" />  
                                            	<f:selectItem itemLabel="1pp Carta" itemValue="1" /> 
                                        		<f:selectItem itemLabel="2pp Carta" itemValue="2" /> 
                                        		<f:selectItem itemLabel="3pp Carta" itemValue="3" />  
                                        		<f:selectItem itemLabel="4pp Carta" itemValue="4" /> 
                                            	 </h:selectOneMenu>
    									</td>
    								</tr>	

    								<tr>
    									<td>
    										Recibos (Agrupaci�n) 
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportNominaForm.agrupacionRecibos}"
	    										rendered="#{reportNominaForm.showRecibo}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="General" itemValue="1" />
                                            	<f:selectItem itemLabel="Por Dependencia" itemValue="2" />  
                                        		<f:selectItem itemLabel="Por Unidad Ejecutora" itemValue="3" /> 
                                            	 </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
                						<td>
                							Concepto Desde
                						</td>
                						<td>
			                        		<h:inputText
                    							size="5"
                    							maxlength="4"
                    							id="conceptoDesde"
                    							rendered="#{reportNominaForm.showConcepto}"
                								value="#{reportNominaForm.conceptoDesde}"                								                       								                								                    								
                								required="false" />													
                						</td>
                					</tr>		
                					<tr>
                						<td>
                							Concepto Hasta
                						</td>
                						<td>
			                        		<h:inputText
                    							size="5"
                    							maxlength="4"
                    							id="conceptoHasta"
                    							rendered="#{reportNominaForm.showConcepto}"
                								value="#{reportNominaForm.conceptoHasta}"                								                       								                								                    								
                								required="false" />													
                						</td>
                					</tr>		
    								<tr>
                						<td>
                							Mensaje
                						</td>
                						<td>
			                        		<h:inputTextarea
                    							cols="50"
                    							rows="2"
                    							
                    							id="mensaje"
                								value="#{reportNominaForm.mensaje}"
                								onchange="javascript:this.value=this.value.toUpperCase()"                        								                								                    								
                								required="false" />													
                						</td>
                					</tr>								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										rendered="#{reportNominaForm.show}"
    											action="#{reportNominaForm.runReport}"
    											onclick="windowReport()" />
    								
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>