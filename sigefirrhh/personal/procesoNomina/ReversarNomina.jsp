<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">

	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{reversarNominaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
		     
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportNomina">
				
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reversar N�mina
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/procesoNomina/ReversarNomina.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Nomina
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Grupo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reversarNominaForm.selectGrupoNomina}"
                                            	immediate="true"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{reversarNominaForm.changeGrupoNomina}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reversarNominaForm.listGrupoNomina}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										Tipo de N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reversarNominaForm.tipoNomina}"	    										
	    										onchange="this.form.submit()"
                                            	immediate="true"
                                            	rendered="#{reversarNominaForm.auxShow}"
    										    valueChangeListener="#{reversarNominaForm.changeTipoNomina}">
                                            	<f:selectItem itemLabel="Ordinaria" itemValue="O" />  
                                            	<f:selectItem itemLabel="Especial" itemValue="E" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td>
    										N�mina Especial
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reversarNominaForm.selectNominaEspecial}"
                                            	immediate="true"
                                            	onchange="this.form.submit()" 
                                            	rendered="#{reversarNominaForm.showNominaEspecial}" 
                                            	valueChangeListener="#{reversarNominaForm.changeNominaEspecial}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reversarNominaForm.listNominaEspecial}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>		
    								<tr>
    									<td width="20%">
    										Desde
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{reversarNominaForm.auxShow}"
    											size="10"
    											id="Desde"
    											maxlength="10"
    											value="#{reversarNominaForm.inicio}"
    											readonly="true"
    											required="false" />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Hasta
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											rendered="#{reversarNominaForm.auxShow}"
    											size="10"
    											id="Hasta"
    											maxlength="10"
    											value="#{reversarNominaForm.fin}"
    											readonly="true"
    											required="false" />    								
    									</td>
									</tr>
									
    														    								    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										rendered="#{reversarNominaForm.auxShow}"
    											action="#{reversarNominaForm.preGenerate}"/>
    								
    									<td>
    								</tr>
    								
    								<tr>
    								<td colspan="2" align="center">	
	    															
										<h:outputText
                        					value="�Esta seguro que desea REVERSAR de la n�mina?"
                        					rendered="#{reversarNominaForm.show2}" />                							                        				                				
                        				<h:commandButton value="Si" 
                        					image="/images/yes.gif"
                        					action="#{reversarNominaForm.reversar}"
                        					onclick="javascript:return clickMade()"
                        					rendered="#{reversarNominaForm.show2}" />                        					                        			                        				
                        				<h:commandButton value="Cancelar" 
                        					image="/images/cancel.gif"
                        					action="#{reversarNominaForm.abort}"
                        					immediate="true"
                        					rendered="#{reversarNominaForm.show2}"
                        					onclick="javascript:return clickMade()"
                	        				 />
                	        			
    								<td>
    								</tr>
    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>