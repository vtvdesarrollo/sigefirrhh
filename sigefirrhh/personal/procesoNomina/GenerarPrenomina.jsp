<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
				
		<jsp:include page="/inc/functions.jsp" />
				
</head>

<body>
<f:view>
	<x:saveState value="#{generarPrenominaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formGenerarPrenomina">
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Proceso: Generar Prenomina Ordinaria
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/procesoNomina/GenerarPrenomina.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">
    								<tr>
    									<td>
    										Grupo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarPrenominaForm.selectGrupoNomina}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{generarPrenominaForm.changeGrupoNomina}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{generarPrenominaForm.listGrupoNomina}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>										
    								<tr>
    									<td width="20%">
    										Desde
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    											rendered="#{generarPrenominaForm.show}"    											
    											id="Desde"    											
    											value="#{generarPrenominaForm.inicio}"
    											 />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Hasta
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    											rendered="#{generarPrenominaForm.show}"    										
    											id="Hasta"    											
    											value="#{generarPrenominaForm.fin}"
    											 />    								
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Semana del A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    											rendered="#{generarPrenominaForm.showSemana}"    											
    											id="Semana"
    											value="#{generarPrenominaForm.semanaAnio}"
    										/>    								
    									</td>
									</tr>
									<tr>
    									<td>
    										Rec�lculo Conceptos ?
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{generarPrenominaForm.recalculo}"
	    										rendered="#{generarPrenominaForm.show}"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="No" itemValue="N" />
                                            	<f:selectItem itemLabel="Si" itemValue="S" />
                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										rendered="#{generarPrenominaForm.show}"
    											action="#{generarPrenominaForm.generate}"
    											
    											/>
    										
    									<td>
    								</tr>
    								
    							</table>
    						</td>
    					</tr>
    									<tr>
		<td>
		 
		</td>
		</tr>
    					<tr>
		<td>
		
						  <h:dataTable id="mensajesPrenomina" value="#{generarPrenominaForm.mensajesUltimaPrenomina}" var="mensajeUltimaPrenomina" styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2" rendered="#{generarPrenominaForm.showMensajesPrenomina}" width="100%">
    				<h:column>
    				 <f:facet name="header"><h:outputText value="Mensajes de Advertencia" /></f:facet>
    				<h:outputText id="mensajePrenomina" value="#{mensajeUltimaPrenomina.mensaje}"/>
    				</h:column>		
    				</h:dataTable>
		</td>
		</tr>
    				</table>
    				
								
				</h:form>
			</td>
		</tr>
		
	</table>

</f:view>	
</body>
</html>