
<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body>
<f:view>
	<x:saveState value="#{ReportRetencionBanavihForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />
	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportRetencionBanavih">
    				<h:inputHidden id="reportId" value="#{ReportRetencionBanavihForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{ReportRetencionBanavihForm.reportName}" />
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reporte de Retenciones de Banavih
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/disquete/DisqueteAporte.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Criterios del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo Personal
    									</td>
    									<td width="100%">
    										<h:selectOneMenu  value="#{ReportRetencionBanavihForm.selectTipoPersonal}"
    										valueChangeListener="#{ReportRetencionBanavihForm.changeTipoPersonal}" 
    										onchange="this.form.submit()" immediate="true">
    										<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{ReportRetencionBanavihForm.listTipoPersonal}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td width="20%">
    										Mes Proceso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="3"
    											id="Mes"
    											maxlength="2"
    											value="#{ReportRetencionBanavihForm.mes}"
    											required="true" />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										A�o Proceso
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    											size="5"
    											id="Anio"
    											maxlength="5"
    											value="#{ReportRetencionBanavihForm.anio}"
    											readonly="false"
    											required="true" />    								
    									</td>
									</tr>	    	
									<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
	    										action="#{ReportRetencionBanavihForm.generar}"
    											title="Ejecutar Reporte"/>
    									<td>
    								</tr>	
    								<tr>
                						<!-- td align="center">                							
                							<h:outputLink value="#{ReportRetencionBanavihForm.urlArchivo}" rendered="#{ReportRetencionBanavihForm.generado}">
                							<f:verbatim>Archivo Ingresos Generado  (Abrir nueva ventana con el bot�n derecho del mouse)</f:verbatim></h:outputLink>
                						</td-->
                						<td colspan="2" align="center">                							
                							<h:outputLink value="#{ReportRetencionBanavihForm.urlArchivo2}" rendered="#{ReportRetencionBanavihForm.generado}">
                							<f:verbatim>Archivo Generado  (Abrir en una nueva ventana con el bot�n derecho del mouse)</f:verbatim></h:outputLink>
                						</td>
                					</tr>						    								
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>