<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportPrenomina'].elements['formReportPrenomina:reportId'].value;
			var name = 
				document.forms['formReportPrenomina'].elements['formReportPrenomina:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportPrenominaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportPrenomina">
				
    				<h:inputHidden id="reportId" value="#{reportPrenominaForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportPrenominaForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Reportes: Prenomina
    							</b>
    						</td>
    						<td align="right">
    							<a href="#" onclick="window.open
    								('/sigefirrhh//help/sigefirrhh/personal/procesoNomina/ReportPrenomina.jsp',
    								'helpwindow',
    								'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
    								<h:graphicImage  url="/images/help/help.gif" />
    							</a>
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Grupo N�mina
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPrenominaForm.selectGrupoNomina}"
                                            	immediate="false"
                                            	onchange="this.form.submit()"  
                                            	valueChangeListener="#{reportPrenominaForm.changeGrupoNomina}">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                <f:selectItems value="#{reportPrenominaForm.listGrupoNomina}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>										
    								<tr>
    									<td width="20%">
    										Desde
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    											rendered="#{reportPrenominaForm.show}"    											
    											id="Desde"    											
    											value="#{reportPrenominaForm.inicio}"
    											 />    										
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Hasta
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    											rendered="#{reportPrenominaForm.show}"    											
    											id="Hasta"    											
    											value="#{reportPrenominaForm.fin}"
    											 />    								
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Semana del A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:outputText
    											rendered="#{reportPrenominaForm.showSemana}"    											
    											id="Semana"
    											value="#{reportPrenominaForm.semanaAnio}"
    										/>    								
    									</td>
									</tr>
									<tr>
    									<td>
    										Unidad Administradora
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPrenominaForm.idUnidadAdministradora}"
                                            	immediate="false"
                                            	rendered="#{reportPrenominaForm.show}"
                                            	onchange="this.form.submit()"  >
                                            	<f:selectItem itemLabel="Todas" itemValue="0" />
                                                <f:selectItems value="#{reportPrenominaForm.listUnidadAdministradora}" />                                                
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
    									<td>
    										Criterios
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportPrenominaForm.tipoReporte}"
	    										rendered="#{reportPrenominaForm.show}"
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Seleccione" itemValue="0" />  
                                            	<f:selectItem itemLabel="Prenomina por Codigo" itemValue="10" />
                                            	<f:selectItem itemLabel="Prenomina Alfabetico" itemValue="15" />
                                            	<f:selectItem itemLabel="Prenomina por Categoria Presupuestaria" itemValue="20" /> 
                                            	<f:selectItem itemLabel="Prenomina por UEL" itemValue="25" /> 
                                            	<f:selectItem itemLabel="Prenomina por Region" itemValue="30" />
                                            	<f:selectItem itemLabel="Prenomina por Dependencia" itemValue="35" />                                            	
                                            	<f:selectItem itemLabel="Detalle de Conceptos Alfab�tico" itemValue="50" />
                                            	<f:selectItem itemLabel="Detalle de Conceptos por C�digo" itemValue="55" />
                                            	<f:selectItem itemLabel="Detalle de Conceptos por C�dula" itemValue="60" />
                                            	<f:selectItem itemLabel="Resumen de Conceptos General" itemValue="100" />
                                            	<f:selectItem itemLabel="Resumen de Conceptos por UEL" itemValue="105" />    
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Categoria Presupuestaria" itemValue="110" />  
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Categoria Presupuestaria/UEL" itemValue="115" />                                                	
                                            	<f:selectItem itemLabel="Resumen de Conceptos por Unidad Administradora" itemValue="130" />                                   				
    											<f:selectItem itemLabel="Dep�sitos Bancarios Alfab�tico" itemValue="150" /> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por C�digo" itemValue="155" /> 
    											<f:selectItem itemLabel="Dep�sitos Bancarios por C�dula" itemValue="160" />  
    											<f:selectItem itemLabel="Dep�sitos Bancarios por Unidad Administradora" itemValue="165" />  
    											<f:selectItem itemLabel="Dep�sitos Bancarios por Tipo de Cuenta" itemValue="170" />  									    											    											
    											<f:selectItem itemLabel="Listado de Cheques" itemValue="200" /> 
    											<f:selectItem itemLabel="Listado de Contratos Vencidos" itemValue="207" />    		   											
    											<f:selectItem itemLabel="Sobregirados" itemValue="300" />
    											

                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								<tr>
                						<td>
                							Concepto Desde
                						</td>
                						<td>
			                        		<h:inputText
                    							size="5"
                    							maxlength="4"
                    							id="conceptoDesde"
                    							rendered="#{reportPrenominaForm.showConcepto}"
                								value="#{reportPrenominaForm.conceptoDesde}"                								                       								                								                    								
                								required="false" />													
                						</td>
                					</tr>		
                					<tr>
                						<td>
                							Concepto Hasta
                						</td>
                						<td>
			                        		<h:inputText
                    							size="5"
                    							maxlength="4"
                    							id="conceptoHasta"
                    							rendered="#{reportPrenominaForm.showConcepto}"
                								value="#{reportPrenominaForm.conceptoHasta}"                								                       								                								                    								
                								required="false" />													
                						</td>
                					</tr>		
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"
	    										rendered="#{reportPrenominaForm.show}"
    											action="#{reportPrenominaForm.runReport}"
    											onclick="windowReport()" />
    										
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>