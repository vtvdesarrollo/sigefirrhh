<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{dependenciaEducativaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formDependenciaEducativa">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Dependencias Educativas
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/docente/DependenciaEducativa.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{dependenciaEducativaForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!dependenciaEducativaForm.editing&&!dependenciaEducativaForm.deleting&&dependenciaEducativaForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchDependenciaEducativa"
								rendered="#{!dependenciaEducativaForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Nombre
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="1"
														maxlength="1"
														value="#{dependenciaEducativaForm.findNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{dependenciaEducativaForm.findDependenciaEducativaByNombre}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Correlativo
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="4"
														maxlength="4"
														value="#{dependenciaEducativaForm.findCorrelativoParroquia}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{dependenciaEducativaForm.findDependenciaEducativaByCorrelativoParroquia}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultDependenciaEducativaByNombre"
								rendered="#{dependenciaEducativaForm.showDependenciaEducativaByNombre&&
								!dependenciaEducativaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{dependenciaEducativaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{dependenciaEducativaForm.selectDependenciaEducativa}"
                                						styleClass="listitem">
                                						<f:param name="idDependenciaEducativa" value="#{result.idDependenciaEducativa}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultDependenciaEducativaByCorrelativoParroquia"
								rendered="#{dependenciaEducativaForm.showDependenciaEducativaByCorrelativoParroquia&&
								!dependenciaEducativaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{dependenciaEducativaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{dependenciaEducativaForm.selectDependenciaEducativa}"
                                						styleClass="listitem">
                                						<f:param name="idDependenciaEducativa" value="#{result.idDependenciaEducativa}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataDependenciaEducativa"
								rendered="#{dependenciaEducativaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!dependenciaEducativaForm.editing&&!dependenciaEducativaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{dependenciaEducativaForm.editing&&!dependenciaEducativaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{dependenciaEducativaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{dependenciaEducativaForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="dependenciaEducativaForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{dependenciaEducativaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!dependenciaEducativaForm.editing&&!dependenciaEducativaForm.deleting&&dependenciaEducativaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{dependenciaEducativaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!dependenciaEducativaForm.editing&&!dependenciaEducativaForm.deleting&&dependenciaEducativaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{dependenciaEducativaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{dependenciaEducativaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{dependenciaEducativaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{dependenciaEducativaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{dependenciaEducativaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{dependenciaEducativaForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nombre
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="1"
	                        							id="nombre"
	                        							maxlength="1"
                        								value="#{dependenciaEducativaForm.dependenciaEducativa.nombre}"
                        								readonly="#{!dependenciaEducativaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="nombre" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Correlativo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="correlativoParroquia"
	                        							maxlength="4"
                        								value="#{dependenciaEducativaForm.dependenciaEducativa.correlativoParroquia}"
                        								readonly="#{!dependenciaEducativaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="correlativoParroquia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Ubicacion Geopolitica
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{dependenciaEducativaForm.selectUbicacionGeopolitica}"
														id="ubicacionGeopolitica"
                                                    	disabled="#{!dependenciaEducativaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Ubicacion Geopolitica">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{dependenciaEducativaForm.colUbicacionGeopolitica}" />
                                                    </h:selectOneMenu>													<h:message for="ubicacionGeopolitica" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Dependencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{dependenciaEducativaForm.selectTipoDependencia}"
														id="tipoDependencia"
                                                    	disabled="#{!dependenciaEducativaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Tipo Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{dependenciaEducativaForm.colTipoDependencia}" />
                                                    </h:selectOneMenu>													<h:message for="tipoDependencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nivel Dependencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{dependenciaEducativaForm.selectNivelDependencia}"
														id="nivelDependencia"
                                                    	disabled="#{!dependenciaEducativaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Nivel Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{dependenciaEducativaForm.colNivelDependencia}" />
                                                    </h:selectOneMenu>													<h:message for="nivelDependencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Turno Dependencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{dependenciaEducativaForm.selectTurnoDependencia}"
														id="turnoDependencia"
                                                    	disabled="#{!dependenciaEducativaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Turno Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{dependenciaEducativaForm.colTurnoDependencia}" />
                                                    </h:selectOneMenu>													<h:message for="turnoDependencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Zona Educativa
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{dependenciaEducativaForm.selectZonaEducativa}"
														id="zonaEducativa"
                                                    	disabled="#{!dependenciaEducativaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Zona Educativa">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{dependenciaEducativaForm.colZonaEducativa}" />
                                                    </h:selectOneMenu>													<h:message for="zonaEducativa" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Area Geografica
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{dependenciaEducativaForm.selectAreaGeografica}"
														id="areaGeografica"
                                                    	disabled="#{!dependenciaEducativaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Area Geografica">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{dependenciaEducativaForm.colAreaGeografica}" />
                                                    </h:selectOneMenu>													<h:message for="areaGeografica" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Parroquia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{dependenciaEducativaForm.selectParroquia}"
														id="parroquia"
                                                    	disabled="#{!dependenciaEducativaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Parroquia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{dependenciaEducativaForm.colParroquia}" />
                                                    </h:selectOneMenu>													<h:message for="parroquia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Plantel Actual
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{dependenciaEducativaForm.selectPlantelActual}"
														id="plantelActual"
                                                    	disabled="#{!dependenciaEducativaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Plantel Actual">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{dependenciaEducativaForm.colPlantelActual}" />
                                                    </h:selectOneMenu>													<h:message for="plantelActual" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{dependenciaEducativaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!dependenciaEducativaForm.editing&&!dependenciaEducativaForm.deleting&&dependenciaEducativaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{dependenciaEducativaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!dependenciaEducativaForm.editing&&!dependenciaEducativaForm.deleting&&dependenciaEducativaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{dependenciaEducativaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{dependenciaEducativaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{dependenciaEducativaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{dependenciaEducativaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{dependenciaEducativaForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{dependenciaEducativaForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>