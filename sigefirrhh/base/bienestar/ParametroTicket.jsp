<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{parametroTicketForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formParametroTicket">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Par�metros de Tickets
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/bienestar/ParametroTicket.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{parametroTicketForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!parametroTicketForm.editing&&!parametroTicketForm.deleting&&parametroTicketForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchParametroTicket"
								rendered="#{!parametroTicketForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Tipo Personal
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{parametroTicketForm.findSelectTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroTicketForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{parametroTicketForm.findParametroTicketByTipoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultParametroTicketByTipoPersonal"
								rendered="#{parametroTicketForm.showParametroTicketByTipoPersonal&&
								!parametroTicketForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{parametroTicketForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{parametroTicketForm.selectParametroTicket}"
                                						styleClass="listitem">
                                						<f:param name="idParametroTicket" value="#{result.idParametroTicket}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataParametroTicket"
								rendered="#{parametroTicketForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!parametroTicketForm.editing&&!parametroTicketForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{parametroTicketForm.editing&&!parametroTicketForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{parametroTicketForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{parametroTicketForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="parametroTicketForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{parametroTicketForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!parametroTicketForm.editing&&!parametroTicketForm.deleting&&parametroTicketForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{parametroTicketForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!parametroTicketForm.editing&&!parametroTicketForm.deleting&&parametroTicketForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{parametroTicketForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{parametroTicketForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{parametroTicketForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{parametroTicketForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{parametroTicketForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{parametroTicketForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroTicketForm.selectTipoPersonal}"
														id="tipoPersonal"
                                                    	disabled="#{!parametroTicketForm.editing}"
                                                    	onchange="this.form.submit()"
                        								valueChangeListener="#{parametroTicketForm.changeTipoPersonal}"        								                                                    	immediate="false"
                                                    	required="true"
														title="Tipo Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroTicketForm.colTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="tipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroTicketForm.selectConceptoTipoPersonal}"
	                                                    id="conceptoTipoPersonal"
                                                    	disabled="#{!parametroTicketForm.editing}"
                                                    	rendered="#{parametroTicketForm.showConceptoTipoPersonal}"
                                                    	immediate="false"
                                                    	required="true"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{parametroTicketForm.colConceptoTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="conceptoTipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se paga por d�as h�biles ?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroTicketForm.parametroTicket.diasHabiles}"
	                                                    id="diasHabiles"
                                                    	disabled="#{!parametroTicketForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Se paga por d�as h�biles ?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroTicketForm.listDiasHabiles}" />
                                                    </h:selectOneMenu>													<h:message for="diasHabiles" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							D�as Fijos
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="diasFijos"
	                        							maxlength="10"
                        								value="#{parametroTicketForm.parametroTicket.diasFijos}"
                        								readonly="#{!parametroTicketForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								 rendered="#{parametroTicketForm.showDiasFijosAux}" 														                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="diasFijos" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se incluyen lo sabados ?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroTicketForm.parametroTicket.incluyeSabados}"
	                                                    id="incluyeSabados"
                                                    	disabled="#{!parametroTicketForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Se incluyen lo sabados ?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroTicketForm.listIncluyeSabados}" />
                                                    </h:selectOneMenu>													<h:message for="incluyeSabados" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se deducen ausencias ?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroTicketForm.parametroTicket.deducirAusencias}"
	                                                    id="deducirAusencias"
                                                    	disabled="#{!parametroTicketForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Se deducen ausencias ?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroTicketForm.listDeducirAusencias}" />
                                                    </h:selectOneMenu>													<h:message for="deducirAusencias" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Verificar cierre de ausencias ?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroTicketForm.parametroTicket.cierreAusencias}"
	                                                    id="cierreAusencias"
                                                    	disabled="#{!parametroTicketForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Verificar cierre de ausencias ?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroTicketForm.listCierreAusencias}" />
                                                    </h:selectOneMenu>													<h:message for="cierreAusencias" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se paga por n�mina ?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroTicketForm.parametroTicket.pagoNomina}"
	                                                    id="pagoNomina"
                                                    	disabled="#{!parametroTicketForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Se paga por n�mina ?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroTicketForm.listPagoNomina}" />
                                                    </h:selectOneMenu>													<h:message for="pagoNomina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se paga por tarjeta ?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroTicketForm.parametroTicket.pagoTarjeta}"
	                                                    id="pagoTarjeta"
                                                    	disabled="#{!parametroTicketForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Se paga por tarjeta ?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroTicketForm.listPagoTarjeta}" />
                                                    </h:selectOneMenu>													<h:message for="pagoTarjeta" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Los trabajadores se buscan por el sueldo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroTicketForm.parametroTicket.sueldoBasicoIntegral}"
	                                                    id="sueldoBasicoIntegral"
                                                    	disabled="#{!parametroTicketForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Los trabajadores se buscan por el sueldo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroTicketForm.listSueldoBasicoIntegral}" />
                                                    </h:selectOneMenu>													<h:message for="sueldoBasicoIntegral" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Codigo distribuci�n corresponde a 
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroTicketForm.parametroTicket.distribucion}"
	                                                    id="distribucion"
                                                    	disabled="#{!parametroTicketForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Codigo distribuacion corresponde a">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroTicketForm.listDistribucion}" />
                                                    </h:selectOneMenu>													<h:message for="distribucion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Sueldo M�ximo para optar
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="12"
	                        							id="sueldoMaximo"
	                        							maxlength="12"
                        								value="#{parametroTicketForm.parametroTicket.sueldoMaximo}"
                        								readonly="#{!parametroTicketForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="sueldoMaximo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{parametroTicketForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!parametroTicketForm.editing&&!parametroTicketForm.deleting&&parametroTicketForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{parametroTicketForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!parametroTicketForm.editing&&!parametroTicketForm.deleting&&parametroTicketForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{parametroTicketForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{parametroTicketForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{parametroTicketForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{parametroTicketForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{parametroTicketForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{parametroTicketForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>