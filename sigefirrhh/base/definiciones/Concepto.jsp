<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
    <jsp:include page="/inc/functions.jsp" />
    
    <script language=javascript> 
	
		    	function windowReport() {
				var number = 
					document.forms['formConcepto'].elements['formConcepto:reportId'].value;
				var url = '/sigefirrhh/reports/process.jsp?reportName=Concepto' + number;
				window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
	    	}
		
 	</script>
 	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{conceptoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formConcepto">
				<h:inputHidden id="reportId" value="#{conceptoForm.reportId}" />
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Conceptos
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/Concepto.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/report.gif"
														action="#{conceptoForm.runReport}"
        												onclick="windowReport()"        												
														rendered="#{!conceptoForm.editing&&!conceptoForm.deleting}" />
													<h:commandButton image="/images/add.gif"
														action="#{conceptoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!conceptoForm.editing&&!conceptoForm.deleting&&conceptoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchConcepto"
								rendered="#{!conceptoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													C�digo
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="4"
														maxlength="4"
														value="#{conceptoForm.findCodConcepto}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{conceptoForm.findConceptoByCodConcepto}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Descripci�n
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="50"
														maxlength="50"
														value="#{conceptoForm.findDescripcion}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{conceptoForm.findConceptoByDescripcion}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConceptoByCodConcepto"
								rendered="#{conceptoForm.showConceptoByCodConcepto&&
								!conceptoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoForm.selectConcepto}"
                                						styleClass="listitem">
                                						<f:param name="idConcepto" value="#{result.idConcepto}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConceptoByDescripcion"
								rendered="#{conceptoForm.showConceptoByDescripcion&&
								!conceptoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoForm.selectConcepto}"
                                						styleClass="listitem">
                                						<f:param name="idConcepto" value="#{result.idConcepto}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataConcepto"
								rendered="#{conceptoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!conceptoForm.editing&&!conceptoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{conceptoForm.editing&&!conceptoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{conceptoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{conceptoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="conceptoForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{conceptoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoForm.editing&&!conceptoForm.deleting&&conceptoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{conceptoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoForm.editing&&!conceptoForm.deleting&&conceptoForm.login.eliminar&&!conceptoForm.concepto.protegido}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{conceptoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{conceptoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{conceptoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{conceptoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{conceptoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{conceptoForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�digo</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="CodConcepto"
	                        							maxlength="4"
                        								value="#{conceptoForm.concepto.codConcepto}"
                        								readonly="#{!conceptoForm.editing || conceptoForm.concepto.protegido}"
                        								onchange="javascript:this.value=rellenaCero(this.value)"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="CodConcepto" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Descripci�n
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="50"
	                        							id="Descripcion"
	                        							maxlength="50"
                        								value="#{conceptoForm.concepto.descripcion}"
                        								readonly="#{!conceptoForm.editing || conceptoForm.concepto.protegido}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="Descripcion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Unidad
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.unidad}"
	                                                    id="Unidad"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Unidad">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listUnidad}" />
                                                    </h:selectOneMenu>													<h:message for="Unidad" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Forma parte del Sueldo Integral?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.sueldoIntegral}"
	                                                    id="SueldoIntegral"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Forma parte del Sueldo Integral?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listSueldoIntegral}" />
                                                    </h:selectOneMenu>													<h:message for="SueldoIntegral" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Forma parte del Sueldo B�sico?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.sueldoBasico}"
	                                                    id="SueldoBasico"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Forma parte del Sueldo B�sico?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listSueldoBasico}" />
                                                    </h:selectOneMenu>													<h:message for="SueldoBasico" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Ajuste del Sueldo B�sico?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.ajusteSueldo}"
	                                                    id="AjusteSueldo"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Ajuste del Sueldo B�sico?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listAjusteSueldo}" />
                                                    </h:selectOneMenu>													<h:message for="AjusteSueldo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Forma parte de la Compensaci�n?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.compensacion}"
	                                                    id="Compensacion"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Forma parte de la Compensaci�n?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listCompensacion}" />
                                                    </h:selectOneMenu>													<h:message for="Compensacion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Corresponde a una Prima del Cargo?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.primasCargo}"
	                                                    id="PrimasCargo"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Corresponde a una Prima del Cargo?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listPrimasCargo}" />
                                                    </h:selectOneMenu>													<h:message for="PrimasCargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Corresponde a una Prima del Trabajador?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.primasTrabajador}"
	                                                    id="PrimasTrabajador"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Corresponde a una Prima del Trabajador?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listPrimasTrabajador}" />
                                                    </h:selectOneMenu>													<h:message for="PrimasTrabajador" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Es Gravable?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.gravable}"
	                                                    id="Gravable"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Es Gravable?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listGravable}" />
                                                    </h:selectOneMenu>													<h:message for="Gravable" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.tipoPrestamo}"
	                                                    id="TipoPrestamo"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Tipo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listTipoPrestamo}" />
                                                    </h:selectOneMenu>													<h:message for="TipoPrestamo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Es una Cuota Sindicato?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.deduccionSindicato}"
	                                                    id="DeduccionSindicato"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Es una Cuota Sindicato?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listDeduccionSindicato}" />
                                                    </h:selectOneMenu>													<h:message for="DeduccionSindicato" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Es una Cuota Gremio?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.deduccionGremio}"
	                                                    id="DeduccionGremio"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Es una Cuota Gremio?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listDeduccionGremio}" />
                                                    </h:selectOneMenu>													<h:message for="DeduccionGremio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Es un Sobretiempo?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.sobretiempo}"
	                                                    id="Sobretiempo"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Es un Sobretiempo?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listSobretiempo}" />
                                                    </h:selectOneMenu>													<h:message for="Sobretiempo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Es un Beneficio?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.beneficio}"
	                                                    id="Beneficio"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Es un Beneficio?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listBeneficio}" />
                                                    </h:selectOneMenu>													<h:message for="Beneficio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Es base para el proceso de Jubilaci�n?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.jubilacion}"
	                                                    id="Jubilacion"
                                                    	disabled="#{!conceptoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Es base para el proceso de Jubilaci�n?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listJubilacion}" />
                                                    </h:selectOneMenu>													<h:message for="Jubilacion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Relacionado a una Caja Ahorros?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.deduccionCaja}"
	                                                    id="deduccionCaja"
                                                    	disabled="#{!conceptoForm.editing}"
                        								onchange="this.form.submit()"
                        								immediate="false"
                                                    	required="false"
														title="Relacionado a una Caja Ahorros?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listDeduccionCaja}" />
                                                    </h:selectOneMenu>													<h:message for="deduccionCaja" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Caja de Ahorro Asociada
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.selectConceptoCaja}"
														id="conceptoCaja"
                                                    	disabled="#{!conceptoForm.editing}"
                        								 rendered="#{conceptoForm.showConceptoCajaAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Caja de Ahorro Asociada">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.colConceptoCaja}" />
                                                    </h:selectOneMenu>													<h:message for="conceptoCaja" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tiene Aporte Patronal Asociado?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.aportePatronal}"
	                                                    id="AportePatronal"
                                                    	disabled="#{!conceptoForm.editing}"
                        								onchange="this.form.submit()"
                        								immediate="false"
                                                    	required="false"
														title="Tiene Aporte Patronal Asociado?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listAportePatronal}" />
                                                    </h:selectOneMenu>													<h:message for="AportePatronal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Aporte Patronal Asociado
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.selectConceptoAporte}"
														id="ConceptoAporte"
                                                    	disabled="#{!conceptoForm.editing}"
                        								 rendered="#{conceptoForm.showConceptoAporteAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Aporte Patronal Asociado">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.colConceptoAporte}" />
                                                    </h:selectOneMenu>													<h:message for="ConceptoAporte" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Tiene Retroactivo Asociado?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.retroactivo}"
	                                                    id="Retroactivo"
                                                    	disabled="#{!conceptoForm.editing}"
                        								onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Tiene Retroactivo Asociado?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listRetroactivo}" />
                                                    </h:selectOneMenu> <h:message for="Retroactivo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Retroactivo Asociado
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.selectConceptoRetroactivo}"
														id="ConceptoRetroactivo"
                                                    	disabled="#{!conceptoForm.editing}"
                        								 rendered="#{conceptoForm.showConceptoRetroactivoAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Retroactivo Asociado">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.colConceptoRetroactivo}" />
                                                    </h:selectOneMenu><h:message for="ConceptoRetroactivo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Aplica Descuento por Ausencia?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.ausencia}"
	                                                    id="Ausencia"
                                                    	disabled="#{!conceptoForm.editing}"
                        								onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Aplica Descuento por Ausencia?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listAusencia}" />
                                                    </h:selectOneMenu> <h:message for="Ausencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo de Ausencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.selectConceptoAusencia}"
														id="ConceptoAusencia"
                                                    	disabled="#{!conceptoForm.editing}"
                        								 rendered="#{conceptoForm.showConceptoAusenciaAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Tipo de Ausencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.colConceptoAusencia}" />
                                                    </h:selectOneMenu><h:message for="ConceptoAusencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tiene Retroactivo Asociado A�o Anterior?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.concepto.retroactivoAnterior}"
	                                                    id="RetroactivoAnterior"
                                                    	disabled="#{!conceptoForm.editing}"
                        								onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Tiene Retroactivo Asociado A�o Anterior?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.listRetroactivoAnterior}" />
                                                    </h:selectOneMenu> <h:message for="RetroactivoAnterior" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Retroactivo Asociado A�o Anterior
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoForm.selectConceptoRetroactivoAnterior}"
														id="ConceptoRetroactivoAnterior"
                                                    	disabled="#{!conceptoForm.editing}"
                        								 rendered="#{conceptoForm.showConceptoRetroactivoAnteriorAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Retroactivo Asociado Anterior">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoForm.colConceptoRetroactivoAnterior}" />
                                                    </h:selectOneMenu><h:message for="ConceptoRetroactivoAnterior" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{conceptoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoForm.editing&&!conceptoForm.deleting&&conceptoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{conceptoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoForm.editing&&!conceptoForm.deleting&&conceptoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{conceptoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{conceptoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{conceptoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{conceptoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{conceptoForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{conceptoForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>