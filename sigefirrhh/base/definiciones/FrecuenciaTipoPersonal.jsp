<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		    	function windowReport() {
				var number = 
					document.forms['formFrecuenciaTipoPersonal'].elements['formFrecuenciaTipoPersonal:reportId'].value;
				var url = '/sigefirrhh/reports/process.jsp?reportName=FrecuenciaTipoPersonal' + number;
				window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
	    	}
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{frecuenciaTipoPersonalForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formFrecuenciaTipoPersonal">
				<h:inputHidden id="reportId" value="#{frecuenciaTipoPersonalForm.reportId}" />
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Frecuencias por Tipo de Personal
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/FrecuenciaTipoPersonal.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/report.gif"
														action="#{frecuenciaTipoPersonalForm.runReport}"
        												onclick="windowReport()"        												
														rendered="#{!frecuenciaTipoPersonalForm.editing&&!frecuenciaTipoPersonalForm.deleting}" />
													<h:commandButton image="/images/add.gif"
														action="#{frecuenciaTipoPersonalForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!frecuenciaTipoPersonalForm.editing&&!frecuenciaTipoPersonalForm.deleting&&frecuenciaTipoPersonalForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchFrecuenciaTipoPersonal"
								rendered="#{!frecuenciaTipoPersonalForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Tipo Personal
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{frecuenciaTipoPersonalForm.findSelectTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{frecuenciaTipoPersonalForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{frecuenciaTipoPersonalForm.findFrecuenciaTipoPersonalByTipoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Frecuencia
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{frecuenciaTipoPersonalForm.findSelectFrecuenciaPago}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{frecuenciaTipoPersonalForm.findColFrecuenciaPago}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{frecuenciaTipoPersonalForm.findFrecuenciaTipoPersonalByFrecuenciaPago}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultFrecuenciaTipoPersonalByTipoPersonal"
								rendered="#{frecuenciaTipoPersonalForm.showFrecuenciaTipoPersonalByTipoPersonal&&
								!frecuenciaTipoPersonalForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{frecuenciaTipoPersonalForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{frecuenciaTipoPersonalForm.selectFrecuenciaTipoPersonal}"
                                						styleClass="listitem">
                                						<f:param name="idFrecuenciaTipoPersonal" value="#{result.idFrecuenciaTipoPersonal}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultFrecuenciaTipoPersonalByFrecuenciaPago"
								rendered="#{frecuenciaTipoPersonalForm.showFrecuenciaTipoPersonalByFrecuenciaPago&&
								!frecuenciaTipoPersonalForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{frecuenciaTipoPersonalForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{frecuenciaTipoPersonalForm.selectFrecuenciaTipoPersonal}"
                                						styleClass="listitem">
                                						<f:param name="idFrecuenciaTipoPersonal" value="#{result.idFrecuenciaTipoPersonal}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataFrecuenciaTipoPersonal"
								rendered="#{frecuenciaTipoPersonalForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!frecuenciaTipoPersonalForm.editing&&!frecuenciaTipoPersonalForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{frecuenciaTipoPersonalForm.editing&&!frecuenciaTipoPersonalForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{frecuenciaTipoPersonalForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{frecuenciaTipoPersonalForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="frecuenciaTipoPersonalForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{frecuenciaTipoPersonalForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!frecuenciaTipoPersonalForm.editing&&!frecuenciaTipoPersonalForm.deleting&&frecuenciaTipoPersonalForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{frecuenciaTipoPersonalForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!frecuenciaTipoPersonalForm.editing&&!frecuenciaTipoPersonalForm.deleting&&frecuenciaTipoPersonalForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{frecuenciaTipoPersonalForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{frecuenciaTipoPersonalForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{frecuenciaTipoPersonalForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{frecuenciaTipoPersonalForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{frecuenciaTipoPersonalForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{frecuenciaTipoPersonalForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{frecuenciaTipoPersonalForm.selectTipoPersonal}"
														id="tipoPersonal"
                                                    	disabled="#{!frecuenciaTipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Tipo Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{frecuenciaTipoPersonalForm.colTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="tipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Frecuencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{frecuenciaTipoPersonalForm.selectFrecuenciaPago}"
														id="frecuenciaPago"
                                                    	disabled="#{!frecuenciaTipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Frecuencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{frecuenciaTipoPersonalForm.colFrecuenciaPago}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="frecuenciaPago" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{frecuenciaTipoPersonalForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!frecuenciaTipoPersonalForm.editing&&!frecuenciaTipoPersonalForm.deleting&&frecuenciaTipoPersonalForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{frecuenciaTipoPersonalForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!frecuenciaTipoPersonalForm.editing&&!frecuenciaTipoPersonalForm.deleting&&frecuenciaTipoPersonalForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{frecuenciaTipoPersonalForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{frecuenciaTipoPersonalForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{frecuenciaTipoPersonalForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{frecuenciaTipoPersonalForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{frecuenciaTipoPersonalForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{frecuenciaTipoPersonalForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>