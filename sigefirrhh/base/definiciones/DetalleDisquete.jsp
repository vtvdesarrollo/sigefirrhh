<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{detalleDisqueteForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formDetalleDisquete">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Detalle Archivos(Disquetes)
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/DetalleDisquete.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{detalleDisqueteForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!detalleDisqueteForm.editing&&!detalleDisqueteForm.deleting&&detalleDisqueteForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchDetalleDisquete"
								rendered="#{!detalleDisqueteForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Disquete
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{detalleDisqueteForm.findSelectDisquete}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.findColDisquete}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{detalleDisqueteForm.findDetalleDisqueteByDisquete}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultDetalleDisqueteByDisquete"
								rendered="#{detalleDisqueteForm.showDetalleDisqueteByDisquete&&
								!detalleDisqueteForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{detalleDisqueteForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{detalleDisqueteForm.selectDetalleDisquete}"
                                						styleClass="listitem">
                                						<f:param name="idDetalleDisquete" value="#{result.idDetalleDisquete}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataDetalleDisquete"
								rendered="#{detalleDisqueteForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!detalleDisqueteForm.editing&&!detalleDisqueteForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{detalleDisqueteForm.editing&&!detalleDisqueteForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{detalleDisqueteForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{detalleDisqueteForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="detalleDisqueteForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{detalleDisqueteForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!detalleDisqueteForm.editing&&!detalleDisqueteForm.deleting&&detalleDisqueteForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{detalleDisqueteForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!detalleDisqueteForm.editing&&!detalleDisqueteForm.deleting&&detalleDisqueteForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{detalleDisqueteForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{detalleDisqueteForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{detalleDisqueteForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{detalleDisqueteForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{detalleDisqueteForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{detalleDisqueteForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Disquete
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.selectDisquete}"
														id="Disquete"
                                                    	disabled="#{!detalleDisqueteForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Disquete">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.colDisquete}" />
                                                    </h:selectOneMenu>													<h:message for="Disquete" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo registro
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.detalleDisquete.tipoRegistro}"
	                                                    id="TipoRegistro"
                                                    	disabled="#{!detalleDisqueteForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Tipo registro">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.listTipoRegistro}" />
                                                    </h:selectOneMenu>													<h:message for="TipoRegistro" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Campo N�
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="NumeroCampo"
	                        							maxlength="2"
                        								value="#{detalleDisqueteForm.detalleDisquete.numeroCampo}"
                        								readonly="true"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="NumeroCampo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo campo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.detalleDisquete.tipoCampo}"
	                                                    id="TipoCampo"
                                                    	disabled="#{!detalleDisqueteForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Tipo campo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.listTipoCampo}" />
                                                    </h:selectOneMenu>													<h:message for="TipoCampo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Longitud Campo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="3"
	                        							id="LongitudCampo"
	                        							maxlength="3"
                        								value="#{detalleDisqueteForm.detalleDisquete.longitudCampo}"
                        								readonly="#{!detalleDisqueteForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="LongitudCampo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Campo Valor BD
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.detalleDisquete.campoBaseDatos}"
	                                                    id="CampoBaseDatos"
                                                    	disabled="#{!detalleDisqueteForm.editing}"
                        								 rendered="#{detalleDisqueteForm.showCampoBaseDatosAux}"                         								                                                    	immediate="false"
                                                    	required="false" 
														title="Campo Valor BD">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.listCampoBaseDatos}" />
                                                    </h:selectOneMenu>													<h:message for="CampoBaseDatos" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Campo Valor BD (Totales)
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.detalleDisquete.campoTotales}"
	                                                    id="CampoTotales"
                                                    	disabled="#{!detalleDisqueteForm.editing}"
                        								 rendered="#{detalleDisqueteForm.showCampoTotalesAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Campo Valor BD (Totales)">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.listCampoTotales}" />
                                                    </h:selectOneMenu>													<h:message for="CampoTotales" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Campo Valor Fijo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="100"
	                        							id="CampoUsuario"
	                        							maxlength="100"
                        								value="#{detalleDisqueteForm.detalleDisquete.campoUsuario}"
                        								readonly="#{!detalleDisqueteForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        								 rendered="#{detalleDisqueteForm.showCampoUsuarioAux}" 														                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="CampoUsuario" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Campo Entrada
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.detalleDisquete.campoEntrada}"
	                                                    id="CampoEntrada"
                                                    	disabled="#{!detalleDisqueteForm.editing}"
                        								 rendered="#{detalleDisqueteForm.showCampoEntradaAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Campo Entrada">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.listCampoEntrada}" />
                                                    </h:selectOneMenu>													<h:message for="CampoEntrada" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Separador miles
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.detalleDisquete.separadorMiles}"
	                                                    id="SeparadorMiles"
                                                    	disabled="#{!detalleDisqueteForm.editing}"
                        								 rendered="#{detalleDisqueteForm.showSeparadorMilesAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Separador miles">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.listSeparadorMiles}" />
                                                    </h:selectOneMenu>	<h:message for="SeparadorMiles" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Separador decimales
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.detalleDisquete.separadorDecimal}"
	                                                    id="SeparadorDecimal"
                                                    	disabled="#{!detalleDisqueteForm.editing}"
                        								 rendered="#{detalleDisqueteForm.showSeparadorDecimalAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Separador decimales">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.listSeparadorDecimal}" />
                                                    </h:selectOneMenu>	<h:message for="SeparadorDecimal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Alineaci�n
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.detalleDisquete.alineacionCampo}"
	                                                    id="AlineacionCampo"
                                                    	disabled="#{!detalleDisqueteForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Alineaci�n">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.listAlineacionCampo}" />
                                                    </h:selectOneMenu>													<h:message for="AlineacionCampo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se rellena con
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.detalleDisquete.rellenarCero}"
	                                                    id="RellenarCero"
                                                    	disabled="#{!detalleDisqueteForm.editing}"                        								                      								                                                    	immediate="false"
                                                    	required="true"
														title="Se rellena con">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.listRellenarCero}"/>
                                                    </h:selectOneMenu>													<h:message for="RellenarCero" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Multiplicador
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="Multiplicador"
	                        							maxlength="10"
                        								value="#{detalleDisqueteForm.detalleDisquete.multiplicador}"
                        								readonly="#{!detalleDisqueteForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								rendered="#{detalleDisqueteForm.showMultiplicadorAux}" 	
                        								style="text-align:right"       
                        								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        							<h:message for="Multiplicador" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleDisqueteForm.selectConcepto}"
														id="Concepto"
                                                    	disabled="#{!detalleDisqueteForm.editing}"
                        								immediate="false"
                                                    	required="false"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleDisqueteForm.colConcepto}" />
                                                    </h:selectOneMenu>	<h:message for="Concepto" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{detalleDisqueteForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!detalleDisqueteForm.editing&&!detalleDisqueteForm.deleting&&detalleDisqueteForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{detalleDisqueteForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!detalleDisqueteForm.editing&&!detalleDisqueteForm.deleting&&detalleDisqueteForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{detalleDisqueteForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{detalleDisqueteForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{detalleDisqueteForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{detalleDisqueteForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{detalleDisqueteForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{detalleDisqueteForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>