<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{conceptoDependenciaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formConceptoDependencia">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Conceptos por Tipo Dependencia
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/ConceptoDependencia.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{conceptoDependenciaForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!conceptoDependenciaForm.editing&&!conceptoDependenciaForm.deleting&&conceptoDependenciaForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchConceptoDependencia"
								rendered="#{!conceptoDependenciaForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
						                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Caracteristica
                        						</td>
                        						<td></f:verbatim>
				                                                    <h:selectOneMenu value="#{conceptoDependenciaForm.findSelectTipoCaracteristicaForCaracteristicaDependencia}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{conceptoDependenciaForm.findChangeTipoCaracteristicaForCaracteristicaDependencia}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{conceptoDependenciaForm.findColTipoCaracteristicaForCaracteristicaDependencia}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
																<f:verbatim><tr>
												<td>
													Clasificacion Dependencia
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{conceptoDependenciaForm.findSelectCaracteristicaDependencia}"
                                                    	rendered="#{conceptoDependenciaForm.findShowCaracteristicaDependencia}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoDependenciaForm.findColCaracteristicaDependencia}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{conceptoDependenciaForm.findConceptoDependenciaByCaracteristicaDependencia}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConceptoDependenciaByCaracteristicaDependencia"
								rendered="#{conceptoDependenciaForm.showConceptoDependenciaByCaracteristicaDependencia&&
								!conceptoDependenciaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoDependenciaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoDependenciaForm.selectConceptoDependencia}"
                                						styleClass="listitem">
                                						<f:param name="idConceptoDependencia" value="#{result.idConceptoDependencia}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataConceptoDependencia"
								rendered="#{conceptoDependenciaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!conceptoDependenciaForm.editing&&!conceptoDependenciaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{conceptoDependenciaForm.editing&&!conceptoDependenciaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{conceptoDependenciaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{conceptoDependenciaForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="conceptoDependenciaForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{conceptoDependenciaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoDependenciaForm.editing&&!conceptoDependenciaForm.deleting&&conceptoDependenciaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{conceptoDependenciaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoDependenciaForm.editing&&!conceptoDependenciaForm.deleting&&conceptoDependenciaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{conceptoDependenciaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{conceptoDependenciaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{conceptoDependenciaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{conceptoDependenciaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{conceptoDependenciaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{conceptoDependenciaForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Tipo Caracteristica
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoDependenciaForm.selectTipoCaracteristicaForCaracteristicaDependencia}"
                                                    	disabled="#{!conceptoDependenciaForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="TipoCaracteristicaForCaracteristicaDependencia"
														valueChangeListener="#{conceptoDependenciaForm.changeTipoCaracteristicaForCaracteristicaDependencia}"
                        																						immediate="false"
														required="false"
														title="Clasificacion Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{conceptoDependenciaForm.colTipoCaracteristicaForCaracteristicaDependencia}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Clasificacion Dependencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoDependenciaForm.selectCaracteristicaDependencia}"
	                                                    id="caracteristicaDependencia"
                                                    	disabled="#{!conceptoDependenciaForm.editing}"
                                                    	rendered="#{conceptoDependenciaForm.showCaracteristicaDependencia
														 }"
                                                    	immediate="false"
                                                    	required="false"
														title="Clasificacion Dependencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoDependenciaForm.colCaracteristicaDependencia}" />
                                                    </h:selectOneMenu>													<h:message for="caracteristicaDependencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoDependenciaForm.selectTipoPersonalForConceptoTipoPersonal}"
                                                    	disabled="#{!conceptoDependenciaForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="TipoPersonalForConceptoTipoPersonal"
														valueChangeListener="#{conceptoDependenciaForm.changeTipoPersonalForConceptoTipoPersonal}"
                        																						immediate="false"
														required="false"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{conceptoDependenciaForm.colTipoPersonalForConceptoTipoPersonal}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoDependenciaForm.selectConceptoTipoPersonal}"
	                                                    id="conceptoTipoPersonal"
                                                    	disabled="#{!conceptoDependenciaForm.editing}"
                                                    	rendered="#{conceptoDependenciaForm.showConceptoTipoPersonal
														 }"
                                                    	immediate="false"
                                                    	required="false"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoDependenciaForm.colConceptoTipoPersonal}" />
                                                    </h:selectOneMenu>													<h:message for="conceptoTipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Unidades
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="unidades"
	                        							maxlength="10"
                        								value="#{conceptoDependenciaForm.conceptoDependencia.unidades}"
                        								readonly="#{!conceptoDependenciaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="unidades" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Monto
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="monto"
	                        							maxlength="10"
                        								value="#{conceptoDependenciaForm.conceptoDependencia.monto}"
                        								readonly="#{!conceptoDependenciaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="monto" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Autom�tico al Ingresar
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoDependenciaForm.conceptoDependencia.automaticoIngreso}"
	                                                    id="automaticoIngreso"
                                                    	disabled="#{!conceptoDependenciaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Autom�tico al Ingresar">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoDependenciaForm.listAutomaticoIngreso}" />
                                                    </h:selectOneMenu>													<h:message for="automaticoIngreso" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{conceptoDependenciaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoDependenciaForm.editing&&!conceptoDependenciaForm.deleting&&conceptoDependenciaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{conceptoDependenciaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoDependenciaForm.editing&&!conceptoDependenciaForm.deleting&&conceptoDependenciaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{conceptoDependenciaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{conceptoDependenciaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{conceptoDependenciaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{conceptoDependenciaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{conceptoDependenciaForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{conceptoDependenciaForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>