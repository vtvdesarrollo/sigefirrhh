<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		function windowReport() {
			var number = 
				document.forms['formSemana'].elements['formSemana:reportId'].value;
			var name = 
				document.forms['formSemana'].elements['formSemana:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
		
 	</script>
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{semanaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formSemana">
				<h:inputHidden id="reportId" value="#{semanaForm.reportId}" />
    		<h:inputHidden id="reportName" value="#{semanaForm.reportName}" />
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Per�odos Semanales
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/Semana.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/report.gif"
														action="#{semanaForm.runReport}"
        												onclick="windowReport()"
														rendered="#{!semanaForm.editing&&!semanaForm.deleting}" />
													<h:commandButton image="/images/add.gif"
														action="#{semanaForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!semanaForm.editing&&!semanaForm.deleting&&semanaForm.login.agregar}"
														 />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="search1"
								rendered="#{!semanaForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Grupo de N�mina
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{semanaForm.findSelectGrupoNomina}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{semanaForm.findColGrupoNomina}" />
                                                    </h:selectOneMenu>																		
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													A�o
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="4"
														maxlength="4"
														value="#{semanaForm.findAnio}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyIntegerCheck(event, this)" />														

															<h:commandButton image="/images/find.gif" 
														action="#{semanaForm.findSemanaByGrupoNominaAndAnio}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultSemanaByGrupoNomina"
								rendered="#{semanaForm.showSemanaByGrupoNomina&&
								!semanaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{semanaForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{semanaForm.selectSemana}"
                                						styleClass="listitem">
                                						<f:param name="idSemana" value="#{result.idSemana}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultSemanaByAnio"
								rendered="#{semanaForm.showSemanaByAnio&&
								!semanaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{semanaForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{semanaForm.selectSemana}"
                                						styleClass="listitem">
                                						<f:param name="idSemana" value="#{result.idSemana}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataSemana"
								rendered="#{semanaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!semanaForm.editing&&!semanaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{semanaForm.editing&&!semanaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{semanaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{semanaForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="semanaForm">
<h:inputHidden id="scrollx" value="#{semanaForm.scrollx}" />
<h:inputHidden id="scrolly" value="#{semanaForm.scrolly}" />
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{semanaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!semanaForm.editing&&!semanaForm.deleting&&semanaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{semanaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!semanaForm.editing&&!semanaForm.deleting&&semanaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{semanaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{semanaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{semanaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{semanaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{semanaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{semanaForm.abort}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Grupo de N�mina
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{semanaForm.selectGrupoNomina}"
														id="GrupoNomina"
                                                    	disabled="#{!semanaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Grupo de N�mina">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{semanaForm.colGrupoNomina}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							A�o
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="Anio"
	                        							maxlength="4"
                        								value="#{semanaForm.semana.anio}"
                        								readonly="#{!semanaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Semana del A�o
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="SemanadelAnio"
	                        							maxlength="2"
                        								value="#{semanaForm.semana.semanaAnio}"
                        								readonly="#{!semanaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Mes
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{semanaForm.semana.mes}"
	                                                    id="Mes"
                                                    	disabled="#{!semanaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Mes">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{semanaForm.listMes}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Semana del Mes
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="1"
	                        							id="SemanadelMes"
	                        							maxlength="1"
                        								value="#{semanaForm.semana.semanaMes}"
                        								readonly="#{!semanaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha Inicio
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="FechaInicio"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{semanaForm.semana.fechaInicio}"
                        								readonly="#{!semanaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								onblur="javascript:check_date(this)"
                        								required="true">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha Fin
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="FechaFin"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{semanaForm.semana.fechaFin}"
                        								readonly="#{!semanaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								onblur="javascript:check_date(this)"
                        								required="true">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{semanaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!semanaForm.editing&&!semanaForm.deleting&&semanaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{semanaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!semanaForm.editing&&!semanaForm.deleting&&semanaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{semanaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{semanaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{semanaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{semanaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{semanaForm.editing}" />
													<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{semanaForm.abort}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />
                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>