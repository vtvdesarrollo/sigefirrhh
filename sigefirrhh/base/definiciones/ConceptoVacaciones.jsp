<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
			function windowReport() {
				var number = 
					document.forms['formConceptoVacaciones'].elements['formConceptoVacaciones:reportId'].value;
				var url = '/sigefirrhh/reports/process.jsp?reportName=ConceptoVacaciones' + number;
				window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
	    	}	
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{conceptoVacacionesForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formConceptoVacaciones">
<h:inputHidden id="reportId" value="#{conceptoVacacionesForm.reportId}" />
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											F�rmula Bono Vacacional
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/ConceptoVacaciones.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
												<h:commandButton image="/images/report.gif"
														action="#{conceptoVacacionesForm.runReport}"
        												onclick="windowReport()"        												
														rendered="#{!conceptoVacacionesForm.editing&&!conceptoVacacionesForm.deleting}" />
														
													<h:commandButton image="/images/add.gif"
														action="#{conceptoVacacionesForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!conceptoVacacionesForm.editing&&!conceptoVacacionesForm.deleting&&conceptoVacacionesForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchConceptoVacaciones"
								rendered="#{!conceptoVacacionesForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Tipo Personal
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{conceptoVacacionesForm.findSelectTipoPersonal}"
					                                                    
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVacacionesForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{conceptoVacacionesForm.findConceptoVacacionesByTipoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConceptoVacacionesByTipoPersonal"
								rendered="#{conceptoVacacionesForm.showConceptoVacacionesByTipoPersonal&&
								!conceptoVacacionesForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoVacacionesForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoVacacionesForm.selectConceptoVacaciones}"
                                						styleClass="listitem">
                                						<f:param name="idConceptoVacaciones" value="#{result.idConceptoVacaciones}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataConceptoVacaciones"
								rendered="#{conceptoVacacionesForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!conceptoVacacionesForm.editing&&!conceptoVacacionesForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{conceptoVacacionesForm.editing&&!conceptoVacacionesForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{conceptoVacacionesForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{conceptoVacacionesForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="conceptoVacacionesForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{conceptoVacacionesForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoVacacionesForm.editing&&!conceptoVacacionesForm.deleting&&conceptoVacacionesForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{conceptoVacacionesForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoVacacionesForm.editing&&!conceptoVacacionesForm.deleting&&conceptoVacacionesForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{conceptoVacacionesForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{conceptoVacacionesForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{conceptoVacacionesForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{conceptoVacacionesForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{conceptoVacacionesForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{conceptoVacacionesForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoVacacionesForm.selectTipoPersonal}"
														id="TipoPersonal"
                                                    	disabled="#{!conceptoVacacionesForm.editing}"
                        								 onchange="this.form.submit()"
					                                                    
														valueChangeListener="#{conceptoVacacionesForm.changeTipoPersonal}"                       								                                                    	immediate="false"
                                                    	required="true"
														title="Tipo Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVacacionesForm.colTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="TipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoVacacionesForm.selectConceptoTipoPersonal}"
														id="ConceptoTipoPersonal"
                                                    	disabled="#{!conceptoVacacionesForm.editing}"
                        								rendered="#{conceptoVacacionesForm.showConcepto}"                       								                                                    	
                        								immediate="false"
                                                    	required="true"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVacacionesForm.colConceptoTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="ConceptoTipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo C�lculo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoVacacionesForm.conceptoVacaciones.tipo}"
	                                                    id="Tipo"
                                                    	disabled="#{!conceptoVacacionesForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Tipo C�lculo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVacacionesForm.listTipo}" />
                                                    </h:selectOneMenu>													<h:message for="Tipo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							A partir del �ltimo mes cerrado?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoVacacionesForm.conceptoVacaciones.mesCerrado}"
	                                                    id="MesCerrado"
                                                    	disabled="#{!conceptoVacacionesForm.editing}"
                        								 rendered="#{conceptoVacacionesForm.showMesCerradoAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="A partir del �ltimo mes cerrado?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVacacionesForm.listMesCerrado}" />
                                                    </h:selectOneMenu>													<h:message for="MesCerrado" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Meses de 30 d�as?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoVacacionesForm.conceptoVacaciones.mes30}"
	                                                    id="Mes30"
                                                    	disabled="#{!conceptoVacacionesForm.editing}"
                        								 rendered="#{conceptoVacacionesForm.showMes30Aux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Meses de 30 d�as?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVacacionesForm.listMes30}" />
                                                    </h:selectOneMenu>													<h:message for="Mes30" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Meses a promediar
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="NumeroMeses"
	                        							maxlength="2"
                        								value="#{conceptoVacacionesForm.conceptoVacaciones.numeroMeses}"
                        								readonly="#{!conceptoVacacionesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								 rendered="#{conceptoVacacionesForm.showNumeroMesesAux}" 														                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="NumeroMeses" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Factor
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="Factor"
	                        							maxlength="6"
                        								value="#{conceptoVacacionesForm.conceptoVacaciones.factor}"
                        								readonly="#{!conceptoVacacionesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="Factor" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tope Monto
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="TopeMonto"
	                        							maxlength="16"
                        								value="#{conceptoVacacionesForm.conceptoVacaciones.topeMonto}"
                        								readonly="#{!conceptoVacacionesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="TopeMonto" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tope Unidades
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="TopeUnidades"
	                        							maxlength="16"
                        								value="#{conceptoVacacionesForm.conceptoVacaciones.topeUnidades}"
                        								readonly="#{!conceptoVacacionesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="TopeUnidades" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cruce de alicuotas?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoVacacionesForm.conceptoVacaciones.alicuotaVacacional}"
	                                                    id="AlicuotaVacacional"
                                                    	disabled="#{!conceptoVacacionesForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Cruce de alicuotas?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVacacionesForm.listAlicuotaVacacional}" />
                                                    </h:selectOneMenu>													<h:message for="AlicuotaVacacional" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto Alicuota
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoVacacionesForm.selectConceptoAlicuota}"
														id="ConceptoAlicuota"
                                                    	disabled="#{!conceptoVacacionesForm.editing}"
                        								 rendered="#{conceptoVacacionesForm.showConceptoAlicuotaAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Concepto Alicuota">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoVacacionesForm.colConceptoAlicuota}" />
                                                    </h:selectOneMenu>													<h:message for="ConceptoAlicuota" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{conceptoVacacionesForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoVacacionesForm.editing&&!conceptoVacacionesForm.deleting&&conceptoVacacionesForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{conceptoVacacionesForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoVacacionesForm.editing&&!conceptoVacacionesForm.deleting&&conceptoVacacionesForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{conceptoVacacionesForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{conceptoVacacionesForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{conceptoVacacionesForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{conceptoVacacionesForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{conceptoVacacionesForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{conceptoVacacionesForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>