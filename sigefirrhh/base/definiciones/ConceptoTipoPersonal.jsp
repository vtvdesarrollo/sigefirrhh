<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		    	function windowReport() {
				var number = 
					document.forms['formConceptoTipoPersonal'].elements['formConceptoTipoPersonal:reportId'].value;
				var url = '/sigefirrhh/reports/process.jsp?reportName=ConceptoTipoPersonal' + number;
				window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
	    	}
		
 	</script>
		
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{conceptoTipoPersonalForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formConceptoTipoPersonal">
<h:inputHidden id="reportId" value="#{conceptoTipoPersonalForm.reportId}" />
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Conceptos por Tipo de Personal
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/ConceptoTipoPersonal.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/report.gif"
														action="#{conceptoTipoPersonalForm.runReport}"
        												onclick="windowReport()"
														rendered="#{!conceptoTipoPersonalForm.editing&&!conceptoTipoPersonalForm.deleting}" />
													<h:commandButton image="/images/add.gif"
														action="#{conceptoTipoPersonalForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!conceptoTipoPersonalForm.editing&&!conceptoTipoPersonalForm.deleting&&conceptoTipoPersonalForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="search1"
								rendered="#{!conceptoTipoPersonalForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Tipo Personal
												</td>
												<td width="100%"></f:verbatim>
					                                 <h:selectOneMenu value="#{conceptoTipoPersonalForm.findSelectTipoPersonal}"
                                                    	immediate="false">														
                                                    	<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>													
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Concepto
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.findSelectConcepto}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Todos" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.findColConcepto}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{conceptoTipoPersonalForm.findConceptoTipoPersonalByTipoPersonalAndConcepto}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConceptoTipoPersonalByTipoPersonal"
								rendered="#{conceptoTipoPersonalForm.showConceptoTipoPersonalByTipoPersonal&&
								!conceptoTipoPersonalForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoTipoPersonalForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoTipoPersonalForm.selectConceptoTipoPersonal}"
                                						styleClass="listitem">
                                						<f:param name="idConceptoTipoPersonal" value="#{result.idConceptoTipoPersonal}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConceptoTipoPersonalByConcepto"
								rendered="#{conceptoTipoPersonalForm.showConceptoTipoPersonalByConcepto&&
								!conceptoTipoPersonalForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoTipoPersonalForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoTipoPersonalForm.selectConceptoTipoPersonal}"
                                						styleClass="listitem">
                                						<f:param name="idConceptoTipoPersonal" value="#{result.idConceptoTipoPersonal}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataConceptoTipoPersonal"
								rendered="#{conceptoTipoPersonalForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!conceptoTipoPersonalForm.editing&&!conceptoTipoPersonalForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{conceptoTipoPersonalForm.editing&&!conceptoTipoPersonalForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{conceptoTipoPersonalForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{conceptoTipoPersonalForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>

<h:form id="conceptoTipoPersonalForm">
<h:inputHidden id="scrollx" value="#{conceptoTipoPersonalForm.scrollx}" />
<h:inputHidden id="scrolly" value="#{conceptoTipoPersonalForm.scrolly}" />
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{conceptoTipoPersonalForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoTipoPersonalForm.editing&&!conceptoTipoPersonalForm.deleting&&conceptoTipoPersonalForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{conceptoTipoPersonalForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoTipoPersonalForm.editing&&!conceptoTipoPersonalForm.deleting&&conceptoTipoPersonalForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{conceptoTipoPersonalForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{conceptoTipoPersonalForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{conceptoTipoPersonalForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{conceptoTipoPersonalForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{conceptoTipoPersonalForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{conceptoTipoPersonalForm.abort}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.selectTipoPersonal}"
														id="TipoPersonal"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                        								immediate="false"
                                                    	required="true"
                                                    	onchange="this.form.submit()"
                                                    	valueChangeListener="#{conceptoTipoPersonalForm.changeTipoPersonalForFrecuenciaTipoPersonal}"
														title="Tipo Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.colTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.selectConcepto}"
														id="Concepto"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.colConcepto}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Frecuencia
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.selectFrecuenciaTipoPersonal}"
	                                                    id="FrecuenciaTipoPersonal"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                                                    	rendered="#{conceptoTipoPersonalForm.showFrecuenciaTipoPersonal}"
                                                    	immediate="false"
                                                    	required="false"
														title="Frecuencia">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoTipoPersonalForm.colFrecuenciaTipoPersonal}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.tipo}"
	                                                    id="Tipo"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Tipo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.listTipo}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Monto
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="Monto"
	                        							maxlength="10"
                        								value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.valor}"
                        								readonly="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="4" maxFractionDigits="4" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        								                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Unidades
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="Unidades"
	                        							maxlength="10"
                        								value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.unidades}"
                        								readonly="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        								                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Ingreso Autom�tico
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.automaticoIngreso}"
	                                                    id="AutomaticoIngreso"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Ingreso Autom�tico">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.listAutomaticoIngreso}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se realiza rec�lculo autom�tico?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.recalculo}"
	                                                    id="Recalculo"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Se realiza rec�lculo autom�tico?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.listRecalculo}" />
                                                    </h:selectOneMenu>													<h:message for="Recalculo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tope M�ximo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="TopeMaximo"
	                        							maxlength="10"
                        								value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.topeMaximo}"
                        								readonly="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"                         								
                        								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        								                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tope M�nimo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="TopeMinimo"
	                        							maxlength="10"
                        								value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.topeMinimo}"
                        								readonly="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"                         								
                        								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        								                        						<f:verbatim></td>
                        					</tr></f:verbatim>                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Tope Anual
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="TopeAnual"
	                        							maxlength="10"
                        								value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.topeAnual}"
                        								readonly="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        					<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Es base jubilacion?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.baseJubilacion}"
	                                                    id="BaseJubilacion"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Es base Jubilacion?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.listBaseJubilacion}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Formulaci�n Anual
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.formulaConcepto}"
	                                                    id="FormulaConcepto"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="this.form.submit()"                       								                                                    	                        								                        								                                                    	
                        								immediate="false"
                                                    	required="false"
														title="Tipo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.listFormulaConcepto}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Multiplicador
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="Multiplicador"
	                        							maxlength="10"
                        								value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.multiplicadorFormula}"
                        								readonly="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								rendered="#{conceptoTipoPersonalForm.showMultiplicadorFormula}"   
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        					<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Se Refleja en Movimientos?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.reflejaMovimiento}"
	                                                    id="ReflejaMovimiento"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="this.form.submit()"                       								                                                    	                        								                        								                                                    	
                        								immediate="false"
                                                    	required="false"
														title="Tipo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.listReflejaMovimiento}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					 <f:verbatim><tr>
                        						<td>
                        							Lo Aprueba MPF?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.aprobacionMpd}"
	                                                    id="AprobacionMpd"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="this.form.submit()"                       								                                                    	                        								                        								                                                    	
                        								immediate="false"
                                                    	required="false"
														title="Tipo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.listAprobacionMpd}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					 <f:verbatim><tr>
                        						<td>
                        							Distribuci�n Presupuestaria
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.distribucion}"
	                                                    id="Distribucion"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="this.form.submit()"                       								                                                    	                        								                        								                                                    	
                        								immediate="false"
                                                    	required="false"
														title="Distribucion Presupuestaria">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoTipoPersonalForm.listDistribucion}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Contrato Colectivo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoTipoPersonalForm.selectContratoColectivo}"
	                                                    id="ContratoColectivo"
                                                    	disabled="#{!conceptoTipoPersonalForm.editing}"
                                                    	immediate="false"
                                                    	required="false"
														title="Contrato Colectivo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoTipoPersonalForm.colContratoColectivo}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Base Legal
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputTextarea
	                        							cols="100"
	                        							rows="4"
	                        							id="BaseLegal"
                        								value="#{conceptoTipoPersonalForm.conceptoTipoPersonal.baseLegal}"
                        								readonly="#{!conceptoTipoPersonalForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"                        								
                        								onkeypress="return keyEnterCheck(event, this)"                        								
                        								required="false" />													
                        								<h:message for="BaseLegal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{conceptoTipoPersonalForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoTipoPersonalForm.editing&&!conceptoTipoPersonalForm.deleting&&conceptoTipoPersonalForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{conceptoTipoPersonalForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoTipoPersonalForm.editing&&!conceptoTipoPersonalForm.deleting&&conceptoTipoPersonalForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{conceptoTipoPersonalForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{conceptoTipoPersonalForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{conceptoTipoPersonalForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{conceptoTipoPersonalForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{conceptoTipoPersonalForm.editing}" />
													<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{conceptoTipoPersonalForm.abort}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />
                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>
