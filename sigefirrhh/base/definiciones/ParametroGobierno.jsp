<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{parametroGobiernoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formParametroGobierno">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Par�metros Retenciones
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/ParametroGobierno.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{parametroGobiernoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!parametroGobiernoForm.editing&&!parametroGobiernoForm.deleting&&parametroGobiernoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
								<f:subview 
									id="search1"
									rendered="#{!parametroGobiernoForm.showData}">
								<f:verbatim>
								<tr>
									<td>
									</f:verbatim>
										<f:verbatim>
										<table width="100%" class="bandtable">
											<tr>
												<td>Buscar</td>
											</tr>
										</table>
										<table width="100%" class="datatable">
											<tr>
												<td>Grupo Organismo</td>
												<td width="100%">
												</f:verbatim>
					                                <h:selectOneMenu value="#{parametroGobiernoForm.findSelectGrupoOrganismo}"
                                                    immediate="false">
													<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    <f:selectItems value="#{parametroGobiernoForm.findColGrupoOrganismo}" />
                                                    </h:selectOneMenu>
													<h:commandButton image="/images/find.gif" 
													action="#{parametroGobiernoForm.findParametroGobiernoByGrupoOrganismo}"
													onclick="javascript:return clickMade()" />
												<f:verbatim>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								</f:verbatim>
								</f:subview>
							<f:verbatim>
							</table>
							</f:verbatim>
							
							<f:subview
								id="viewResultParametroGobiernoByGrupoOrganismo"
								rendered="#{parametroGobiernoForm.showParametroGobiernoByGrupoOrganismo&&
								!parametroGobiernoForm.showData}">
								<f:verbatim>
								<table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{parametroGobiernoForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{parametroGobiernoForm.selectParametroGobierno}"
                                						styleClass="listitem">
                                						<f:param name="idParametroGobierno" value="#{result.idParametroGobierno}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table>
								</f:verbatim>
							</f:subview>
						</h:form>
							<f:subview 
								id="viewDataParametroGobierno"
								rendered="#{parametroGobiernoForm.showData}">
								<f:verbatim>
								<table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!parametroGobiernoForm.editing&&!parametroGobiernoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{parametroGobiernoForm.editing&&!parametroGobiernoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{parametroGobiernoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{parametroGobiernoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="parametroGobiernoForm">

                            			<f:verbatim>
										<table class="datatable" width="100%">	
                        					<tr>
	                        					<td colspan="4">
                                    				<table align="center">
                                    					<tr>
                                    						<td>
															</f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{parametroGobiernoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!parametroGobiernoForm.editing&&!parametroGobiernoForm.deleting&&parametroGobiernoForm.login.modificar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{parametroGobiernoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{parametroGobiernoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{parametroGobiernoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{parametroGobiernoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{parametroGobiernoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{parametroGobiernoForm.abort}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr>
											</f:verbatim>
                        					<f:verbatim>
											<tr>
                        						<td>
                        							Grupo Organismo
                        						</td>
                        						<td colspan="3">
												</f:verbatim>
                                                    <h:selectOneMenu value="#{parametroGobiernoForm.selectGrupoOrganismo}"
														id="GrupoOrganismo"
                                                    	disabled="#{!parametroGobiernoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Grupo Organismo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroGobiernoForm.colGrupoOrganismo}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td height="2" colspan="4" class="bandtable"></td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td><b>SSO</b></td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td width="30%">L�mite semanal</td>
												<td width="20%">
												</f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="LimitesemanalSSO"
	                        							maxlength="16"
                        								value="#{parametroGobiernoForm.parametroGobierno.limsemSso}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td width="30%">L�mite mensual</td>
												<td width="20%">
												</f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="LimitemensualSSO"
	                        							maxlength="16"
                        								value="#{parametroGobiernoForm.parametroGobierno.limmenSso}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>L�mite edad masculino</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="LimiteedadmasculinoSSO"
	                        							maxlength="2"
                        								value="#{parametroGobiernoForm.parametroGobierno.edadmascSso}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        								required="false">
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>L�mite edad femenino</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="LimiteedadfemeninoSSO"
	                        							maxlength="2"
                        								value="#{parametroGobiernoForm.parametroGobierno.edadfemSso}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
														required="false">
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>% Trabajador Regimen Integral</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="TrabajadorRegimenIntegral"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porcentajeIntegral}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>% Trabajador Regimen Parcial</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="TrabajadorRegimenParcial"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porcentajeParcial}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>% patr�n riesgo bajo</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="patronriesgobajoSSO"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porcbpatSso}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>% patr�n riesgo medio</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="patronriesgomedioSSO"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porcmpatSso}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>% trabajador riesgo alto</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="trabajadorriesgoaltoSSO"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porcapatSso}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td></td>
												<td></td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td height="2" colspan="4" class="bandtable"></td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td><b>SPF</b></td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td width="30%">L�mite semanal</td>
												<td width="20%">
												</f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="LimitesemanalSPF"
	                        							maxlength="16"
                        								value="#{parametroGobiernoForm.parametroGobierno.limsemSpf}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td width="30%">L�mite mensual</td>
												<td width="20%">
												</f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="LimitemensualSPF"
	                        							maxlength="16"
                        								value="#{parametroGobiernoForm.parametroGobierno.limmenSpf}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>% trabajador</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="trabajadorSPF"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porctrabSpf}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>% patr�n</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="patronSPF"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porcpatSpf}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td height="2" colspan="4" class="bandtable"></td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td><b>FAOV</b></td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>L�mite semanal</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="LimitesemanalLPH"
	                        							maxlength="16"
                        								value="#{parametroGobiernoForm.parametroGobierno.limsemLph}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>L�mite mensual</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="LimitemensualLPH"
	                        							maxlength="16"
                        								value="#{parametroGobiernoForm.parametroGobierno.limmenLph}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>L�mite Edad masculino</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="LimiteEdadmasculinoLPH"
	                        							maxlength="2"
                        								value="#{parametroGobiernoForm.parametroGobierno.edadmascLph}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        								required="false">
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>L�mite Edad femenino</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="LimiteEdadfemeninoLPH"
	                        							maxlength="2"
                        								value="#{parametroGobiernoForm.parametroGobierno.edadfemLph}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        								required="false">
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>% trabajador</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="trabajadorLPH"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porctrabLph}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>% patr�n</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="patronLPH"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porcpatLph}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
                        						<td height="2" colspan="4" class="bandtable"></td>
                        					</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td><b>FJU</b></td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>L�mite semanal</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="LimitesemanalFJU"
	                        							maxlength="16"
                        								value="#{parametroGobiernoForm.parametroGobierno.limsemFju}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>L�mite mensual</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="LimitemensualFJU"
	                        							maxlength="16"
                        								value="#{parametroGobiernoForm.parametroGobierno.limmenFju}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>L�mite Edad masculino</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="LimiteEdadmasculinoFJU"
	                        							maxlength="2"
                        								value="#{parametroGobiernoForm.parametroGobierno.edadmascFju}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        								required="false">
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>L�mite Edad femenino</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="LimiteEdadfemeninoFJU"
	                        							maxlength="2"
                        								value="#{parametroGobiernoForm.parametroGobierno.edadfemFju}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        								required="false">
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
											<f:verbatim>
											<tr>
												<td>% trabajador</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="trabajadorFJU"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porctrabFju}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
												<td>% patr�n</td>
												<td>
												</f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="patronFJU"
	                        							maxlength="6"
                        								value="#{parametroGobiernoForm.parametroGobierno.porcpatFju}"
                        								readonly="#{!parametroGobiernoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        								style="text-align:right"
														required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        						<f:verbatim>
												</td>
											</tr>
											</f:verbatim>
                        				<f:verbatim>
										</table>
                        				<table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{parametroGobiernoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!parametroGobiernoForm.editing&&!parametroGobiernoForm.deleting&&parametroGobiernoForm.login.modificar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{parametroGobiernoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{parametroGobiernoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{parametroGobiernoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{parametroGobiernoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{parametroGobiernoForm.editing}" />
													<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{parametroGobiernoForm.abort}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />
                                                    <f:verbatim>
													
                                    			</td>
                                    		</tr>
										</table>
										</f:verbatim>
										</h:form>
                        				<f:verbatim>
										</td>
                        			</tr>
                        		</table>
								</f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>
