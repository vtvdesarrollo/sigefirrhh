<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		    function windowReport() {
				var number = 
					document.forms['formTipoPersonal'].elements['formTipoPersonal:reportId'].value;
				var url = '/sigefirrhh/reports/process.jsp?reportName=TipoPersonal' + number;
				window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
	    	}
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{tipoPersonalForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formTipoPersonal">
				<h:inputHidden id="reportId" value="#{tipoPersonalForm.reportId}" />
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Tipos de Personal
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/TipoPersonal.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/report.gif"
														action="#{tipoPersonalForm.runReport}"
        												onclick="windowReport()"        												
														rendered="#{!tipoPersonalForm.editing&&!tipoPersonalForm.deleting}" />
													<h:commandButton image="/images/add.gif"
														action="#{tipoPersonalForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!tipoPersonalForm.editing&&!tipoPersonalForm.deleting&&tipoPersonalForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchTipoPersonal"
								rendered="#{!tipoPersonalForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													C�digo
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="4"
														maxlength="4"
														value="#{tipoPersonalForm.findCodTipoPersonal}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{tipoPersonalForm.findTipoPersonalByCodTipoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Nombre
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="60"
														maxlength="60"
														value="#{tipoPersonalForm.findNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{tipoPersonalForm.findTipoPersonalByNombre}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultTipoPersonalByCodTipoPersonal"
								rendered="#{tipoPersonalForm.showTipoPersonalByCodTipoPersonal&&
								!tipoPersonalForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{tipoPersonalForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{tipoPersonalForm.selectTipoPersonal}"
                                						styleClass="listitem">
                                						<f:param name="idTipoPersonal" value="#{result.idTipoPersonal}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultTipoPersonalByNombre"
								rendered="#{tipoPersonalForm.showTipoPersonalByNombre&&
								!tipoPersonalForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{tipoPersonalForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{tipoPersonalForm.selectTipoPersonal}"
                                						styleClass="listitem">
                                						<f:param name="idTipoPersonal" value="#{result.idTipoPersonal}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataTipoPersonal"
								rendered="#{tipoPersonalForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!tipoPersonalForm.editing&&!tipoPersonalForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{tipoPersonalForm.editing&&!tipoPersonalForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{tipoPersonalForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{tipoPersonalForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="tipoPersonalForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{tipoPersonalForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!tipoPersonalForm.editing&&!tipoPersonalForm.deleting&&tipoPersonalForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{tipoPersonalForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!tipoPersonalForm.editing&&!tipoPersonalForm.deleting&&tipoPersonalForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{tipoPersonalForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{tipoPersonalForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{tipoPersonalForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{tipoPersonalForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{tipoPersonalForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{tipoPersonalForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�digo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="codTipoPersonal"
	                        							maxlength="2"
                        								value="#{tipoPersonalForm.tipoPersonal.codTipoPersonal}"
                        								readonly="#{!tipoPersonalForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="codTipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nombre
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="60"
	                        							id="nombre"
	                        							maxlength="60"
                        								value="#{tipoPersonalForm.tipoPersonal.nombre}"
                        								readonly="#{!tipoPersonalForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="nombre" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Clasificaci�n de Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.selectClasificacionPersonal}"
														id="clasificacionPersonal"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Clasificaci�n de Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.colClasificacionPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="clasificacionPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Grupo de N�mina
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.selectGrupoNomina}"
														id="grupoNomina"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="true"
														title="Grupo de N�mina">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.colGrupoNomina}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="grupoNomina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Grupo de Organismo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.selectGrupoOrganismo}"
														id="grupoOrganismo"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Grupo de Organismo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.colGrupoOrganismo}" />
                                                    </h:selectOneMenu>													<h:message for="grupoOrganismo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Movimientos sujetos a LEFP?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.aprobacionMpd}"
	                                                    id="aprobacionMpd"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Movimientos sujetos a LEFP?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listAprobacionMpd}" />
                                                    </h:selectOneMenu>													<h:message for="aprobacionMpd" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Maneja Registro de Cargos/Puestos?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.manejaRac}"
	                                                    id="manejaRac"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Maneja Registro de Cargos/Puestos?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listManejaRac}" />
                                                    </h:selectOneMenu>													<h:message for="manejaRac" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se calculan prestaciones?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.calculaPrestaciones}"
	                                                    id="calculaPrestaciones"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Se calculan prestaciones?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listCalculaPrestaciones}" />
                                                    </h:selectOneMenu>													<h:message for="calculaPrestaciones" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se asignan dotaciones?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.asignanDotaciones}"
	                                                    id="asignanDotaciones"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Se asignan dotaciones?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listAsignanDotaciones}" />
                                                    </h:selectOneMenu>													<h:message for="asignanDotaciones" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se realizan aumentos por Evaluaci�n?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.aumentoEvaluacion}"
	                                                    id="aumentoEvaluacion"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Se realizan aumentos por Evaluaci�n?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listAumentoEvaluacion}" />
                                                    </h:selectOneMenu>													<h:message for="aumentoEvaluacion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Goza Beneficio de Ticket Alimentaci�n?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.beneficioCestaTicket}"
	                                                    id="beneficioCestaTicket"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Goza Beneficio de Cesta Ticket?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listBeneficioCestaTicket}" />
                                                    </h:selectOneMenu>													<h:message for="beneficioCestaTicket" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Grupo de Ticket
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.selectGrupoTicket}"
														id="grupoTicket"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								 rendered="#{tipoPersonalForm.showGrupoTicketAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="Grupo de Ticket">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.colGrupoTicket}" />
                                                    </h:selectOneMenu>													<h:message for="grupoTicket" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							F�rmula Salario Integral?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.formulaIntegral}"
	                                                    id="formulaIntegral"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								 rendered="#{tipoPersonalForm.showFormulaIntegralAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="F�rmula Salario Integral?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listFormulaIntegral}" />
                                                    </h:selectOneMenu>													<h:message for="formulaIntegral" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							F�rmula Salario Semanal?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.formulaSemanal}"
	                                                    id="formulaSemanal"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								 rendered="#{tipoPersonalForm.showFormulaSemanalAux}"                         								                                                    	immediate="false"
                                                    	required="false"
														title="F�rmula Salario Semanal?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listFormulaSemanal}" />
                                                    </h:selectOneMenu>													<h:message for="formulaSemanal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Permite varios registros de un trabajador activo?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.multipleRegistro}"
	                                                    id="multipleRegistro"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Permite varios registros de un trabajador activo?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listMultipleRegistro}" />
                                                    </h:selectOneMenu>													<h:message for="multipleRegistro" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cotiza SSO
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.cotizaSso}"
	                                                    id="cotizaSso"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Cotiza SSO">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listCotizaSso}" />
                                                    </h:selectOneMenu>													<h:message for="cotizaSso" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cotiza SPF
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.cotizaSpf}"
	                                                    id="cotizaSpf"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Cotiza SPF">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listCotizaSpf}" />
                                                    </h:selectOneMenu>													<h:message for="cotizaSpf" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cotiza LPH
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.cotizaLph}"
	                                                    id="cotizaLph"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Cotiza LPH">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listCotizaLph}" />
                                                    </h:selectOneMenu>													<h:message for="cotizaLph" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cotiza FJU
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.cotizaFju}"
	                                                    id="cotizaFju"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Cotiza FJU">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listCotizaFju}" />
                                                    </h:selectOneMenu>													<h:message for="cotizaFju" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Disfruta Vacaciones
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.disfrutaVacaciones}"
	                                                    id="disfrutaVacaciones"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Disfruta Vacaciones">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listDisfrutaVacaciones}" />
                                                    </h:selectOneMenu>													<h:message for="disfrutaVacaciones" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Turno
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.selectTurno}"
														id="turno"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Turno">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.colTurno}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="turno" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tiene deuda de regimen derogado?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.deudaRegimenDerogado}"
	                                                    id="deudaRegimenDerogado"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Tiene deuda de regimen derogado?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listDeudaRegimenDerogado}" />
                                                    </h:selectOneMenu>													<h:message for="deudaRegimenDerogado" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Forma de Pago
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.tipoPersonal.formaPagoNomina}"
	                                                    id="formaPagoNomina"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Forma de Pago">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.listFormaPagoNomina}" />
                                                    </h:selectOneMenu>													<h:message for="formaPagoNomina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Banco Dep�sito N�mina
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.selectBancoNomina}"
														id="bancoNomina"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Banco N�mina">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.colBancoNomina}" />
                                                    </h:selectOneMenu>													<h:message for="bancoNomina" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Banco LPH
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.selectBancoLph}"
														id="bancoLph"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Banco LPH">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.colBancoLph}" />
                                                    </h:selectOneMenu>													<h:message for="bancoLph" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Banco Fideicomiso
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{tipoPersonalForm.selectBancoFid}"
														id="bancoFid"
                                                    	disabled="#{!tipoPersonalForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Banco Fideicomiso">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{tipoPersonalForm.colBancoFid}" />
                                                    </h:selectOneMenu>													<h:message for="bancoFid" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{tipoPersonalForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!tipoPersonalForm.editing&&!tipoPersonalForm.deleting&&tipoPersonalForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{tipoPersonalForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!tipoPersonalForm.editing&&!tipoPersonalForm.deleting&&tipoPersonalForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{tipoPersonalForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{tipoPersonalForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{tipoPersonalForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{tipoPersonalForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{tipoPersonalForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{tipoPersonalForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>