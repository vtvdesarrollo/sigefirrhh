<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{parametroVariosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formParametroVarios">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Par�metros Varios
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/ParametroVarios.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{parametroVariosForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!parametroVariosForm.editing&&!parametroVariosForm.deleting&&parametroVariosForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchParametroVarios"
								rendered="#{!parametroVariosForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Tipo Personal
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{parametroVariosForm.findSelectTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{parametroVariosForm.findParametroVariosByTipoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultParametroVariosByTipoPersonal"
								rendered="#{parametroVariosForm.showParametroVariosByTipoPersonal&&
								!parametroVariosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{parametroVariosForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{parametroVariosForm.selectParametroVarios}"
                                						styleClass="listitem">
                                						<f:param name="idParametroVarios" value="#{result.idParametroVarios}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataParametroVarios"
								rendered="#{parametroVariosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!parametroVariosForm.editing&&!parametroVariosForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{parametroVariosForm.editing&&!parametroVariosForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{parametroVariosForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{parametroVariosForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="parametroVariosForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{parametroVariosForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!parametroVariosForm.editing&&!parametroVariosForm.deleting&&parametroVariosForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{parametroVariosForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!parametroVariosForm.editing&&!parametroVariosForm.deleting&&parametroVariosForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{parametroVariosForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{parametroVariosForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{parametroVariosForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{parametroVariosForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{parametroVariosForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{parametroVariosForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.selectTipoPersonal}"
														id="tipoPersonal"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Tipo Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.colTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="tipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Grupo Organismo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.selectGrupoOrganismo}"
														id="grupoOrganismo"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Grupo Organismo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.colGrupoOrganismo}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="grupoOrganismo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se calculan prestaciones con Nuevo Regimen?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.nuevoRegimen}"
	                                                    id="nuevoRegimen"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Se calculan prestaciones con Nuevo Regimen?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listNuevoRegimen}" />
                                                    </h:selectOneMenu>													<h:message for="nuevoRegimen" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�lculo de prestaciones mensuales toma alicuota Bono Fin de A�o?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.alicuotaBfaPrestac}"
	                                                    id="alicuotaBfaPrestac"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="C�lculo de prestaciones mensuales toma alicuota Bono Fin de A�o?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listAlicuotaBfaPrestac}" />
                                                    </h:selectOneMenu>													<h:message for="alicuotaBfaPrestac" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo C�lculo Bono de Fin de A�o 
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.tipoCalculoBfa}"
	                                                    id="tipoCaluclo"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Tipo C�lculo Bono Fin de A�o">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listTipoCalculo}" />
                                                    </h:selectOneMenu>													<h:message for="tipoCalculoBfa" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							C�lculo de bono vacacional toma alicuota Bono Fin de A�o?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.alicuotaBfaBvac}"
	                                                    id="alicuotaBfaBvac"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="C�lculo de bono vacacional toma alicuota Bono Fin de A�o?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listAlicuotaBfaBvac}" />
                                                    </h:selectOneMenu>													<h:message for="alicuotaBfaBvac" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�lculo alicuota Bono Fin de A�o es sobre?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.calculoAlicuotaBfa}"
	                                                    id="calculoAlicuotaBfa"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="C�lculo alicuota Bono Fin de A�o es sobre?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listCalculoAlicuotaBfa}" />
                                                    </h:selectOneMenu>													<h:message for="calculoAlicuotaBfa" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�lculo de prestaciones mensuales toma alicuota Bono Vacacional?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.alicuotaBvacPrestac}"
	                                                    id="alicuotaBvacPrestac"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="C�lculo de prestaciones mensuales toma alicuota Bono Vacacional?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listAlicuotaBvacPrestac}" />
                                                    </h:selectOneMenu>													<h:message for="alicuotaBvacPrestac" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha Apertura Fideicomiso
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="fechaAperturaFideicomiso"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{parametroVariosForm.parametroVarios.fechaAperturaFideicomiso}"
                        								readonly="#{!parametroVariosForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onblur="javascript:check_date(this)"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
													<h:message for="fechaAperturaFideicomiso" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�lculo de Bono Fin de A�o toma alicuota de Bono Vacacional?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.alicuotaBvacBfa}"
	                                                    id="alicuotaBvacBfa"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="C�lculo de Bono Fin de A�o toma alicuota de Bono Vacacional?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listAlicuotaBvacBfa}" />
                                                    </h:selectOneMenu>													<h:message for="alicuotaBvacBfa" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cantidad de dias para dividir devengados anuales
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="4"
	                        							id="diasAnio"
	                        							maxlength="4"
                        								value="#{parametroVariosForm.parametroVarios.diasAnio}"
                        								readonly="#{!parametroVariosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        							<h:message for="diasAnio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha Ingreso Tope Bono Fin A�o
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="fechaTopeUtilidades"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{parametroVariosForm.parametroVarios.fechaTopeUtilidades}"
                        								readonly="#{!parametroVariosForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onblur="javascript:check_date(this)"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								required="false">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)</f:verbatim>
													<h:message for="fechaTopeUtilidades" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cuando se paga el Bono Vacacional?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.aniversarioDisfrute}"
	                                                    id="aniversarioDisfrute"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Cuando se paga el Bono Vacacional?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listAniversarioDisfrute}" />
                                                    </h:selectOneMenu>													<h:message for="aniversarioDisfrute" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Se otorga un bono extra vacacional?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.bonoExtra}"
	                                                    id="bonoExtra"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Se otorga un bono extra vacacional?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listBonoExtra}" />
                                                    </h:selectOneMenu>													<h:message for="bonoExtra" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							A Fecha de Vacaciones se suman los a�os APN?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.sumoApn}"
	                                                    id="sumoApn"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="A Fecha de Vacaciones se suman los a�os APN?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listSumoApn}" />
                                                    </h:selectOneMenu>													<h:message for="sumoApn" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Proces� deuda de regimen derogado?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.regimenDerogadoProcesado}"
	                                                    id="regimenDerogadoProcesado"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Proces� deuda de regimen derogado?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listRegimenDerogadoProcesado}" />
                                                    </h:selectOneMenu>													<h:message for="regimenDerogadoProcesado" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Calcul� los Intereses Adicionales?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.interesesAdicionales}"
	                                                    id="interesesAdicionales"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Calcul� los Intereses Adicionales?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listInteresesAdicionales}" />
                                                    </h:selectOneMenu>													<h:message for="interesesAdicionales" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Descuento por ausencia injustificada?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.ausenciaInjustificada}"
	                                                    id="ausenciaInjustificada"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Descuento por ausencia injustificada?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listAusenciaInjustificada}" />
                                                    </h:selectOneMenu>													<h:message for="ausenciaInjustificada" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�lculo de Bono Vacacional toma alicuota de Bono Petrolero?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parametroVariosForm.parametroVarios.alicuotaBonoPetrolero}"
	                                                    id="alicuotaBonoPetrolero"
                                                    	disabled="#{!parametroVariosForm.editing}"
                        								                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="C�lculo de Bono Vacacional toma alicuota de Bono Petrolero?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{parametroVariosForm.listAlicuotaBonoPetrolero}" />
                                                    </h:selectOneMenu>													<h:message for="alicuotaBonoPetrolero" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Constante 1
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="18"
	                        							id="constantePetroleroA"
	                        							maxlength="18"
                        								value="#{parametroVariosForm.parametroVarios.constantePetroleroA}"
                        								readonly="#{!parametroVariosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								 rendered="#{parametroVariosForm.showConstantePetroleroAAux}" 														                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="constantePetroleroA" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Constante 2
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="18"
	                        							id="constantePetroleroB"
	                        							maxlength="18"
                        								value="#{parametroVariosForm.parametroVarios.constantePetroleroB}"
                        								readonly="#{!parametroVariosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								 rendered="#{parametroVariosForm.showConstantePetroleroBAux}" 														                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="constantePetroleroB" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Constante 3
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="18"
	                        							id="constantePetroleroC"
	                        							maxlength="18"
                        								value="#{parametroVariosForm.parametroVarios.constantePetroleroC}"
                        								readonly="#{!parametroVariosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								 rendered="#{parametroVariosForm.showConstantePetroleroCAux}" 														                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="constantePetroleroC" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cambio Oficial Moneda
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="cambioMoneda"
	                        							maxlength="10"
                        								value="#{parametroVariosForm.parametroVarios.cambioMoneda}"
                        								readonly="#{!parametroVariosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="cambioMoneda" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tope Horas Extra Anual
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="topeHorasExtra"
	                        							maxlength="10"
                        								value="#{parametroVariosForm.parametroVarios.topeHorasExtra}"
                        								readonly="#{!parametroVariosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber integerOnly="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="topeHorasExtra" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tope Horas Extra en Per&iacute;odo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="topeHorasExtraMensual"
	                        							maxlength="10"
                        								value="#{parametroVariosForm.parametroVarios.topeHorasExtraMensual}"
                        								readonly="#{!parametroVariosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber integerOnly="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="topeHorasExtra" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{parametroVariosForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!parametroVariosForm.editing&&!parametroVariosForm.deleting&&parametroVariosForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{parametroVariosForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!parametroVariosForm.editing&&!parametroVariosForm.deleting&&parametroVariosForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{parametroVariosForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{parametroVariosForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{parametroVariosForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{parametroVariosForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{parametroVariosForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{parametroVariosForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>