<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
			function windowReport() {
				var number = 
					document.forms['formConceptoUtilidades'].elements['formConceptoUtilidades:reportId'].value;
				var url = '/sigefirrhh/reports/process.jsp?reportName=ConceptoUtilidades' + number;
				window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
	    	}	
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{conceptoUtilidadesForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formConceptoUtilidades">
<h:inputHidden id="reportId" value="#{conceptoUtilidadesForm.reportId}" />
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											F�rmula Bono Fin de A�o
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/ConceptoUtilidades.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
												<h:commandButton image="/images/report.gif"
														action="#{conceptoUtilidadesForm.runReport}"
        												onclick="windowReport()"        												
														rendered="#{!conceptoUtilidadesForm.editing&&!conceptoUtilidadesForm.deleting}" />
														
													<h:commandButton image="/images/add.gif"
														action="#{conceptoUtilidadesForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!conceptoUtilidadesForm.editing&&!conceptoUtilidadesForm.deleting&&conceptoUtilidadesForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchConceptoUtilidades"
								rendered="#{!conceptoUtilidadesForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Tipo Personal
												</td>
												<td width="100%"></f:verbatim>
												   
													<h:selectOneMenu value="#{conceptoUtilidadesForm.findSelectTipoPersonal}"
                                                    	immediate="false"
                                                    	required="true">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoUtilidadesForm.findColTipoPersonal}" />
                                                    </h:selectOneMenu>
                                                    
														<h:commandButton image="/images/find.gif" 
														action="#{conceptoUtilidadesForm.findConceptoUtilidadesByTipoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConceptoUtilidadesByTipoPersonal"
								rendered="#{conceptoUtilidadesForm.showConceptoUtilidadesByTipoPersonal&&
								!conceptoUtilidadesForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoUtilidadesForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoUtilidadesForm.selectConceptoUtilidades}"
                                						styleClass="listitem">
                                						<f:param name="idConceptoUtilidades" value="#{result.idConceptoUtilidades}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataConceptoUtilidades"
								rendered="#{conceptoUtilidadesForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!conceptoUtilidadesForm.editing&&!conceptoUtilidadesForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{conceptoUtilidadesForm.editing&&!conceptoUtilidadesForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{conceptoUtilidadesForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{conceptoUtilidadesForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="conceptoUtilidadesForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{conceptoUtilidadesForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoUtilidadesForm.editing&&!conceptoUtilidadesForm.deleting&&conceptoUtilidadesForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{conceptoUtilidadesForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoUtilidadesForm.editing&&!conceptoUtilidadesForm.deleting&&conceptoUtilidadesForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{conceptoUtilidadesForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{conceptoUtilidadesForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{conceptoUtilidadesForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{conceptoUtilidadesForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{conceptoUtilidadesForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{conceptoUtilidadesForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoUtilidadesForm.selectTipoPersonal}"
														id="tipoPersonal"
                                                    	disabled="#{!conceptoUtilidadesForm.editing}"
                        								 onchange="this.form.submit()"
					                                                    
														valueChangeListener="#{conceptoUtilidadesForm.changeTipoPersonal}"
														immediate="false"
                                                    	required="true"
														title="Tipo Personal">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoUtilidadesForm.colTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="tipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                        						
                        						
                        						
                        						
                        						<h:selectOneMenu value="#{conceptoUtilidadesForm.selectConceptoTipoPersonal}"
														id="conceptoTipoPersonal"
                                                    	disabled="#{!conceptoUtilidadesForm.editing}"
                        								rendered="#{conceptoUtilidadesForm.showConcepto}"                         								                                                    	
                        								immediate="false"
                                                    	required="true"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoUtilidadesForm.colConceptoTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="conceptoTipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo C�lculo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoUtilidadesForm.conceptoUtilidades.tipo}"
	                                                    id="tipo"
                                                    	disabled="#{!conceptoUtilidadesForm.editing}"
                        								onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Tipo C�lculo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoUtilidadesForm.listTipo}" />
                                                    </h:selectOneMenu>													<h:message for="tipo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        					<f:verbatim><tr>
                        						<td>
                        							A partir de que mes ?
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="mesInicio"
	                        							maxlength="2"
                        								value="#{conceptoUtilidadesForm.conceptoUtilidades.mesInicio}"
                        								readonly="#{!conceptoUtilidadesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								rendered="#{conceptoUtilidadesForm.showMesInicioAux}" required="false">
                        							</h:inputText>
                        								
                        							<h:message for="mesInicio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Hasta el mes ?
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="mesFinal"
	                        							maxlength="2"
                        								value="#{conceptoUtilidadesForm.conceptoUtilidades.mesFinal}"
                        								readonly="#{!conceptoUtilidadesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								rendered="#{conceptoUtilidadesForm.showMesFinalAux}" 	required="false">
                        							</h:inputText>
                        								
                        							<h:message for="mesFinal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							N�mero de d�as a dividir ?
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="3"
	                        							id="numeroDias"
	                        							maxlength="3"
                        								value="#{conceptoUtilidadesForm.conceptoUtilidades.numeroDias}"
                        								readonly="#{!conceptoUtilidadesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								rendered="#{conceptoUtilidadesForm.showNumeroDiasAux}" 	required="false">
                        							</h:inputText>
                        								
                        							<h:message for="numeroDias" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							N�mero de meses a proyectar
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="numeroMeses"
	                        							maxlength="2"
                        								value="#{conceptoUtilidadesForm.conceptoUtilidades.numeroMeses}"
                        								readonly="#{!conceptoUtilidadesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								rendered="#{conceptoUtilidadesForm.showNumeroMesesAux}" 	required="false">
                        							</h:inputText>
                        								
                        							<h:message for="numeroMeses" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Devengado en el mes
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="mesBuscar"
	                        							maxlength="2"
                        								value="#{conceptoUtilidadesForm.conceptoUtilidades.mesBuscar}"
                        								readonly="#{!conceptoUtilidadesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								 rendered="#{conceptoUtilidadesForm.showMesBuscarAux}" 														                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="mesBuscar" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Devengado en la semana (A�o)
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="semanaBuscar"
	                        							maxlength="2"
                        								value="#{conceptoUtilidadesForm.conceptoUtilidades.semanaBuscar}"
                        								readonly="#{!conceptoUtilidadesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								 rendered="#{conceptoUtilidadesForm.showSemanaBuscarAux}" 														                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="semanaBuscar" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Factor
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="6"
	                        							id="factor"
	                        							maxlength="6"
                        								value="#{conceptoUtilidadesForm.conceptoUtilidades.factor}"
                        								readonly="#{!conceptoUtilidadesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="factor" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tope Monto
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="topeMonto"
	                        							maxlength="16"
                        								value="#{conceptoUtilidadesForm.conceptoUtilidades.topeMonto}"
                        								readonly="#{!conceptoUtilidadesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="topeMonto" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tope Unidades
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="16"
	                        							id="topeUnidades"
	                        							maxlength="16"
                        								value="#{conceptoUtilidadesForm.conceptoUtilidades.topeUnidades}"
                        								readonly="#{!conceptoUtilidadesForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="topeUnidades" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{conceptoUtilidadesForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoUtilidadesForm.editing&&!conceptoUtilidadesForm.deleting&&conceptoUtilidadesForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{conceptoUtilidadesForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoUtilidadesForm.editing&&!conceptoUtilidadesForm.deleting&&conceptoUtilidadesForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{conceptoUtilidadesForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{conceptoUtilidadesForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{conceptoUtilidadesForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{conceptoUtilidadesForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{conceptoUtilidadesForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{conceptoUtilidadesForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>