<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		   function windowReport() {
				var number = 
					document.forms['formConceptoAsociado'].elements['formConceptoAsociado:reportId'].value;
				var url = '/sigefirrhh/reports/process.jsp?reportName=ConceptoAsociado' + number;
				window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
	    	}
		
 	</script>
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{conceptoAsociadoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />



	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formConceptoAsociado">
<h:inputHidden id="reportId" value="#{conceptoAsociadoForm.reportId}" />
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Conceptos Asociados
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/ConceptoAsociado.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
												<h:commandButton image="/images/report.gif"
														action="#{conceptoAsociadoForm.runReport}"
        												onclick="windowReport()"        												
														rendered="#{!conceptoAsociadoForm.editing&&!conceptoAsociadoForm.deleting}" />
														
													<h:commandButton image="/images/add.gif"
														action="#{conceptoAsociadoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!conceptoAsociadoForm.editing&&!conceptoAsociadoForm.deleting&&conceptoAsociadoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="search1"
								rendered="#{!conceptoAsociadoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
						                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
				                                                    <h:selectOneMenu value="#{conceptoAsociadoForm.findSelectTipoPersonalForConceptoTipoPersonal}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{conceptoAsociadoForm.findChangeTipoPersonalForConceptoTipoPersonal}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{conceptoAsociadoForm.findColTipoPersonalForConceptoTipoPersonal}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
																<f:verbatim><tr>
												<td>
													Concepto
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{conceptoAsociadoForm.findSelectConceptoTipoPersonal}"
                                                    	rendered="#{conceptoAsociadoForm.findShowConceptoTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoAsociadoForm.findColConceptoTipoPersonal}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{conceptoAsociadoForm.findConceptoAsociadoByConceptoTipoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConceptoAsociadoByConceptoTipoPersonal"
								rendered="#{conceptoAsociadoForm.showConceptoAsociadoByConceptoTipoPersonal&&
								!conceptoAsociadoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoAsociadoForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoAsociadoForm.selectConceptoAsociado}"
                                						styleClass="listitem">
                                						<f:param name="idConceptoAsociado" value="#{result.idConceptoAsociado}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataConceptoAsociado"
								rendered="#{conceptoAsociadoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!conceptoAsociadoForm.editing&&!conceptoAsociadoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{conceptoAsociadoForm.editing&&!conceptoAsociadoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{conceptoAsociadoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{conceptoAsociadoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="conceptoAsociadoForm">
<h:inputHidden id="scrollx" value="#{conceptoAsociadoForm.scrollx}" />
<h:inputHidden id="scrolly" value="#{conceptoAsociadoForm.scrolly}" />
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{conceptoAsociadoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoAsociadoForm.editing&&!conceptoAsociadoForm.deleting&&conceptoAsociadoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{conceptoAsociadoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoAsociadoForm.editing&&!conceptoAsociadoForm.deleting&&conceptoAsociadoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{conceptoAsociadoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{conceptoAsociadoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{conceptoAsociadoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{conceptoAsociadoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{conceptoAsociadoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{conceptoAsociadoForm.abort}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoAsociadoForm.selectTipoPersonalForConceptoTipoPersonal}"
                                                    	disabled="#{!conceptoAsociadoForm.editing}"
                                                    	onchange="saveScrollCoordinates();this.form.submit()"
														valueChangeListener="#{conceptoAsociadoForm.changeTipoPersonalForConceptoTipoPersonal}"
                        																						immediate="false"
														required="true"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{conceptoAsociadoForm.colTipoPersonalForConceptoTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoAsociadoForm.selectConceptoTipoPersonal}"
	                                                    id="ConceptoTipoPersonal"
                                                    	disabled="#{!conceptoAsociadoForm.editing}"
                                                    	rendered="#{conceptoAsociadoForm.showConceptoTipoPersonal
														 }"
                                                    	immediate="false"
                                                    	required="true"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoAsociadoForm.colConceptoTipoPersonal}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto a Asociar
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoAsociadoForm.selectConceptoAsociar}"
	                                                    id="ConceptoAsociar"
                                                    	disabled="#{!conceptoAsociadoForm.editing}"
                                                    	rendered="#{conceptoAsociadoForm.showConceptoAsociar
														 }"
                                                    	immediate="false"
                                                    	required="true"
														title="Concepto a Asociar">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoAsociadoForm.colConceptoAsociar}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Conversi�n a
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoAsociadoForm.conceptoAsociado.base}"
	                                                    id="Base"
                                                    	disabled="#{!conceptoAsociadoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Conversi�n a">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoAsociadoForm.listBase}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Factor
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="Factor"
	                        							maxlength="10"
                        								value="#{conceptoAsociadoForm.conceptoAsociado.factor}"
                        								readonly="#{!conceptoAsociadoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyFloatCheck(event, this)"
                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="4" maxFractionDigits="4" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        								                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{conceptoAsociadoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoAsociadoForm.editing&&!conceptoAsociadoForm.deleting&&conceptoAsociadoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{conceptoAsociadoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoAsociadoForm.editing&&!conceptoAsociadoForm.deleting&&conceptoAsociadoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{conceptoAsociadoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{conceptoAsociadoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{conceptoAsociadoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{conceptoAsociadoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{conceptoAsociadoForm.editing}" />
													<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{conceptoAsociadoForm.abort}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />	
                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>