<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
	<jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{conceptoCargoAnioForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formConceptoCargoAnio">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Conceptos por Cargo y Tiempo de Servicio
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/definiciones/ConceptoCargoAnio.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{conceptoCargoAnioForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!conceptoCargoAnioForm.editing&&!conceptoCargoAnioForm.deleting&&conceptoCargoAnioForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchConceptoCargoAnio"
								rendered="#{!conceptoCargoAnioForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
						                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
				                                        <h:selectOneMenu value="#{conceptoCargoAnioForm.findSelectTipoPersonalForConceptoTipoPersonal}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{conceptoCargoAnioForm.findChangeTipoPersonalForConceptoTipoPersonal}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{conceptoCargoAnioForm.findColTipoPersonalForConceptoTipoPersonal}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
																<f:verbatim><tr>
												<td>
													Concepto
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{conceptoCargoAnioForm.findSelectConceptoTipoPersonal}"
                                                    	rendered="#{conceptoCargoAnioForm.findShowConceptoTipoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoCargoAnioForm.findColConceptoTipoPersonal}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{conceptoCargoAnioForm.findConceptoCargoAnioByConceptoTipoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
							                    <f:verbatim><tr>
                        						<td>
                        							Manual Cargo
                        						</td>
                        						<td></f:verbatim>
				                                                    <h:selectOneMenu value="#{conceptoCargoAnioForm.findSelectManualCargoForCargo}"
                                                    	onchange="this.form.submit()"
                                                    	rendered="#{conceptoCargoAnioForm.findShowConceptoTipoPersonal}"
														valueChangeListener="#{conceptoCargoAnioForm.findChangeManualCargoForCargo}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{conceptoCargoAnioForm.findColManualCargoForCargo}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
																<f:verbatim><tr>
												<td>
													Cargo
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{conceptoCargoAnioForm.findSelectCargo}"
                                                    	rendered="#{conceptoCargoAnioForm.findShowCargo}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoCargoAnioForm.findColCargo}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{conceptoCargoAnioForm.findConceptoCargoAnioByCargo}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConceptoCargoAnioByConceptoTipoPersonal"
								rendered="#{conceptoCargoAnioForm.showConceptoCargoAnioByConceptoTipoPersonal&&
								!conceptoCargoAnioForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoCargoAnioForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoCargoAnioForm.selectConceptoCargoAnio}"
                                						styleClass="listitem">
                                						<f:param name="idConceptoCargoAnio" value="#{result.idConceptoCargoAnio}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultConceptoCargoAnioByCargo"
								rendered="#{conceptoCargoAnioForm.showConceptoCargoAnioByCargo&&
								!conceptoCargoAnioForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{conceptoCargoAnioForm.result}"
                                               
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{conceptoCargoAnioForm.selectConceptoCargoAnio}"
                                						styleClass="listitem">
                                						<f:param name="idConceptoCargoAnio" value="#{result.idConceptoCargoAnio}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataConceptoCargoAnio"
								rendered="#{conceptoCargoAnioForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!conceptoCargoAnioForm.editing&&!conceptoCargoAnioForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{conceptoCargoAnioForm.editing&&!conceptoCargoAnioForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{conceptoCargoAnioForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{conceptoCargoAnioForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="conceptoCargoAnioForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{conceptoCargoAnioForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoCargoAnioForm.editing&&!conceptoCargoAnioForm.deleting&&conceptoCargoAnioForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{conceptoCargoAnioForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!conceptoCargoAnioForm.editing&&!conceptoCargoAnioForm.deleting&&conceptoCargoAnioForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{conceptoCargoAnioForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{conceptoCargoAnioForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{conceptoCargoAnioForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{conceptoCargoAnioForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{conceptoCargoAnioForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{conceptoCargoAnioForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Tipo Personal
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoCargoAnioForm.selectTipoPersonalForConceptoTipoPersonal}"
                                                    	disabled="#{!conceptoCargoAnioForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="TipoPersonalForConceptoTipoPersonal"
														valueChangeListener="#{conceptoCargoAnioForm.changeTipoPersonalForConceptoTipoPersonal}"
                        																						immediate="false"
														required="false"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{conceptoCargoAnioForm.colTipoPersonalForConceptoTipoPersonal}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Concepto
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoCargoAnioForm.selectConceptoTipoPersonal}"
	                                                    id="ConceptoTipoPersonal"
                                                    	disabled="#{!conceptoCargoAnioForm.editing}"
                                                    	rendered="#{conceptoCargoAnioForm.showConceptoTipoPersonal}"
                                                    	immediate="false"
                                                    	required="false"
														title="Concepto">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoCargoAnioForm.colConceptoTipoPersonal}" />
                                                    </h:selectOneMenu>													<h:message for="ConceptoTipoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Manual Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoCargoAnioForm.selectManualCargoForCargo}"
                                                    	disabled="#{!conceptoCargoAnioForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="ManualCargoForCargo"
	                        							rendered="#{conceptoCargoAnioForm.showConceptoTipoPersonal}"
														valueChangeListener="#{conceptoCargoAnioForm.changeManualCargoForCargo}"
                        																						immediate="false"
														required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{conceptoCargoAnioForm.colManualCargoForCargo}" />
													</h:selectOneMenu>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoCargoAnioForm.selectCargo}"
	                                                    id="Cargo"
                                                    	disabled="#{!conceptoCargoAnioForm.editing}"
                                                    	rendered="#{conceptoCargoAnioForm.showCargo
														 }"
                                                    	immediate="false"
                                                    	required="false"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{conceptoCargoAnioForm.colCargo}" />
                                                    </h:selectOneMenu>													<h:message for="Cargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Excluir
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoCargoAnioForm.conceptoCargoAnio.excluir}"
	                                                    id="Excluir"
                                                    	disabled="#{!conceptoCargoAnioForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Excluir">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoCargoAnioForm.listExcluir}" />
                                                    </h:selectOneMenu>													<h:message for="Excluir" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Autom�tico al Ingresar
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{conceptoCargoAnioForm.conceptoCargoAnio.automaticoIngreso}"
	                                                    id="AutomaticoIngreso"
                                                    	disabled="#{!conceptoCargoAnioForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Autom�tico al Ingresar">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{conceptoCargoAnioForm.listAutomaticoIngreso}" />
                                                    </h:selectOneMenu>													<h:message for="AutomaticoIngreso" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Unidades
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="Unidades"
	                        							maxlength="10"
                        								value="#{conceptoCargoAnioForm.conceptoCargoAnio.unidades}"
                        								readonly="#{!conceptoCargoAnioForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="Unidades" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Monto
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="Monto"
	                        							maxlength="10"
                        								value="#{conceptoCargoAnioForm.conceptoCargoAnio.monto}"
                        								readonly="#{!conceptoCargoAnioForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="Monto" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Porcentaje
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="Porcentaje"
	                        							maxlength="10"
                        								value="#{conceptoCargoAnioForm.conceptoCargoAnio.porcentaje}"
                        								readonly="#{!conceptoCargoAnioForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="Porcentaje" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							A�os de Servicio (Mayor o Igual)
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="Anios"
	                        							maxlength="2"
                        								value="#{conceptoCargoAnioForm.conceptoCargoAnio.anios}"
                        								readonly="#{!conceptoCargoAnioForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="Anios" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{conceptoCargoAnioForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoCargoAnioForm.editing&&!conceptoCargoAnioForm.deleting&&conceptoCargoAnioForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{conceptoCargoAnioForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!conceptoCargoAnioForm.editing&&!conceptoCargoAnioForm.deleting&&conceptoCargoAnioForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{conceptoCargoAnioForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{conceptoCargoAnioForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{conceptoCargoAnioForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{conceptoCargoAnioForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{conceptoCargoAnioForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{conceptoCargoAnioForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>