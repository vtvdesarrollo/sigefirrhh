<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{parroquiaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formParroquia">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Parroquias
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/ubicacion/Parroquia.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchParroquia"
								rendered="#{!parroquiaForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													C�digo
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="2"
														maxlength="2"
														value="#{parroquiaForm.findCodParroquia}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{parroquiaForm.findParroquiaByCodParroquia}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Nombre
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="40"
														maxlength="40"
														value="#{parroquiaForm.findNombre}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{parroquiaForm.findParroquiaByNombre}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
							                        					<f:verbatim><tr>
                        						<td>
                        							Pais
                        						</td>
                        						<td></f:verbatim>
				                                                    <h:selectOneMenu value="#{parroquiaForm.findSelectPaisForMunicipio}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{parroquiaForm.findChangePaisForMunicipio}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{parroquiaForm.findColPaisForMunicipio}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
			                        					<f:verbatim><tr>
                        						<td>
                        							Estado
                        						</td>
                        						<td></f:verbatim>
																	<h:selectOneMenu value="#{parroquiaForm.findSelectEstadoForMunicipio}"
                                                    	onchange="this.form.submit()"
														valueChangeListener="#{parroquiaForm.findChangeEstadoForMunicipio}"
														rendered="#{parroquiaForm.findShowEstadoForMunicipio}"
														immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{parroquiaForm.findColEstadoForMunicipio}" />
													</h:selectOneMenu>
																<f:verbatim></td>
											</tr></f:verbatim>
																<f:verbatim><tr>
												<td>
													Municipio
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{parroquiaForm.findSelectMunicipio}"
                                                    	rendered="#{parroquiaForm.findShowMunicipio}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{parroquiaForm.findColMunicipio}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{parroquiaForm.findParroquiaByMunicipio}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultParroquiaByCodParroquia"
								rendered="#{parroquiaForm.showParroquiaByCodParroquia&&
								!parroquiaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{parroquiaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{parroquiaForm.selectParroquia}"
                                						styleClass="listitem">
                                						<f:param name="idParroquia" value="#{result.idParroquia}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultParroquiaByNombre"
								rendered="#{parroquiaForm.showParroquiaByNombre&&
								!parroquiaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{parroquiaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{parroquiaForm.selectParroquia}"
                                						styleClass="listitem">
                                						<f:param name="idParroquia" value="#{result.idParroquia}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultParroquiaByMunicipio"
								rendered="#{parroquiaForm.showParroquiaByMunicipio&&
								!parroquiaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{parroquiaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{parroquiaForm.selectParroquia}"
                                						styleClass="listitem">
                                						<f:param name="idParroquia" value="#{result.idParroquia}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataParroquia"
								rendered="#{parroquiaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!parroquiaForm.editing&&!parroquiaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{parroquiaForm.editing&&!parroquiaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{parroquiaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{parroquiaForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="parroquiaForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{parroquiaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{parroquiaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{parroquiaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{parroquiaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{parroquiaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{parroquiaForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�digo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="codParroquia"
	                        							maxlength="2"
                        								value="#{parroquiaForm.parroquia.codParroquia}"
                        								readonly="#{!parroquiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="codParroquia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nombre
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="40"
	                        							id="nombre"
	                        							maxlength="40"
                        								value="#{parroquiaForm.parroquia.nombre}"
                        								readonly="#{!parroquiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="nombre" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Abreviatura
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="3"
	                        							id="abreviatura"
	                        							maxlength="3"
                        								value="#{parroquiaForm.parroquia.abreviatura}"
                        								readonly="#{!parroquiaForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="abreviatura" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Pais
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parroquiaForm.selectPaisForMunicipio}"
                                                    	disabled="#{!parroquiaForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="PaisForMunicipio"
														valueChangeListener="#{parroquiaForm.changePaisForMunicipio}"
                        																						immediate="false"
														required="true"
														title="Municipio">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{parroquiaForm.colPaisForMunicipio}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Estado
                        						</td>
                        						<td></f:verbatim>
													<h:selectOneMenu value="#{parroquiaForm.selectEstadoForMunicipio}"
                                                    	disabled="#{!parroquiaForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="EstadoForMunicipio"
														valueChangeListener="#{parroquiaForm.changeEstadoForMunicipio}"
														rendered="#{parroquiaForm.showEstadoForMunicipio
														 }"
														immediate="false"
														required="true"
														title="Municipio">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{parroquiaForm.colEstadoForMunicipio}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Municipio
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{parroquiaForm.selectMunicipio}"
	                                                    id="municipio"
                                                    	disabled="#{!parroquiaForm.editing}"
                                                    	rendered="#{parroquiaForm.showMunicipio
														 }"
                                                    	immediate="false"
                                                    	required="true"
														title="Municipio">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{parroquiaForm.colMunicipio}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="municipio" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{parroquiaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{parroquiaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{parroquiaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{parroquiaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{parroquiaForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{parroquiaForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>