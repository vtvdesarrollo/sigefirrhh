<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />
<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
	
</head>

<body onload="scrollToCoordinates(); firstFocus();">
<f:view>
	<x:saveState value="#{cargoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formCargo">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y B�squeda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Cargos
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/cargo/Cargo.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage  url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{cargoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!cargoForm.editing&&!cargoForm.deleting&&cargoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="viewBuscar"
								rendered="#{!cargoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Clasificador
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{cargoForm.findSelectManualCargo}"
                                                    	immediate="false">														
                                                        <f:selectItems value="#{cargoForm.findColManualCargo}" />
                                                    </h:selectOneMenu>
																		
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													C�digo Clase/Cargo
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="10"
														maxlength="10"
														value="#{cargoForm.findCodCargo}"
														onchange="javascript:this.value=this.value.toUpperCase()" />

															<h:commandButton image="/images/find.gif" 
														action="#{cargoForm.findCargoByCodCargo}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Descripci�n
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="60"
														maxlength="60"
														value="#{cargoForm.findDescripcionCargo}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														onkeypress="return keyEnterCheck(event, this)" />

															<h:commandButton image="/images/find.gif" 
														action="#{cargoForm.findCargoByDescripcionCargo}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultCargoByManualCargo"
								rendered="#{cargoForm.showCargoByManualCargo&&
								!cargoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{cargoForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{cargoForm.selectCargo}"
                                						styleClass="listitem">
                                						<f:param name="idCargo" value="#{result.idCargo}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultCargoByCodCargo"
								rendered="#{cargoForm.showCargoByCodCargo&&
								!cargoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{cargoForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{cargoForm.selectCargo}"
                                						styleClass="listitem">
                                						<f:param name="idCargo" value="#{result.idCargo}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultCargoByDescripcionCargo"
								rendered="#{cargoForm.showCargoByDescripcionCargo&&
								!cargoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{cargoForm.result}"
                                                
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{cargoForm.selectCargo}"
                                						styleClass="listitem">
                                						<f:param name="idCargo" value="#{result.idCargo}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif"  />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataCargo"
								rendered="#{cargoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!cargoForm.editing&&!cargoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{cargoForm.editing&&!cargoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{cargoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{cargoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="cargoForm">
<h:inputHidden id="scrollx" value="#{cargoForm.scrollx}" />
<h:inputHidden id="scrolly" value="#{cargoForm.scrolly}" />
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{cargoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!cargoForm.editing&&!cargoForm.deleting&&cargoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{cargoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!cargoForm.editing&&!cargoForm.deleting&&cargoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{cargoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{cargoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{cargoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{cargoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{cargoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                        								action="#{cargoForm.abort}"
			                        								immediate="true"
			                        								onclick="javascript:return clickMade()"
			                	        							 />	
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Clasificador
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{cargoForm.selectManualCargo}"
														id="ManualCargo"
                                                    	disabled="#{!cargoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Manual">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cargoForm.colManualCargo}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�digo Clase/Cargo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="CodigoClaseCargo"
	                        							maxlength="10"
                        								value="#{cargoForm.cargo.codCargo}"
                        								readonly="#{!cargoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyEnterCheck(event, this)"
                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Descripci�n
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="60"
	                        							id="Descripcion"
	                        							maxlength="60"
                        								value="#{cargoForm.cargo.descripcionCargo}"
                        								readonly="#{!cargoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyEnterCheck(event, this)"
                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Tipo Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{cargoForm.cargo.tipoCargo}"
	                                                    id="TipoCargo"
                                                    	disabled="#{!cargoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Tipo Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cargoForm.listTipoCargo}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Grado
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="Grado"
	                        							maxlength="10"
                        								value="#{cargoForm.cargo.grado}"
                        								readonly="#{!cargoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        																						                        								required="false">
                        							</h:inputText>
                        								
                        								                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nivel
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="SubGrado"
	                        							maxlength="10"
                        								value="#{cargoForm.cargo.subGrado}"
                        								readonly="#{!cargoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onkeypress="return keyIntegerCheck(event, this)"
                        																						                        								required="false">
                        							</h:inputText>
                        								
                        								                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cauci�n
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{cargoForm.cargo.caucion}"
	                                                    id="Caucion"
                                                    	disabled="#{!cargoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Cauci�n">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cargoForm.listCaucion}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Serie
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{cargoForm.selectSerieCargo}"
														id="SerieCargo"
                                                    	disabled="#{!cargoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Serie">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{cargoForm.colSerieCargo}" />
                                                    </h:selectOneMenu>                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table>
                        					<tr>
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{cargoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!cargoForm.editing&&!cargoForm.deleting&&cargoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{cargoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!cargoForm.editing&&!cargoForm.deleting&&cargoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{cargoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{cargoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{cargoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{cargoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{cargoForm.editing}" />
                            	        			<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                        								action="#{cargoForm.abort}"
                        								immediate="true"
                        								onclick="javascript:return clickMade()"
                	        							 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</f:view>
</body>
</html>
