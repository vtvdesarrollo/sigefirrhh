<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{sueldoMinimoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formSueldoMinimo">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Sueldos/Salario M�nimo
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/cargo/SueldoMinimo.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{sueldoMinimoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!sueldoMinimoForm.editing&&!sueldoMinimoForm.deleting&&sueldoMinimoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchSueldoMinimo"
								rendered="#{!sueldoMinimoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Fecha de Vigencia
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="10"
														maxlength="10"
														value="#{sueldoMinimoForm.findFechaVigencia}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{sueldoMinimoForm.findSueldoMinimoByFechaVigencia}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultSueldoMinimoByFechaVigencia"
								rendered="#{sueldoMinimoForm.showSueldoMinimoByFechaVigencia&&
								!sueldoMinimoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{sueldoMinimoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{sueldoMinimoForm.selectSueldoMinimo}"
                                						styleClass="listitem">
                                						<f:param name="idSueldoMinimo" value="#{result.idSueldoMinimo}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataSueldoMinimo"
								rendered="#{sueldoMinimoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!sueldoMinimoForm.editing&&!sueldoMinimoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{sueldoMinimoForm.editing&&!sueldoMinimoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{sueldoMinimoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{sueldoMinimoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="sueldoMinimoForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{sueldoMinimoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!sueldoMinimoForm.editing&&!sueldoMinimoForm.deleting&&sueldoMinimoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{sueldoMinimoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!sueldoMinimoForm.editing&&!sueldoMinimoForm.deleting&&sueldoMinimoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{sueldoMinimoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{sueldoMinimoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{sueldoMinimoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{sueldoMinimoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{sueldoMinimoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{sueldoMinimoForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fecha de Vigencia
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
					                        			id="fechaVigencia"
	                        							size="10"
	                        							maxlength="10"
                        								value="#{sueldoMinimoForm.sueldoMinimo.fechaVigencia}"
                        								readonly="#{!sueldoMinimoForm.editing}"
                        								                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								onblur="javascript:check_date(this)"
                        								onkeypress="return keyEnterCheck(event, this)"
                        								required="true">
                                                      <f:convertDateTime timeZone="#{dateTime.timeZone}"
                                                        pattern="dd-MM-yyyy" />
                        							</h:inputText><f:verbatim>(dd-mm-aaaa)<span class="required"> *</span></f:verbatim>
													<h:message for="fechaVigencia" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Sueldo M�nimo Mensual
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							id="sueldoMinimo"
	                        							maxlength="15"
                        								value="#{sueldoMinimoForm.sueldoMinimo.sueldoMinimo}"
                        								readonly="#{!sueldoMinimoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="true">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="sueldoMinimo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Sueldo M�nimo Rural
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="15"
	                        							id="sueldoRural"
	                        							maxlength="15"
                        								value="#{sueldoMinimoForm.sueldoMinimo.sueldoRural}"
                        								readonly="#{!sueldoMinimoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="sueldoRural" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Base Legal
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="100"
	                        							id="baseLegal"
	                        							maxlength="100"
                        								value="#{sueldoMinimoForm.sueldoMinimo.baseLegal}"
                        								readonly="#{!sueldoMinimoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="baseLegal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{sueldoMinimoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!sueldoMinimoForm.editing&&!sueldoMinimoForm.deleting&&sueldoMinimoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{sueldoMinimoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!sueldoMinimoForm.editing&&!sueldoMinimoForm.deleting&&sueldoMinimoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{sueldoMinimoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{sueldoMinimoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{sueldoMinimoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{sueldoMinimoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{sueldoMinimoForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{sueldoMinimoForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>