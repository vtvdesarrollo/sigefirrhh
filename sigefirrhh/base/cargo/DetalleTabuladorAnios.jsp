<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{detalleTabuladorAniosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formDetalleTabuladorAnios">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Detalle de la Escla y Tabulador por A�os
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/cargo/DetalleTabuladorAnios.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{detalleTabuladorAniosForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!detalleTabuladorAniosForm.editing&&!detalleTabuladorAniosForm.deleting&&detalleTabuladorAniosForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchDetalleTabuladorAnios"
								rendered="#{!detalleTabuladorAniosForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													A�o desde
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="2"
														maxlength="2"
														value="#{detalleTabuladorAniosForm.findAnioDesde}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{detalleTabuladorAniosForm.findDetalleTabuladorAniosByAnioDesde}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													A�o hasta
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="2"
														maxlength="2"
														value="#{detalleTabuladorAniosForm.findAnioHasta}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{detalleTabuladorAniosForm.findDetalleTabuladorAniosByAnioHasta}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Paso
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="2"
														maxlength="2"
														value="#{detalleTabuladorAniosForm.findPaso}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{detalleTabuladorAniosForm.findDetalleTabuladorAniosByPaso}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Carga Horaria
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="1"
														maxlength="1"
														value="#{detalleTabuladorAniosForm.findHoras}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{detalleTabuladorAniosForm.findDetalleTabuladorAniosByHoras}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Escala y Tabulador
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{detalleTabuladorAniosForm.findSelectTabulador}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleTabuladorAniosForm.findColTabulador}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{detalleTabuladorAniosForm.findDetalleTabuladorAniosByTabulador}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultDetalleTabuladorAniosByAnioDesde"
								rendered="#{detalleTabuladorAniosForm.showDetalleTabuladorAniosByAnioDesde&&
								!detalleTabuladorAniosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{detalleTabuladorAniosForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{detalleTabuladorAniosForm.selectDetalleTabuladorAnios}"
                                						styleClass="listitem">
                                						<f:param name="idDetalleTabuladorAnios" value="#{result.idDetalleTabuladorAnios}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultDetalleTabuladorAniosByAnioHasta"
								rendered="#{detalleTabuladorAniosForm.showDetalleTabuladorAniosByAnioHasta&&
								!detalleTabuladorAniosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{detalleTabuladorAniosForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{detalleTabuladorAniosForm.selectDetalleTabuladorAnios}"
                                						styleClass="listitem">
                                						<f:param name="idDetalleTabuladorAnios" value="#{result.idDetalleTabuladorAnios}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultDetalleTabuladorAniosByPaso"
								rendered="#{detalleTabuladorAniosForm.showDetalleTabuladorAniosByPaso&&
								!detalleTabuladorAniosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{detalleTabuladorAniosForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{detalleTabuladorAniosForm.selectDetalleTabuladorAnios}"
                                						styleClass="listitem">
                                						<f:param name="idDetalleTabuladorAnios" value="#{result.idDetalleTabuladorAnios}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultDetalleTabuladorAniosByHoras"
								rendered="#{detalleTabuladorAniosForm.showDetalleTabuladorAniosByHoras&&
								!detalleTabuladorAniosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{detalleTabuladorAniosForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{detalleTabuladorAniosForm.selectDetalleTabuladorAnios}"
                                						styleClass="listitem">
                                						<f:param name="idDetalleTabuladorAnios" value="#{result.idDetalleTabuladorAnios}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultDetalleTabuladorAniosByTabulador"
								rendered="#{detalleTabuladorAniosForm.showDetalleTabuladorAniosByTabulador&&
								!detalleTabuladorAniosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{detalleTabuladorAniosForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{detalleTabuladorAniosForm.selectDetalleTabuladorAnios}"
                                						styleClass="listitem">
                                						<f:param name="idDetalleTabuladorAnios" value="#{result.idDetalleTabuladorAnios}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataDetalleTabuladorAnios"
								rendered="#{detalleTabuladorAniosForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!detalleTabuladorAniosForm.editing&&!detalleTabuladorAniosForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{detalleTabuladorAniosForm.editing&&!detalleTabuladorAniosForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{detalleTabuladorAniosForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{detalleTabuladorAniosForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="detalleTabuladorAniosForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{detalleTabuladorAniosForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!detalleTabuladorAniosForm.editing&&!detalleTabuladorAniosForm.deleting&&detalleTabuladorAniosForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{detalleTabuladorAniosForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!detalleTabuladorAniosForm.editing&&!detalleTabuladorAniosForm.deleting&&detalleTabuladorAniosForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{detalleTabuladorAniosForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{detalleTabuladorAniosForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{detalleTabuladorAniosForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{detalleTabuladorAniosForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{detalleTabuladorAniosForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{detalleTabuladorAniosForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							A�o desde
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="anioDesde"
	                        							maxlength="2"
                        								value="#{detalleTabuladorAniosForm.detalleTabuladorAnios.anioDesde}"
                        								readonly="#{!detalleTabuladorAniosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="anioDesde" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							A�o hasta
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="anioHasta"
	                        							maxlength="2"
                        								value="#{detalleTabuladorAniosForm.detalleTabuladorAnios.anioHasta}"
                        								readonly="#{!detalleTabuladorAniosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="anioHasta" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Paso
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="2"
	                        							id="paso"
	                        							maxlength="2"
                        								value="#{detalleTabuladorAniosForm.detalleTabuladorAnios.paso}"
                        								readonly="#{!detalleTabuladorAniosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="paso" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Carga Horaria
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="1"
	                        							id="horas"
	                        							maxlength="1"
                        								value="#{detalleTabuladorAniosForm.detalleTabuladorAnios.horas}"
                        								readonly="#{!detalleTabuladorAniosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="horas" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Sueldo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="sueldo"
	                        							maxlength="10"
                        								value="#{detalleTabuladorAniosForm.detalleTabuladorAnios.sueldo}"
                        								readonly="#{!detalleTabuladorAniosForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="true">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="sueldo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Manual Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleTabuladorAniosForm.selectManualCargoForCargo}"
                                                    	disabled="#{!detalleTabuladorAniosForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="ManualCargoForCargo"
														valueChangeListener="#{detalleTabuladorAniosForm.changeManualCargoForCargo}"
                        																						immediate="false"
														required="true"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{detalleTabuladorAniosForm.colManualCargoForCargo}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Cargo
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleTabuladorAniosForm.selectCargo}"
	                                                    id="cargo"
                                                    	disabled="#{!detalleTabuladorAniosForm.editing}"
                                                    	rendered="#{detalleTabuladorAniosForm.showCargo
														 }"
                                                    	immediate="false"
                                                    	required="true"
														title="Cargo">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{detalleTabuladorAniosForm.colCargo}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="cargo" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Escala y Tabulador
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleTabuladorAniosForm.selectTabulador}"
														id="tabulador"
                                                    	disabled="#{!detalleTabuladorAniosForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Tabulador">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleTabuladorAniosForm.colTabulador}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="tabulador" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{detalleTabuladorAniosForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!detalleTabuladorAniosForm.editing&&!detalleTabuladorAniosForm.deleting&&detalleTabuladorAniosForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{detalleTabuladorAniosForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!detalleTabuladorAniosForm.editing&&!detalleTabuladorAniosForm.deleting&&detalleTabuladorAniosForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{detalleTabuladorAniosForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{detalleTabuladorAniosForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{detalleTabuladorAniosForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{detalleTabuladorAniosForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{detalleTabuladorAniosForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{detalleTabuladorAniosForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>
