<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
	<jsp:include page="/inc/functions.jsp" />
    
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{clasificacionDependenciaForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formClasificacionDependencia">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Clasificaci�n de Dependencias
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/estructura/ClasificacionDependencia.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{clasificacionDependenciaForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!clasificacionDependenciaForm.editing&&!clasificacionDependenciaForm.deleting&&clasificacionDependenciaForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchClasificacionDependencia"
								rendered="#{!clasificacionDependenciaForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														
											<f:verbatim><tr>
												<td>
													Region
												</td>
												<td width="100%"></f:verbatim>
					                                <h:selectOneMenu value="#{clasificacionDependenciaForm.findIdRegion}"
                                                    	immediate="false">
                                                 
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{clasificacionDependenciaForm.findColRegion}" />
                                                    </h:selectOneMenu>
                                                    
													
												<f:verbatim></td>
											</tr></f:verbatim>
											
											<f:verbatim><tr>
												<td>
													C�digo Entidad
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="12"
														maxlength="12"
														value="#{clasificacionDependenciaForm.findCodDependencia}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"                        																						
														 />

													<h:commandButton image="/images/find2.gif" 
														action="#{clasificacionDependenciaForm.findDependenciaByCodigoAndRegion}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>		
											<f:verbatim>
												<tr>
												<td>
													
												</td>
													<td ></f:verbatim>
														<h:outputText 		
														rendered="#{clasificacionDependenciaForm.showFindDependencia}"												
														value="#{clasificacionDependenciaForm.dependencia}"																												                         																						
													/>

															
												<f:verbatim></td>
											</tr></f:verbatim>	
											
											<f:verbatim><tr>
											<td>
													
												</td>
												<td></f:verbatim>
													<h:commandButton image="/images/find.gif" 
														rendered="#{clasificacionDependenciaForm.showFindDependencia}"
														action="#{clasificacionDependenciaForm.findClasificacionDependenciaByDependencia}"
														onclick="javascript:return clickMade()" />
												</td><f:verbatim>
												
											</tr>								
											</table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultClasificacionDependenciaByCaracteristicaDependencia"
								rendered="#{clasificacionDependenciaForm.showClasificacionDependenciaByCaracteristicaDependencia&&
								!clasificacionDependenciaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{clasificacionDependenciaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{clasificacionDependenciaForm.selectClasificacionDependencia}"
                                						styleClass="listitem">
                                						<f:param name="idClasificacionDependencia" value="#{result.idClasificacionDependencia}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultClasificacionDependenciaByDependencia"
								rendered="#{clasificacionDependenciaForm.showClasificacionDependenciaByDependencia&&
								!clasificacionDependenciaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{clasificacionDependenciaForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{clasificacionDependenciaForm.selectClasificacionDependencia}"
                                						styleClass="listitem">
                                						<f:param name="idClasificacionDependencia" value="#{result.idClasificacionDependencia}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataClasificacionDependencia"
								rendered="#{clasificacionDependenciaForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!clasificacionDependenciaForm.editing&&!clasificacionDependenciaForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{clasificacionDependenciaForm.editing&&!clasificacionDependenciaForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{clasificacionDependenciaForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{clasificacionDependenciaForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="clasificacionDependenciaForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{clasificacionDependenciaForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!clasificacionDependenciaForm.editing&&!clasificacionDependenciaForm.deleting&&clasificacionDependenciaForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{clasificacionDependenciaForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!clasificacionDependenciaForm.editing&&!clasificacionDependenciaForm.deleting&&clasificacionDependenciaForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{clasificacionDependenciaForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{clasificacionDependenciaForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{clasificacionDependenciaForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{clasificacionDependenciaForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{clasificacionDependenciaForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{clasificacionDependenciaForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
											<f:subview 
												id="viewDataClasificacionDependencia"
												rendered="#{clasificacionDependenciaForm.adding}">
												<f:verbatim><tr>
													<td>
														Region
													</td>
													<td width="100%"></f:verbatim>
						                                <h:selectOneMenu value="#{clasificacionDependenciaForm.findIdRegion}"
	                                                    	immediate="false">
															<f:selectItem itemLabel="Seleccione" itemValue="0" />
	                                                        <f:selectItems value="#{clasificacionDependenciaForm.findColRegion}" />
	                                                    </h:selectOneMenu>
	                                                    
														
													<f:verbatim></td>
												</tr></f:verbatim>
												
												<f:verbatim><tr>
													<td>
														C�digo Entidad
													</td>
													<td width="100%"></f:verbatim>
																<h:inputText size="12"
															maxlength="12"
															value="#{clasificacionDependenciaForm.findCodDependencia}"
															onchange="javascript:this.value=this.value.toUpperCase()"
															                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"                        																						
															 />
	
														<h:commandButton image="/images/find2.gif" 
															action="#{clasificacionDependenciaForm.findDependenciaByCodigoAndRegion}"
															onclick="javascript:return clickMade()" />
													<f:verbatim></td>
												</tr></f:verbatim>		
											</f:subview>
											<f:verbatim>
												<tr>
												<td>
													
												</td>
												<td ></f:verbatim>
													<h:outputText 		
													rendered="#{clasificacionDependenciaForm.showFindDependencia}"												
													value="#{clasificacionDependenciaForm.dependencia}"																												                         																						
												/>

															
												<f:verbatim></td>
											</tr></f:verbatim>	
                        					<f:verbatim><tr>
                        						<td>
                        							Caracter�stica
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{clasificacionDependenciaForm.selectCaracteristicaDependencia}"
														id="caracteristicaDependencia"
                                                    	disabled="#{!clasificacionDependenciaForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Caracter�stica">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{clasificacionDependenciaForm.colCaracteristicaDependencia}" />
														
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{clasificacionDependenciaForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!clasificacionDependenciaForm.editing&&!clasificacionDependenciaForm.deleting&&clasificacionDependenciaForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{clasificacionDependenciaForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!clasificacionDependenciaForm.editing&&!clasificacionDependenciaForm.deleting&&clasificacionDependenciaForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{clasificacionDependenciaForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{clasificacionDependenciaForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{clasificacionDependenciaForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{clasificacionDependenciaForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{clasificacionDependenciaForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{clasificacionDependenciaForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>