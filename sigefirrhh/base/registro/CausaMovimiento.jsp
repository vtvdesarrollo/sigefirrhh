<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

f
<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{causaMovimientoForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formCausaMovimiento">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Causas del Movimiento
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/registro/CausaMovimiento.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{causaMovimientoForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!causaMovimientoForm.editing&&!causaMovimientoForm.deleting&&causaMovimientoForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchCausaMovimiento"
								rendered="#{!causaMovimientoForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Movimiento
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{causaMovimientoForm.findSelectMovimientoPersonal}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{causaMovimientoForm.findColMovimientoPersonal}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{causaMovimientoForm.findCausaMovimientoByMovimientoPersonal}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													C�digo
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="3"
														maxlength="3"
														value="#{causaMovimientoForm.findCodCausaMovimiento}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{causaMovimientoForm.findCausaMovimientoByCodCausaMovimiento}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
															<f:verbatim><tr>
												<td>
													Descripci�n
												</td>
												<td width="100%"></f:verbatim>
															<h:inputText size="60"
														maxlength="60"
														value="#{causaMovimientoForm.findDescripcion}"
														onchange="javascript:this.value=this.value.toUpperCase()"
														                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        																						
														 />

															<h:commandButton image="/images/find.gif" 
														action="#{causaMovimientoForm.findCausaMovimientoByDescripcion}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultCausaMovimientoByMovimientoPersonal"
								rendered="#{causaMovimientoForm.showCausaMovimientoByMovimientoPersonal&&
								!causaMovimientoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{causaMovimientoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{causaMovimientoForm.selectCausaMovimiento}"
                                						styleClass="listitem">
                                						<f:param name="idCausaMovimiento" value="#{result.idCausaMovimiento}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultCausaMovimientoByCodCausaMovimiento"
								rendered="#{causaMovimientoForm.showCausaMovimientoByCodCausaMovimiento&&
								!causaMovimientoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{causaMovimientoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{causaMovimientoForm.selectCausaMovimiento}"
                                						styleClass="listitem">
                                						<f:param name="idCausaMovimiento" value="#{result.idCausaMovimiento}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultCausaMovimientoByDescripcion"
								rendered="#{causaMovimientoForm.showCausaMovimientoByDescripcion&&
								!causaMovimientoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{causaMovimientoForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{causaMovimientoForm.selectCausaMovimiento}"
                                						styleClass="listitem">
                                						<f:param name="idCausaMovimiento" value="#{result.idCausaMovimiento}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataCausaMovimiento"
								rendered="#{causaMovimientoForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!causaMovimientoForm.editing&&!causaMovimientoForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{causaMovimientoForm.editing&&!causaMovimientoForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{causaMovimientoForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{causaMovimientoForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="causaMovimientoForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{causaMovimientoForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!causaMovimientoForm.editing&&!causaMovimientoForm.deleting&&causaMovimientoForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{causaMovimientoForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!causaMovimientoForm.editing&&!causaMovimientoForm.deleting&&causaMovimientoForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{causaMovimientoForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{causaMovimientoForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{causaMovimientoForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{causaMovimientoForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{causaMovimientoForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{causaMovimientoForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>

                        					<f:verbatim><tr>
                        						<td>
                        							Movimiento
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{causaMovimientoForm.selectMovimientoPersonal}"
														id="movimientoPersonal"
                                                    	disabled="#{!causaMovimientoForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="false"
														title="Movimiento">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{causaMovimientoForm.colMovimientoPersonal}" />
                                                    </h:selectOneMenu>													<h:message for="movimientoPersonal" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							C�digo
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="3"
	                        							id="codCausaMovimiento"
	                        							maxlength="3"
                        								value="#{causaMovimientoForm.causaMovimiento.codCausaMovimiento}"
                        								readonly="#{!causaMovimientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="codCausaMovimiento" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Descripci�n
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="60"
	                        							id="descripcion"
	                        							maxlength="60"
                        								value="#{causaMovimientoForm.causaMovimiento.descripcion}"
                        								readonly="#{!causaMovimientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="descripcion" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Formato
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="8"
	                        							id="formato"
	                        							maxlength="8"
                        								value="#{causaMovimientoForm.causaMovimiento.formato}"
                        								readonly="#{!causaMovimientoForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								                        								onkeypress="return keyEnterCheck(event, this)"
                        								                        																						                        								required="false">
                        							</h:inputText>
                        								
                        																					<h:message for="formato" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Definido por MPF?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{causaMovimientoForm.causaMovimiento.sujetoLefp}"
	                                                    id="sujetoLefp"
                                                    	readonly="#{!causaMovimientoForm.editing}"
                        								 onchange="this.form.submit()"                                                     	immediate="false"
                                                    	required="false"
														title="Definido por MPF?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{causaMovimientoForm.listSujetoLefp}" />
                                                    </h:selectOneMenu>													<h:message for="sujetoLefp" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Aplica a ?
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{causaMovimientoForm.causaMovimiento.grado99}"
	                                                    id="grado99"
                                                    	readonly="#{!causaMovimientoForm.editing}"                        								                                                    	immediate="false"
                                                    	required="false"
														title="Aplica a ?">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{causaMovimientoForm.listGrado99}" />
                                                    </h:selectOneMenu>													<h:message for="grado99" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{causaMovimientoForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!causaMovimientoForm.editing&&!causaMovimientoForm.deleting&&causaMovimientoForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{causaMovimientoForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!causaMovimientoForm.editing&&!causaMovimientoForm.deleting&&causaMovimientoForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{causaMovimientoForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{causaMovimientoForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{causaMovimientoForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{causaMovimientoForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{causaMovimientoForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{causaMovimientoForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>
