<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>


<tags:seguridad />


<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
		  if ( ((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	  }
	%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Sigefirrhh</title>
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript> 
	
		
 	</script>
 	
	<jsp:include page="/inc/functions.jsp" />
		
</head>

<body onload="firstFocus();">
<f:view>
	<x:saveState value="#{detalleTabuladorMreForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
<h:form id="formDetalleTabuladorMre">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top">
					<tr>
						<!-- Agregar y Busqueda -->
						<td valign="top">
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<b>
											Detalle Tabulador MRE
										</b>
									</td>
									<td align="right">
										<a href="#" onclick="window.open
											('/sigefirrhh//help/sigefirrhh/base/mre/DetalleTabuladorMre.jsp',
											'helpwindow',
											'toolbar=1,resizable=1,width=660,height=480,scrollbars=1');">
											<h:graphicImage url="/images/help/help.gif" />
										</a>
									</td>
								</tr>
							</table>
							<table width="100%" class="toptable">
								<tr>
									<td align="left">
										<table width="100%" class="bandtable">
											<tr>
												<td style="text-align: right">
													<h:commandButton image="/images/add.gif"
														action="#{detalleTabuladorMreForm.add}"
														onclick="javascript:return clickMade()"
														rendered="#{!detalleTabuladorMreForm.editing&&!detalleTabuladorMreForm.deleting&&detalleTabuladorMreForm.login.agregar}" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left">
										<h:messages errorClass="error" styleClass="success" />
									</td>
								</tr>
							<f:subview 
								id="searchDetalleTabuladorMre"
								rendered="#{!detalleTabuladorMreForm.showData}">
								<f:verbatim><tr>
									<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Buscar
												</td>
											</tr>
										</table>
										
										<table width="100%" class="datatable"></f:verbatim>
														<f:verbatim><tr>
												<td>
													Escala y Tabulador
												</td>
												<td width="100%"></f:verbatim>
					                                                    <h:selectOneMenu value="#{detalleTabuladorMreForm.findSelectTabulador}"
                                                    	immediate="false">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleTabuladorMreForm.findColTabulador}" />
                                                    </h:selectOneMenu>
																		<h:commandButton image="/images/find.gif" 
														action="#{detalleTabuladorMreForm.findDetalleTabuladorMreByTabulador}"
														onclick="javascript:return clickMade()" />
												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim></table>
									</td>
								</tr>
							</table></f:verbatim>
							</f:subview>
							<f:subview
								id="viewResultDetalleTabuladorMreByTabulador"
								rendered="#{detalleTabuladorMreForm.showDetalleTabuladorMreByTabulador&&
								!detalleTabuladorMreForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Resultado de B�squeda
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td></f:verbatim>
                                            <h:dataTable id="result"
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column"
                                                var="result"
                                				value="#{detalleTabuladorMreForm.result}"
                                              
                                                rows="10"
                                                width="100%">
                                				<h:column>
                                					<h:commandLink value="#{result}"
                                						action="#{detalleTabuladorMreForm.selectDetalleTabuladorMre}"
                                						styleClass="listitem">
                                						<f:param name="idDetalleTabuladorMre" value="#{result.idDetalleTabuladorMre}" />
                                					</h:commandLink>
                                				</h:column>
                                            </h:dataTable>
                                            <h:panelGrid columns="1" styleClass="scrollerTable2" columnClasses="standardTable_ColumnCentered">
                                                <x:dataScroller id="scroll_3"
                                                    for="result"
                                                    fastStep="10"
                                                    pageCountVar="pageCount"
                                                    pageIndexVar="pageIndex">
        	                                        <f:facet name="first" >
    	                                            	<h:graphicImage url="/images/arrow-first.gif"  />
	                                                </f:facet>
                                                	<f:facet name="last">
                                            	    	<h:graphicImage url="/images/arrow-last.gif"  />
                                        	        </f:facet>
                                    	        	<f:facet name="previous">
                                	            		<h:graphicImage url="/images/arrow-previous.gif"  />
                            	                	</f:facet>
                        	                    	<f:facet name="next">
                    	                        		<h:graphicImage url="/images/arrow-next.gif"  />
                	                            	</f:facet>
            	                                	<f:facet name="fastforward">
        	                                    		<h:graphicImage url="/images/arrow-ff.gif" />
    	                                        	</f:facet>
	                                            	<f:facet name="fastrewind">
                                            			<h:graphicImage url="/images/arrow-fr.gif"  />
                                            		</f:facet>
                                            	</x:dataScroller>
	                                            <x:dataScroller id="scroll_4"
    	                                        	for="result"
        	                                    	pageCountVar="pageCount"
            	                                	pageIndexVar="pageIndex" >
                	                        	    <h:outputFormat value="#{custMessages['data_scroller_pages']}" >
                    	        	            	    <f:param value="#{pageIndex}" />
                        	        	            	<f:param value="#{pageCount}" />
	                            	                </h:outputFormat>
                                        	    </x:dataScroller>
                                            </h:panelGrid>
                        				<f:verbatim></td>
                    				</tr>
                    			</table></f:verbatim>
							</f:subview>
</h:form>
							<f:subview 
								id="viewDataDetalleTabuladorMre"
								rendered="#{detalleTabuladorMreForm.showData}">
								<f:verbatim><table class="toptable" width="100%">
									<tr><td>
										<table width="100%" class="bandtable">
											<tr>
												<td></f:verbatim>
													<h:outputText value="Consultando"
														rendered="#{!detalleTabuladorMreForm.editing&&!detalleTabuladorMreForm.deleting}" />
													<h:outputText value="Modificando"
														rendered="#{detalleTabuladorMreForm.editing&&!detalleTabuladorMreForm.adding}" />
													<h:outputText value="Eliminando"
														rendered="#{detalleTabuladorMreForm.deleting}" />
													<h:outputText value="Agregando"
														rendered="#{detalleTabuladorMreForm.adding}" />
												<f:verbatim></td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>	
										</f:verbatim>
<h:form id="detalleTabuladorMreForm">
                            				<f:verbatim><table class="datatable" width="100%">
                        					<tr>
	                        					<td colspan="2">
                                    				<table align="center">
                                    					<tr>
                                    						<td></f:verbatim>
                                                				<h:commandButton value="Modificar" 
                                                					image="/images/modify.gif"
                                                					action="#{detalleTabuladorMreForm.edit}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!detalleTabuladorMreForm.editing&&!detalleTabuladorMreForm.deleting&&detalleTabuladorMreForm.login.modificar}" />
                                                				<h:commandButton value="Eliminar"
                                                					image="/images/delete.gif"
                                                					action="#{detalleTabuladorMreForm.delete}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{!detalleTabuladorMreForm.editing&&!detalleTabuladorMreForm.deleting&&detalleTabuladorMreForm.login.eliminar}" />
                                                				<h:outputText
                                                					value="�Seguro que desea eliminar?"
                                                					rendered="#{detalleTabuladorMreForm.deleting}" />
                                        				
                                                				<h:commandButton value="Si" 
                                                					image="/images/yes.gif"
                                                					action="#{detalleTabuladorMreForm.deleteOk}"
                                                					onclick="javascript:return clickMade()"
                                                					rendered="#{detalleTabuladorMreForm.deleting}" />
                                                			
                                                				<h:commandButton value="Guardar" 
                                                					image="/images/save.gif"
                                                					action="#{detalleTabuladorMreForm.save}"
                                                					onclick="javascript:return clickMade()"
                                        	        				rendered="#{detalleTabuladorMreForm.editing}" />
                                        	        			<h:commandButton value="Cancelar" 
			                                    					image="/images/cancel.gif"
			                                    					action="#{detalleTabuladorMreForm.abort}"
			                                    					immediate="true"
			                                    					onclick="javascript:return clickMade()"
			                            	        				 />
                                                                <f:verbatim>
                                                			</td>
                                                		</tr>
            										</table>
												</td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Escala y Tabulador
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleTabuladorMreForm.selectTabulador}"
														id="tabulador"
                                                    	disabled="#{!detalleTabuladorMreForm.editing}"
                        								                        								                                                    	immediate="false"
                                                    	required="true"
														title="Tabulador">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                        <f:selectItems value="#{detalleTabuladorMreForm.colTabulador}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="tabulador" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Nivel
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="nivel"
	                        							maxlength="10"
                        								value="#{detalleTabuladorMreForm.detalleTabuladorMre.nivel}"
                        								readonly="#{!detalleTabuladorMreForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								                        								onkeypress="return keyIntegerCheck(event, this)"
                        								onblur="javascript:fieldEmpty(this)"
                        								                        								                        																						                        								required="true">
                        							</h:inputText>
                        								
                        								<f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="nivel" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Asignacion Mensual
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="asignacionMensual"
	                        							maxlength="10"
                        								value="#{detalleTabuladorMreForm.detalleTabuladorMre.asignacionMensual}"
                        								readonly="#{!detalleTabuladorMreForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="asignacionMensual" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Fluctuaci�n Mensual
                        						</td>
                        						<td></f:verbatim>
					                        		<h:inputText
	                        							size="10"
	                        							id="fluctuacionMensual"
	                        							maxlength="10"
                        								value="#{detalleTabuladorMreForm.detalleTabuladorMre.fluctuacionMensual}"
                        								readonly="#{!detalleTabuladorMreForm.editing}"
                        								onchange="javascript:this.value=this.value.toUpperCase()"
                        								                        								onkeypress="return keyFloatCheck(event, this)"
                        								
                        								                        								                        								                        																						 style="text-align:right"                         								required="false">
														<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
                        							</h:inputText>
                        								
                        																					<h:message for="fluctuacionMensual" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Pais
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleTabuladorMreForm.selectPaisForCiudad}"
                                                    	disabled="#{!detalleTabuladorMreForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="PaisForCiudad"
														valueChangeListener="#{detalleTabuladorMreForm.changePaisForCiudad}"
                        																						immediate="false"
														required="true"
														title="Ciudad">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{detalleTabuladorMreForm.colPaisForCiudad}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>												<f:verbatim></td>
											</tr></f:verbatim>
											<f:verbatim>
											<tr>
                        						<td>
                        							Estado
                        						</td>
                        						<td></f:verbatim>
													<h:selectOneMenu value="#{detalleTabuladorMreForm.selectEstadoForCiudad}"
                                                    	disabled="#{!detalleTabuladorMreForm.editing}"
                                                    	onchange="this.form.submit()"
	                        							id="EstadoForCiudad"
														valueChangeListener="#{detalleTabuladorMreForm.changeEstadoForCiudad}"
														rendered="#{detalleTabuladorMreForm.showEstadoForCiudad
														 }"
														immediate="false"
														required="true"
														title="Ciudad">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
														<f:selectItems value="#{detalleTabuladorMreForm.colEstadoForCiudad}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
													</h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>												<f:verbatim></td>
											</tr></f:verbatim>
                        					<f:verbatim><tr>
                        						<td>
                        							Ciudad
                        						</td>
                        						<td></f:verbatim>
                                                    <h:selectOneMenu value="#{detalleTabuladorMreForm.selectCiudad}"
	                                                    id="ciudad"
                                                    	disabled="#{!detalleTabuladorMreForm.editing}"
                                                    	rendered="#{detalleTabuladorMreForm.showCiudad
														 }"
                                                    	immediate="false"
                                                    	required="true"
														title="Ciudad">
														<f:selectItem itemLabel="Seleccione" itemValue="0" />
                                                    	<f:selectItems value="#{detalleTabuladorMreForm.colCiudad}" />
														<f:validator validatorId="eforserver.RequiredJSFValidator"/>
                                                    </h:selectOneMenu><f:verbatim><span class="required"> *</span></f:verbatim>													<h:message for="ciudad" styleClass="error"/>
                        						<f:verbatim></td>
                        					</tr></f:verbatim>
                        				<f:verbatim></table>
                        				<table class="datatable" align="center">
                        					<tr>
                        						
                        						<td></f:verbatim>
                                    				<h:commandButton value="Modificar" 
                                    					image="/images/modify.gif"
                                    					action="#{detalleTabuladorMreForm.edit}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!detalleTabuladorMreForm.editing&&!detalleTabuladorMreForm.deleting&&detalleTabuladorMreForm.login.modificar}" />
                                    				<h:commandButton value="Eliminar"
                                    					image="/images/delete.gif"
                                    					action="#{detalleTabuladorMreForm.delete}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{!detalleTabuladorMreForm.editing&&!detalleTabuladorMreForm.deleting&&detalleTabuladorMreForm.login.eliminar}" />
                                    				<h:outputText
                                    					value="�Seguro que desea eliminar?"
                                    					rendered="#{detalleTabuladorMreForm.deleting}" />
                            				
                                    				<h:commandButton value="Si" 
                                    					image="/images/yes.gif"
                                    					action="#{detalleTabuladorMreForm.deleteOk}"
                                    					onclick="javascript:return clickMade()"
                                    					rendered="#{detalleTabuladorMreForm.deleting}" />
                                    			
                                    				<h:commandButton value="Guardar" 
                                    					image="/images/save.gif"
                                    					action="#{detalleTabuladorMreForm.save}"
                                    					onclick="javascript:return clickMade()"
                            	        				rendered="#{detalleTabuladorMreForm.editing}" />
                                    				<h:commandButton value="Cancelar" 
                                    					image="/images/cancel.gif"
                                    					action="#{detalleTabuladorMreForm.abort}"
                                    					immediate="true"
                                    					onclick="javascript:return clickMade()"
                            	        				 />

                                                    <f:verbatim>
                                    			</td>
                                    		</tr>
										</table></f:verbatim>
</h:form>
                        				<f:verbatim></td>
                        			</tr>
                        		</table></f:verbatim>
                        	</f:subview>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>
