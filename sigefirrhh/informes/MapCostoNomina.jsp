<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<%	if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
</head>

<body>

<f:view>


<f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
<jsp:include page="/inc/top.jsp" />
<x:saveState value="#{mapCostoNominaForm}" />
<h:form id="formMapCostoNomina" enctype="multipart/form-data">
<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
	<tr>
		<td width="100%" valign="top" align="center">
			
			
				<MAP name=Venezuela><AREA alt=Bolivar 
					coords=310,161,420,264 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='7';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Amazonas coords=225,243,297,367 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='2';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Guyana coords=460,134,515,348 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='0';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Monagas coords=332,81,375,119 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='16';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Delta coords=383,77,445,137 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='10';
					document.forms['formMapCostoNomina'].submit()" shape=RECT
					target=""><AREA alt=Apure coords=139,156,218,206 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='4';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Sucre coords=318,53,384,70 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='19';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Anzoategui coords=287,80,320,149 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='3';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt="Nueva Esparta" coords=273,31,326,48 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='17';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Guarico coords=203,105,272,150 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='12';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Miranda coords=232,70,254,85 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='15';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Barinas coords=97,132,130,163 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='6';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt="Dtto Federal" coords=225,60,240,67 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='1';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Aragua coords=205,65,219,92 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='5';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Tachira coords=44,141,66,169 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='20';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Carabobo coords=183,69,195,84 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='8';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Cojedes coords=159,83,187,129 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='9';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Yaracuy coords=164,70,170,82 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='22';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Merida coords=66,123,89,138 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='14';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Portuguesa coords=116,100,153,125 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='18';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Lara coords=103,68,150,90 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='13';
					document.forms['formMapCostoNomina'].submit()" shape=RECT
					target=""><AREA alt=Falcon coords=93,15,173,60 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='11';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Trujillo coords=89,96,107,112 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='21';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target=""><AREA alt=Zulia coords=14,11,80,115 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='23';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target="">
				</MAP>
				<MAP name=ve1Map><AREA alt="Estado Zulia" 
					coords=76,13,46,26,35,46,21,63,16,83,6,113,27,117,39,132,77,114,82,86,96,81,95,62,78,48 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='23';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Zulia"><AREA alt="Estado T�chira" coords=44,136,65,145 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='20';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target="" title="Estado T�chira"><AREA alt="Estado  M�rida" 
					coords=63,129,88,112,103,122,92,127,86,134,84,143 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='14';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado  M�rida"><AREA alt="Estado Trujillo" 
					coords=105,116,108,108,109,95,114,93,98,88,91,91,87,97,82,104 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='21';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Trujillo"><AREA alt="Lara"
					coords=102,74,122,66,146,63,146,84,137,92,122,90,105,84 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='13';
					document.forms['formMapCostoNomina'].submit()" shape=POLY><AREA 
					alt="Estado Falc�n" coords=84,46,101,65,144,57,145,35,131,11,111,11 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='11';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Falc�n"><AREA alt="Estado Portuguesa" 
					coords=110,123,118,95,129,100,143,99,143,108,142,124,145,136 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='18';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Portuguesa"><AREA alt="Estado Barinas" 
					coords=90,142,90,135,98,129,107,125,111,129,121,132,133,137,136,143 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='6';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Barinas"><AREA coords="" 
					href="http://" 
					shape=POLY>
				</MAP>
				<MAP name=v4Map><AREA alt="Estado T�chira" 
					coords=47,2,76,2,80,13,71,21,58,23,55,19,44,18,42,11 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='20';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado T�chira"><AREA alt="Estado Apure" 
					coords=41,22,53,42,105,47,133,58,144,71,145,11,121,21,103,25,83,21 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='4';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Apure"><AREA alt="Barinas"
					coords=81,19,91,3,139,3,139,6,125,13,109,20 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='6';
					document.forms['formMapCostoNomina'].submit()" 
					shape=POLY></MAP>
				<MAP name=v5Map>
				<AREA coords="" 
					href="#" 
					shape=POLY><AREA alt="Estado Apure" 
					coords=3,74,56,65,63,50,69,42,70,32,74,24,89,20,83,10,68,10,49,6,25,8,9,8,5,8,2,40 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='4';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Apure"><AREA alt="Estado Amazonas" 
					coords=49,121,41,93,59,67,79,71,101,70,130,65,139,60,136,75,141,95,149,101,157,108,159,119 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='2';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Amazonas"><AREA alt="Estado Bol�var" 
					coords=65,66,69,50,76,32,87,25,97,15,110,10,130,10,144,12,159,13,169,13,176,10,179,23,177,31,176,42,176,55,176,79,178,97,176,111,162,113,159,100,137,76,148,59,140,51,125,54,110,61,92,58 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='7';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Bol�var"><AREA alt="Estado Guarico" 
					coords=58,2,71,8,93,9,125,7,132,2 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='12';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Guarico"></MAP><MAP name=v8Map><AREA 
					alt="Estado Amazonas" 
					coords=48,4,60,21,48,38,73,65,88,101,120,110,171,89,180,78,176,59,174,46,171,24,170,7,175,4 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='2';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Amazonas"></MAP><MAP name=v6Map><AREA 
					alt="Estado Bol�var" 
					coords=1,104,4,120,91,118,99,105,101,86,85,63,85,50,88,30,110,26,103,11,100,3,77,5,58,3,20,3,4,1,2,46,1,79 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='7';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Bol�var"><AREA alt="Zona en Reclamaci�n" 
					coords=117,118,196,118,194,2,109,2,112,17,110,31,95,39,92,49,92,61,104,72,107,90 
					href="" shape=POLY 
					target="" title="Zona en Reclamaci�n"></MAP><MAP name=ve3Map><AREA 
					coords=15,144,27,140,32,136,42,136,49,135,58,138,67,142 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='7';
					document.forms['formMapCostoNomina'].submit()" 
					shape=POLY>
				<AREA coords=118,143,137,129,156,133,166,140 
					href="#" 
					shape=POLY><AREA alt="Estado Delta Amacuro" 
					coords=50,74,53,85,54,92,50,99,56,111,60,120,64,132,71,140,79,143,88,145,109,138,118,136,131,125,100,93,81,71 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='10';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Delta Amacuro"><AREA alt="Estado Monagas" 
					coords=52,129,30,131,22,128,15,119,3,111,2,97,5,76,44,80 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='16';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Monagas"><AREA alt="Estado Sucre" 
					coords=4,53,58,54,49,61,43,68,41,73,29,71,5,71 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='19';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Sucre"><AREA alt="Estado Anzoategui" 
					coords=2,117,24,133,16,137,3,141 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='2';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Anzoategui"></MAP><MAP name=v9Map><AREA 
					coords=111,3,127,12,123,31,125,48,151,71,159,83,180,55,188,40,193,30,192,14,192,2 
					href="#" 
					shape=POLY><AREA coords="" 
					href="#" 
					shape=POLY><AREA alt="Estado Amazonas" coords=0,42,33,85 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='2';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					target="" title="Estado Amazonas"></MAP><MAP name=ve2Map><AREA 
					alt="Estado Sucre" coords=155,67,171,80,178,63 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='19';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Sucre"><AREA alt="Estado Anzoategui" 
					coords=116,83,131,74,150,72,163,82,171,90,172,111,179,118,179,134,171,142,160,142,143,142,141,121,125,112,125,97 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='3';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Anzoategui"><AREA alt="Estado Guarico" 
					coords=46,92,39,103,47,111,43,125,51,140,136,143,137,128,128,119,119,109,116,87,92,85,90,107,71,107,73,93 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='12';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Guarico"><AREA alt="Estado Nueva Esparta" 
					coords=113,21,181,51 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='17';
					document.forms['formMapCostoNomina'].submit()" shape=RECT 
					title="Estado Nueva Esparta"><AREA alt="Distrito Capital" 
					coords=81,67,93,64,85,60,82,63,77,63 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='1';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Distrito Capital"><AREA alt="Estado Miranda" 
					coords=78,73,108,61,116,69,125,75,106,83,88,80 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='15';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Miranda"><AREA alt="Estado Aragua" 
					coords=50,65,65,65,70,78,84,85,84,97,74,88,61,87 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='5';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Aragua"><AREA alt="Estado Carabobo" 
					coords=35,63,44,65,47,80,54,89,42,87,34,82 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='8';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Carabobo"><AREA alt="Estado Cojedes" 
					coords=39,127,20,126,14,108,9,98,6,92,16,86,29,87,37,91 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='9';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Cojedes"><AREA alt="Estado Yaracuy" 
					coords=6,83,22,80,27,67,21,59,13,59 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='22';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Yaracuy"><AREA alt="Estado Barinas" 
					coords=3,143,11,132,39,135,44,143 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='6';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Barinas"><AREA alt="Estado Portuguesa" 
					coords=1,137,10,128,8,114,2,110 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='18';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Portuguesa"><AREA alt="Estado Falc�n" 
					coords=3,54,29,54,6,33 
					href="javascript:document.forms['formMapCostoNomina'].elements['formMapCostoNomina:idEstado'].value='11';
					document.forms['formMapCostoNomina'].submit()" shape=POLY 
					target="" title="Estado Falc�n"><AREA coords=69,63,70,63,88,60 
					href="#" 
					shape=POLY><AREA coords=89,56,96,62,77,58,77,68,68,62 
					href="#" 
					shape=POLY>
				</MAP>
			<p></p>
    		<h:inputHidden
				id="idEstado"
				value="#{mapCostoNominaForm.idEstado}"
				valueChangeListener="#{mapCostoNominaForm.changeIdEstado}"
				immediate="false" />
			<p></p>
			<TABLE align=center border=0 cellPadding=0 cellSpacing=0 width=541>
				<TBODY>
					<TR>
						<TD><IMG border=0 height=1 src="../../images/mapa/spacer.gif" 
						width=148></TD>
						<TD><IMG border=0 height=1 src="../../images/mapa/spacer.gif" 
						width=184></TD>
						<TD><IMG border=0 height=1 src="../../images/mapa/spacer.gif" 
						width=209></TD>
						<TD><IMG border=0 height=1 src="../../images/mapa/spacer.gif" 
						width=1></TD></TR>
					<TR>
						<TD><IMG border=0 height=146 name=ve1 
						src="../../images/mapa/ve1.gif" useMap=#ve1Map width=148></TD>
						<TD><IMG border=0 height=146 name=ve2 
						src="../../images/mapa/ve2.gif" useMap=#ve2Map width=184></TD>
						<TD><IMG border=0 height=146 name=ve3 
						src="../../images/mapa/ve3.gif" useMap=#ve3Map width=209></TD>
						<TD><IMG border=0 height=146 
						src="../../images/mapa/spacer.gif" width=1></TD></TR>
					<TR>
						<TD><IMG border=0 height=124 name=v4 
						src="../../images/mapa/v4.gif" useMap=#v4Map width=148></TD>
						<TD><IMG border=0 height=124 name=v5 
						src="../../images/mapa/v5.gif" useMap=#v5Map width=184></TD>
						<TD><IMG border=0 height=124 name=v6 
						src="../../images/mapa/v6.gif" useMap=#v6Map width=209></TD>
						<TD><IMG border=0 height=124 
						src="../../images/mapa/spacer.gif" width=1></TD></TR>
					<TR>
						<TD><IMG border=0 height=114 name=v7 
						src="../../images/mapa/v7.gif" width=148></TD>
						<TD><IMG border=0 height=114 name=v8 
						src="../../images/mapa/v8.gif" useMap=#v8Map width=184></TD>
						<TD><IMG border=0 height=114 name=v9 
						src="../../images/mapa/v9.gif" useMap=#v9Map width=209></TD>
						<TD><IMG border=0 height=114 
						src="../../images/mapa/spacer.gif" 
						width=1></TD>
					</TR>
				</TBODY>
			</TABLE>
		</td>
	</tr>
	<tr>
	<td>
		<h:commandButton 
            image="/images/mapa-venezuela.gif"									
			action="#{mapCostoNominaForm.nivelNacional}"    						
 		/>
	</td>
	</tr>
	<tr>
		<td class="title">
		Costos de N�mina
		</td>
	</tr>
	<tr>
		<td>
		&nbsp;
		</td>
	</tr>
	<tr>
		<td class="sub-bandtable">
		Criterios de b�squeda
		</td>
	</tr>
	<tr>
		<td>
				
				
					<table width=100% class="datatable">
					<tr>
  						<td>
  							Tipo de Personal
  						</td>
  						<td width="100%">
  							<h:selectOneMenu value="#{mapCostoNominaForm.selectTipoPersonal}"
   								onchange="this.form.submit()"  
                           	 	valueChangeListener="#{mapCostoNominaForm.changeTipoPersonal}"
                            	immediate="false">
                            	<f:selectItem itemLabel="Todos" itemValue="0" />
                            	<f:selectItems value="#{mapCostoNominaForm.listTipoPersonal}" />                                                
                            </h:selectOneMenu>
  						</td>
  					</tr>	
  					<tr>
						<td width="20%">
							A�o
						</td>
						<td width="30%" colspan="3">    									
							<h:inputText								
								size="5"
								id="Anio"
								maxlength="4"
								value="#{mapCostoNominaForm.anio}"   																										
								required="false" />    								
								
                        		<h:commandButton 
	                        		image="/images/check.gif"
	                        		immediate="false"									
									action="#{mapCostoNominaForm.actualizar}"    						
		    						/>
						</td>												
    					
					</tr>		
  					<tr>
						<td>
							Desde Mes
						</td>
						<td width="20%">
							<h:selectOneMenu value="#{mapCostoNominaForm.mesDesde}"    											    											
								onchange="this.form.submit()"
                            	immediate="false">
                                <f:selectItem itemLabel="Enero" itemValue="1" />
                            	<f:selectItem itemLabel="Febrero" itemValue="2" />
                            	<f:selectItem itemLabel="Marzo" itemValue="3" />
                            	<f:selectItem itemLabel="Abril" itemValue="4" />
                            	<f:selectItem itemLabel="Mayo" itemValue="5" />
                            	<f:selectItem itemLabel="Junio" itemValue="6" />
                            	<f:selectItem itemLabel="Julio" itemValue="7" />
                            	<f:selectItem itemLabel="Agosto" itemValue="8" />
                            	<f:selectItem itemLabel="Septiembre" itemValue="9" />
                            	<f:selectItem itemLabel="Octubre" itemValue="10" />
                            	<f:selectItem itemLabel="Noviembre" itemValue="11" />
                            	<f:selectItem itemLabel="Diciembre" itemValue="12" />                                            											
                            </h:selectOneMenu>
						</td>
					</tr>
					<tr>
						<td>
							Hasta Mes
						</td>
						<td width="20%">
							<h:selectOneMenu value="#{mapCostoNominaForm.mesHasta}"    											    											
								onchange="this.form.submit()"
                            	immediate="false">
                                <f:selectItem itemLabel="Enero" itemValue="1" />
                            	<f:selectItem itemLabel="Febrero" itemValue="2" />
                            	<f:selectItem itemLabel="Marzo" itemValue="3" />
                            	<f:selectItem itemLabel="Abril" itemValue="4" />
                            	<f:selectItem itemLabel="Mayo" itemValue="5" />
                            	<f:selectItem itemLabel="Junio" itemValue="6" />
                            	<f:selectItem itemLabel="Julio" itemValue="7" />
                            	<f:selectItem itemLabel="Agosto" itemValue="8" />
                            	<f:selectItem itemLabel="Septiembre" itemValue="9" />
                            	<f:selectItem itemLabel="Octubre" itemValue="10" />
                            	<f:selectItem itemLabel="Noviembre" itemValue="11" />
                            	<f:selectItem itemLabel="Diciembre" itemValue="12" />                                            											
                            </h:selectOneMenu>
						</td>
					</tr>	
					
					
					</table>

		</td>
	</tr>
	<tr>
		<td class="sub-bandtable">
		<h:outputText value="#{mapCostoNominaForm.nombreEstado}" />
		</td>
	</tr>
	<tr>
		<td>
			<h:dataTable id="result"
			    styleClass="datatable"
			    headerClass="standardTable_Header,standardTable_Header,standardTable_HeaderRight,standardTable_HeaderRight"
			    footerClass="standardTable_Header"
			    rowClasses="standardTable_Row1,standardTable_Row2"
			    columnClasses="standardTable_Column,standardTable_Column,standardTable_ColumnRight,standardTable_ColumnRight"
			    var="result"
				value="#{mapCostoNominaForm.colResultado}"			      
			    rows="100"
			    width="100%">
			    <h:column>
			    	<f:facet name="header">			    	
			    		<h:outputText value="Codigo" />
			    	</f:facet>
					<h:outputText value="#{result.codigo}" />
				</h:column>
				<h:column>
					<f:facet name="header">
			    		<h:outputText value="Nombre" />
			    	</f:facet>
				
					<h:outputText value="#{result.nombre}" />
				</h:column>
				<h:column>
					<f:facet name="header">
			    		<h:outputText value="%" />
			    	</f:facet>				
					<h:outputText value="#{result.porcentaje}" >
						<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
					</h:outputText>
				</h:column>				
				<h:column>
					<f:facet name="header">
			    		<h:outputText value="Total Bs." />
			    	</f:facet>
				
					<h:outputText value="#{result.totalCosto}" >
						<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>
					</h:outputText>
				</h:column>
			</h:dataTable>
		</td>
	</tr>
</table>
</h:form>
</f:view>

</body>

</html>