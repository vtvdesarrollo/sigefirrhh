<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ page import="sigefirrhh.login.LoginSession" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<tags:seguridad />

<% if ( !((LoginSession)session.getAttribute("loginSession")).isValid() ) {
		response.sendRedirect("/sigefirrhh/error.html");
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sigefirrhh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sigefirrhh/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	<script language=javascript>         
    	function windowReport() {
			var number = 
				document.forms['formReportAdministracionId'].elements['formReportAdministracionId:reportId'].value;
			var name = 
				document.forms['formReportAdministracionId'].elements['formReportAdministracionId:reportName'].value;
			var url = '/sigefirrhh/reports/process.jsp?reportName=' + name + number;
			window.open(url, 'report' + number, 'width=600,height=500,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no,resizable=yes,left=100,top=50,screenX=100,screenY=50');
    	}
    </script>
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{reportGraficosForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">

				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formReportAdministracionId">
				
    				<h:inputHidden id="reportId" value="#{reportGraficosForm.reportId}" />
    				<h:inputHidden id="reportName" value="#{reportGraficosForm.reportName}" />
    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Gr�ficos
    							</b>
    						</td>
    						<td align="right">
    							
    							<h:commandButton value="Cerrar" 
                					image="/images/close.gif"
                					action="go_cancelOption"
                					immediate="true"                        					
                					onclick="javascript:return clickMade()"
        	        				 />
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros del Reporte
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    															    								
    								<tr>
    									<td>
    										Reporte
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{reportGraficosForm.tipoReporte}"	    										
	    										onchange="this.form.submit()"
                                            	immediate="false">
                                            	<f:selectItem itemLabel="Trabajadores por Tipo de Personal" itemValue="tipopersonal" />
                                            	<f:selectItem itemLabel="Trabajadores por Tipo de Personal y Sexo" itemValue="tipopersonal1" />
                                            	<f:selectItem itemLabel="Personal Empleado por Tipo de Cargo" itemValue="tipocargo" /> 
                                            	<f:selectItem itemLabel="Personal Empleado por Tipo de Cargo y Sexo" itemValue="tipocargo1" /> 
                                            	<f:selectItem itemLabel="Personal Empleado por Grado de Escala" itemValue="gradoscarrera" />
                                            	<f:selectItem itemLabel="Personal Empleado por Grado de Escala y Sexo" itemValue="gradoscarrera1" />                                            	
                                            	<f:selectItem itemLabel="Personal Obrero por Grado de Escala" itemValue="gradosobrero" /> 
                                            	<f:selectItem itemLabel="Personal Obrero por Grado de Escala y Sexo" itemValue="gradosobrero1" />                                            	                                            	                                           	
                                            	<f:selectItem itemLabel="Personal Empleado por Rango de Sueldo Basico" itemValue="sueldosempleados" />                                            	                                            	
                                            	<f:selectItem itemLabel="Personal Empleado por Rango de Sueldo Basico y Sexo" itemValue="sueldosempleados1" />                                            	                                            	
                                            	<f:selectItem itemLabel="Personal Contratado por Rango de Sueldo Basico" itemValue="sueldoscontratados" />                                            	                                            	
                                            	<f:selectItem itemLabel="Personal Contratado por Rango de Sueldo Basico y Sexo" itemValue="sueldoscontratados1" />                                            	                                            	
                                            	<f:selectItem itemLabel="Personal Obrero por Rango de Sueldo Basico" itemValue="sueldosobreros" />                                            	                                            	
                                            	<f:selectItem itemLabel="Personal Obrero por Rango de Sueldo Basico y Sexo" itemValue="sueldosobreros1" />                                            	                                            	
                                            	<f:selectItem itemLabel="Personal Empleado por Rango de Sueldo Integral" itemValue="sueldosintempleados" />                                            	                                            	
                                            	<f:selectItem itemLabel="Personal Empleado por Rango de Sueldo Integral y Sexo" itemValue="sueldosintempleados1" /> 
                                            	<f:selectItem itemLabel="Personal Obrero por Rango de Sueldo Integral" itemValue="sueldosintobreros" />                                            	                                            	
                                            	<f:selectItem itemLabel="Personal Obrero por Rango de Sueldo Integral y Sexo" itemValue="sueldosintobreros1" /> 
                                            	<f:selectItem itemLabel="Movimientos de Ingreso Personal Empleado" itemValue="ingresosempleados" /> 
                                            	<f:selectItem itemLabel="Movimientos de Ingreso Personal Empleado por Sexo" itemValue="ingresosempleados1" /> 
                                            	<f:selectItem itemLabel="Movimientos de Ingreso Personal Contratado" itemValue="ingresoscontratados" /> 
                                            	<f:selectItem itemLabel="Movimientos de Ingreso Personal Contratado por Sexo" itemValue="ingresoscontratados1" />                                             	
                                            	<f:selectItem itemLabel="Movimientos de Ingreso Personal Obrero" itemValue="ingresosobreros" /> 
                                            	<f:selectItem itemLabel="Movimientos de Ingreso Personal Obrero por Sexo" itemValue="ingresosobreros1" />                                             	
                                            	<f:selectItem itemLabel="Movimientos de Egreso Personal Empleado" itemValue="egresosempleados" /> 
                                            	<f:selectItem itemLabel="Movimientos de Egreso Personal Empleado por Sexo" itemValue="egresosempleados1" /> 
                                            	<f:selectItem itemLabel="Movimientos de Egreso Personal Contratado" itemValue="egresoscontratados" /> 
                                            	<f:selectItem itemLabel="Movimientos de Egreso Personal Contratado por Sexo" itemValue="egresoscontratados1" /> 
                                            	<f:selectItem itemLabel="Movimientos de Egreso Personal Obrero" itemValue="egresosobreros" /> 
                                            	<f:selectItem itemLabel="Movimientos de Egreso Personal Obrero por Sexo" itemValue="egresosobreros1" /> 
                                            	
                                            </h:selectOneMenu>
    									</td>
    								</tr>	
    								
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/report.gif"	    										
    											action="#{reportGraficosForm.runReport}"
    											onclick="windowReport()" />
    								
    									<td>
    								</tr>
    							</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>