<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x"%>
<%@ page import="sigefirrhh.login.LoginSession" %>
<f:verbatim>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
	    <td align="left" width="100%">
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="22%" align="left"></f:verbatim>
						<h:graphicImage url="/images/logo-sigefirrhh-web.jpg" />
					<f:verbatim></td>
					<td width="64%" align="center"></f:verbatim>
						<h:graphicImage value="#{loginSession.URLNombre}" />
					<f:verbatim></td>
					<td width="78%" align="right"></f:verbatim>
						<h:graphicImage value="#{loginSession.URLLogo}" />
					<f:verbatim></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td class="menutitle">
    		<table width="100%" height="22" border="0" align="right" cellpadding="0" cellspacing="0">
    			<tr>
    				<td width="25%" align="center">
	    				<jsp:include page="/version.inc" />
    				</td>
    				<td width="50%" align="center">
    					Sistema de Gesti�n Financiera de los Recursos Humanos
    				</td>
    				<td width="25%" align="center" valign="center">
    					<a href="/sigefirrhh/logout.jsf">
    					</f:verbatim>
							<h:graphicImage url="/images/buttons/exit.gif" />	
						<f:verbatim></a>										    					
    				</td>
    			</tr>
    		</table>		  
    	</td>
    </tr>
</table>
</f:verbatim>
			 <% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
			<x:jscookMenu layout="hbr" theme="ThemePanel"  rendered="#{loginSession.valid}">
			    <x:navigationMenuItem id="nav_1"  itemLabel="Planificaci�n |"   >
			        <x:navigationMenuItem id="nav_1_1" itemLabel="Estructura Geogr�fica">
				        <x:navigationMenuItem id="nav_1_1_1" itemLabel="Continentes" action="go_continenteForm" />
				        <x:navigationMenuItem id="nav_1_1_2" itemLabel="Regiones Continentales" action="go_regionContinenteForm" />	        
			            <x:navigationMenuItem id="nav_1_1_3" itemLabel="Pa�ses" action="go_paisForm" />
			            <x:navigationMenuItem id="nav_1_1_4" itemLabel="Estados" action="go_estadoForm" />
			            <x:navigationMenuItem id="nav_1_1_5" itemLabel="Ciudades" action="go_ciudadForm" />
			            <x:navigationMenuItem id="nav_1_1_6" itemLabel="Municipios" action="go_municipioForm" />
			            <x:navigationMenuItem id="nav_1_1_7" itemLabel="Parroquias" action="go_parroquiaForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_1_2" itemLabel="Estructura Organizativa">
			            <x:navigationMenuItem id="nav_1_2_1" itemLabel="Organismos" action="go_organismoForm" />
			            <x:navigationMenuItem id="nav_1_2_2" itemLabel="Nombres del Organismo" action="go_nombreOrganismoForm" />
			            <x:navigationMenuItem id="nav_1_2_3" itemLabel="Grupos del Organismo" action="go_grupoOrganismoForm" />
			            <x:navigationMenuItem id="nav_1_2_4" itemLabel="Regiones" action="go_regionForm" />
			            <x:navigationMenuItem id="nav_1_2_5" itemLabel="Sedes" action="go_sedeForm" />
			            <x:navigationMenuItem id="nav_1_2_6" itemLabel="Lugares de Pago por Sedes" action="go_lugarPagoForm" />
			            <x:navigationMenuItem id="nav_1_2_7" itemLabel="Unidades Administradoras" action="go_unidadAdministradoraForm" />
			            <x:navigationMenuItem id="nav_1_2_8" itemLabel="Unidades Ejecutoras Locales" action="go_unidadEjecutoraForm" />
			            <x:navigationMenuItem id="nav_1_2_9" itemLabel="Unidades Ejecutoras por Unidad Administradora" action="go_administradoraUelForm" />
			            <x:navigationMenuItem id="nav_1_2_10" itemLabel="Estructuras Organizativas" action="go_estructuraForm" />
			            <x:navigationMenuItem id="nav_1_2_11" itemLabel="Unidades Funcionales" action="go_unidadFuncionalForm" />			            
			            <x:navigationMenuItem id="nav_1_2_12" itemLabel="Tipos de Dependencias" action="go_tipoDependenciaForm" />
			            <x:navigationMenuItem id="nav_1_2_13" itemLabel="Dependencias Administrativas" action="go_dependenciaForm" />
			            <x:navigationMenuItem id="nav_1_2_14" itemLabel="Tipos de Caracter�sticas(Familias)" action="go_tipoCaracteristicaForm" />
			            <x:navigationMenuItem id="nav_1_2_16" itemLabel="Caracter�sticas de Dependencias" action="go_caracteristicaDependenciaForm" />			           
			            <x:navigationMenuItem id="nav_1_2_17" itemLabel="Clasificaci�n de Dependencias" action="go_clasificacionDependenciaForm" />
			            <x:navigationMenuItem id="nav_1_2_15" itemLabel="Reportes de Estructura" action="go_reportEstructuraForm" />
			        </x:navigationMenuItem>
			        
			     	<x:navigationMenuItem id="nav_1_3" itemLabel="Clases de Cargos y Tabuladores">
			            <x:navigationMenuItem id="nav_1_3_1" itemLabel="Ramos Ocupacionales" action="go_ramoOcupacionalForm" />
			            <x:navigationMenuItem id="nav_1_3_2" itemLabel="Grupos Ocupacionales" action="go_grupoOcupacionalForm" />
			            <x:navigationMenuItem id="nav_1_3_3" itemLabel="Series de Cargos" action="go_serieCargoForm" />
			            <x:navigationMenuItem id="nav_1_3_4" itemLabel="Tabuladores" action="go_tabuladorForm" />
			            <x:navigationMenuItem id="nav_1_3_5" itemLabel="Detalles de un Tabulador" action="go_detalleTabuladorForm" />
			            <x:navigationMenuItem id="nav_1_3_6" itemLabel="Clasificadores de Cargos" action="go_manualCargoForm" />
			            <x:navigationMenuItem id="nav_1_3_7" itemLabel="Cargos" action="go_cargoForm" />
			            <x:navigationMenuItem id="nav_1_3_8" itemLabel="Reporte de Cargos" action="go_reportCargosForm" />
			        </x:navigationMenuItem>
			        
			      
			        <x:navigationMenuItem id="nav_1_9" itemLabel="Clasificaci�n">
			            <x:navigationMenuItem id="nav_1_9_1" itemLabel="Perfil de un Cargo" action="go_perfilForm" />
			            <x:navigationMenuItem id="nav_1_9_2" itemLabel="Requisitos de un Cargo" >
				            <x:navigationMenuItem id="nav_1_9_2_1" itemLabel="Profesiones" action="go_profesionCargoForm" />
				            <x:navigationMenuItem id="nav_1_9_2_2" itemLabel="Experiencia Laboral" action="go_experienciaCargoForm" />
				            <x:navigationMenuItem id="nav_1_9_2_3" itemLabel="Adiestramiento" action="go_adiestramientoCargoForm" />
				            <x:navigationMenuItem id="nav_1_9_2_4" itemLabel="Habilidades/Competencias" action="go_habilidadCargoForm" />
				         </x:navigationMenuItem>
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_1_12" itemLabel="Registro de Oferentes/Elegible">
				        <x:navigationMenuItem id="nav_1_12_1" itemLabel="Datos Personales" action="go_elegibleForm" />
			            <x:navigationMenuItem id="nav_1_12_2" itemLabel="Educaci�n Formal" action="go_elegibleEducacionForm"  />
			            <x:navigationMenuItem id="nav_1_12_3" itemLabel="Profesiones y/o Oficios" action="go_elegibleProfesionForm" />
			            <x:navigationMenuItem id="nav_1_12_4" itemLabel="Estudios Informales" action="go_elegibleEstudioForm" />
			            <x:navigationMenuItem id="nav_1_12_5" itemLabel="Certificaciones" action="go_elegibleCertificacionForm" />
			            <x:navigationMenuItem id="nav_1_12_6" itemLabel="Experiencia Laboral" action="go_elegibleExperienciaForm" />
			            <x:navigationMenuItem id="nav_1_12_7" itemLabel="Idiomas" action="go_elegibleIdiomaForm" />
			            <x:navigationMenuItem id="nav_1_12_8" itemLabel="Grupo Familiar" action="go_elegibleFamiliarForm" />
			            <x:navigationMenuItem id="nav_1_12_9" itemLabel="Afiliaciones a Gremios" action="go_elegibleAfiliacionForm" />
			            <x:navigationMenuItem id="nav_1_12_10" itemLabel="Actividades Docentes" action="go_elegibleActividadDocenteForm" />
			            <x:navigationMenuItem id="nav_1_12_11" itemLabel="Otras Actividades" action="go_elegibleOtraActividadForm" />
			            <x:navigationMenuItem id="nav_1_12_12" itemLabel="Habilidades/Competencias" action="go_elegibleHabilidadForm" />
			            <x:navigationMenuItem id="nav_1_12_13" itemLabel="Publicaciones" action="go_elegiblePublicacionForm" />            
			         </x:navigationMenuItem>
			         <x:navigationMenuItem id="nav_1_10" itemLabel="Reclutamiento y Selecci�n">
 			        	 <x:navigationMenuItem id="nav_1_10_1" itemLabel="Concursos">
			            	<x:navigationMenuItem id="nav_1_10_1_1" itemLabel="Concursos P�blicos" action="go_concursoForm" />			            	
			            	<x:navigationMenuItem id="nav_1_10_1_2" itemLabel="Cargos a asociar a Concursos" action="go_concursoCargoForm" />
			         	</x:navigationMenuItem> 	
			         	<x:navigationMenuItem id="nav_1_10_2" itemLabel="Procesos de Reclutamiento">
			           		<x:navigationMenuItem id="nav_1_10_2_1" itemLabel="Postulados a Cargos por Concursos" action="go_postuladoConcursoForm" />
			            <%--<x:navigationMenuItem id="nav_1_10_2_2" itemLabel="Registro Externo de Postulados" action="go_postuladoExternoForm" /> --%>
			            	<x:navigationMenuItem id="nav_1_10_2_3" itemLabel="Preselecci�n de Postulados" action="go_preseleccionarForm" />
			        	</x:navigationMenuItem>
			        	<x:navigationMenuItem id="nav_1_10_3" itemLabel="Procesos de Selecci�n" >
							<x:navigationMenuItem id="nav_1_10_3_1" itemLabel="Registrar Resultados Pruebas y Entrevistas" action="go_pruebaPreseleccionadoForm" />
							<x:navigationMenuItem id="nav_1_10_3_2" itemLabel="Registrar Resultados Baremos" action="go_baremoPreseleccionadoForm" />	
							<x:navigationMenuItem id="nav_1_10_3_3" itemLabel="Actualizar Resultado del Proceso de Selecci�n" action="go_seleccionarForm" />
						<%--	<x:navigationMenuItem id="nav_1_10_3_4" itemLabel="Registrar Elegible" action="go_seleccionarElegibleForm" />	--%>		            	
			        	</x:navigationMenuItem>
			        	<%--<x:navigationMenuItem id="nav_1_10_4" itemLabel="Reporte de Elegibles" action="go_reportElegibleForm" /> --%>
			        	<x:navigationMenuItem id="nav_1_10_5" itemLabel="Tipos de Pruebas y/o Entrevistas" action="go_pruebaSeleccionForm" />
			        	<x:navigationMenuItem id="nav_1_10_6" itemLabel="Baremos" action="go_varemosForm" />	
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_1_5" itemLabel="Capacitaci�n">
			            <x:navigationMenuItem id="nav_1_5_1" itemLabel="Tablas B�sicas">
			                <x:navigationMenuItem id="nav_1_5_1_1" itemLabel="Areas de Conocimiento" action="go_areaConocimientoForm" />
			                <x:navigationMenuItem id="nav_1_5_1_2" itemLabel="Tipos de Cursos" action="go_tipoCursoForm" />			               
			            </x:navigationMenuItem>
			            <x:navigationMenuItem id="nav_1_5_2" itemLabel="Planificaci�n">
			                <x:navigationMenuItem id="nav_1_5_2_1" itemLabel="Planes Capacitaci�n por Unidad Funcional" action="go_planAdiestramientoForm" />
			                <x:navigationMenuItem id="nav_1_5_2_2" itemLabel="Incluir Participantes" action="go_actualizarParticipanteForm" />
			                <x:navigationMenuItem id="nav_1_5_2_3" itemLabel="Registrar Resultados de un Plan" action="go_actualizarEstudioForm" />
			                <x:navigationMenuItem id="nav_1_5_2_4" itemLabel="Reportes" >
				                <x:navigationMenuItem id="nav_1_5_2_4_1" itemLabel="Reporte de Planes de Capacitaci�n" action="go_reportPlanesAdiestramientoForm" />			                
			                </x:navigationMenuItem>
			            </x:navigationMenuItem>
			        </x:navigationMenuItem>
			         <x:navigationMenuItem id="nav_1_6" itemLabel="Evaluaciones de Desempe�o">			                
			                <x:navigationMenuItem id="nav_1_6_1" itemLabel="Consulta de Evaluaciones" action="go_evaluacionForm" />
			                <x:navigationMenuItem id="nav_1_6_2" itemLabel="Registrar Evaluaciones" action="go_registrarEvaluacionForm" />
			                <x:navigationMenuItem id="nav_1_6_3" itemLabel="Actualizar/Eliminar Evaluaciones" action="go_actualizarEvaluacionForm" />
			                <x:navigationMenuItem id="nav_1_6_4" itemLabel="Apelaciones" action="go_apelacionForm" />
			                <x:navigationMenuItem id="nav_1_6_5" itemLabel="Trabajadores No Evaluados" action="go_noEvaluacionForm" />
			                <x:navigationMenuItem id="nav_1_6_6" itemLabel="Tipos de Acciones Resultantes" action="go_accionEvaluacionForm" />
			                <x:navigationMenuItem id="nav_1_6_7" itemLabel="Tipos de Resultados de Evaluaci�n" action="go_resultadoEvaluacionForm" />  			                
			                <x:navigationMenuItem id="nav_1_6_8" itemLabel="Reportes" >        
			                	<x:navigationMenuItem id="nav_1_6_8_1" itemLabel="Reportes Detalle Evaluaciones" action="go_reportEvaluacionesForm" />        
			                  	<x:navigationMenuItem id="nav_1_6_8_2" itemLabel="Reportes Resumen Evaluaciones" action="go_reportResumenEvaluacionesForm" />     
			                  	<x:navigationMenuItem id="nav_1_6_8_3" itemLabel="Reportes Notificaci�n de Resultados" action="go_reportNotificacionEvaluacionesForm" />  
			                  	<x:navigationMenuItem id="nav_1_6_8_4" itemLabel="Reportes Trabajadores No Evaluados" action="go_reportNoEvaluacionesForm" />  
			              	</x:navigationMenuItem>
			            </x:navigationMenuItem>
			         <x:navigationMenuItem id="nav_1_17" itemLabel="Planes de Personal">
			            <x:navigationMenuItem id="nav_1_7_1" itemLabel="Definici�n de Planes" action="go_planPersonalForm" />	
			            <x:navigationMenuItem id="nav_1_7_2" itemLabel="Componentes" >  
			               <x:navigationMenuItem id="nav_1_7_2_1" itemLabel="Movimientos de Personal" action="go_movimientosPlanForm" />
			               <x:navigationMenuItem id="nav_1_7_2_2" itemLabel="Movimientos de Cargos" action="go_cargosPlanForm" />
			               <x:navigationMenuItem id="nav_1_7_2_3" itemLabel="Acciones Administrativas" action="go_registroCargosAprobadoForm" />
				           <x:navigationMenuItem id="nav_1_7_2_4" itemLabel="Contratos de Personal" action="go_contratosPlanForm" />
				       </x:navigationMenuItem>
				        <x:navigationMenuItem id="nav_1_7_3" itemLabel="Reportes" >
				        	<x:navigationMenuItem id="nav_1_7_3_1" itemLabel="Reportes de Planes de Personal" action="go_reportPlanPersonalForm" />	
				        	<x:navigationMenuItem id="nav_1_7_3_2" itemLabel="Reportes de Indicadores Planes de Personal" action="go_reportPlanIndicadoresForm" />	
				        </x:navigationMenuItem> 				        		           
			        </x:navigationMenuItem>       
			   </x:navigationMenuItem>
			    <x:navigationMenuItem id="nav_2" itemLabel="Expediente |">
			    	<x:navigationMenuItem id="nav_2_1" itemLabel="Expediente">
			            <x:navigationMenuItem id="nav_2_1_1" itemLabel="Datos Personales" action="go_personalForm" />
			            <x:navigationMenuItem id="nav_2_1_2" itemLabel="Educaci�n Formal" action="go_educacionForm"  />
			            <x:navigationMenuItem id="nav_2_1_3" itemLabel="Profesiones y/o Oficios" action="go_profesionTrabajadorForm" />
			            <x:navigationMenuItem id="nav_2_1_4" itemLabel="Estudios Informales" action="go_estudioForm" />
			            <x:navigationMenuItem id="nav_2_1_5" itemLabel="Certificaciones" action="go_certificacionForm" />
			            <x:navigationMenuItem id="nav_2_1_6" itemLabel="Experiencia Laboral (S. Privado)" action="go_experienciaForm" />
			            <x:navigationMenuItem id="nav_2_1_7" itemLabel="Idiomas" action="go_idiomaForm" />
			            <x:navigationMenuItem id="nav_2_1_8" itemLabel="Grupo Familiar" action="go_familiarForm" />
			            <x:navigationMenuItem id="nav_2_1_9" itemLabel="Afiliaciones a Gremios" action="go_afiliacionForm" />
			            <x:navigationMenuItem id="nav_2_1_10" itemLabel="Actividades Docentes" action="go_actividadDocenteForm" />
			            <x:navigationMenuItem id="nav_2_1_11" itemLabel="Otras Actividades" action="go_otraActividadForm" />
			            <x:navigationMenuItem id="nav_2_1_12" itemLabel="Habilidades/Competencias" action="go_habilidadForm" />
			            <x:navigationMenuItem id="nav_2_1_13" itemLabel="Reconocimientos" action="go_reconocimientoForm" />
			            <x:navigationMenuItem id="nav_2_1_14" itemLabel="Sanciones" action="go_sancionForm" />
			            <x:navigationMenuItem id="nav_2_1_15" itemLabel="Averiguaciones Administrativas" action="go_averiguacionForm" />
			            <x:navigationMenuItem id="nav_2_1_16" itemLabel="Certificados de Carrera" action="go_certificadoForm" />
			            <x:navigationMenuItem id="nav_2_1_17" itemLabel="Publicaciones" action="go_publicacionForm" />            
			            <x:navigationMenuItem id="nav_2_1_18" itemLabel="Pasant�as" action="go_pasantiaForm" />
			            <x:navigationMenuItem id="nav_2_1_19" itemLabel="Declaraciones Juradas" action="go_declaracionForm" />            
			         </x:navigationMenuItem>         
			        <x:navigationMenuItem id="nav_2_2" itemLabel="Historial">
			            <x:navigationMenuItem id="nav_2_2_1" itemLabel="Trayectoria y Antecedentes" >				            
			            	<x:navigationMenuItem id="nav_2_2_1_1" itemLabel="Trayectoria en APN" action="go_trayectoriaForm" />
				            <x:navigationMenuItem id="nav_2_2_1_2" itemLabel="Actualizar Trayectoria en Organismo" action="go_actualizarTrayectoriaForm" />
				            <x:navigationMenuItem id="nav_2_2_1_3" itemLabel="Antecedentes de Servicio sujeto a LEFP" action="go_antecedenteForm" />
			            	<x:navigationMenuItem id="nav_2_2_1_4" itemLabel="Antecedentes de Servicio no sujeto a LEFP" action="go_experienciaNoEstForm" />
				            <x:navigationMenuItem id="nav_2_2_1_5" itemLabel="Reportes" >
				        	   <x:navigationMenuItem id="nav_2_2_1_5_1" itemLabel="Relaci�n de Cargos Trabajador" action="go_reportRelacionCargosForm" />	
				        	   <x:navigationMenuItem id="nav_2_2_1_5_2" itemLabel="Antecedentes de Servicios del Trabajador" action="go_reportAntecedentesForm" />
				            </x:navigationMenuItem> 
				        </x:navigationMenuItem>            
			            <x:navigationMenuItem id="nav_2_2_2" itemLabel="Encargadur�as" action="go_encargaduriaForm" />
			            <x:navigationMenuItem id="nav_2_2_3" itemLabel="Suplencias" action="go_suplenciaForm" />            
			            <x:navigationMenuItem id="nav_2_2_4" itemLabel="Contratos" action="go_contratoForm" />			            			            
			            <x:navigationMenuItem id="nav_2_2_5" itemLabel="Comisi�n de Servicio(Personal Interno)" action="go_comisionServicioForm" />
			            <x:navigationMenuItem id="nav_2_2_6" itemLabel="Servicio Exterior" action="go_servicioExteriorForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_2_3" itemLabel="Tablas B�sicas Expediente">
			            <x:navigationMenuItem id="nav_2_3_2" itemLabel="Grupos de Profesi�n" action="go_grupoProfesionForm" />
			            <x:navigationMenuItem id="nav_2_3_15" itemLabel="Subgrupos de Profesi�n" action="go_subgrupoProfesionForm" />
			            <x:navigationMenuItem id="nav_2_3_16" itemLabel="Profesiones/Oficios" action="go_profesionForm" />
			            <x:navigationMenuItem id="nav_2_3_1" itemLabel="Areas de Carrera" action="go_areaCarreraForm" />
			            <x:navigationMenuItem id="nav_2_3_4" itemLabel="Carreras" action="go_carreraForm" />
			            <x:navigationMenuItem id="nav_2_3_5" itemLabel="Carreras por Area" action="go_carreraAreaForm" />
			            <x:navigationMenuItem id="nav_2_3_6" itemLabel="T�tulos de Educaci�n" action="go_tituloForm" />
			            <x:navigationMenuItem id="nav_2_3_7" itemLabel="Idiomas" action="go_tipoIdiomaForm" />
			            <x:navigationMenuItem id="nav_2_3_8" itemLabel="Tipos de Sanciones" action="go_tipoAmonestacionForm" />
			            <x:navigationMenuItem id="nav_2_3_9" itemLabel="Tipos de Reconocimientos" action="go_tipoReconocimientoForm" />
			            <x:navigationMenuItem id="nav_2_3_10" itemLabel="Tipos de Habilidades/Competencias" action="go_tipoHabilidadForm" />
			            <x:navigationMenuItem id="nav_2_3_11" itemLabel="Tipos de Otras Actividades" action="go_tipoOtraActividadForm" />
			            <x:navigationMenuItem id="nav_2_3_12" itemLabel="Instituciones P�blicas" action="go_institucionForm" />
				   </x:navigationMenuItem>
				   <x:navigationMenuItem id="nav_2_4" itemLabel="Reportes" >
			            	<x:navigationMenuItem id="nav_2_4_1" itemLabel="Hoja de Vida" action="go_reportCurriculumForm" />
			            	<x:navigationMenuItem id="nav_2_4_2" itemLabel="Hoja de Vida por Regi�n" action="go_reportCurriculumRegionForm" />
			            	<x:navigationMenuItem id="nav_2_4_3" itemLabel="Criterios Varios" action="go_reportTrabajadorForm" />
			        </x:navigationMenuItem>
				  </x:navigationMenuItem>
			   <x:navigationMenuItem id="nav_3" itemLabel="Administraci�n |">
				   <x:navigationMenuItem id="nav_3_1" itemLabel="Parametrizaci�n">
					   <x:navigationMenuItem id="nav_3_1_1" itemLabel="Definiciones">
			                <x:navigationMenuItem id="nav_3_1_1_1" itemLabel="Categor�a de Personal" action="go_categoriaPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_2" itemLabel="Relaci�n Laboral" action="go_relacionPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_3" itemLabel="Clasificaci�n de Personal" action="go_clasificacionPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_4" itemLabel="Grupos de N�mina" action="go_grupoNominaForm" />
			                <x:navigationMenuItem id="nav_3_1_1_5" itemLabel="Tipos de Personal" action="go_tipoPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_6" itemLabel="Restricciones de Personal" action="go_restringidoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_7" itemLabel="Clasificadores Asociados a Tipo de Personal" action="go_manualPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_8" itemLabel="Frecuencias de Pago" action="go_frecuenciaPagoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_9" itemLabel="Conceptos" action="go_conceptoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_10" itemLabel="Frecuencias por Tipo de Personal" action="go_frecuenciaTipoPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_11" itemLabel="Conceptos por Tipo de Personal" action="go_conceptoTipoPersonalForm" />
			         <%--   <x:navigationMenuItem id="nav_3_1_1_12" itemLabel="Conceptos por Cargo y A�o" action="go_conceptoCargoAnioForm" /> --%>
			                <x:navigationMenuItem id="nav_3_1_1_13" itemLabel="Conceptos por Cargo" action="go_conceptoCargoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_14" itemLabel="Conceptos por Tipo Dependencia" action="go_conceptoDependenciaForm" />
			                <x:navigationMenuItem id="nav_3_1_1_15" itemLabel="Turnos" action="go_turnoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_16" itemLabel="Tipos de Contrato" action="go_tipoContratoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_17" itemLabel="Archivos (Disquetes)" action="go_disqueteForm" />
			                <x:navigationMenuItem id="nav_3_1_1_18" itemLabel="Detalle Archivos (Disquetes)" action="go_detalleDisqueteForm" />
						</x:navigationMenuItem>        
			            <x:navigationMenuItem id="nav_3_1_2" itemLabel="Par�metros y F�rmulas">
			    	        <x:navigationMenuItem id="nav_3_1_2_1" itemLabel="Conceptos Asociados" action="go_conceptoAsociadoForm" />
			                <x:navigationMenuItem id="nav_3_1_2_2" itemLabel="Prima Antiguedad" action="go_primaAntiguedadForm" />
			                <x:navigationMenuItem id="nav_3_1_2_3" itemLabel="Prima Por Hijo" action="go_primaHijoForm" />
			                <x:navigationMenuItem id="nav_3_1_2_5" itemLabel="Par�metros Retenciones" action="go_parametroGobiernoForm" />
			                <x:navigationMenuItem id="nav_3_1_2_6" itemLabel="F�rmula Bono Vacacional" action="go_conceptoVacacionesForm" />
			                <x:navigationMenuItem id="nav_3_1_2_7" itemLabel="D�as Vacaciones por A�o" action="go_vacacionesPorAnioForm" />   			                     
			                <x:navigationMenuItem id="nav_3_1_2_8" itemLabel="F�rmula Bono Fin de A�o" action="go_conceptoUtilidadesForm" />        			                
			                <x:navigationMenuItem id="nav_3_1_2_9" itemLabel="D�as Bono Fin de A�o" action="go_utilidadesPorAnioForm" />
			                <x:navigationMenuItem id="nav_3_1_2_10" itemLabel="Par�metros Varios" action="go_parametroVariosForm" />
			                <x:navigationMenuItem id="nav_3_1_2_11" itemLabel="Par�metros Planilla ARI" action="go_parametroAriForm" />
			         <%--   <x:navigationMenuItem id="nav_3_1_2_12" itemLabel="Par�metros Jubilaci�n" action="go_parametroJubilacionForm" /> --%>
			             </x:navigationMenuItem>            
						<x:navigationMenuItem id="nav_3_1_3" itemLabel="Tablas B�sicas N�mina">
			                <x:navigationMenuItem id="nav_3_1_3_1" itemLabel="Bancos" action="go_bancoForm" />
			                <x:navigationMenuItem id="nav_3_1_3_2" itemLabel="Cuentas de Banco" action="go_cuentaBancoForm" />
			                <x:navigationMenuItem id="nav_3_1_3_3" itemLabel="Per�odos Semanales" action="go_semanaForm" />
			                <x:navigationMenuItem id="nav_3_1_3_4" itemLabel="Meses" action="go_mesForm" />
			                <x:navigationMenuItem id="nav_3_1_3_5" itemLabel="Sindicatos" action="go_sindicatoForm" />
			                <x:navigationMenuItem id="nav_3_1_3_6" itemLabel="Contratos Colectivos" action="go_contratoColectivoForm" />
			                <x:navigationMenuItem id="nav_3_1_3_7" itemLabel="Tarifas ARI" action="go_tarifaAriForm" />
			                <x:navigationMenuItem id="nav_3_1_3_8" itemLabel="Firmas para Reportes" action="go_firmasReportesForm" />
			                <x:navigationMenuItem id="nav_3_1_3_9" itemLabel="Tipos de Ausencias" action="go_tipoAusenciaForm" />
					   </x:navigationMenuItem>
						<x:navigationMenuItem id="nav_3_1_4" itemLabel="Reportes">
			                <x:navigationMenuItem id="nav_3_1_4_1" itemLabel="Definiciones de Conceptos" action="go_reportDefinicionesForm" />

					   </x:navigationMenuItem>
			        </x:navigationMenuItem>    
			           
			           <x:navigationMenuItem id="nav_3_2" itemLabel="Datos del Trabajador">
			            <x:navigationMenuItem id="nav_3_2_1" itemLabel="Trabajador">
				            <x:navigationMenuItem id="nav_3_2_1_1" itemLabel="Ubicar Trabajador" action="go_ubicarTrabajadorForm"/>
				            <x:navigationMenuItem id="nav_3_2_1_3" itemLabel="Consultar Datos N�mina" action="go_trabajadorForm"/>
			            	<x:navigationMenuItem id="nav_3_2_1_5" itemLabel="Actualizar Datos del Trabajador" action="go_actualizarDatosTrabajadorForm" />			            	
			            	<x:navigationMenuItem id="nav_3_2_1_7" itemLabel="Actualizar Fechas del Trabajador" action="go_actualizarFechasTrabajadorForm" />			            	
			            	<x:navigationMenuItem id="nav_3_2_1_8" itemLabel="Correcci�n/Cambio de C�dula" action="go_cambioCedulaForm" />
			        	</x:navigationMenuItem>        
			            <x:navigationMenuItem id="nav_3_2_2" itemLabel="Conceptos Fijos" action="go_conceptoFijoForm" />
			            <x:navigationMenuItem id="nav_3_2_3" itemLabel="Conceptos Variables" action="go_conceptoVariableForm" />
			            <x:navigationMenuItem id="nav_3_2_4" itemLabel="Pr�stamos" action="go_prestamoForm" />
			            <x:navigationMenuItem id="nav_3_2_5" itemLabel="Sueldos Promedios" action="go_sueldoPromedioForm" />			          		            
			            <x:navigationMenuItem id="nav_3_2_6" itemLabel="Anticipos" action="go_anticipoForm" />
			            <x:navigationMenuItem id="nav_3_2_9" itemLabel="Ausencias, Permisos y/o Reposos" action="go_ausenciaForm" />
			            <x:navigationMenuItem id="nav_3_2_0" itemLabel="Ausencias (Unidad Funcional)" action="go_ausenciaUnidadFuncionalForm" />
			            <x:navigationMenuItem id="nav_3_2_10" itemLabel="Planilla ARI" action="go_planillaAriForm" />
			            <x:navigationMenuItem id="nav_3_2_11" itemLabel="Planilla ARC" action="go_planillaArcForm" />	 
			            <x:navigationMenuItem id="nav_3_2_12" itemLabel="Embargos" >
			            	<x:navigationMenuItem id="nav_3_2_12_1" itemLabel="Sentencias de Embargo al Trabajador" action="go_embargoForm" />
			            	<x:navigationMenuItem id="nav_3_2_12_2" itemLabel="Detalle Embargo" action="go_embargoConceptoForm" />
			            </x:navigationMenuItem>  
			            <x:navigationMenuItem id="nav_3_2_13" itemLabel="Caja de Ahorros" action="go_cajaAhorroForm" />	 
			            <x:navigationMenuItem id="nav_3_2_14" itemLabel="Reportes" >
			            	<x:navigationMenuItem id="nav_3_2_14_1" itemLabel="Trabajadores Activos con Concepto" action="go_reportConceptosForm" />
			            	<x:navigationMenuItem id="nav_3_2_14_2" itemLabel="Trabajadores Activos sin Concepto" action="go_reportSinConceptosForm" />
			            	<x:navigationMenuItem id="nav_3_2_14_3" itemLabel="Pr�stamos Vigentes" action="go_reportPrestamosForm" />
			            	<x:navigationMenuItem id="nav_3_2_14_4" itemLabel="Reporte de Reposos, ausencias y/o Permisos" action="go_reportRepososAusenciasPermisosForm" />
			            </x:navigationMenuItem>        
			        </x:navigationMenuItem>   
			               
			        <x:navigationMenuItem id="nav_3_3" itemLabel="Movimientos de Personal">
			            	<x:navigationMenuItem id="nav_3_3_1" itemLabel="Tipos de Movimientos y Causas"  >    
			            		<x:navigationMenuItem id="nav_3_3_1_1" itemLabel="Tipos de Movimiento" action="go_movimientoPersonalForm" />                  
			            		<x:navigationMenuItem id="nav_3_3_1_2" itemLabel="Causas del Movimiento" action="go_causaMovimientoForm" />                  
			            		<x:navigationMenuItem id="nav_3_3_1_3" itemLabel="Movimientos por Tipo de Personal" action="go_causaPersonalForm" />                  
			        		</x:navigationMenuItem> 
			        		<x:navigationMenuItem id="nav_3_3_2" itemLabel="Personal Bajo LEFP/LEJP"  >    
			            		<x:navigationMenuItem id="nav_3_3_2_1" itemLabel="Ingresos" >                  
			       					<x:navigationMenuItem id="nav_3_3_2_1_1" itemLabel="Ingreso Personal a un Cargo de Carrera" action="go_ingresoCargoCarreraLefpForm" />                  
			      					<x:navigationMenuItem id="nav_3_3_2_1_2" itemLabel="Ingreso Personal de Libre Nombramiento y Remoci�n" action="go_ingresoTrabajadorLefpForm" />                  
			      					<x:navigationMenuItem id="nav_3_3_2_1_3" itemLabel="Reubicaci�n Personal LEFP" action="go_reubicacionLefpForm" />                                              		  		                                             		  		
			      					<x:navigationMenuItem id="nav_3_3_2_1_4" itemLabel="Traslado Art. 35" action="go_trasladoArt35LefpForm" />       
			      					<x:navigationMenuItem id="nav_3_3_2_1_5" itemLabel="Traslado por Transferencia de Competencia" action="go_trasladoTransferenciaCompetenciaLefpForm" />                                              		  		
								</x:navigationMenuItem> 	
			            		<x:navigationMenuItem id="nav_3_3_2_2" itemLabel="Reingresos" >                  
			            			<x:navigationMenuItem id="nav_3_3_2_2_1" itemLabel="Reingreso Personal a un Cargo de Carrera" action="go_reingresoCargoCarreraLefpForm" />
			            			<x:navigationMenuItem id="nav_3_3_2_2_2" itemLabel="Reingreso Personal de Libre Nombramiento y Remoci�n" action="go_reingresoTrabajadorLefpForm" />
			            		</x:navigationMenuItem> 				            		
			            		<x:navigationMenuItem id="nav_3_3_2_3" itemLabel="Actualizaciones">                  
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_1" itemLabel="Cambio en el Cargo sujeto a LEFP" action="go_cambioCargoLefpForm" />                  
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_2" itemLabel="Designaci�n sujeto a LEFP" action="go_designacionLefpForm" />                  
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_3" itemLabel="Ascenso sujeto a LEFP" action="go_ascensoLefpForm" />                  
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_4" itemLabel="Clasificaci�n sujeto a LEFP" action="go_clasificacionLefpForm" />                  
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_5" itemLabel="Cambio Clasificaci�n" action="go_cambioClasificacionLefpForm" />                  
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_6" itemLabel="Aumento de Sueldo sujeto a LEFP" action="go_aumentoSueldoLefpForm" />
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_7" itemLabel="Licencia sin Sueldo sujeto a LEFP" action="go_licenciaSinSueldoLefpForm" />                                    
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_8" itemLabel="Licencia con Sueldo sujeto a LEFP" action="go_licenciaConSueldoLefpForm" />                                    
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_9" itemLabel="Suspensi�n sin Goce de Sueldo" action="go_suspensionLefpForm" /> 			          		  		
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_11" itemLabel="Reincorporaci�n personal sujeto a LEFP" action="go_reincorporacionLefpForm" /> 
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_12" itemLabel="Reincorporaci�n por Sentencia" action="go_reincorporacionSentenciaNewLefpForm" /> 
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_13" itemLabel="Traslado sujeto a LEFP" action="go_trasladoLefpForm" /> 
			          		  		<x:navigationMenuItem id="nav_3_3_2_3_14" itemLabel="Anulaci�n de Movimiento" action="go_anulacionRemesaMovimientoSitpForm" />                                             		  		                                                                                		  					          		  		
			          		  	</x:navigationMenuItem> 
			            		<x:navigationMenuItem id="nav_3_3_2_4" itemLabel="Jubilaciones y Pensiones"  >  
			            		    <x:navigationMenuItem id="nav_3_3_2_4_1" itemLabel="Jubilaci�n Derecho sujeto a LEJP" action="go_jubilacionLefpForm" />                                              		  		                                             		  		
			            			<x:navigationMenuItem id="nav_3_3_2_4_2" itemLabel="Jubilaci�n Especial sujeto a LEJP" action="go_jubilacionEspecialLefpForm" />                                              		  		                                             		  		
			            			<x:navigationMenuItem id="nav_3_3_2_4_3" itemLabel="Ajuste de jubilaci�n sujeto a LEJP" action="go_ajusteJubilacionLefpForm" />                                              		  		                                             		  		
									<x:navigationMenuItem id="nav_3_3_2_4_4" itemLabel="Ajuste de pensi�n sujeto a LEJP" action="go_ajustePensionLefpForm" />                                              		  		                                             		  		
									<x:navigationMenuItem id="nav_3_3_2_4_5" itemLabel="Suspensi�n de jubilaci�n sujeto a LEJP" action="go_suspensionJubilacionLefpForm" />                                              		  		                                             		  					            			
									<x:navigationMenuItem id="nav_3_3_2_4_6" itemLabel="Reactivaci�n de jubilaci�n sujeto a LEJP" action="go_reactivacionJubilacionLefpForm" />                                              		  		                                             		  		
									<x:navigationMenuItem id="nav_3_3_2_4_7" itemLabel="Cese de Jubilaci�n sujeto a LEJP" action="go_ceseJubilacionLefpForm" />                                              		  		                                             		  		
			            			<x:navigationMenuItem id="nav_3_3_2_4_8" itemLabel="Cese de Pensi�n sujeto a LEJP" action="go_cesePensionLefpForm" />                                              		  		                                             		  		
			            			<x:navigationMenuItem id="nav_3_3_2_4_9" itemLabel="Ingreso a N�mina Jubilado/Pensionado" action="go_ingresoTrabajadorJubiladoForm" />                                              		  		                                             		  		
			            			<x:navigationMenuItem id="nav_3_3_2_4_10" itemLabel="Egreso de N�mina Jubilado/Pensionado" action="go_egresoTrabajadorJubiladoForm" />                                              		  		                                             		  		
			            		</x:navigationMenuItem>
			            		<x:navigationMenuItem id="nav_3_3_2_5" itemLabel="Egreso de Trabajador sujeto a LEFP" action="go_egresoTrabajadorLefpForm" />                  
			        		</x:navigationMenuItem> 
			        		
			                <x:navigationMenuItem id="nav_3_3_3" itemLabel="Personal no Sujeto a LEFP con Registro"  >  
			            		<%--   Cambio de menu para la dem incluir movimientos en no lefpt --%>
			            		<x:navigationMenuItem id="nav_3_3_3_1" itemLabel="Ingresos" >                  
			       					<x:navigationMenuItem id="nav_3_3_3_1_1" itemLabel="Ingreso no Sujeto a LEFP con Registro" action="go_ingresoTrabajadorRegistroForm" /> 
			      			        <x:navigationMenuItem id="nav_3_3_3_1_2" itemLabel="Reubicaci�n no Sujeto a Personal LEFP con Registro" action="go_reubicacionNoLefpDemForm" />                                              		  		                                             		  		
			      					<x:navigationMenuItem id="nav_3_3_3_1_3" itemLabel="Traslado Art. 35 no Sujeto LEFP con Registro" action="go_trasladoArt35NoLefpDemForm" />       
			      					<x:navigationMenuItem id="nav_3_3_3_1_4" itemLabel="Traslado por Transferencia de Competencia no Sujeto LEFTP con Registro" action="go_trasladoTransferenciaCompetenciaNoLefpDemForm" />
			      							  		
								</x:navigationMenuItem> 	
			            		<%--   Fin  del menu  --%>								
			            		
			                <x:navigationMenuItem id="nav_3_3_3_2" itemLabel="Reingreso no Sujeto a LEFP con Registro" action="go_reingresoTrabajadorRegistroForm" />                  
					                <x:navigationMenuItem id="nav_3_3_3_3" itemLabel="Actualizaciones" >                  
						                <x:navigationMenuItem id="nav_3_3_3_3_1" itemLabel="Cambio en el Cargo No sujeto a LEFP" action="go_cambioCargoNoLefpForm" />
						                <x:navigationMenuItem id="nav_3_3_3_3_2" itemLabel="Clasificaci�n No Sujeto a LEFP" action="go_clasificacionNoLefpForm" />                  
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_3" itemLabel="Ascenso No Sujeto a LEFP" action="go_ascensoConRegistroForm" />                  
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_4" itemLabel="Aumento de Sueldo No Sujeto a LEFP" action="go_aumentoSueldoConRegistroForm" />
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_5" itemLabel="Licencia sin Sueldo No Sujeto a LEFP" action="go_licenciaSinSueldoConRegistroForm" />                                    
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_6" itemLabel="Licencia con Sueldo No Sujeto a LEFP" action="go_licenciaConSueldoConRegistroForm" />                                    
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_7" itemLabel="Suspensi�n No Sujeto a LEFP" action="go_suspensionConRegistroForm" /> 
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_8" itemLabel="Reincorporaci�n No Sujeto a LEFP" action="go_reincorporacionConRegistroForm" /> 
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_9" itemLabel="Traslado No Sujeto a LEFP" action="go_trasladoNoLefpForm" /> 
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_14" itemLabel="Traslado Mutuo No Sujeto a LEFP" action="go_trasladoMutuoNoLefpForm"/>                                             		  		                                                                                		  		
			            		<%--   Cambio de menu para la dem incluir movimientos en no lefpt --%>
  				          		  		<x:navigationMenuItem id="nav_3_3_3_3_10" itemLabel="Designaci�n no sujeto a LEFP con registro" action="go_designacionNoLefpDemForm" />                  				          		  		
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_11" itemLabel="Suspensi�n sin Goce de Sueldo no sujeto a LEFP con registro" action="go_suspensionNoLefpDemForm" /> 	
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_12" itemLabel="Reincorporaci�n por Sentencia no sujeto a LEFP con registro" action="go_reincorporacionSentenciaNewNoLefpDemForm" /> 
				          		  		<x:navigationMenuItem id="nav_3_3_3_3_13" itemLabel="Anulaci�n de Movimiento " action="go_anulacionRemesaMovimientoSitpForm" />                                             		  		                                                                                		  					          		  						          		  		
			            		<%--   Cambio de menu para la dem incluir movimientos en no lefpt Fin  del menu --%>				          		  		
					            	</x:navigationMenuItem> 
					                <x:navigationMenuItem id="nav_3_3_3_4" itemLabel="Egreso de Trabajador con Registro" action="go_egresoTrabajadorRegistroForm" />
					                <x:navigationMenuItem id="nav_3_3_3_5" itemLabel="Cambio Tipo Personal con Registro" action="go_movimientoTrabajadorRegistroForm" />
			      					                                                                					               
								</x:navigationMenuItem> 
				   				<x:navigationMenuItem id="nav_3_3_30" itemLabel="Personal no Sujeto a LEFP Sin Registro"  >    
				   					<x:navigationMenuItem id="nav_3_3_30_1" itemLabel="Ingreso de Personal Sin Registro" action="go_ingresoTrabajadorSinRegistroForm" />                  
				   					<x:navigationMenuItem id="nav_3_3_30_4" itemLabel="Reingreso de Personal Sin Registro" action="go_reingresoTrabajadorSinRegistroForm" />                  
				   					 <x:navigationMenuItem id="nav_3_3_30_2" itemLabel="Actualizaciones" >                  				          		  		                
				          		  		<x:navigationMenuItem id="nav_3_3_30_2_1" itemLabel="Aumento de Sueldo Personal sin Registro" action="go_aumentoSueldoSinRegistroForm" />
				          		  		<x:navigationMenuItem id="nav_3_3_30_2_2" itemLabel="Cambio Denominaci�n de Cargo sin Registro" action="go_cambioCargoSinRegistroForm" />
				          		  		<x:navigationMenuItem id="nav_3_3_30_2_3" itemLabel="Licencia sin Sueldo Personal sin Registro" action="go_licenciaSinSueldoSinRegistroForm" />                                    
				          		  		<x:navigationMenuItem id="nav_3_3_30_2_4" itemLabel="Licencia con Sueldo Personal sin Registro" action="go_licenciaConSueldoSinRegistroForm" />                                    
				          		  		<x:navigationMenuItem id="nav_3_3_30_2_5" itemLabel="Suspensi�n Personal sin Registro" action="go_suspensionSinRegistroForm" /> 
				          		  		<x:navigationMenuItem id="nav_3_3_30_2_6" itemLabel="Reincorporaci�n Personal sin Registro" action="go_reincorporacionSinRegistroForm" /> 
				          		  		<x:navigationMenuItem id="nav_3_3_30_2_7" itemLabel="Traslado Personal Sin Registro" action="go_trasladoSinRegistroForm" /> 
				          		  		                                             		  		                                                                                		  		
				          		  	                             		  		                                             		  		
					            	</x:navigationMenuItem> 
					            	<x:navigationMenuItem id="nav_3_3_30_3" itemLabel="Egreso de Personal sin Registro" action="go_egresoTrabajadorSinRegistroForm" />
					            	<x:navigationMenuItem id="nav_3_3_30_5" itemLabel="Cambio Tipo Personal sin Registro" action="go_movimientoTrabajadorSinRegistroForm" />
				                </x:navigationMenuItem> 
				                              
			               	<x:navigationMenuItem id="nav_3_3_4" itemLabel="Manejo de Remesas"  >    
								<x:navigationMenuItem id="nav_3_3_4_1" itemLabel="Remesas" action="go_remesaForm" />                  
								<x:navigationMenuItem id="nav_3_3_4_2" itemLabel="Asociar Movimientos" action="go_remesaMovimientoSitpForm" />                  
								<x:navigationMenuItem id="nav_3_3_4_3" itemLabel="Cerrar Remesa" action="go_cerrarRemesaForm" />      
								<x:navigationMenuItem id="nav_3_3_4_4" itemLabel="Enviar Remesa" action="go_enviarRemesaForm" />
								<x:navigationMenuItem id="nav_3_3_4_8" itemLabel="Aprobar Movimientos" action="go_aprobarRemesaMovimientoSitpForm" />  
								<x:navigationMenuItem id="nav_3_3_4_5" itemLabel="Recibir Remesas" action="go_recibirRemesaForm" />                              
								<x:navigationMenuItem id="nav_3_3_4_6" itemLabel="Reportes" >    
									<x:navigationMenuItem id="nav_3_3_4_6_1" itemLabel="Movimientos con Remesa"  action="go_reportRemesasForm" />     					
									<x:navigationMenuItem id="nav_3_3_4_6_2" itemLabel="Movimientos sin Remesa"  action="go_reportMovimientosForm" />     					
			            		</x:navigationMenuItem> 								
			            	</x:navigationMenuItem> 			            	
							<x:navigationMenuItem id="nav_3_3_5" itemLabel="Personal Inhabilitado" action="go_inhabilitadoForm" />                  
							
			        </x:navigationMenuItem>  
			              
			         
			        <x:navigationMenuItem id="nav_3_5" itemLabel="Actualizaci�n de Conceptos">
			            <x:navigationMenuItem id="nav_3_5_1" itemLabel="Carga Externa de Conceptos (Archivo)" action="go_cargaConceptosForm" />    
			            <x:navigationMenuItem id="nav_3_5_2" itemLabel="Carga Externa de Prestamos (Archivo)" action="go_cargaPrestamosForm" /> 
			            <x:navigationMenuItem id="nav_3_5_3" itemLabel="Carga Externa de Caja de Ahorros (Archivo)" action="go_cargaCajaAhorrosForm" />   
			            <x:navigationMenuItem id="nav_3_5_4" itemLabel="Actualizaci�n Masiva de Conceptos" action="go_cargaMasivaConceptosForm" />      
			            <x:navigationMenuItem id="nav_3_5_5" itemLabel="Carga por Criterios" action="go_recalculoPorCriterioForm" /> 
			            <x:navigationMenuItem id="nav_3_5_6" itemLabel="Cambio de Frecuencia" action="go_cambioFrecuenciaForm" />    
			            <x:navigationMenuItem id="nav_3_5_7" itemLabel="Cambio de Estatus" action="go_cambioEstatusForm" />   
			            <%/* <x:navigationMenuItem id="nav_3_5_6" itemLabel="Actualizar Documento Soporte" action="go_cambioDocumentoSoporteForm" -->    */%>
			            <x:navigationMenuItem id="nav_3_5_8" itemLabel="Actualizar Monto y Unidades" action="go_actualizarMontoForm" />    
	<%-- Mod --%>       <x:navigationMenuItem id="nav_3_5_9" itemLabel="Eliminar Conceptos" action="go_eliminarConceptosForm" />
						<x:navigationMenuItem id="nav_3_5_16" itemLabel="Eliminar Prestamos" action="go_eliminarPrestamosForm" />
			            <x:navigationMenuItem id="nav_3_5_10" itemLabel="Rec�lculo de un concepto" action="go_recalculoForm" />    
			            <x:navigationMenuItem id="nav_3_5_11" itemLabel="Rec�lculo de conceptos por variaci�n" action="go_recalculoPorAsociadoForm" />                             
			            <x:navigationMenuItem id="nav_3_5_12" itemLabel="Carga Relacionada a un Concepto" action="go_ingresarConceptosForm" />                             
			            <x:navigationMenuItem id="nav_3_5_13" itemLabel="Carga Masiva de Pr�stamos" action="go_cargaMasivaPrestamosForm" />
			  <%--      <x:navigationMenuItem id="nav_3_5_14" itemLabel="Carga Sobretiempo" action="go_cargaSobretiempoForm" /> --%>
			            <x:navigationMenuItem id="nav_3_5_15" itemLabel="Sumar un Concepto a otro Concepto" action="go_sumarConceptoForm" />
			        </x:navigationMenuItem>
			        
			        <x:navigationMenuItem id="nav_3_20" itemLabel="Procesos de Vacaciones" >
			               <x:navigationMenuItem id="nav_3_20_1" itemLabel="Generar Vacaciones Pendientes(Derecho)" action="go_generarDerechoVacacionForm" />
			               <x:navigationMenuItem id="nav_3_20_2" itemLabel="Vacaciones Pendientes" action="go_vacacionForm" />
			               <x:navigationMenuItem id="nav_3_20_3" itemLabel="Vacaciones Disfrutadas" action="go_vacacionDisfrutadaForm" />
			               <x:navigationMenuItem id="nav_3_20_4" itemLabel="Vacaciones Programadas" action="go_vacacionProgramadaForm" />
			               <x:navigationMenuItem id="nav_3_20_5" itemLabel="Constancia de Disfrute" action="go_reportConstanciaDisfruteForm" />
			               <x:navigationMenuItem id="nav_3_20_6" itemLabel="Reportes de Vacaciones" action="go_reportVacacionForm" /> 
			               <x:navigationMenuItem id="nav_3_20_7" itemLabel="Vacaciones Pendientes Generadas" action="go_seguridadVacacionForm" /> 	 
			               <x:navigationMenuItem id="nav_3_20_8" itemLabel="Liquidaci�n de Vacaciones">	
	    		              <x:navigationMenuItem id="nav_3_20_8_1" itemLabel="Liquidaci�n de Vacaci�n Trabajador" action="go_liquidacionVacacionForm"  />       
			                  <x:navigationMenuItem id="nav_3_20_8_2" itemLabel="Formato de Liquidaci�n de Vacaciones" action="go_reportLiquidacionVacacionForm" />              
			               </x:navigationMenuItem> 
			        </x:navigationMenuItem>  	          
			        <x:navigationMenuItem id="nav_3_6" itemLabel="Procesos Aniversario">
			            <x:navigationMenuItem id="nav_3_6_1" itemLabel="Calculo Prima Antiguedad" action="go_calcularPrimaAntiguedadForm" />       
			            <x:navigationMenuItem id="nav_3_6_2" itemLabel="Calculo Bono Vacacional" action="go_calcularBonoVacacionalForm" />              
			            <x:navigationMenuItem id="nav_3_6_3" itemLabel="Seguridad Procesos Aniversario" action="go_seguridadAniversarioForm" />       
			        </x:navigationMenuItem>        
			        <x:navigationMenuItem id="nav_3_7" itemLabel="Procesos de N�mina">        	
			            <x:navigationMenuItem id="nav_3_7_4" itemLabel="Pren�mina Ordinaria" >
			                <x:navigationMenuItem id="nav_3_7_4_1" itemLabel="Proceso" action="go_generarPrenominaForm" />
							<x:navigationMenuItem id="nav_3_7_4_2" itemLabel="Variaciones" action="go_reportVariacionesPrenominaForm" />
			                <x:navigationMenuItem id="nav_3_7_4_3" itemLabel="Reportes" action="go_reportPrenominaForm" />
			            </x:navigationMenuItem>
			            <x:navigationMenuItem id="nav_3_7_3" itemLabel="Definici�n de N�mina Especial" action="go_nominaEspecialForm" /> 
			            <x:navigationMenuItem id="nav_3_7_5" itemLabel="Pren�mina Especial" >
			                <x:navigationMenuItem id="nav_3_7_5_1" itemLabel="Proceso" action="go_generarPrenominaEspecialForm" />
			                <x:navigationMenuItem id="nav_3_7_5_2" itemLabel="Reportes" action="go_reportPrenominaEspecialForm" />
			            </x:navigationMenuItem>
			            <x:navigationMenuItem id="nav_3_7_6_0" itemLabel="------------------------------------------" />
			            <x:navigationMenuItem id="nav_3_7_6" itemLabel="N�mina Ordinaria" >
			                <x:navigationMenuItem id="nav_3_7_6_1" itemLabel="Proceso" action="go_generarNominaForm" />
			                <x:navigationMenuItem id="nav_3_7_6_2" itemLabel="Reportes" action="go_reportNominaForm" />
			            </x:navigationMenuItem>
			            <x:navigationMenuItem id="nav_3_7_7" itemLabel="N�mina Especial" >
			                <x:navigationMenuItem id="nav_3_7_7_1" itemLabel="Proceso" action="go_generarNominaEspecialForm" />
			                <x:navigationMenuItem id="nav_3_7_7_2" itemLabel="Reportes" action="go_reportNominaEspecialForm" />
			            </x:navigationMenuItem> 
			            <x:navigationMenuItem id="nav_3_7_8" itemLabel="Generar Archivos/Disquete de N�mina" action="go_disqueteNominaForm" />  
			            <x:navigationMenuItem id="nav_3_7_9" itemLabel="Reversar N�mina" action="go_reversarNominaForm" />  			            
			            <x:navigationMenuItem id="nav_3_7_10" itemLabel="------------------------------------------" />
			            <x:navigationMenuItem id="nav_3_7_11" itemLabel="Calcular Sueldos Promedios" action="go_generarSueldoPromedioForm" />
			            <x:navigationMenuItem id="nav_3_7_12" itemLabel="N�minas Ordinarias Procesadas" action="go_seguridadOrdinariaForm" />
			            <x:navigationMenuItem id="nav_3_7_13" itemLabel="N�minas Especiales Procesadas" action="go_seguridadEspecialForm" />     
			        </x:navigationMenuItem>
			         <x:navigationMenuItem id="nav_3_13" itemLabel="Retenciones y Aportes">			         	                
			         	<x:navigationMenuItem id="nav_3_13_1" itemLabel="Reportes de Retenciones y Aportes Patronales"  action="go_reportAportesPatronalesForm" /> 
			         	<x:navigationMenuItem id="nav_3_13_2" itemLabel="Trabajadores que no Cotizan" action="go_reportCotizacionTrabajadorForm" />   
			         	<x:navigationMenuItem id="nav_3_13_3" itemLabel="Reportes Cajas de Ahorro"  action="go_reportCajaAhorrosForm" /> 
			         	<x:navigationMenuItem id="nav_3_13_4" itemLabel="Generar Archivos/Disquete Retenciones y Aportes(Asociado a Banco)"  action="go_disqueteAporteForm" />    
			            <x:navigationMenuItem id="nav_3_13_5" itemLabel="Generar Archivos/Disquete Retenciones y Aportes(No Asociado a Banco)"  action="go_disqueteAporteOtrosForm" /> 
			            <x:navigationMenuItem id="nav_3_13_6" itemLabel="Recalcular Aportes Patronales"  action="go_actualizacionAportePatronalForm" />
			            <x:navigationMenuItem id="nav_3_13_7" itemLabel="Reporte de Retenciones ISLR"  action="go_reportRetencionISLRForm" />
			            <x:navigationMenuItem id="nav_3_13_8" itemLabel="Reporte Banavih"  action="go_reportRetencionBanavihForm" />
			        </x:navigationMenuItem> 
			         <x:navigationMenuItem id="nav_3_17" itemLabel="Procesos de Aumento y Proyecciones">			         	                
			         	<x:navigationMenuItem id="nav_3_17_1" itemLabel="Aumento por Porcentaje"  action="go_procesoAumentoPorPorcentajeForm" />
			         	<x:navigationMenuItem id="nav_3_17_4" itemLabel="Conceptos para Proceso de Aumento"  action="go_conceptoEvaluacionForm" /> 				         	
			         	<x:navigationMenuItem id="nav_3_17_2" itemLabel="Aumento por Evaluaci�n"  action="go_procesoAumentoPorEvaluacionForm" /> 
			         	<x:navigationMenuItem id="nav_3_17_3" itemLabel="Aplicar Nuevo Tabulador"  action="go_procesoAumentoPorTabuladorForm" /> 	
			         	<x:navigationMenuItem id="nav_3_17_5" itemLabel="Calcular Retroactivos"  action="go_calcularRetroactivoForm" /> 		
			         	<x:navigationMenuItem id="nav_3_17_7" itemLabel="Recalculo de Concepto para Proyecci�n"  action="go_recalculoProyectadoForm" /> 		
			         	<x:navigationMenuItem id="nav_3_17_6" itemLabel="Reportes"   > 				         			         	
			         		<x:navigationMenuItem id="nav_3_17_6_1" itemLabel="Proyecci�n por Conceptos"  action="go_reportProyeccionConceptoForm" /> 				         			         	
			         		<%/*<--x:navigationMenuItem id="nav_3_17_6_2" itemLabel="Resumen de Proyecci�n"  action="go_reportProyeccionResumenForm" /> 	*/%>			         			         	
			         	</x:navigationMenuItem>
			        </x:navigationMenuItem> 
			         <x:navigationMenuItem id="nav_3_4" itemLabel="Otros Procesos">			         	                			         	
			         	<x:navigationMenuItem id="nav_3_4_1" itemLabel="C�lculo Bono Fin A�o"  action="go_calcularBonoFinAnioForm" />                     
			         	<x:navigationMenuItem id="nav_3_4_2" itemLabel="Generar Planilla ARC" action="go_generarPlanillaArcForm" />
			            <x:navigationMenuItem id="nav_3_4_3" itemLabel="Reporte Planilla ARC" action="go_reportPlanillaArcForm" />
			            <x:navigationMenuItem id="nav_3_4_4" itemLabel="Pagos Fuera del Sistema" action="go_pagoFueraNominaForm" />			          
			            <x:navigationMenuItem id="nav_3_4_5" itemLabel="Remuneraci�n Especial Anual" action="go_calcularBonoPetroleroForm" />
			        </x:navigationMenuItem> 
			        
			        <x:navigationMenuItem id="nav_3_9" itemLabel="Registro Cargos/Puestos">
			            <x:navigationMenuItem id="nav_3_9_1" itemLabel="Definici�n de Registros" action="go_registroForm" />
			            <x:navigationMenuItem id="nav_3_9_2" itemLabel="Registros por Tipo Personal" action="go_registroPersonalForm" />
			            <x:navigationMenuItem id="nav_3_9_3" itemLabel="Consultar/Eliminar Posici�n Registro" action="go_registroCargosForm" />
			        	<x:navigationMenuItem id="nav_3_9_4" itemLabel="Trayectoria Posici�n Registro" action="go_historicoCargosForm" />
			            <x:navigationMenuItem id="nav_3_9_5" itemLabel="Agregar Cargos/Puestos" action="go_agregarRegistroCargosForm" />
			            <x:navigationMenuItem id="nav_3_9_6" itemLabel="Reportes">
			            	<x:navigationMenuItem id="nav_3_9_6_1" itemLabel="Registros Cargos/Puestos" action="go_reportRegistroCargosForm" />
			            	<x:navigationMenuItem id="nav_3_9_6_2" itemLabel="Registros Cantidad Cargos" action="go_reportCantidadCargosForm" />
			            	<x:navigationMenuItem id="nav_3_9_6_3" itemLabel="Registros Cargos/Puestos por UEL" action="go_reportRegistroCargosUELForm" />
			            </x:navigationMenuItem>
			       <%--     <x:navigationMenuItem id="nav_3_9_7" itemLabel="Nueva Estructura">
			            	<x:navigationMenuItem id="nav_3_9_7_1" itemLabel="Reemplazo de Dependencia" action="go_reemplazarDependenciaForm" />
			            	<x:navigationMenuItem id="nav_3_9_7_2" itemLabel="Carga Externa de Nueva Estructura" action="go_cargaNuevoRegistroCargosForm" />
			            	<x:navigationMenuItem id="nav_3_9_7_3" itemLabel="Aplicar Nueva Estructura" action="go_actualizarNuevoRegistroCargosForm" />
			                <x:navigationMenuItem id="nav_3_9_7_4" itemLabel="Reportes">
			            	   <x:navigationMenuItem id="nav_3_9_7_4_1" itemLabel="Registros Cargos/Puestos Nuevo" action="go_reportRegistroCargosNuevoForm" />
			            	   <x:navigationMenuItem id="nav_3_9_7_4_2" itemLabel="Registros Cargos/Puestos Hist�rico" action="go_reportRegistroCargosHistoricoForm" />
			                </x:navigationMenuItem>
			            </x:navigationMenuItem> --%>
			        </x:navigationMenuItem>
			        
			        <x:navigationMenuItem id="nav_3_10" itemLabel="Prestaciones Mensuales de Antiguedad">
				        <x:navigationMenuItem id="nav_3_10_0" itemLabel="Procesos Generados Prestaciones" action="go_seguridadPrestacionesForm" />
			            <x:navigationMenuItem id="nav_3_10_1" itemLabel="Conceptos para Prestaciones" action="go_conceptoPrestacionesForm" />
			            <x:navigationMenuItem id="nav_3_10_2" itemLabel="Prestaciones por Trabajador" action="go_prestacionesMensualesForm" />
			            <x:navigationMenuItem id="nav_3_10_9" itemLabel="Prestaciones Formato Onapre por Trabajador" action="go_prestacionesOnapreForm" />
			            <x:navigationMenuItem id="nav_3_10_3" itemLabel="Proceso Mensual Prestaciones" action="go_calcularPrestacionesMensualesForm" />     
			            <x:navigationMenuItem id="nav_3_10_4" itemLabel="Pago D�as Adicionales" action="go_generarPagoDiasAdicionalesForm" />     
			            <x:navigationMenuItem id="nav_3_10_5" itemLabel="Registro Fideicomiso" action="go_actualizarFideicomisoForm" />
			            <x:navigationMenuItem id="nav_3_10_6" itemLabel="Fideicomiso por Trabajador" action="go_fideicomisoForm" />
			            <x:navigationMenuItem id="nav_3_10_8" itemLabel="Generar Archivos/Disquete Fideicomiso" action="go_disqueteFideicomisoForm" />
			            <x:navigationMenuItem id="nav_3_10_7" itemLabel="Reportes">
			            	<x:navigationMenuItem id="nav_3_10_7_1" itemLabel="Prestaciones Mensuales" action="go_reportPrestacionesMensualesForm" />            	
			            	<x:navigationMenuItem id="nav_3_10_7_2" itemLabel="Dias Adicionales" action="go_reportDiasAdicionalesForm" />      
			            	<x:navigationMenuItem id="nav_3_10_7_3" itemLabel="Abono 5 D�as" action="go_reportAbono5DiasForm" />                	
			            	<x:navigationMenuItem id="nav_3_10_7_4" itemLabel="Estados de Cuenta General" action="go_reportPrestacionesTrabajadorGeneralForm" />            	
			            	<x:navigationMenuItem id="nav_3_10_7_5" itemLabel="Estado de Cuenta Individual" action="go_reportPrestacionesTrabajadorForm" />        
			            	<x:navigationMenuItem id="nav_3_10_7_6" itemLabel="Por Categoria Presupuesto" action="go_reportPrestacionesPresupuestoForm" />            	
			            </x:navigationMenuItem>
			         </x:navigationMenuItem>
			         <x:navigationMenuItem id="nav_3_19" itemLabel="Pasivos Laborales">
			         	<x:navigationMenuItem id="nav_3_19_0" itemLabel="Calcular el Tiempo de Servicio en APN" action="go_calcularTiempoApnForm" />                       
			         	<x:navigationMenuItem id="nav_3_19_4" itemLabel="Historico Devengado Integral" action="go_historicoDevengadoIntegralForm" />
			         	<x:navigationMenuItem id="nav_3_19_7" itemLabel="Registrar Anticipos" action="go_registrarAnticiposForm" />
			            <x:navigationMenuItem id="nav_3_19_1" itemLabel="Regimen Derogado" >     			
	    		        	<x:navigationMenuItem id="nav_3_19_1_1" itemLabel="Calcular Deuda e Intereses al 19-06-97" action="go_calcularInteresViejoRegimenForm" />
	    		        	<x:navigationMenuItem id="nav_3_19_1_2" itemLabel="Calcular Intereses Adicionales" action="go_calcularInteresAdicionalForm" />
	    		        	<x:navigationMenuItem id="nav_3_19_1_3" itemLabel="Actualizaci�n Mensual Intereses Adicionales" action="go_calcularInteresAdicionalMensualForm" />	    		        	
	    		        </x:navigationMenuItem>	    		        
	    		        <x:navigationMenuItem id="nav_3_19_8" itemLabel="Nuevo Regimen" >     			
	    		        	<x:navigationMenuItem id="nav_3_19_8_1" itemLabel="Calcular Hist�rico Prestaciones a partir de 19-06-97" action="go_calcularPrestacionesNuevoRegimenForm" />
	    		        	<x:navigationMenuItem id="nav_3_19_8_2" itemLabel="Calcular Intereses Nuevo Regimen a partir de 19-06-97" action="go_calcularInteresesNuevoRegimenForm" />
	    		        </x:navigationMenuItem>	
	    		        <x:navigationMenuItem id="nav_3_19_9" itemLabel="Liquidaci�n" >     			
	    		            <x:navigationMenuItem id="nav_3_19_9_1" itemLabel="Liquidaci�n de Prestaciones Sociales" action="go_liquidacionTrabajadorForm" />	
	    		            <x:navigationMenuItem id="nav_3_19_9_2" itemLabel="Otros Pagos Liquidaci�n" action="go_conceptoLiquidacionForm" />	
	    		             <x:navigationMenuItem id="nav_3_19_9_3" itemLabel="Formatos de Liquidaci�n de Prestaciones Sociales" action="go_reportLiquidacionTrabajadorForm" />	    
	    		        </x:navigationMenuItem>	
	    		        <x:navigationMenuItem id="nav_3_19_10" itemLabel="Reportes" >	    		        	
		    		        <x:navigationMenuItem id="nav_3_19_10_1" itemLabel="Regimen Derogado" >	    		        	
		    		   	        <x:navigationMenuItem id="nav_3_19_10_1_1" itemLabel="Deuda e Intereses por Tipo Personal" action="go_reportRegimenDerogadoForm" />	    		        	
		    		            <x:navigationMenuItem id="nav_3_19_10_1_2" itemLabel="Deuda/Intereses/Devengados por Trabajador" action="go_reportRegimenDerogadoTrabajadorForm" />	    		        	
		    		            <x:navigationMenuItem id="nav_3_19_10_1_3"  itemLabel="Intereses Adicionales Mensuales" action="go_reportInteresAdicionalMensualForm" />	    		        	
	    		            </x:navigationMenuItem>	  
		    		        <x:navigationMenuItem id="nav_3_19_10_2" itemLabel="Nuevo Regimen" >	    		        	
		    		   	    	<x:navigationMenuItem id="nav_3_19_10_2_1" itemLabel="Deuda e Intereses por  Tipo Personal" action="go_reportNuevoRegimenForm" />	    		        	
		    		       	    <x:navigationMenuItem id="nav_3_19_10_2_2" itemLabel="Deuda/Intereses/Devengados por Trabajador" action="go_reportNuevoRegimenTrabajadorForm" />	    		        	
	    		        	</x:navigationMenuItem>	    		        
	    		           		        	
		    		   	</x:navigationMenuItem>	    		        
	    		        <x:navigationMenuItem id="nav_3_19_5" itemLabel="Tasas BCV" action="go_tasaBcvForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_3_12" itemLabel="Jubilaciones y Pensiones">
				        <x:navigationMenuItem id="nav_3_12_0" itemLabel="Calcular el Tiempo de Servico en APN" action="go_calcularTiempoApnForm" />          
			            <x:navigationMenuItem id="nav_3_12_1" itemLabel="Par�metros Jubilaci�n" action="go_parametroJubilacionForm" />
			            <x:navigationMenuItem id="nav_3_12_2" itemLabel="Par�metros Pensi�n Invalidez" action="go_pensionInvalidezForm" />
			            <x:navigationMenuItem id="nav_3_12_4" itemLabel="Suspender a Jubilados por F� de Vida"  action="go_suspenderTrabajadoresForm" /> 
			         	<x:navigationMenuItem id="nav_3_12_5" itemLabel="Desactivar F� de Vida a Jubilados"  action="go_desactivarFeVidaForm" /> 			
			         	<x:navigationMenuItem id="nav_3_12_6" itemLabel="Actualizar Estatus y F� de Vida" action="go_actualizarEstatusFeVidaTrabajadorForm" />	
			         	<x:navigationMenuItem id="nav_3_12_7" itemLabel="Datos Personal Jubilado" action="go_jubiladoForm" />
			         	<x:navigationMenuItem id="nav_3_12_8" itemLabel="Datos Pensionados Sobrevivientes" action="go_sobrevivienteForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_3_11" itemLabel="Formulaci�n y Ejecuci�n Presupuestaria">
			          	<x:navigationMenuItem id="nav_3_11_1" itemLabel="Estructura Presupuestaria">
			          		<x:navigationMenuItem id="nav_3_11_1_1" itemLabel="Fuentes de Financiamiento" action="go_fuenteFinanciamientoForm" />			          	
			          		<x:navigationMenuItem id="nav_3_11_1_2" itemLabel="Cuentas Presupuestarias" action="go_cuentaPresupuestoForm" />			          	
				            <x:navigationMenuItem id="nav_3_11_1_3" itemLabel="Fuentes de Financiamiento por Partida" action="go_partidaUelEspecificaForm" />
			          		<x:navigationMenuItem id="nav_3_11_1_5" itemLabel="Cuentas Contables" action="go_cuentaContableForm" />			          	
				            <x:navigationMenuItem id="nav_3_11_1_6" itemLabel="Proyectos" action="go_proyectoForm" />
				            <x:navigationMenuItem id="nav_3_11_1_7" itemLabel="Acciones Centralizadas" action="go_accionCentralizadaForm" />
				            <x:navigationMenuItem id="nav_3_11_1_8" itemLabel="Acciones Especificas" action="go_accionEspecificaForm" />	
				            <x:navigationMenuItem id="nav_3_11_1_9" itemLabel="Acciones Especificas por Unidades Ejecutoras" action="go_uelEspecificaForm" />			       				           				           
				            <x:navigationMenuItem id="nav_3_11_1_10" itemLabel="Asignar Fuentes de Financiamiento" action="go_generarPartidaUelEspecificaForm" />			          	
			          	 	<x:navigationMenuItem id="nav_3_11_1_12" itemLabel="Relaci�n Concepto - Cuenta Presupuestaria" action="go_conceptoCuentaForm" />
				            <x:navigationMenuItem id="nav_3_11_1_13" itemLabel="Relaci�n Concepto - Cuenta Contable" action="go_conceptoCuentaContableForm" />
				            <x:navigationMenuItem id="nav_3_11_1_14" itemLabel="Relaci�n Concepto - Proyecto/Acci�n" action="go_conceptoEspecificaForm" />				            				          
			        	</x:navigationMenuItem>			        	
			        	<x:navigationMenuItem id="nav_3_11_2" itemLabel="Asignaci�n a Proyectos/Acciones">				            
				            <x:navigationMenuItem id="nav_3_11_2_1" itemLabel="Asignaci�n Masiva Trabajadores/Cargos a Proyecto/Acci�n" action="go_generarTrabajadorCargoEspecificaForm" />
				            <x:navigationMenuItem id="nav_3_11_2_2" itemLabel="Asignar Trabajador a Proyecto/Acci�n" action="go_trabajadorEspecificaForm" />
				            <x:navigationMenuItem id="nav_3_11_2_3" itemLabel="Asignar Cargo a Proyecto/Acci�n" action="go_cargoEspecificaForm" />				          			           
				         </x:navigationMenuItem>  
				         <x:navigationMenuItem id="nav_3_11_3" itemLabel="Ejecuci�n Presupuestaria" >
					         <x:navigationMenuItem id="nav_3_11_3_1" itemLabel="Procesos"  >
					            	<x:navigationMenuItem id="nav_3_11_3_1_1" itemLabel="Generar Base C�lculo y Resumen Mensual" action="go_generarBaseResumenForm" />
					            	<x:navigationMenuItem id="nav_3_11_3_1_2" itemLabel="Generar Resumen Mensual" action="go_generarResumenMensualForm" />				   		
					   				<x:navigationMenuItem id="nav_3_11_3_1_4" itemLabel="Generar Resumen Aportes" action="go_generarResumenAportesForm" />
					   				<x:navigationMenuItem id="nav_3_11_3_1_5" itemLabel="Generar Resumen Complementario" action="go_generarResumenAdicionalForm" />
					   				<x:navigationMenuItem id="nav_3_11_3_1_6" itemLabel="Generar Rendici�n" action="go_generarRendicionMensualForm" />
					   				<x:navigationMenuItem id="nav_3_11_3_1_7" itemLabel="Generar Rendici�n Aportes" action="go_generarRendicionAportesForm" />
					         </x:navigationMenuItem> 				         				         
					         <x:navigationMenuItem id="nav_3_11_3_2" itemLabel="Actualizaciones"  >
						         <x:navigationMenuItem id="nav_3_11_3_2_1" itemLabel="Conceptos Base C�lculo" action="go_conceptoResumenForm" />
						 		 <x:navigationMenuItem id="nav_3_11_3_2_2" itemLabel="Resumen Mensual" action="go_resumenMensualForm" />
					         </x:navigationMenuItem>				          
					         <x:navigationMenuItem id="nav_3_11_3_3" itemLabel="Reportes de Estructura Presupuestaria"  >
					            	<x:navigationMenuItem id="nav_3_11_3_3_1" itemLabel="Cat�logo de Cuentas" action="go_reportCatalogoCuentasForm" />
					            	<x:navigationMenuItem id="nav_3_11_3_3_2" itemLabel="Relaci�n Concepto - Cuentas" action="go_reportConceptoCuentasForm" />
					            	<x:navigationMenuItem id="nav_3_11_3_3_3" itemLabel="Categorias Presupuestarias" action="go_reportCategoriaPresupuestariaForm" />		
					            	<x:navigationMenuItem id="nav_3_11_3_3_4" itemLabel="Asignaci�n de Proyectos/Acciones" action="go_reportProyectoAccionForm" />					            	   	            	
					         </x:navigationMenuItem>   	   
					         <x:navigationMenuItem id="nav_3_11_3_4" itemLabel="Reportes de Ejecuci�n"  >
					            	<x:navigationMenuItem id="nav_3_11_3_4_1" itemLabel="Resumen Mensual" action="go_reportResumenMensualForm" />	     
					            	<x:navigationMenuItem id="nav_3_11_3_4_2" itemLabel="Resumen Aportes" action="go_reportResumenAportesForm" />	            	            	
					            	<x:navigationMenuItem id="nav_3_11_3_4_3" itemLabel="Rendici�n Mensual" action="go_reportRendicionMensualForm" />	
					            	<x:navigationMenuItem id="nav_3_11_3_4_4" itemLabel="Rendici�n Aportes" action="go_reportRendicionAportesForm" />	  
					            	<x:navigationMenuItem id="nav_3_11_3_4_5" itemLabel="Interface SAP" action="go_reportInterfaceSapForm" />	            	            	
					         </x:navigationMenuItem>   
					         <x:navigationMenuItem id="nav_3_11_3_5" itemLabel="Reportes Rechazos"  >
					            	<x:navigationMenuItem id="nav_3_11_3_5_1" itemLabel="Trabajadores - Conceptos no Asignados" action="go_reportTrabajadoresNoAsignadosForm" />	     
					         </x:navigationMenuItem>   
						</x:navigationMenuItem>  
						 <x:navigationMenuItem id="nav_3_11_4" itemLabel="Formulaci�n Presupuestaria" >
					         <x:navigationMenuItem id="nav_3_11_4_1" itemLabel="Procesos"  >
					            	<x:navigationMenuItem id="nav_3_11_4_1_1" itemLabel="Generar Cuadro ONAPRE" action="go_generarCuadroOnapreForm" />
					            	<x:navigationMenuItem id="nav_3_11_4_1_2" itemLabel="Escala Cuadro Onapre" action="go_escalaCuadroOnapreForm" />
					         </x:navigationMenuItem> 
					         <x:navigationMenuItem id="nav_3_11_4_2" itemLabel="Reportes"  >
					            	<x:navigationMenuItem id="nav_3_11_4_2_1" itemLabel="Cuadros por Tipo de Cargo" action="go_reportCuadroOnapreForm" />
					         </x:navigationMenuItem> 					         				         
					        
						</x:navigationMenuItem>     	   	 	   
			        </x:navigationMenuItem>        	   
				   
				  <x:navigationMenuItem id="nav_3_16" itemLabel="Consultas/Reportes Historicos">
			            <x:navigationMenuItem id="nav_3_16_1" itemLabel="Hist�rico Conceptos por Trabajador" action="go_historicoConceptoTrabajadorForm" />			            
			            <x:navigationMenuItem id="nav_3_16_2" itemLabel="Hist�rico Concepto" action="go_reportHistoricoConceptoForm" />		
			            <x:navigationMenuItem id="nav_3_16_0" itemLabel="Hist�rico por Grupo Nomina" action="go_reportHistoricoGrupoNominaForm" />		
			            <x:navigationMenuItem id="nav_3_16_3" itemLabel="Hist�rico por Unidad Administradora" action="go_reportHistoricoAdministradoraForm" />		                	   			            	                	   
			            <x:navigationMenuItem id="nav_3_16_4" itemLabel="Hist�rico por Devengados/Incidencias" action="go_reportHistoricoDevengadoForm" />	
			            <x:navigationMenuItem id="nav_3_16_5" itemLabel="Reportes de Hist�rico de Nominas" action="go_reportNominaHistoricoForm" />			                	   
			      </x:navigationMenuItem>
			       <x:navigationMenuItem id="nav_3_18" itemLabel="Constancias de Trabajo">
			            <x:navigationMenuItem id="nav_3_18_1" itemLabel="Definici�n de Constancias" action="go_constanciaForm" />			            
			            <x:navigationMenuItem id="nav_3_18_2" itemLabel="Conceptos de Constancias de Trabajo" action="go_conceptoConstanciaForm" />		
			            <x:navigationMenuItem id="nav_3_18_3" itemLabel="Emisi�n de Constancias" action="go_reportConstanciasForm" />		                	   			            	                	   			            	                	   
			      </x:navigationMenuItem>
			      
			      
			   
			   </x:navigationMenuItem>
			   
			   <x:navigationMenuItem id="nav_4" itemLabel="Bienestar |">	
			  		<x:navigationMenuItem id="nav_4_9" itemLabel="Documentaci�n de Identificaci�n">
			            <x:navigationMenuItem id="nav_4_9_1" itemLabel="Tipo Documentaci�n" action="go_tipoCredencialForm" />
			            <x:navigationMenuItem id="nav_4_9_2" itemLabel="Clase Documentaci�n" action="go_subtipoCredencialForm" />
			            <x:navigationMenuItem id="nav_4_9_3" itemLabel="Manejo de Documentaci�n" action="go_credencialForm" />	
			            <x:navigationMenuItem id="nav_4_9_4" itemLabel="Reportes">	
			                <x:navigationMenuItem id="nav_4_9_4_2" itemLabel="Documentaci�n Entregada" action="go_reportCredencialesForm" />
				            <x:navigationMenuItem id="nav_4_9_4_3" itemLabel="Documentaci�n Retirada" action="go_reportCredencialesRetiradasForm" />
			            </x:navigationMenuItem>			           
			        </x:navigationMenuItem>	
			        <x:navigationMenuItem id="nav_4_1" itemLabel="Juguetes">
			            <x:navigationMenuItem id="nav_4_1_1" itemLabel="Par�metros" action="go_parametroJugueteForm" />
			            <x:navigationMenuItem id="nav_4_1_2" itemLabel="Asignar Juguetes" action="go_asignarJuguetesForm" />	
			            <x:navigationMenuItem id="nav_4_1_3" itemLabel="Reportes" action="go_reportJuguetesForm" />				           
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_2" itemLabel="Utiles Escolares">
			            <x:navigationMenuItem id="nav_4_2_1" itemLabel="Par�metros" action="go_parametroUtilesForm" />
			            <x:navigationMenuItem id="nav_4_2_2" itemLabel="Asignar Utiles" action="go_asignarUtilesForm" />
			            <x:navigationMenuItem id="nav_4_2_3" itemLabel="Reportes" action="go_reportUtilesForm" />			            
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_3" itemLabel="Becas a Familiares">
			            <x:navigationMenuItem id="nav_4_3_1" itemLabel="Niveles Educativos" action="go_nivelBecaForm" />
			            <x:navigationMenuItem id="nav_4_3_2" itemLabel="Par�metros Generales" action="go_parametroBecaGeneralForm" />
			            <x:navigationMenuItem id="nav_4_3_3" itemLabel="Par�metros por Nivel" action="go_parametroBecaForm" />
			            <x:navigationMenuItem id="nav_4_3_4" itemLabel="Solicitudes de Becas" action="go_solicitudBecaFamiliarForm" />
			            <x:navigationMenuItem id="nav_4_3_5" itemLabel="Procesos">
				            <x:navigationMenuItem id="nav_4_3_5_1" itemLabel="Aprobar Solicitudes" action="go_aprobarBecaFamiliarForm" />
				            <x:navigationMenuItem id="nav_4_3_5_2" itemLabel="Generar Pagos" action="go_pagarBecaFamiliarForm" />
				            <x:navigationMenuItem id="nav_4_3_5_3" itemLabel="Nuevo A�o Escolar" action="go_inicializarSolicitudesForm" />
			            </x:navigationMenuItem>
			            <x:navigationMenuItem id="nav_4_3_6" itemLabel="Reportes">
			                <x:navigationMenuItem id="nav_4_3_6_1" itemLabel="Reporte Solicitudes de Becas" action="go_reportSolicitudesForm" />
				            <x:navigationMenuItem id="nav_4_3_6_2" itemLabel="Reporte Becas Aprobadas" action="go_reportBecaFamiliarAprobadaForm" />
				            <x:navigationMenuItem id="nav_4_3_6_3" itemLabel="Reporte Becas Rechazadas" action="go_reportBecaFamiliarRechazadaForm" />
				            <x:navigationMenuItem id="nav_4_3_6_4" itemLabel="Reporte Becas por Nivel Educativo" action="go_reportBecaFamiliarNivelForm" />
			            </x:navigationMenuItem>
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_4" itemLabel="Centro Educaci�n Inicial">
			            <x:navigationMenuItem id="nav_4_4_1" itemLabel="Par�metros" action="go_parametroGuarderiaForm" />
			            <x:navigationMenuItem id="nav_4_4_2" itemLabel="Centro Educaci�n Inicial" action="go_guarderiaForm" />
			            <x:navigationMenuItem id="nav_4_4_3" itemLabel="Centro Educaci�n Inicial por Trabajador" action="go_guarderiaFamiliarForm" />
			            <x:navigationMenuItem id="nav_4_4_4" itemLabel="Proceso Generar Pagos" action="go_pagarGuarderiaForm" />
			            <x:navigationMenuItem id="nav_4_4_5" itemLabel="Reporte Centro Educaci�n Inicial" action="go_reportGuarderiaForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_5" itemLabel="Dotaciones">
			            <x:navigationMenuItem id="nav_4_5_1" itemLabel="Tipos de Dotaci�n" action="go_tipoDotacionForm" />
			            <x:navigationMenuItem id="nav_4_5_2" itemLabel="Sub-tipos de Dotaci�n" action="go_subtipoDotacionForm" />
			            <x:navigationMenuItem id="nav_4_5_3" itemLabel="Dotaci�n por Cargo" action="go_dotacionCargoForm" />
			            <x:navigationMenuItem id="nav_4_5_5" itemLabel="Procesos">
				            <x:navigationMenuItem id="nav_4_5_5_1" itemLabel="Asignar Dotaciones" action="go_generarDotacionTrabajadorForm" />
				            <x:navigationMenuItem id="nav_4_5_5_2" itemLabel="Actualizaci�n de Tallas" action="go_actualizarDotacionTrabajadorForm" />
				            <x:navigationMenuItem id="nav_4_5_5_3" itemLabel="Entregar/Eliminar Dotaciones" action="go_generarDotacionEntregadaForm" />
			            </x:navigationMenuItem>		     
			            <x:navigationMenuItem id="nav_4_5_7" itemLabel="Dotaciones Asignadas a Trabajadores" action="go_dotacionTrabajadorForm" />
			       		<x:navigationMenuItem id="nav_4_5_8" itemLabel="Dotaciones Entregadas a Trabajadores" action="go_dotacionEntregadaForm" />
			            <x:navigationMenuItem id="nav_4_5_6" itemLabel="Reportes de Dotaciones" action="go_reportDotacionesForm" />			     
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_6" itemLabel="Polizas de Seguro">
				        <x:navigationMenuItem id="nav_4_6_1" itemLabel="Establecimientos de Salud" action="go_establecimientoSaludForm" />
				        <x:navigationMenuItem id="nav_4_6_2" itemLabel="Tipos de Siniestro" action="go_tipoSiniestroForm" />
			            <x:navigationMenuItem id="nav_4_6_3" itemLabel="P�lizas de Seguros" action="go_polizaForm" />
			            <x:navigationMenuItem id="nav_4_6_4" itemLabel="Planes de P�lizas" action="go_planPolizaForm" />
			            <x:navigationMenuItem id="nav_4_6_5" itemLabel="Primas por Planes" action="go_primasPlanForm" />
			            <x:navigationMenuItem id="nav_4_6_6" itemLabel="Primas por Cargo" action="go_primasCargoForm" />
			            <x:navigationMenuItem id="nav_4_6_7" itemLabel="Excepciones">
			            <x:navigationMenuItem id="nav_4_6_7_1" itemLabel="Excepciones de Titulares" action="go_excepcionTitularForm" />
			            <x:navigationMenuItem id="nav_4_6_7_2" itemLabel="Excepciones de Beneficiarios" action="go_excepcionBeneficiarioForm" />
			            </x:navigationMenuItem>	
			            <x:navigationMenuItem id="nav_4_6_8" itemLabel="Procesos">
			            <x:navigationMenuItem id="nav_4_6_8_1" itemLabel="Generar Titulares/Beneficiarios" action="go_generarPolizasForm" />
			            <x:navigationMenuItem id="nav_4_6_8_2" itemLabel="Registrar Pr�stamos por Financiamiento" action="go_generarPrestamoPolizaForm" />
			            <x:navigationMenuItem id="nav_4_6_8_4" itemLabel="Suspender Beneficiarios por Edad" action="go_suspenderPrimasForm" />
			            </x:navigationMenuItem>	
			            <x:navigationMenuItem id="nav_4_6_9" itemLabel="Titulares" action="go_titularForm" />
			       		<x:navigationMenuItem id="nav_4_6_10" itemLabel="Beneficiarios" action="go_beneficiarioForm" />
			       		<x:navigationMenuItem id="nav_4_6_12" itemLabel="Siniestros" action="go_siniestroForm" />
			       		<x:navigationMenuItem id="nav_4_6_11" itemLabel="Reportes">
			       		<x:navigationMenuItem id="nav_4_6_11_1" itemLabel="Reporte Beneficiarios/Titulares" action="go_reportPolizaBeneficiariosForm" />			       		
			       		<x:navigationMenuItem id="nav_4_6_11_2" itemLabel="Reporte Costos" action="go_reportPolizaCostosForm" />			       		
			       		<x:navigationMenuItem id="nav_4_6_11_3" itemLabel="Reporte Siniestros" action="go_reportPolizaSiniestrosForm" />			       		
			       		</x:navigationMenuItem>		     
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_7" itemLabel="Tickets">
				        <x:navigationMenuItem id="nav_4_7_0" itemLabel="Grupos de Tickets" action="go_grupoTicketForm" />
			        	<x:navigationMenuItem id="nav_4_7_1" itemLabel="Par�metros de Tickets" action="go_parametroTicketForm" />
			            <x:navigationMenuItem id="nav_4_7_2" itemLabel="Proveedores de Tickets" action="go_proveedorTicketForm" />	
			            <x:navigationMenuItem id="nav_4_7_3" itemLabel="Denominaciones de Tickets" action="go_denominacionTicketForm" />			            
			            <x:navigationMenuItem id="nav_4_7_4" itemLabel="Procesos">
				            <x:navigationMenuItem id="nav_4_7_4_1" itemLabel="Cerrar Registro de Ausencias" action="go_seguridadAusenciaForm" />					
							<x:navigationMenuItem id="nav_4_7_4_2" itemLabel="Asignaci�n de Tickets" action="go_asignarTicketsForm" />					
						</x:navigationMenuItem>	
			            <x:navigationMenuItem id="nav_4_7_5" itemLabel="Actualizaciones">
							<x:navigationMenuItem id="nav_4_7_5_1" itemLabel="Excepciones" action="go_excepcionTicketForm" />
							<x:navigationMenuItem id="nav_4_7_5_2" itemLabel="Retroactivos" action="go_retroactivoTicketForm" />
							<x:navigationMenuItem id="nav_4_7_5_3" itemLabel="Descuentos" action="go_descuentoTicketForm" />										
						</x:navigationMenuItem>			         			      		
			            <x:navigationMenuItem id="nav_4_7_6" itemLabel="Disquete de Cesta Tickets" action="go_disqueteTicketForm" />	
						<x:navigationMenuItem id="nav_4_7_7" itemLabel="Hist�ricos de Tickets" action="go_historicoTicketForm" />		    
						<x:navigationMenuItem id="nav_4_7_8" itemLabel="Procesos Generados Tickets" action="go_seguridadTicketForm" />
						<x:navigationMenuItem id="nav_4_7_9" itemLabel="Reporte Hist�ricos Tickets" action="go_reportHistoricoTicketsForm" />
			       </x:navigationMenuItem>
			       
			        <x:navigationMenuItem id="nav_4_8" itemLabel="Reportes" >
			           	<x:navigationMenuItem id="nav_4_8_1" itemLabel="Reporte Familiares" action="go_reportFamiliaresForm" />
			           	<x:navigationMenuItem id="nav_4_8_2" itemLabel="Reporte Madres y Padres Trabajadores" action="go_reportMadresPadresForm" />
			        </x:navigationMenuItem>			        
			   </x:navigationMenuItem>
			   
			   <x:navigationMenuItem id="nav_20" itemLabel="Reportes |">
			        <x:navigationMenuItem id="nav_20_1" itemLabel="Reporte Trabajadores por Tipo de Personal" action="go_reportTrabajadorTipoPersonalForm" />
			        <x:navigationMenuItem id="nav_20_2" itemLabel="Reporte Trabajadores por Regi�n" action="go_reportTrabajadorRegionForm" />
			        <x:navigationMenuItem id="nav_20_3" itemLabel="Reporte Trabajadores por Sede" action="go_reportTrabajadorSedeForm" />
			        <x:navigationMenuItem id="nav_20_4" itemLabel="Reporte Trabajadores por Unidad Ejecutora" action="go_reportTrabajadorUnidadEjecutoraForm" />
			        <x:navigationMenuItem id="nav_20_5" itemLabel="Reporte Trabajadores por Unidad Funcional" action="go_reportTrabajadorUnidadFuncionalForm" />	        
			        <x:navigationMenuItem id="nav_20_6" itemLabel="Reporte Trabajadores por Dependencia" action="go_reportTrabajadorDependenciaForm" />
			        <x:navigationMenuItem id="nav_20_7" itemLabel="Reporte Trabajadores por Situaci�n" action="go_reportTrabajadorSituacionForm" />  	 
			        <x:navigationMenuItem id="nav_20_8" itemLabel="Reporte Ingresos" action="go_reportIngresoForm" />
			        <x:navigationMenuItem id="nav_20_9" itemLabel="Reporte Egresos" action="go_reportEgresoForm" />  	  
			        <x:navigationMenuItem id="nav_20_10" itemLabel="Fechas">        
				        <x:navigationMenuItem id="nav_20_10_1" itemLabel="Reporte Fechas por Regi�n" action="go_reportFechasRegionForm" />   
				        <x:navigationMenuItem id="nav_20_10_2" itemLabel="Reporte Fechas por Sede" action="go_reportFechasSedeForm" /> 
				        <x:navigationMenuItem id="nav_20_10_3" itemLabel="Reporte Fechas por Unidad Funcional" action="go_reportFechasUnidadFuncionalForm" />
				        <x:navigationMenuItem id="nav_20_10_4" itemLabel="Reporte Fechas por Dependencia" action="go_reportFechasDependenciaForm" />   	        	        
				        <x:navigationMenuItem id="nav_20_10_5" itemLabel="Reporte Edad y Tiempo Servicio/Tipo Personal" action="go_reportFechasNacimientoServicioTipoPersonalForm" />   
				        <x:navigationMenuItem id="nav_20_10_6" itemLabel="Reporte Edad y Tiempo Servicio/Regi�n" action="go_reportFechasNacimientoServicioRegionForm" />   
				        <x:navigationMenuItem id="nav_20_10_7" itemLabel="Reporte Edad y Tiempo Servicio/UEL" action="go_reportFechasNacimientoServicioUnidadEjecutoraForm" />   
   				        <x:navigationMenuItem id="nav_20_10_8" itemLabel="Reporte Edad y Tiempo Servicio/Dependencia" action="go_reportFechasNacimientoServicioDependenciaForm" />   
			        </x:navigationMenuItem>   
			   </x:navigationMenuItem>
			        
			   <x:navigationMenuItem id="nav_6" itemLabel="Indicadores |">
			       <x:navigationMenuItem id="nav_6_1" itemLabel="Trabajadores por Tipo Personal" action="go_trabajadoresForm" />
			       <%/*<x:navigationMenuItem id="nav_6_2" itemLabel="Entidades Educativas por Caracteristicas" action="go_queryClasificacionDependenciaForm" />	*/%>		
			       <x:navigationMenuItem id="nav_6_3" itemLabel="Costo de N�mina" action="go_mapCostoNominaForm" />			
			       <x:navigationMenuItem id="nav_6_4" itemLabel="Gr�ficos" action="go_reportGraficosForm" />
			       <x:navigationMenuItem id="nav_6_5" itemLabel="Reportes">		
			            <x:navigationMenuItem id="nav_6_5_1" itemLabel="Trabajadores por Tipo Personal" action="go_reportTotalTrabajadorForm" />
			            <x:navigationMenuItem id="nav_6_5_2" itemLabel="Costos N�mina por Tipo Personal" action="go_reportTotalCostosForm" />
			        </x:navigationMenuItem>	
			   </x:navigationMenuItem>
			   <x:navigationMenuItem id="nav_5" itemLabel="Sistema">
			       <x:navigationMenuItem id="nav_5_1" itemLabel="Usuarios">
			            <x:navigationMenuItem id="nav_5_1_1" itemLabel="Usuarios" action="go_usuarioForm" />
			            <x:navigationMenuItem id="nav_5_1_2" itemLabel="Usuarios por Organismo" action="go_usuarioOrganismoForm" />
			            <x:navigationMenuItem id="nav_5_1_3" itemLabel="Usuarios por Tipo de Personal" action="go_usuarioTipoPersonalForm" />
			            <x:navigationMenuItem id="nav_5_1_0" itemLabel="Usuarios por Unidad Funcional" action="go_usuarioUnidadFuncionalForm" />
			            <x:navigationMenuItem id="nav_5_1_4" itemLabel="Roles" action="go_rolForm" />
			            <x:navigationMenuItem id="nav_5_1_5" itemLabel="Opciones" action="go_opcionForm" />
			            <x:navigationMenuItem id="nav_5_1_6" itemLabel="Asignar Opciones a Roles" action="go_rolOpcionForm" />
			            <x:navigationMenuItem id="nav_5_1_7" itemLabel="Asignar Roles a Usuarios" action="go_usuarioRolForm" />            		
			            <x:navigationMenuItem id="nav_5_1_8" itemLabel="Cambiar Contrase�a" action="go_changePasswordForm" />            		
			       </x:navigationMenuItem>
			       <x:navigationMenuItem id="nav_5_2" itemLabel="Reportes IDs" action="go_reportAdministracionIdForm" />                         
			   	   <x:navigationMenuItem id="nav_5_3" itemLabel="Reporte de Auditoria" action="go_reportAuditoriaForm" />                         
			   	  
			   </x:navigationMenuItem>
			   
			</x:jscookMenu>
			 <%} else {%>

			 
			 <x:jscookMenu layout="hbr" theme="ThemeOffice" rendered="#{loginSession.valid}">
			    
			    <x:navigationMenuItem id="nav_2_1" itemLabel="Expediente">

			            <x:navigationMenuItem id="nav_2_1_1" itemLabel="Datos Personales" action="go_personalForm" />
			            <x:navigationMenuItem id="nav_2_1_2" itemLabel="Educaci�n Formal" action="go_educacionForm"  />
			            <x:navigationMenuItem id="nav_2_1_3" itemLabel="Profesiones" action="go_profesionTrabajadorForm" />
			            <x:navigationMenuItem id="nav_2_1_4" itemLabel="Estudios Informales" action="go_estudioForm" />
			            <x:navigationMenuItem id="nav_2_1_5" itemLabel="Certificaciones" action="go_certificacionForm" />
			            <x:navigationMenuItem id="nav_2_1_6" itemLabel="Experiencia Laboral (S. Privado)" action="go_experienciaForm" />
			            <x:navigationMenuItem id="nav_2_1_7" itemLabel="Idiomas" action="go_idiomaForm" />
			            <x:navigationMenuItem id="nav_2_1_8" itemLabel="Grupo Familiar" action="go_familiarForm" />
			            <x:navigationMenuItem id="nav_2_1_9" itemLabel="Afiliaciones a Gremios" action="go_afiliacionForm" />
			            <x:navigationMenuItem id="nav_2_1_10" itemLabel="Actividades Docentes" action="go_actividadDocenteForm" />
			            <x:navigationMenuItem id="nav_2_1_11" itemLabel="Otras Actividades" action="go_otraActividadForm" />
			            <x:navigationMenuItem id="nav_2_1_12" itemLabel="Habilidades/Competencias" action="go_habilidadForm" />
			            <x:navigationMenuItem id="nav_2_1_13" itemLabel="Reconocimientos" action="go_reconocimientoForm" />
			            <x:navigationMenuItem id="nav_2_1_14" itemLabel="Sanciones" action="go_sancionForm" />
			            <x:navigationMenuItem id="nav_2_1_15" itemLabel="Averiguaciones Administrativas" action="go_averiguacionForm" />
			            <x:navigationMenuItem id="nav_2_1_16" itemLabel="Certificados de Carrera" action="go_certificadoForm" />
			            <x:navigationMenuItem id="nav_2_1_17" itemLabel="Publicaciones" action="go_publicacionForm" />            
			            <x:navigationMenuItem id="nav_2_1_18" itemLabel="Pasantias" action="go_pasantiaForm" />
			            <x:navigationMenuItem id="nav_2_1_19" itemLabel="Declaraciones Juradas" action="go_declaracionForm" />            
			            <x:navigationMenuItem id="nav_2_1_20" itemLabel="Reportes" >
			            <x:navigationMenuItem id="nav_2_1_20_1" itemLabel="Hoja de Vida" action="go_reportCurriculumForm" />			            	
			            </x:navigationMenuItem>
			        
				</x:navigationMenuItem>
			</x:jscookMenu>
			 <%} %>
