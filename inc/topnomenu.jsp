<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x"%>
<%@ page import="sigefirrhh.login.LoginSession" %>
<f:verbatim>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
	    <td align="left" width="100%">
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="22%" align="left"></f:verbatim>
						<h:graphicImage url="/images/logo-sigefirrhh-web.jpg" />
					<f:verbatim></td>
					<td width="64%" align="center"></f:verbatim>
						<h:graphicImage value="#{loginSession.URLNombre}" />
					<f:verbatim></td>
					<td width="78%" align="right"></f:verbatim>
						<h:graphicImage value="#{loginSession.URLLogo}" />
					<f:verbatim></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td class="menutitle">
    		<table width="100%" height="22" border="0" align="right" cellpadding="0" cellspacing="0">
    			<tr>
    				<td width="25%" align="center">
	    				<jsp:include page="/version.inc" />
    				</td>
    				<td width="50%" align="center">
    					Sistema de Gesti�n Financiera de los Recursos Humanos
    				</td>
    				<td width="25%" align="center" valign="center">
    					<a href="javascript:self.close();">
    					</f:verbatim>
							<h:graphicImage url="/images/buttons/exit.gif" />	
						<f:verbatim></a>										    					
    				</td>
    			</tr>
    		</table>		  
    	</td>
    </tr>
</table>
</f:verbatim>
