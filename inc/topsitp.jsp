<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x"%>
<%@ page import="sigefirrhh.login.LoginSession" %>
<f:verbatim>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
	    <td align="left" width="100%">
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="22%" align="left"></f:verbatim>
						<h:graphicImage url="/images/logo-sigefirrhh-web.jpg" />
					<f:verbatim></td>
					<td width="64%" align="center"></f:verbatim>
						<h:graphicImage value="#{loginSession.URLNombre}" />
					<f:verbatim></td>
					<td width="78%" align="right"></f:verbatim>
						<h:graphicImage value="#{loginSession.URLLogo}" />
					<f:verbatim></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td class="menutitle">
    		<table width="100%" height="22" border="0" align="right" cellpadding="0" cellspacing="0">
    			<tr>
    				<td width="25%" align="center">
    				</td>
    				<td width="50%" align="center">
    					Sistema de Gesti�n Financiera de los Recursos Humanos
    				</td>
    				<td width="25%" align="center" valign="center">
    					<a href="javascript:self.close();">
    					</f:verbatim>
							<h:graphicImage url="/images/buttons/exit.gif" />	
						<f:verbatim></a>										    					
    				</td>
    			</tr>
    		</table>		  
    	</td>
    </tr>
</table>
</f:verbatim>
			 <% if ( !((LoginSession)session.getAttribute("loginSession")).isServicioPersonal() ) { %>
			<x:jscookMenu layout="hbr" theme="ThemeOffice"  rendered="#{loginSession.valid}">
			    <x:navigationMenuItem id="nav_1"  itemLabel="Planificaci�n |"   >
			        <x:navigationMenuItem id="nav_1_1" itemLabel="Estructura Geogr�fica">
				        <x:navigationMenuItem id="nav_1_1_1" itemLabel="Continentes" action="go_continenteForm" />
				        <x:navigationMenuItem id="nav_1_1_2" itemLabel="Regiones Continentales" action="go_regionContinenteForm" />	        
			            <x:navigationMenuItem id="nav_1_1_3" itemLabel="Pa�s" action="go_paisForm" />
			            <x:navigationMenuItem id="nav_1_1_4" itemLabel="Estado" action="go_estadoForm" />
			            <x:navigationMenuItem id="nav_1_1_5" itemLabel="Ciudad" action="go_ciudadForm" />
			            <x:navigationMenuItem id="nav_1_1_6" itemLabel="Municipio" action="go_municipioForm" />
			            <x:navigationMenuItem id="nav_1_1_7" itemLabel="Parroquia" action="go_parroquiaForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_1_2" itemLabel="Estructura Organizativa">
			            <x:navigationMenuItem id="nav_1_2_1" itemLabel="Organismo" action="go_organismoForm" />
			            <x:navigationMenuItem id="nav_1_2_2" itemLabel="Nombres del Organismo" action="go_nombreOrganismoForm" />
			            <x:navigationMenuItem id="nav_1_2_3" itemLabel="Grupos del Organismo" action="go_grupoOrganismoForm" />
			            <x:navigationMenuItem id="nav_1_2_4" itemLabel="Regiones" action="go_regionForm" />
			            <x:navigationMenuItem id="nav_1_2_5" itemLabel="Sedes" action="go_sedeForm" />
			            <x:navigationMenuItem id="nav_1_2_6" itemLabel="Lugares de Pago" action="go_lugarPagoForm" />
			            <x:navigationMenuItem id="nav_1_2_7" itemLabel="Unidades Administradoras" action="go_unidadAdministradoraForm" />
			            <x:navigationMenuItem id="nav_1_2_8" itemLabel="Unidades Ejecutoras" action="go_unidadEjecutoraForm" />
			            <x:navigationMenuItem id="nav_1_2_9" itemLabel="Relaci�n UADM-UEL" action="go_administradoraUelForm" />
			            <x:navigationMenuItem id="nav_1_2_10" itemLabel="Estructuras Organizativas" action="go_estructuraForm" />
			            <x:navigationMenuItem id="nav_1_2_11" itemLabel="Unidades Funcionales" action="go_unidadFuncionalForm" />			            
			            <x:navigationMenuItem id="nav_1_2_12" itemLabel="Tipo Dependencias" action="go_tipoDependenciaForm" />
			            <x:navigationMenuItem id="nav_1_2_13" itemLabel="Dependencias" action="go_dependenciaForm" />			           
			            <x:navigationMenuItem id="nav_1_2_15" itemLabel="Reportes" action="go_reportEstructuraForm" />
			        </x:navigationMenuItem>
			        
			        <x:navigationMenuItem id="nav_1_13" itemLabel="Estructura Organizativa Docente">
			       		<x:navigationMenuItem id="nav_1_13_1" itemLabel="Tipos de Caracter�sticas" action="go_tipoCaracteristicaForm" />
			            <x:navigationMenuItem id="nav_1_13_2" itemLabel="Caracter�sticas de Entidades" action="go_caracteristicaDependenciaForm" />
			            <x:navigationMenuItem id="nav_1_13_3" itemLabel="Clasificaci�n Entidades Educativas" action="go_clasificacionDependenciaForm" />
			         
			        </x:navigationMenuItem>
			     	<x:navigationMenuItem id="nav_1_3" itemLabel="Clases de Cargos y Tabuladores">
			            <x:navigationMenuItem id="nav_1_3_1" itemLabel="Ramos Ocupacionales" action="go_ramoOcupacionalForm" />
			            <x:navigationMenuItem id="nav_1_3_2" itemLabel="Grupos Ocupacionales" action="go_grupoOcupacionalForm" />
			            <x:navigationMenuItem id="nav_1_3_3" itemLabel="Series de Cargos" action="go_serieCargoForm" />
			            <x:navigationMenuItem id="nav_1_3_4" itemLabel="Tabuladores" action="go_tabuladorForm" />
			            <x:navigationMenuItem id="nav_1_3_5" itemLabel="Detalle de Tabuladores" action="go_detalleTabuladorForm" />
			            <x:navigationMenuItem id="nav_1_3_6" itemLabel="Clasificadores de Cargos" action="go_manualCargoForm" />
			            <x:navigationMenuItem id="nav_1_3_7" itemLabel="Cargos" action="go_cargoForm" />
			            <x:navigationMenuItem id="nav_1_3_8" itemLabel="Reportes" action="go_reportCargosForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_1_11" itemLabel="Clases de Cargos y Tabuladores (Docentes)">
			            <x:navigationMenuItem id="nav_1_11_1" itemLabel="Grados" action="go_gradoDocenteForm" />
			            <x:navigationMenuItem id="nav_1_11_2" itemLabel="Jerarquias" action="go_jerarquiaDocenteForm" />
			            <x:navigationMenuItem id="nav_1_11_3" itemLabel="Niveles" action="go_nivelDocenteForm" />
			            <x:navigationMenuItem id="nav_1_11_4" itemLabel="Categorias" action="go_categoriaDocenteForm" />
			            <x:navigationMenuItem id="nav_1_11_5" itemLabel="Turnos" action="go_turnoDocenteForm" />
			            <x:navigationMenuItem id="nav_1_11_6" itemLabel="Dedicacion" action="go_dedicacionDocenteForm" />
			            <x:navigationMenuItem id="nav_1_11_7" itemLabel="Tabulador" action="go_tabuladorForm" />
			            <x:navigationMenuItem id="nav_1_11_8" itemLabel="Detalle Tabulador" action="go_detalleTabuladorMedForm" />
			            <x:navigationMenuItem id="nav_1_11_9" itemLabel="Cargos" action="go_cargoDocenteForm" />
			        </x:navigationMenuItem>
			      
			        <x:navigationMenuItem id="nav_1_9" itemLabel="Clasificacion">
			            <x:navigationMenuItem id="nav_1_9_1" itemLabel="Perfil de Cargos" action="go_perfilForm" />
			            <x:navigationMenuItem id="nav_1_9_2" itemLabel="Requisitos de Cargos" >
				            <x:navigationMenuItem id="nav_1_9_2_1" itemLabel="Profesiones" action="go_profesionCargoForm" />
				            <x:navigationMenuItem id="nav_1_9_2_2" itemLabel="Experiencia Laboral" action="go_experienciaCargoForm" />
				            <x:navigationMenuItem id="nav_1_9_2_3" itemLabel="Adiestramiento" action="go_adiestramientoCargoForm" />
				            <x:navigationMenuItem id="nav_1_9_2_4" itemLabel="Habilidades/Competencias" action="go_habilidadCargoForm" />
				         </x:navigationMenuItem>
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_1_12" itemLabel="Registro de Oferentes">
				        <x:navigationMenuItem id="nav_1_12_1" itemLabel="Datos Personales" action="go_elegibleForm" />
			            <x:navigationMenuItem id="nav_1_12_2" itemLabel="Educaci�n Formal" action="go_elegibleEducacionForm"  />
			            <x:navigationMenuItem id="nav_1_12_3" itemLabel="Profesiones" action="go_elegibleProfesionForm" />
			            <x:navigationMenuItem id="nav_1_12_4" itemLabel="Estudios Informales" action="go_elegibleEstudioForm" />
			            <x:navigationMenuItem id="nav_1_12_5" itemLabel="Certificaciones" action="go_elegibleCertificacionForm" />
			            <x:navigationMenuItem id="nav_1_12_6" itemLabel="Experiencia Laboral" action="go_elegibleExperienciaForm" />
			            <x:navigationMenuItem id="nav_1_12_7" itemLabel="Idiomas" action="go_elegibleIdiomaForm" />
			            <x:navigationMenuItem id="nav_1_12_8" itemLabel="Grupo Familiar" action="go_elegibleFamiliarForm" />
			            <x:navigationMenuItem id="nav_1_12_9" itemLabel="Afiliaciones a Gremios" action="go_elegibleAfiliacionForm" />
			            <x:navigationMenuItem id="nav_1_12_10" itemLabel="Actividades Docentes" action="go_elegibleActividadDocenteForm" />
			            <x:navigationMenuItem id="nav_1_12_11" itemLabel="Otras Actividades" action="go_elegibleOtraActividadForm" />
			            <x:navigationMenuItem id="nav_1_12_12" itemLabel="Habilidades/Competencias" action="go_elegibleHabilidadForm" />
			            <x:navigationMenuItem id="nav_1_12_13" itemLabel="Publicaciones" action="go_elegiblePublicacionForm" />            
			            <x:navigationMenuItem id="nav_1_12_14" itemLabel="Hoja de Vida" action="go_reportCurriculumElegibleForm" />           
			        </x:navigationMenuItem>
			         <x:navigationMenuItem id="nav_1_10" itemLabel="Reclutamiento y Selecci�n">
 			        	 <x:navigationMenuItem id="nav_1_10_1" itemLabel="Concursos">
			            	<x:navigationMenuItem id="nav_1_10_1_1" itemLabel="Concursos P�blicos" action="go_concursoForm" />			            	
			            	<x:navigationMenuItem id="nav_1_10_1_2" itemLabel="Cargos Asociados a Concursos" action="go_concursoCargoForm" />
			         	</x:navigationMenuItem> 	
			         	<x:navigationMenuItem id="nav_1_10_2" itemLabel="Procesos de Reclutamiento">
			           		<x:navigationMenuItem id="nav_1_10_2_1" itemLabel="Postulados a Concursos" action="go_postuladoConcursoForm" />
			            <%--<x:navigationMenuItem id="nav_1_10_2_2" itemLabel="Registro Externo de Postulados" action="go_postuladoExternoForm" /> --%>
			            	<x:navigationMenuItem id="nav_1_10_2_3" itemLabel="Preselecci�n de Postulados" action="go_preseleccionarForm" />
			        	</x:navigationMenuItem>
			        	<x:navigationMenuItem id="nav_1_10_3" itemLabel="Procesos de Selecci�n" >
							<x:navigationMenuItem id="nav_1_10_3_1" itemLabel="Pruebas y Entrevistas" action="go_pruebaPreseleccionadoForm" />
							<x:navigationMenuItem id="nav_1_10_3_2" itemLabel="Evaluar Requerimientos" action="go_postuladoConcursoForm" />	
							<x:navigationMenuItem id="nav_1_10_3_3" itemLabel="Registrar Seleccionado" action="go_seleccionarForm" />
							<x:navigationMenuItem id="nav_1_10_3_4" itemLabel="Registrar Elegible" action="go_seleccionarElegibleForm" />			            	
			        	</x:navigationMenuItem>
			        	<x:navigationMenuItem id="nav_1_10_4" itemLabel="Reporte de Elegibles" action="go_reportElegibleForm" />
			        	<x:navigationMenuItem id="nav_1_10_5" itemLabel="Tipos Pruebas/Entrevistas" action="go_pruebaSeleccionForm" />
			        	<x:navigationMenuItem id="nav_1_10_6" itemLabel="Baremos" action="go_varemosForm" />	
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_1_5" itemLabel="Capacitaci�n">
			            <x:navigationMenuItem id="nav_1_5_1" itemLabel="Tablas B�sicas">
			                <x:navigationMenuItem id="nav_1_5_1_1" itemLabel="Areas de Conocimiento" action="go_areaConocimientoForm" />
			                <x:navigationMenuItem id="nav_1_5_1_2" itemLabel="Tipos de Cursos" action="go_tipoCursoForm" />			               
			            </x:navigationMenuItem>
			            <x:navigationMenuItem id="nav_1_5_2" itemLabel="Planificaci�n">
			                <x:navigationMenuItem id="nav_1_5_2_1" itemLabel="Planes por Unidad Funcional" action="go_planAdiestramientoForm" />
			               <x:navigationMenuItem id="nav_1_5_2_4" itemLabel="Reportes" >
				                <x:navigationMenuItem id="nav_1_5_2_4_1" itemLabel="Planes de Adiestramiento" action="go_reportPlanesAdiestramientoForm" />			                
			                </x:navigationMenuItem>
			            </x:navigationMenuItem>
			        </x:navigationMenuItem>
			         <x:navigationMenuItem id="nav_1_6" itemLabel="Evaluaciones de Desempe�o">			                
			                <x:navigationMenuItem id="nav_1_6_1" itemLabel="Consulta de Evaluaciones" action="go_evaluacionForm" />
			                <x:navigationMenuItem id="nav_1_6_6" itemLabel="Apelaciones" action="go_apelacionForm" />
			                <x:navigationMenuItem id="nav_1_6_7" itemLabel="Trabajadores No Evaluados" action="go_noEvaluacionForm" />
			                <x:navigationMenuItem id="nav_1_6_3" itemLabel="Tipos de Acciones Resultantes" action="go_accionEvaluacionForm" />
			                <x:navigationMenuItem id="nav_1_6_4" itemLabel="Tipos de Resultados de Evaluacion" action="go_resultadoEvaluacionForm" />  			                
			                <x:navigationMenuItem id="nav_1_6_5" itemLabel="Reportes" >        
			                	<x:navigationMenuItem id="nav_1_6_5_1" itemLabel="Detalle Evaluaciones" action="go_reportEvaluacionesForm" />        
			                  	<x:navigationMenuItem id="nav_1_6_5_2" itemLabel="Resumen Evaluaciones" action="go_reportResumenEvaluacionesForm" />     
			                  	<x:navigationMenuItem id="nav_1_6_5_3" itemLabel="Notificaci�n de Resultados" action="go_reportNotificacionEvaluacionesForm" />  
			                  	<x:navigationMenuItem id="nav_1_6_5_4" itemLabel="Trabajadores No Evaluados" action="go_reportNoEvaluacionesForm" />  
			              	</x:navigationMenuItem>
			            </x:navigationMenuItem>
			         <x:navigationMenuItem id="nav_1_17" itemLabel="Planes de Personal">
			            <x:navigationMenuItem id="nav_1_7_1" itemLabel="Planes" action="go_planPersonalForm" />	
			            <x:navigationMenuItem id="nav_1_7_2" itemLabel="Componentes" >  
			               <x:navigationMenuItem id="nav_1_7_2_1" itemLabel="Movimientos de Personal" action="go_movimientosPlanForm" />
			               <x:navigationMenuItem id="nav_1_7_2_2" itemLabel="Movimientos de Cargos" action="go_cargosPlanForm" />
			               <x:navigationMenuItem id="nav_1_7_2_3" itemLabel="Acciones Administrativas" action="go_registroCargosAprobadoForm" />
				           <x:navigationMenuItem id="nav_1_7_2_4" itemLabel="Contratos de Personal" action="go_contratosPlanForm" />
				       </x:navigationMenuItem>
				        <x:navigationMenuItem id="nav_1_7_3" itemLabel="Reportes" >
				        	<x:navigationMenuItem id="nav_1_7_3_1" itemLabel="Plan Personal" action="go_reportPlanPersonalForm" />	
				        	<x:navigationMenuItem id="nav_1_7_3_2" itemLabel="Indicadores" action="go_reportPlanIndicadoresForm" />	
				        </x:navigationMenuItem> 				        		           
			        </x:navigationMenuItem>       
			   </x:navigationMenuItem>
			    <x:navigationMenuItem id="nav_2" itemLabel="Expediente |">
			    	<x:navigationMenuItem id="nav_2_1" itemLabel="Expediente">
			            <x:navigationMenuItem id="nav_2_1_1" itemLabel="Datos Personales" action="go_personalForm" />
			            <x:navigationMenuItem id="nav_2_1_2" itemLabel="Educaci�n Formal" action="go_educacionForm"  />
			            <x:navigationMenuItem id="nav_2_1_3" itemLabel="Profesiones" action="go_profesionTrabajadorForm" />
			            <x:navigationMenuItem id="nav_2_1_4" itemLabel="Estudios Informales" action="go_estudioForm" />
			            <x:navigationMenuItem id="nav_2_1_5" itemLabel="Certificaciones" action="go_certificacionForm" />
			            <x:navigationMenuItem id="nav_2_1_6" itemLabel="Experiencia Laboral (S. Privado)" action="go_experienciaForm" />
			            <x:navigationMenuItem id="nav_2_1_7" itemLabel="Idiomas" action="go_idiomaForm" />
			            <x:navigationMenuItem id="nav_2_1_8" itemLabel="Grupo Familiar" action="go_familiarForm" />
			            <x:navigationMenuItem id="nav_2_1_9" itemLabel="Afiliaciones a Gremios" action="go_afiliacionForm" />
			            <x:navigationMenuItem id="nav_2_1_10" itemLabel="Actividades Docentes" action="go_actividadDocenteForm" />
			            <x:navigationMenuItem id="nav_2_1_11" itemLabel="Otras Actividades" action="go_otraActividadForm" />
			            <x:navigationMenuItem id="nav_2_1_12" itemLabel="Habilidades/Competencias" action="go_habilidadForm" />
			            <x:navigationMenuItem id="nav_2_1_13" itemLabel="Reconocimientos" action="go_reconocimientoForm" />
			            <x:navigationMenuItem id="nav_2_1_14" itemLabel="Sanciones" action="go_sancionForm" />
			            <x:navigationMenuItem id="nav_2_1_15" itemLabel="Averiguaciones Administrativas" action="go_averiguacionForm" />
			            <x:navigationMenuItem id="nav_2_1_16" itemLabel="Certificados de Carrera" action="go_certificadoForm" />
			            <x:navigationMenuItem id="nav_2_1_17" itemLabel="Publicaciones" action="go_publicacionForm" />            
			            <x:navigationMenuItem id="nav_2_1_18" itemLabel="Pasantias" action="go_pasantiaForm" />
			            <x:navigationMenuItem id="nav_2_1_19" itemLabel="Declaraciones Juradas" action="go_declaracionForm" />            
			            
			         </x:navigationMenuItem>         
			        <x:navigationMenuItem id="nav_2_2" itemLabel="Historial">
			            <x:navigationMenuItem id="nav_2_2_1" itemLabel="Trayectoria y Antecedentes" >				            
			            	<x:navigationMenuItem id="nav_2_2_1_1" itemLabel="Consultar Trayectoria APN" action="go_trayectoriaForm" />
				            <x:navigationMenuItem id="nav_2_2_1_3" itemLabel="Antecedentes/Servicio sujeto a LEFP" action="go_antecedenteForm" />
			            	<x:navigationMenuItem id="nav_2_2_1_4" itemLabel="Antecedentes/Servicio no sujeto a LEFP" action="go_experienciaNoEstForm" />
				            <x:navigationMenuItem id="nav_2_2_1_5" itemLabel="Reporte Relaci�n de Cargos" action="go_reportRelacionCargosForm" />
				            <x:navigationMenuItem id="nav_2_2_1_6" itemLabel="Reporte Antecedentes de Servicio" action="go_reportAntecedentesForm" />
				        </x:navigationMenuItem>            
			            <x:navigationMenuItem id="nav_2_2_2" itemLabel="Encargadur�as" action="go_encargaduriaForm" />
			            <x:navigationMenuItem id="nav_2_2_3" itemLabel="Suplencias" action="go_suplenciaForm" />            
			            <x:navigationMenuItem id="nav_2_2_4" itemLabel="Contratos" action="go_contratoForm" />			            			            
			            <x:navigationMenuItem id="nav_2_2_5" itemLabel="Comision/Servicio (personal interno)" action="go_comisionServicioForm" />
			            <x:navigationMenuItem id="nav_2_2_6" itemLabel="Servicio Exterior" action="go_servicioExteriorForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_2_3" itemLabel="Tablas B�sicas Expediente">
			            <x:navigationMenuItem id="nav_2_3_2" itemLabel="Grupo de Profesiones" action="go_grupoProfesionForm" />
			            <x:navigationMenuItem id="nav_2_3_15" itemLabel="Subgrupo de Profesiones" action="go_subgrupoProfesionForm" />
			            <x:navigationMenuItem id="nav_2_3_16" itemLabel="Profesiones" action="go_profesionForm" />
			            <x:navigationMenuItem id="nav_2_3_1" itemLabel="Areas de Carrera" action="go_areaCarreraForm" />
			            <x:navigationMenuItem id="nav_2_3_4" itemLabel="Carreras" action="go_carreraForm" />
			            <x:navigationMenuItem id="nav_2_3_5" itemLabel="Carreras por Area" action="go_carreraAreaForm" />
			            <x:navigationMenuItem id="nav_2_3_6" itemLabel="T�tulos" action="go_tituloForm" />
			            <x:navigationMenuItem id="nav_2_3_7" itemLabel="Idiomas" action="go_tipoIdiomaForm" />
			            <x:navigationMenuItem id="nav_2_3_8" itemLabel="Tipo Sanciones" action="go_tipoAmonestacionForm" />
			            <x:navigationMenuItem id="nav_2_3_9" itemLabel="Tipo Reconocimientos" action="go_tipoReconocimientoForm" />
			            <x:navigationMenuItem id="nav_2_3_10" itemLabel="Habilidades/Competencias" action="go_tipoHabilidadForm" />
			            <x:navigationMenuItem id="nav_2_3_11" itemLabel="Otras Actividades" action="go_tipoOtraActividadForm" />
				   </x:navigationMenuItem>
				   <x:navigationMenuItem id="nav_2_4" itemLabel="Reportes" >
			            	<x:navigationMenuItem id="nav_2_4_1" itemLabel="Hoja de Vida" action="go_reportCurriculumForm" />
			            	<x:navigationMenuItem id="nav_2_4_2" itemLabel="Hojas de Vida por Regi�n" action="go_reportCurriculumRegionForm" />
			            	<x:navigationMenuItem id="nav_2_4_3" itemLabel="Criterios Varios" action="go_reportTrabajadorForm" />
			        </x:navigationMenuItem>
				  </x:navigationMenuItem>
			   <x:navigationMenuItem id="nav_3" itemLabel="Administraci�n |">
				   <x:navigationMenuItem id="nav_3_1" itemLabel="Parametrizacion">
					   <x:navigationMenuItem id="nav_3_1_1" itemLabel="Definiciones">
			                <x:navigationMenuItem id="nav_3_1_1_1" itemLabel="Categor�a de Personal" action="go_categoriaPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_2" itemLabel="Relaci�n Laboral" action="go_relacionPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_3" itemLabel="Clasificaci�n de Personal" action="go_clasificacionPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_4" itemLabel="Grupo de N�minas" action="go_grupoNominaForm" />
			                <x:navigationMenuItem id="nav_3_1_1_5" itemLabel="Tipos de Personal" action="go_tipoPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_6" itemLabel="Restricciones de Personal" action="go_restringidoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_7" itemLabel="Clasificadores Tipo de Personal" action="go_manualPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_8" itemLabel="Frecuencias de Pago" action="go_frecuenciaPagoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_9" itemLabel="Conceptos" action="go_conceptoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_10" itemLabel="Frecuencias Tipo de Personal" action="go_frecuenciaTipoPersonalForm" />
			                <x:navigationMenuItem id="nav_3_1_1_11" itemLabel="Conceptos Tipo de Personal" action="go_conceptoTipoPersonalForm" />
			         <%--   <x:navigationMenuItem id="nav_3_1_1_12" itemLabel="Conceptos por Cargo y A�o" action="go_conceptoCargoAnioForm" /> --%>
			                <x:navigationMenuItem id="nav_3_1_1_13" itemLabel="Conceptos por Cargo" action="go_conceptoCargoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_14" itemLabel="Conceptos por Dependencia" action="go_conceptoDependenciaForm" />
			                <x:navigationMenuItem id="nav_3_1_1_15" itemLabel="Turno" action="go_turnoForm" />
			                <x:navigationMenuItem id="nav_3_1_1_16" itemLabel="Tipos de Contrato" action="go_tipoContratoForm" />
			            </x:navigationMenuItem>        
			            <x:navigationMenuItem id="nav_3_1_2" itemLabel="Parametros y F�rmulas">
			    	        <x:navigationMenuItem id="nav_3_1_2_1" itemLabel="Conceptos Asociados" action="go_conceptoAsociadoForm" />
			                <x:navigationMenuItem id="nav_3_1_2_2" itemLabel="Prima Antiguedad" action="go_primaAntiguedadForm" />
			                <x:navigationMenuItem id="nav_3_1_2_3" itemLabel="Prima Por Hijo" action="go_primaHijoForm" />
			                <x:navigationMenuItem id="nav_3_1_2_5" itemLabel="Par�metros Retenciones" action="go_parametroGobiernoForm" />
			                <x:navigationMenuItem id="nav_3_1_2_6" itemLabel="F�rmula Bono Vacacional" action="go_conceptoVacacionesForm" />
			                <x:navigationMenuItem id="nav_3_1_2_7" itemLabel="Vacaciones por A�os" action="go_vacacionesPorAnioForm" />   			                     
			                <x:navigationMenuItem id="nav_3_1_2_8" itemLabel="F�rmula Bono Fin de A�o" action="go_conceptoUtilidadesForm" />        			                
			                <x:navigationMenuItem id="nav_3_1_2_9" itemLabel="Dias Bono Fin de A�o" action="go_utilidadesPorAnioForm" />
			                <x:navigationMenuItem id="nav_3_1_2_10" itemLabel="Par�metros Varios" action="go_parametroVariosForm" />
			                <x:navigationMenuItem id="nav_3_1_2_11" itemLabel="Par�metros Planilla ARI" action="go_parametroAriForm" />
			         <%--   <x:navigationMenuItem id="nav_3_1_2_12" itemLabel="Par�metros Jubilaci�n" action="go_parametroJubilacionForm" /> --%>
			             </x:navigationMenuItem>            
						<x:navigationMenuItem id="nav_3_1_3" itemLabel="Tablas B�sicas N�mina">
			                <x:navigationMenuItem id="nav_3_1_3_1" itemLabel="Bancos" action="go_bancoForm" />
			                <x:navigationMenuItem id="nav_3_1_3_2" itemLabel="Cuentas de Banco" action="go_cuentaBancoForm" />
			                <x:navigationMenuItem id="nav_3_1_3_3" itemLabel="Per�odos Semanales" action="go_semanaForm" />
			                <x:navigationMenuItem id="nav_3_1_3_4" itemLabel="Meses" action="go_mesForm" />
			                <x:navigationMenuItem id="nav_3_1_3_5" itemLabel="Sindicatos" action="go_sindicatoForm" />
			                <x:navigationMenuItem id="nav_3_1_3_6" itemLabel="Contratos Colectivos" action="go_contratoColectivoForm" />
			                <x:navigationMenuItem id="nav_3_1_3_7" itemLabel="Tarifas ARI" action="go_tarifaAriForm" />
			                <x:navigationMenuItem id="nav_3_1_3_8" itemLabel="Firmas para Reportes" action="go_firmasReportesForm" />
					   </x:navigationMenuItem>
						<x:navigationMenuItem id="nav_3_1_4" itemLabel="Reportes">
			                <x:navigationMenuItem id="nav_3_1_4_1" itemLabel="Definiciones de Conceptos" action="go_reportDefinicionesForm" />

					   </x:navigationMenuItem>
			        </x:navigationMenuItem>    
			           
			           <x:navigationMenuItem id="nav_3_2" itemLabel="Datos del Trabajador">
			            <x:navigationMenuItem id="nav_3_2_1" itemLabel="Trabajador">
				            <x:navigationMenuItem id="nav_3_2_1_1" itemLabel="Ubicar Trabajador" action="go_ubicarTrabajadorForm"/>
				            <x:navigationMenuItem id="nav_3_2_1_2" itemLabel="Ubicar Trabajador Docente" action="go_ubicarTrabajadorDocenteForm"/>
			            	<x:navigationMenuItem id="nav_3_2_1_3" itemLabel="Consultar Datos N�mina" action="go_trabajadorForm"/>
			            </x:navigationMenuItem>        
			            <x:navigationMenuItem id="nav_3_2_2" itemLabel="Conceptos Fijos" action="go_conceptoFijoForm" />
			            <x:navigationMenuItem id="nav_3_2_4" itemLabel="Pr�stamos" action="go_prestamoForm" />
			            <x:navigationMenuItem id="nav_3_2_5" itemLabel="Sueldos Promedios" action="go_sueldoPromedioForm" />			          		            
			            <x:navigationMenuItem id="nav_3_2_6" itemLabel="Anticipos" action="go_anticipoForm" />
			            <x:navigationMenuItem id="nav_3_2_9" itemLabel="Ausencias" action="go_ausenciaForm" />
			            <x:navigationMenuItem id="nav_3_2_0" itemLabel="Ausencias (Unidad Funcional)" action="go_ausenciaUnidadFuncionalForm" />
			            <x:navigationMenuItem id="nav_3_2_10" itemLabel="Planilla ARI" action="go_planillaAriForm" />
			            <x:navigationMenuItem id="nav_3_2_11" itemLabel="Planilla ARC" action="go_planillaArcForm" />	 
			            <x:navigationMenuItem id="nav_3_2_12" itemLabel="Embargos" >
			            	<x:navigationMenuItem id="nav_3_2_12_1" itemLabel="Sentencias" action="go_embargoForm" />
			            	<x:navigationMenuItem id="nav_3_2_12_2" itemLabel="Detalle Embargo" action="go_embargoConceptoForm" />
			            </x:navigationMenuItem>        
			            <x:navigationMenuItem id="nav_3_2_13" itemLabel="Reportes" >
			            	<x:navigationMenuItem id="nav_3_2_13_1" itemLabel="Trabajadores Con un Concepto" action="go_reportConceptosForm" />
			            	<x:navigationMenuItem id="nav_3_2_13_2" itemLabel="Trabajadores Sin un Concepto" action="go_reportSinConceptosForm" />
			            	<x:navigationMenuItem id="nav_3_2_13_3" itemLabel="Pr�stamos Vigentes" action="go_reportPrestamosForm" />
			            </x:navigationMenuItem>        
			        </x:navigationMenuItem>   
			               
			        <x:navigationMenuItem id="nav_3_3" itemLabel="Movimientos de Personal">
			            	<x:navigationMenuItem id="nav_3_3_1" itemLabel="Tipos de Movimientos y Causas"  >    
			            		<x:navigationMenuItem id="nav_3_3_1_1" itemLabel="Tipos de Movimiento" action="go_movimientoPersonalForm" />                  
			            		<x:navigationMenuItem id="nav_3_3_1_2" itemLabel="Causas del Movimiento" action="go_causaMovimientoForm" />                  
			            		<x:navigationMenuItem id="nav_3_3_1_3" itemLabel="Movimientos por Tipo Personal" action="go_causaPersonalForm" />                  
			        		</x:navigationMenuItem> 
			        		
				                              
			               	<x:navigationMenuItem id="nav_3_3_4" itemLabel="Manejo de Remesas"  >    
								<x:navigationMenuItem id="nav_3_3_4_1" itemLabel="Remesas" action="go_remesaForm" />                  
								<x:navigationMenuItem id="nav_3_3_4_2" itemLabel="Asociar Movimientos" action="go_remesaMovimientoSitpForm" />                  
								<x:navigationMenuItem id="nav_3_3_4_3" itemLabel="Cerrar Remesa" action="go_cerrarRemesaForm" />      
								<x:navigationMenuItem id="nav_3_3_4_4" itemLabel="Enviar Remesa" action="go_enviarRemesaForm" />
								<x:navigationMenuItem id="nav_3_3_4_7" itemLabel="Anular Movimientos" action="go_anulacionRemesaMovimientoSitpForm" />
								<x:navigationMenuItem id="nav_3_3_4_8" itemLabel="Aprobar Movimientos" action="go_aprobarRemesaMovimientoSitpForm" />  
								<x:navigationMenuItem id="nav_3_3_4_5" itemLabel="Recibir Remesas" action="go_recibirRemesaForm" />                              
								<x:navigationMenuItem id="nav_3_3_4_6" itemLabel="Reportes" >    
									<x:navigationMenuItem id="nav_3_3_4_6_1" itemLabel="Movimientos con Remesa"  action="go_reportRemesasForm" />     					
									<x:navigationMenuItem id="nav_3_3_4_6_2" itemLabel="Movimientos sin Remesa"  action="go_reportMovimientosForm" />     					
			            		</x:navigationMenuItem> 								
			            	</x:navigationMenuItem> 			            	
							<x:navigationMenuItem id="nav_3_3_5" itemLabel="Personal Inhabilitado" action="go_inhabilitadoForm" />                  
							
			        </x:navigationMenuItem>  
			      
			        <x:navigationMenuItem id="nav_3_9" itemLabel="Registro Cargos/Puestos">
			            <x:navigationMenuItem id="nav_3_9_1" itemLabel="Definici�n de Registros" action="go_registroForm" />
			            <x:navigationMenuItem id="nav_3_9_2" itemLabel="Registros por Tipo Personal" action="go_registroPersonalForm" />
			            <x:navigationMenuItem id="nav_3_9_3" itemLabel="Consultar Posicion Registro" action="go_registroCargosForm" />
			        	<x:navigationMenuItem id="nav_3_9_4" itemLabel="Trayectoria Posicion Registro" action="go_historicoCargosForm" />
			            <x:navigationMenuItem id="nav_3_9_5" itemLabel="Agregar Cargos/Puestos" action="go_agregarRegistroCargosForm" />
			            <x:navigationMenuItem id="nav_3_9_6" itemLabel="Reportes">
			            	<x:navigationMenuItem id="nav_3_9_6_1" itemLabel="Registros Cargos/Puestos" action="go_reportRegistroCargosForm" />
			            	<x:navigationMenuItem id="nav_3_9_6_2" itemLabel="Registros Cantidad Cargos" action="go_reportCantidadCargosForm" />
			            	<x:navigationMenuItem id="nav_3_9_6_3" itemLabel="Registros Cargos/Puestos por UEL" action="go_reportRegistroCargosUELForm" />
			            </x:navigationMenuItem>
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_3_10" itemLabel="Prestaciones Mensuales de Antiguedad">
				        <x:navigationMenuItem id="nav_3_10_0" itemLabel="Procesos Generados" action="go_seguridadPrestacionesForm" />
			            <x:navigationMenuItem id="nav_3_10_1" itemLabel="Conceptos para Prestaciones" action="go_conceptoPrestacionesForm" />
			            <x:navigationMenuItem id="nav_3_10_2" itemLabel="Prestaciones por Trabajador" action="go_prestacionesMensualesForm" />
			            <x:navigationMenuItem id="nav_3_10_9" itemLabel="Prestaciones Formato Onapre por Trabajador" action="go_prestacionesOnapreForm" />
			            <x:navigationMenuItem id="nav_3_10_7" itemLabel="Reportes">
			            	<x:navigationMenuItem id="nav_3_10_7_1" itemLabel="Prestaciones Mensuales" action="go_reportPrestacionesMensualesForm" />            	
			            	<x:navigationMenuItem id="nav_3_10_7_2" itemLabel="Dias Adicionales" action="go_reportDiasAdicionalesForm" />      
			            	<x:navigationMenuItem id="nav_3_10_7_3" itemLabel="Abono 5 D�as" action="go_reportAbono5DiasForm" />                	
			            	<x:navigationMenuItem id="nav_3_10_7_4" itemLabel="Estados de Cuenta General" action="go_reportPrestacionesTrabajadorGeneralForm" />            	
			            	<x:navigationMenuItem id="nav_3_10_7_5" itemLabel="Estado de Cuenta Individual" action="go_reportPrestacionesTrabajadorForm" />        
			            	<x:navigationMenuItem id="nav_3_10_7_6" itemLabel="Por Categoria Presupuesto" action="go_reportPrestacionesPresupuestoForm" />            	
			            </x:navigationMenuItem>
			         </x:navigationMenuItem>
			         <x:navigationMenuItem id="nav_3_19" itemLabel="Pasivos Laborales">
			         	
	    		        <x:navigationMenuItem id="nav_3_19_10" itemLabel="Reportes" >	    		        	
		    		        <x:navigationMenuItem id="nav_3_19_10_1" itemLabel="Regimen Derogado" >	    		        	
		    		   	        <x:navigationMenuItem id="nav_3_19_10_1_1" itemLabel="Deuda e Intereses por Tipo Personal" action="go_reportRegimenDerogadoForm" />	    		        	
		    		            <x:navigationMenuItem id="nav_3_19_10_1_2" itemLabel="Deuda/Intereses/Devengados por Trabajador" action="go_reportRegimenDerogadoTrabajadorForm" />	    		        	
		    		            <x:navigationMenuItem id="nav_3_19_10_1_3"  itemLabel="Intereses Adicionales Mensuales" action="go_reportInteresAdicionalMensualForm" />	    		        	
	    		            </x:navigationMenuItem>	  
		    		        <x:navigationMenuItem id="nav_3_19_10_2" itemLabel="Nuevo Regimen" >	    		        	
		    		   	    	<x:navigationMenuItem id="nav_3_19_10_2_1" itemLabel="Deuda e Intereses por  Tipo Personal" action="go_reportNuevoRegimenForm" />	    		        	
		    		       	    <x:navigationMenuItem id="nav_3_19_10_2_2" itemLabel="Deuda/Intereses/Devengados por Trabajador" action="go_reportNuevoRegimenTrabajadorForm" />	    		        	
	    		        	</x:navigationMenuItem>	    		        
	    		           		        	
		    		   	</x:navigationMenuItem>	    		        
	    		        <x:navigationMenuItem id="nav_3_19_5" itemLabel="Tasas BCV" action="go_tasaBcvForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_3_12" itemLabel="Jubilaciones y Pensiones">
				         <x:navigationMenuItem id="nav_3_12_1" itemLabel="Par�metros Jubilaci�n" action="go_parametroJubilacionForm" />
			            <x:navigationMenuItem id="nav_3_12_2" itemLabel="Par�metros Pension Invalidez" action="go_pensionInvalidezForm" />
			            <x:navigationMenuItem id="nav_3_12_3" itemLabel="Par�metros Pension Sobreviviente" action="go_pensionSobrevivienteForm" />
			            <x:navigationMenuItem id="nav_3_12_7" itemLabel="Datos Personal Jubilado" action="go_jubiladoForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_3_11" itemLabel="Formulaci�n y Ejecuci�n Presupuestaria">
			          			        	
			        	<x:navigationMenuItem id="nav_3_11_3" itemLabel="Ejecuci�n Presupuestaria" >
					        				          
					         <x:navigationMenuItem id="nav_3_11_3_3" itemLabel="Reportes de Estructura Presupuestaria"  >
					            	<x:navigationMenuItem id="nav_3_11_3_3_1" itemLabel="Cat�logo Cuentas" action="go_reportCatalogoCuentasForm" />
					            	<x:navigationMenuItem id="nav_3_11_3_3_2" itemLabel="Conceptos/Cuentas" action="go_reportConceptoCuentasForm" />
					            	<x:navigationMenuItem id="nav_3_11_3_3_3" itemLabel="Categorias Presupuestarias" action="go_reportCategoriaPresupuestariaForm" />		
					            	<x:navigationMenuItem id="nav_3_11_3_3_4" itemLabel="Asignacion de Proyectos/Acciones" action="go_reportProyectoAccionForm" />					            	   	            	
					         </x:navigationMenuItem>   	   
					         
						</x:navigationMenuItem>  
						 <x:navigationMenuItem id="nav_3_11_4" itemLabel="Formulaci�n Presupuestaria" >
					        
					          <x:navigationMenuItem id="nav_3_11_4_2" itemLabel="Reportes"  >
					            	<x:navigationMenuItem id="nav_3_11_4_2_1" itemLabel="Cuadros por Tipo de Cargo" action="go_reportCuadroOnapreForm" />
					         </x:navigationMenuItem> 					         				         
					        
						</x:navigationMenuItem>     	   	 	   
			        </x:navigationMenuItem>        	   
				   
				  <x:navigationMenuItem id="nav_3_16" itemLabel="Consultas/Reportes Historicos">
			            <x:navigationMenuItem id="nav_3_16_1" itemLabel="Por Trabajador" action="go_historicoConceptoTrabajadorForm" />			            
			            <x:navigationMenuItem id="nav_3_16_2" itemLabel="Por Concepto" action="go_reportHistoricoConceptoForm" />		
			            <x:navigationMenuItem id="nav_3_16_0" itemLabel="Por Grupo Nomina" action="go_reportHistoricoGrupoNominaForm" />		
			            <x:navigationMenuItem id="nav_3_16_3" itemLabel="Por Unidad Administradora/Categoria" action="go_reportHistoricoAdministradoraForm" />		                	   			            	                	   
			            <x:navigationMenuItem id="nav_3_16_4" itemLabel="Por Devengado/Incidencia" action="go_reportHistoricoDevengadoForm" />	
			            <x:navigationMenuItem id="nav_3_16_5" itemLabel="Reportes de Historico de Nominas" action="go_reportNominaHistoricoForm" />			                	   
			      </x:navigationMenuItem>
			      
			      
			      
			   
			   </x:navigationMenuItem>
			   
			   <x:navigationMenuItem id="nav_4" itemLabel="Bienestar |">				   
			        <x:navigationMenuItem id="nav_4_1" itemLabel="Juguetes">
			            <x:navigationMenuItem id="nav_4_1_1" itemLabel="Par�metros" action="go_parametroJugueteForm" />
			             </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_2" itemLabel="Utiles Escolares">
			            <x:navigationMenuItem id="nav_4_2_1" itemLabel="Par�metros" action="go_parametroUtilesForm" />
			           </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_3" itemLabel="Becas a Familiares">
			            <x:navigationMenuItem id="nav_4_3_1" itemLabel="Niveles Educativos" action="go_nivelBecaForm" />
			            <x:navigationMenuItem id="nav_4_3_2" itemLabel="Par�metros Generales" action="go_parametroBecaGeneralForm" />
			            <x:navigationMenuItem id="nav_4_3_3" itemLabel="Par�metros por Nivel" action="go_parametroBecaForm" />
			            
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_4" itemLabel="Guarder�as">
			            <x:navigationMenuItem id="nav_4_4_1" itemLabel="Par�metros" action="go_parametroGuarderiaForm" />
			       </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_5" itemLabel="Dotaciones">
			            <x:navigationMenuItem id="nav_4_5_1" itemLabel="Tipos Dotaci�n" action="go_tipoDotacionForm" />
			            <x:navigationMenuItem id="nav_4_5_2" itemLabel="Subtipos Dotaci�n" action="go_subtipoDotacionForm" />
			            <x:navigationMenuItem id="nav_4_5_3" itemLabel="Dotaci�n por Cargo" action="go_dotacionCargoForm" />
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_6" itemLabel="Polizas de Seguro">
				        <x:navigationMenuItem id="nav_4_6_1" itemLabel="Establecimientos de Salud" action="go_establecimientoSaludForm" />
				        <x:navigationMenuItem id="nav_4_6_2" itemLabel="Tipos de Siniestro" action="go_tipoSiniestroForm" />
			            <x:navigationMenuItem id="nav_4_6_3" itemLabel="P�lizas" action="go_polizaForm" />
			            <x:navigationMenuItem id="nav_4_6_4" itemLabel="Planes" action="go_planPolizaForm" />
			            <x:navigationMenuItem id="nav_4_6_5" itemLabel="Primas por Planes" action="go_primasPlanForm" />
			            <x:navigationMenuItem id="nav_4_6_6" itemLabel="Primas por Cargo" action="go_primasCargoForm" />
			            	     
			        </x:navigationMenuItem>
			        <x:navigationMenuItem id="nav_4_7" itemLabel="Tickets">
				        <x:navigationMenuItem id="nav_4_7_0" itemLabel="Grupos de Tickets" action="go_grupoTicketForm" />
			        	<x:navigationMenuItem id="nav_4_7_1" itemLabel="Parametros de Tickets" action="go_parametroTicketForm" />
			        </x:navigationMenuItem>
			       
			        <x:navigationMenuItem id="nav_4_8" itemLabel="Reportes" >
			           	<x:navigationMenuItem id="nav_4_8_1" itemLabel="Familiares" action="go_reportFamiliaresForm" />
			           	<x:navigationMenuItem id="nav_4_8_2" itemLabel="Madres y Padres Trabajadores" action="go_reportMadresPadresForm" />
			        </x:navigationMenuItem>			        
			   </x:navigationMenuItem>
			   
			   <x:navigationMenuItem id="nav_20" itemLabel="Reportes |">
			        <x:navigationMenuItem id="nav_20_1" itemLabel="Por Tipo de Personal" action="go_reportTrabajadorTipoPersonalForm" />
			        <x:navigationMenuItem id="nav_20_2" itemLabel="Por Regi�n" action="go_reportTrabajadorRegionForm" />
			        <x:navigationMenuItem id="nav_20_3" itemLabel="Por Sede" action="go_reportTrabajadorSedeForm" />
			        <x:navigationMenuItem id="nav_20_4" itemLabel="Por Unidad Ejecutora" action="go_reportTrabajadorUnidadEjecutoraForm" />
			        <x:navigationMenuItem id="nav_20_5" itemLabel="Por Unidad Funcional" action="go_reportTrabajadorUnidadFuncionalForm" />	        
			        <x:navigationMenuItem id="nav_20_6" itemLabel="Por Dependencia" action="go_reportTrabajadorDependenciaForm" />
			        <x:navigationMenuItem id="nav_20_7" itemLabel="Por Situaci�n" action="go_reportTrabajadorSituacionForm" />  	 
			        <x:navigationMenuItem id="nav_20_8" itemLabel="Ingresos por Per�odo" action="go_reportIngresoForm" />
			        <x:navigationMenuItem id="nav_20_9" itemLabel="Egresos por Per�odo" action="go_reportEgresoForm" />  	  
			        <x:navigationMenuItem id="nav_20_10" itemLabel="Fechas">        
				        <x:navigationMenuItem id="nav_20_10_1" itemLabel="Fechas por Regi�n" action="go_reportFechasRegionForm" />   
				        <x:navigationMenuItem id="nav_20_10_2" itemLabel="Fechas por Sede" action="go_reportFechasSedeForm" /> 
				        <x:navigationMenuItem id="nav_20_10_3" itemLabel="Fechas por Unidad Funcional" action="go_reportFechasUnidadFuncionalForm" />
				        <x:navigationMenuItem id="nav_20_10_4" itemLabel="Fechas por Dependencia" action="go_reportFechasDependenciaForm" />   	        	        
				        <x:navigationMenuItem id="nav_20_10_5" itemLabel="Edad y Tiempo Servicio/Tipo Personal" action="go_reportFechasNacimientoServicioTipoPersonalForm" />   
				        <x:navigationMenuItem id="nav_20_10_6" itemLabel="Edad y Tiempo Servicio/Region" action="go_reportFechasNacimientoServicioRegionForm" />   
				        <x:navigationMenuItem id="nav_20_10_7" itemLabel="Edad y Tiempo Servicio/UEL" action="go_reportFechasNacimientoServicioUnidadEjecutoraForm" />   
			        </x:navigationMenuItem>   
			   </x:navigationMenuItem>
			        
			   <x:navigationMenuItem id="nav_6" itemLabel="Indicadores |">
			       <x:navigationMenuItem id="nav_6_1" itemLabel="Trabajadores por Tipo Personal" action="go_trabajadoresForm" />
			       <%/*<x:navigationMenuItem id="nav_6_2" itemLabel="Entidades Educativas por Caracteristicas" action="go_queryClasificacionDependenciaForm" />	*/%>		
			       <x:navigationMenuItem id="nav_6_3" itemLabel="Costo de N�mina" action="go_mapCostoNominaForm" />			
			       <x:navigationMenuItem id="nav_6_4" itemLabel="Graficos" action="go_reportGraficosForm" />			
			   </x:navigationMenuItem>
			   <x:navigationMenuItem id="nav_5" itemLabel="Sistema">
			       <x:navigationMenuItem id="nav_5_1" itemLabel="Usuarios">
			            <x:navigationMenuItem id="nav_5_1_1" itemLabel="Usuarios" action="go_usuarioForm" />
			            <x:navigationMenuItem id="nav_5_1_2" itemLabel="Usuarios por Organismo" action="go_usuarioOrganismoForm" />
			            <x:navigationMenuItem id="nav_5_1_3" itemLabel="Usuarios por Tipo de Personal" action="go_usuarioTipoPersonalForm" />
			            <x:navigationMenuItem id="nav_5_1_0" itemLabel="Usuarios por Unidad Funcional" action="go_usuarioUnidadFuncionalForm" />
			            <x:navigationMenuItem id="nav_5_1_4" itemLabel="Roles" action="go_rolForm" />
			            <x:navigationMenuItem id="nav_5_1_5" itemLabel="Opciones" action="go_opcionForm" />
			            <x:navigationMenuItem id="nav_5_1_6" itemLabel="Asignar Opciones a Roles" action="go_rolOpcionForm" />
			            <x:navigationMenuItem id="nav_5_1_7" itemLabel="Asignar Roles a Usuarios" action="go_usuarioRolForm" />            		
			            <x:navigationMenuItem id="nav_5_1_8" itemLabel="Cambiar Password" action="go_changePasswordForm" />            		
			       </x:navigationMenuItem>
			       <x:navigationMenuItem id="nav_5_2" itemLabel="Reportes IDs" action="go_reportAdministracionIdForm" />                         
			   	   <x:navigationMenuItem id="nav_5_3" itemLabel="Reporte de Auditoria" action="go_reportAuditoriaForm" />                         
			   	  
			   </x:navigationMenuItem>
			   
			</x:jscookMenu>
			 <%} else {%>

			 
			 <x:jscookMenu layout="hbr" theme="ThemeOffice" rendered="#{loginSession.valid}">
			    
			    <x:navigationMenuItem id="nav_2_1" itemLabel="Expediente">

			            <x:navigationMenuItem id="nav_2_1_1" itemLabel="Datos Personales" action="go_personalForm" />
			            <x:navigationMenuItem id="nav_2_1_2" itemLabel="Educaci�n Formal" action="go_educacionForm"  />
			            <x:navigationMenuItem id="nav_2_1_3" itemLabel="Profesiones" action="go_profesionTrabajadorForm" />
			            <x:navigationMenuItem id="nav_2_1_4" itemLabel="Estudios Informales" action="go_estudioForm" />
			            <x:navigationMenuItem id="nav_2_1_5" itemLabel="Certificaciones" action="go_certificacionForm" />
			            <x:navigationMenuItem id="nav_2_1_6" itemLabel="Experiencia Laboral (S. Privado)" action="go_experienciaForm" />
			            <x:navigationMenuItem id="nav_2_1_7" itemLabel="Idiomas" action="go_idiomaForm" />
			            <x:navigationMenuItem id="nav_2_1_8" itemLabel="Grupo Familiar" action="go_familiarForm" />
			            <x:navigationMenuItem id="nav_2_1_9" itemLabel="Afiliaciones a Gremios" action="go_afiliacionForm" />
			            <x:navigationMenuItem id="nav_2_1_10" itemLabel="Actividades Docentes" action="go_actividadDocenteForm" />
			            <x:navigationMenuItem id="nav_2_1_11" itemLabel="Otras Actividades" action="go_otraActividadForm" />
			            <x:navigationMenuItem id="nav_2_1_12" itemLabel="Habilidades/Competencias" action="go_habilidadForm" />
			            <x:navigationMenuItem id="nav_2_1_13" itemLabel="Reconocimientos" action="go_reconocimientoForm" />
			            <x:navigationMenuItem id="nav_2_1_14" itemLabel="Sanciones" action="go_sancionForm" />
			            <x:navigationMenuItem id="nav_2_1_15" itemLabel="Averiguaciones Administrativas" action="go_averiguacionForm" />
			            <x:navigationMenuItem id="nav_2_1_16" itemLabel="Certificados de Carrera" action="go_certificadoForm" />
			            <x:navigationMenuItem id="nav_2_1_17" itemLabel="Publicaciones" action="go_publicacionForm" />            
			            <x:navigationMenuItem id="nav_2_1_18" itemLabel="Pasantias" action="go_pasantiaForm" />
			            <x:navigationMenuItem id="nav_2_1_19" itemLabel="Declaraciones Juradas" action="go_declaracionForm" />            
			            <x:navigationMenuItem id="nav_2_1_20" itemLabel="Reportes" >
			            	<x:navigationMenuItem id="nav_2_1_20_1" itemLabel="Hoja de Vida" action="go_reportCurriculumForm" />			            	
			            </x:navigationMenuItem>
			        
				</x:navigationMenuItem>
			</x:jscookMenu>
			 <%} %>
