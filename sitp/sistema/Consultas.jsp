<%@ taglib uri="/WEB-INF/tags.tld" prefix="tags" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>SITP</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sitp/style/webstyle.css" 
		rel="stylesheet" type="text/css">
	
    
    <jsp:include page="/inc/functions.jsp" />
    
</head>

<body>
<f:view>
	<x:saveState value="#{consultasForm}" />
    <f:loadBundle basename="sigefirrhh.custMessages" var="custMessages"/>
	<jsp:include page="/inc/top.jsp" />

	<table width="770"  border="0" cellspacing="0" cellpadding="5" align="left">
		<tr>
			<!-- Men� Izquierdo -->
			<td width="200" valign="top">
				<div align="left">
	                 
				</div>
			</td>
			<td width="570" valign="top">
				<h:form id="formConsultas">
				    				
    				<table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" class="toptable">
    					<tr>
    						<td valign="top">													
    							<b>
    								Consultas Consolidadas
    							</b>
    						</td>
    						<td align="right">
    							
    						</td>
    					</tr>
    				</table>
    				<table width="100%" class="toptable">
    				
    					<tr>
    						<td align="left">
    							<h:messages errorClass="error" styleClass="success"  />
    						</td>
    					</tr>					
    						<td>
    							<table width="100%" class="bandtable">
    								<tr>
    									<td>
    										Par�metros de la Consulta
    									</td>
    								</tr>
    							</table>
    										
    							<table width="100%" class="datatable">    								    																	
    								<tr>
    									<td>
    										Tipo Consulta
    									</td>
    									<td width="100%">
    										<h:selectOneRadio value="#{consultasForm.consulta}"
	    										onchange="this.form.submit()" 
	    										layout="pageDirection">
   	 											<f:selectItem itemLabel="Trayectoria de Trabajador" itemValue="0" />  													
    											<f:selectItem itemLabel="Cantidad de Trabajadores" itemValue="1" />  													
    											<f:selectItem itemLabel="Cantidad de Trabajadores por Cargo" itemValue="2" /><br>
    											<f:selectItem itemLabel="Cantidad de Trabajadores por Edad, Tiempo Servicio y Sexo" itemValue="3" />
    											<f:selectItem itemLabel="Cantidad de Trabajadores por Rango de Sueldo B�sico" itemValue="5" />
    											<f:selectItem itemLabel="Cantidad de Trabajadores por Rango de Sueldo Integral" itemValue="8" />
    											<f:selectItem itemLabel="Registro de Estructura de Cargos" itemValue="10" />
    											<f:selectItem itemLabel="Cantidad de Cargos Vacantes Libre Nombramiento y Remoci�n" itemValue="6" />
    											<f:selectItem itemLabel="Cantidad de Cargos Vacantes de Carrera" itemValue="7" />
    											<f:selectItem itemLabel="Costos de N�mina" itemValue="4" /><br>
    											<f:selectItem itemLabel="Gasto Anual de Concepto a la Fecha" itemValue="9" /><br>
    											
    										</h:selectOneRadio>
    									</td>
    								</tr>      								
									 <tr>
    									<td width="20%">
    										C�dula
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{consultasForm.consulta==0}"		
    											size="10"
    											id="cedula"
    											maxlength="8"
    											value="#{consultasForm.cedula}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>		 		
									
									 <tr>
    									<td width="20%">
    										C�digo Cargo
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{consultasForm.consulta==2}"		
    											size="10"
    											id="cargo"
    											maxlength="10"
    											value="#{consultasForm.cargo}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	 	
									 <tr>
    									<td width="20%">
    										A�o
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{consultasForm.consulta==4||consultasForm.consulta==9}"		
    											size="5"
    											id="anio"
    											maxlength="4"
    											value="#{consultasForm.anio}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>		 		
									 <tr>
    									<td width="20%">
    										Mes
    									</td>
    									<td width="30%" colspan="3">    									
    									
    										<h:selectOneMenu value="#{consultasForm.mes}"    
	    										rendered="#{consultasForm.consulta==4||consultasForm.consulta==9}"		
    											onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Total a la Fecha" itemValue="0" />
                                            	<f:selectItem itemLabel="Enero" itemValue="1" />
                                            	<f:selectItem itemLabel="Febrero" itemValue="2" />
                                            	<f:selectItem itemLabel="Marzo" itemValue="3" />
                                            	<f:selectItem itemLabel="Abril" itemValue="4" />
                                            	<f:selectItem itemLabel="Mayo" itemValue="5" />
                                            	<f:selectItem itemLabel="Junio" itemValue="6" />
                                            	<f:selectItem itemLabel="Julio" itemValue="7" />
                                            	<f:selectItem itemLabel="Agosto" itemValue="8" />
                                            	<f:selectItem itemLabel="Septiembre" itemValue="9" />
                                            	<f:selectItem itemLabel="Octubre" itemValue="10" />
                                            	<f:selectItem itemLabel="Noviembre" itemValue="11" />
                                            	<f:selectItem itemLabel="Diciembre" itemValue="12" />
                                            	
                                            	
                                            </h:selectOneMenu>
    									
    									</td>
									</tr>	 								
    								
    								<tr>
    									<td>
    										Concepto
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{consultasForm.concepto}"    
	    										rendered="#{consultasForm.consulta==9}"													
    											onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Sueldo/Salario" itemValue="1" />
                                            	<f:selectItem itemLabel="Compensaci�n" itemValue="2" />
                                            	<f:selectItem itemLabel="Primas del Cargo" itemValue="3" />
                                            	<f:selectItem itemLabel="Primas del Trabajador" itemValue="4" />
                                            	<f:selectItem itemLabel="Otras Asignaciones" itemValue="5" />
                                            	<f:selectItem itemLabel="Bono Vacacional" itemValue="6" />
                                            	<f:selectItem itemLabel="Bono Fin de A�o" itemValue="7" />
                                            	<f:selectItem itemLabel="Deducciones" itemValue="8" />
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								
    								<tr>
    									<td width="20%">
    										Fecha Tope
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText    
	    										rendered="#{consultasForm.consulta==3}"						    															
    											size="10"
    											id="FechaTope"
    											maxlength="10"
    											onblur="javascript:check_date(this)"
    											value="#{consultasForm.fechaTope}"
    											required="false" >    										
    											<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		pattern="dd-MM-yyyy" />
                	                        </h:inputText>
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Edad M�nima
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
    										    rendered="#{consultasForm.consulta==3}"						
    											size="5"
    											id="EdadMinima"
    											maxlength="3"
    											value="#{consultasForm.edadMinima}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Edad M�xima
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{consultasForm.consulta==3}"		
    											size="5"
    											id="EdadMaxima"
    											maxlength="3"
    											value="#{consultasForm.edadMaxima}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Tiempo Servicio M�nimo
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{consultasForm.consulta==3}"		
    											size="5"
    											id="ServicioMinimo"
    											maxlength="3"
    											value="#{consultasForm.servicioMinimo}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td width="20%">
    										Tiempo de Servicio M�ximo
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{consultasForm.consulta==3}"		
    											size="5"
    											id="ServicioMaximo"
    											maxlength="3"
    											value="#{consultasForm.servicioMaximo}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>	
									<tr>
    									<td>
    										Sexo
    									</td>
    									<td width="100%">
    										<h:selectOneMenu value="#{consultasForm.sexo}"    
	    										rendered="#{consultasForm.consulta==3}"													
    											onchange="this.form.submit()"
                                            	immediate="false">                                            	
                                            	<f:selectItem itemLabel="Femenino" itemValue="F" />
                                            	<f:selectItem itemLabel="Masculino" itemValue="M" />
                                            											
                                            </h:selectOneMenu>
    									</td>
    								</tr>
    								<tr>
    									<td width="20%">
    										Sueldo Desde
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{consultasForm.consulta==5 || consultasForm.consulta==8}"		
    											size="15"
    											id="sueldoDesde"
    											maxlength="15"
    											value="#{consultasForm.sueldoDesde}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>
									<tr>
    									<td width="20%">
    										Sueldo Hasta
    									</td>
    									<td width="30%" colspan="3">    									
    										<h:inputText
	    										rendered="#{consultasForm.consulta==5 || consultasForm.consulta==8}"		
    											size="15"
    											id="sueldoHasta"
    											maxlength="15"
    											value="#{consultasForm.sueldoHasta}"
    											required="false" >    								
                	                        </h:inputText>
    									</td>
									</tr>
    								<tr>
    									<td colspan="2" align="center">										
    										<h:commandButton image="/images/run.gif"
    											action="#{consultasForm.ejecutar}"/>
    									<td>
    								</tr>
    							</table>
    							<table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Totales
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>
										
										  <h:dataTable id="totalTrayectoria"
                                                rendered="#{consultasForm.showTotal&&consultasForm.consulta==0}"    
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_ColumnCenter, standardTable_ColumnRight"
                                                var="total"
                                				value="#{consultasForm.total}"                                                
                                                rows="1000"
                                                width="100%">
                                                <h:column>
                                					<h:outputText 
                                						value="#{total.nombre}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>
                                        
                                            <h:dataTable id="totalTrabajadores"
                                                rendered="#{consultasForm.showTotal&&consultasForm.consulta==1}"    
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_ColumnRight"
                                                var="total"
                                				value="#{consultasForm.total}"                                                
                                                rows="1000"
                                                width="100%">
                                			
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.categoria}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.relacion}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>
                                                
                                            <h:dataTable id="totalTrabajadoresPorCargo"
                                                rendered="#{consultasForm.showTotal&&consultasForm.consulta==2}"    
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_ColumnCenter, standardTable_ColumnRight"
                                                var="total"
                                				value="#{consultasForm.total}"                                                
                                                rows="1000"
                                                width="100%">
                                                 <h:column>
                                					<h:outputText 
                                						value="#{total.descripcionCargo}" >                               						
                                					</h:outputText>
                                				</h:column>                                     				
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>    
                                                                            
                            			
                            			<h:dataTable id="totalTrabajadoresEdadServicioSexo"
                                                rendered="#{consultasForm.showTotal&&consultasForm.consulta==3}"    
                                               styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_ColumnRight"
                                                var="total"
                                				value="#{consultasForm.total}"                                                
                                                rows="1000"
                                                width="100%">
                                			
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.categoria}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.relacion}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>
                                            
                                        <h:dataTable id="totalTrabajadoresPorSueldo"
                                                rendered="#{consultasForm.showTotal&&(consultasForm.consulta==5 || consultasForm.consulta==8)}"    
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_ColumnRight"
                                                var="total"
                                				value="#{consultasForm.total}"                                                
                                                rows="1000"
                                                width="100%">
                                			
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.categoria}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.relacion}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>
                                        <h:dataTable id="totalCargosVacantes99"
                                                rendered="#{consultasForm.showTotal&&(consultasForm.consulta==6 || consultasForm.consulta==7|| consultasForm.consulta==10)}"    
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_ColumnCenter, standardTable_ColumnRight"
                                                var="total"
                                				value="#{consultasForm.total}"                                                
                                                rows="1000"
                                                width="100%">
                                                 <h:column>
                                					<h:outputText 
                                						value="#{total.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>                                     				
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>
                                         <h:dataTable id="totalCostoNomina"
                                                rendered="#{consultasForm.showTotal&&consultasForm.consulta==4}"    
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_ColumnRight"
                                                var="total"
                                				value="#{consultasForm.total}"                                                
                                                rows="1000"
                                                width="100%">
                                			    <h:column>
                                					<h:outputText 
                                						value="#{total.categoria}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.relacion}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.monto}" >  
                                						<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>                             						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>    
                                
                                
                                            <h:dataTable id="totalGastoConcepto"
                                                rendered="#{consultasForm.showTotal&&consultasForm.consulta==9}"    
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column,standardTable_ColumnRight"
                                                var="total"
                                				value="#{consultasForm.total}"                                                
                                                rows="1000"
                                                width="100%">
                                			    <h:column>
                                					<h:outputText 
                                						value="#{total.descripcionConcepto}" >                               						
                                					</h:outputText>
                                				</h:column>
                                			    <h:column>
                                					<h:outputText 
                                						value="#{total.categoria}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.relacion}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{total.monto}" >  
                                						<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>                             						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>    
                                                                            
                            	
                                                                            
                            			</td>
                            			
                    				</tr>
                    			</table>
    							<table class="toptable" width="100%">
									<tr>
										<td>
										<table width="100%" class="bandtable">
											<tr>
												<td>
													Detalle
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>
										
										
										  <h:dataTable id="resultadoTrayectoria"     
	                                            rendered="#{consultasForm.showResultado&&consultasForm.consulta==0}"                                       	                                        
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column,standardTable_ColumnRight"
                                                var="resultado"
                                				value="#{consultasForm.resultado}"                                                
                                                rows="1000"
                                                width="100%">
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.movimiento}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.descripcionCargo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.fecha}" > 
                                						<f:convertDateTime timeZone="#{dateTime.timeZone}" 
                	                        		      pattern="dd-MM-yyyy" />
                                						                              						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>      
                                            
										
                                            <h:dataTable id="resultadoTrabajadores"     
	                                            rendered="#{consultasForm.showResultado&&consultasForm.consulta==1}"                                       	                                        
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column,standardTable_ColumnRight"
                                                var="resultado"
                                				value="#{consultasForm.resultado}"                                                
                                                rows="1000"
                                                width="100%">
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.categoria}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.relacion}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>      
                                             <h:dataTable id="resultadoTrabajadoresPorCargo"  
	                                            rendered="#{consultasForm.showResultado&&consultasForm.consulta==2}"                                             	                                        
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column, standardTable_ColumnCenter,standardTable_ColumnRight"
                                                var="resultado"
                                				value="#{consultasForm.resultado}"                                                
                                                rows="1000"
                                                width="100%">
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				 <h:column>
                                					<h:outputText 
                                						value="#{resultado.descripcionCargo}" >                               						
                                					</h:outputText>
                                				</h:column>                                     				
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>      
                                            <h:dataTable id="resultadoEdadServicioSexo"  
	                                            rendered="#{consultasForm.showResultado&&consultasForm.consulta==3}"                                             	                                        
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column,standardTable_ColumnRight"
                                                var="resultado"
                                				value="#{consultasForm.resultado}"                                                
                                                rows="1000"
                                                width="100%">
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.categoria}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.relacion}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:dataTable id="resultadoTrabajadoresPorSueldo"     
	                                            rendered="#{consultasForm.showResultado&&(consultasForm.consulta==5 || consultasForm.consulta==8)}"                                       	                                        
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column,standardTable_ColumnRight"
                                                var="resultado"
                                				value="#{consultasForm.resultado}"                                                
                                                rows="1000"
                                                width="100%">
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.categoria}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.relacion}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>
                                            
                                            <h:dataTable id="resultadoCargosVacantes"  
	                                            rendered="#{consultasForm.showResultado&&(consultasForm.consulta==6||consultasForm.consulta==7)}"                                             	                                        
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column, standardTable_ColumnCenter,standardTable_ColumnRight"
                                                var="resultado"
                                				value="#{consultasForm.resultado}"                                                
                                                rows="1000"
                                                width="100%">
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				 <h:column>
                                					<h:outputText 
                                						value="#{resultado.descripcionCargo}" >                               						
                                					</h:outputText>
                                				</h:column>                                     				
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>
                                                
                                            <h:dataTable id="resultadoEstructuraCargos"  
	                                           rendered="#{consultasForm.showResultado&&consultasForm.consulta==10}"                                             	                                        
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column, standardTable_ColumnCenter,standardTable_ColumnRight"
                                                var="resultado"
                                				value="#{consultasForm.resultado}"                                                
                                                rows="1000"
                                                width="100%">
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				 <h:column>
                                					<h:outputText 
                                						value="#{resultado.movimiento}" >                               						
                                					</h:outputText>
                                				</h:column>                                     				
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.cantidad}" >                               						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>    
                                                
                                            <h:dataTable id="resultadoCostoNomina"     
	                                            rendered="#{consultasForm.showResultado&&consultasForm.consulta==4}"                                       	                                        
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column,standardTable_ColumnRight"
                                                var="resultado"
                                				value="#{consultasForm.resultado}"                                                
                                                rows="1000"
                                                width="100%">
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.categoria}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.relacion}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.monto}" >  
                                						<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>                             						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>  
                                            
                                            
                                            <h:dataTable id="resultadoGastoConcepto"     
	                                            rendered="#{consultasForm.showResultado&&consultasForm.consulta==9}"                                       	                                        
                                                styleClass="datatable"
                                                headerClass="standardTable_Header"
                                                footerClass="standardTable_Header"
                                                rowClasses="standardTable_Row1,standardTable_Row2"
                                                columnClasses="standardTable_Column,standardTable_ColumnCentered,standardTable_Column,standardTable_ColumnRight,standardTable_ColumnRight"
                                                var="resultado"
                                				value="#{consultasForm.resultado}"                                                
                                                rows="1000"
                                                width="100%">
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.organismo}" >                               						
                                					</h:outputText>
                                				</h:column>
                                			
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.descripcionConcepto}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.categoria}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.relacion}" >                               						
                                					</h:outputText>
                                				</h:column>
                                				<h:column>
                                					<h:outputText 
                                						value="#{resultado.monto}" >  
                                						<f:convertNumber minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/>                             						
                                					</h:outputText>
                                				</h:column>
                                            </h:dataTable>                                              
                            			</td>
                    				</tr>
                    			</table>
    						</td>
    					</tr>
    				</table>										
				</h:form>
			</td>
		</tr>
	</table>
</f:view>	
</body>
</html>