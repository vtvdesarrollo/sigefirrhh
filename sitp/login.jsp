<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://myfaces.apache.org/extensions" prefix="x" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<title>Sitp</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="/sitp/style/webstyle.css" 
		rel="stylesheet" type="text/css">
		
</head>

<body onload="firstFocus();">
<f:view>

	<jsp:include page="/inc/top.jsp" />

	<f:verbatim>
	<table width="800"  border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" valign="top" align="center">
					<tr>
						<td align="center"></f:verbatim>
						<h:form>
							<f:verbatim><table class="datatable" width="500" align="center">
            					<tr>
            						<td align="right"></f:verbatim>
										<h:commandButton value="Ingresar" 
                                    		action="#{loginSitpForm.consolidado}" />
									<f:verbatim></td>
            						<td align="left">
										Consolidado
            						</td>
            					</tr>
            					<tr>
            						<td>
            						</td>
            					</tr>
            					<tr>
            						<td>
            						</td>
            						<td align="left">
										Organismo
            						</td>
            					</tr>
            					<tr>
            						<td align="right"></f:verbatim>
										<h:commandButton value="Ingresar" 
                                    		action="#{loginSitpForm.organismo}" />
									<f:verbatim></td>
            						<td align="left"></f:verbatim>
                                        <h:selectOneMenu value="#{loginSitpForm.schema}"
                                        	immediate="false">
                                           	<f:selectItem itemLabel="Ministerio de Finanzas" itemValue="sitp_mf" />
                                           	<f:selectItem itemLabel="Ministerio de Interior y Justicia" itemValue="sitp_mij" />
                                           	<f:selectItem itemLabel="Ministerio de Educación Superior" itemValue="sitp_mes" />
                                           	<f:selectItem itemLabel="Ministerio de Salud" itemValue="sitp_ms" />
                                           	<f:selectItem itemLabel="Ministerio del Trabajo y Seguridad Social" itemValue="sitp_mintra" />
                                           	<f:selectItem itemLabel="Ministerio de Relaciones Exteriores" itemValue="sitp_mre" />
                                           	<f:selectItem itemLabel="Ministerio de Ciencia y Tecnología" itemValue="sitp_mct" />
                                           	<f:selectItem itemLabel="Ministerio de Comunicación e Información" itemValue="sitp_mci" />
                                           	<f:selectItem itemLabel="Ministerio de Energía y Petroleo" itemValue="sitp_mep" />
                                           	<f:selectItem itemLabel="Presidencia" itemValue="sitp_pr" />
                                           	<f:selectItem itemLabel="VicePresidencia" itemValue="sitp_vp" />
                                           
                                        </h:selectOneMenu>
            						<f:verbatim></td>
            					</tr>
            				</table></f:verbatim>
						</h:form>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</f:view>
</body>
</html>